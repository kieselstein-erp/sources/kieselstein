<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="finanz_kontoblatt_zahllast1" pageWidth="535" pageHeight="842" columnWidth="535" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="0ac2a9db-f95f-4192-b3dc-0ae5e2410bc1">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="REPORT_DIRECTORY" class="java.lang.String"/>
	<parameter name="AnwesendGesamt" class="java.lang.Double"/>
	<field name="Sort" class="java.lang.String"/>
	<field name="Buchungsdatum" class="java.util.Date">
		<fieldDescription><![CDATA[Buchungs-Datum]]></fieldDescription>
	</field>
	<field name="OriginalBuchungsdatum" class="java.util.Date"/>
	<field name="Gegenkonto" class="java.lang.String">
		<fieldDescription><![CDATA[Gegen-Kontonummer]]></fieldDescription>
	</field>
	<field name="Buchungsart" class="java.lang.String">
		<fieldDescription><![CDATA[Buchungsart]]></fieldDescription>
	</field>
	<field name="Beleg" class="java.lang.String">
		<fieldDescription><![CDATA[Beleg-Nr.]]></fieldDescription>
	</field>
	<field name="Soll" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[Soll]]></fieldDescription>
	</field>
	<field name="Haben" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[Haben]]></fieldDescription>
	</field>
	<field name="Text" class="java.lang.String">
		<fieldDescription><![CDATA[Text]]></fieldDescription>
	</field>
	<variable name="Saldo" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[($F{Soll}==null ? new BigDecimal(0.0000) : $F{Soll})
.subtract($F{Haben}==null ? new BigDecimal(0.0000) : $F{Haben})]]></variableExpression>
	</variable>
	<variable name="Quartal" class="java.lang.Integer">
		<variableExpression><![CDATA[$F{Buchungsdatum}.getMonth() / 3]]></variableExpression>
	</variable>
	<variable name="QuartalSaldo" class="java.math.BigDecimal" resetType="Group" resetGroup="Quartal" calculation="Sum">
		<variableExpression><![CDATA[($F{Soll}==null ? new BigDecimal(0.0000) : $F{Soll})
.subtract($F{Haben}==null ? new BigDecimal(0.0000) : $F{Haben})]]></variableExpression>
	</variable>
	<group name="Quartal">
		<groupExpression><![CDATA[$V{Quartal}]]></groupExpression>
		<groupHeader>
			<band height="20">
				<textField>
					<reportElement mode="Transparent" x="0" y="0" width="20" height="16" forecolor="#000000" backcolor="#FFFFFF" uuid="782df7ca-8288-405b-8343-5191d6bc545f"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
						<font fontName="Arial" size="12" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Quartal} == null ? 1 : $V{Quartal}.intValue()+2
/* Hinweis: Im Group Header ist das der Werte des VORHERIGEN Ergebnisses
   Daher +2 */]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="20" y="0" width="60" height="16" uuid="e74cb2b9-ee90-4a7a-b9a2-f56841becbc7"/>
					<textElement markup="none">
						<font fontName="Arial" size="12"/>
					</textElement>
					<text><![CDATA[. Quartal]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="20">
				<rectangle>
					<reportElement x="0" y="0" width="535" height="5" forecolor="#CCCCCC" backcolor="#CCCCCC" uuid="f74654eb-8063-496b-b3c3-9386b3623a5f"/>
				</rectangle>
				<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false">
					<reportElement mode="Transparent" x="380" y="5" width="50" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="e238670a-dbf0-4e07-b7c2-817bcb93fad5"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
						<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{QuartalSaldo}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement mode="Transparent" x="20" y="5" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="61ed8455-ef5f-4844-a1da-4a8cb3e2b4b3"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" markup="none">
						<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[. Quartal]]></text>
				</staticText>
				<textField>
					<reportElement mode="Transparent" x="0" y="5" width="20" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="56a974b5-8536-4293-8b46-848554ac124d"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" markup="none">
						<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{Quartal}.intValue()+1]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<pageHeader>
		<band height="21">
			<staticText>
				<reportElement x="0" y="0" width="220" height="10" uuid="22ddfbce-904a-47dc-a9f0-622d8e5baa2b"/>
				<textElement markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Sortiert nach Buchungstag, gruppiert nach Quartal]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="10" width="45" height="10" uuid="10ee877e-0745-405b-beb2-98adc96a984d"/>
				<textElement markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Datum]]></text>
			</staticText>
			<staticText>
				<reportElement x="45" y="10" width="40" height="10" uuid="779d4e2a-3602-4ee1-abf4-8951b018526f"/>
				<textElement markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[GKT]]></text>
			</staticText>
			<staticText>
				<reportElement x="85" y="10" width="25" height="10" uuid="d35cf625-f457-4a87-8c0b-1f6ffbaadc7f"/>
				<textElement markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[BA]]></text>
			</staticText>
			<staticText>
				<reportElement x="110" y="10" width="50" height="10" uuid="ea8d3ca5-6a4e-4586-a08e-217d4a00220e"/>
				<textElement markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Beleg]]></text>
			</staticText>
			<staticText>
				<reportElement x="160" y="10" width="120" height="10" uuid="af44b15e-2745-42ba-a3f3-7527173c6905"/>
				<textElement markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Text]]></text>
			</staticText>
			<staticText>
				<reportElement x="280" y="10" width="50" height="10" uuid="b591129e-a2c2-4d02-9c5e-fdc7c551502b"/>
				<textElement textAlignment="Right" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Soll]]></text>
			</staticText>
			<staticText>
				<reportElement x="330" y="10" width="50" height="10" uuid="9c632c3f-385b-4354-a9eb-dd0cf66a47fb"/>
				<textElement textAlignment="Right" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Haben]]></text>
			</staticText>
			<staticText>
				<reportElement x="380" y="10" width="50" height="10" uuid="d618b287-48c0-4101-bda4-6039cc813a12"/>
				<textElement textAlignment="Right" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Saldo]]></text>
			</staticText>
			<line>
				<reportElement key="line-2" mode="Opaque" x="0" y="20" width="535" height="1" forecolor="#000000" backcolor="#FFFFFF" uuid="d5061c81-e53b-4f58-bbce-873d4162e34b"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
		</band>
	</pageHeader>
	<detail>
		<band height="10" splitType="Stretch">
			<textField pattern="dd.MM.yyyy" isBlankWhenNull="false">
				<reportElement key="F_BUCHUNGSDATUM" mode="Transparent" x="0" y="0" width="45" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="2879ba3c-e594-4131-aa91-38065d286b0c"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Buchungsdatum}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
				<reportElement key="F_HABEN" mode="Transparent" x="330" y="0" width="50" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="58552b62-3891-4aa2-830a-4863a401c842"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Haben}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
				<reportElement key="F_SOLL" mode="Transparent" x="280" y="0" width="50" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="ee25ea4a-84db-43aa-818f-1a972a48deec"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Soll}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="F_GEGENKONTONUMMER" mode="Transparent" x="45" y="0" width="40" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="09bafde3-b09d-4ba7-960c-0869d9fc237d"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Gegenkonto}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="F_BUCHUNGSART" mode="Transparent" x="85" y="0" width="25" height="10" uuid="d5bce2e4-4fce-4977-b5f2-0b48d6edd39f"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Buchungsart}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="F_BELEGNUMMER" positionType="Float" mode="Transparent" x="110" y="0" width="50" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="ff872a6e-1cd4-4856-b012-31384f5f3b38"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Beleg}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="false">
				<reportElement key="F_TEXT" positionType="Float" mode="Transparent" x="160" y="0" width="120" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="a90d219f-7abc-414a-a402-2479ccc45f69"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Text}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false">
				<reportElement key="Saldo" mode="Transparent" x="380" y="0" width="50" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="48c3ce67-cddb-4a38-bb97-ca76a9598af0"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{Soll}==null ? new BigDecimal(0.0000) : $F{Soll})
.subtract($F{Haben}==null ? new BigDecimal(0.0000) : $F{Haben})]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
