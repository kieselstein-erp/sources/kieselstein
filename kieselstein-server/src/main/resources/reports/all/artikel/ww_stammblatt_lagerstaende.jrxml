<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ww_stammblatt_lagerstaende" pageWidth="270" pageHeight="842" columnWidth="270" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="cf033e80-66c2-4817-a91d-0ce85b801e15">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="3.4522712143931042"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Einheit" class="java.lang.String"/>
	<parameter name="Bestellmengeneinheit" class="java.lang.String"/>
	<parameter name="Umrechnungsfaktor" class="java.math.BigDecimal"/>
	<parameter name="P_MANDANT_OBJ" class="com.lp.server.system.service.ReportMandantDto" isForPrompting="false"/>
	<parameter name="P_SQLEXEC" class="com.lp.server.util.ReportSqlExecutor">
		<parameterDescription><![CDATA[Ausführung von SQL Befehlen über die ReportConnectionUrl]]></parameterDescription>
	</parameter>
	<field name="F_LAGER" class="java.lang.String">
		<fieldDescription><![CDATA[Lager]]></fieldDescription>
	</field>
	<field name="F_LAGERSTAND" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[Lagerstand]]></fieldDescription>
	</field>
	<field name="F_LAGERART" class="java.lang.String"/>
	<variable name="Konsilager" class="java.lang.Integer">
		<variableExpression><![CDATA[$P{P_SQLEXEC}.execute(
"select b_konsignationslager from ww_lager where c_nr='"+$F{F_LAGER}+"' and Mandant_c_nr='"+$P{P_MANDANT_OBJ}.getMandantCNr()+"';"
)]]></variableExpression>
	</variable>
	<variable name="Hauptlager" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{F_LAGERART}.startsWith("Hauptlager") ? $F{F_LAGERSTAND} : BigDecimal.ZERO]]></variableExpression>
	</variable>
	<variable name="Normallager" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{F_LAGERART}.startsWith("Normal") ? $F{F_LAGERSTAND} : BigDecimal.ZERO]]></variableExpression>
	</variable>
	<variable name="Kundenlager" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{F_LAGERART}.startsWith("Kundenlager") ? $F{F_LAGERSTAND} : BigDecimal.ZERO]]></variableExpression>
	</variable>
	<variable name="Sperrlager" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{F_LAGERART}.startsWith("Sperrlager") ||
$F{F_LAGERART}.startsWith("Schrott")
? $F{F_LAGERSTAND} : BigDecimal.ZERO]]></variableExpression>
	</variable>
	<variable name="Summe_ohne_Konsilager" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$V{Konsilager}==null || $V{Konsilager}.intValue() == 1 ? $F{F_LAGERSTAND} : BigDecimal.ZERO]]></variableExpression>
	</variable>
	<variable name="Hauptlager_ok" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{F_LAGERART}.startsWith("Hauptlager") &&
($V{Konsilager}==null || $V{Konsilager}.intValue() == 0)
? $F{F_LAGERSTAND} : BigDecimal.ZERO]]></variableExpression>
	</variable>
	<variable name="Normallager_ok" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{F_LAGERART}.startsWith("Normal") &&
($V{Konsilager}==null || $V{Konsilager}.intValue() == 0)
? $F{F_LAGERSTAND} : BigDecimal.ZERO]]></variableExpression>
	</variable>
	<variable name="Kundenlager_ok" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{F_LAGERART}.startsWith("Kundenlager") &&
($V{Konsilager}==null || $V{Konsilager}.intValue() == 0)
? $F{F_LAGERSTAND} : BigDecimal.ZERO]]></variableExpression>
	</variable>
	<variable name="Sperrlager_ok" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[($F{F_LAGERART}.startsWith("Sperrlager") ||
 $F{F_LAGERART}.startsWith("Schrott") ) &&
($V{Konsilager}==null || $V{Konsilager}.intValue() == 0)
? $F{F_LAGERSTAND} : BigDecimal.ZERO]]></variableExpression>
	</variable>
	<title>
		<band height="16" splitType="Stretch">
			<staticText>
				<reportElement key="Lagerstände" mode="Transparent" x="0" y="5" width="80" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="19e83243-0f9c-41f9-97d4-a441c24fe938"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[Lagerstände:]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="Einheit" x="150" y="5" width="60" height="10" uuid="b5e644dd-9a5b-4a78-8430-f5f9356be8d2"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{Einheit}.trim()]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="15" width="270" height="1" uuid="6fda5259-2a74-40f9-a6b8-886e1e1e22bf"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<textField isBlankWhenNull="false">
				<reportElement key="Bestellmengeneinheit" x="210" y="5" width="60" height="10" uuid="45f6353a-7f06-4919-a2cc-e32a6f162ea7">
					<printWhenExpression><![CDATA[$P{Umrechnungsfaktor}!=null]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{Bestellmengeneinheit}.trim()]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="Lagerart" mode="Transparent" x="80" y="5" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="c28ab811-606e-4a43-90b1-cf1ae85f251d"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[Lagerart]]></text>
			</staticText>
			<staticText>
				<reportElement key="Konsi" mode="Transparent" x="140" y="5" width="25" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="b6db48d7-471d-4c32-82fa-585ed8383f53"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[Konsi]]></text>
			</staticText>
		</band>
	</title>
	<detail>
		<band height="11" splitType="Stretch">
			<textField isBlankWhenNull="false">
				<reportElement key="F_LAGER" x="0" y="0" width="80" height="10" uuid="3cf030cb-68ef-4244-97d9-d9afd1be6cb4"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{F_LAGER}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.0##" isBlankWhenNull="true">
				<reportElement key="F_LAGERSTAND" mode="Transparent" x="150" y="0" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="9d521807-2dd6-4e10-ab63-afcfb9bd7d04">
					<printWhenExpression><![CDATA[$F{F_LAGERSTAND}.compareTo(BigDecimal.ZERO) != 0]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{F_LAGERSTAND}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.0##" isBlankWhenNull="true">
				<reportElement key="F_LAGERSTAND" x="210" y="0" width="60" height="10" uuid="e17616db-588f-4664-92ab-aa6e000f2c1e">
					<printWhenExpression><![CDATA[$P{Umrechnungsfaktor}!=null &&
$F{F_LAGERSTAND}.compareTo(BigDecimal.ZERO) != 0]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{F_LAGERSTAND}.doubleValue() *
$P{Umrechnungsfaktor}.doubleValue()]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="Lagerart" x="80" y="0" width="60" height="10" uuid="c58d7525-bdbd-4d43-a6e8-0d6af1a8ee23"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{F_LAGERART}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="Konsilager" x="140" y="0" width="10" height="10" uuid="022dc574-8563-45b5-858d-9d7cb838bbf8"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Konsilager}==null ? "??":
$V{Konsilager}.intValue() == 1 ? "K":""]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<summary>
		<band height="41" splitType="Stretch">
			<line>
				<reportElement x="0" y="0" width="270" height="1" uuid="0a55fa69-fde2-49f7-80c3-01875bc6e490"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<textField pattern="#,##0.0##" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="150" y="1" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="8697661a-2ad8-40eb-b020-93a48195761b"/>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Hauptlager}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.0##" isBlankWhenNull="true">
				<reportElement positionType="Float" mode="Transparent" x="150" y="11" width="60" height="10" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF" uuid="882b8408-4818-4f11-9144-ad9af4995da4">
					<printWhenExpression><![CDATA[$V{Normallager} != null && $V{Normallager}.compareTo(BigDecimal.ZERO) != 0]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Normallager}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="Hauptlager" x="0" y="1" width="80" height="10" uuid="07deecec-7c79-49e7-9793-9c846cc2f89d"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Hauptlager"]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.0##" isBlankWhenNull="true">
				<reportElement positionType="Float" mode="Transparent" x="150" y="21" width="60" height="10" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF" uuid="363d9d52-0be9-4e27-be86-64530a58cd9a">
					<printWhenExpression><![CDATA[$V{Sperrlager} != null &&
$V{Sperrlager}.compareTo(BigDecimal.ZERO) != 0]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Sperrlager}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.0##" isBlankWhenNull="true">
				<reportElement positionType="Float" mode="Transparent" x="150" y="31" width="60" height="10" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF" uuid="335b70db-991c-423f-a5c0-55e8c4329dce">
					<printWhenExpression><![CDATA[$V{Kundenlager} != null &&
$V{Kundenlager}.compareTo(BigDecimal.ZERO) != 0]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Kundenlager}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="Normallager" positionType="Float" x="0" y="11" width="80" height="10" isRemoveLineWhenBlank="true" uuid="ec1f1d7b-4f62-4b99-92c4-7bc8505049c8">
					<printWhenExpression><![CDATA[$V{Normallager} != null &&
$V{Normallager}.compareTo(BigDecimal.ZERO) != 0]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Normallager"]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="Sperr und Schrott Lager" positionType="Float" x="0" y="21" width="80" height="10" isRemoveLineWhenBlank="true" uuid="95864193-87f5-46f2-bab8-bffc76aecc94">
					<printWhenExpression><![CDATA[$V{Sperrlager} != null &&
$V{Sperrlager}.compareTo(BigDecimal.ZERO) != 0]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Sperr & Schrottlager"]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="Kundenlager" positionType="Float" x="0" y="31" width="80" height="10" isRemoveLineWhenBlank="true" uuid="91b961e0-1b55-40a1-b6ae-38d9ddfed5e4">
					<printWhenExpression><![CDATA[$V{Kundenlager} != null &&
$V{Kundenlager}.compareTo(BigDecimal.ZERO) != 0]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Kundenlager"]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.0##" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="210" y="1" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="83085a0f-19aa-467d-a51a-30da464a83ed"/>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Hauptlager_ok}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.0##" isBlankWhenNull="true">
				<reportElement positionType="Float" mode="Transparent" x="210" y="31" width="60" height="10" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF" uuid="0b0fd491-2361-4227-a543-165a680537e7">
					<printWhenExpression><![CDATA[$V{Kundenlager} != null &&
$V{Kundenlager}.compareTo(BigDecimal.ZERO) != 0]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Kundenlager_ok}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.0##" isBlankWhenNull="true">
				<reportElement positionType="Float" mode="Transparent" x="210" y="11" width="60" height="10" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF" uuid="c32531e3-4934-40d1-acc2-2232f7a13cf1">
					<printWhenExpression><![CDATA[$V{Normallager} != null && $V{Normallager}.compareTo(BigDecimal.ZERO) != 0]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Normallager_ok}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.0##" isBlankWhenNull="true">
				<reportElement positionType="Float" mode="Transparent" x="210" y="21" width="60" height="10" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF" uuid="974a0cba-faf2-4782-88e6-46cc75b0423d">
					<printWhenExpression><![CDATA[$V{Sperrlager} != null &&
$V{Sperrlager}.compareTo(BigDecimal.ZERO) != 0]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Sperrlager_ok}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
