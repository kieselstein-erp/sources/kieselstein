<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ww_kundeumsatz_target" pageWidth="802" pageHeight="802" columnWidth="802" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="246993fa-da3d-4641-af1d-8be654feeb25">
	<property name="ireport.zoom" value="2.4157650000000017"/>
	<property name="ireport.x" value="198"/>
	<property name="ireport.y" value="0"/>
	<parameter name="Tage_dazwischen" class="java.lang.Double"/>
	<parameter name="P_SQLEXEC" class="com.lp.server.util.ReportSqlExecutor" isForPrompting="false">
		<parameterDescription><![CDATA[Ausführung von SQL Befehlen über die ReportConnectionUrl]]></parameterDescription>
	</parameter>
	<parameter name="P_MANDANT_OBJ" class="com.lp.server.system.service.ReportMandantDto" isForPrompting="false"/>
	<field name="Sort" class="java.lang.String"/>
	<field name="Region" class="java.lang.String"/>
	<field name="UmsatzzielRegion" class="java.math.BigDecimal"/>
	<field name="Lkz" class="java.lang.String"/>
	<field name="LandName" class="java.lang.String"/>
	<field name="Umsatzziel" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="Umsatz" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<variable name="UmsatzLand" class="java.lang.Double" resetType="Group" resetGroup="Land" calculation="Sum">
		<variableExpression><![CDATA[$F{Umsatzziel}.doubleValue() >= 0 ?
$F{Umsatz}.doubleValue() :
0.00]]></variableExpression>
	</variable>
	<variable name="UmsatzRegion" class="java.lang.Double" resetType="Group" resetGroup="Region" calculation="Sum">
		<variableExpression><![CDATA[$F{Umsatzziel}.doubleValue() >= 0 ?
$F{Umsatz}.doubleValue() :
0.00]]></variableExpression>
	</variable>
	<variable name="UmsatzGesamt" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$F{Umsatzziel}.doubleValue() >= 0 ?
$F{Umsatz}.doubleValue() :
0.00]]></variableExpression>
	</variable>
	<variable name="TargetLand" class="java.lang.Double" resetType="Group" resetGroup="Land" calculation="Sum">
		<variableExpression><![CDATA[$F{Umsatzziel}.doubleValue() > 0 ?
$F{Umsatzziel} :
0.00]]></variableExpression>
	</variable>
	<variable name="TargetRegionKunde" class="java.lang.Double" resetType="Group" resetGroup="Region" incrementType="Report" calculation="Sum">
		<variableExpression><![CDATA[/* Das ist das Ziel auf Basis der Kunden-Umsatzziele */
$F{Umsatzziel}.doubleValue() > 0 ?
$F{Umsatzziel} :
0.00]]></variableExpression>
	</variable>
	<variable name="TargetGesamtKunde" class="java.lang.Double" incrementType="Group" incrementGroup="Region" calculation="Sum">
		<variableExpression><![CDATA[$F{Umsatzziel}.doubleValue() > 0 ?
$F{Umsatzziel} :
0.00]]></variableExpression>
	</variable>
	<variable name="TargetGesamt" class="java.lang.Double" incrementType="Group" incrementGroup="Region" calculation="Sum">
		<variableExpression><![CDATA[/* Das ist die Summe der Ziele je Regionen */
$F{UmsatzzielRegion}.doubleValue() > 0 ?
$F{UmsatzzielRegion} :
0.00]]></variableExpression>
	</variable>
	<variable name="TageFaktor" class="java.lang.Double">
		<variableExpression><![CDATA[/* Multiplikator für das Jahrestarget um auf ein Monat usw. zu kommen */
$P{Tage_dazwischen}.doubleValue() / 365.0]]></variableExpression>
	</variable>
	<variable name="FaktorLand" class="java.lang.Double">
		<variableExpression><![CDATA[/* Erreichnung des Umsatzziels als Faktor */
$V{UmsatzLand} == null || $V{TargetLand} == null ||
$V{TargetLand}.doubleValue() <= 0 ? 0.00 :
($V{UmsatzLand}.doubleValue() <= 0 ? 0.00 :
$V{UmsatzLand}.doubleValue() / ($V{TargetLand}.doubleValue() * $V{TageFaktor}.doubleValue()) * 100)]]></variableExpression>
	</variable>
	<group name="Region">
		<groupExpression><![CDATA[$F{Region}]]></groupExpression>
		<groupFooter>
			<band height="25">
				<staticText>
					<reportElement x="27" y="1" width="31" height="10" uuid="bcb756dc-ae03-484c-aa2e-a5ed66c51f4b"/>
					<textElement markup="none">
						<font fontName="Arial" size="8"/>
					</textElement>
					<text><![CDATA[Summe]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="345" y="1" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="c704271a-f83c-4254-b178-ed57e24022c6"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
						<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{UmsatzRegion}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="60" y="1" width="160" height="10" uuid="4bf733e8-2463-4840-8523-ff1aea8539b8"/>
					<textElement>
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Region}==null?"nicht definiert":$F{Region}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="220" y="1" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="d134eb95-b514-4e77-afe0-c9bda40fe50b"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
						<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{TargetRegionKunde}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement x="0" y="0" width="590" height="1" uuid="135fb958-b140-4928-accb-e9af7a36d924"/>
					<graphicElement>
						<pen lineWidth="0.5"/>
					</graphicElement>
				</line>
				<textField isStretchWithOverflow="true" pattern="#,##0.0 %" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="405" y="1" width="40" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="f8e86304-322d-405c-ad73-4f380f7b6c49"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
						<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{UmsatzRegion}.doubleValue() / ($F{UmsatzzielRegion}.doubleValue() * $V{TageFaktor}.doubleValue() )]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="280" y="1" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="0138360b-3860-409c-a688-22c97843b3e5"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
						<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{UmsatzzielRegion}.doubleValue() * $V{TageFaktor}.doubleValue()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="345" y="11" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="3db86e90-2003-4d92-9e3c-4345e2adeeb6"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
						<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{UmsatzzielRegion}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="220" y="11" width="120" height="10" uuid="1aae6f8a-9821-470a-82ac-a82c87c9685c"/>
					<textElement textAlignment="Right" markup="none">
						<font fontName="Arial" size="8"/>
					</textElement>
					<text><![CDATA[Umsatzziel der Region]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<group name="Land">
		<groupExpression><![CDATA[$F{Lkz}]]></groupExpression>
		<groupFooter>
			<band height="10">
				<textField isStretchWithOverflow="true" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="345" y="0" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="32cdb68c-ea7a-41d0-a2e2-9bbff80cc994"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
						<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{UmsatzLand}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="220" y="0" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="8fc0878a-ec4d-4cf1-a78a-575107fddc7d"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
						<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{TargetLand}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="108" y="0" width="31" height="10" uuid="19085556-05d1-4ea3-a594-47da4b227d83">
						<printWhenExpression><![CDATA[false]]></printWhenExpression>
					</reportElement>
					<textElement markup="none">
						<font fontName="Arial" size="8"/>
					</textElement>
					<text><![CDATA[Summe]]></text>
				</staticText>
				<textField>
					<reportElement x="140" y="0" width="20" height="10" uuid="429803db-aadf-4be5-83a0-19d1b172012e"/>
					<textElement>
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Lkz}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.0 %" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="405" y="0" width="40" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="670a6414-a2fc-44c1-81dd-b05ecfe8637f"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
						<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{UmsatzLand}.doubleValue() / ($V{TargetLand}.doubleValue() * $V{TageFaktor}.doubleValue())]]></textFieldExpression>
				</textField>
				<rectangle>
					<reportElement key="FL200" x="580" y="0" width="5" height="10" forecolor="#00FF00" backcolor="#00FF00" uuid="3d826f8e-d336-4be3-8255-c71e72fdf264">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 190.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL190" x="575" y="0" width="5" height="10" forecolor="#00FF00" backcolor="#00FF00" uuid="570fe51a-c591-4649-a278-9cfbd043b19d">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 180.0 &&
$V{FaktorLand}.doubleValue() < 190.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL180" x="570" y="0" width="5" height="10" forecolor="#00FF00" backcolor="#00FF00" uuid="95eec890-0b9b-46d8-bd58-85c8feb8c564">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 170.0 &&
$V{FaktorLand}.doubleValue() < 180.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL170" x="565" y="0" width="5" height="10" forecolor="#00FF00" backcolor="#00FF00" uuid="0b172622-1a46-4dbc-a74b-f7e6318c2945">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 160.0 &&
$V{FaktorLand}.doubleValue() < 170.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL160" x="560" y="0" width="5" height="10" forecolor="#00FF00" backcolor="#00FF00" uuid="bd41f859-bab0-4eb3-a7f7-2e0ecbf7764a">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 150.0 &&
$V{FaktorLand}.doubleValue() < 160.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL150" x="555" y="0" width="5" height="10" forecolor="#00FF00" backcolor="#00FF00" uuid="77d13bda-c7eb-4793-8d22-68ca89067eb6">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 140.0 &&
$V{FaktorLand}.doubleValue() < 150.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL140" x="550" y="0" width="5" height="10" forecolor="#00FF00" backcolor="#00FF00" uuid="acce329f-220a-4d7a-98ce-502bc32e2476">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 130.0 &&
$V{FaktorLand}.doubleValue() < 140.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL130" x="545" y="0" width="5" height="10" forecolor="#00FF00" backcolor="#00FF00" uuid="4121b982-1df4-4f87-93d6-723177afb160">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 120.0 &&
$V{FaktorLand}.doubleValue() < 130.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL120" x="540" y="0" width="5" height="10" forecolor="#FFA132" backcolor="#FFA132" uuid="064530c2-3b52-41c5-92f7-dcd1462042ab">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 110.0 &&
$V{FaktorLand}.doubleValue() < 120.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL110" x="535" y="0" width="5" height="10" forecolor="#FFA132" backcolor="#FFA132" uuid="d9fcca21-da7b-4545-8a35-87e32fa57698">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 100.0 &&
$V{FaktorLand}.doubleValue() < 110.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL100" x="530" y="0" width="5" height="10" forecolor="#FF6666" backcolor="#FF6666" uuid="4cdb33cc-6015-46d3-99e2-434a19b6e21a">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 90.0 &&
$V{FaktorLand}.doubleValue() < 100.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL090" x="525" y="0" width="5" height="10" forecolor="#FF0000" backcolor="#FF0000" uuid="5f38d1d3-265b-4821-a993-86bef9338ba7">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 80.0 &&
$V{FaktorLand}.doubleValue() < 90.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL080" x="520" y="0" width="5" height="10" forecolor="#FF0000" backcolor="#FF0000" uuid="7f4896ad-6ba9-411f-bc18-a10c742cc5ea">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 70.0 &&
$V{FaktorLand}.doubleValue() < 80.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL070" x="515" y="0" width="5" height="10" forecolor="#FF0000" backcolor="#FF0000" uuid="ee2ddddf-7777-41e7-8515-22cf7f1a6301">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 60.0 &&
$V{FaktorLand}.doubleValue() < 70.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL060" x="510" y="0" width="5" height="10" forecolor="#FF0000" backcolor="#FF0000" uuid="23d66bc7-41b9-46c6-af5c-7ead19a330fb">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 50.0 &&
$V{FaktorLand}.doubleValue() < 60.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL050" x="505" y="0" width="5" height="10" forecolor="#FF0000" backcolor="#FF0000" uuid="31ae0e55-ea2d-4345-adc4-a579cfbe19a6">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 40.0 &&
$V{FaktorLand}.doubleValue() < 50.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL040" x="500" y="0" width="5" height="10" forecolor="#FF0000" backcolor="#FF0000" uuid="fd96ba10-25e0-4561-ba3c-08d36b7c7887">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 30.0 &&
$V{FaktorLand}.doubleValue() < 40.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL030" x="495" y="0" width="5" height="10" forecolor="#FF0000" backcolor="#FF0000" uuid="ded8f181-aafa-4a1d-beda-adcea2fd1c3e">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 20.0 &&
$V{FaktorLand}.doubleValue() < 30.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL020" x="490" y="0" width="5" height="10" forecolor="#FF0000" backcolor="#FF0000" uuid="b031d7ba-4dba-4421-b46b-740585c080a8">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() >= 10.0 &&
$V{FaktorLand}.doubleValue() < 20.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<rectangle>
					<reportElement key="FL010" x="485" y="0" width="5" height="10" forecolor="#FF0000" backcolor="#FF0000" uuid="a15dcc55-b7b0-4bb9-9582-0c36b0ab036e">
						<printWhenExpression><![CDATA[$V{FaktorLand}.doubleValue() < 10.0]]></printWhenExpression>
					</reportElement>
				</rectangle>
				<textField isStretchWithOverflow="true" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
					<reportElement mode="Transparent" x="280" y="0" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="ee7d2abe-2ac0-4eb8-8107-0bcff8e5c32a"/>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
						<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{TargetLand}.doubleValue() * $V{TageFaktor}.doubleValue()]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="160" y="0" width="60" height="10" uuid="f948ea28-069f-4440-8d31-0214a3f9fe46"/>
					<textElement>
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{LandName}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<pageHeader>
		<band height="35">
			<staticText>
				<reportElement x="0" y="24" width="60" height="10" uuid="c73fd5db-96d0-445c-9bd1-8df3d6b33022">
					<printWhenExpression><![CDATA[false]]></printWhenExpression>
				</reportElement>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Sort]]></text>
			</staticText>
			<staticText>
				<reportElement x="60" y="24" width="80" height="10" uuid="036f565a-257e-42e5-b619-3d2d8d64744e"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Region]]></text>
			</staticText>
			<staticText>
				<reportElement x="140" y="24" width="20" height="10" uuid="ef82380f-48df-4366-9d1c-801a5f05c9a5"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[LKZ]]></text>
			</staticText>
			<staticText>
				<reportElement x="220" y="24" width="60" height="10" uuid="08ea7b21-bb14-4437-a744-fbc27e606b27"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Ziel / Jahr]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="34" width="590" height="1" uuid="40e4c0bd-775c-45f0-a382-5ef4f9b33a6f"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement x="345" y="24" width="60" height="10" uuid="e1092ec0-0b96-4c90-9ea3-62d95f815d36"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Umsatz]]></text>
			</staticText>
			<staticText>
				<reportElement x="405" y="24" width="40" height="10" uuid="224ccfb3-9a9c-4eda-93fb-fc41c5d0fd6c"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[% Ziel]]></text>
			</staticText>
			<staticText>
				<reportElement key="PZ100%" mode="Transparent" x="538" y="27" width="25" height="8" forecolor="#000000" backcolor="#FFFFFF" uuid="d2bec584-1490-49d4-9b01-2c4d9d28c33b"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="6" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[100%]]></text>
			</staticText>
			<textField>
				<reportElement key="Zielerreichnung" mode="Opaque" x="485" y="17" width="100" height="10" forecolor="#000000" backcolor="#EEEEEE" uuid="e7fc6f16-5ea7-4df1-9962-ff9aa771b71f"/>
				<box>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA["Zielerreichnung"]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="535" y="27" width="1" height="7" uuid="eb26eb90-4cb3-4db0-9493-fa45db0e7069"/>
			</line>
			<textField pattern="#,##0" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="445" y="27" width="30" height="8" forecolor="#000000" backcolor="#FFFFFF" uuid="b9da5f23-1843-4cab-856c-059b065e2fc8"/>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="6" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{Tage_dazwischen}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="477" y="27" width="20" height="8" uuid="e9f95682-db66-42cd-bd9a-7e4dba40e103"/>
				<textElement>
					<font fontName="Arial" size="6"/>
				</textElement>
				<text><![CDATA[Tage]]></text>
			</staticText>
			<staticText>
				<reportElement x="280" y="24" width="60" height="10" uuid="03fed905-1653-4b1e-b99b-f2846504c9cd"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Ziel]]></text>
			</staticText>
			<staticText>
				<reportElement x="160" y="24" width="60" height="10" uuid="17342de6-8901-47d6-9055-ec0d8aed9e81"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Land]]></text>
			</staticText>
			<staticText>
				<reportElement x="325" y="16" width="15" height="8" uuid="e183a3b9-fe50-40cb-8deb-4f4380d16f1b"/>
				<textElement>
					<font fontName="Arial" size="6"/>
				</textElement>
				<text><![CDATA[Tage]]></text>
			</staticText>
			<textField pattern="#,##0" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="290" y="16" width="30" height="8" forecolor="#000000" backcolor="#FFFFFF" uuid="14eddc7f-e0b6-44a2-824f-e84fa29b32a8"/>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="6" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{Tage_dazwischen}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="0" width="171" height="20" uuid="f8731b95-95a1-4413-af30-e971466806e2">
					<printWhenExpression><![CDATA[false]]></printWhenExpression>
				</reportElement>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Info:
Umsatz ist der echte erreichte Umsatz
Target ist das Umsatzziel je Kunde und somit ist die Summe der Targets der geplante Umsatz pro Land
TargetRegion ist die Umsatzziele je Kunde in der Region
TargetGesamt ist das Umsatzziel aller Kunden

Zusätzlich haben wir die Umsatzziele je Region
Und auch diese Gesamt je Region]]></text>
			</staticText>
		</band>
	</pageHeader>
	<detail>
		<band height="10" splitType="Stretch">
			<printWhenExpression><![CDATA[false]]></printWhenExpression>
			<textField>
				<reportElement x="0" y="0" width="60" height="10" uuid="82ffbde9-7804-4abb-a586-786b1860b463"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Sort}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="60" y="0" width="80" height="10" uuid="82067c73-c52f-49e5-bdea-5cf648e26ccb"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Region}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="140" y="0" width="20" height="10" uuid="8d07abc3-3d88-41a0-ac5c-e360f7dd2bab"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Lkz}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="345" y="0" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="5380dab8-99b3-4499-a8a7-4de339962483"/>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Umsatz}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;-#,##0.00">
				<reportElement x="220" y="0" width="60" height="10" uuid="4485b084-b675-4d50-bcfc-5abb29d7283a"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Umsatzziel}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;-#,##0.00">
				<reportElement x="280" y="0" width="60" height="10" uuid="2c4c73bb-ff32-4a69-a934-396ea6d679c6"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Umsatzziel}.doubleValue() * $V{TageFaktor}.doubleValue()]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="160" y="0" width="60" height="10" uuid="746afa2e-664f-475e-b2aa-67866736e839"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{LandName}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<summary>
		<band height="31">
			<line>
				<reportElement x="0" y="0" width="590" height="1" uuid="d1a084ff-8d05-49ee-a139-34f645814455"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement x="0" y="1" width="31" height="10" uuid="1886306d-1345-4c69-8db2-5ce63e3c3fc1"/>
				<textElement markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Gesamt]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="345" y="1" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="a1899faa-44e2-4612-96bf-cfce8f64dc67"/>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{UmsatzGesamt}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="220" y="1" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="c3ed4459-357f-4af7-af07-377828fd7244"/>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{TargetGesamt}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.0 %" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="405" y="1" width="40" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="45c80bfb-3763-43b3-a69f-2931f2766c37"/>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{UmsatzGesamt}.doubleValue() / ($V{TargetGesamt}.doubleValue() * $V{TageFaktor}.doubleValue() )]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="280" y="1" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="5da940be-4298-4d82-b5c4-044fe4502849"/>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{TargetGesamt}.doubleValue() * $V{TageFaktor}.doubleValue()]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="11" width="190" height="10" uuid="498d4b51-3d3c-44fa-96f9-a3122fa74784"/>
				<textElement markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Target < 0 wird auch im Umsatz nicht berücksichtigt.]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="345" y="11" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="824f4756-95de-4aa1-a463-80c8d0d80ea2"/>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{TargetGesamt}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="190" y="11" width="150" height="10" uuid="e720f5d4-d2df-4210-a624-c39fd98440ba"/>
				<textElement textAlignment="Right" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Umsatzziel aller aufgelisteten Regionen]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="405" y="11" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="7f952f60-28f6-49b4-ba3a-8dd5c97a7447"/>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{TargetGesamt}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="220" y="21" width="120" height="10" uuid="5c78fdb4-f964-4dc8-b556-3814e1635ede"/>
				<textElement textAlignment="Right" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Umsatzziel aller Regionen]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="345" y="21" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="1b1dc3b1-75e7-42be-8a48-7571c06e39b6"/>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{P_SQLEXEC}.execute(
"select sum(n_umsatzziel) from lp_region;"
)]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
