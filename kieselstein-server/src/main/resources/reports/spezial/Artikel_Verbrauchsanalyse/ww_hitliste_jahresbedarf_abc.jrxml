<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ww_hitliste_jahresbedarf_abc" pageWidth="798" pageHeight="545" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="798" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="b5ce934b-ea56-433f-989d-ac4e4686a041">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="V_Summe_EKWert" class="java.math.BigDecimal"/>
	<parameter name="REPORT_INFORMATION" class="java.lang.String" isForPrompting="false">
		<parameterDescription><![CDATA[Informationen zum Report]]></parameterDescription>
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="P_VON" class="java.sql.Timestamp" isForPrompting="false">
		<parameterDescription><![CDATA[Von]]></parameterDescription>
	</parameter>
	<parameter name="P_BIS" class="java.sql.Timestamp" isForPrompting="false">
		<parameterDescription><![CDATA[Bis]]></parameterDescription>
	</parameter>
	<parameter name="Tage dazwischen" class="java.lang.Double"/>
	<field name="EK_Wert_String" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="Artikelnummer" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="Bezeichnung" class="java.lang.String"/>
	<field name="Verkauftemenge" class="java.math.BigDecimal"/>
	<field name="Durchschnittlichergestpreis" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="Artikelgruppe" class="java.lang.String"/>
	<field name="Artikelklasse" class="java.lang.String"/>
	<field name="Lagermindeststand" class="java.lang.Double"/>
	<field name="Lagerstand" class="java.math.BigDecimal"/>
	<field name="Bestellmenge" class="java.math.BigDecimal"/>
	<field name="Reserviertemenge" class="java.math.BigDecimal"/>
	<field name="Fehlmenge" class="java.math.BigDecimal"/>
	<variable name="EK_Wert_kumuliert" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{Verkauftemenge}.multiply($F{Durchschnittlichergestpreis})]]></variableExpression>
	</variable>
	<variable name="ABC_Prozent" class="java.lang.Double">
		<variableExpression><![CDATA[$V{EK_Wert_kumuliert}.doubleValue() /
$P{V_Summe_EKWert}.doubleValue()]]></variableExpression>
	</variable>
	<variable name="ABC" class="java.lang.String">
		<variableExpression><![CDATA[$V{ABC_Prozent}.doubleValue() < 0.8 ? "A" :
    $V{ABC_Prozent}.doubleValue() < 0.95 ? "B" : "C"]]></variableExpression>
	</variable>
	<variable name="ABC_Wert" class="java.math.BigDecimal" resetType="Group" resetGroup="ABC" calculation="Sum">
		<variableExpression><![CDATA[$F{Verkauftemenge}.multiply(
$F{Durchschnittlichergestpreis})]]></variableExpression>
	</variable>
	<variable name="Lagerreichweite" class="java.lang.Double">
		<variableExpression><![CDATA[$F{Lagerstand}.doubleValue() /
($F{Verkauftemenge}.doubleValue() / $P{Tage dazwischen}.doubleValue())]]></variableExpression>
	</variable>
	<variable name="Bestellreichweite" class="java.lang.Double">
		<variableExpression><![CDATA[$F{Bestellmenge}.doubleValue() /
($F{Verkauftemenge}.doubleValue() / $P{Tage dazwischen}.doubleValue())]]></variableExpression>
	</variable>
	<variable name="Res_FM" class="java.math.BigDecimal">
		<variableExpression><![CDATA[($F{Reserviertemenge}==null?BigDecimal.ZERO:$F{Reserviertemenge}).add(
 $F{Fehlmenge}==null?BigDecimal.ZERO:$F{Fehlmenge})]]></variableExpression>
	</variable>
	<group name="ABC">
		<groupExpression><![CDATA[$V{ABC}]]></groupExpression>
		<groupFooter>
			<band height="15">
				<line>
					<reportElement x="0" y="0" width="798" height="1" uuid="9e6cc3bb-dc62-4722-934a-70948478bd38"/>
					<graphicElement>
						<pen lineWidth="2.0" lineColor="#CCCCCC"/>
					</graphicElement>
				</line>
				<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement key="EinkaufswertDerABC_Gruppe" mode="Transparent" x="495" y="1" width="60" height="10" uuid="eb0e08b9-ece1-4e8c-9a93-2ac405bb20ba"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Bottom">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{ABC_Wert}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="#,##0.0%" isBlankWhenNull="false">
					<reportElement key="Pareto" mode="Transparent" x="555" y="1" width="30" height="10" uuid="f00f36fb-6e40-47d4-862a-e68aede88e72"/>
					<box leftPadding="2"/>
					<textElement verticalAlignment="Bottom">
						<font fontName="Arial" size="8"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{ABC_Wert}.doubleValue() /
$P{V_Summe_EKWert}.doubleValue()]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<pageHeader>
		<band height="41">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="REPORT_INFORMATION" mode="Opaque" x="0" y="0" width="624" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="31762090-78a3-4872-8e15-0b183c7f415e"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement verticalAlignment="Top" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{REPORT_INFORMATION}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="VON_PAGE_NUMBER" mode="Opaque" x="772" y="0" width="25" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="9ff34a39-2452-4825-97c6-a670e1ff987a"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[String.valueOf($V{PAGE_NUMBER})]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="PAGE_NUMBER" mode="Opaque" x="646" y="0" width="124" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="99633161-f485-481f-82d5-16a1b1901cc6"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA["Seite " + String.valueOf($V{PAGE_NUMBER}) + " von "]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="Verbrauchsanalyse" mode="Opaque" x="0" y="11" width="100" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="b2dd6a87-283a-49b7-a3fe-2fee36125d0a"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Bottom" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[Verbrauchsanalyse]]></text>
			</staticText>
			<staticText>
				<reportElement key="Von" mode="Opaque" x="100" y="11" width="23" height="10" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF" uuid="edaac0f7-2a05-44e5-b397-185f1dffddf0">
					<printWhenExpression><![CDATA[new Boolean($P{P_VON}!=null)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[Von]]></text>
			</staticText>
			<textField pattern="dd.MM.yyyy" isBlankWhenNull="false">
				<reportElement key="P_VON" mode="Opaque" x="124" y="11" width="87" height="10" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF" uuid="c9f27761-f388-4c42-9b80-42f240ff485b">
					<printWhenExpression><![CDATA[new Boolean($P{P_VON}!=null)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{P_VON}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="Bis" mode="Opaque" x="212" y="11" width="23" height="10" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF" uuid="46bd0b67-9c19-4d22-9919-2b3dbf9db3b1">
					<printWhenExpression><![CDATA[new Boolean($P{P_BIS}!=null)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[Bis]]></text>
			</staticText>
			<textField pattern="dd.MM.yyyy" isBlankWhenNull="false">
				<reportElement key="P_BIS" mode="Opaque" x="236" y="11" width="87" height="10" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF" uuid="11cc7264-68bd-4677-b961-fd77d768caa7">
					<printWhenExpression><![CDATA[new Boolean($P{P_BIS}!=null)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{P_BIS}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0" isBlankWhenNull="true">
				<reportElement key="tage dazwischen" mode="Opaque" x="326" y="11" width="87" height="10" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF" uuid="34c06d6d-af55-4320-a36c-fb999d0a3e5a"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{Tage dazwischen}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="Tage" mode="Opaque" x="415" y="11" width="23" height="10" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF" uuid="8c8deb35-b595-46c5-82d1-505597890c46">
					<printWhenExpression><![CDATA[$P{Tage dazwischen}.doubleValue() > 0]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[Tage]]></text>
			</staticText>
			<staticText>
				<reportElement key="Artikelnummer" mode="Transparent" x="0" y="30" width="60" height="10" uuid="fe0eea3a-2120-4067-9dd5-605a6b03cf7b"/>
				<textElement verticalAlignment="Bottom" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Artikelnummer]]></text>
			</staticText>
			<staticText>
				<reportElement key="Bezeichnung" mode="Transparent" x="60" y="30" width="205" height="10" uuid="6b95b9f4-578e-49ad-8253-dae26ac659d1"/>
				<textElement verticalAlignment="Bottom" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Bezeichnung]]></text>
			</staticText>
			<staticText>
				<reportElement key="Menge" mode="Transparent" x="250" y="30" width="80" height="10" uuid="4fb166ea-ed9d-4d03-a02a-c69df47917fc"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[verbrauchte Menge]]></text>
			</staticText>
			<staticText>
				<reportElement key="Gestpreis" mode="Transparent" x="330" y="30" width="40" height="10" uuid="fcc115e7-60a0-4e78-b1c1-c8d4407afda1"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Gestpreis]]></text>
			</staticText>
			<staticText>
				<reportElement key="Artikelgruppe" mode="Transparent" x="375" y="30" width="70" height="10" uuid="7c8036be-b1b5-4361-a60b-f1c6d3bc2326"/>
				<textElement verticalAlignment="Bottom" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Artikelgruppe]]></text>
			</staticText>
			<staticText>
				<reportElement key="Artikelklasse" mode="Transparent" x="445" y="30" width="50" height="10" uuid="f7f1a38e-ae48-45d2-98f0-1d6d950d6713"/>
				<textElement verticalAlignment="Bottom" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Artikelklasse]]></text>
			</staticText>
			<staticText>
				<reportElement key="EK_Wert" mode="Transparent" x="495" y="30" width="60" height="10" uuid="9686f67b-007c-4428-8026-31358dc786d6"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Verbrauchswert]]></text>
			</staticText>
			<staticText>
				<reportElement key="ABC" mode="Transparent" x="555" y="30" width="20" height="10" uuid="fef4d505-b504-496b-9249-2ed11e58caf0"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Bottom" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[ABC]]></text>
			</staticText>
			<staticText>
				<reportElement key="Lagerstand" mode="Transparent" x="580" y="30" width="45" height="10" uuid="05375cf5-43e4-4b18-aa98-e85c2482092c"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Lagerstand]]></text>
			</staticText>
			<staticText>
				<reportElement key="Bestellt" mode="Transparent" x="665" y="30" width="40" height="10" uuid="34c0a18f-2d80-4698-a1ac-1247d1ab2a51"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Bestellt]]></text>
			</staticText>
			<staticText>
				<reportElement key="Lagermin." mode="Transparent" x="705" y="30" width="40" height="10" uuid="7b7794af-26f5-4b4d-808e-d1220dc78828"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Lagermin.]]></text>
			</staticText>
			<staticText>
				<reportElement key="Reichweite in Monaten" mode="Transparent" x="745" y="11" width="53" height="19" uuid="803439ca-7dce-4222-b852-fa775b478b7f"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Reichweite in Monaten]]></text>
			</staticText>
			<staticText>
				<reportElement key="Lager Reichweite in Tagen" mode="Transparent" x="745" y="30" width="25" height="10" uuid="44f8ee78-344a-4ad2-b6fd-b463170efc8a"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Lager]]></text>
			</staticText>
			<staticText>
				<reportElement key="Bestell-Reichweite in Tagen" mode="Transparent" x="775" y="30" width="25" height="10" uuid="9cb7ba71-7a8b-4600-9adf-b5447fad3495"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Bestellt]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="40" width="798" height="1" uuid="2d270ac6-d531-4113-a8fc-c6f2c0c6ce0d"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="576" y="30" width="1" height="11" uuid="5bce41f2-5268-4810-9547-00ecc88a5089"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="745" y="11" width="1" height="30" uuid="1aa5cfd0-1025-424f-a891-a550fe16844a"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement key="Reserviert" mode="Transparent" x="625" y="20" width="40" height="20" uuid="43756bf0-d063-4e8e-9c9b-fd9d315f5116"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom" markup="none">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[Reserviert
Fehlmenge]]></text>
			</staticText>
		</band>
	</pageHeader>
	<detail>
		<band height="11" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="false">
				<reportElement key="Artikelnummer" mode="Transparent" x="0" y="0" width="60" height="10" uuid="744b4770-cefb-41fd-95b8-9c0bd97dbece"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Artikelnummer}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="Bezeichnung" mode="Transparent" x="60" y="0" width="205" height="10" uuid="f84c6b58-0800-4ce0-b8d0-c1283485df54"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Bezeichnung}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.0" isBlankWhenNull="true">
				<reportElement key="VerkaufteMenge" mode="Transparent" x="270" y="0" width="60" height="10" uuid="30985900-e814-480b-b115-2fc10ba8df33"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Verkauftemenge}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="Gestpreis" mode="Transparent" x="330" y="0" width="40" height="10" uuid="f42a4b72-79d3-4c0f-8c6d-67103af04ba2"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Durchschnittlichergestpreis}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="Artikelgruppe" mode="Transparent" x="375" y="0" width="70" height="10" uuid="00b10d0a-6080-4990-b728-f2af3a7e5f39"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Artikelgruppe}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="Artikelklasse" mode="Transparent" x="445" y="0" width="50" height="10" uuid="e63afa2e-59fa-4e0c-af56-dc9ef75c6eed"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Artikelklasse}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.0" isBlankWhenNull="true">
				<reportElement key="Lagerstand" mode="Transparent" x="580" y="0" width="45" height="10" uuid="eccb0609-9e66-4467-b935-4758a696b2da"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Lagerstand}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="Einkaufswert" mode="Transparent" x="495" y="0" width="60" height="10" uuid="47dc5f5d-7bc4-47ff-ad69-6058ebde50e6"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Verkauftemenge}.multiply(
$F{Durchschnittlichergestpreis})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="false">
				<reportElement key="ABC_Einteliung" mode="Transparent" x="555" y="0" width="20" height="10" uuid="d73bcf54-c3a9-4adf-a87c-6c49c832f812"/>
				<box leftPadding="2"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{ABC}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.0" isBlankWhenNull="true">
				<reportElement key="Lagermindeststand" mode="Transparent" x="705" y="0" width="40" height="10" uuid="f25657ee-1976-456c-a1f5-fe7646eb1da1"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Lagermindeststand}]]></textFieldExpression>
			</textField>
			<rectangle>
				<reportElement x="745" y="0" width="25" height="10" forecolor="#FFCC00" backcolor="#FFCC00" uuid="7a0f902c-aed7-4028-a71c-0d5018b1d2cf">
					<printWhenExpression><![CDATA[$V{Lagerreichweite}.doubleValue() / 30.5 > 6]]></printWhenExpression>
				</reportElement>
			</rectangle>
			<textField isStretchWithOverflow="true" pattern="#,##0.0" isBlankWhenNull="true">
				<reportElement key="Reichweite in Tagen" mode="Transparent" x="745" y="0" width="25" height="10" uuid="471b2043-1138-46fb-8d30-484b5a57a3ed">
					<printWhenExpression><![CDATA[$V{Lagerreichweite}.doubleValue() != 0]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Lagerreichweite}.doubleValue() / 30.5]]></textFieldExpression>
			</textField>
			<rectangle>
				<reportElement x="775" y="0" width="25" height="10" forecolor="#FFCC00" backcolor="#FFCC00" uuid="b6e6006d-3e89-4cc7-a747-0d14f90f0ee6">
					<printWhenExpression><![CDATA[$V{Bestellreichweite}.doubleValue() / 30.5 > 6]]></printWhenExpression>
				</reportElement>
			</rectangle>
			<textField isStretchWithOverflow="true" pattern="#,##0.0" isBlankWhenNull="true">
				<reportElement key="Bestellreichweite in Tagen" mode="Transparent" x="775" y="0" width="25" height="10" uuid="6b2a6706-3717-4bad-aa37-b035934c9924">
					<printWhenExpression><![CDATA[$V{Bestellreichweite}.doubleValue() != 0]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Bestellreichweite}.doubleValue() / 30.5]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.0" isBlankWhenNull="true">
				<reportElement key="Bestellmenge" mode="Transparent" x="665" y="0" width="40" height="10" uuid="3dc804ca-1e82-40cc-a77e-e0b17f8a8da7"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Bestellmenge}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement positionType="Float" x="0" y="10" width="798" height="1" uuid="8b6e0718-d708-4a75-9b29-9169e36edd5b"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="576" y="0" width="1" height="10" uuid="24791edc-c3af-4f54-8938-af89bdf89dc2"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<line>
				<reportElement stretchType="RelativeToBandHeight" x="745" y="0" width="1" height="10" uuid="48753ab1-9ce3-4e9c-b481-bd661a196b22"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<textField isStretchWithOverflow="true" pattern="#,##0.0" isBlankWhenNull="true">
				<reportElement key="Lagerstand" mode="Transparent" x="625" y="0" width="40" height="10" uuid="c3443a92-be10-4bb7-816b-600c1c7db56a">
					<printWhenExpression><![CDATA[$V{Res_FM}.compareTo(BigDecimal.ZERO) != 0]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Res_FM}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<summary>
		<band height="12">
			<line>
				<reportElement x="0" y="0" width="798" height="1" uuid="8157386c-4133-42a9-9eb3-c1e96bc79a02"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="495" y="1" width="60" height="10" forecolor="#000000" backcolor="#FFFFFF" uuid="da776f59-38e1-4951-8384-124ea506ea59"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom" rotation="None" markup="none">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{V_Summe_EKWert}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
