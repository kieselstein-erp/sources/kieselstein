Hier findest du eine Sammlung von zusätzlichen Auswertungen, sogenannten Spezialreports, die im Laufe der Jahre für die verschiedensten Anwendungen entstanden sind.

Die Eigenheit ist immer, dass diese als Reportvariante (System, Medien, Reportvariante) eingetragen werden müssen.

### Siehe
Diese findest unter 
- Artikel_Etiketten_mit_Bildern
- Artikel_Etiketten_mit_Eigenschaften
- Losetiketten_aus_Menge

### Spezielle FLR-Drucke
FLR Drucke, also der Druck der Auswahlliste ![](FLR_Druck.png)  wird gerne verwendet um für den Anwender rasch die passende Information darstellen zu können.<br>
Da diese FLR-Drucke auch immer von der Anzahl der übergebenen Spalten und den Datentypen abhängig sind, können diese nicht allgemein zur Verfügung gestellt werden.
| Modul | Verzeichnis | FLR_Druck | Zweck / Beschreibung |
| --- | --- | --- | --- |
| Projekt | Projekt_mit_Betreiber_Forecast_Verkaufsentwicklung | flrdruck12000 | Projekt mit Betreiber zur Darstellung der in der Auswahlliste enthaltenen Projekte mit Forecastbetrachtung und Verkaufstrichter |


### In Vorbereitung
Angebot_mit_Deckblatt
Artikelstatistik_mit_Lager
Artikelstatistik_mit_Verfuegbar
Artikel_Kunden_Laender_Auswertung
Artikel_Lagerstand_mit_MHD
Artikel_Pruefmittelliste
Auftrag_kommissionierung_mit_setartikel-Tren
Auftrag_Positionen_Materialzuschlaege
Aufträge_und_deren_offene_Bestellpositionen
Barcodes_F630
Bestellung_mit_von_Hilfsstücklisten
Chargeneigenschaften_andrucken
CostBreakDown
Etikette_mit_Kundenspezifischen_Logos_und_Te
Factoring_Raiffeisen
Fertigung
Fertigungsschein_mit_Messprotokoll
Fertigung_mit_Leistungsgrad
Fibu_Schnittstellen
Freigegebene_Lieferanten
GS1-128_Barcodes
Konjunktur_Statistik
Lieferantenstatistik_mit_Abmessungen
Lieferschein_als_Proforma
Lieferschein_mit_Geräteseriennummer
Lieferschein_packinglist
Losausgabe_mit_Abmessungen
Losausgabe_mit_Abmessungen_Verpackung
Losetiketten_mit_manuellen_SNR
Losnachkalkulation_auf_der_HTML_BDE
Los_Eti_GS1
LS_als_Herstellerzeugniss
LS_mit_Artikeleigenschaften
LS_mit_Lagerstand_Abbuchungslager
Maschinenzeitdatenumschaltscript
Maschinen_Barcodes
Monatsabrechnung_mit_Schicht_Zeitausweis
OPs_mit_Rechnungsstatus
Packliste_mit_Loszeiten
Reisezeiten_Anlagenbauer
Reisezeiten_Maschinenbauer
RE_Abrechnungsvorschlag_CRS
RE_Brutto
RE_Dreiecksgeschäft
RE_mit_QR_Code
RE_WA_Journal_mit_Detailstatistik
Stkl_Gesamtkalkulation_mit_Kundeneigenschaft
Stkl_Gesamtkalkulation_mit_Tafelberechnung
WEP_nach_VDA4992
WEP_Sortiert_nach_Buchungszeit
Zeiterfassung Gemeinden
Zeitjournal_mit_IF_am_Tag

### Weitere Spezial Reports, die aber über Reportvariante eingerichtet werden müssen
- Fibu, Kontoblatt ... Darstellung der Ust-Zahlungen passend zu AT-UVA Meldungszeitraum
- RE, Journal alle ... Berechnung / Belege für Tourismusabgabe
- Personalliste mit erlaubten Terminals, pers_personalliste_terminals.jrxml
