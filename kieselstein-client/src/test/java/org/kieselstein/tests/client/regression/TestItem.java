package org.kieselstein.tests.client.regression;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import static org.assertj.core.api.Assertions.assertThat;

import com.lp.client.frame.ExceptionLP;
import com.lp.server.artikel.service.ArtikelDto;
import com.lp.server.artikel.service.ArtikelFac;
import com.lp.server.artikel.service.ArtikelsprDto;
import com.lp.server.util.HvOptional;
import com.lp.util.Helper;


class TestItem{

    @Nested
    @TestMethodOrder(OrderAnnotation.class)
    @DisplayName("Tests for item creation")
    class TestItemCreation extends TestKs{

        private final String testSuiteBusinessKeyPrefix = "AR";

        public TestItemCreation() throws ExceptionLP {
			super();
		}

		@Test
        @Order(1)
        @DisplayName("Check for existing item")
        @Disabled("Disabled till Update-Process in Test-Access")
        void checkForExistingItem() throws Exception {
            String itemNumber = artikelFac.generiereNeueArtikelnummer("TEST1011010", theClientDto);
			ArtikelDto existingArtikelDto = artikelFac.artikelFindByCNrOhneExc(itemNumber, theClientDto);
			HvOptional<ArtikelDto> duplicate = HvOptional.ofNullable(existingArtikelDto);
			assertThat(duplicate.isPresent()).isFalse();
        }
        
        @Test
        @Order(2)
        @DisplayName("Create new item")
        @Disabled("Disabled till Update-Process in Test-Access")
        void createNewItem() throws Exception {
			ArtikelDto artikelDto = new ArtikelDto();
            String itemNumber = getNextBusinessKey(ArtikelDto.class, testSuiteBusinessKeyPrefix, "_");
	        artikelDto.setCNr(itemNumber);
	        artikelDto.setArtikelartCNr(ArtikelFac.ARTIKELART_ARTIKEL);
	        artikelDto.setBVersteckt(Helper.boolean2Short(false));
	        artikelDto.setEinheitCNr("Stk");
	        ArtikelsprDto oArtikelsprDto = new ArtikelsprDto();
	        oArtikelsprDto.setCBez("Test Artikel - " + itemNumber);
	        artikelDto.setArtikelsprDto(oArtikelsprDto);
	        Integer itemId = artikelFac.createArtikel(artikelDto, theClientDto);
			assertThat(itemId).isNotNull();
        }

    }
	


}
