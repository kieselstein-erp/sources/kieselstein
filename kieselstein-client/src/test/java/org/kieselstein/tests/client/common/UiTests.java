package org.kieselstein.tests.client.common;

import org.assertj.swing.core.KeyPressInfo;
import org.assertj.swing.fixture.JButtonFixture;

import java.awt.event.KeyEvent;

/**
 * This class contains utility methods for kieselstein UI tests.
 */
public final class UiTests {

    private UiTests() {
        throw new UnsupportedOperationException("Only static methods.");
    }

    /**
     * Focuses on the given button and presses the ENTER key. This seems to be more reliable than the click method.
     *
     * @param button the button on which to focus and send the enter key to.
     */
    public static void pressEnterOn(JButtonFixture button) {
        button.focus();
        button.pressAndReleaseKey(KeyPressInfo.keyCode(KeyEvent.VK_ENTER));
    }
}
