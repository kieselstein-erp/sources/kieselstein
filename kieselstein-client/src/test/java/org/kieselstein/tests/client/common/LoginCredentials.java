package org.kieselstein.tests.client.common;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class LoginCredentials {


    private final String user;
    private final String password;

    private LoginCredentials(String user, String password) {
        this.user = requireNonNull(user, "user must not be null");
        this.password = requireNonNull(password, "password must not be null");
    }

    public static final LoginCredentials user(String user) {
        return  new LoginCredentials(user, "");
    }

    public LoginCredentials password(String password) {
        return new LoginCredentials(user, password);
    }

    public String user() {
        return user;
    }

    public String password() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoginCredentials that = (LoginCredentials) o;
        return Objects.equals(user, that.user) && Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, password);
    }

    @Override
    public String toString() {
        return "LoginCredentials{" +
                "user='" + user + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
