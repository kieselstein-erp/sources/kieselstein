package org.kieselstein.tests.client.common;

import java.util.Arrays;
import java.util.NoSuchElementException;

import static java.util.Objects.requireNonNull;

/**
 * Represents kieselstein modules in the swing UI. This enum can be used within tests e.g. to open and close modules.
 */
public enum Module {
    /*
     * The first three are not really modules, but appear here for the moment,
     * as they have corresponding toolbar buttons ;-)
     */
    MODULE_LOGIN("module_login"),
    MODULE_BEENDEN("module_beenden"),
    MODULE_LOGOUT("module_logout"),
    MODULE_MANDANT("module_mandant"),
    ARTIKEL,
    STUECKLISTE,
    LIEFERANT,
    ANFRAGE,
    BESTELLUNG,
    EINGANGSRECHNUNG("Eingangsrechng"),
    FERTIGUNG("Los"),
    KUECHE,
    REKLAMATION,
    INSTANDHALTUNG,
    ZEITERFASSUNG,
    PERSONAL,
    KUNDE,
    PROJEKT,
    ANGEBOT,
    ANGEBOTSTUECKLISTE("AGStueckliste"),
    INSERAT,
    FORECAST,
    AUFTRAG,
    LIEFERSCHEIN,
    RECHNUNG,
    ZUTRITT,
    FINANZBUCHHALTUNG("Finanzbuchhaltg"),
    DASHBOARD,
    COCKPIT,
    PARTNER,
    BENUTZER,
    SYSTEM,
    EMAIL,
    NACHRICHTEN,
    GEODATENANZEIGE;

    private final String suffixOverride;

    Module() {
        this(null);
    }

    Module(String suffixOverride) {
        this.suffixOverride = suffixOverride;
    }

    /**
     * @return the name of the toolbar button within the AWT hierarchy, corresponding to the module.
     */
    public String toolbarButtonName() {
        return "toolbarButton_" + suffix();
    }

    public String internalFrameName() {
        return "internalFrame" + internalFrameSuffix();
    }

    private String internalFrameSuffix() {
        if (ANGEBOTSTUECKLISTE.equals(this)) {
            return "Angebotstkl";
        }
        if (FINANZBUCHHALTUNG.equals(this)) {
            return "Finanz";
        }
        if (GEODATENANZEIGE.equals(this)) {
            return "Maps";
        }
        return moduleName();
    }

    private String suffix() {
        if (suffixOverride != null) {
            return suffixOverride;
        }
        return moduleName();
    }

    private String moduleName() {
        String name = name();
        return name.substring(0, 1) + name.substring(1).toLowerCase();
    }

    /**
     * Retrieves the module corresponding to the given toolbar button name.
     *
     * @param toolbarButtonName the toolbar button name for which to get the module
     * @return the corresponding module
     * @throws NoSuchElementException in case the name cannot be mapped to any module
     */
    public static Module fromToolbarButtonName(String toolbarButtonName) {
        requireNonNull(toolbarButtonName, "toolbarButtonName must not be null.");
        return Arrays.stream(Module.values())
                .filter(m -> toolbarButtonName.equals(m.toolbarButtonName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Could not find module corresponding to toolbar button with name '" + toolbarButtonName + "'"));
    }
}
