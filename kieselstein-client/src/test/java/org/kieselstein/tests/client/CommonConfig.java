package org.kieselstein.tests.client;

public final class CommonConfig {

    private CommonConfig() {
        throw new UnsupportedOperationException("Only static methods.");
    }


    public static void setupDefaultProperties() {
        System.setProperty("java.naming.factory.initial", "org.wildfly.naming.client.WildFlyInitialContextFactory");
        System.setProperty("java.naming.provider.url", "remote+http://localhost:8080");
        System.setProperty("loc", "de_AT");
        //  System.setProperty("sun.java2d.dpiaware", "false");  // not sure yet if it is better to have this enabled or not.
    }
}
