package org.kieselstein.tests.client.regression;


import com.lp.client.frame.ExceptionLP;
import com.lp.client.frame.delegate.ArtikelDelegate;
import com.lp.client.frame.delegate.BenutzerDelegate;
import com.lp.client.frame.delegate.DelegateFactory;
import com.lp.client.frame.delegate.LogonDelegate;
import com.lp.server.artikel.service.ArtikelDto;
import com.lp.server.benutzer.service.LogonFac;
import com.lp.server.system.service.TheClientDto;
import com.lp.util.Helper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.net.UnknownHostException;
import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class FirstTestAccessingClientDelegate {

    @BeforeAll
    public static void setUp() {
        System.setProperty("java.naming.factory.initial", "org.wildfly.naming.client.WildFlyInitialContextFactory");
        System.setProperty("java.naming.provider.url", "remote+http://localhost:8080");
        System.setProperty("loc", "de_AT");
    }

    @Test
    @Disabled
    public void connection_refused_when_server_not_started() {
        assertThatThrownBy(() -> DelegateFactory.artikel())//
                .isInstanceOf(ExceptionLP.class);//
    }

    @Test
    @Disabled("still throws NullPointerException")
    public void artikel_try() throws Throwable {
        ArtikelDelegate artikel = DelegateFactory.artikel();
        Integer id = artikel.createArtikel(artikel());
        assertThat(id).isNotNull();

        ArtikelDto found = artikel.artikelFindByPrimaryKey(id);
        assertThat(found.getIId()).isEqualTo(id);


//                .hasMessageContaining("Failed to connect to remote host");
    }

    @Test
    @Disabled
    public void stammdaten_try() throws Throwable {
        BenutzerDelegate delegate = DelegateFactory.getInstance().getBenutzerDelegate();

        Assertions.assertThatThrownBy(() ->
                        delegate.benutzerFindByCBenutzerkennung("Admin", "admin"))// This somehow means "FEHLER In PASSWORD" - which is mysterious, as in the client this works this works ;-)
                .isInstanceOf(ExceptionLP.class).matches(e -> ((ExceptionLP) e).getICode() == 1);
    }

    @Test
//    @Disabled("Works on windows, but not in the pipeline!???")
    public void logon() throws Exception {
        LogonDelegate delegate = DelegateFactory.getInstance().getLogonDelegate();
        Locale locale = new Locale("de", "AT");

        TheClientDto theClient = delegate.logon(user("Admin"), "admin".toCharArray(), locale, null);
        assertThat(theClient).isNotNull();
    }

    @Test
    public void never_fails() throws Exception {
        Assertions.assertThat(true).isTrue();
    }

    private static String user(String userName) throws UnknownHostException {
        return userName + LogonFac.USERNAMEDELIMITER + Helper.getPCName() + LogonFac.USERNAMEDELIMITER;
    }

    private static ArtikelDto artikel() {
        ArtikelDto artikelDto = new ArtikelDto();
        artikelDto.setCNr("xyz");
        artikelDto.setArtikelartCNr("abc");
        artikelDto.setEinheitCNr("def");
        artikelDto.setBVersteckt((short) 0);
        return artikelDto;
    }
}
