package org.kieselstein.tests.client.regression;

import com.lp.client.frame.ExceptionLP;
import com.lp.client.frame.delegate.Delegate;
import com.lp.client.frame.delegate.DelegateFactory;
import com.lp.client.frame.delegate.LogonDelegate;
import com.lp.server.artikel.service.*;
import com.lp.server.benutzer.service.LogonFac;
import com.lp.server.finanz.service.FinanzFac;
import com.lp.server.finanz.service.FinanzamtDto;
import com.lp.server.partner.service.KundeDto;
import com.lp.server.partner.service.KundeFac;
import com.lp.server.partner.service.PartnerDto;
import com.lp.server.partner.service.PartnerFac;
import com.lp.server.personal.service.PersonalFac;
import com.lp.server.personal.service.ZeiterfassungFac;
import com.lp.server.rechnung.service.RechnungDto;
import com.lp.server.rechnung.service.RechnungFac;
import com.lp.server.rechnung.service.RechnungPositionDto;
import com.lp.server.system.pkgenerator.service.PKGeneratorFac;
import com.lp.server.system.service.*;
import com.lp.service.BelegpositionVerkaufDto;
import com.lp.util.Helper;
import liquibase.*;
import liquibase.changelog.ChangeLogParameters;
import liquibase.changelog.ChangeSetStatus;
import liquibase.changelog.DatabaseChangeLog;
import liquibase.command.CommandScope;
import liquibase.command.core.UpdateCommandStep;
import liquibase.command.core.helpers.DatabaseChangelogCommandStep;
import liquibase.command.core.helpers.DbUrlConnectionCommandStep;
import liquibase.command.core.helpers.ShowSummaryArgument;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CommandExecutionException;
import liquibase.resource.DirectoryResourceAccessor;
import org.apache.http.util.Asserts;
import org.junit.jupiter.api.*;
import org.kieselstein.tests.client.database.DumpImporter;

import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.File;
import java.math.BigDecimal;
import java.net.UnknownHostException;
import java.nio.file.FileSystems;
import java.rmi.RemoteException;
import java.sql.Date;
import java.sql.*;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;


public abstract class TestKs extends Delegate {
    private static final String LIQUIBASE_SOURCES = "/kieselstein-distpack/src/main/resources/bootstrap/liquibase";

    private static CommandExecutionException liquibaseCommandException = null;

    protected MandantFac mandantFac;
    protected ParameterFac parameterFac;
    protected SystemFac systemFac;
    protected ArtikelFac artikelFac;
    protected PartnerFac partnerFac;
    protected PersonalFac personalFac;
    protected ZeiterfassungFac zeiterfassungFac;
    protected LagerFac lagerfac;

    // Nummernkreis für i_kundennummer 8000-9000
    protected KundeFac kundeFac;

    protected VkPreisfindungFac vkPreisfindungFac;

    protected RechnungFac rechnungFac;

    protected FinanzFac finanzFac;
    protected PKGeneratorFac pkGeneratorFac;

    protected static Integer partnerId = -1;
    protected static Integer personalId = -1;

    protected Connection conn = null;
    protected TheClientDto theClientDto;



    public TestKs() throws ExceptionLP {
        System.setProperty("java.naming.factory.initial", "org.wildfly.naming.client.WildFlyInitialContextFactory");
        System.setProperty("java.naming.provider.url", "remote+http://localhost:8080");
        System.setProperty("loc", "de_AT");

        try {
            Context context = new InitialContext();

            mandantFac = lookupFac(context, MandantFac.class);
            parameterFac = lookupFac(context, ParameterFac.class);
            systemFac = lookupFac(context, SystemFac.class);
            artikelFac = lookupFac(context, ArtikelFac.class);
            partnerFac = lookupFac(context, PartnerFac.class);
            personalFac = lookupFac(context, PersonalFac.class);
            zeiterfassungFac = lookupFac(context, ZeiterfassungFac.class);
            lagerfac = lookupFac(context, LagerFac.class);
            kundeFac = lookupFac(context, KundeFac.class);
            vkPreisfindungFac = lookupFac(context, VkPreisfindungFac.class);
            rechnungFac = lookupFac(context, RechnungFac.class);
            finanzFac = lookupFac(context, FinanzFac.class);
            pkGeneratorFac = lookupFac(context, PKGeneratorFac.class);


        } catch (Throwable t) {
            handleThrowable(t);
        }
    }

    @BeforeAll
    static void beforeAll() {
        System.out.println("before db reimport");
        reimportBaseDatabase();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @BeforeEach
    void beforeEach() throws Exception {
        pkGeneratorFac.clearHashedEntries();
        LogonDelegate logonDelegate = DelegateFactory.getInstance().getLogonDelegate();
        theClientDto = logonDelegate.logon(getUser("Admin"), "admin".toCharArray(), new Locale("de", "AT"), "001");

        openDatabaseConnection();
    }

    @AfterEach
    void afterEach() throws Exception {
        DelegateFactory.getInstance().getLogonDelegate().logout(theClientDto);

        conn.close();
    }

    protected static String getUser(String userName) throws UnknownHostException {
        return userName + LogonFac.USERNAMEDELIMITER + Helper.getPCName() + LogonFac.USERNAMEDELIMITER;
    }

    private static Connection createConnection(boolean connectToKSEDB) throws SQLException {
        String pgHost = propertyOrEnv("MAIN_DB_HOST").orElse("localhost");
        String pgUser = propertyOrEnv("POSTGRES_USER").orElse("postgres");
        String pgPassword = propertyOrEnv("POSTGRES_PASSWORD").orElse("postgres");
        String dbName = propertyOrEnv("POSTGRES_DB").orElse("KIESELSTEIN");
        String url = "jdbc:postgresql://" + pgHost + ":5432/";

        if (connectToKSEDB) {
            url += dbName;
        }

        Properties props = new Properties();
        props.setProperty("user", pgUser);
        props.setProperty("password", pgPassword);
        props.setProperty("ssl", "false");
        props.setProperty("escapeSyntaxCallMode", "callIfNoReturn");
        return DriverManager.getConnection(url, props);
    }

    private void openDatabaseConnection() throws SQLException {
        conn = createConnection(true);
    }

    private static Optional<String> propertyOrEnv(String name){
        String val = System.getProperty(name);
        if (val != null){
            return Optional.of(val);
        }
        return Optional.ofNullable(System.getenv(name));
    }


    protected <T> String getNextBusinessKey(Class<T> c, String prefix, String delimiter) throws SQLException {

        createBusinessKeyProcedure();

        String classname = c.getName();

        CallableStatement callableStatement = conn.prepareCall("{call public.test_get_next_businesskey(?, '')}");
        callableStatement.setString(1, classname);
        callableStatement.registerOutParameter(1, Types.VARCHAR);
        callableStatement.execute();
        String result = callableStatement.getString(1);
        callableStatement.close();

        String objectShortCut = c.getSimpleName().substring(0, Math.min(c.getSimpleName().length(), 3)).toUpperCase() + delimiter;

        return (prefix == null ? "" : (prefix + delimiter)) + objectShortCut + result;

    }

    private void createBusinessKeyProcedure() throws SQLException {

        String sql = "CREATE OR REPLACE PROCEDURE public.test_Get_Next_BusinessKey(\n" +
                "  IN delegateIn VARCHAR(256),\n" +
                "  OUT bkOut VARCHAR(256)\n" +
                ") LANGUAGE plpgsql AS $$\n" +
                "\n" +
                "DECLARE\n" +
                "  latest_bk_ INTEGER;\n" +
                "  bk INTEGER;\n" +
                "BEGIN\n" +
                "\n" +
                "  CREATE SCHEMA IF NOT EXISTS test;\n" +
                "\n" +
                "  CREATE TABLE IF NOT EXISTS test.delegate_bk (\n" +
                "    delegate VARCHAR(256),\n" +
                "    latest_bk INTEGER\n" +
                "  );\n" +
                "\n" +
                "  SELECT\n" +
                "    delegate_bk.latest_bk\n" +
                "  FROM\n" +
                "    test.delegate_bk\n" +
                "  WHERE\n" +
                "    delegate_bk.delegate = delegateIn\n" +
                "  INTO\n" +
                "    latest_bk_;\n" +
                "\n" +
                "  IF latest_bk_ IS NULL THEN\n" +
                "    INSERT INTO\n" +
                "      test.delegate_bk (delegate, latest_bk)\n" +
                "    VALUES\n" +
                "      (delegateIn, 1);\n" +
                "    bk = 1;\n" +
                "  ELSE\n" +
                "    bk = latest_bk_ + 1;\n" +
                "    UPDATE\n" +
                "      test.delegate_bk\n" +
                "    SET\n" +
                "      latest_bk = bk\n" +
                "    WHERE\n" +
                "      delegate_bk.delegate = delegateIn;\n" +
                "  END IF;\n" +
                "\n" +
                "  bkout = bk;\n" +
                "\n" +
                "  COMMIT;\n" +
                "END; $$\n";

        Statement stmt = conn.createStatement();
        stmt.execute(sql);
        stmt.close();

    }

    private static void reimportBaseDatabase() {
        String pgUser = propertyOrEnv("POSTGRES_USER").orElse("postgres");
        String dbName = propertyOrEnv("POSTGRES_DB").orElse("KIESELSTEIN");

        try(Connection conn = createConnection(false); Statement stmt = conn.createStatement();) {
            String dropDB = "DROP DATABASE \"" + dbName + "\" with (FORCE);";
            stmt.executeUpdate(dropDB);
            String createDB = "CREATE DATABASE \"" + dbName + "\" with owner " +  pgUser + ";";
            stmt.executeUpdate(createDB);
            System.out.println("Database dropped successfully!" + " " +  new Timestamp(System.currentTimeMillis()));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try(Connection conn = createConnection(true)) {
            // TODO set correct working directory once this method is implemented in TestKS
            DumpImporter importer = new DumpImporter(conn, getInitDBFilename());
            importer.handleFile(false, false);
            System.out.println("Base dump imported successfully!" +  " "+ new Timestamp(System.currentTimeMillis()));
            updateDatabaseWithLiquibase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getInitDBFilename() {
        return "base_0.0.13.sql";
    }

    public void changeParameterValue(String mandantCnr, String cKategorie, String mandantParameterCNr, String newValue) throws RemoteException {
        ParametermandantDto parametermandantDto = parameterFac.getMandantparameter(mandantCnr, cKategorie, mandantParameterCNr);
        parametermandantDto.setCWert(newValue);
        parametermandantDto.setTAendern(new Timestamp(System.currentTimeMillis()));
        parametermandantDto.setPersonalIIdAendern(mandantFac.mandantFindByPrimaryKey(mandantCnr, theClientDto).getPartnerDto().getPersonalIIdAendern());
        parameterFac.updateParametermandant(parametermandantDto, theClientDto);
        assertThat(parameterFac.getMandantparameter(mandantCnr, cKategorie, mandantParameterCNr).getCWert()).isEqualTo(newValue);

    }

    public ArtikelDto createArtikel(String cNr, String artikelArtCnr, String einheitCnrBestellung, int mwstSatzBezIId) throws RemoteException {
        ArtikelDto artikelDto = new ArtikelDto();
        artikelDto.setCNr(cNr);
        artikelDto.setArtikelartCNr(artikelArtCnr);
        artikelDto.setEinheitCNr(einheitCnrBestellung);
        artikelDto.setMwstsatzbezIId(mwstSatzBezIId);
        artikelDto.setBVersteckt(Helper.boolean2Short(false));
        artikelDto.setIId(artikelFac.createArtikel(artikelDto, theClientDto));

        ArtikelDto artikelDtoFromDB = artikelFac.artikelFindByPrimaryKey(artikelDto.getIId(), theClientDto);

        assertThat(artikelDtoFromDB).isNotNull();
        assertThat(artikelDtoFromDB.getCNr()).isEqualTo(artikelDto.getCNr());

        return artikelDtoFromDB;
    }

    public void createManualBooking(LagerDto lagerDto,
                                    BigDecimal nEinstandsPreis,
                                    String cKommentar,
                                    int nArtikelID,
                                    BigDecimal nMenge,
                                    boolean bAendereLagerplatz,
                                    Short bAbgang,
                                    BigDecimal nVerkaufspreis
                                    ) throws RemoteException {

        HandlagerbewegungDto handlagerbewegungDto = new HandlagerbewegungDto();
        // Get Values from tests
        handlagerbewegungDto.setNEinstandspreis(nEinstandsPreis);
        handlagerbewegungDto.setCKommentar(cKommentar);
        handlagerbewegungDto.setArtikelIId(nArtikelID);
        handlagerbewegungDto.setNMenge(nMenge);
        handlagerbewegungDto.setLagerIId(lagerDto.getIId());
        handlagerbewegungDto.setBAendereLagerplatz(bAendereLagerplatz);
        handlagerbewegungDto.setBAbgang(bAbgang);
        handlagerbewegungDto.setNVerkaufspreis(nVerkaufspreis);
        handlagerbewegungDto.setIId(lagerfac.createHandlagerbewegung(handlagerbewegungDto, theClientDto));

        assertThat(lagerfac.handlagerbewegungFindByPrimaryKey(handlagerbewegungDto.getIId(), theClientDto)).isNotNull();
    }

    public KundeDto createCustomer(PartnerDto partnerDto,
                               MandantDto mandantDto,
                               int nKundenNummer,
                               int nMwstSteuerint,
                               BigDecimal nKreditlimit,
                               Short sAkzeptierTeillieferung,
                               Short sRechnungsdruckMitRabatt,
                               boolean bDistibutor,
                               boolean bIstreempfaenger,
                               boolean bIstinteressent,
                               boolean bMonatsrechnung,
                               boolean bPreiseanlsandrucken,
                               boolean bBSammelrechnung
                               ) throws RemoteException {

        KundeDto kundeDto = new KundeDto();

        kundeDto.setBDistributor(Helper.boolean2Short(bDistibutor));
        kundeDto.setBIstreempfaenger(Helper.boolean2Short(bIstreempfaenger));
        kundeDto.setbIstinteressent(Helper.boolean2Short(bIstinteressent));
        kundeDto.setBMonatsrechnung(Helper.boolean2Short(bMonatsrechnung));
        kundeDto.setBPreiseanlsandrucken(Helper.boolean2Short(bPreiseanlsandrucken));
        kundeDto.setBSammelrechnung(Helper.boolean2Short(bBSammelrechnung));
        kundeDto.setMwstsatzbezIId(nMwstSteuerint);
        kundeDto.setNKreditlimit(nKreditlimit);
        kundeDto.setBAkzeptiertteillieferung(sAkzeptierTeillieferung);
        kundeDto.setBRechnungsdruckmitrabatt(sRechnungsdruckMitRabatt);
        kundeDto.setMandantCNr(mandantDto.getCNr());
        kundeDto.setLieferartIId(mandantDto.getLieferartIIdKunde());
        kundeDto.setSpediteurIId(mandantDto.getSpediteurIIdKunde());
        kundeDto.setZahlungszielIId(mandantDto.getZahlungszielIIdKunde());
        kundeDto.setVerrechnungsmodellIId(mandantDto.getVerrechnungsmodellIId());
        kundeDto.setCWaehrung(mandantDto.getWaehrungCNr());
        kundeDto.setPersonaliIdProvisionsempfaenger(theClientDto.getIDPersonal());
        kundeDto.setKostenstelleIId(mandantDto.getIIdKostenstelle());
        kundeDto.setVkpfArtikelpreislisteIIdStdpreisliste(mandantDto.getVkpfArtikelpreislisteIId());

        kundeDto.setIKundennummer(nKundenNummer);
        kundeDto.setPartnerDto(partnerDto);
        kundeDto.setIId(kundeFac.createKunde(kundeDto, theClientDto));

        KundeDto kundeDtoFromDB = (kundeFac.kundeFindByPrimaryKey(kundeDto.getIId(),theClientDto));

        assertThat(kundeDtoFromDB).isNotNull();
        assertThat(kundeDtoFromDB.getIKundennummer()).isEqualTo(nKundenNummer);

        return kundeDtoFromDB;

    }

    public PartnerDto createPartner(String cKBez, String cPartnerArtCnr, String localeCnrKommunikation, boolean bVersteckt, String cName1, String cEori, LandplzortDto landplzortDto) {

        PartnerDto partnerDto = new PartnerDto();
        partnerDto.setBVersteckt(Helper.boolean2Short(bVersteckt));
        partnerDto.setLocaleCNrKommunikation(localeCnrKommunikation);
        partnerDto.setPartnerartCNr(cPartnerArtCnr);

        partnerDto.setCKbez(cKBez);
        partnerDto.setCName1nachnamefirmazeile1(cName1);
        partnerDto.setCEori(cEori);
        partnerDto.setLandplzortDto(landplzortDto);
        partnerDto.setLandplzortIId(landplzortDto.getIId());

        return partnerDto;

    }

    public FinanzamtDto createFinanzAmt(PartnerDto partnerDto, MandantDto mandantDto, boolean bUmsatzRunden) throws RemoteException {
        FinanzamtDto finanzamtDto = new FinanzamtDto();
        finanzamtDto.setPartnerDto(partnerDto);
        finanzamtDto.setPartnerIId(partnerDto.getIId());
        finanzamtDto.setMandantCNr(mandantDto.getCNr());
        finanzamtDto.setBUmsatzRunden(Helper.boolean2Short(bUmsatzRunden));

        FinanzamtDto createdFA = finanzFac.createFinanzamt(finanzamtDto, theClientDto);

        FinanzamtDto finanzamtDtoFromDb = finanzFac.finanzamtFindByPrimaryKey(createdFA.getPartnerIId(),  "001", theClientDto);

        assertThat(createdFA.getPartnerDto().getCKbez()).isEqualTo(finanzamtDtoFromDb.getPartnerDto().getCKbez());

        return finanzamtDtoFromDb;

    }

    public int createSellingPriceOfArticle(String mandantCnr, int nArtikelID, BigDecimal nVerkaufPreisBasis) throws RemoteException {
        VkPreisfindungEinzelverkaufspreisDto vkPreisfindungEinzelverkaufspreisDto = new VkPreisfindungEinzelverkaufspreisDto();
        vkPreisfindungEinzelverkaufspreisDto.setMandantCNr(mandantCnr);
        vkPreisfindungEinzelverkaufspreisDto.setArtikelIId(nArtikelID);
        vkPreisfindungEinzelverkaufspreisDto.setNVerkaufspreisbasis(nVerkaufPreisBasis);
        vkPreisfindungEinzelverkaufspreisDto.setTVerkaufspreisbasisgueltigab(new Date(System.currentTimeMillis()));
        return vkPreisfindungFac.createVkPreisfindungEinzelverkaufspreis(vkPreisfindungEinzelverkaufspreisDto, theClientDto);

    }

    public RechnungDto createInvoice(String cMandantCnr, String cNr, String cBestellNummer, Timestamp tBelegdatum, String cRechnungArtCnr, BigDecimal nKurs, MandantDto mandantDto, KundeDto kundeDto, LagerDto lagerDto) throws RemoteException {
        RechnungDto rechnungDto = new RechnungDto();

        rechnungDto.setMandantCNr(cMandantCnr);
        rechnungDto.setCNr(cNr);
        rechnungDto.setCBestellnummer(cBestellNummer);
        rechnungDto.setTBelegdatum(tBelegdatum);
        rechnungDto.setRechnungartCNr(cRechnungArtCnr);
        rechnungDto.setNKurs(nKurs);

        rechnungDto.setKostenstelleIId(mandantDto.getIIdKostenstelle());
        rechnungDto.setLagerIId(lagerDto.getIId());

        rechnungDto.setKundeIId(kundeDto.getIId());
        rechnungDto.setZahlungszielIId(kundeDto.getZahlungszielIId());
        rechnungDto.setLieferartIId(kundeDto.getLieferartIId());
        rechnungDto.setSpediteurIId(kundeDto.getSpediteurIId());
        rechnungDto.setFAllgemeinerRabattsatz(kundeDto.getFRabattsatz());
        rechnungDto.setKundeIIdStatistikadresse(kundeDto.getIId());
        rechnungDto.setWaehrungCNr(kundeDto.getCWaehrung());
        rechnungDto.setReversechargeartId(kundeDto.getReversechargeartId());

        RechnungDto savedDto = rechnungFac.createRechnung(rechnungDto, theClientDto);

        RechnungDto rechnungDtoFromDB = rechnungFac.rechnungFindByPrimaryKey(savedDto.getIId());
        assertThat(rechnungDtoFromDB).isNotNull();
        assertThat(rechnungDtoFromDB.getCNr()).isEqualTo(savedDto.getCNr());

        return rechnungDtoFromDB;

    }

    public RechnungPositionDto createInvoiceItem(RechnungDto rechnungDto,
                                                 ArtikelDto artikelDto,
                                                 KundeDto kundeDto,
                                                 VkPreisfindungEinzelverkaufspreisDto vkPreisfindungEinzelverkaufspreisDto,
                                                 LagerDto lagerDto,
                                                 String sPositionsArt,
                                                 BigDecimal nMenge,
                                                 String sEinheitCnr,
                                                 boolean bNettoPreisUebersteuert,
                                                 boolean bDrucken
    ) throws RemoteException {

        RechnungPositionDto rechnungPositionDto = new RechnungPositionDto();

        rechnungPositionDto.setBelegIId(rechnungDto.getIId());
        rechnungPositionDto.setArtikelIId(artikelDto.getIId());

        rechnungPositionDto.setPositionsartCNr(sPositionsArt);
        rechnungPositionDto.setNMenge(nMenge);
        rechnungPositionDto.setEinheitCNr(sEinheitCnr);
        rechnungPositionDto.setBNettopreisuebersteuert(Helper.boolean2Short(bNettoPreisUebersteuert));
        rechnungPositionDto.setBDrucken(Helper.boolean2Short(bDrucken));

        MwstsatzDto mwstAktuell = mandantFac.determineMWSTSatz(artikelDto,kundeDto, new Timestamp(System.currentTimeMillis()), theClientDto);


        rechnungPositionDto.setMwstsatzIId(mwstAktuell.getIId());

        // cast to BelegpositionVerkaufDto
        BelegpositionVerkaufDto positionDtoVK = rechnungPositionDto;
        positionDtoVK.setKostentraegerIId(null);
        positionDtoVK.setFRabattsatz(new Double("0.00"));
        positionDtoVK.setFZusatzrabattsatz(new Double("0.00"));
        positionDtoVK.setMwstsatzIId(mwstAktuell.getIId());
        positionDtoVK.setNEinzelpreis(vkPreisfindungEinzelverkaufspreisDto.getNVerkaufspreisbasis());
        positionDtoVK.setNNettoeinzelpreis(vkPreisfindungEinzelverkaufspreisDto.getNVerkaufspreisbasis());

        BigDecimal bruttoPreis = vkPreisfindungEinzelverkaufspreisDto.getNVerkaufspreisbasis().multiply(
                new BigDecimal(1).add(new BigDecimal(mwstAktuell.getFMwstsatz()).movePointLeft(2))
        );

        positionDtoVK.setNBruttoeinzelpreis(bruttoPreis);
        positionDtoVK.setPositioniId(rechnungPositionDto.getPositioniId());
        positionDtoVK.setArtikelIId(artikelDto.getIId());

        RechnungPositionDto savedDto = rechnungFac.createRechnungPosition(rechnungPositionDto, lagerDto.getIId(), theClientDto);

        RechnungPositionDto rechnungPositionDtoFromDB = rechnungFac.rechnungPositionFindByPrimaryKey(savedDto.getIId());

        assertThat(rechnungPositionDtoFromDB).isNotNull();
        assertThat(rechnungPositionDtoFromDB.getRechnungIId()).isEqualTo(savedDto.getRechnungIId());

        return rechnungPositionDtoFromDB;

    }


    public MwstsatzbezDto createVATDescription(String cBezeichnung, String cMandantCnr, int nFinanzAmtID) throws RemoteException {

        MwstsatzbezDto mwstsatzbezDto = new MwstsatzbezDto();
        mwstsatzbezDto.setCBezeichnung(cBezeichnung);
        mwstsatzbezDto.setMandantCNr(cMandantCnr);
        mwstsatzbezDto.setFinanzamtIId(nFinanzAmtID);

        return mwstsatzbezDto;
    }


    public MwstsatzDto createVAT(MwstsatzbezDto mwstsatzbezDto, Double dMwstSatz, Timestamp tgueltigAb) throws RemoteException {
        MwstsatzDto mwstsatzDto = new MwstsatzDto();
        mwstsatzDto.setMwstsatzbezDto(mwstsatzbezDto);
        mwstsatzDto.setFMwstsatz(dMwstSatz);
        mwstsatzDto.setDGueltigab(tgueltigAb);
        mwstsatzDto.setIId(mandantFac.createMwstsatz(mwstsatzDto, theClientDto));

        assertThat(mandantFac.mwstsatzFindByPrimaryKey(mwstsatzDto.getIId(), theClientDto)).isNotNull();

        return mwstsatzDto;
    }

    public void addAlternativeVAT(MwstsatzbezDto vATSameCountryAsFA, MwstsatzbezDto vATDifferentCountryAsFA) throws RemoteException {
        Set<MwstsatzbezDto> alternativeMWSTSetNormal = new HashSet<>();
        alternativeMWSTSetNormal.add(vATDifferentCountryAsFA);
        vATSameCountryAsFA.setAlternativeMwst(alternativeMWSTSetNormal);
        mandantFac.updateMwstsatzbez(vATSameCountryAsFA, theClientDto);
    }

    public static void updateDatabaseWithLiquibase() {
        // TODO comment logic
        try (Connection conn = createConnection(true)) {
            final Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(conn));
            database.setLiquibaseSchemaName("public");
            try {
                liquibaseCommandException = null;
                Map<String, Object> scopeObjects = new HashMap<>();
                scopeObjects.put(Scope.Attr.database.name(), database);
                System.out.println("Parent of Current Dir: " + FileSystems.getDefault().getPath(".").toAbsolutePath().getParent().getParent().toString());
                scopeObjects.put(Scope.Attr.resourceAccessor.name(), new DirectoryResourceAccessor(new File(FileSystems.getDefault().getPath(".").toAbsolutePath().getParent().getParent() + LIQUIBASE_SOURCES)));
                System.out.println("param " + GlobalConfiguration.VALIDATE_XML_CHANGELOG_FILES.getCurrentValue().toString());
                Scope.child(scopeObjects, (Scope.ScopedRunner) () -> new CommandScope(UpdateCommandStep.COMMAND_NAME)
                        .addArgumentValue(DbUrlConnectionCommandStep.DATABASE_ARG, database)
                        .addArgumentValue(UpdateCommandStep.CHANGELOG_FILE_ARG, "changelog.xml")
                        .addArgumentValue(DatabaseChangelogCommandStep.CHANGELOG_PARAMETERS, new ChangeLogParameters(database))
                        .addArgumentValue(ShowSummaryArgument.SHOW_SUMMARY, UpdateSummaryEnum.VERBOSE)
                        .execute());
                Scope.getCurrentScope().describe();
                System.out.println("After Update.");
            } catch (CommandExecutionException e) {
                System.out.println("Error running update: " + e.getMessage());
                liquibaseCommandException = e;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @Order(0)
    @DisplayName("Test Liquibase Update Status")
    void testLiquibaseStatus() throws Throwable {
        String liquibaseExceptionMessage = (liquibaseCommandException != null ? liquibaseCommandException.getMessage() : "");
        Asserts.check(liquibaseCommandException == null, "Command Exception while Liquibase Update: " + liquibaseExceptionMessage);

        try (Connection conn = createConnection(true)) {
            final Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(conn));
            database.setLiquibaseSchemaName("public");
            Liquibase l = new Liquibase(
                new DatabaseChangeLog("changelog.xml"),
                new DirectoryResourceAccessor(new File(FileSystems.getDefault().getPath(".").toAbsolutePath().getParent().getParent() + LIQUIBASE_SOURCES)),
                database
            );
            List<ChangeSetStatus> liquibaseStatus = l.getChangeSetStatuses(new Contexts(), new LabelExpression());
            for (ChangeSetStatus changeSetStatus : liquibaseStatus) {
                Assertions.assertTrue(
                    changeSetStatus.getPreviouslyRan(),
                    "Changelog: " + changeSetStatus.getChangeSet().getFilePath() + " didn't ran before!"
                );
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }
}
