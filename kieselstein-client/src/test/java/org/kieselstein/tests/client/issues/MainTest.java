package org.kieselstein.tests.client.issues;

import org.kieselstein.tests.client.database.DumpImporter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

public class MainTest {

    private static Optional<String> propertyOrEnv(String name){
        String val = System.getProperty(name);
        if (val != null){
            return Optional.of(val);
        }
        return Optional.ofNullable(System.getenv(name));
    }

    public static void main(String[] args){

        String pgHost = propertyOrEnv("MAIN_DB_HOST").orElse("localhost");
        String pgUser = propertyOrEnv("POSTGRES_USER").orElse("postgres");
        String pgPassword = propertyOrEnv("POSTGRES_PASSWORD").orElse("postgres");
        String dbName = propertyOrEnv("POSTGRES_DB").orElse("KIESELSTEIN");
        String url = "jdbc:postgresql://" + pgHost + ":5432/";

        try(Connection conn = DriverManager.getConnection(url, pgUser, pgPassword); Statement stmt = conn.createStatement();) {
            String dropDB = "DROP DATABASE \"" + dbName + "\" with (FORCE);";
            stmt.executeUpdate(dropDB);
            String createDB = "CREATE DATABASE \"" + dbName + "\" with owner " +  pgUser + ";";
            stmt.executeUpdate(createDB);
            System.out.println("Database dropped successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        url = url + dbName;

        try(Connection conn = DriverManager.getConnection(url, pgUser, pgPassword);) {
            // TODO set correct working directory once this method is implemented in TestKS
            String dumpFile = "base_0.0.13.sql";
            DumpImporter importer = new DumpImporter(conn, dumpFile);
            importer.handleFile(false, false);
            System.out.println("Base dump imported successfully!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
