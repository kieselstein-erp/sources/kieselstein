package org.kieselstein.tests.client.database;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.sql.*;
import java.time.LocalTime;
import java.util.*;

import static java.nio.file.Files.newInputStream;

public class DumpImporter {

    final Connection dbConnection;
    final String sqlDumpFileName;

    final String dbSchema = "public";

    HashMap<String, HashMap<String, String>> tableColumnTypes;

    final String[][] STRING_PLACE_HOLDERS = {
        { "\\\\n", "\n" },
        {"[\\\\]{2}", "\\\\" },
    };

    public DumpImporter(Connection connection, String fileName) {
        this.dbConnection = connection;
        this.sqlDumpFileName = fileName;
        this.tableColumnTypes = new HashMap<>();
        initDatabaseTypes(dbSchema);
    }

    private void initDatabaseTypes(String tableSchema) {
        try (Statement stmt = dbConnection.createStatement()) {
            final String query = "SELECT table_name, column_name, data_type, ordinal_position\n" +
                    "FROM information_schema.columns\n" +
                    "WHERE table_schema = ? \n" +
                    "order by table_schema, table_name , ordinal_position;";
            PreparedStatement ps = dbConnection.prepareStatement(query);
            ps.setString(1, tableSchema);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String table_name = rs.getString(1);
                String column_name = rs.getString(2);
                String data_type = rs.getString(3);
                if (!tableColumnTypes.containsKey(table_name)) {
                    tableColumnTypes.put(table_name, new HashMap<>());
                }
                tableColumnTypes.get(table_name).put(column_name, data_type);
            }
        } catch (SQLException e) {
            e.printStackTrace();  // This should never happen.
        }
    }

    private String getDatabaseType(String tableName, String columnName) {
        String tab = tableName;
        if (!tableColumnTypes.containsKey(tab) && tableName.contains(".")) {
            tab = tableName.split("\\.")[1];
            if (!tableColumnTypes.containsKey(tab)) {
                initDatabaseTypes(dbSchema);
            }
        }
        return tableColumnTypes.get(tab).get(columnName);
    }

    private Object parseValueForDatabase(String tableName, String columnName, String value) {
        if ("\\N".equals(value)) {
            return null;
        }
        String type = getDatabaseType(tableName, columnName);
        if ("smallint".equals(type) || "integer".equals(type)) {
            return Integer.parseInt(value);
        } else if ("boolean".equals(type)) {
            return "t".equalsIgnoreCase(value);
        } else if ("double precision".equals(type) || "numeric".equals(type)) {
            return Double.parseDouble(value);
        } else if ("timestamp without time zone".equals(type)) {
            return Timestamp.valueOf(value);
        } else if ("time without time zone".equals(type)) {
            return LocalTime.parse(value);
        } else if ("bytea".equals(type)) {
            String hex = value;
            if (hex.startsWith("\\\\x")) {
                hex = hex.substring(3);
            }
            // Convert the Hex-String into a byte array.
            int len = hex.length();
            byte[] data = new byte[len / 2];
            for (int i = 0; i < len; i += 2) {
                data[i / 2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4)
                        + Character.digit(hex.charAt(i+1), 16));
            }
            return data;
        }
        for (String[] repl: STRING_PLACE_HOLDERS) {
            value = value.replaceAll(repl[0], repl[1]);
        }

        if (value.contains("\\")) {  // time without time zone
            System.out.println("What is this: " + value);
        }

        return value;
    }

    private void handleCopyData(String tableName, String colString, List<String> dataRows) throws SQLException {
        if (dataRows != null && !dataRows.isEmpty()) {
            StringBuilder sqlInsert = new StringBuilder("INSERT INTO ").append(tableName).append(" ").append(colString).append(" VALUES (");
            String[] tableCols = colString.substring(1, colString.length() - 1).split(",");
            for (int i = 0; i < tableCols.length; i++) {
                if (i > 0) {
                    sqlInsert.append(",");
                }
                sqlInsert.append("?");
                tableCols[i] = tableCols[i].trim();
            }
            sqlInsert.append(");");
            PreparedStatement prepStatement = dbConnection.prepareStatement(sqlInsert.toString());
            try {
                for (String dataLine : dataRows) {
                    String[] tableData = dataLine.split("\t");
                    for (int i = 0; i < tableCols.length; i++) {
                        Object parsedValue = parseValueForDatabase(tableName, tableCols[i], tableData[i]);
                        prepStatement.setObject(i + 1, parsedValue);
                    }
                    prepStatement.addBatch();
                }
                prepStatement.executeBatch();
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    private void executeSQL(Statement statement, String sql, int count) {
        try {
            // Execute the SQL command
            statement.execute(sql);
            System.out.println(
                count
                + " Command successfully executed : "
                + sql.substring(0, Math.min(sql.length(), 55))
                + "..."
            );
        } catch (SQLException e) {
            System.err.println("Error at SQL \"" + sql + "\" : " + e.getMessage() + "\n");
        }
    }

    public void handleFile(boolean importGrantSelect, boolean importChangeOwner) {
        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream is = classloader.getResourceAsStream(sqlDumpFileName);
            InputStreamReader reader = new InputStreamReader(is, StandardCharsets.UTF_8);

            // Wrap the FileReader in a BufferedReader for efficient reading.
            BufferedReader bufferedReader = new BufferedReader(reader);
            // Create a statement object to execute SQL commands.
            Statement statement = dbConnection.createStatement();
            StringBuilder builder = new StringBuilder();

            String line;
            int count = 0;

            HashMap<String, List<String>> constraintsMap = new HashMap<>();

            // Read lines from the SQL file until the end of the file is reached.
            while ((line = bufferedReader.readLine()) != null) {
                if (builder.length() == 0 && line.startsWith("COPY")) {
                    String tableName = line.substring(5, line.indexOf("(")).trim();
                    String colString = line.substring(line.indexOf("("), line.indexOf(")") + 1);
                    List<String> copyData = new ArrayList<>();
                    while ((line = bufferedReader.readLine()) != null && !line.startsWith("\\.")) {
                        copyData.add(line);
                    }
                    handleCopyData(tableName, colString, copyData);
                    continue;
                } else {
                    line = line.trim();
                }

                // Skip empty lines and single-line comments.
                if (line.isEmpty() || line.startsWith("--"))
                    continue;
                else if (builder.length() == 0 && !importGrantSelect && line.startsWith("GRANT SELECT") && line.contains(";"))
                    continue;
                else if (builder.length() == 0 && !importChangeOwner && line.startsWith("ALTER") && line.contains(" OWNER TO ") && line.contains(";"))
                    continue;

                builder.append(line);
                String sql = builder.toString();
                // If the line ends with a semicolon, it
                // indicates the end of an SQL command.
                if ((line.endsWith(";") && !sql.startsWith("CREATE FUNCTION")) || (sql.startsWith("CREATE FUNCTION") && line.endsWith("$$;"))) {
                    count += 1;
                    executeSQL(statement, sql, count);
                    builder.setLength(0);
                } else {
                    builder.append(" ");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleFile() {
        final boolean importGrantSelect = true;
        final boolean importChangeOwner = true;
        handleFile(importGrantSelect, importChangeOwner);
    }
}
