package org.kieselstein.tests.client.regression;

import com.lp.client.frame.ExceptionLP;
import com.lp.server.partner.service.PartnerDto;
import com.lp.server.partner.service.PartnerFac;
import com.lp.server.personal.service.*;
import com.lp.server.system.service.MandantDto;
import com.lp.util.Helper;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;


class TestSuiteTimeRecord {

    @Nested
    @TestMethodOrder(OrderAnnotation.class)
    @DisplayName("Test time record creation")
    class CreateTimeRecord extends TestKs{

        private final String testSuiteBusinessKeyPrefix = "ZE";



        public CreateTimeRecord() throws ExceptionLP {
			super();
		}

        @Test
        @Order(1)
        @DisplayName("Create partner for employee")
        void createPartner() throws Exception {
            PartnerDto partnerDto = new PartnerDto();
            partnerDto.setPartnerartCNr(PartnerFac.PARTNERART_PERSON);
            partnerDto.setCName1nachnamefirmazeile1(getNextBusinessKey(PartnerDto.class, testSuiteBusinessKeyPrefix, "_"));
            partnerDto.setCKbez("MM");
            partnerDto.setBVersteckt(Helper.boolean2Short(false));
            partnerDto.setLocaleCNrKommunikation(theClientDto.getLocUiAsString());
            partnerId = partnerFac.createPartner(partnerDto, theClientDto);
            assertThat(partnerId).isNotNull();
            assertThat(partnerId).isGreaterThan(0);
        }


		@Test
        @Order(2)
        @DisplayName("Create employee for time-record")
        @Disabled("Disabled till Update-Process in Test-Access")
        void createEmployee() throws Exception {

            if (partnerId == null || partnerId < 0 ) {
                createPartner();
            }

            assertThat(partnerId).isNotNull();
            assertThat(partnerId).isGreaterThan(0);

            PersonalDto personalDto = new PersonalDto();
            personalDto.setPartnerIId(partnerId);
            personalDto.setCPersonalnr(getNextBusinessKey(PersonalDto.class, testSuiteBusinessKeyPrefix, ""));
            personalDto.setBVersteckt(Helper.boolean2Short(false));
            personalDto.setPersonalartCNr(PersonalFac.PERSONALART_ANGESTELLTER);

            MandantDto mandantDto = mandantFac.mandantFindByPrimaryKey(theClientDto.getMandant(), theClientDto);
            personalDto.setMandantCNr(theClientDto.getMandant());
            personalDto.setKostenstelleIIdStamm(mandantDto.getIIdKostenstelle());

            personalId = personalFac.createPersonal(personalDto, theClientDto);
            assertThat(personalId).isNotNull();
        }

        @Test
        @Order(3)
        @DisplayName("Create time-record start")
        @Disabled("Disabled till Update-Process in Test-Access")
        void createTimeRecordStart() throws Exception {

            ZeitdatenDto zeitdatenDto = new ZeitdatenDto();
            zeitdatenDto.setPersonalIId(personalId);
            zeitdatenDto.setTZeit(Timestamp.valueOf(LocalDateTime.now()));

            TaetigkeitDto taetigkeitDto = zeiterfassungFac.taetigkeitFindByCNr(ZeiterfassungFacAll.TAETIGKEIT_KOMMT, theClientDto);
            zeitdatenDto.setTaetigkeitIId(taetigkeitDto.getIId());

            zeiterfassungFac.createZeitdaten(zeitdatenDto, false, false, false, false, theClientDto);



        }
        


    }
	


}
