
package org.kieselstein.tests.client.database;

//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.lessThan;

import java.util.Arrays;
import java.util.List;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitForeignKeyNameSource;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;
import org.hibernate.boot.model.naming.ImplicitUniqueKeyNameSource;
import org.junit.jupiter.api.Assertions;


//import eu.yb.basedata_interface.YbConstants;
//import lombok.NonNull;
//import lombok.extern.slf4j.Slf4j;

public class KSNamingStrategyJpaCompliantImpl extends ImplicitNamingStrategyJpaCompliantImpl {

	private static final long serialVersionUID = 1L;

	private static final String DELIMITER3 = "_";
	private static final String DELIMITER2 = "_";
	private static final String DELIMITER1 = "_";

	@Override
	public Identifier determineUniqueKeyName(ImplicitUniqueKeyNameSource source) {

		Identifier userProvidedIdentifier = null; // source.getUserProvidedIdentifier();

		Identifier identifier = userProvidedIdentifier == null
				? toIdentifier(generateHashedConstraintName("uk", source.getTableName(), source.getColumnNames()),
						source.getBuildingContext())
				: userProvidedIdentifier;

		System.out.println("YB generated uk name='" + identifier + "'");

		return identifier;
	}

	@Override
	public Identifier determineForeignKeyName(ImplicitForeignKeyNameSource source) {

		Identifier userProvidedIdentifier = null; // source.getUserProvidedIdentifier();

//		source.getBuildingContext().getBuildingOptions().getSchemaCharset();

		// fk_as_ekweblieferant_einkaufsangebot_i_id_as_einkaufsangebot_i_
		// fk_as_ekweblieferant_webpartner_i_id_as_webpartner_i_id

		Identifier identifier = toIdentifier(generateHashedFkName("FK", source.getTableName(),
				source.getReferencedTableName(), source.getColumnNames()), source.getBuildingContext());

//Identifier identifier = userProvidedIdentifier == null //
//			? toIdentifier(
//				generateHashedFkName("FK_", source.getTableName(), source.getReferencedTableName(), source.getColumnNames()),
//				source.getBuildingContext())
//			: userProvidedIdentifier;

		System.out.println("Generated fk name='" + identifier + "'");

		Assertions.assertEquals(identifier.getText().length() < 64, true);

		return identifier;
	}

	private static String cutSchema(final Identifier tableName) {

		final int iE = tableName.getText().indexOf("_");
		return tableName.getText().substring(iE + 1);
	}

	private static String generateHashedConstraintName(final String prefix, final Identifier tableName,
			final List<Identifier> columnNames) {

		// final String tn = cutSchema(tableName);
		String name = prefix + DELIMITER1 + tableName;

		for (final Identifier columnName : columnNames)
			name += DELIMITER2 + columnName;

		return name; // checkLen(name);
	}

	private static String generateHashedFkName(final String prefix, final Identifier tableName,
			final Identifier referencedTableName, final Identifier... columnNames) {

		// Use a concatenation that guarantees uniqueness, even if identical names
		// exist between all table and column identifiers.

		final String rtn = cutSchema(referencedTableName);
		final StringBuilder sb = new StringBuilder().append(tableName).append(DELIMITER2).append(rtn);

		// Ensure a consistent ordering of columns, regardless of the order they were
		// bound.
		// Clone the list, as sometimes a set of order-dependent Column bindings are
		// given.

		final Identifier[] alphabeticalColumns = columnNames.clone();
		Arrays.sort(alphabeticalColumns, (o1, o2) -> o1.getCanonicalName().compareTo(o2.getCanonicalName()));

		for (final Identifier columnName : alphabeticalColumns)
			sb.append(DELIMITER3).append(columnName);

		return prefix + DELIMITER1 + sb.toString();
	}

	private static String generateHashedFkName(final String prefix, final Identifier tableName,
			final Identifier referencedTableName, final List<Identifier> columnNames) {

		final Identifier[] columnNamesArray;

		if (columnNames == null || columnNames.isEmpty())
			columnNamesArray = new Identifier[0];
		else
			columnNamesArray = columnNames.toArray(new Identifier[columnNames.size()]);

		return generateHashedFkName(prefix, tableName, referencedTableName, columnNamesArray);
	}

}
