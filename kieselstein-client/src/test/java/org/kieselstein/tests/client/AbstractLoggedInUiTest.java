package org.kieselstein.tests.client;

import com.lp.client.pc.Desktop;
import com.lp.client.pc.DialogLogin;
import com.lp.client.pc.LPMain;
import org.assertj.core.api.Assertions;
import org.assertj.swing.annotation.GUITest;
import org.assertj.swing.core.BasicRobot;
import org.assertj.swing.core.KeyPressInfo;
import org.assertj.swing.core.Robot;
import org.assertj.swing.edt.FailOnThreadViolationRepaintManager;
import org.assertj.swing.fixture.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.kieselstein.tests.client.jupiter.GUITestExtension;
import org.kieselstein.tests.client.common.LoginCredentials;
import org.kieselstein.tests.client.common.UiTests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static java.util.Objects.requireNonNull;
import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.swing.finder.WindowFinder.findDialog;
import static org.assertj.swing.finder.WindowFinder.findFrame;
import static org.assertj.swing.launcher.ApplicationLauncher.application;

import org.kieselstein.tests.client.common.Module;

/**
 * This class is intended to be inherited from by ui tests, which require a successful login. The default login is the
 * 'Admin' user. In case a different user is desired, the credentials can be provided by overriding the
 * {@link #credentials()} method.
 * <p>
 * Note: This test has the {@link TestInstance.Lifecycle} per class! Therefore, one single instance
 * of the class will be created once for all the test methods in the class. Thus, state *could* be kept in instance
 * fields (despite not recommended ;-)
 */
@GUITest
@ExtendWith(GUITestExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class AbstractLoggedInUiTest {

    private static Logger LOGGER = LoggerFactory.getLogger(AbstractLoggedInUiTest.class);

    /**
     * This executor will be used to open the UI. This is required, as otherwise the test will be blocked because the
     * UI waits forever on login.
     */
    private final Executor uiExecutor = Executors.newSingleThreadExecutor();
    private FrameFixture desktop;
    private Robot robot;

    @BeforeAll
    public void setUpBeforeAll() throws InterruptedException {
        CommonConfig.setupDefaultProperties();


        robot = BasicRobot.robotWithNewAwtHierarchy();

//        FailOnThreadViolationRepaintManager.install();

        uiExecutor.execute(() -> {
            application(LPMain.class).start();
        });

        desktop = findFrame(Desktop.class).withTimeout(30, SECONDS).using(robot);

        login();
    }

    /**
     * Retrieves all the available modules by collecting the toolbar buttons from the desktop frame.
     *
     * @return all the kieselstein modules which are available after login.
     */
    protected List<Module> availableModules() {
        List<Module> modules = robot.finder()
                .findAll(desktop.target(), c -> {
                    if (c == null || c.getName() == null) {
                        return false;
                    }
                    String name = c.getName();
                    return name.startsWith("toolbarButton_");
                })
                .stream()
                .map(c -> c.getName())
                .map(Module::fromToolbarButtonName)
                .collect(toList());
        LOGGER.info("The following modules are available: " + modules);
        return modules;
    }

    private void login() {
        DialogFixture loginDialog = findDialog(DialogLogin.class)
                .withTimeout(10, SECONDS)
                .using(robot);
        Assertions.assertThat(loginDialog).isNotNull();

        LoginCredentials credentials = credentials();

        JTextComponentFixture benutzer = loginDialog.textBox("wtfBenutzer");
        Assertions.assertThat(benutzer).isNotNull();
        benutzer.setText(credentials.user());

        JTextComponentFixture kennwort = loginDialog.textBox("wpwdKennwort");
        kennwort.setText(credentials.password());
        kennwort.pressAndReleaseKey(KeyPressInfo.keyCode(KeyEvent.VK_TAB));

        JButtonFixture btn = loginDialog.button("wbuAnmelden");
        UiTests.pressEnterOn(btn);
    }

    @AfterAll
    public void tearDownOnce() {
        robot.cleanUp();
        FailOnThreadViolationRepaintManager.uninstall();
    }

    /**
     * This method is used to determine the credentials to use for log into Kieselstein. The default is the admin user.
     * In case one wants to test with a different user, override this method.
     *
     * @return the login credentials to be used in the setup method to log into kieselstein UI.
     */
    protected LoginCredentials credentials() {
        return LoginCredentials.user("Admin").password("admin");
    }


    /**
     * To be used in subclasses to access the kieselstein desktop
     *
     * @return the desktop frame as fixture.
     */
    protected FrameFixture desktop() {
        return desktop;
    }

    /**
     * The robot to be used to traverse the component tree.
     *
     * @return the robot with the actual AWT hierarchy.
     */
    protected Robot robot() {
        return robot;
    }

    /**
     * Opens the given kieselstein module by the use of the corresponding toolbar button. In case the button is disabled,
     * it will throw...
     *
     * @param module the module whose internal frame shall be opened
     * @throws UnsupportedOperationException in case the button is disabled and thus the module cannot be opened.
     */
    protected void open(Module module) {
        requireNonNull(module, "module must not be null.");
        LOGGER.info("Opening module " + module + " using the toolbar button.");
        JButtonFixture button = desktop.button(module.toolbarButtonName());
        if (button.isEnabled()) {
            UiTests.pressEnterOn(button);
        } else {
            throw new UnsupportedOperationException("Button " + button + " is disabled. Cannot open module.");
        }
    }

    /**
     * Closes the internal frame for the given module
     *
     * @param module the kieselstein module for which to close the internal frame.
     */
    protected void close(Module module) {
        JInternalFrameFixture internalFrame = internalFrame(module);
        internalFrame.close();
    }

    /**
     * Retrieves the internal frame for the given module (as fixture)
     *
     * @param module the kieselstein module for which to retrieve the frame
     * @return the fixture for the internal frame, corresponding to the given module.
     */
    protected JInternalFrameFixture internalFrame(Module module) {
        JInternalFrameFixture internalFrame = desktop().internalFrame(module.internalFrameName());
        assertThat(internalFrame.target().isShowing())
                .as("Internal frame for module {} is not showing. Cannot close!", module)
                .isTrue();
        return internalFrame;
    }
}
