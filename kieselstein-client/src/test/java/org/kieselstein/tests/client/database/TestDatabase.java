
package org.kieselstein.tests.client.database;

import java.io.File;
import java.io.IOException;
import java.util.EnumSet;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.BootstrapServiceRegistry;
import org.hibernate.boot.registry.BootstrapServiceRegistryBuilder;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.google.common.io.Files;


//import eu.basedata.testng.YBPriority;
//import eu.basedata.util.DatabaseData;
//import eu.yb.basedata_interface.AcademicTitleEntity;
//import eu.yb.basedata_interface.LocaleEntity;
//import eu.yb.basedata_interface.ValueAddedTaxEntity;
//import eu.yb.basedata_interface.YbConstants;
//import lombok.extern.slf4j.Slf4j;

//@Slf4j
//@YBPriority(10000)

@Disabled
class TestDatabase {

	private static final String SCHEMA_FILENAME_HBM2DDL = "schema_ddl_by_hbm2ddl.sql";
	private static final String SCHEMA_FILENAME_HBM2DDL_BEFORE = "schema_ddl_by_hbm2ddl_before.sql";

	private static final String PATH_TEMP = "./target/temp";
	private static final String PATH_AND_FILENAME_DDL = PATH_TEMP + File.separatorChar + SCHEMA_FILENAME_HBM2DDL;

//	private DatabaseData databaseData;

//	@SuppressWarnings({ "unchecked", "rawtypes" })
//	private DatabaseData gatherDatabaseData() throws IOException, FileNotFoundException {
//
////		final DatabaseData databaseData = new DatabaseData();
//
//		try (InputStream inputStream = new FileInputStream(new File("src/main/resources/application.yml"))) {
//
//			final Yaml yaml = new Yaml();
//			final Map<String, Object> applicationYml = yaml.load(inputStream);
//
//			((LinkedHashMap<String, LinkedHashMap>) applicationYml.get("spring"))
//					.forEach((k, v) -> v.forEach((l, m) -> {
//						if ("username".equals(l))
//							databaseData.username = m.toString();
//						else if ("password".equals(l))
//							databaseData.passwd = m.toString();
//						else if ("url".equals(l)) {
//							databaseData.jdbcUrlYalablue = m.toString();
//							final String[] split = databaseData.jdbcUrlYalablue.split("/");
//							databaseData.databaseName = split[3];
//							databaseData.jdbcUrlTemplate1 = databaseData.jdbcUrlYalablue.replace("yalablue",
//									"template1");
//						}
//					}));
//		}
//		return databaseData;
//	}

//	@BeforeClass
//	public void beforeClass() throws FileNotFoundException, IOException {
//
//		databaseData = gatherDatabaseData();
//	}

//	@Test
//	void _0_drop_DatabaseIfExists_dropped() throws SQLException {
//
//		System.out.println("A TRACE Message");
//		System.out.println("A DEBUG Message");
//		System.out.println("An INFO Message");
//		System.out.println("A WARN Message");
//		System.out.println("An ERROR Message");
//
//		if (existYalablue()) {
//			System.out.println("Database {} found, will now drop...", "kieselstein");
//
//			executeDdlUseTemplate1(String.format("DROP DATABASE %s;", databaseData.databaseName));
//
//			System.out.println("Database dropped");
//		}
//	}

//	@Test
//	@YBPriority(10001)
//	public void _1_create_Database_created() throws SQLException {
//
//		executeDdlUseTemplate1(
//			String
//				.format(
//					"CREATE DATABASE %s WITH OWNER = %s ENCODING = 'UTF8' CONNECTION LIMIT = -1; COMMENT ON DATABASE %s IS 'yalablue development database'",
//					databaseData.databaseName,
//					databaseData.username,
//					databaseData.databaseName));
//
//		log.debug("Database {} created!", databaseData.databaseName);
//	}
//
//	@Test
//	@YBPriority(10002)
//	public void _2_create_Schema_created() throws SQLException {
//
//		executeYalablueDev(String.format("CREATE SCHEMA %s", YbConstants.DB_SCHEMA_NAME));
//	}

	@Test
	void _3_create_DdlTable_created() throws IOException {

		backupFile();

		BootstrapServiceRegistry bootstrapServiceRegistry = new BootstrapServiceRegistryBuilder().build();

		StandardServiceRegistryBuilder standardServiceRegistryBuilder = new StandardServiceRegistryBuilder(
				bootstrapServiceRegistry);

		StandardServiceRegistry standardServiceRegistry = standardServiceRegistryBuilder.configure().build();

		MetadataSources metadataSources = new MetadataSources(standardServiceRegistry);

		metadataSources.addAnnotatedClass(com.lp.server.anfrage.ejb.Anfrage.class);
		metadataSources.addAnnotatedClass(com.lp.server.anfrage.ejb.Anfrageart.class);
		metadataSources.addAnnotatedClass(com.lp.server.anfrage.ejb.Anfrageartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.anfrage.ejb.AnfrageartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.anfrage.ejb.Anfrageerledigungsgrund.class);
		metadataSources.addAnnotatedClass(com.lp.server.anfrage.ejb.Anfrageposition.class);
		metadataSources.addAnnotatedClass(com.lp.server.anfrage.ejb.Anfragepositionart.class);
		metadataSources.addAnnotatedClass(com.lp.server.anfrage.ejb.Anfragepositionlieferdaten.class);
		metadataSources.addAnnotatedClass(com.lp.server.anfrage.ejb.Anfragestatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.anfrage.ejb.Anfragetext.class);
		metadataSources.addAnnotatedClass(com.lp.server.anfrage.ejb.Zertifikatart.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebot.ejb.Akquisestatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebot.ejb.Angebot.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebot.ejb.Angebotart.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebot.ejb.Angebotartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebot.ejb.AngebotartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebot.ejb.Angebotauftrag.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebot.ejb.Angeboteinheit.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebot.ejb.Angeboterledigungsgrund.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebot.ejb.Angeboterledigungsgrundspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebot.ejb.AngeboterledigungsgrundsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebot.ejb.Angebotposition.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebot.ejb.Angebotpositionart.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebot.ejb.Angebotstatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebot.ejb.Angebottext.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Agstkl.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Agstklarbeitsplan.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Agstklaufschlag.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Agstklmaterial.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Agstklmengenstaffel.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.AgstklmengenstaffelSchnellerfassung.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Agstklposition.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Agstklpositionsart.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Aufschlag.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Einkaufsangebot.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Einkaufsangebotposition.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Ekaglieferant.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Ekgruppe.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Ekgruppelieferant.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Ekweblieferant.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Positionlieferant.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Weblieferant.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Alergen.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artgru.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artgrumandant.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artgruspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.ArtgrusprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikel.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikelalergen.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikelart.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikelartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.ArtikelartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikelbestellt.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikelfehlmenge.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikelkommentar.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikelkommentarart.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikelkommentarartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.ArtikelkommentarartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikelkommentardruck.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikelkommentarspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.ArtikelkommentarsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikellager.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.ArtikellagerPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikellagerplaetze.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikellieferant.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikellieferantstaffel.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikellog.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikelreservierung.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikelshopgruppe.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikelsnrchnr.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikelsperren.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artikelspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.ArtikelsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artkla.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Artklaspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.ArtklasprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Automotive.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Dateiverweis.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Einkaufsean.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Ersatztypen.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Farbcode.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Fasession.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Fasessioneintrag.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Gebinde.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Geometrie.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Geraetesnr.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Handlagerbewegung.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Hersteller.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Inventur.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Inventurliste.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Inventurprotokoll.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Inventurstand.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Katalog.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Lager.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Lagerabgangursprung.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.LagerabgangursprungPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Lagerart.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Lagerartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.LagerartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Lagerbewegung.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Lagerplatz.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Lagerumbuchung.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.LagerumbuchungPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Lagerzugangursprung.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.LagerzugangursprungPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Laseroberflaeche.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Material.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Materialpreis.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Materialspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.MaterialsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Materialzuschlag.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Medical.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Montage.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Paternoster.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Paternostereigenschaft.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Rahmenbedarfe.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Reach.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Rohs.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Shopgruppe.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Shopgruppespr.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.ShopgruppesprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Shopgruppewebshop.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Sollverkauf.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Sperren.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Trumphtopslog.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Verleih.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Verpackung.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Verpackungsmittel.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Verpackungsmittelspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Verschleissteil.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Verschleissteilwerkzeug.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.VkPreisfindungEinzelverkaufspreis.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.VkPreisfindungPreisliste.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Vkpfartikelpreisliste.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Vkpfmengenstaffel.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Vorschlagstext.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Vorzug.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Waffenausfuehrung.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Waffenkaliber.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Waffenkategorie.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Waffentyp.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.WaffentypFein.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Waffenzusatz.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Webshop.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Werkzeug.class);
		metadataSources.addAnnotatedClass(com.lp.server.artikel.ejb.Zugehoerige.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Auftrag.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Auftragart.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Auftragartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.AuftragartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Auftragauftragdokument.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Auftragbegruendung.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Auftragdokument.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Auftragkostenstelle.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Auftragposition.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Auftragpositionart.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Auftragpositionstatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Auftragseriennrn.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Auftragstatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Auftragteilnehmer.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Auftragtext.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Auftragwiederholungsintervall.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Auftragwiederholungsintervallspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.AuftragwiederholungsintervallsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.IndexanpassungLog.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Lieferstatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Meilenstein.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Meilensteinspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.MeilensteinsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Verrechenbar.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Zahlungsplan.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Zahlungsplanmeilenstein.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Zeitplan.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Zeitplantyp.class);
		metadataSources.addAnnotatedClass(com.lp.server.auftrag.ejb.Zeitplantypdetail.class);
		metadataSources.addAnnotatedClass(com.lp.server.benutzer.ejb.Artgrurolle.class);
		metadataSources.addAnnotatedClass(com.lp.server.benutzer.ejb.Benutzer.class);
		metadataSources.addAnnotatedClass(com.lp.server.benutzer.ejb.Benutzermandantsystemrolle.class);
		metadataSources.addAnnotatedClass(com.lp.server.benutzer.ejb.Fertigungsgrupperolle.class);
		metadataSources.addAnnotatedClass(com.lp.server.benutzer.ejb.Lagerrolle.class);
		metadataSources.addAnnotatedClass(com.lp.server.benutzer.ejb.Nachrichtarchiv.class);
		metadataSources.addAnnotatedClass(com.lp.server.benutzer.ejb.Nachrichtart.class);
		metadataSources.addAnnotatedClass(com.lp.server.benutzer.ejb.Recht.class);
		metadataSources.addAnnotatedClass(com.lp.server.benutzer.ejb.Rollerecht.class);
		metadataSources.addAnnotatedClass(com.lp.server.benutzer.ejb.Systemrolle.class);
		metadataSources.addAnnotatedClass(com.lp.server.benutzer.ejb.Thema.class);
		metadataSources.addAnnotatedClass(com.lp.server.benutzer.ejb.Themarolle.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.BSZahlungsplan.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.Bestellposition.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.Bestellpositionart.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.Bestellpositionstatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.Bestellung.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.Bestellungart.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.Bestellungartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.BestellungartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.Bestellungstatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.Bestellungtext.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.Bestellvorschlag.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.Bsmahnlauf.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.Bsmahnstufe.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.BsmahnstufePK.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.Bsmahntext.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.Bsmahnung.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.Mahngruppe.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.Wareneingang.class);
		metadataSources.addAnnotatedClass(com.lp.server.bestellung.ejb.Wareneingangsposition.class);
		metadataSources.addAnnotatedClass(com.lp.server.eingangsrechnung.ejb.Auftragszuordnungverrechnet.class);
		metadataSources.addAnnotatedClass(com.lp.server.eingangsrechnung.ejb.Eingangsrechnung.class);
		metadataSources.addAnnotatedClass(com.lp.server.eingangsrechnung.ejb.EingangsrechnungAuftragszuordnung.class);
		metadataSources.addAnnotatedClass(com.lp.server.eingangsrechnung.ejb.EingangsrechnungKontierung.class);
		metadataSources.addAnnotatedClass(com.lp.server.eingangsrechnung.ejb.Eingangsrechnungart.class);
		metadataSources.addAnnotatedClass(com.lp.server.eingangsrechnung.ejb.Eingangsrechnungartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.eingangsrechnung.ejb.EingangsrechnungartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.eingangsrechnung.ejb.Eingangsrechnungstatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.eingangsrechnung.ejb.Eingangsrechnungtext.class);
		metadataSources.addAnnotatedClass(com.lp.server.eingangsrechnung.ejb.Eingangsrechnungzahlung.class);
		metadataSources.addAnnotatedClass(com.lp.server.eingangsrechnung.ejb.Zahlungsvorschlag.class);
		metadataSources.addAnnotatedClass(com.lp.server.eingangsrechnung.ejb.Zahlungsvorschlaglauf.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Bedarfsuebernahme.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Erledigtermaterialwert.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Internebestellung.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Los.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Losablieferung.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Losbereich.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Losgutschlecht.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Losistmaterial.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Losklasse.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Losklassespr.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.LosklassesprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Loslagerentnahme.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Loslosklasse.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Lospruefplan.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Lossollarbeitsplan.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Lossollmaterial.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Losstatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Lostechniker.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Loszusatzstatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Pruefergebnis.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Wiederholendelose.class);
		metadataSources.addAnnotatedClass(com.lp.server.fertigung.ejb.Zusatzstatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Bankverbindung.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Belegbuchung.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Buchung.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Buchungdetail.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Buchungdetailart.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Buchungsart.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Buchungsartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.BuchungsartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Ergebnisgruppe.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Exportdaten.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Exportlauf.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Finanzamt.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.FinanzamtPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Kassenbuch.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Konto.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Kontoart.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Kontoartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.KontoartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Kontolaenderart.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.KontolaenderartPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Kontoland.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.KontolandPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Kontotyp.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Kontotypspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.KontotypsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Mahnlauf.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Mahnspesen.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Mahnstufe.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.MahnstufePK.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Mahntext.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Mahnung.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Rechenregel.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Steuerkategorie.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Steuerkategoriekonto.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.SteuerkategoriekontoPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Uvaart.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Uvaartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.UvaartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Uvaverprobung.class);
		metadataSources.addAnnotatedClass(com.lp.server.finanz.ejb.Warenverkehrsnummer.class);
		metadataSources.addAnnotatedClass(com.lp.server.forecast.ejb.Fclieferadresse.class);
		metadataSources.addAnnotatedClass(com.lp.server.forecast.ejb.Forecast.class);
		metadataSources.addAnnotatedClass(com.lp.server.forecast.ejb.Forecastart.class);
		metadataSources.addAnnotatedClass(com.lp.server.forecast.ejb.Forecastartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.forecast.ejb.ForecastartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.forecast.ejb.Forecastauftrag.class);
		metadataSources.addAnnotatedClass(com.lp.server.forecast.ejb.Forecastposition.class);
		metadataSources.addAnnotatedClass(com.lp.server.forecast.ejb.Importdef.class);
		metadataSources.addAnnotatedClass(com.lp.server.forecast.ejb.Importdefspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.forecast.ejb.ImportdefsprPk.class);
		metadataSources.addAnnotatedClass(com.lp.server.forecast.ejb.Kommdrucker.class);
		metadataSources.addAnnotatedClass(com.lp.server.forecast.ejb.Linienabruf.class);
		metadataSources.addAnnotatedClass(com.lp.server.inserat.ejb.Inserat.class);
		metadataSources.addAnnotatedClass(com.lp.server.inserat.ejb.Inseratartikel.class);
		metadataSources.addAnnotatedClass(com.lp.server.inserat.ejb.Inserater.class);
		metadataSources.addAnnotatedClass(com.lp.server.inserat.ejb.Inseratrechnung.class);
		metadataSources.addAnnotatedClass(com.lp.server.inserat.ejb.Inseratrechnungartikel.class);
		metadataSources.addAnnotatedClass(com.lp.server.instandhaltung.ejb.Anlage.class);
		metadataSources.addAnnotatedClass(com.lp.server.instandhaltung.ejb.Geraet.class);
		metadataSources.addAnnotatedClass(com.lp.server.instandhaltung.ejb.Geraetehistorie.class);
		metadataSources.addAnnotatedClass(com.lp.server.instandhaltung.ejb.Geraetetyp.class);
		metadataSources.addAnnotatedClass(com.lp.server.instandhaltung.ejb.Gewerk.class);
		metadataSources.addAnnotatedClass(com.lp.server.instandhaltung.ejb.Halle.class);
		metadataSources.addAnnotatedClass(com.lp.server.instandhaltung.ejb.Instandhaltung.class);
		metadataSources.addAnnotatedClass(com.lp.server.instandhaltung.ejb.Iskategorie.class);
		metadataSources.addAnnotatedClass(com.lp.server.instandhaltung.ejb.Ismaschine.class);
		metadataSources.addAnnotatedClass(com.lp.server.instandhaltung.ejb.Standort.class);
		metadataSources.addAnnotatedClass(com.lp.server.instandhaltung.ejb.Standorttechniker.class);
		metadataSources.addAnnotatedClass(com.lp.server.instandhaltung.ejb.Wartungsliste.class);
		metadataSources.addAnnotatedClass(com.lp.server.instandhaltung.ejb.Wartungsschritte.class);
		metadataSources.addAnnotatedClass(com.lp.server.kueche.ejb.Bedienerlager.class);
		metadataSources.addAnnotatedClass(com.lp.server.kueche.ejb.Kassaartikel.class);
		metadataSources.addAnnotatedClass(com.lp.server.kueche.ejb.Kassaimport.class);
		metadataSources.addAnnotatedClass(com.lp.server.kueche.ejb.Kdc100log.class);
		metadataSources.addAnnotatedClass(com.lp.server.kueche.ejb.Kuecheumrechnung.class);
		metadataSources.addAnnotatedClass(com.lp.server.kueche.ejb.Speiseplan.class);
		metadataSources.addAnnotatedClass(com.lp.server.kueche.ejb.Speiseplanposition.class);
		metadataSources.addAnnotatedClass(com.lp.server.kueche.ejb.Tageslos.class);
		metadataSources.addAnnotatedClass(com.lp.server.lieferschein.ejb.Ausliefervorschlag.class);
		metadataSources.addAnnotatedClass(com.lp.server.lieferschein.ejb.Begruendung.class);
		metadataSources.addAnnotatedClass(com.lp.server.lieferschein.ejb.Lieferschein.class);
		metadataSources.addAnnotatedClass(com.lp.server.lieferschein.ejb.Lieferscheinart.class);
		metadataSources.addAnnotatedClass(com.lp.server.lieferschein.ejb.Lieferscheinartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.lieferschein.ejb.LieferscheinartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.lieferschein.ejb.Lieferscheinposition.class);
		metadataSources.addAnnotatedClass(com.lp.server.lieferschein.ejb.Lieferscheinpositionart.class);
		metadataSources.addAnnotatedClass(com.lp.server.lieferschein.ejb.Lieferscheinstatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.lieferschein.ejb.Lieferscheintext.class);
		metadataSources.addAnnotatedClass(com.lp.server.lieferschein.ejb.Packstueck.class);
		metadataSources.addAnnotatedClass(com.lp.server.lieferschein.ejb.Verkettet.class);
		metadataSources.addAnnotatedClass(com.lp.server.media.ejb.MediaEmailAttachment.class);
		metadataSources.addAnnotatedClass(com.lp.server.media.ejb.MediaEmailMeta.class);
		metadataSources.addAnnotatedClass(com.lp.server.media.ejb.MediaInbox.class);
		metadataSources.addAnnotatedClass(com.lp.server.media.ejb.MediaStore.class);
		metadataSources.addAnnotatedClass(com.lp.server.media.ejb.MediaStoreBeleg.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Anrede.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Anredespr.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.AnredesprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Ansprechpartner.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Ansprechpartneradressbuch.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Ansprechpartnerfunktion.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Ansprechpartnerfunktionspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.AnsprechpartnerfunktionsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Bank.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Beauskunftung.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Branche.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Branchespr.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.BranchesprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Dsgvokategorie.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Dsgvokategoriespr.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.DsgvokategoriesprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Dsgvotext.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Identifikation.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Identifikationspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.IdentifikationsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Kommunikationsart.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Kommunikationsartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.KommunikationsartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Kontakt.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Kontaktart.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Kunde.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Kundematerial.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Kundesachbearbeiter.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Kundesoko.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Kundesokomengenstaffel.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Kundespediteur.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Kurzbrief.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Lflfliefergruppe.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.LflfliefergruppePK.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Lfliefergruppe.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Lfliefergruppespr.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.LfliefergruppesprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Lieferant.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Lieferantbeurteilung.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Liefermengen.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Newslettergrund.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Partner.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Partneradressbuch.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Partnerart.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Partnerartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.PartnerartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Partnerbank.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Partnerbild.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Partnerklasse.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Partnerklassespr.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.PartnerklassesprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Partnerkommentar.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Partnerkommentarart.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Partnerkommentardruck.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Partnerkommunikation.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Paselektion.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Selektion.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Selektionspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.SelektionsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Serienbrief.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Serienbriefselektion.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.SerienbriefselektionPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Serienbriefselektionnegativ.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.SerienbriefselektionnegativPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.partner.ejb.Telefonnummer.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Abwesenheitsart.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Abwesenheitsartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.AbwesenheitsartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Angehoerigenart.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Angehoerigenartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.AngehoerigenartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Anwesenheitsbestaetigung.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Artikelzulage.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Artikelzuschlag.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.AuszahlungBVA.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Bereitschaft.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Bereitschaftart.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Bereitschafttag.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Beruf.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Betriebskalender.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Diaeten.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Diaetentagessatz.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Eintrittaustritt.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Fahrzeug.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Fahrzeugkosten.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Fahrzeugverwendungsart.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Familienstand.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Familienstandspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.FamilienstandsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Feiertag.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Fingerart.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Gleitzeitsaldo.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Hvmabenutzer.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Hvmalizenz.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Hvmarecht.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Hvmarolle.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Hvmasync.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Kollektiv.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.KollektivUestdBVA.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Kollektivuestd.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Kollektivuestd50.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Lohnart.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Lohnartstundenfaktor.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Lohngruppe.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Lohnstundenart.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Maschine.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Maschineleistungsfaktor.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Maschinemaschinenzm.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Maschinengruppe.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Maschinenkosten.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Maschinenzeitdaten.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Maschinenzeitdatenverrechnet.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Maschinenzm.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Maschinenzmtagesart.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Nachrichten.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Nachrichtenabo.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Nachrichtenart.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Nachrichtenartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.NachrichtenartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Nachrichtenempfaenger.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Nachrichtengruppe.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Nachrichtengruppeteilnehmer.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Passivereise.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Pendlerpauschale.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Personal.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Personalangehoerige.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Personalart.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Personalartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.PersonalartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Personalfahrzeug.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Personalfinger.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Personalfunktion.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Personalfunktionspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.PersonalfunktionsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Personalgehalt.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Personalgruppe.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Personalgruppekosten.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Personalterminal.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Personalverfuegbarkeit.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Personalzeiten.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Personalzeitmodell.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Personalzutrittsklasse.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Projektzeiten.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Reise.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Reiselog.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Reisespesen.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Reiseverrechnet.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Religion.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Religionspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.ReligionsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Schicht.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Schichtzeit.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Schichtzeitmodell.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Schichtzuschlag.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Signatur.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Sonderzeiten.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Stundenabrechnung.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Taetigkeit.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Taetigkeitart.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Taetigkeitartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.TaetigkeitartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Taetigkeitspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.TaetigkeitsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Tagesart.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Tagesartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.TagesartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Telefonzeiten.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Telefonzeitenverrechnet.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.UebertragBVA.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Urlaubsanspruch.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zahltag.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zeitabschluss.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zeitdaten.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zeitdatenverrechnet.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zeitdatenverrechnetzeitraum.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zeitgutschrift.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zeitmodell.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zeitmodellspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.ZeitmodellsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zeitmodelltag.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zeitmodelltagpause.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zeitstift.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zeitverteilung.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zulage.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zutrittdaueroffen.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zutrittonlinecheck.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zutrittscontroller.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zutrittsklasse.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zutrittsklasseobjekt.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zutrittsleser.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zutrittslog.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zutrittsmodell.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zutrittsmodelltag.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zutrittsmodelltagdetail.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zutrittsobjekt.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zutrittsobjektverwendung.class);
		metadataSources.addAnnotatedClass(com.lp.server.personal.ejb.Zutrittsoeffnungsart.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.Bereich.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.History.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.Historyart.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.Kategorie.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.KategoriePK.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.Kategoriespr.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.KategoriesprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.Leadstatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.Projekt.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.Projekterledigungsgrund.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.Projektgruppe.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.Projektstatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.ProjektstatusPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.Projekttaetigkeit.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.Projekttechniker.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.Projekttyp.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.ProjekttypPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.Projekttypspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.ProjekttypsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.Vkfortschritt.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.Vkfortschrittspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.projekt.ejb.VkfortschrittsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Abrechnungsvorschlag.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Gutschriftpositionsart.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Gutschriftsgrund.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Gutschriftsgrundspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.GutschriftsgrundsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Gutschrifttext.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Mmz.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Proformarechnungpositionsart.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Rechnung.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Rechnungart.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Rechnungartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.RechnungartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Rechnungkontierung.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Rechnungposition.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Rechnungpositionsart.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Rechnungstatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Rechnungtext.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Rechnungtyp.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Rechnungzahlung.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Verrechnungsmodell.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Verrechnungsmodelltag.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Vorkasseposition.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Zahlungsart.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.Zahlungsartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.rechnung.ejb.ZahlungsartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Aufnahmeart.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Aufnahmeartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.AufnahmeartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Behandlung.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Behandlungspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.BehandlungsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Fehler.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Fehlerangabe.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Fehlerangabespr.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.FehlerangabesprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Fehlerspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.FehlersprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Massnahme.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Massnahmespr.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.MassnahmesprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Reklamation.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Reklamationart.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Reklamationbild.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Schwere.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Schwerespr.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.SchweresprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Termintreue.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Wirksamkeit.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.Wirksamkeitspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.reklamation.ejb.WirksamkeitsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Alternativmaschine.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Apkommentar.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Apkommentarspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.ApkommentarsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Fertigungsgruppe.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Kommentarimport.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Montageart.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Posersatz.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Profirstignore.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Pruefart.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Pruefartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.PruefartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Pruefkombination.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Pruefkombinationspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.PruefkombinationsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Stklagerentnahme.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Stklparameter.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Stklparameterspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.StklparametersprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Stklpruefplan.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Stueckliste.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Stuecklistearbeitsplan.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Stuecklisteart.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Stuecklisteeigenschaft.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Stuecklisteeigenschaftart.class);
		metadataSources.addAnnotatedClass(com.lp.server.stueckliste.ejb.Stuecklisteposition.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Anwender.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Arbeitsplatz.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Arbeitsplatzparameter.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.AutoBestellvorschlag.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.AutoFehlmengendruck.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.AutoLoseerledigen.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.AutoMahnen.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.AutoMahnungsversand.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.AutoMonatsabrechnungversand.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.AutoMonatsabrechnungversandAbteilungen.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.AutoRahmendetailbedarfdruck.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Automatikjobs.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Automatikjobtype.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Automatiktimer.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Belegart.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Belegartdokument.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Belegartmedia.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Belegartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.BelegartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Datenformat.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Dokument.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Dokumentenlink.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Dokumentenlinkbeleg.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Dokumentschlagwort.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Einheit.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Einheitkonvertierung.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Einheitspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.EinheitsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.EntityLog.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Extraliste.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Funktion.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Funktionspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.FunktionsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Geschaeftsjahr.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.GeschaeftsjahrMandant.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Installer.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Keyvalue.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.KeyvaluePK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Kostenstelle.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Kostentraeger.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Laenderart.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Laenderartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.LaenderartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Land.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Landkfzkennzeichen.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Landplzort.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Landspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.LandsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Lieferart.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Lieferartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.LieferartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.LocaleLP.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.LpDirekthilfe.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.LpUserCount.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Mandant.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Mandantagbspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Mediaart.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Mediaartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.MediaartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Mediastandard.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Modulberechtigung.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.ModulberechtigungPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Mwstsatz.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Mwstsatzbez.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Ort.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Panel.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Panelbeschreibung.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Paneldaten.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Panelsperren.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Parameter.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Parameteranwender.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.ParameteranwenderPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Parametermandant.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.ParametermandantPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Parametermandantgueltigab.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.ParametermandantgueltigabPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Positionsart.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Positionsartspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.PositionsartsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Protokoll.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Rechtsform.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Reportkonf.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Reportvariante.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Spediteur.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Standarddrucker.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Status.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Statusspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.StatussprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Text.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.TextPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.TheJudgePK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Theclient.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Thejudge.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Verkehrsweg.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.VerkehrswegCC.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Versandanhang.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Versandauftrag.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Versandstatus.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Versandweg.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.VersandwegCC.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.VersandwegCCPartner.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Waehrung.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Wechselkurs.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.WechselkursPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Zahlungsziel.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Zahlungszielspr.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.ZahlungszielsprPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Zusatzfunktion.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.Zusatzfunktionberechtigung.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.ejb.ZusatzfunktionberechtigungPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.jcr.ejb.Dokumentbelegart.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.jcr.ejb.DokumentbelegartPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.jcr.ejb.Dokumentgruppierung.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.jcr.ejb.DokumentgruppierungPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.jcr.ejb.Dokumentnichtarchiviert.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.jcr.ejb.DokumentnichtarchiviertPK.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.pkgenerator.ejb.Sequence.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.pkgenerator.ejb.SequenceBelegnr.class);
		metadataSources.addAnnotatedClass(com.lp.server.system.pkgenerator.service.SequenceBelegnrPK.class);

		// trouble!
		// metadataSources.addAnnotatedClass(com.lp.server.system.ejb.AutoLumiquote.class);

		// no in persistence.xml, added by JO
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Webpartner.class);
		metadataSources.addAnnotatedClass(com.lp.server.angebotstkl.ejb.Webabfrage.class);

		Metadata metadata = metadataSources.buildMetadata();

		SchemaExport schemaExport = new SchemaExport();
		schemaExport.setFormat(true);
		schemaExport.setOutputFile(PATH_AND_FILENAME_DDL);
		schemaExport.createOnly(EnumSet.of(TargetType.SCRIPT), metadata);

		// postcondition
		final File ddlFile = new File(PATH_AND_FILENAME_DDL);

		Assertions.assertEquals(true, ddlFile != null);

		System.out.println("DDL-FILE_PATH_TEMP= found: " + ddlFile.getAbsolutePath());
	}

//	@Test
//	@YBPriority(10004)
//	public void _4_execute_Ddl_executed() {
//
//		log.debug("Database {} found, will now create the Schema...", databaseData.jdbcUrlYalablue);
//
//		final String ddlYB = PATH_TEMP + File.separatorChar + SCHEMA_FILENAME_HBM2DDL;
//
//		final File ddlFile = new File(ddlYB);
//
//		Assert.assertTrue(ddlFile.isFile());
//		Assert.assertTrue(ddlFile.canRead());
//
//		log.debug("DDL-FILE_PATH_TEMP=", ddlFile.getAbsolutePath() + "found");
//
//		try (final BufferedReader brDDLFile = new BufferedReader(new FileReader(ddlFile))) {
//
//			String statement = readNextStatement(brDDLFile);
//
//			while (statement != null) {
//				executeYalablueDev(statement);
//
//				log.debug(statement);
//
//				statement = readNextStatement(brDDLFile);
//			}
//		}
//		catch (final Exception e) {
//			log.error(e.getMessage());
//			Assert.fail(e.getMessage());
//		}
//	}
//
//	@Test
//	@YBPriority(10005)
//	public void _5_checkUniqueKeyNaming_checked() throws IOException {
//
//		final File ddlFile = new File(PATH_AND_FILENAME_DDL);
//
//		Assert.assertTrue(ddlFile.isFile());
//		Assert.assertTrue(ddlFile.canRead());
//
//		log.debug("DDL-FILE_PATH_TEMP={} found", ddlFile.getAbsolutePath());
//
//		final List<String> listOfWrongNamedUk = new ArrayList<>();
//
//		try (final BufferedReader brDdlFile = new BufferedReader(new FileReader(ddlFile))) {
//
//			String sqlStatement = readNextStatement(brDdlFile);
//			int i = 0;
//			while (sqlStatement != null) {
//				log.debug(sqlStatement);
//
//				final int xUnique = sqlStatement.indexOf("unique");
//				if (xUnique > -1) {
//					final String uk = sqlStatement.substring(sqlStatement.indexOf("constraint") + 11, xUnique - 1);
//					if (!uk.startsWith("uk")) {
//						log.debug("unique='{}'", uk);
//						listOfWrongNamedUk.add(String.format("%s. %s: %s", ++i, uk, sqlStatement));
//					}
//				}
//				sqlStatement = readNextStatement(brDdlFile);
//			}
//		}
//
//		listOfWrongNamedUk.forEach(log::debug);
//
//		assertThat(listOfWrongNamedUk, hasSize(0));
//	}

//	private void executeDdlUseTemplate1(final String ddl) throws SQLException {
//
//		try (Connection conn = DriverManager.getConnection(databaseData.jdbcUrlTemplate1, databaseData.username,
//				databaseData.passwd); Statement stmt = conn.createStatement();) {
//			stmt.executeUpdate(ddl);
//		}
//	}
//
//	private void executeYalablueDev(final String statement) throws SQLException {
//
//		log.debug(statement);
//		try (Connection conn = DriverManager.getConnection(databaseData.jdbcUrlYalablue, databaseData.username,
//				databaseData.passwd); Statement stmt = conn.createStatement();) {
//
//			stmt.executeUpdate(statement);
//		}
//	}

//	private boolean existYalablue() throws SQLException {
//
//		boolean exists = false;
//		ResultSet r = null;
//		try (Connection conn = DriverManager.getConnection(databaseData.jdbcUrlYalablue, "postgres", "postgrespw");
//				Statement stmt = conn.createStatement();) {
//			r = stmt.executeQuery(
//					String.format("select datname from pg_database where datname = '%s'", databaseData.databaseName));
//
//			if (!r.isClosed() && r.next())
//				exists = r.getString(1).equals(databaseData.databaseName);
//		} catch (final Exception e) {
//			exists = false;
//		}
//
//		return exists;
//	}
//
	private void backupFile() throws IOException {

		final File source = new File(PATH_AND_FILENAME_DDL);
		final File dest = new File(PATH_TEMP + File.separatorChar + SCHEMA_FILENAME_HBM2DDL_BEFORE);

		if (source.exists()) {
			Files.copy(source, dest);
			source.delete();
		}
	}
//
//	private static String readNextStatement(final BufferedReader bufferedReaderIn) throws IOException {
//
//		Assert.assertNotNull(bufferedReaderIn);
//
//		final StringBuffer ddlLine = new StringBuffer();
//
//		String readLine = bufferedReaderIn.readLine();
//
//		while (readLine != null && !readLine.endsWith(";")) {
//			ddlLine.append(readLine);
//			readLine = bufferedReaderIn.readLine();
//		}
//
//		if (readLine != null && readLine.endsWith(";"))
//			ddlLine.append(readLine);
//
//		return ddlLine.length() == 0 ? null : ddlLine.toString();
//	}
//
}
