package org.kieselstein.tests.client.issues;

import com.lp.client.frame.ExceptionLP;
import com.lp.server.artikel.service.ArtikelDto;
import com.lp.server.artikel.service.ArtikelFac;
import com.lp.server.artikel.service.LagerDto;
import com.lp.server.artikel.service.VkPreisfindungEinzelverkaufspreisDto;
import com.lp.server.finanz.service.FinanzamtDto;
import com.lp.server.partner.service.KundeDto;
import com.lp.server.partner.service.PartnerDto;
import com.lp.server.partner.service.PartnerFac;
import com.lp.server.rechnung.service.RechnungDto;
import com.lp.server.rechnung.service.RechnungFac;
import com.lp.server.rechnung.service.RechnungPositionDto;
import com.lp.server.system.service.*;
import com.lp.util.Helper;
import org.junit.jupiter.api.*;
import org.kieselstein.tests.client.regression.TestKs;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class TestSuiteIssue79 {

    @Nested
    @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    @DisplayName("Base data setup")
    class BaseDataSetup extends TestKs {

        public BaseDataSetup() throws ExceptionLP {
            super();
        }

        @Test
        @Order(1)
        @DisplayName("Set the Parameter KUNDE_POSITIONSKONTIERUNG and DEFAULT_ARTIKEL_MWSTSATZ")
        void SetParameter() throws Throwable {
            changeParameterValue("001", "KUNDEN", "KUNDEN_POSITIONSKONTIERUNG", "1");
            String cValue = mandantFac.mwstsatzbezFindByBezeichnung("Allgemeine Waren", "001").getIId().toString();
            changeParameterValue("001", "ARTIKEL", "DEFAULT_ARTIKEL_MWSTSATZ", cValue);
        }

        @Test
        @Order(2)
        @DisplayName("Create article with normal VAT")
        void createArticleWithNormalVAT() throws RemoteException {
            int mwstSatzBezIId = mandantFac.mwstsatzbezFindByBezeichnung("Allgemeine Waren", "001").getIId();
            createArtikel("NORMALER",ArtikelFac.ARTIKELART_ARTIKEL, SystemFac.EINHEIT_STUECK, mwstSatzBezIId);
        }

        @Test
        @Order(3)
        @DisplayName("Create article with reduced VAT")
        void createArticleWithReducedVAT() throws RemoteException {
            int mwstSatzBezIId = mandantFac.mwstsatzbezFindByBezeichnung("reduzierter Satz, Lebensmittel, B\u00fccher", "001").getIId();
            createArtikel("REDUZIERT",ArtikelFac.ARTIKELART_ARTIKEL, SystemFac.EINHEIT_STUECK, mwstSatzBezIId);
        }

        @Test
        @Order(4)
        @DisplayName("Change location, ssn and country of client")
        void changeLocationSSNCountryOfClient() throws RemoteException {
            PartnerDto  partnerDto = mandantFac.mandantFindByPrimaryKey("001", theClientDto).getPartnerDto();
            LandplzortDto  newLandplzortDto = systemFac.landplzortFindByLandOrtPlzOhneExc("AT", "Salzburg", "5020");
            partnerDto.setLandplzortDto(newLandplzortDto);
            partnerDto.setLandplzortIId(newLandplzortDto.getIId());
            partnerFac.updatePartner(partnerDto, theClientDto);

            PartnerDto partnerDtoFromDB = mandantFac.mandantFindByPrimaryKey("001", theClientDto).getPartnerDto();

            assertThat(partnerDtoFromDB).isNotNull();
            assertThat(partnerDtoFromDB.getLandplzortDto().getIlandID()).isEqualTo(partnerDto.getLandplzortDto().getIlandID());

        }
        @Test
        @Order(5)
        @DisplayName("Set the Parameter AUTOMATISCHE_DEBITORENNUMMER")
        void SetParameterAutomatioscheDebitorennummer() throws Throwable {
            changeParameterValue("001", "KUNDEN", "AUTOMATISCHE_DEBITORENNUMMER", "1");
        }
        @Nested
        @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
        @DisplayName("Manual Booking Setup")
        public class ManualBookingSetup {

            @Test
            @Order(1)
            @DisplayName("Create manual booking of article with normal VAT")
            void createManualArticleBookingWithNormalVAT() throws RemoteException {
                ParametermandantDto parameterDto = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_ARTIKEL, ParameterFac.PARAMETER_DEFAULT_LAGER);
                LagerDto lagerDto = lagerfac.lagerFindByPrimaryKey(new Integer(parameterDto.getCWert()));

                BigDecimal nEinstandsPreis = BigDecimal.ONE;
                String cKommentar = "NORMAL INLAND";
                int nArtikelID = artikelFac.artikelFindByCNr("NORMALER", theClientDto).getIId();
                BigDecimal nMenge = new BigDecimal("100.000000");

                createManualBooking(lagerDto, nEinstandsPreis, cKommentar, nArtikelID, nMenge, true, (short) 0, null);

            }

            @Test
            @Order(2)
            @DisplayName("Create manual booking of article with reduced VAT")
            void createManualArticleBookingWithReducedVAT() throws RemoteException {
                ParametermandantDto parameterDto = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_ARTIKEL, ParameterFac.PARAMETER_DEFAULT_LAGER);
                LagerDto lagerDto = lagerfac.lagerFindByPrimaryKey(new Integer(parameterDto.getCWert()));

                BigDecimal nEinstandsPreis = new BigDecimal("0.500000");
                String cKommentar = "REDUZIERT  INLAND";
                int nArtikelID = artikelFac.artikelFindByCNr("REDUZIERT", theClientDto).getIId();
                BigDecimal nMenge = new BigDecimal("100.000000");

                createManualBooking(lagerDto, nEinstandsPreis, cKommentar, nArtikelID, nMenge, true, (short) 0, null);
            }

            @Nested
            @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
            @DisplayName("Customer Setup")
            class CustomerSetup {

                @Test
                @Order(1)
                @DisplayName("Create Customer IG and free of fee")
                void CreateCustomerIG() throws RemoteException {
                    LandplzortDto  newLandplzortDto = systemFac.landplzortFindByLandOrtPlzOhneExc("AT", "Salzburg", "5020");
                    MandantDto mandantDto = mandantFac.mandantFindByPrimaryKey("001", theClientDto);

                    String cKbez = "MIG";
                    String cName1 = "Musterkunde Inland IG";
                    String cEori = "111";

                    String cPartnerArtCnr = PartnerFac.PARTNERART_ADRESSE;
                    String localeCnrKommunikation = theClientDto.getLocUiAsString();
                    boolean bVersteckt = false;

                    PartnerDto partnerDto = createPartner(cKbez, cPartnerArtCnr, localeCnrKommunikation, bVersteckt, cName1, cEori, newLandplzortDto);

                    int nKundenNummer = 8000;
                    int nMwstSteuerint = mandantFac.mwstsatzbezFindByBezeichnung("Steuerfrei", mandantDto.getCNr()).getIId();

                    String sKreditlimit = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_KUNDEN, ParameterFac.PARAMETER_KREDITLIMIT).getCWert();
                    int iKreditlimit = Integer.parseInt(sKreditlimit);
                    BigDecimal bdKreditLimit = new BigDecimal(iKreditlimit);

                    ParametermandantDto parameter = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_KUNDEN, ParameterFac.PARAMETER_DEFAULT_KUNDE_AKZEPTIERT_TEILLIEFERUNGEN);
                    Short sAkzeptierTeillieferung = new Short(parameter.getCWert());

                    parameter = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_KUNDEN, ParameterFac.PARAMETER_DEFAULT_BELEGDRUCK_MIT_RABATT);
                    Short bRechnungsdruckMitRabatt = Helper.boolean2Short((Boolean) parameter.getCWertAsObject());

                    createCustomer(partnerDto, mandantDto, nKundenNummer, nMwstSteuerint, bdKreditLimit, sAkzeptierTeillieferung, bRechnungsdruckMitRabatt, false, false, false, false, false, false);

                }

                @Test
                @Order(2)
                @DisplayName("Create new customer within same country as client and with normal VAT")
                void CreateCustomerClientCountryNormalVAT() throws RemoteException {
                    LandplzortDto  newLandplzortDto = systemFac.landplzortFindByLandOrtPlzOhneExc("AT", "Salzburg", "5020");
                    MandantDto mandantDto = mandantFac.mandantFindByPrimaryKey("001", theClientDto);

                    String cKbez = "MIN";
                    String cName1 = "Musterkunde Inland Normal MWST";
                    String cEori = "222";
                    String cPartnerArtCnr = PartnerFac.PARTNERART_ADRESSE;
                    String localeCnrKommunikation = theClientDto.getLocUiAsString();
                    boolean bVersteckt = false;

                    PartnerDto partnerDto = createPartner(cKbez, cPartnerArtCnr, localeCnrKommunikation, bVersteckt, cName1, cEori, newLandplzortDto);

                    int nKundenNummer = 8001;
                    int nMwstSteuerint = mandantFac.mwstsatzbezFindByBezeichnung("Allgemeine Waren", mandantDto.getCNr()).getIId();

                    String sKreditlimit = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_KUNDEN, ParameterFac.PARAMETER_KREDITLIMIT).getCWert();
                    int iKreditlimit = Integer.parseInt(sKreditlimit);
                    BigDecimal bdKreditLimit = new BigDecimal(iKreditlimit);

                    ParametermandantDto parameter = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_KUNDEN, ParameterFac.PARAMETER_DEFAULT_KUNDE_AKZEPTIERT_TEILLIEFERUNGEN);
                    Short sAkzeptierTeillieferung = new Short(parameter.getCWert());

                    parameter = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_KUNDEN, ParameterFac.PARAMETER_DEFAULT_BELEGDRUCK_MIT_RABATT);
                    Short bRechnungsdruckMitRabatt = Helper.boolean2Short((Boolean) parameter.getCWertAsObject());

                    createCustomer(partnerDto, mandantDto, nKundenNummer, nMwstSteuerint, bdKreditLimit, sAkzeptierTeillieferung, bRechnungsdruckMitRabatt, false, false, false, false, false, false);

                }

                @Test
                @Order(3)
                @DisplayName("Adapt selling price of article with normal and reduced VAT")
                void AdaptSellingPrice() throws RemoteException {
                    String mandantCnr = "001";
                    int nArtikelIDNormal = artikelFac.artikelFindByCNr("NORMALER", theClientDto).getIId();
                    BigDecimal nVerkaufPreisBasisNormal = new BigDecimal("10.00");
                    int nArtikelIDReduziert = artikelFac.artikelFindByCNr("REDUZIERT", theClientDto).getIId();
                    BigDecimal nVerkaufPreisBasisReduziert = new BigDecimal("5.00");

                    int normalArticle = createSellingPriceOfArticle(mandantCnr, nArtikelIDNormal, nVerkaufPreisBasisNormal);
                    int reducedArticle = createSellingPriceOfArticle(mandantCnr, nArtikelIDReduziert, nVerkaufPreisBasisReduziert);

                    assertThat(vkPreisfindungFac.einzelverkaufspreisFindByPrimaryKey(normalArticle,theClientDto).getNVerkaufspreisbasis()).isEqualTo(new BigDecimal("10.000000"));
                    assertThat(vkPreisfindungFac.einzelverkaufspreisFindByPrimaryKey(reducedArticle, theClientDto).getNVerkaufspreisbasis()).isEqualTo(new BigDecimal("5.000000"));
                }

                @Nested
                @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
                @DisplayName("Invoice Setup")
                class invoiceSetup {

                    @Test
                    @Order(1)
                    @DisplayName("Create invoice with customer with same country as client")
                    void testInvoiceClientCountryCustomer() throws RemoteException {
                        MandantDto mandantDto = mandantFac.mandantFindByPrimaryKey("001", theClientDto);
                        KundeDto kundeDto = kundeFac.kundeFindByKundennummer(8001, theClientDto);
                        // call function kundeFindByPrimaryKey, because partnerdto is set with this exact function
                        kundeDto = kundeFac.kundeFindByPrimaryKey(kundeDto.getIId(), theClientDto);
                        ParametermandantDto parameterDto = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_ARTIKEL, ParameterFac.PARAMETER_DEFAULT_LAGER);
                        LagerDto lagerDto = lagerfac.lagerFindByPrimaryKey(new Integer(parameterDto.getCWert()));

                        String mandantCnr = "001";
                        String cNr = "Rechnung Inland";
                        String cBestellNummer = "Rechnung Inland";
                        Timestamp tBelegDatum = new Timestamp(System.currentTimeMillis());
                        String cRechnungArtCnr = RechnungFac.RECHNUNGTYP_RECHNUNG;
                        BigDecimal nKurs = BigDecimal.ONE;

                        createInvoice(mandantCnr, cNr, cBestellNummer, tBelegDatum, cRechnungArtCnr, nKurs, mandantDto, kundeDto, lagerDto);
                    }

                    @Test
                    @Order(2)
                    @DisplayName("Create invoice item for customer within client country and with normal VAT")
                    void testInvoiceItemClientCountryCustomerNormalVAT() throws RemoteException {
                        KundeDto kundeDto = kundeFac.kundeFindByKundennummer(8001, theClientDto);
                        kundeDto = kundeFac.kundeFindByPrimaryKey(kundeDto.getIId(), theClientDto);
                        RechnungDto [] rechnungDtos = rechnungFac.rechnungFindByKundeIIdMandantCNr(kundeDto.getIId(), "001");
                        ParametermandantDto parameterDto = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_ARTIKEL, ParameterFac.PARAMETER_DEFAULT_LAGER);
                        LagerDto lagerDto = lagerfac.lagerFindByPrimaryKey(new Integer(parameterDto.getCWert()));
                        RechnungDto rechnungDto = null;
                        for (RechnungDto dto: rechnungDtos) {
                            if (dto.getCBestellnummer().equals("Rechnung Inland")) {
                                rechnungDto = dto;
                            }
                        }
                        ArtikelDto artikelDto = artikelFac.artikelFindByCNr("NORMALER", theClientDto);
                        VkPreisfindungEinzelverkaufspreisDto vkPreisfindungEinzelverkaufspreisDto = vkPreisfindungFac.getArtikeleinzelverkaufspreis(artikelDto.getIId(), null, kundeDto.getCWaehrung(), theClientDto);

                        String sPositionsArt = LocaleFac.POSITIONSART_IDENT;
                        BigDecimal nMenge = BigDecimal.ONE;
                        String sEinheitCnr = SystemFac.EINHEIT_STUECK;
                        boolean bNettoPreisUebersteuert = false;
                        boolean bDrucken = true;

                        RechnungPositionDto rechnungPositionDto = createInvoiceItem(rechnungDto, artikelDto, kundeDto, vkPreisfindungEinzelverkaufspreisDto, lagerDto, sPositionsArt, nMenge, sEinheitCnr, bNettoPreisUebersteuert, bDrucken);

                        assertThat(rechnungFac.rechnungPositionFindByPrimaryKey(rechnungPositionDto.getIId()).getMwstsatzIId()).
                                isEqualTo(mandantFac.mwstsatzZuDatumClient(mandantFac.mwstsatzbezFindByBezeichnung("Allgemeine Waren", "001").getIId(), new Timestamp(System.currentTimeMillis()), theClientDto).getIId());
                    }

                    @Test
                    @Order(3)
                    @DisplayName("Create invoice item for customer within client country and with reduced VAT")
                    void testInvoiceItemClientCountryCustomerReducedVAT() throws RemoteException {
                        KundeDto kundeDto = kundeFac.kundeFindByKundennummer(8001, theClientDto);
                        kundeDto = kundeFac.kundeFindByPrimaryKey(kundeDto.getIId(), theClientDto);
                        RechnungDto [] rechnungDtos = rechnungFac.rechnungFindByKundeIIdMandantCNr(kundeDto.getIId(), "001");
                        ParametermandantDto parameterDto = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_ARTIKEL, ParameterFac.PARAMETER_DEFAULT_LAGER);
                        LagerDto lagerDto = lagerfac.lagerFindByPrimaryKey(new Integer(parameterDto.getCWert()));
                        RechnungDto rechnungDto = null;
                        for (RechnungDto dto: rechnungDtos) {
                            if (dto.getCBestellnummer().equals("Rechnung Inland")) {
                                rechnungDto = dto;
                            }
                        }
                        ArtikelDto artikelDto = artikelFac.artikelFindByCNr("REDUZIERT", theClientDto);
                        VkPreisfindungEinzelverkaufspreisDto vkPreisfindungEinzelverkaufspreisDto = vkPreisfindungFac.getArtikeleinzelverkaufspreis(artikelDto.getIId(), null, kundeDto.getCWaehrung(), theClientDto);

                        String sPositionsArt = LocaleFac.POSITIONSART_IDENT;
                        BigDecimal nMenge = BigDecimal.ONE;
                        String sEinheitCnr = SystemFac.EINHEIT_STUECK;
                        boolean bNettoPreisUebersteuert = false;
                        boolean bDrucken = true;

                        RechnungPositionDto rechnungPositionDto = createInvoiceItem(rechnungDto, artikelDto, kundeDto, vkPreisfindungEinzelverkaufspreisDto, lagerDto, sPositionsArt, nMenge, sEinheitCnr, bNettoPreisUebersteuert, bDrucken);

                        assertThat(rechnungFac.rechnungPositionFindByPrimaryKey(rechnungPositionDto.getIId()).getMwstsatzIId()).
                                isEqualTo(mandantFac.mwstsatzZuDatumClient(mandantFac.mwstsatzbezFindByBezeichnung("reduzierter Satz, Lebensmittel, B\u00fccher", "001").getIId(), new Timestamp(System.currentTimeMillis()), theClientDto).getIId());


                    }

                    @Test
                    @Order(4)
                    @DisplayName("Create invoice with IG Customer")
                    void testInvoiceCustomerIG() throws RemoteException {
                        MandantDto mandantDto = mandantFac.mandantFindByPrimaryKey("001", theClientDto);
                        KundeDto kundeDto = kundeFac.kundeFindByKundennummer(8000, theClientDto);
                        kundeDto = kundeFac.kundeFindByPrimaryKey(kundeDto.getIId(), theClientDto);
                        ParametermandantDto parameterDto = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_ARTIKEL, ParameterFac.PARAMETER_DEFAULT_LAGER);
                        LagerDto lagerDto = lagerfac.lagerFindByPrimaryKey(new Integer(parameterDto.getCWert()));

                        String mandantCnr = "001";
                        String cNr = "Rechnung Inland IG";
                        String cBestellNummer = "Rechnung Inland IG";
                        Timestamp tBelegDatum = new Timestamp(System.currentTimeMillis());
                        String cRechnungArtCnr = RechnungFac.RECHNUNGTYP_RECHNUNG;
                        BigDecimal nKurs = BigDecimal.ONE;

                        createInvoice(mandantCnr, cNr, cBestellNummer, tBelegDatum, cRechnungArtCnr, nKurs, mandantDto, kundeDto, lagerDto);

                    }

                    @Test
                    @Order(5)
                    @DisplayName("Create invoice item with customer IG with normal VAT")
                    void testInvoiceItemIGCustomerNormalVAT() throws RemoteException {
                        KundeDto kundeDto = kundeFac.kundeFindByKundennummer(8000, theClientDto);
                        kundeDto = kundeFac.kundeFindByPrimaryKey(kundeDto.getIId(), theClientDto);
                        RechnungDto [] rechnungDtos = rechnungFac.rechnungFindByKundeIIdMandantCNr(kundeDto.getIId(), "001");
                        ParametermandantDto parameterDto = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_ARTIKEL, ParameterFac.PARAMETER_DEFAULT_LAGER);
                        LagerDto lagerDto = lagerfac.lagerFindByPrimaryKey(new Integer(parameterDto.getCWert()));
                        RechnungDto rechnungDto = null;
                        for (RechnungDto dto: rechnungDtos) {
                            if (dto.getCBestellnummer().equals("Rechnung Inland IG")) {
                                rechnungDto = dto;
                            }
                        }
                        ArtikelDto artikelDto = artikelFac.artikelFindByCNr("NORMALER", theClientDto);
                        VkPreisfindungEinzelverkaufspreisDto vkPreisfindungEinzelverkaufspreisDto = vkPreisfindungFac.getArtikeleinzelverkaufspreis(artikelDto.getIId(), null, kundeDto.getCWaehrung(), theClientDto);

                        String sPositionsArt = LocaleFac.POSITIONSART_IDENT;
                        BigDecimal nMenge = BigDecimal.ONE;
                        String sEinheitCnr = SystemFac.EINHEIT_STUECK;
                        boolean bNettoPreisUebersteuert = false;
                        boolean bDrucken = true;

                        RechnungPositionDto rechnungPositionDto = createInvoiceItem(rechnungDto, artikelDto, kundeDto, vkPreisfindungEinzelverkaufspreisDto, lagerDto, sPositionsArt, nMenge, sEinheitCnr, bNettoPreisUebersteuert, bDrucken);

                        assertThat(rechnungFac.rechnungPositionFindByPrimaryKey(rechnungPositionDto.getIId()).getMwstsatzIId()).
                                isEqualTo(mandantFac.mwstsatzZuDatumClient(mandantFac.mwstsatzbezFindByBezeichnung("Steuerfrei", "001").getIId(), new Timestamp(System.currentTimeMillis()), theClientDto).getIId());

                    }

                    @Test
                    @Order(6)
                    @DisplayName("Create invoice item with customer IG with reduced VAT")
                    void testInvoiceItemIGCustomerReducedVAT() throws RemoteException {
                        KundeDto kundeDto = kundeFac.kundeFindByKundennummer(8000, theClientDto);
                        kundeDto = kundeFac.kundeFindByPrimaryKey(kundeDto.getIId(), theClientDto);
                        RechnungDto [] rechnungDtos = rechnungFac.rechnungFindByKundeIIdMandantCNr(kundeDto.getIId(), "001");
                        ParametermandantDto parameterDto = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_ARTIKEL, ParameterFac.PARAMETER_DEFAULT_LAGER);
                        LagerDto lagerDto = lagerfac.lagerFindByPrimaryKey(new Integer(parameterDto.getCWert()));
                        RechnungDto rechnungDto = null;
                        for (RechnungDto dto: rechnungDtos) {
                            if (dto.getCBestellnummer().equals("Rechnung Inland IG")) {
                                rechnungDto = dto;
                            }
                        }
                        ArtikelDto artikelDto = artikelFac.artikelFindByCNr("REDUZIERT", theClientDto);
                        VkPreisfindungEinzelverkaufspreisDto vkPreisfindungEinzelverkaufspreisDto = vkPreisfindungFac.getArtikeleinzelverkaufspreis(artikelDto.getIId(), null, kundeDto.getCWaehrung(), theClientDto);

                        String sPositionsArt = LocaleFac.POSITIONSART_IDENT;
                        BigDecimal nMenge = BigDecimal.ONE;
                        String sEinheitCnr = SystemFac.EINHEIT_STUECK;
                        boolean bNettoPreisUebersteuert = false;
                        boolean bDrucken = true;

                        RechnungPositionDto rechnungPositionDto = createInvoiceItem(rechnungDto, artikelDto, kundeDto, vkPreisfindungEinzelverkaufspreisDto, lagerDto, sPositionsArt, nMenge, sEinheitCnr, bNettoPreisUebersteuert, bDrucken);

                        assertThat(rechnungFac.rechnungPositionFindByPrimaryKey(rechnungPositionDto.getIId()).getMwstsatzIId()).
                                isEqualTo(mandantFac.mwstsatzZuDatumClient(mandantFac.mwstsatzbezFindByBezeichnung("Steuerfrei", "001").getIId(), new Timestamp(System.currentTimeMillis()), theClientDto).getIId());
                    }

                    @Test
                    @Order(7)
                    @DisplayName("Copy Invoice Customer Inland")
                    void testCopyInvoiceCustomerInland() {

                    }

                    @Test
                    @Order(8)
                    @DisplayName("Create Invoice item creation via Barcode Scanner")
                    void testBarcodeCreationInvoiceCustomerClientCountry() throws RemoteException {
                        KundeDto kundeDto = kundeFac.kundeFindByKundennummer(8001, theClientDto);
                        kundeDto = kundeFac.kundeFindByPrimaryKey(kundeDto.getIId(), theClientDto);
                        RechnungDto [] rechnungDtos = rechnungFac.rechnungFindByKundeIIdMandantCNr(kundeDto.getIId(), "001");
                        RechnungDto rechnungDto = null;
                        for (RechnungDto dto: rechnungDtos) {
                            if (dto.getCBestellnummer().equals("Rechnung Inland")) {
                                rechnungDto = dto;
                            }
                        }

                        ArtikelDto artikelDto = artikelFac.artikelFindByCNr("NORMALER", theClientDto);

                        RechnungPositionDto rechnungPositionDto = new RechnungPositionDto();
                        rechnungPositionDto.setBelegIId(rechnungDto.getIId());
                        rechnungPositionDto.setArtikelIId(artikelDto.getIId());
                        rechnungPositionDto.setPositionsartCNr(LocaleFac.POSITIONSART_IDENT);
                        rechnungPositionDto.setNMenge(BigDecimal.ONE);
                        rechnungPositionDto.setEinheitCNr(artikelDto.getEinheitCNr());
                        rechnungDto.setMwstsatzIId(artikelDto.getMwstsatzbezIId());
                        RechnungPositionDto savedDto = rechnungFac.createRechnungPosition(rechnungPositionDto, rechnungDto.getLagerIId(), false,true, theClientDto);

                        RechnungPositionDto rechnungPositionDtoFromDB = rechnungFac.rechnungPositionFindByPrimaryKey(savedDto.getIId());

                        assertThat(rechnungPositionDtoFromDB).isNotNull();
                        assertThat(rechnungPositionDtoFromDB.getMwstsatzIId()).
                                isEqualTo(mandantFac.mwstsatzZuDatumClient(mandantFac.mwstsatzbezFindByBezeichnung("Allgemeine Waren", "001").getIId(), new Timestamp(System.currentTimeMillis()), theClientDto).getIId());

                    }

                    @Test
                    @Order(9)
                    @DisplayName("Create VAT for DE")
                    void createVATForDE() throws RemoteException, ParseException {
                        MandantDto mandantDto = mandantFac.mandantFindByPrimaryKey("001", theClientDto);
                        LandplzortDto  newLandplzortDEDto = systemFac.landplzortFindByLandOrtPlzOhneExc("DE", "Stuttgart", "70563");

                        String cKbez = "FADE";
                        String cName1 = "Finazamt DE";
                        String cEori = "123";
                        String cPartnerArtCnr = PartnerFac.PARTNERART_SONSTIGES;
                        String localeCnrKommunikation = theClientDto.getLocUiAsString();
                        boolean bVersteckt = false;

                        PartnerDto partnerDto = createPartner(cKbez, cPartnerArtCnr, localeCnrKommunikation, bVersteckt, cName1, cEori, newLandplzortDEDto);
                        FinanzamtDto createdFA = createFinanzAmt(partnerDto, mandantDto, false);

                        MwstsatzbezDto mwstsatzbezDtoNormalDE =  createVATDescription("Allgemeine Waren DE", "001", createdFA.getPartnerIId());
                        MwstsatzbezDto mwstsatzbezDtoReducedDE =  createVATDescription("Reduzierte Waren DE", "001", createdFA.getPartnerIId());

                        DateFormat dateFormat = new SimpleDateFormat("dd.mm.yyyy");
                        Date date = dateFormat.parse("01.01.1999");
                        long time = date.getTime();


                        MwstsatzDto mwstsatzDtoNormalDE = createVAT(mwstsatzbezDtoNormalDE, 19.00, new Timestamp(time));
                        MwstsatzDto mwstsatzReduziertDE = createVAT(mwstsatzbezDtoReducedDE, 7.00, new Timestamp(time));

                    }

                    @Test
                    @Order(10)
                    @DisplayName("Create new Customer DE")
                    void createCustomerDE() throws RemoteException {
                        LandplzortDto  newLandplzortDto = systemFac.landplzortFindByLandOrtPlzOhneExc("DE", "Stuttgart", "70563");
                        MandantDto mandantDto = mandantFac.mandantFindByPrimaryKey("001", theClientDto);

                        String cKbez = "MDE";
                        String cName1 = "Musterkunde DE Normal MWST";
                        String cEori = null;
                        String cPartnerArtCnr = PartnerFac.PARTNERART_ADRESSE;
                        String localeCnrKommunikation = theClientDto.getLocUiAsString();
                        boolean bVersteckt = false;

                        PartnerDto partnerDto = createPartner(cKbez, cPartnerArtCnr, localeCnrKommunikation, bVersteckt, cName1, cEori, newLandplzortDto);

                        int nKundenNummer = 8003;
                        int nMwstSteuerint = mandantFac.mwstsatzbezFindByBezeichnung("Allgemeine Waren DE", mandantDto.getCNr()).getIId();

                        String sKreditlimit = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_KUNDEN, ParameterFac.PARAMETER_KREDITLIMIT).getCWert();
                        int iKreditlimit = Integer.parseInt(sKreditlimit);
                        BigDecimal bdKreditLimit = new BigDecimal(iKreditlimit);

                        ParametermandantDto parameter = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_KUNDEN, ParameterFac.PARAMETER_DEFAULT_KUNDE_AKZEPTIERT_TEILLIEFERUNGEN);
                        Short sAkzeptierTeillieferung = new Short(parameter.getCWert());

                        parameter = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_KUNDEN, ParameterFac.PARAMETER_DEFAULT_BELEGDRUCK_MIT_RABATT);
                        Short bRechnungsdruckMitRabatt = Helper.boolean2Short((Boolean) parameter.getCWertAsObject());

                        createCustomer(partnerDto, mandantDto, nKundenNummer, nMwstSteuerint, bdKreditLimit, sAkzeptierTeillieferung, bRechnungsdruckMitRabatt, false, false, false, false, false, false);

                    }

                    @Test
                    @Order(11)
                    @DisplayName("Test creation of new invoice for customer DE")
                    void testInvoiceCustomerDE() throws RemoteException {
                        MandantDto mandantDto = mandantFac.mandantFindByPrimaryKey("001", theClientDto);
                        KundeDto kundeDto = kundeFac.kundeFindByKundennummer(8003, theClientDto);
                        kundeDto = kundeFac.kundeFindByPrimaryKey(kundeDto.getIId(), theClientDto);
                        ParametermandantDto parameterDto = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_ARTIKEL, ParameterFac.PARAMETER_DEFAULT_LAGER);
                        LagerDto lagerDto = lagerfac.lagerFindByPrimaryKey(new Integer(parameterDto.getCWert()));

                        String mandantCnr = "001";
                        String cNr = "Rechnung DE";
                        String cBestellNummer = "Rechnung DE";
                        Timestamp tBelegDatum = new Timestamp(System.currentTimeMillis());
                        String cRechnungArtCnr = RechnungFac.RECHNUNGTYP_RECHNUNG;
                        BigDecimal nKurs = BigDecimal.ONE;

                        createInvoice(mandantCnr, cNr, cBestellNummer, tBelegDatum, cRechnungArtCnr, nKurs, mandantDto, kundeDto, lagerDto);

                    }
                    @Test
                    @Order(12)
                    @DisplayName("Test creation of new invoice item for customer DE with normal VAT")
                    void testInvoiceItemDECustomerNormalVAT() throws RemoteException {
                        KundeDto kundeDto = kundeFac.kundeFindByKundennummer(8003, theClientDto);
                        kundeDto = kundeFac.kundeFindByPrimaryKey(kundeDto.getIId(), theClientDto);
                        RechnungDto [] rechnungDtos = rechnungFac.rechnungFindByKundeIIdMandantCNr(kundeDto.getIId(), "001");
                        ParametermandantDto parameterDto = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_ARTIKEL, ParameterFac.PARAMETER_DEFAULT_LAGER);
                        LagerDto lagerDto = lagerfac.lagerFindByPrimaryKey(new Integer(parameterDto.getCWert()));

                        RechnungDto rechnungDto = null;
                        for (RechnungDto dto: rechnungDtos) {
                            if (dto.getCBestellnummer().equals("Rechnung DE")) {
                                rechnungDto = dto;
                            }
                        }
                        ArtikelDto artikelDto = artikelFac.artikelFindByCNr("NORMALER", theClientDto);
                        VkPreisfindungEinzelverkaufspreisDto vkPreisfindungEinzelverkaufspreisDto = vkPreisfindungFac.getArtikeleinzelverkaufspreis(artikelDto.getIId(), null, kundeDto.getCWaehrung(), theClientDto);

                        String sPositionsArt = LocaleFac.POSITIONSART_IDENT;
                        BigDecimal nMenge = BigDecimal.ONE;
                        String sEinheitCnr = SystemFac.EINHEIT_STUECK;
                        boolean bNettoPreisUebersteuert = false;
                        boolean bDrucken = true;

                        RechnungPositionDto rechnungPositionDto = createInvoiceItem(rechnungDto, artikelDto, kundeDto, vkPreisfindungEinzelverkaufspreisDto, lagerDto, sPositionsArt, nMenge, sEinheitCnr, bNettoPreisUebersteuert, bDrucken);

                        assertThat(rechnungFac.rechnungPositionFindByPrimaryKey(rechnungPositionDto.getIId()).getMwstsatzIId()).
                                isEqualTo(mandantFac.mwstsatzZuDatumClient(mandantFac.mwstsatzbezFindByBezeichnung("Allgemeine Waren", "001").getIId(), new Timestamp(System.currentTimeMillis()), theClientDto).getIId());

                    }

                    @Test
                    @Order(12)
                    @DisplayName("Test creation of new invoice item for customer DE with reduced VAT")
                    void testInvoiceItemDECustomerReducedVAT() throws RemoteException {
                        KundeDto kundeDto = kundeFac.kundeFindByKundennummer(8003, theClientDto);
                        kundeDto = kundeFac.kundeFindByPrimaryKey(kundeDto.getIId(), theClientDto);
                        RechnungDto [] rechnungDtos = rechnungFac.rechnungFindByKundeIIdMandantCNr(kundeDto.getIId(), "001");
                        ParametermandantDto parameterDto = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_ARTIKEL, ParameterFac.PARAMETER_DEFAULT_LAGER);
                        LagerDto lagerDto = lagerfac.lagerFindByPrimaryKey(new Integer(parameterDto.getCWert()));
                        RechnungDto rechnungDto = null;
                        for (RechnungDto dto: rechnungDtos) {
                            if (dto.getCBestellnummer().equals("Rechnung DE")) {
                                rechnungDto = dto;
                            }
                        }
                        ArtikelDto artikelDto = artikelFac.artikelFindByCNr("REDUZIERT", theClientDto);
                        VkPreisfindungEinzelverkaufspreisDto vkPreisfindungEinzelverkaufspreisDto = vkPreisfindungFac.getArtikeleinzelverkaufspreis(artikelDto.getIId(), null, kundeDto.getCWaehrung(), theClientDto);

                        String sPositionsArt = LocaleFac.POSITIONSART_IDENT;
                        BigDecimal nMenge = BigDecimal.ONE;
                        String sEinheitCnr = SystemFac.EINHEIT_STUECK;
                        boolean bNettoPreisUebersteuert = false;
                        boolean bDrucken = true;

                        RechnungPositionDto rechnungPositionDto = createInvoiceItem(rechnungDto, artikelDto, kundeDto, vkPreisfindungEinzelverkaufspreisDto, lagerDto, sPositionsArt, nMenge, sEinheitCnr, bNettoPreisUebersteuert, bDrucken);

                        assertThat(rechnungFac.rechnungPositionFindByPrimaryKey(rechnungPositionDto.getIId()).getMwstsatzIId()).
                                isEqualTo(mandantFac.mwstsatzZuDatumClient(mandantFac.mwstsatzbezFindByBezeichnung("reduzierter Satz, Lebensmittel, B\u00fccher", "001").getIId(), new Timestamp(System.currentTimeMillis()), theClientDto).getIId());

                    }

                    @Test
                    @Order(13)
                    @DisplayName("Add alternative VAT for NORMAL and REDUCED VAT")
                    void addNormalAndReducedAlternativeVAT() throws RemoteException {

                        MwstsatzbezDto mwstSatzBezNormal = mandantFac.mwstsatzbezFindByBezeichnung("Allgemeine Waren", "001");
                        MwstsatzbezDto mwstsatzbezDtoDENormal = mandantFac.mwstsatzbezFindByBezeichnung("Allgemeine Waren DE", "001");
                        addAlternativeVAT(mwstSatzBezNormal, mwstsatzbezDtoDENormal);

                        MwstsatzbezDto mwstSatzBezReduced = mandantFac.mwstsatzbezFindByBezeichnung("reduzierter Satz, Lebensmittel, B\u00fccher", "001");
                        MwstsatzbezDto mwstsatzbezDtoDEReduced = mandantFac.mwstsatzbezFindByBezeichnung("Reduzierte Waren DE", "001");
                        addAlternativeVAT(mwstSatzBezReduced, mwstsatzbezDtoDEReduced);

                    }

                    @Test
                    @Order(14)
                    @DisplayName("Test creation of new invoice item for customer DE with normal VAT when alternative VAT exists")
                    void testInvoiceItemDECustomerNormalVATWithAlternativeVAT() throws RemoteException {
                        KundeDto kundeDto = kundeFac.kundeFindByKundennummer(8003, theClientDto);
                        kundeDto = kundeFac.kundeFindByPrimaryKey(kundeDto.getIId(), theClientDto);
                        RechnungDto [] rechnungDtos = rechnungFac.rechnungFindByKundeIIdMandantCNr(kundeDto.getIId(), "001");
                        ParametermandantDto parameterDto = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_ARTIKEL, ParameterFac.PARAMETER_DEFAULT_LAGER);
                        LagerDto lagerDto = lagerfac.lagerFindByPrimaryKey(new Integer(parameterDto.getCWert()));

                        RechnungDto rechnungDto = null;
                        for (RechnungDto dto: rechnungDtos) {
                            if (dto.getCBestellnummer().equals("Rechnung DE")) {
                                rechnungDto = dto;
                            }
                        }
                        ArtikelDto artikelDto = artikelFac.artikelFindByCNr("NORMALER", theClientDto);
                        VkPreisfindungEinzelverkaufspreisDto vkPreisfindungEinzelverkaufspreisDto = vkPreisfindungFac.getArtikeleinzelverkaufspreis(artikelDto.getIId(), null, kundeDto.getCWaehrung(), theClientDto);

                        String sPositionsArt = LocaleFac.POSITIONSART_IDENT;
                        BigDecimal nMenge = BigDecimal.ONE;
                        String sEinheitCnr = SystemFac.EINHEIT_STUECK;
                        boolean bNettoPreisUebersteuert = false;
                        boolean bDrucken = true;

                        RechnungPositionDto rechnungPositionDto = createInvoiceItem(rechnungDto, artikelDto, kundeDto, vkPreisfindungEinzelverkaufspreisDto, lagerDto, sPositionsArt, nMenge, sEinheitCnr, bNettoPreisUebersteuert, bDrucken);

                        assertThat(rechnungFac.rechnungPositionFindByPrimaryKey(rechnungPositionDto.getIId()).getMwstsatzIId()).isEqualTo(
                                mandantFac.mwstsatzZuDatumClient(mandantFac.mwstsatzbezFindByBezeichnung("Allgemeine Waren DE", "001").getIId(), new Timestamp(System.currentTimeMillis()), theClientDto).getIId());

                    }

                    @Test
                    @Order(15)
                    @DisplayName("Test creation of new invoice item for customer DE with reduced VAT when alternative VAT exists")
                    void testInvoiceItemDECustomerReducedVATWithAlternativeVAT() throws RemoteException {
                        KundeDto kundeDto = kundeFac.kundeFindByKundennummer(8003, theClientDto);
                        kundeDto = kundeFac.kundeFindByPrimaryKey(kundeDto.getIId(), theClientDto);
                        RechnungDto [] rechnungDtos = rechnungFac.rechnungFindByKundeIIdMandantCNr(kundeDto.getIId(), "001");
                        ParametermandantDto parameterDto = parameterFac.getMandantparameter(theClientDto.getMandant(), ParameterFac.KATEGORIE_ARTIKEL, ParameterFac.PARAMETER_DEFAULT_LAGER);
                        LagerDto lagerDto = lagerfac.lagerFindByPrimaryKey(new Integer(parameterDto.getCWert()));
                        RechnungDto rechnungDto = null;
                        for (RechnungDto dto: rechnungDtos) {
                            if (dto.getCBestellnummer().equals("Rechnung DE")) {
                                rechnungDto = dto;
                            }
                        }
                        ArtikelDto artikelDto = artikelFac.artikelFindByCNr("REDUZIERT", theClientDto);
                        VkPreisfindungEinzelverkaufspreisDto vkPreisfindungEinzelverkaufspreisDto = vkPreisfindungFac.getArtikeleinzelverkaufspreis(artikelDto.getIId(), null, kundeDto.getCWaehrung(), theClientDto);

                        String sPositionsArt = LocaleFac.POSITIONSART_IDENT;
                        BigDecimal nMenge = BigDecimal.ONE;
                        String sEinheitCnr = SystemFac.EINHEIT_STUECK;
                        boolean bNettoPreisUebersteuert = false;
                        boolean bDrucken = true;

                        RechnungPositionDto rechnungPositionDto = createInvoiceItem(rechnungDto, artikelDto, kundeDto, vkPreisfindungEinzelverkaufspreisDto, lagerDto, sPositionsArt, nMenge, sEinheitCnr, bNettoPreisUebersteuert, bDrucken);

                        assertThat(rechnungFac.rechnungPositionFindByPrimaryKey(rechnungPositionDto.getIId()).getMwstsatzIId()).
                                isEqualTo(mandantFac.mwstsatzZuDatumClient(mandantFac.mwstsatzbezFindByBezeichnung("Reduzierte Waren DE", "001").getIId(), new Timestamp(System.currentTimeMillis()), theClientDto).getIId());
                    }

                    @Test
                    @Order(16)
                    @DisplayName("Create invoice item with customer IG with normal VAT when alternative VAT exist")
                    void testInvoiceItemIGCustomerNormalVATWithAlternativeVAT() throws RemoteException {
                        testInvoiceItemIGCustomerNormalVAT();
                    }

                    @Test
                    @Order(17)
                    @DisplayName("Create invoice item with customer IG with reduced VAT when alternative VAT exist")
                    void testInvoiceItemIGCustomerReducedVATWithAlternativeVAT() throws RemoteException {
                        testInvoiceItemIGCustomerReducedVAT();
                    }

                    @Test
                    @Order(18)
                    @DisplayName("Create Invoice item creation via Barcode Scanner with reduced VAT when alternative VAT exist")
                    void testBarcodeCreationInvoiceCustomerClientCountryWithAlternativeVAT() throws RemoteException {
                        KundeDto kundeDto = kundeFac.kundeFindByKundennummer(8003, theClientDto);
                        kundeDto = kundeFac.kundeFindByPrimaryKey(kundeDto.getIId(), theClientDto);
                        RechnungDto [] rechnungDtos = rechnungFac.rechnungFindByKundeIIdMandantCNr(kundeDto.getIId(), "001");
                        RechnungDto rechnungDto = null;
                        for (RechnungDto dto: rechnungDtos) {
                            if (dto.getCBestellnummer().equals("Rechnung DE")) {
                                rechnungDto = dto;
                            }
                        }

                        ArtikelDto artikelDto = artikelFac.artikelFindByCNr("REDUZIERT", theClientDto);

                        RechnungPositionDto rechnungPositionDto = new RechnungPositionDto();
                        rechnungPositionDto.setBelegIId(rechnungDto.getIId());
                        rechnungPositionDto.setArtikelIId(artikelDto.getIId());
                        rechnungPositionDto.setPositionsartCNr(LocaleFac.POSITIONSART_IDENT);
                        rechnungPositionDto.setNMenge(BigDecimal.ONE);
                        rechnungPositionDto.setEinheitCNr(artikelDto.getEinheitCNr());

                        MwstsatzDto mwstsatzDto = mandantFac.determineMWSTSatz(artikelDto, kundeDto, new Timestamp(System.currentTimeMillis()), theClientDto);

                        rechnungDto.setMwstsatzIId(mwstsatzDto.getIId());
                        rechnungPositionDto.setMwstsatzIId(mwstsatzDto.getIId());
                        RechnungPositionDto savedDto = rechnungFac.createRechnungPosition(rechnungPositionDto, rechnungDto.getLagerIId(), true, true, theClientDto);

                        RechnungPositionDto rechnungPositionDtoFromDB = rechnungFac.rechnungPositionFindByPrimaryKey(savedDto.getIId());

                        assertThat(rechnungPositionDtoFromDB).isNotNull();
                        assertThat(rechnungPositionDtoFromDB.getMwstsatzIId()).
                                isEqualTo(mandantFac.mwstsatzZuDatumClient(mandantFac.mwstsatzbezFindByBezeichnung("Reduzierte Waren DE", "001").getIId(), new Timestamp(System.currentTimeMillis()), theClientDto).getIId());

                    }



                }

            }

        }

    }
}

