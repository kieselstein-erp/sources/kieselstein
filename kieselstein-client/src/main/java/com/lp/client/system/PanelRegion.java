package com.lp.client.system;

import com.lp.client.frame.ExceptionLP;
import com.lp.client.frame.HelperClient;
import com.lp.client.frame.component.*;
import com.lp.client.frame.delegate.DelegateFactory;
import com.lp.client.pc.LPMain;
import com.lp.server.system.service.RegionDto;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.EventObject;

public class PanelRegion extends PanelBasis {

    private static final long serialVersionUID = 1L;

    private RegionDto regionDto = new RegionDto();

    private JPanel jPanelWorkingOn = new JPanel();
    private JPanel panelButtonAction = null;
    private Border border = null;
    private WrapperLabel wlaRegion = null;
    private WrapperTextField wtfRegion = null;
    private WrapperLabel wlaUmsatzziel = null;
    private WrapperNumberField wnfUmsatzziel = null;

    public PanelRegion(InternalFrame internalFrame, String add2TitleI, Object pk) throws Throwable {
        super(internalFrame, add2TitleI, pk);

        jbInit();
        initComponents();
        initPanel();
    }

    private void initPanel() {
    }

    public void eventActionNew(EventObject eventObject, boolean bLockMeI, boolean bNeedNoNewI) throws Throwable {
        super.eventActionNew(eventObject, true, false);
        setRegionDto(new RegionDto());
        leereAlleFelder(this);
    }
    protected void eventActionDelete(ActionEvent e, boolean bAdministrateLockKeyI, boolean bNeedNoDeleteI) throws Throwable {
        DelegateFactory.getInstance().getSystemDelegate().removeRegion(getRegionDto());
        super.eventActionDelete(e, false, false);
    }

    public void eventActionSave(ActionEvent e, boolean bNeedNoSaveI) throws Throwable {
        if (allMandatoryFieldsSetDlg()) {
            Integer iId = null;
            components2Dto();

            if (getRegionDto().getIID() == null) {
                iId = DelegateFactory.getInstance().getSystemDelegate().createRegion(getRegionDto());
                getRegionDto().setIID(iId);
                setKeyWhenDetailPanel(iId);
            } else {
                DelegateFactory.getInstance().getSystemDelegate().updateRegion(getRegionDto());
            }
            super.eventActionSave(e, true);
            eventYouAreSelected(false);
        }
    }

    public void eventYouAreSelected(boolean bNeedNoYouAreSelectedI) throws Throwable {
        super.eventYouAreSelected(false);

        Object key = getKeyWhenDetailPanel();

        if (key == null || key.equals(LPMain.getLockMeForNew())) {
            leereAlleFelder(this);
            clearStatusbar();
        } else {
            setRegionDto(DelegateFactory.getInstance().getSystemDelegate()
                    .regionFindByIId((Integer) key));
            dto2Components();
        }
    }

    protected void components2Dto() throws ExceptionLP {
        getRegionDto().setCNr(wtfRegion.getText());
        getRegionDto().setBdUmsatzziel(wnfUmsatzziel.getBigDecimal());
    }

    protected void dto2Components() throws ExceptionLP {
        wtfRegion.setText(getRegionDto().getCNr());
        wnfUmsatzziel.setBigDecimal(getRegionDto().getBdUmsatzziel());
    }

    private void jbInit() throws Throwable {
        String[] aWhichButtonIUse = { ACTION_UPDATE, ACTION_SAVE, ACTION_DELETE, ACTION_DISCARD, };

        enableToolsPanelButtons(aWhichButtonIUse);
        border = BorderFactory.createEmptyBorder(10, 10, 10, 10);
        setBorder(border);
        setLayout(new GridBagLayout());

        panelButtonAction = getToolsPanel();
        add(panelButtonAction, new GridBagConstraints(0, 0, 1, 1, 0.0,
                0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0), 0, 0));

        jPanelWorkingOn = new JPanel(new GridBagLayout());
        add(jPanelWorkingOn, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
                GridBagConstraints.SOUTHEAST, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));

        add(getPanelStatusbar(), new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
                0, 0, 0, 0), 0, 0));

        int row = 0;
        wlaRegion = new WrapperLabel(LPMain.getTextRespectUISPr("lp.region"));
        wtfRegion = new WrapperTextField();
        wtfRegion.setMandatoryField(true);

        jPanelWorkingOn.add(wlaRegion, new GridBagConstraints(0, row, 1,1,1,0,GridBagConstraints.WEST, GridBagConstraints.BOTH,
                new Insets(2,2,2,2), 0,0));
        jPanelWorkingOn.add(wtfRegion, new GridBagConstraints(1, row, 1,1,1,0,GridBagConstraints.EAST, GridBagConstraints.BOTH,
                new Insets(2,2,2,2), 0,0));

        row++;
        wlaUmsatzziel = new WrapperLabel(LPMain.getTextRespectUISPr("lp.umsatzziel"));
        wnfUmsatzziel = new WrapperNumberField();

        jPanelWorkingOn.add(wlaUmsatzziel, new GridBagConstraints(0, row, 1,1,1,0,GridBagConstraints.WEST, GridBagConstraints.BOTH,
                new Insets(2,2,2,2), 0,0));
        jPanelWorkingOn.add(wnfUmsatzziel, new GridBagConstraints(1, row, 1,1,1,0,GridBagConstraints.EAST, GridBagConstraints.BOTH,
                new Insets(2,2,2,2), 0,0));

    }

    public RegionDto getRegionDto() {
        return regionDto;
    }

    public void setRegionDto(RegionDto regionDto) {
        this.regionDto = regionDto;
    }

    protected String getLockMeWer() throws Exception {
        return HelperClient.LOCKME_REGION;
    }
}
