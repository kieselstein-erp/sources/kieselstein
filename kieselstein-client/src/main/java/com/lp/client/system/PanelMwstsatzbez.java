/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 * 
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 * 
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: developers@heliumv.com
 ******************************************************************************/
package com.lp.client.system;

import com.lp.client.frame.ExceptionLP;
import com.lp.client.frame.HelperClient;
import com.lp.client.frame.component.*;
import com.lp.client.frame.delegate.DelegateFactory;
import com.lp.client.frame.dialog.DialogFactory;
import com.lp.client.pc.LPMain;
import com.lp.server.system.service.MwstsatzbezDto;
import com.lp.util.EJBExceptionLP;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.EventObject;
import java.util.HashSet;
import java.util.Set;

public class PanelMwstsatzbez extends PanelBasis {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private InternalFrameSystem internalFrameSystem = null;
	private JPanel jpaWorkingOn = new JPanel();
	private JPanel jpaButtonAction = new JPanel();
	private Border border = null;
	private GridBagLayout gridBagLayoutWorkingPanel = new GridBagLayout();
	private GridBagLayout gridBagLayoutAll = new GridBagLayout();

	private WrapperLabel wlaBezeichnung = new WrapperLabel();
	private WrapperTextField wtfBezeichnung = new WrapperTextField();

	private WrapperSelectField wsfFinanzamt = new WrapperSelectField(
			WrapperSelectField.FINANZAMT, getInternalFrame(), true);

	private MwstsatzbezDto mwstsatzbezDto = null;

	private Set<MwstsatzbezDto> alternativeMwstsatzbezDtos = new HashSet<MwstsatzbezDto>();

	private WrapperLabel wlaAlternativeMwstBez = new WrapperLabel();
	private WrapperTextField wtfAlternativeMwstBez = new WrapperTextField();

	private WrapperButton wbuMwstbez = null;
	private PanelQueryFLR panelQueryFLRMwstbez = null;

	private final String ACTION_SPECIAL_ACLTERNATIVE_MWST = "ACTION_SPECIAL_ACLTERNATIVE_MWST";

	public InternalFrameSystem getInternalFrameSystem() {
		return internalFrameSystem;
	}

	public PanelMwstsatzbez(InternalFrame internalFrame, String add2TitleI,
							Object pk) throws Throwable {
		super(internalFrame, add2TitleI, pk);
		internalFrameSystem = (InternalFrameSystem) internalFrame;

		jbInit();
		setDefaults();
		initComponents();
	}

	protected JComponent getFirstFocusableComponent() throws Exception {
		return null;
	}

	public void eventYouAreSelected(boolean bNeedNoYouAreSelectedI)
			throws Throwable {

		super.eventYouAreSelected(false);

		Object key = getKeyWhenDetailPanel();

		if (key == null || key.equals(LPMain.getLockMeForNew())) {
			clearStatusbar();
		} else {
			mwstsatzbezDto = DelegateFactory.getInstance().getMandantDelegate().mwstsatzbezFindByPrimaryKey((Integer) key);
			toggleAlternativeMWST();
			dto2Components();
		}

	}


	protected void dto2Components() throws Throwable {

		wsfFinanzamt.setKey(mwstsatzbezDto.getFinanzamtIId());
		wtfBezeichnung.setText(mwstsatzbezDto.getCBezeichnung());

		alternativeMwstsatzbezDtos.clear();
		if (mwstsatzbezDto.getAlternativeMwst() != null) {
			alternativeMwstsatzbezDtos.addAll(mwstsatzbezDto.getAlternativeMwst());
		}

		refreshAlternativeMWSTBez();

	}

	private void jbInit() throws Throwable {
		border = BorderFactory.createEmptyBorder(10, 10, 0, 10);
		setBorder(border);
		// das Aussenpanel hat immer das Gridbaglayout.
		gridBagLayoutAll = new GridBagLayout();
		this.setLayout(gridBagLayoutAll);
		getInternalFrame().addItemChangedListener(this);

		// Actionpanel von Oberklasse holen und anhaengen.
		jpaButtonAction = getToolsPanel();
		this.setActionMap(null);
		jpaWorkingOn = new JPanel();
		gridBagLayoutWorkingPanel = new GridBagLayout();
		jpaWorkingOn.setLayout(gridBagLayoutWorkingPanel);

		wlaBezeichnung.setText(LPMain.getTextRespectUISPr("label.bezeichnung_lang"));
		wtfBezeichnung.setMandatoryField(true);

		wbuMwstbez = new WrapperButton();
		wbuMwstbez.setText(LPMain.getTextRespectUISPr("lp.alternativermwstsatz"));
		wbuMwstbez.setActionCommand(this.ACTION_SPECIAL_ACLTERNATIVE_MWST);
		wbuMwstbez.addActionListener(this);

		wlaAlternativeMwstBez.setText(LPMain.getTextRespectUISPr("lp.alternativermwstsatz"));
		wlaAlternativeMwstBez.setVisible(false);
		wtfAlternativeMwstBez.setVisible(false);
		wtfAlternativeMwstBez.setColumnsMax(256);
		wtfAlternativeMwstBez.setActivatable(false);
		wbuMwstbez.setVisible(false);

		this.add(jpaButtonAction, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,
				0, 0, 0), 0, 0));
		this.add(jpaWorkingOn, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH,
				new Insets(-9, 0, 9, 0), 0, 0));
		this.add(getPanelStatusbar(), new GridBagConstraints(0, 2, 1, 1, 1.0,
				0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));

		iZeile++;
		jpaWorkingOn.add(wlaBezeichnung, new GridBagConstraints(0, iZeile, 1,
				1, 0.1, 0.0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
		jpaWorkingOn.add(wtfBezeichnung, new GridBagConstraints(1, iZeile, 3,
				1, 0.3, 0.0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
		iZeile++;
		jpaWorkingOn.add(wsfFinanzamt.getWrapperButton(),
				new GridBagConstraints(0, iZeile, 1, 1, 0.1, 0.0,
						GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2),
						0, 0));
		jpaWorkingOn.add(wsfFinanzamt.getWrapperTextField(),
				new GridBagConstraints(1, iZeile, 3, 1, 0.3, 0.0,
						GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2),
						0, 0));
		iZeile++;
		jpaWorkingOn.add(wbuMwstbez,
				new GridBagConstraints(0, iZeile, 1, 1, 0.1, 0.0,
						GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2),
						0, 0));



		jpaWorkingOn.add(wtfAlternativeMwstBez,
				new GridBagConstraints(1, iZeile, 1, 1, 0.1, 0.0,
						GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2),
						0, 0));

		String[] aWhichButtonIUse = {ACTION_UPDATE, ACTION_SAVE,
				ACTION_DELETE, ACTION_DISCARD};

		enableToolsPanelButtons(aWhichButtonIUse);

	}

	public void eventActionNew(EventObject eventObject, boolean bLockMeI, boolean bNeedNoNewI) throws Throwable {

		super.eventActionNew(eventObject, true, false);
		leereAlleFelder(this);

		mwstsatzbezDto = new MwstsatzbezDto();
		alternativeMwstsatzbezDtos = new HashSet<>();


	}

	protected void eventActionSpecial(ActionEvent e) throws Throwable {
		if (e.getActionCommand().equals(ACTION_SPECIAL_ACLTERNATIVE_MWST)) {

			if (wsfFinanzamt.getIKey() == null) {
				DialogFactory.showModalDialog(LPMain.getTextRespectUISPr("lp.error"), LPMain.getTextRespectUISPr("lp.error.finazamt.missing"));
			} else {
				components2Dto();
				boolean is_valid = DelegateFactory.getInstance().getMandantDelegate().isMwstSameCountryAsFA(mwstsatzbezDto);
				if (is_valid) {
					Integer finanzamt_id_id = mwstsatzbezDto.getFinanzamtIId();
					panelQueryFLRMwstbez = SystemFilterFactory
							.getInstance()
							.createPanelFLRAlternativeMwstbez(getInternalFrame(), true, null, finanzamt_id_id);

					panelQueryFLRMwstbez.setMultipleRowSelectionEnabled(true);
					panelQueryFLRMwstbez.addButtonAuswahlBestaetigen(null);
					new DialogQuery(panelQueryFLRMwstbez);
				} else {
					DialogFactory.showModalDialog(LPMain.getTextRespectUISPr("lp.warning"), LPMain.getTextRespectUISPr("lp.warning.finanzamt.not.valid.for.alternativemwst"));
				}
			}
		}
	}

	protected String getLockMeWer() throws Exception {
		return HelperClient.LOCKME_MWSTSATZBEZ;
	}

	protected void setDefaults() throws Throwable {

	}

	protected void components2Dto() throws Throwable {
		mwstsatzbezDto.setCBezeichnung(wtfBezeichnung.getText());
		mwstsatzbezDto.setMandantCNr(LPMain.getTheClient().getMandant());
		mwstsatzbezDto.setFinanzamtIId(wsfFinanzamt.getIKey());

		if (!alternativeMwstsatzbezDtos.isEmpty()) {
			mwstsatzbezDto.setAlternativeMwst(alternativeMwstsatzbezDtos);
		} else {
			mwstsatzbezDto.setAlternativeMwst(null);
		}
	}

	protected void eventActionDelete(ActionEvent e,
									 boolean bAdministrateLockKeyI, boolean bNeedNoDeleteI)
			throws Throwable {
		DelegateFactory.getInstance().getMandantDelegate()
				.removeMwstsatzbez(mwstsatzbezDto);
		super.eventActionDelete(e, true, true);
	}

	public void eventActionSave(ActionEvent e, boolean bNeedNoSaveI) throws Throwable {
		if (allMandatoryFieldsSetDlg()) {

			components2Dto();

			if (mwstsatzbezDto.getIId() == null) {
				mwstsatzbezDto
						.setIId(DelegateFactory.getInstance()
								.getMandantDelegate()
								.createMwstsatzbez(mwstsatzbezDto));
				setKeyWhenDetailPanel(mwstsatzbezDto.getIId());

			} else {
				DelegateFactory.getInstance().getMandantDelegate()
						.updateMwstsatzbez(mwstsatzbezDto);

			}
			super.eventActionSave(e, true);

			if (getInternalFrame().getKeyWasForLockMe() == null) {
				getInternalFrame().setKeyWasForLockMe(
						mwstsatzbezDto.getIId().toString());
			}
			eventYouAreSelected(false);

		}
	}

	private void refreshAlternativeMWSTBez() {
		StringBuilder alternativeMwstBez = new StringBuilder();
		if (alternativeMwstsatzbezDtos != null) {
			for (MwstsatzbezDto dto: alternativeMwstsatzbezDtos) {
				if (alternativeMwstBez.length() > 0) {
					alternativeMwstBez.append("|");
				}
				alternativeMwstBez.append(dto.getCBezeichnung());
			}
		}

		wtfAlternativeMwstBez.setText(alternativeMwstBez.toString());
	}

	protected void eventItemchanged(EventObject eI) throws Throwable {
		ItemChangedEvent e = (ItemChangedEvent) eI;
		if (e.getID() == ItemChangedEvent.ACTION_LEEREN) {
			if (e.getSource() == panelQueryFLRMwstbez) {
				wtfAlternativeMwstBez.setText("");
				alternativeMwstsatzbezDtos.clear();
			}
		} else if (e.getID() == ItemChangedEvent.GOTO_DETAIL_PANEL) {
			if (e.getSource() == panelQueryFLRMwstbez) {
				Object[] o = panelQueryFLRMwstbez.getSelectedIds();

				Object key = ((ISourceEvent) e.getSource()).getIdSelected();

				boolean isValid = false;
				try {
					isValid = DelegateFactory.getInstance().getMandantDelegate().isValidAlternativeMwstBezeichnung(mwstsatzbezDto, o);
				} catch (ExceptionLP ex1) {
					DialogFactory.showModalDialog(LPMain.getTextRespectUISPr("lp.warning"),  LPMain.getInstance().getMsg(ex1));
					return;
				} catch (Exception ex2) {
					DialogFactory.showModalDialog(LPMain.getTextRespectUISPr("lp.warning"), ex2.getMessage());
					return;
				}

				if (isValid) {

					for (int i = 0; i < o.length; i++) {
						MwstsatzbezDto mwstsatzbezDto1 = DelegateFactory.getInstance().getMandantDelegate().mwstsatzbezFindByPrimaryKey((Integer) o[i]);
						alternativeMwstsatzbezDtos.add(mwstsatzbezDto1);
					}

					refreshAlternativeMWSTBez();

					if (panelQueryFLRMwstbez != null && panelQueryFLRMwstbez.getDialog() != null) {
						panelQueryFLRMwstbez.getDialog().setVisible(false);
					}

                }
            } else if (e.getSource() == wsfFinanzamt.getPanelQueryFLR()) {
				if (mwstsatzbezDto.getFinanzamtIId() != null) {
					int previous_id = mwstsatzbezDto.getFinanzamtIId();
					boolean isAnAlternative = true;
					try {
						isAnAlternative = DelegateFactory.getInstance().getMandantDelegate().isMWSTAnAlternative(mwstsatzbezDto);
					} catch (ExceptionLP ex1) {
						DialogFactory.showModalDialog(LPMain.getTextRespectUISPr("lp.warning"), LPMain.getInstance().getMsg(ex1));
					}

					if (isAnAlternative) {
						wsfFinanzamt.setKey(previous_id);
					} else if (!alternativeMwstsatzbezDtos.isEmpty()) {
						DialogFactory.showModalDialog(LPMain.getTextRespectUISPr("lp.warning"), LPMain.getTextRespectUISPr("lp.warning.alternativemwst.already.selected"));
						wsfFinanzamt.setKey(previous_id);
					} else {
						wsfFinanzamt.setKey(wsfFinanzamt.getPanelQueryFLR().getSelectedId());
						components2Dto();
						toggleAlternativeMWST();
					}
				} else {
					wsfFinanzamt.setKey(wsfFinanzamt.getPanelQueryFLR().getSelectedId());
					components2Dto();
					toggleAlternativeMWST();
				}

			}
		}
	}

	private void toggleAlternativeMWST() throws Throwable {
		if (DelegateFactory.getInstance().getMandantDelegate().isMwstSameCountryAsFA(mwstsatzbezDto)) {
			wlaAlternativeMwstBez.setVisible(true);
			wtfAlternativeMwstBez.setVisible(true);
			wbuMwstbez.setVisible(true);
		} else {
			wlaAlternativeMwstBez.setVisible(false);
			wtfAlternativeMwstBez.setVisible(false);
			wbuMwstbez.setVisible(false);
		}
	}


}


