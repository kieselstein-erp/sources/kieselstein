package com.lp.client.fertigung;

import com.lp.client.frame.component.WrapperTextArea;
import com.lp.client.frame.delegate.DelegateFactory;
import com.lp.client.pc.LPMain;
import com.lp.server.fertigung.service.ImportPruefergebnis;
import org.jruby.RubyProcess;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.EventObject;

public class DialogLosTerminImport extends JDialog implements ActionListener {
    private byte[] file;
    private JTabbedPane parent;

    private JButton btnCancel = new JButton();
    private JButton btnImport = new JButton();

    private GridBagLayout gbl = new GridBagLayout();

    private ImportPruefergebnis importPruefergebnis = null;

    private WrapperTextArea wtaFehler = new WrapperTextArea();
    private JScrollPane jspScrollPane = new JScrollPane();

    public DialogLosTerminImport(byte[] file, JTabbedPane parent) throws Throwable {
        super(LPMain.getInstance().getDesktop(), LPMain.getTextRespectUISPr("fert.import.lostermin.title"), true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                setVisible(false);
                dispose();
            }
        });

        this.file = file;
        this.parent = parent;

        setSize(500, 500);

        jbInit();
        pack();
    }

    private void jbInit() throws Throwable {
        btnImport.setText(LPMain.getInstance().getTextRespectUISPr("fert.verbrauchsartikel.import.importieren"));
        btnImport.addActionListener(this);
        btnCancel.setText(LPMain.getInstance().getTextRespectUISPr(
                "lp.abbrechen"));
        btnCancel.addActionListener(this);

        importPruefergebnis = DelegateFactory.getInstance()
                .getFertigungDelegate().validateTerminImportFile(this.file);

        ArrayList<String> alFehler = importPruefergebnis.getAlFehler();

        StringBuilder result = new StringBuilder();
        if (!alFehler.isEmpty()) {
            btnImport.setEnabled(false);
            result.append(LPMain.getInstance().getTextRespectUISPr(
                    "fert.xlsimport.fehler")).append(System.lineSeparator()).append(System.lineSeparator());

            for (String s : alFehler) {
                result.append(s).append(System.lineSeparator());
            }
        }
        result.append(System.lineSeparator());
        ArrayList<String> warnings = importPruefergebnis.getAlWarnung();
        if (!warnings.isEmpty()) {
            result.append(LPMain.getInstance().getTextRespectUISPr(
                    "fert.xlsimport.warnung")).append(System.lineSeparator())
                    .append(System.lineSeparator());

            for (String s : warnings) {
                result.append(s).append(System.lineSeparator());
            }
        }

        if (alFehler.isEmpty()) {
            result.append(System.lineSeparator());
            result.append(LPMain.getTextRespectUISPr("fert.import.lostermin.noerrors"));
        }

        wtaFehler.setText(result.toString());

        this.getContentPane().setLayout(gbl);

        add(jspScrollPane,  new GridBagConstraints(0, 0, 2,
                1, 1.0, 1.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 2), 0, 0));
        jspScrollPane.getViewport().add(wtaFehler, null);

        add(btnImport,new GridBagConstraints(0, 4,
                1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 100, 0));
        add(btnCancel, new GridBagConstraints(1, 4, 1,
                1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 100, 0));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        final Object src = e.getSource();
        if (src.equals(btnImport)) {
            try {
                DelegateFactory.getInstance().getFertigungDelegate()
                        .importiereTerminLoseXLS(importPruefergebnis);
                this.setVisible(false);
            } catch (Throwable ex) {
                throw new RuntimeException(ex);
            }
        } else if (src.equals(btnCancel)) {
            this.setVisible(false);
        }
    }
}
