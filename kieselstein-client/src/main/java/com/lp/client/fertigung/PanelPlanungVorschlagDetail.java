/*******************************************************************************
 * kieselsteinERP, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2023 - 2024 Kieselstein ERP eG, X-Net Services GmbH
 * based on: HELIUM V, Copyright (C) 2004 - 2022 HELIUM V IT-Solutions GmbH
 * <p>
 * distributed under the terms of GNU Affero General Public License, version 3
 * with supplements. See LICENSE file in base source directory for details.
 * <p>
 * Contact: help@kieselstein-erp.org
 ******************************************************************************/
package com.lp.client.fertigung;

import com.lp.client.frame.HelperClient;
import com.lp.client.frame.component.*;
import com.lp.client.frame.delegate.DelegateFactory;
import com.lp.client.pc.LPMain;
import com.lp.editor.util.TextBlockOverflowException;
import com.lp.server.fertigung.service.LosPlanungvorschlagDto;
import com.lp.server.system.service.SystemFac;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyVetoException;
import java.util.EventObject;


public class PanelPlanungVorschlagDetail extends PanelBasis {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final TabbedPanePlanungVorschlag tabbedPane;

	private WrapperSpinner wspRuestzeitStunden = null;
	private WrapperSpinner wspRuestzeitMinuten = null;
	private WrapperSpinner wspRuestzeitSekunden = null;
	private WrapperSpinner wspRuestzeitMillisekunden = null;


	private WrapperSpinner wspStueckzeitStunden = null;
	private WrapperSpinner wspStueckzeitMinuten = null;
	private WrapperSpinner wspStueckzeitSekunden = null;
	private WrapperSpinner wspStueckzeitMillisekunden = null;

	private WrapperSelectField wsfMaschinenAuswahl = null;

	private WrapperSpinner wspReihung = null;

	private WrapperEditorField wefText = null;

	private LosPlanungvorschlagDto dto = null;

	public PanelPlanungVorschlagDetail(InternalFrame internalFrame, String add2TitleI, Object key,
			TabbedPanePlanungVorschlag tabbedPane) throws Throwable {
		super(internalFrame, add2TitleI, key);
		this.tabbedPane = tabbedPane;

		jbInit();
		initComponents();
	}

	private TabbedPanePlanungVorschlag getTabbedPane() {
		return tabbedPane;
	}

	private JPanel jbInitTimePanel() {
		JPanel timePanel = new JPanel(new MigLayout("flowy, wrap 3", "[20%][20%][20%][20%]"));

		WrapperLabel wlaUeberschriftZeitStd = new WrapperLabel(LPMain.getTextRespectUISPr("stkl.arbeitsplan.std"));
		WrapperLabel wlaUeberschriftZeitMin = new WrapperLabel(LPMain.getTextRespectUISPr("stkl.arbeitsplan.min"));
		WrapperLabel wlaUeberschriftZeitSek = new WrapperLabel(LPMain.getTextRespectUISPr("stkl.arbeitsplan.sek"));

		WrapperLabel wlaRuestzeit = new WrapperLabel(LPMain.getTextRespectUISPr("stkl.arbeitsplan.ruestzeit"));
		WrapperLabel wlaStueckzeit = new WrapperLabel(LPMain.getTextRespectUISPr("stkl.arbeitsplan.stueckzeit"));

		wspRuestzeitStunden = new WrapperSpinner(0, 0, 50000, 1);
		wspRuestzeitMinuten = new WrapperSpinner(0, 0, 59, 1);
		wspRuestzeitSekunden = new WrapperSpinner(0, 0, 59, 1);
		wspRuestzeitMillisekunden = new WrapperSpinner(0, 0, 999, 1);

		wspStueckzeitStunden = new WrapperSpinner(0, 0, 50000, 1);
		wspStueckzeitMinuten = new WrapperSpinner(0, 0, 59, 1);
		wspStueckzeitSekunden = new WrapperSpinner(0, 0, 59, 1);
		wspStueckzeitMillisekunden = new WrapperSpinner(0, 0, 999, 1);

		timePanel.add(new WrapperLabel(), "growx");
		timePanel.add(wlaRuestzeit, "growx");
		timePanel.add(wlaStueckzeit, "growx");

		timePanel.add(wlaUeberschriftZeitStd, "growx");
		timePanel.add(wspRuestzeitStunden, "growx");
		timePanel.add(wspStueckzeitStunden, "growx");

		timePanel.add(wlaUeberschriftZeitMin, "growx");
		timePanel.add(wspRuestzeitMinuten, "growx");
		timePanel.add(wspStueckzeitMinuten, "growx");

		timePanel.add(wlaUeberschriftZeitSek, "growx");
		timePanel.add(wspRuestzeitSekunden, "growx");
		timePanel.add(wspStueckzeitSekunden, "growx");

		WrapperLabel wlaUeberschriftZeitMillis = new WrapperLabel(LPMain.getTextRespectUISPr("stkl.arbeitsplan.millisek"));

		timePanel.add(wlaUeberschriftZeitMillis, "growx");
		timePanel.add(wspRuestzeitMillisekunden, "growx");
		timePanel.add(wspStueckzeitMillisekunden, "growx");

		return timePanel;
	}

	private JPanel jbInitMachineAndReihungPanel() {
		JPanel machinePanel = new JPanel(new GridBagLayout());
		machinePanel.add(this.jbInitMachinePanel(), new GridBagConstraints(1, 0, 1, 1, 1, 0.0,
						GridBagConstraints.CENTER, GridBagConstraints.BOTH,
						new Insets(0, 0, 0, 0), 1, 0));
		machinePanel.add(this.jbInitReihungPanel(), new GridBagConstraints(2, 0, 2, 1, 1, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));

		return machinePanel;
	}

	private JPanel jbInitMachinePanel() {
		JPanel machinePanel = new JPanel(new GridBagLayout());
		wsfMaschinenAuswahl = new WrapperSelectField(WrapperSelectField.MASCHINE, getInternalFrame(), false);

		wsfMaschinenAuswahl.setText(LPMain.getTextRespectUISPr("lp.maschine") + "...");
		wsfMaschinenAuswahl.setMandatoryField(true);
		wsfMaschinenAuswahl.setActivatable(true);

		machinePanel.add(wsfMaschinenAuswahl.getWrapperButton(),
				new GridBagConstraints(1, 0, 1, 1, 1, 0.0,
						GridBagConstraints.CENTER, GridBagConstraints.BOTH,
						new Insets(0, 0, 0, 0), 0, 0));
		machinePanel.add(wsfMaschinenAuswahl.getWrapperTextField(),
				new GridBagConstraints(2, 0, 2, 1, 1, 0.0,
						GridBagConstraints.CENTER, GridBagConstraints.BOTH,
						new Insets(0, 0, 0, 0), 0, 0));

		return machinePanel;
	}

	private JPanel jbInitReihungPanel() {
		JPanel ReihungPanel = new JPanel(new GridBagLayout());
		wspReihung = new WrapperSpinner(0, 0, 50000, 1);

		ReihungPanel.add(new WrapperLabel(LPMain.getTextRespectUISPr("lp.reihung")),
				new GridBagConstraints(1, 0, 1, 1, 1, 0.0,
						GridBagConstraints.CENTER, GridBagConstraints.BOTH,
						new Insets(0, 0, 0, 0), 0, 0));
		ReihungPanel.add(wspReihung,
				new GridBagConstraints(2, 0, 2, 1, 1, 0.0,
						GridBagConstraints.CENTER, GridBagConstraints.BOTH,
						new Insets(0, 0, 0, 0), 0, 0));

		return ReihungPanel;
	}

	private JPanel jbInitCLosTextPanel() throws Throwable {
		JPanel cLosTextPanel = new JPanel();
		cLosTextPanel.setLayout(new MigLayout("wrap 5, hidemode 0", "[25%][30%][15%][25%][25%]"));
		WrapperLabel wlaText = new WrapperLabel(LPMain.getTextRespectUISPr("los.kopfdaten.text"));
		wefText = new WrapperEditorFieldTexteingabe(getInternalFrame(),
				LPMain.getTextRespectUISPr("los.kopfdaten.text"));
		wefText.getLpEditor().getTextBlockAttributes(-1).capacity = SystemFac.MAX_LAENGE_EDITORTEXT_WENN_NTEXT;

		cLosTextPanel.add(wlaText, "growx");
		cLosTextPanel.add(wefText, "grow, span");


		return cLosTextPanel;
	}

	private void jbInit() throws Throwable {
		this.setLayout(new GridBagLayout());
		String[] aWhichButtonIUse = { ACTION_UPDATE, ACTION_SAVE, ACTION_DELETE, ACTION_DISCARD };
		this.enableToolsPanelButtons(aWhichButtonIUse);

		iZeile++;
		this.add(this.getToolsPanel(), new GridBagConstraints(0, iZeile, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

		iZeile++;
		this.add(this.jbInitTimePanel(), new GridBagConstraints(0, iZeile, 1, 1, 1.0, 0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

		iZeile++;
		this.add(this.jbInitMachineAndReihungPanel(), new GridBagConstraints(0, iZeile, 1, 1, 1.0, 0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));


		iZeile++;
		this.add(this.jbInitCLosTextPanel(), new GridBagConstraints(0, iZeile, 1,1,1.0,1.0, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

		iZeile++;
		this.add(this.getPanelStatusbar(), new GridBagConstraints(0, iZeile, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

		getInternalFrame().addItemChangedListener(this);

	}

	protected PropertyVetoException eventActionVetoableChangeLP() throws Throwable {
		PropertyVetoException pve = super.eventActionVetoableChangeLP();
		DelegateFactory.getInstance().getFertigungDelegate().removeLockDerInternenBestellungWennIchIhnSperre();
		return pve;
	}

	public void eventActionSave(ActionEvent e, boolean bNeedNoSaveI) throws Throwable {
		if (allMandatoryFieldsSetDlg()) {
			components2Dto();
			DelegateFactory.getInstance().getFertigungDelegate().changePlanningSuggestion(
					dto.getIId(), dto.getMaschineIId(), dto.getiReihung(), dto.getlRuestzeit(), dto.getlStueckzeit(), dto.getcLosText()
			);
			this.getTabbedPane().refreshPanelQueryForID(dto.getIId());
			super.eventActionSave(e, false);
			eventYouAreSelected(false);
		}
	}

	protected void eventActionDelete(ActionEvent e, boolean bAdministrateLockKeyI, boolean bNeedNoDeleteI)
			throws Throwable {
		getTabbedPane().deleteSelectedEntry();
	}

	private Long getTimeValueFromComponents(WrapperSpinner wspHours, WrapperSpinner wspMinutes, WrapperSpinner wspSeconds, WrapperSpinner wspMilliSeconds) {
		long lValue = (long)wspMilliSeconds.getInteger();
		lValue += (long)wspSeconds.getInteger() * 1000;
		lValue += (long)wspMinutes.getInteger() * 60 * 1000;
		lValue += (long)wspHours.getInteger() * 60 * 60 * 1000;
		return lValue;
	}

	/**
	 * Die eingegebenen Daten in ein Dto schreiben
	 *
	 */
	private void components2Dto() throws TextBlockOverflowException {
		if (dto != null) {
			dto.setMaschineIId(wsfMaschinenAuswahl.getIKey());
			dto.setiReihung(wspReihung.getInteger());
			dto.setlRuestzeit(getTimeValueFromComponents(wspRuestzeitStunden, wspRuestzeitMinuten, wspRuestzeitSekunden, wspRuestzeitMillisekunden));
			dto.setlStueckzeit(getTimeValueFromComponents(wspStueckzeitStunden, wspStueckzeitMinuten, wspStueckzeitSekunden, wspStueckzeitMillisekunden));
			dto.setcLosText(wefText.getText());
		}
	}

	private void setTimeValue(Long timeValue, WrapperSpinner wspHours, WrapperSpinner wspMinutes,
							  WrapperSpinner wspSeconds, WrapperSpinner wspMilliSeconds) {
		if (timeValue != null && timeValue > 0) {
			long lValue = timeValue;
			double hours = (double) lValue / 3600000;
			if (hours != 0) {
				lValue = lValue % 3600000;
			}
			double minutes = (double) lValue / 60000;
			if (minutes != 0) {
				lValue = lValue % 60000;
			}
			double seconds = (double) lValue / 1000;
			if (seconds != 0) {
				lValue = lValue % 1000;
			}
			double milliSeconds = lValue;

			wspHours.setInteger((int) hours);
			wspMinutes.setInteger((int) minutes);
			wspSeconds.setInteger((int) seconds);
			wspMilliSeconds.setInteger((int) milliSeconds);
		} else {
			wspHours.setInteger(0);
			wspMinutes.setInteger(0);
			wspSeconds.setInteger(0);
			wspMilliSeconds.setInteger(0);
		}
	}
	/**
	 * Ein Dto-Objekt ins Panel uebertragen
	 *
     */
	private void dto2Components(LosPlanungvorschlagDto dto) throws Throwable {
		this.setTimeValue(dto.getlRuestzeit(), wspRuestzeitStunden, wspRuestzeitMinuten, wspRuestzeitSekunden, wspRuestzeitMillisekunden);
		this.setTimeValue(dto.getlStueckzeit(), wspStueckzeitStunden, wspStueckzeitMinuten, wspStueckzeitSekunden, wspStueckzeitMillisekunden);
		this.wsfMaschinenAuswahl.setKey(dto.getMaschineIId());
		this.wspReihung.setInteger(dto.getiReihung());
		this.wefText.setText(dto.getcLosText());

		this.setStatusbarPersonalIIdAnlegen(dto.getPersonalIIdAnlegen());
		this.setStatusbarTAnlegen(dto.getTAnlegen());
		this.setStatusbarPersonalIIdAendern(dto.getPersonalIIdAendern());
		this.setStatusbarTAendern(dto.getTAendern());
	}

	protected String getLockMeWer() {
		return HelperClient.LOCKME_FERT_PLANUNGVORSCHLAG;
	}

	public void eventYouAreSelected(boolean bNeedNoYouAreSelectedI) throws Throwable {
		super.eventYouAreSelected(bNeedNoYouAreSelectedI);
		if (!bNeedNoYouAreSelectedI) {
			dto = getTabbedPane().getPlanungvorschlagDto();
			if (dto != null) {
				dto2Components(dto);
			} else {
				this.leereAlleFelder(this);
			}
		}
	}

	protected JComponent getFirstFocusableComponent() throws Exception {
		return wspRuestzeitStunden;
	}
}
