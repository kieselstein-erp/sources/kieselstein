/*******************************************************************************
 * kieselsteinERP, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2023 - 2024 Kieselstein ERP eG, X-Net Services GmbH
 * based on: HELIUM V, Copyright (C) 2004 - 2022 HELIUM V IT-Solutions GmbH
 * <p>
 * distributed under the terms of GNU Affero General Public License, version 3
 * with supplements. See LICENSE file in base source directory for details.
 * <p>
 * Contact: help@kieselstein-erp.org
 ******************************************************************************/
package com.lp.client.fertigung;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.Date;
import java.util.Calendar;

import com.lp.client.frame.component.*;
import com.lp.client.frame.delegate.DelegateFactory;
import com.lp.client.pc.LPMain;
import com.lp.server.util.fastlanereader.service.query.FilterKriterium;

import static com.lp.server.fertigung.service.FertigungFac.*;

/**
 * <code>PanelDialogKriterien</code> to enter the Criteria
 * for generation new Planning Suggestion entries.
 * @author DSk
 */
public class PanelDialogKriterienPlanungVorschlag extends
        PanelDialogKriterien {

    private static final long serialVersionUID = 1L;

    private WrapperDateField wdfPlannungBeginn = null;

    private WrapperDateField wdfLosBeginnBis = null;

    private WrapperCheckBox wcbMaterialKomplett = null;

    private WrapperSelectField wsfMaschinenGruppenAuswahl = new WrapperSelectField(WrapperSelectField.MASCHINE_GRUPPE, getInternalFrame(), false);

    private WrapperSelectField wsfLosstatusAuswahl = new WrapperSelectField(WrapperSelectField.LOS_STATUS, getInternalFrame(), false);


    public PanelDialogKriterienPlanungVorschlag(
            InternalFrame oInternalFrameI, String title)
            throws Throwable {
        super(oInternalFrameI, title);

        try {
            jbInit();
            setDefaults();
            initComponents();
        } catch (Throwable t) {
            LPMain.getInstance().exitFrame(getInternalFrame());
        }
    }

    private void jbInit() {

        WrapperLabel wlaLosBeginnBis = new WrapperLabel();
        wlaLosBeginnBis.setText(LPMain.getTextRespectUISPr("fert.los.planungsvorschlag.max.los.begin"));
        wdfPlannungBeginn = new WrapperDateField();
        wdfPlannungBeginn.setMandatoryField(true);
        wdfLosBeginnBis = new WrapperDateField();
        wdfLosBeginnBis.setMandatoryField(true);
        wcbMaterialKomplett = new WrapperCheckBox(LPMain.getTextRespectUISPr("fert.los.planungsvorschlag.material.komplett"));

        wsfMaschinenGruppenAuswahl.setText(LPMain.getTextRespectUISPr("pers.maschinengruppe") + "...");
        wsfMaschinenGruppenAuswahl.setMandatoryField(true);

        wsfLosstatusAuswahl.setText(LPMain.getTextRespectUISPr("fert.los.planungsvorschlag.min.losstatus") + "...");
        wsfLosstatusAuswahl.setMandatoryField(true);
        FilterKriterium[] statusFilter = WrapperSelectField.createInFilterCriteria(
                "status_c_nr", new String[] {STATUS_ANGELEGT, STATUS_AUSGEGEBEN, STATUS_IN_PRODUKTION, STATUS_TEILERLEDIGT }
        );
        wsfLosstatusAuswahl.setFilterCriteria(statusFilter);

        iZeile++;
        jpaWorkingOn.add(new WrapperLabel(LPMain.getTextRespectUISPr("fert.los.planungsvorschlag.plan.begin")), new GridBagConstraints(1, iZeile,
                1, 1, 0.1, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));

        jpaWorkingOn.add(wdfPlannungBeginn, new GridBagConstraints(2, iZeile,
                1, 1, 0.1, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));

        iZeile++;
        jpaWorkingOn.add(wsfLosstatusAuswahl.getWrapperButton(),
                new GridBagConstraints(1, iZeile, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(2, 2, 2, 2), 0, 0));
        jpaWorkingOn.add(wsfLosstatusAuswahl.getWrapperTextField(),
                new GridBagConstraints(2, iZeile, 2, 1, 0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(2, 2, 2, 2), 0, 0));

        iZeile++;
        jpaWorkingOn.add(wlaLosBeginnBis, new GridBagConstraints(1, iZeile,
                1, 1, 0.1, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));

        jpaWorkingOn.add(wdfLosBeginnBis, new GridBagConstraints(2, iZeile,
                1, 1, 0.1, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));

        iZeile++;
        jpaWorkingOn.add(wcbMaterialKomplett, new GridBagConstraints(1, iZeile,
                1, 1, 0.1, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));

        iZeile++;
        jpaWorkingOn.add(wsfMaschinenGruppenAuswahl.getWrapperButton(),
                new GridBagConstraints(1, iZeile, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(2, 2, 2, 2), 0, 0));
        jpaWorkingOn.add(wsfMaschinenGruppenAuswahl.getWrapperTextField(),
                new GridBagConstraints(2, iZeile, 2, 1, 0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(2, 2, 2, 2), 0, 0));

    }

    public void setDefaults() throws Throwable {
        Calendar cal = Calendar.getInstance();
        if (wdfPlannungBeginn.getDate() == null) {
            // Init Plannungsbeginn with Today.
            wdfPlannungBeginn.setDate(cal.getTime());
        }

        cal.add(Calendar.MONTH, 3);
        wdfLosBeginnBis.setDate(cal.getTime());

        if (wsfLosstatusAuswahl.getSKey() == null) {
            wsfLosstatusAuswahl.setKey(STATUS_AUSGEGEBEN);
        }
    }

    public Date getLosBeginnBis() {
        return wdfLosBeginnBis.getDate();
    }

    protected void eventActionSpecial(ActionEvent e) throws Throwable {
        if (e.getActionCommand().equals(ACTION_SPECIAL_OK) && wsfMaschinenGruppenAuswahl.getIKey() == null) {
            //Pflichtfelder
            showDialogPflichtfelderAusfuellen();
            return;
        }
        else if (e.getActionCommand().equals(ACTION_SPECIAL_OK)) {
            // Generate the Planning Suggestions.
            DelegateFactory.getInstance().getFertigungDelegate().generatePlanningSuggestions(
                    wsfMaschinenGruppenAuswahl.getIKey(), wcbMaterialKomplett.isSelected(),
                    wdfPlannungBeginn.getDate(), getLosBeginnBis(), wsfLosstatusAuswahl.getSKey()
            );
        }
        super.eventActionSpecial(e);
    }

}
