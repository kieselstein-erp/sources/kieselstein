/*******************************************************************************
 * kieselsteinERP, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2023 - 2024 Kieselstein ERP eG, X-Net Services GmbH
 * based on: HELIUM V, Copyright (C) 2004 - 2022 HELIUM V IT-Solutions GmbH
 * <p>
 * distributed under the terms of GNU Affero General Public License, version 3
 * with supplements. See LICENSE file in base source directory for details.
 * <p>
 * Contact: help@kieselstein-erp.org
 ******************************************************************************/
package com.lp.client.fertigung;

import com.lp.client.frame.component.*;
import com.lp.client.frame.delegate.DelegateFactory;
import com.lp.client.pc.LPMain;
import com.lp.server.util.fastlanereader.service.query.FilterKriterium;

import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * <code>PanelDialogKriterien</code> to enter the Criteria
 * to save the Planning Suggestion values back to the "Los".
 * @author DSk
 */
public class PanelDialogKriterienSavePlanungVorschlagTermine extends
        PanelDialogKriterien {


    private WrapperSelectField wsfMaschinenAuswahl = new WrapperSelectField(WrapperSelectField.MASCHINE, getInternalFrame(), false);


    public PanelDialogKriterienSavePlanungVorschlagTermine(
            InternalFrame oInternalFrameI, String title)
            throws Throwable {
        super(oInternalFrameI, title);

        try {
            jbInit();
            initComponents();
        } catch (Throwable t) {
            LPMain.getInstance().exitFrame(getInternalFrame());
        }
    }

    private void jbInit() {
        wsfMaschinenAuswahl.setText(LPMain.getTextRespectUISPr("lp.maschine") + "...");

        iZeile++;
        jpaWorkingOn.add(wsfMaschinenAuswahl.getWrapperButton(),
                new GridBagConstraints(1, iZeile, 1, 1, 1, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(2, 2, 2, 2), 0, 0));
        jpaWorkingOn.add(wsfMaschinenAuswahl.getWrapperTextField(),
                new GridBagConstraints(2, iZeile, 2, 1, 1, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(2, 2, 2, 2), 0, 0));

    }

    protected void eventActionSpecial(ActionEvent e) throws Throwable {
        if (e.getActionCommand().equals(ACTION_SPECIAL_OK)) {
            // Generate the Planning Suggestions.
            DelegateFactory.getInstance().getFertigungDelegate().savePlanningSuggestionsLosTermine(
                    wsfMaschinenAuswahl.getIKey()
            );
        }
        super.eventActionSpecial(e);
    }

    public void initMaschineAuswahlValues() throws Throwable {
        FilterKriterium[] maschineIDs = WrapperSelectField.createInFilterCriteria(
                "i_id", DelegateFactory.getInstance().getFertigungDelegate().getAllPlanningSuggestionMaschineIDs()
        );
        wsfMaschinenAuswahl.setFilterCriteria(maschineIDs);
    }

}
