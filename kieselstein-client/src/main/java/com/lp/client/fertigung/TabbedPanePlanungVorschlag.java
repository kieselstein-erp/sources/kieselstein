/*******************************************************************************
 * kieselsteinERP, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2023 - 2024 Kieselstein ERP eG, X-Net Services GmbH
 * based on: HELIUM V, Copyright (C) 2004 - 2022 HELIUM V IT-Solutions GmbH
 * <p>
 * distributed under the terms of GNU Affero General Public License, version 3
 * with supplements. See LICENSE file in base source directory for details.
 * <p>
 * Contact: help@kieselstein-erp.org
 ******************************************************************************/
package com.lp.client.fertigung;

import com.lp.client.frame.component.*;
import com.lp.client.frame.delegate.DelegateFactory;
import com.lp.client.frame.dialog.DialogFactory;
import com.lp.client.pc.LPMain;
import com.lp.client.util.dtable.DistributedTableModelImpl;
import com.lp.server.benutzer.service.RechteFac;
import com.lp.server.fertigung.service.FertigungFac;
import com.lp.server.fertigung.service.LosPlanungvorschlagDto;
import com.lp.server.fertigung.service.LossollarbeitsplanDto;
import com.lp.server.util.fastlanereader.service.query.FilterKriterium;
import com.lp.server.util.fastlanereader.service.query.QueryParameters;
import com.lp.server.util.fastlanereader.service.query.QueryResult;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.sql.Timestamp;
import java.util.Map;

import static com.lp.server.fertigung.service.LosPlanungVorschlagFac.MOVE_PLANNING_DOWN;
import static com.lp.server.fertigung.service.LosPlanungVorschlagFac.MOVE_PLANNING_UP;

/**
 * <code>TabbedPane</code> to show and edit Planning Suggestions.
 * @author DSk
 *
 */
public class TabbedPanePlanungVorschlag extends TabbedPane {

	private PanelQuery pqPlanungVorschlag = null;


	private PanelDialogKriterienPlanungVorschlag pdKriterienPlanungVorschlag = null;

	private PanelDialogKriterienSavePlanungVorschlagTermine pdSavePlanungVorschlagTermine = null;

	private final static int IDX_PANEL_AUSWAHL = 0;
	private final static String EXTRA_GOTO_AG = PanelBasis.ACTION_MY_OWN_NEW + "goto_ag";

	private static final String ACTION_SPECIAL_NEUER_PLANUNGSVORSCHLAG = PanelBasis.ALWAYSENABLED
			+ "action_special_neuer_planungs_vorschlag";

	private static final String ACTION_SPECIAL_EMPTY_PLANNING_SUGGESTIONS = PanelBasis.ALWAYSENABLED
			+ "action_special_empty_planning_suggestions";

	private static final String ACTION_SPECIAL_SAVE_PLANUNGSVORSCHLAG_TERMINE = PanelBasis.ALWAYSENABLED
			+ "action_special_save_planungs_vorschlag_termine";

	private WrapperMenuBar wrapperMenuBar = null;

	private PanelPlanungVorschlagDetail panelDetail = null;

	private LosPlanungvorschlagDto planungvorschlagDto = null;

	public TabbedPanePlanungVorschlag(InternalFrame internalFrameI) throws Throwable {
		super(internalFrameI, LPMain.getTextRespectUISPr("fert.los.planungsvorschlag"));

		jbInit();
		initComponents();

	}

	private void jbInitSplitPanel() throws Throwable {
		PanelSplit splittPanel = new PanelSplit(getInternalFrame(), this.getPanelDetailPlanungVorschlag(),
					pqPlanungVorschlag, 210);
		setComponentAt(IDX_PANEL_AUSWAHL, splittPanel);
	}

	private void jbInit() throws Throwable {

		insertTab(LPMain.getTextRespectUISPr("fert.los.planungsvorschlag"), null, null,
				LPMain.getTextRespectUISPr("fert.los.planungsvorschlag"), IDX_PANEL_AUSWAHL);

		jbInitPanelQuery();
		this.jbInitSplitPanel();

		pqPlanungVorschlag.eventYouAreSelected(false);

		this.addChangeListener(this);
		this.getInternalFrame().addItemChangedListener(this);
	}

	private void jbInitPanelQuery() throws Throwable {

		if (pqPlanungVorschlag == null) {

			String[] aWhichButtonIUse = new String[] {
					PanelBasis.ACTION_POSITION_VONNNACHNMINUS1,
					PanelBasis.ACTION_POSITION_VONNNACHNPLUS1
			};

			pqPlanungVorschlag = new PanelQuery(
					null, null, QueryParameters.UC_ID_PLANUNG_VORSCHLAG,
					aWhichButtonIUse, this.getInternalFrame(), LPMain.getTextRespectUISPr("lp.auswahl")
			);

			Map mMaschinenGruppen = DelegateFactory.getInstance().getZeiterfassungDelegate().getAllMaschinen();
			if (mMaschinenGruppen != null) {
				FilterKriterium filterKriteriumMaschine = new FilterKriterium("flrmaschine.c_bez", true, "" , FilterKriterium.OPERATOR_EQUAL, false);
				pqPlanungVorschlag.setFilterComboBox(mMaschinenGruppen, filterKriteriumMaschine, false,LPMain.getTextRespectUISPr("lp.alle"), false);
				pqPlanungVorschlag.eventActionRefresh(null, true);
			}

			String [] ids = {FertigungFac.STATUS_ANGELEGT, FertigungFac.STATUS_AUSGEGEBEN, FertigungFac.STATUS_ERLEDIGT, FertigungFac.STATUS_IN_PRODUKTION, FertigungFac.STATUS_TEILERLEDIGT};
			Map<String, String> lStatus = DelegateFactory.getInstance().getFertigungDelegate().getLosStatus(ids);
			if (lStatus != null) {
				FilterKriterium filterKriteriumAlleStatus = FertigungFilterFactory.getInstance().createFKLosStatusAbAusGegeben();
				pqPlanungVorschlag.setFilterComboBox2(lStatus, filterKriteriumAlleStatus, false,LPMain.getTextRespectUISPr("lp.alle"), false);
				pqPlanungVorschlag.eventActionRefresh(null, true);
			}

			pqPlanungVorschlag.addDirektFilter(FertigungFilterFactory.getInstance().createFKDLosnummer());
			pqPlanungVorschlag.addDirektFilter(FertigungFilterFactory.getInstance().createFKDVolltextsucheArtikel());
			pqPlanungVorschlag.addDirektFilter(FertigungFilterFactory.getInstance().createFKDLosKunde());

			pqPlanungVorschlag.setMultipleRowSelectionEnabled(false);

			pqPlanungVorschlag.createAndSaveAndShowButton("/com/lp/client/res/data_into.png",
					LPMain.getTextRespectUISPr("fert.offeneags.gotoag"),
					EXTRA_GOTO_AG, null);

			pqPlanungVorschlag.createAndSaveAndShowButton("/com/lp/client/res/clipboard.png",
					LPMain.getTextRespectUISPr("fert.los.planungsvorschlag.create"),
					ACTION_SPECIAL_NEUER_PLANUNGSVORSCHLAG, RechteFac.RECHT_FERT_LOS_CUD);

			pqPlanungVorschlag.createAndSaveAndShowButton("/com/lp/client/res/delete2.png",
					LPMain.getTextRespectUISPr("fert.los.planungsvorschlag.discard"),
					ACTION_SPECIAL_EMPTY_PLANNING_SUGGESTIONS, RechteFac.RECHT_FERT_LOS_CUD);

			pqPlanungVorschlag.createAndSaveAndShowButton("/com/lp/client/res/clock_preferences.png",
					LPMain.getTextRespectUISPr("fert.los.planungsvorschlag.save.los.termine"),
					ACTION_SPECIAL_SAVE_PLANUNGSVORSCHLAG_TERMINE, RechteFac.RECHT_FERT_LOS_CUD);
		}
	}

	public InternalFrameFertigung getInternalFrameFertigung() {
		return (InternalFrameFertigung) getInternalFrame();
	}

	private PanelPlanungVorschlagDetail getPanelDetailPlanungVorschlag() throws Throwable {
		if (panelDetail == null) {
			panelDetail = new PanelPlanungVorschlagDetail(getInternalFrame(),
					LPMain.getTextRespectUISPr("lp.auswahl"), null,
					this);
		}
		return panelDetail;
	}

	public void setKeyWasForLockMe() {
		Object oKey = pqPlanungVorschlag.getSelectedId();

		if (oKey != null) {
			getInternalFrame().setKeyWasForLockMe(oKey.toString());
		} else {
			getInternalFrame().setKeyWasForLockMe(null);
		}
	}

	private void fetchDTO(Object key) throws Throwable {
		if (key != null) {
			planungvorschlagDto = DelegateFactory.getInstance().getFertigungDelegate().getLosPlanungsVorschlagDTO((Integer) key);
		} else {
			planungvorschlagDto = null;
		}
	}

	private void refreshSelectedPlanungVorschlag() throws Throwable {
		if (pqPlanungVorschlag != null) {
			Integer id = (Integer)pqPlanungVorschlag.getIdSelected();
			getInternalFrame().enableAllOberePanelsExceptMe(this, IDX_PANEL_AUSWAHL, id != null);
			fetchDTO(id);

			Object lockKey = null;
			if (planungvorschlagDto != null) {
				QueryResult qr = ((DistributedTableModelImpl)pqPlanungVorschlag.getTable().getModel()).getDataSource().getQueryResult();
				Timestamp queryExecutionTime = qr.getQueryExecutionTimestamp();
				if (queryExecutionTime != null && queryExecutionTime.compareTo(planungvorschlagDto.getTAendern()) < 0) {
					DialogFactory.showModalInfoDialog(
							LPMain.getTextRespectUISPr( "lp.info"),
							LPMain.getTextRespectUISPr("lp.daten.nachladen")
					);
					pqPlanungVorschlag.refreshMe(planungvorschlagDto.getIId());
				}

				// Lock the complete Maschine, because when the order is changed all entries with of the maschine have
				// to be edited.
				lockKey = planungvorschlagDto.getMaschineIId();
			}

			getInternalFrame().setKeyWasForLockMe(lockKey + "");
			getPanelDetailPlanungVorschlag().setKeyWhenDetailPanel(lockKey);
			getPanelDetailPlanungVorschlag().eventYouAreSelected(false);

			if (pqPlanungVorschlag.getHmOfButtons().containsKey(EXTRA_GOTO_AG)) {
				// Enable-Disable Goto-AG Key
				pqPlanungVorschlag.getHmOfButtons().get(EXTRA_GOTO_AG).getButton().setEnabled(planungvorschlagDto != null);
			}
		}

	}

	@Override
	public void lPEventItemChanged(ItemChangedEvent e) throws Throwable {
		super.lPEventItemChanged(e);
		if (e != null) {
			if (e.getSource() == pqPlanungVorschlag) {
				if (e.getID() == ItemChangedEvent.ITEM_CHANGED) {
					refreshSelectedPlanungVorschlag();
				} else if (e.getID() == ItemChangedEvent.ACTION_YOU_ARE_SELECTED) {
					refreshTitle();
				} else if (e.getID() == ItemChangedEvent.ACTION_SPECIAL_BUTTON) {
					String sAspectInfo = ((ISourceEvent) e.getSource()).getAspect();
					if (sAspectInfo.equals(ACTION_SPECIAL_NEUER_PLANUNGSVORSCHLAG)) {
						refreshPdKriterienPlanungVorschlag();
						getInternalFrame().showPanelDialog(pdKriterienPlanungVorschlag);
					} else if (sAspectInfo.equals(ACTION_SPECIAL_SAVE_PLANUNGSVORSCHLAG_TERMINE)) {
						refreshPDSavePlanungVorschlagTermine();
						getInternalFrame().showPanelDialog(pdSavePlanungVorschlagTermine);
					} else if (sAspectInfo.equals(ACTION_SPECIAL_EMPTY_PLANNING_SUGGESTIONS)) {
						deleteAllPlanningSuggestions();
					}
				} else if (e.getID() == ItemChangedEvent.ACTION_MY_OWN_NEW) {
					String sAspectInfo = ((ISourceEvent) e.getSource()).getAspect();
					if (EXTRA_GOTO_AG.equals(sAspectInfo) && planungvorschlagDto != null && planungvorschlagDto.getLossollarbeitsplanIId() != null) {
						LossollarbeitsplanDto saDto = DelegateFactory.getInstance().getFertigungDelegate()
								.lossollarbeitsplanFindByPrimaryKey(planungvorschlagDto.getLossollarbeitsplanIId());

						FilterKriterium[] kriterien = new FilterKriterium[1];
						kriterien[0] = new FilterKriterium("flrlos.i_id", true, saDto.getLosIId() + "",
								FilterKriterium.OPERATOR_EQUAL, false);

						getInternalFrameFertigung().geheZu(InternalFrameFertigung.IDX_TABBED_PANE_LOS,
								TabbedPaneLos.IDX_ARBEITSPLAN, saDto.getLosIId(),
								saDto.getIId(), kriterien);
					}
				} else if (e.getID() == ItemChangedEvent.ACTION_POSITION_VONNNACHNMINUS1 ||
						e.getID() == ItemChangedEvent.ACTION_POSITION_VONNNACHNPLUS1) {
					Integer id =  (Integer) ((ISourceEvent) e.getSource()).getIdSelected();
					if (id != null) {
						int move_action = (e.getID() == ItemChangedEvent.ACTION_POSITION_VONNNACHNMINUS1 ? MOVE_PLANNING_UP : MOVE_PLANNING_DOWN);
						DelegateFactory.getInstance().getFertigungDelegate().movePlanningSuggestion(id, move_action);
						pqPlanungVorschlag.refreshMe(id);
						// Refresh the Detail-Panel.
						fetchDTO(id);
						getPanelDetailPlanungVorschlag().eventYouAreSelected(false);
					}
				}
			} else if (e.getSource() == pdKriterienPlanungVorschlag && e.getID() == ItemChangedEvent.ACTION_KRITERIEN_HAVE_BEEN_SELECTED) {
				// Refresh the Table of the Planning Suggestions.
				pqPlanungVorschlag.refreshMe(null);
				refreshSelectedPlanungVorschlag();
			} else if (e.getSource() == pdSavePlanungVorschlagTermine && e.getID() == ItemChangedEvent.ACTION_KRITERIEN_HAVE_BEEN_SELECTED) {
				// Refresh the Table of the Planning Suggestions.
				pqPlanungVorschlag.refreshMe(null);
				refreshSelectedPlanungVorschlag();
			}
		}
	}

	private void refreshTitle() {
		getInternalFrame().setLpTitle(InternalFrame.TITLE_IDX_OHRWASCHLUNTEN,
				LPMain.getTextRespectUISPr("fert.los.planungsvorschlag"));
		getInternalFrame().setLpTitle(InternalFrame.TITLE_IDX_OHRWASCHLOBEN,
				((PanelBasis) this.getSelectedComponent()).getAdd2Title());
	}

	private void refreshPdKriterienPlanungVorschlag() throws Throwable {
		if (pdKriterienPlanungVorschlag == null) {
			pdKriterienPlanungVorschlag = new PanelDialogKriterienPlanungVorschlag(getInternalFrame(),
					LPMain.getTextRespectUISPr("fert.los.planungsvorschlag.create"));
			pdKriterienPlanungVorschlag.setDefaults();
		}
	}

	protected void refreshPDSavePlanungVorschlagTermine() throws Throwable {
		if (pdSavePlanungVorschlagTermine == null) {
			pdSavePlanungVorschlagTermine = new PanelDialogKriterienSavePlanungVorschlagTermine(
					getInternalFrameFertigung(), LPMain.getTextRespectUISPr("fert.los.planungsvorschlag.save.los.termine")
			);
		}
		pdSavePlanungVorschlagTermine.initMaschineAuswahlValues();
	}

	public void lPEventObjectChanged(ChangeEvent e) throws Throwable {
		super.lPEventObjectChanged(e);
		int selectedIndex = this.getSelectedIndex();

		if (selectedIndex == IDX_PANEL_AUSWAHL) {
			jbInitPanelQuery();
			pqPlanungVorschlag.eventYouAreSelected(false);
			pqPlanungVorschlag.updateButtons();
		}

		refreshTitle();
	}

	protected void lPActionEvent(java.awt.event.ActionEvent e) throws Throwable {

	}

	public javax.swing.JMenuBar getJMenuBar() throws Throwable {
		setName("TabbedPanePlanungVorschlag");
		if (wrapperMenuBar == null) {
			wrapperMenuBar = new WrapperMenuBar(this);
		}
		return wrapperMenuBar;
	}

	public LosPlanungvorschlagDto getPlanungvorschlagDto() {
		return planungvorschlagDto;
	}

	public PanelQuery getPqPlanungVorschlag() {
		return pqPlanungVorschlag;
	}

	public void refreshPanelQueryForID(Integer id) throws Throwable {
		pqPlanungVorschlag.refreshMe(id);
		fetchDTO(id);
	}

	public void deleteSelectedEntry() throws Throwable {
		Object iKey = pqPlanungVorschlag.getSelectedId();
		if (iKey == null)
			return;

		Object nextID = pqPlanungVorschlag.getId2SelectAfterDelete();
		DelegateFactory.getInstance().getFertigungDelegate().deletePlanningSuggestion((Integer) iKey);
		pqPlanungVorschlag.refreshMe(nextID);
		refreshSelectedPlanungVorschlag();
	}

	public void deleteAllPlanningSuggestions() throws Throwable {
		Object iKey = pqPlanungVorschlag.getSelectedId();
		if (iKey == null)
			return;

		String title = LPMain.getTextRespectUISPr("lp.frage");
		String msg = LPMain.getTextRespectUISPr("fert.los.planungsvorschlag.discard.all.question");
		int antwort = DialogFactory.showModalJaNeinUnbegrenztDialog(getInternalFrame(), msg, title);
		if (antwort == JOptionPane.YES_OPTION) {
			DelegateFactory.getInstance().getFertigungDelegate().deleteAllPlanningSuggestions();
			pqPlanungVorschlag.refreshMe(null);
			refreshSelectedPlanungVorschlag();
		}
	}
}
