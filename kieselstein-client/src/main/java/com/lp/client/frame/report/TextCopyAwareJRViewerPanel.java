package com.lp.client.frame.report;

import net.sf.jasperreports.engine.JRPrintElement;
import net.sf.jasperreports.engine.JRPrintHyperlinkParameter;
import net.sf.jasperreports.engine.JRPrintHyperlinkParameters;
import net.sf.jasperreports.engine.fill.JRTemplatePrintText;
import net.sf.jasperreports.engine.fill.JRTemplateText;
import net.sf.jasperreports.swing.JRViewerController;
import net.sf.jasperreports.swing.JRViewerPanel;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.lp.util.GotoHelper.GOTO_ZWISCHENABLAGE;
import static com.lp.util.GotoHelper.PARAMETER_KEY;
import static com.lp.util.GotoHelper.PARAMETER_WHERE_TO_GO;

public class TextCopyAwareJRViewerPanel extends JRViewerPanel {

    private Boolean toggleTextCopy = false;
    private List<UUID> textCopyHyperlinkUUIDs = new ArrayList<>();

    public Boolean toggleTextCopy() {
        return this.toggleTextCopy = !toggleTextCopy;
    }

    public TextCopyAwareJRViewerPanel(JRViewerController viewerContext) {
        super(viewerContext);
    }

    @Override
    protected void createHyperlinks(List<JRPrintElement> elements, int offsetX, int offsetY) {
        if (toggleTextCopy) {
            for (var element : elements) {
                if (element instanceof JRTemplatePrintText) {
                    var typedElement = (JRTemplatePrintText) element;
                    var template = ((JRTemplateText) typedElement.getTemplate());

                    if (template.getLinkType() == null) {
                        template.setLinkType("GOTO");
                    }

                    if (typedElement.getHyperlinkParameters() == null) {
                        var hyperlinkParameters = new JRPrintHyperlinkParameters();
                        typedElement.setHyperlinkParameters(hyperlinkParameters);
                        hyperlinkParameters.addParameter(new JRPrintHyperlinkParameter(PARAMETER_WHERE_TO_GO, "java.lang.Integer", GOTO_ZWISCHENABLAGE));
                        hyperlinkParameters.addParameter(new JRPrintHyperlinkParameter(PARAMETER_KEY, "java.lang.String", ((JRTemplatePrintText) element).getFullText()));

                        textCopyHyperlinkUUIDs.add(typedElement.getUUID());
                    }
                }
            }
        } else {
            textCopyHyperlinkUUIDs.forEach(id -> {
                for (var element : elements) {
                    if (element instanceof JRTemplatePrintText) {
                        var typedElement = ((JRTemplatePrintText) element);
                        var template = ((JRTemplateText) typedElement.getTemplate());
                        if (element.getUUID().equals(id)) {
                            template.setLinkType(null);
                            typedElement.setHyperlinkParameters(null);
                        }
                    }
                }
            });
            textCopyHyperlinkUUIDs = new ArrayList<>();
        }
        super.createHyperlinks(elements, offsetX, offsetY);
    }
}
