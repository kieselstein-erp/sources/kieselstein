package com.lp.client.frame.delegate;

import com.lp.client.frame.ExceptionLP;
import com.lp.client.pc.LPMain;
import com.lp.server.eingangsrechnung.service.ERechnungDto;
import com.lp.server.eingangsrechnung.service.ERechnungFac;

import javax.naming.Context;
import javax.naming.InitialContext;

public class ERechnungDelegate extends Delegate {

    private Context context;
    private ERechnungFac eRechnungFac;

    public ERechnungDelegate() throws ExceptionLP {
        try {
            context = new InitialContext();
            eRechnungFac = lookupFac(context, ERechnungFac.class);
        } catch (Throwable ex) {
            handleThrowable(ex);
        }
    }

    public ERechnungDto process(byte[] content) throws ExceptionLP {
        try {
            return eRechnungFac.parseFileContentToDto(content);
        } catch (Throwable ex) {
            handleThrowable(ex);
            return null;
        }
    }

    public void createPartnerbankAusERechnung(Integer partnerId, ERechnungDto eRechnungDto) throws ExceptionLP {
        try {
            eRechnungFac.createPartnerBankAusERechnung(partnerId, eRechnungDto, LPMain.getTheClient());
        } catch (Throwable ex) {
            handleThrowable(ex);
        }
    }
}
