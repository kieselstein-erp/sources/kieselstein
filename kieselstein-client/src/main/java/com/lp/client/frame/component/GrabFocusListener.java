package com.lp.client.frame.component;

import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

/**
 * Listener that can be added to a component which is afterward attached to a parent component.
 * Sets the focus to the component once then deletes itself.
 * Can be used to set the initial Focus on dialog components.
 */
public class GrabFocusListener implements AncestorListener {
    @Override
    public void ancestorAdded(AncestorEvent event) {
        JComponent component = event.getComponent();
        component.requestFocusInWindow();
        component.removeAncestorListener( this );
    }
    @Override
    public void ancestorRemoved(AncestorEvent event) {}
    @Override
    public void ancestorMoved(AncestorEvent event) {}
}