package com.lp.client.pc;

import com.lp.client.util.IconFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class DialogUpdate extends JDialog {
    private JPanel contentPane;

    public JButton btnUpdate;
    private JTextArea txtInfo;

    private JScrollPane spInfo;

    public DialogUpdate(Frame frame) {
        super(frame);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setModal(true);
        getRootPane().setDefaultButton(btnUpdate);
        setIconImage(IconFactory.getKSE().getImage());
        setResizable(true);

        Dimension d = new Dimension(450, 150);
        setMinimumSize(d);
        setSize(d);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                ((Desktop) getOwner()).setBAbbruch(true);
                System.exit(0);
            }
        });

        contentPane = new JPanel();
        contentPane.setLayout(new GridBagLayout());
        setContentPane(contentPane);

        final JPanel panel1 = new JPanel();
        panel1.setLayout(new BorderLayout(5, 0));

        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(0, 5, 5, 5);
        contentPane.add(panel1, gbc);

        btnUpdate = new JButton(LPMain.getTextRespectUISPr("update.download"));
        btnUpdate.addActionListener(e -> {
            Desktop.browseURI(System.getProperty("java.naming.provider.url").replace("remote+", "") + "/clients");
            System.exit(0);
        });
        panel1.add(btnUpdate, BorderLayout.CENTER);

        final JPanel panel2 = new JPanel();
        panel2.setLayout(new BorderLayout(0, 0));

        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(5, 5, 5, 5);
        contentPane.add(panel2, gbc);

        txtInfo = new JTextArea();
        txtInfo.setForeground(Color.RED);
        txtInfo.setBackground(null);
        txtInfo.setMargin(new Insets(5, 5, 5, 5));
        txtInfo.setCaretPosition(0);
        txtInfo.setEditable(false);
        txtInfo.setLineWrap(true);
        txtInfo.setRows(2);
        txtInfo.setWrapStyleWord(true);

        spInfo = new JScrollPane(txtInfo, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        spInfo.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        spInfo.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        spInfo.setAlignmentX(CENTER_ALIGNMENT);

        panel2.add(spInfo, BorderLayout.CENTER);
    }

    public void setText(String text) {
        txtInfo.setText(text);
        txtInfo.setCaretPosition(0);
    }
}
