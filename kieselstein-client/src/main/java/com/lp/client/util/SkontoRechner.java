package com.lp.client.util;

import com.lp.server.finanz.service.FinanzFac;
import com.lp.util.Helper;

import java.math.BigDecimal;

public class SkontoRechner {

    public static BigDecimal berechneSkontoWert(BigDecimal wert, BigDecimal prozent) {
        if (prozent == null)
            return wert;
        return Helper.getProzentWert(wert, prozent, FinanzFac.NACHKOMMASTELLEN);
    }

    public static BigDecimal berechneSkontiertenWert(BigDecimal wert, BigDecimal prozent) {
        if (prozent == null || prozent.compareTo(new BigDecimal("0")) == 0)
            return wert;
        return Helper.getWertPlusProzent(wert, prozent, FinanzFac.NACHKOMMASTELLEN);
    }
}
