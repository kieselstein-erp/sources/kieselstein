package com.lp.client.partner;

import com.lp.client.artikel.InternalFrameArtikel;
import com.lp.client.frame.HelperClient;
import com.lp.client.frame.component.*;
import com.lp.client.frame.delegate.DelegateFactory;
import com.lp.client.pc.LPMain;
import com.lp.server.partner.service.KundeDto;
import net.miginfocom.swing.MigLayout;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.util.EventObject;

public class PanelKundeSonstiges extends PanelBasis {
    private KundeDto kundeDto;
    private InternalFrameKunde internalFrameKunde;
    private final String add2TitleI;
    private GridBagLayout gbl;
    private JPanel panel;
    private JPanel toolsPanel;

    private WrapperLabel wlSftpHostname, wlSftpPort, wlSftpPath, wlSftpUsername, wlSftpPassword;
    private WrapperTextField wtfSftpHostname, wtfSftpPath, wtfSftpUsername;
    private WrapperPasswordField wtfSftpPassword;
    private WrapperNumberField wnfSftpPort;

    public PanelKundeSonstiges(InternalFrame internalFrame, String add2TitleI, Object keyI) throws Throwable {
        super(internalFrame, add2TitleI, keyI);

        internalFrameKunde = (InternalFrameKunde) internalFrame;
        this.add2TitleI = add2TitleI;

        jbInit();
        initComponents();
        enableAllComponents(this, false);
    }

    private void jbInit() throws Throwable {
        gbl = new GridBagLayout();
        this.setLayout(gbl);
        getInternalFrame().addItemChangedListener(this);

        toolsPanel = getToolsPanel();
        this.setActionMap(null);

        panel = new JPanel(
                new MigLayout("ins 0, wrap 6 ", "[fill, 25%|fill,20%|fill,5%|fill, 20%|fill, 10%|fill, 15%]","[]2[]"));

        this.add(toolsPanel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        this.add(panel, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHEAST,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        this.add(getPanelStatusbar(), new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

        wlSftpHostname = new WrapperLabel();
        wlSftpHostname.setText(LPMain.getTextRespectUISPr("kunde.sftp.host"));
        panel.add(wlSftpHostname);

        wtfSftpHostname = new WrapperTextField();
        wtfSftpHostname.setMandatoryField(true);
        panel.add(wtfSftpHostname);

        wlSftpPort = new WrapperLabel();
        wlSftpPort.setText(LPMain.getTextRespectUISPr("kunde.sftp.port"));
        panel.add(wlSftpPort);

        wnfSftpPort = new WrapperNumberField();
        wnfSftpPort.setFractionDigits(0);
        wnfSftpPort.setMinimumValue(0);
        wnfSftpPort.setMandatoryField(true);
        panel.add(wnfSftpPort, "wrap");

        wlSftpPath = new WrapperLabel();
        wlSftpPath.setText(LPMain.getTextRespectUISPr("kunde.sftp.path"));
        panel.add(wlSftpPath);

        wtfSftpPath = new WrapperTextField();
        wtfSftpPath.setMandatoryField(true);
        panel.add(wtfSftpPath, "wrap");

        wlSftpUsername = new WrapperLabel();
        wlSftpUsername.setText(LPMain.getTextRespectUISPr("kunde.sftp.user"));
        panel.add(wlSftpUsername);

        wtfSftpUsername = new WrapperTextField();
        wtfSftpUsername.setMandatoryField(true);
        panel.add(wtfSftpUsername, "wrap");

        wlSftpPassword = new WrapperLabel();
        wlSftpPassword.setText(LPMain.getTextRespectUISPr("kunde.sftp.pass"));
        panel.add(wlSftpPassword);

        wtfSftpPassword = new WrapperPasswordField();
        wtfSftpPassword.setMandatoryField(true);
        panel.add(wtfSftpPassword, "wrap");

        enableToolsPanelButtons(new String[] { ACTION_UPDATE, ACTION_SAVE,
                                               ACTION_DISCARD, ACTION_PREVIOUS,
                                               ACTION_NEXT, });
    }

    protected String getLockMeWer() throws Exception {
        return HelperClient.LOCKME_KUNDE;
    }

    protected JComponent getFirstFocusableComponent() throws Exception {
        return wtfSftpHostname;
    }

    protected void eventItemchanged(EventObject eI) throws Throwable {
        ItemChangedEvent e = (ItemChangedEvent) eI;
        super.eventItemchanged(eI);
    }

    protected void dto2Components() throws Throwable {
        wtfSftpHostname.setText(kundeDto.getSftpHostname());
        wnfSftpPort.setInteger(kundeDto.getSftpPort());
        wtfSftpPath.setText(kundeDto.getSftpPath());
        wtfSftpUsername.setText(kundeDto.getSftpUsername());
        wtfSftpPassword.setText(kundeDto.getSftpPassword());
    }

    protected void components2Dto() throws Throwable {
        kundeDto.setSftpHostname(wtfSftpHostname.getText());
        kundeDto.setSftpPort(wnfSftpPort.getInteger());
        kundeDto.setSftpPath(wtfSftpPath.getText());
        kundeDto.setSftpUsername(wtfSftpUsername.getText());
        kundeDto.setSftpPassword(wtfSftpPassword.getText());
    }

    public void eventYouAreSelected(boolean bNeedNoYouAreSelectedI) throws Throwable {
        super.eventYouAreSelected(false);

        leereAlleFelder(this);
        kundeDto = DelegateFactory.getInstance().getKundeDelegate()
                .kundeFindByPrimaryKey(((InternalFrameKunde) getInternalFrame()).getKundeDto().getIId());

        getInternalFrame().setLpTitle(InternalFrame.TITLE_IDX_AS_I_LIKE, kundeDto.getPartnerDto().formatFixTitelName1Name2());

        dto2Components();
    }

    public void eventActionSave(ActionEvent e, boolean bNeedNoSaveI) throws Throwable {
        if (allMandatoryFieldsSetDlg()) {
            components2Dto();

            boolean verify = DelegateFactory.getInstance().getRechnungDelegate().verifySftpConnection(kundeDto);
            if (!verify && kundeDto.getSftpPath() != null) {
                int reply = JOptionPane.showConfirmDialog(null, LPMain.getTextRespectUISPr("sftp.ordner.erstellen"), "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (reply == JOptionPane.YES_OPTION) {
                    DelegateFactory.getInstance().getRechnungDelegate().makeSftpDirectories(kundeDto);
                } else {
                    return;
                }
            }

            DelegateFactory.getInstance().getKundeDelegate().updateKunde(kundeDto);

            kundeDto = DelegateFactory.getInstance().getKundeDelegate()
                    .kundeFindByPrimaryKey(kundeDto.getIId());
            ((InternalFrameKunde) getInternalFrame()).setKundeDto(kundeDto);
            ((InternalFrameKunde) getInternalFrame()).getTpKunde().getOnlyPanelKundeQP1().setSelectedId(kundeDto.getIId());
            super.eventActionSave(e, true);
        }
    }
}
