package com.lp.client.auftrag;

import com.lp.client.frame.Defaults;
import com.lp.client.frame.ExceptionLP;
import com.lp.client.frame.HelperClient;
import com.lp.client.frame.component.*;
import com.lp.client.frame.delegate.DelegateFactory;
import com.lp.client.frame.dialog.DialogFactory;
import com.lp.client.pc.LPMain;
import com.lp.client.util.IconFactory;
import com.lp.server.auftrag.service.AuftragpositionDto;
import com.lp.server.fertigung.service.LosDto;
import com.lp.server.partner.service.KundeDto;
import com.lp.server.system.service.ParameterFac;
import com.lp.server.system.service.ParametermandantDto;
import com.lp.util.Helper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.EventObject;
import java.util.GregorianCalendar;

public class DialogTerminAuftragsposition extends JDialog implements ActionListener, PropertyChangeListener, ItemChangedListener {
    private TabbedPaneAuftrag tabbedPane;
    private AuftragpositionDto positionDto;
    private LosDto losDto;
    private boolean bParameterMaterialVollstaendigBis = false;

    private WrapperButton btnSave, btnCancel;
    private WrapperLabel lblBisher, lblAktuell;

    private JButton jbuLock, jbuLock1;
    boolean bGesperrt = true, bGesperrt1 = true;
    private JButton wbuUp, wbuDown;

    private WrapperLabel lblLiefertermin;
    private WrapperDateField dfLieferterminBisher, dfLiefertermin;
    private WrapperLabel lblBeginn;
    private WrapperDateField dfBeginnBisher, dfBeginn;
    private WrapperLabel lblEnde;
    private WrapperDateField dfEndeBisher, dfEnde;
    private WrapperLabel lblMaterialVollstaendig;
    private WrapperDateField dfMaterialVollstaendigBisher, dfMaterialVollstaendig;

    public DialogTerminAuftragsposition(TabbedPaneAuftrag tabbedPane, LosDto losDto) throws Throwable {
        super(LPMain.getInstance().getDesktop(), LPMain.getTextRespectUISPr("auft.menu.termineverschieben"), true);

        if (losDto != null) {
            setTitle(getTitle() + " - " + LPMain.getTextRespectUISPr("lp.los") + " " + losDto.getCNr());
        }

        setSize(Defaults.sizeFactor(512), Defaults.sizeFactor(200));
        setMaximumSize(new Dimension(Defaults.sizeFactor(384), Defaults.sizeFactor(160)));
        setMinimumSize(new Dimension(Defaults.sizeFactor(384), Defaults.sizeFactor(160)));

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                setVisible(false);
                dispose();
            }
        });

        this.tabbedPane = tabbedPane;
        this.losDto = losDto;
        this.positionDto = tabbedPane.getAuftragPositionenBottom().getAuftragpositionDto();

        jbInit();
    }

    private void jbInit() throws Throwable {
        JPanel content = new JPanel();
        content.setLayout(new GridBagLayout());

        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridBagLayout());

        int row = 0;

        lblBisher = new WrapperLabel();
        lblBisher.setText(LPMain.getTextRespectUISPr("auft.menu.termineverschieben.bisher"));
        lblBisher.setHorizontalAlignment(SwingConstants.LEFT);
        panel3.add(lblBisher, new GridBagConstraints(2, row, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

        lblAktuell = new WrapperLabel();
        lblAktuell.setText(LPMain.getTextRespectUISPr("auft.menu.termineverschieben.aktuell"));
        lblAktuell.setHorizontalAlignment(SwingConstants.LEFT);
        panel3.add(lblAktuell, new GridBagConstraints(3, row, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

        row++;

        lblLiefertermin = new WrapperLabel();
        lblLiefertermin.setText(LPMain.getTextRespectUISPr("label.liefertermin"));
        panel3.add(lblLiefertermin, new GridBagConstraints(0, row, 2, 1, 2.0, 1.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 10, 2), 0, 0));

        dfLieferterminBisher = new WrapperDateField();
        dfLieferterminBisher.setEnabled(false);
        dfLieferterminBisher.setDate(positionDto.getTUebersteuerbarerLiefertermin());
        panel3.add(dfLieferterminBisher, new GridBagConstraints(2, row, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 10, 0), 0, 0));

        dfLiefertermin = new WrapperDateField();
        dfLiefertermin.setMandatoryField(true);
        dfLiefertermin.setDate(positionDto.getTUebersteuerbarerLiefertermin()); // tabbedPane.getAuftragDto().getDLiefertermin()
        dfLiefertermin.getDateEditor().addPropertyChangeListener("date", this);
        panel3.add(dfLiefertermin, new GridBagConstraints(3, row, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 10, 0), 0, 0));

        if (losDto != null) {
            jbuLock = ButtonFactory.createJButton(IconFactory.getLock(), "");
            jbuLock.addActionListener(this);
            HelperClient.setMinimumAndPreferredSize(jbuLock,
                    new Dimension(Defaults.getInstance().getControlHeight(), Defaults.getInstance().getControlHeight()));
            panel3.add(jbuLock, new GridBagConstraints(5, row, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 10, 0), 0, 0));

            row++;

            lblBeginn = new WrapperLabel();
            lblBeginn.setText(LPMain.getTextRespectUISPr("lp.los") + " - " + LPMain.getTextRespectUISPr("lp.beginn"));
            panel3.add(lblBeginn, new GridBagConstraints(0, row, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 2), 0, 0));

            dfBeginnBisher = new WrapperDateField();
            dfBeginnBisher.setEnabled(false);
            dfBeginnBisher.setDate(losDto.getTProduktionsbeginn());
            panel3.add(dfBeginnBisher, new GridBagConstraints(2, row, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

            dfBeginn = new WrapperDateField();
            dfBeginn.setEnabled(false);
            dfBeginn.setMandatoryField(true);
            dfBeginn.setDate(losDto.getTProduktionsbeginn());
            panel3.add(dfBeginn, new GridBagConstraints(3, row, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

            wbuUp = new JButton();
            wbuUp.setEnabled(false);
            wbuUp.setIcon(new ImageIcon(getClass().getResource("/com/lp/client/res/navigate_open.png")));
            wbuUp.addActionListener(this);
            panel3.add(wbuUp, new GridBagConstraints(4, row, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                    GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 15), 0, 0));

            jbuLock1 = ButtonFactory.createJButton(IconFactory.getLock(), "");
            jbuLock1.addActionListener(this);
            HelperClient.setMinimumAndPreferredSize(jbuLock1,
                    new Dimension(Defaults.getInstance().getControlHeight(), Defaults.getInstance().getControlHeight()));
            panel3.add(jbuLock1, new GridBagConstraints(5, row, 1, 2, 1.0, 1.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

            row++;

            ParametermandantDto parameterMaterialVollstaendig = (ParametermandantDto) DelegateFactory.getInstance()
                    .getParameterDelegate().getParametermandant(ParameterFac.PARAMETER_LOS_MATERIAL_ZIELTERMIN_ANZEIGEN,
                            ParameterFac.KATEGORIE_FERTIGUNG, LPMain.getTheClient().getMandant());
            bParameterMaterialVollstaendigBis = parameterMaterialVollstaendig.asBoolean();

            int padding = bParameterMaterialVollstaendigBis ? 5 : 0;

            lblEnde = new WrapperLabel();
            lblEnde.setText(LPMain.getTextRespectUISPr("lp.los") + " - " + LPMain.getTextRespectUISPr("lp.ende"));
            panel3.add(lblEnde, new GridBagConstraints(0, row, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, padding, 2), 0, 0));

            dfEndeBisher = new WrapperDateField();
            dfEndeBisher.setEnabled(false);
            dfEndeBisher.setDate(losDto.getTProduktionsende());
            panel3.add(dfEndeBisher, new GridBagConstraints(2, row, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, padding, 0), 0, 0));

            dfEnde = new WrapperDateField();
            dfEnde.setMandatoryField(true);
            dfEnde.setEnabled(false);
            dfEnde.setDate(losDto.getTProduktionsende());
            dfEnde.getDateEditor().addPropertyChangeListener("date", this);
            panel3.add(dfEnde, new GridBagConstraints(3, row, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, padding, 0), 0, 0));

            wbuDown = new JButton();
            wbuDown.setEnabled(false);
            wbuDown.setIcon(new ImageIcon(getClass().getResource("/com/lp/client/res/navigate_close.png")));
            wbuDown.addActionListener(this);
            panel3.add(wbuDown, new GridBagConstraints(4, row, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                    GridBagConstraints.HORIZONTAL, new Insets(0, 0, padding, 15), 0, 0));

            if (bParameterMaterialVollstaendigBis) {

                row++;

                lblMaterialVollstaendig = new WrapperLabel();
                lblMaterialVollstaendig.setText(LPMain.getTextRespectUISPr("auft.material.bis"));
                panel3.add(lblMaterialVollstaendig, new GridBagConstraints(0, row, 2, 1, 2.5, 1.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 2), 0, 0));

                dfMaterialVollstaendigBisher = new WrapperDateField();
                dfMaterialVollstaendigBisher.setEnabled(false);
                dfMaterialVollstaendigBisher.setDate(losDto.getTMaterialVollstaendigBis());
                panel3.add(dfMaterialVollstaendigBisher, new GridBagConstraints(2, row, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

                dfMaterialVollstaendig = new WrapperDateField();
                dfMaterialVollstaendig.setEnabled(false);
                dfMaterialVollstaendig.setDate(losDto.getTMaterialVollstaendigBis());
                dfMaterialVollstaendig.getDateEditor().addPropertyChangeListener("date", this);
                panel3.add(dfMaterialVollstaendig, new GridBagConstraints(3, row, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
        }

        content.add(panel3, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
                GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridBagLayout());

        btnSave = new WrapperButton();
        btnSave.addActionListener(this);
        btnSave.setText(LPMain.getTextRespectUISPr("lp.report.save"));
        panel1.add(btnSave, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 2), 0, 0));

        btnCancel = new WrapperButton();
        btnCancel.setText(LPMain.getTextRespectUISPr("Cancel"));
        btnCancel.addActionListener(this);
        panel1.add(btnCancel, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 2, 0, 0), 0, 0));

        content.add(panel1, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
                GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

        setContentPane(content);
    }

    @Override
    public void changed(EventObject e) {

    }

    private void updateDate(WrapperDateField df, boolean increase) {
        if (df.getDate() != null) {
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(df.getDate());
            gc.add(Calendar.DAY_OF_MONTH, increase ? 1 : - 1);
            df.setDate(new java.sql.Date(gc.getTimeInMillis()));
        }
    }

    /**
     * Checks if the new Dates are valid
     * @return true if Dates are valid, else false
     * @throws Throwable
     */
    private boolean isSaveGueltig() throws Throwable {
        int durchlaufzeit =  DelegateFactory.getInstance().getStuecklisteDelegate()
                .stuecklisteFindByPrimaryKey(losDto.getStuecklisteIId()).getNDefaultdurchlaufzeit().intValue();
        Timestamp tLosEndeMin = Helper.addiereTageZuTimestamp(dfMaterialVollstaendig.getTimestamp(), durchlaufzeit);

        if(tLosEndeMin.compareTo(dfEnde.getTimestamp()) > 0){
            DialogFactory.showModalInfoDialog(LPMain.getTextRespectUISPr("lp.error"), LPMain.getTextRespectUISPr("auft.losverschieben.losende.materialbis"));
            return false;
        }

        KundeDto kundeDto = DelegateFactory.getInstance().getKundeDelegate().kundeFindByPrimaryKey(tabbedPane.getAuftragDto().getKundeIIdLieferadresse());
        Integer lieferdauer = kundeDto.getILieferdauer();
        lieferdauer = lieferdauer == null ? 0 : lieferdauer;

        Timestamp tLieferBeginn = Helper.addiereTageZuTimestamp(dfLiefertermin.getTimestamp(), -lieferdauer);

        if(tLieferBeginn.compareTo(dfEnde.getTimestamp()) < 0){
            DialogFactory.showModalInfoDialog(LPMain.getTextRespectUISPr("lp.error"), LPMain.getTextRespectUISPr("auft.losverschieben.losende.lieferdatum"));
            return false;
        }

        if(dfEnde.getTimestamp().compareTo(Helper.addiereTageZuDatum(dfBeginn.getTimestamp(), 2)) < 0){
            DialogFactory.showModalInfoDialog(LPMain.getTextRespectUISPr("lp.error"), LPMain.getTextRespectUISPr("auft.losverschieben.dauer"));
            return false;
        }

        return true;
    }

    /**
     * handles the logic of btnSave
     * @return true if successful
     */
    private boolean eventSave() {
        try {
            if (bParameterMaterialVollstaendigBis && !dfMaterialVollstaendig.hasContent() && dfMaterialVollstaendigBisher.hasContent()) {
                boolean bMaterialVollstaendigLoeschen = DialogFactory.showMeldung(LPMain.getTextRespectUISPr("auft.zieltermin.loeschen"),
                        LPMain.getTextRespectUISPr("lp.hint"), javax.swing.JOptionPane.YES_NO_OPTION) == javax.swing.JOptionPane.YES_OPTION;

                if (!bMaterialVollstaendigLoeschen)
                    return false;
            }

            if (bParameterMaterialVollstaendigBis && dfMaterialVollstaendig.hasContent() && !isSaveGueltig())
                return false;

            tabbedPane.getAuftragPositionenBottom().getAuftragpositionDto()
                    .setTUebersteuerbarerLiefertermin(Helper.asTimestamp(dfLiefertermin.getDate()));

            DelegateFactory.getInstance().getAuftragpositionDelegate()
                    .updateAuftragposition(tabbedPane.getAuftragPositionenBottom().getAuftragpositionDto());

            if (losDto != null) {
                Timestamp tMaterialVollstaendigBis = bParameterMaterialVollstaendigBis ?
                        dfMaterialVollstaendig.getTimestamp() : losDto.getTMaterialVollstaendigBis();

                DelegateFactory.getInstance().getFertigungDelegate().terminVerschieben(losDto.getIId(),
                        Helper.cutTimestamp(dfBeginn.getTimestamp()), Helper.cutTimestamp(dfEnde.getTimestamp()),
                        Helper.cutTimestamp(tMaterialVollstaendigBis));
            }

        } catch (Throwable ex) {
            tabbedPane.handleException(ex, true);
        }
        return true;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        final Object src = e.getSource();
        if (src.equals(btnSave)) {
            if(eventSave())
                setVisible(false);
        } else if (src.equals(btnCancel)) {
            setVisible(false);
        } else if (src.equals(wbuUp)) {
            if (!bGesperrt1) {
                updateDate(dfBeginn, true);
            } else {
                updateDate(dfBeginn, true);
                updateDate(dfEnde, true);
            }
        } else if (src.equals(wbuDown)) {
            if (!bGesperrt1) {
                updateDate(dfBeginn, false);
            } else {
                updateDate(dfBeginn, false);
                updateDate(dfEnde, false);
            }
        } else if (src.equals(jbuLock)) {
            bGesperrt = !bGesperrt;
            if (bGesperrt) {
                bGesperrt1 = true;
            }
        } else if (src.equals(jbuLock1)) {
            bGesperrt1 = !bGesperrt1;
            if (bGesperrt) {
                bGesperrt1 = true;
            }
        }

        if (losDto != null) {
            if (bGesperrt && bGesperrt1) {
                dfLiefertermin.setEnabled(true);
                wbuUp.setEnabled(false);
                wbuDown.setEnabled(false);
                dfBeginn.setEnabled(false);
                dfEnde.setEnabled(false);
                dfMaterialVollstaendig.setEnabled(false);
            }

            if (!bGesperrt) {
                dfLiefertermin.setEnabled(true);
                wbuUp.setEnabled(true);
                wbuDown.setEnabled(true);
                dfBeginn.setEnabled(true);
                dfEnde.setEnabled(true);
                dfMaterialVollstaendig.setEnabled(true);
            }

            jbuLock.setIcon(bGesperrt ? IconFactory.getLock() : IconFactory.getLockOpen());
            jbuLock1.setIcon(bGesperrt1 ? IconFactory.getLock() : IconFactory.getLockOpen());
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent e) {
        final Object o = e.getSource();

        if (losDto != null) {
            if (bGesperrt && bGesperrt1 && o.equals(dfLiefertermin.getDisplay()) && e.getNewValue() instanceof Date && e.getPropertyName().equals("date")) {
                int diff = Helper.getDifferenzInTagen(dfEndeBisher.getDate(), dfLieferterminBisher.getDate());
                int diff2 = Helper.getDifferenzInTagen(dfBeginnBisher.getDate(), dfEndeBisher.getDate());
                dfEnde.setDate(Helper.addiereTageZuDatum(dfLiefertermin.getDate(), -diff));
                dfBeginn.setDate(Helper.addiereTageZuDatum(dfEnde.getDate(), -diff2));
            } else if (!bGesperrt && bGesperrt1 && o.equals(dfEnde.getDisplay()) && e.getNewValue() instanceof Date && e.getPropertyName().equals("date")) {
                int diff = Helper.getDifferenzInTagen(dfBeginnBisher.getDate(), dfEndeBisher.getDate());
                dfBeginn.setDate(Helper.addiereTageZuDatum(dfEnde.getDate(), -diff));
            }
        }
    }
}
