## Zusammenfassung
(Fasse die Anforderung prägnant zusammen)

## Nutzen/Ziel
(Was ist das Ziel der Funktion - dies ist wichtig, um zu verstehen was du umsetzen möchtest, vielleicht gibt es die Funktion schon, aber über einen anderen Weg. Wir werden dann die beste Lösung für die Anforderung suchen)

## Modul, Maske, Terminal, API-Aufruf, Bericht
(in welchem ​​Modul, Fenster, Terminal, API-Aufruf bzw. Report sollte die Erweiterung eingebaut werden)

## Ausführliche Erklärung
(Eine ausführliche Erklärung, was mit dem neuen Feature möglich sein soll)

## Mögliche Lösung
(Wir freuen uns über deine Gedanken, deinen Vorschlag, wie wir deine Anforderung umsetzen können)

/label ~"type::Feature" ~"status::0 - open" ~"priority::normal"
