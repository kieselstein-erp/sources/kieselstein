package com.heliumv.api.item;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
/**
 * Artikelstammblatt als PDF
 */
public class ItemMasterSheet {
    private byte[] pdfContent;

    public byte[] getPdfContent() {
        return pdfContent;
    }
    public void setPdfContent(byte[] pdfContent) {
        this.pdfContent = pdfContent;
    }
}
