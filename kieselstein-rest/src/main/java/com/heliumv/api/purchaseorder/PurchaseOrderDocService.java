package com.heliumv.api.purchaseorder;

import com.heliumv.api.document.DocumentMetadata;
import com.heliumv.api.document.IDocumentCategoryService;
import com.heliumv.factory.IBestellungCall;
import com.heliumv.factory.ILieferantCall;
import com.lp.server.bestellung.service.BestellungDto;
import com.lp.server.partner.service.LieferantDto;
import com.lp.server.system.jcr.service.JCRDocDto;
import com.lp.server.system.jcr.service.JCRDocFac;
import com.lp.server.system.jcr.service.docnode.DocNodeBestellung;
import com.lp.server.system.jcr.service.docnode.DocPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.rmi.RemoteException;

public class PurchaseOrderDocService implements IDocumentCategoryService {
    private static Logger log = LoggerFactory.getLogger(PurchaseOrderDocService.class) ;

    @Autowired
    private IBestellungCall bestellungCall;
    @Autowired
    private ILieferantCall lieferantCall;

    @Override
    public JCRDocDto setupDoc(Integer id, String cnr, DocumentMetadata metadata) throws RemoteException {
        BestellungDto besDto = findBestellung(id, cnr);
        if (besDto == null) return null;

        JCRDocDto docDto = new JCRDocDto();
        DocPath docPath = getDocPath(besDto);
        docDto.setDocPath(docPath);

        docDto.setsBelegnummer(besDto.getCNr());
        docDto.setsTable("BESTELLUNG");
        docDto.setsRow(besDto.getIId().toString());
        docDto.setlSicherheitsstufe(metadata.getSecurityLevel() != null ?
                metadata.getSecurityLevel() : JCRDocFac.SECURITY_ARCHIV);

        LieferantDto lieferantDto = lieferantCall.lieferantFindByPrimaryKeyOhneExc(besDto.getLieferantIIdBestelladresse());
        docDto.setlPartner(lieferantDto.getPartnerIId());

        return docDto;
    }

    private BestellungDto findBestellung(Integer id, String cnr) throws RemoteException {
        if (id != null) {
            BestellungDto besDto = bestellungCall.bestellungFindByPrimaryKeyOhneExc(id);
            if (besDto == null) log.error("Could not find purchase order with id = " + id);
            return besDto;
        }

        if (cnr != null) {
            BestellungDto besDto = bestellungCall.bestellungFindByCNrMandantCNr(cnr);
            if (besDto == null) log.error("Could not find purchase order with cnr = " + cnr);
            return besDto;
        }

        log.warn("Could not find purchase order cause of id and cnr is null");
        return null;
    }

    @Override
    public DocPath getDocPath(Integer id, String cnr) throws RemoteException {
        BestellungDto besDto = findBestellung(id, cnr);
        if (besDto == null) return null;

        return getDocPath(besDto);
    }

    private DocPath getDocPath(BestellungDto besDto) {
        return new DocPath(new DocNodeBestellung(besDto));
    }

}
