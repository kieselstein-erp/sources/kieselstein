package com.heliumv.api;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class ServerConfiguration {
    private static final String RESOURCE_BUNDLE_VERSION = "com.heliumv.res.lp";

    public static String getApplicationVersion() {
        String version = "";

        try {
            ResourceBundle resBundle = ResourceBundle
                    .getBundle(RESOURCE_BUNDLE_VERSION);
            if(resBundle.containsKey("application.version")) {
                version = resBundle.getString("application.version");
            }
        } catch(MissingResourceException e) {
        } catch(NumberFormatException e) {
        }

        return version;
    }

    public static String getApplicationVersionHash() {
        String version = "";

        try {
            ResourceBundle resBundle = ResourceBundle
                    .getBundle(RESOURCE_BUNDLE_VERSION);
            if(resBundle.containsKey("application.version.hash")) {
                version = resBundle.getString("application.version.hash");
            }
        } catch(MissingResourceException e) {
        } catch(NumberFormatException e) {
        }

        return version;
    }
}
