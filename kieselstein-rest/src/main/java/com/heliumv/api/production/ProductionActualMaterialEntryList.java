package com.heliumv.api.production;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class ProductionActualMaterialEntryList {

	private List<ProductionActualMaterialEntry> entries;

	public ProductionActualMaterialEntryList() {
		setEntries(new ArrayList<>());
	}

	public List<ProductionActualMaterialEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<ProductionActualMaterialEntry> entries) {
		this.entries = entries;
	}
	
}
