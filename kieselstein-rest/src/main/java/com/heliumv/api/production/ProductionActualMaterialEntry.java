package com.heliumv.api.production;

import com.heliumv.api.BaseEntryId;
import com.heliumv.api.item.ItemV1Entry;

import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.List;

@XmlRootElement
public class ProductionActualMaterialEntry extends BaseEntryId {

	private Integer productionId;
	private BigDecimal amount;
	private String unitCnr;
	private ItemV1Entry itemEntry;
	private BigDecimal amountIssued;
	private Boolean belatedWithdrawn;
	private List<IdentityAmountEntry> identities ;

	public Integer getProductionId() {
		return productionId;
	}
	public void setProductionId(Integer productionId) {
		this.productionId = productionId;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getUnitCnr() {
		return unitCnr;
	}
	public void setUnitCnr(String unitCnr) {
		this.unitCnr = unitCnr;
	}
	public ItemV1Entry getItemEntry() {
		return itemEntry;
	}
	public void setItemEntry(ItemV1Entry itemEntry) {
		this.itemEntry = itemEntry;
	}
	
	public BigDecimal getAmountIssued() {
		return amountIssued;
	}
	public void setAmountIssued(BigDecimal amountIssued) {
		this.amountIssued = amountIssued;
	}
	public Boolean getBelatedWithdrawn() {
		return belatedWithdrawn;
	}
	public void setBelatedWithdrawn(Boolean belatedWithdrawn) {
		this.belatedWithdrawn = belatedWithdrawn;
	}

	/**
	 * Die Serien/Chargennummer-Infos
	 * @return die Seriennummer bzw. Chargennummern/St&uuml;ckzahlen
	 */
	public List<IdentityAmountEntry> getIdentities() {
		return identities;
	}
	public void setIdentities(List<IdentityAmountEntry> identities) {
		this.identities = identities;
	}

}
