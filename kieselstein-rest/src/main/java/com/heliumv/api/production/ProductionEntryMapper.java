package com.heliumv.api.production;

import com.heliumv.api.item.ItemEntryMapper;
import com.heliumv.factory.ILagerCall;
import com.lp.server.artikel.service.SeriennrChargennrMitMengeDto;
import com.lp.server.fertigung.service.LosDto;
import com.lp.server.fertigung.service.LosistmaterialDto;
import com.lp.server.fertigung.service.LossollmaterialDto;
import com.lp.server.system.service.LocaleFac;
import com.lp.util.Helper;
import org.springframework.beans.factory.annotation.Autowired;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class ProductionEntryMapper {
	@Autowired
	private ItemEntryMapper itemEntryMapper;
	@Autowired
	private ILagerCall lagerCall ;

	public ProductionEntry mapEntry(LosDto losDto) {
		ProductionEntry entry = new ProductionEntry();
		entry.setId(losDto.getIId());
		entry.setCnr(losDto.getCNr());
		entry.setPartlistId(losDto.getStuecklisteIId());
		entry.setAmount(losDto.getNLosgroesse());
		entry.setComment(losDto.getCKommentar());
		entry.setEndDateMs(losDto.getTProduktionsende().getTime());
		entry.setStartDateMs(losDto.getTProduktionsbeginn().getTime());
		entry.setProject(losDto.getCProjekt());
		entry.setStatus(ProductionStatus.fromString(losDto.getStatusCNr()));
		entry.setTargetStockId(losDto.getLagerIIdZiel());
		
		return entry;
	}

	public ProductionTargetMaterialEntry mapEntry(LossollmaterialDto dto) {
		ProductionTargetMaterialEntry entry = new ProductionTargetMaterialEntry();
		entry.setId(dto.getIId());
		entry.setAmount(dto.getNMenge());
		entry.setComment(dto.getCKommentar());
		entry.setiSort(dto.getISort());
		entry.setPosition(dto.getCPosition());
		entry.setPrice(dto.getNSollpreis());
		entry.setProductionId(dto.getLosIId());
		entry.setUnitCnr(dto.getEinheitCNr());
		entry.setMountingMethodId(dto.getMontageartIId());
		entry.setBelatedWithdrawn(Helper.short2Boolean(dto.getBNachtraeglich()));
		
		return entry;
	}

	public ProductionActualMaterialEntry mapEntry(LossollmaterialDto sollDto, LosistmaterialDto[] istDtos, boolean isSerieChargeTragend) {
		ProductionActualMaterialEntry entry = new ProductionActualMaterialEntry();
		entry.setId(sollDto.getIId());
		entry.setProductionId(sollDto.getLosIId());
		entry.setUnitCnr(sollDto.getEinheitCNr());
		entry.setAmount(sollDto.getNMenge());
		entry.setBelatedWithdrawn(Helper.short2Boolean(sollDto.getBNachtraeglich()));
		if (isSerieChargeTragend) {
			ArrayList<IdentityAmountEntry> identityAmountEntries = new ArrayList<>();
			for (LosistmaterialDto istDto : istDtos) {
				try {
					List<SeriennrChargennrMitMengeDto> allSnrs = lagerCall.getAllSeriennrchargennrEinerBelegartposition(LocaleFac.BELEGART_LOS, istDto.getIId());
					for (SeriennrChargennrMitMengeDto dto : allSnrs) {
						IdentityAmountEntry identityAmountEntry = new IdentityAmountEntry();
						identityAmountEntry.setIdentity(dto.getCSeriennrChargennr());
						identityAmountEntry.setAmount(dto.getNMenge());
						identityAmountEntries.add(identityAmountEntry);
					}
				} catch (RemoteException e) {
					// keine Chargen, ignorieren
				}
			}
			entry.setIdentities(identityAmountEntries);
		}
		return entry;
	}

	public ProductionEntry mapEntryIdCnr(LosDto losDto) {
		ProductionEntry entry = new ProductionEntry();
		entry.setId(losDto.getIId());
		entry.setCnr(losDto.getCNr());
		return entry;
	}
	
	public List<ProductionEntry> mapEntriesIdCnr(List<LosDto> losDtos) {
		List<ProductionEntry> entries = new ArrayList<>();
		for (LosDto dto : losDtos) {
			entries.add(mapEntryIdCnr(dto));
		}
		return entries;
	}
}
