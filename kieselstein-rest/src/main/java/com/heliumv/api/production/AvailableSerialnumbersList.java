package com.heliumv.api.production;


import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AvailableSerialnumbersList {
    private String[] entries;

    public String[] getEntries() {
        return entries;
    }

    public void setEntries(String[] entries) {
        this.entries = entries;
    }
}
