package com.heliumv.api.production;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
/**
 * Losnachkalkulation als PDF
 */
public class ProductionOrderFinalCosting {
    private byte[] pdfContent;

    public byte[] getPdfContent() {
        return pdfContent;
    }
    public void setPdfContent(byte[] pdfContent) {
        this.pdfContent = pdfContent;
    }
}
