package com.heliumv.api.production;

import javax.xml.bind.annotation.XmlRootElement;

    @XmlRootElement
/**
 * Produktionsinformationen als PDF
 */
    public class ProductionOrderInformation {
        private byte[] pdfContent;

        public byte[] getPdfContent() {
            return pdfContent;
        }
        public void setPdfContent(byte[] pdfContent) {
            this.pdfContent = pdfContent;
        }
}
