package com.heliumv.api.interfaces;

import com.heliumv.api.BaseEntryId;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "InterfaceRun")
public class InterfaceRunEntry extends BaseEntryId {
	private Integer interfaceID;
	private String interfaceCaption;
	private String startTimeStamp;
	private String endTimeStamp;
	private InterfaceRunStatusPropertyEnum status;
	private Integer countHandledData;
	private String errorMessage;

	public Integer getInterfaceID() {
		return interfaceID;
	}

	public void setInterfaceID(Integer interfaceID) {
		this.interfaceID = interfaceID;
	}

	public String getInterfaceCaption() {
		return interfaceCaption;
	}

	public void setInterfaceCaption(String interfaceCaption) {
		this.interfaceCaption = interfaceCaption;
	}

	public String getStartTimeStamp() {
		return startTimeStamp;
	}

	public void setStartTimeStamp(String startTimeStamp) {
		this.startTimeStamp = startTimeStamp;
	}

	public String getEndTimeStamp() {
		return endTimeStamp;
	}

	public void setEndTimeStamp(String endTimeStamp) {
		this.endTimeStamp = endTimeStamp;
	}

	public InterfaceRunStatusPropertyEnum getStatus() {
		return status;
	}

	public void setStatus(InterfaceRunStatusPropertyEnum status) {
		this.status = status;
	}

	public Integer getCountHandledData() {
		return countHandledData;
	}

	public void setCountHandledData(Integer countHandledData) {
		this.countHandledData = countHandledData;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
