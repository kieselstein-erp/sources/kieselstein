package com.heliumv.api.interfaces;

import javax.xml.bind.annotation.XmlRootElement;

import static com.lp.server.system.service.GenericInterfacesFac.*;

@XmlRootElement
public enum InterfaceRunStatusPropertyEnum {
	STARTED(SST_LAUF_STATUS_STARTED),
	SUCCESSFULLY_FINISHED(SST_LAUF_STATUS_FINISHED),
	ERROR(SST_LAUF_STATUS_ERROR),
	UNKNOWN(-99) ;

	InterfaceRunStatusPropertyEnum(int value) {
		this.value = value;
	}

	public static InterfaceRunStatusPropertyEnum valueOf(short value) {
		if (value == SST_LAUF_STATUS_STARTED) {
			return STARTED;
		} else if (value == SST_LAUF_STATUS_FINISHED) {
			return SUCCESSFULLY_FINISHED;
		} else if (value == SST_LAUF_STATUS_ERROR) {
			return ERROR;
		}
		return UNKNOWN;
	}

	private int value;
}
