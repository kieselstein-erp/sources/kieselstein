package com.heliumv.api.interfaces;

import com.heliumv.api.BaseApi;
import com.heliumv.factory.IGenericInterfacesFac;
import com.lp.server.system.service.GenericInterfacesFac;
import com.lp.server.system.service.SSTExportResult;
import com.lp.server.system.service.SSTLaufDto;
import com.lp.util.EJBExceptionLP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.lp.server.system.service.GenericInterfacesFac.SST_LAUF_STATUS_FINISHED;

/**
 * TODO DSK Create Documentation.
 */
@Service("kieselsteinInterfaces")
@Path("/api/v1/interfaces")
public class InterfacesApi extends BaseApi implements IInterfacesAPI {

	@Autowired
	private IGenericInterfacesFac sstCall;

	private String[] parseContentTypeHeaderToFormat(String contentType) {
		if (contentType != null) {
			String[] parts = contentType.split(",");
			for (String part : parts) {
				String type = part.toLowerCase().trim();
				double priority = 1.0;  // TODO Handle Priority
				if (type.startsWith("application/xml")) {
					return new String[]{GenericInterfacesFac.SST_FORMAT_XML, FORMAT_XML};
				} else if (type.startsWith("text/xml")) {
					return new String[]{GenericInterfacesFac.SST_FORMAT_XML, "text/xml;charset=UTF-8"};
				} else if (type.startsWith("text/csv")) {
					return new String[]{GenericInterfacesFac.SST_FORMAT_CSV, "text/csv;charset=UTF-8"};
				}
			}
		}
		return new String[]{null, null};
	}

	private String parseContentTypeHeaderFromFormat(String format) {
		if (format != null) {
			if (GenericInterfacesFac.SST_FORMAT_XML.equals(format)) {
				return FORMAT_XML;
			} else if (GenericInterfacesFac.SST_FORMAT_CSV.equals(format)) {
				return "text/csv;charset=UTF-8";
			}
		}
		return null;
	}

	private Response handleEJBException(EJBExceptionLP e, Integer interfaceID) {
		if (EJBExceptionLP.FEHLER_BEI_FINDBYPRIMARYKEY == e.getCode() || EJBExceptionLP.FEHLER_DTO_IS_NULL == e.getCode()) {
			Response.ResponseBuilder respBuilder = Response.status(Response.Status.NOT_FOUND.getStatusCode());
			respBuilder.header(X_HV_ERROR_CODE, HvErrorCode.UNKNOWN_ENTITY.toString());
			respBuilder.header(X_HV_ERROR_KEY, "interfaceID");
			respBuilder.header(X_HV_ERROR_VALUE, interfaceID.toString());
			return respBuilder.build();
		} else if (EJBExceptionLP.FEHLER_INVALID_TYPE == e.getCode()) {
			Response.ResponseBuilder respBuilder = Response.status(422);
			respBuilder.header(X_HV_ERROR_CODE, HvErrorCode.VALIDATION_FAILED.toString());
			return respBuilder.build();
		} else if (EJBExceptionLP.FEHLER_PERMISSION_DENIED == e.getCode()) {
			Response.ResponseBuilder respBuilder = Response.status(Response.Status.FORBIDDEN.getStatusCode());
			return respBuilder.build();
		} else {
			throw e;
		}
	}

	private InterfaceRunEntry createResponseEntry(SSTLaufDto sstRun) {
		InterfaceRunEntry result = new InterfaceRunEntry();
		result.setId(sstRun.getIId());
		result.setInterfaceID(sstRun.getSstId());
		if (sstRun.getSST() != null) {
			result.setInterfaceCaption(sstRun.getSST().getBezeichnung());
		}
		result.setStatus(InterfaceRunStatusPropertyEnum.valueOf(sstRun.getStatus()));
		result.setCountHandledData(sstRun.getAnzahlDatensaetze());
		if (sstRun.getZeitstempelStart() != null) {
			result.setStartTimeStamp(sstRun.getZeitstempelStart().toString());
		}
		if (sstRun.getZeitstempelEnde() != null) {
			result.setEndTimeStamp(sstRun.getZeitstempelEnde().toString());
		}

		return result;
	}


	@Path("/{interfaceID}/get-data")
	@GET
	public Response getExportResponse(
			@PathParam("interfaceID") Integer interfaceID,
			@QueryParam(Param.USERID) String userId,
			@HeaderParam("Accept") String acceptContentType,
			@Context UriInfo uriInfo
	) {
		if(connectClient(userId) == null) return null;

		Map<String, List<String>> queryParams = new HashMap<>(uriInfo.getQueryParameters());
		String[] formatAndContentType = parseContentTypeHeaderToFormat(acceptContentType);
		SSTExportResult result;
		try {
			result = sstCall.doSSTExport(interfaceID, formatAndContentType[0], queryParams);
		} catch (EJBExceptionLP e) {
			return handleEJBException(e, interfaceID);
		}

		Response.ResponseBuilder respBuilder;
		if (SST_LAUF_STATUS_FINISHED == result.getSstRun().getStatus()) {
			StreamingOutput so = os -> {
				if (result.getData() != null) {
					os.write(result.getData());
				}
			};

			respBuilder = Response.ok(so);
			if (formatAndContentType[1] != null) {
				respBuilder.header("Content-Type", formatAndContentType[1]);
			} else if (result.getFormat() != null) {
				String contentType = parseContentTypeHeaderFromFormat(result.getFormat());
				if (contentType != null) {
					respBuilder.header("Content-Type", contentType);
				}
			}
		} else {
			// Handle Errors
			respBuilder = Response.serverError();
			respBuilder.header(X_HV_ERROR_CODE, HvErrorCode.EJB_EXCEPTION.toString());
			respBuilder.header(X_HV_ERROR_KEY, "interface_export_error");
			respBuilder.header(X_HV_ERROR_VALUE, result.getSstRun().getSmallFehlermeldung());
			respBuilder.entity(createResponseEntry(result.getSstRun()));
		}

		return respBuilder.build();
	}

	@Path("/{interfaceID}/store-data")
	@POST
	public Response getImportResponse(
			@PathParam("interfaceID") Integer interfaceID,
			@HeaderParam("Content-Type") String contentType,
			@HeaderParam("Accept") String acceptContentType,
			@QueryParam(Param.USERID) String userId,
			String payloadData
	) {
		if(connectClient(userId) == null) return null;

		SSTLaufDto sstLaufDto;
		try {
			sstLaufDto = sstCall.doSSTImport(
					interfaceID, parseContentTypeHeaderToFormat(contentType)[0], null, payloadData
			);
		} catch (EJBExceptionLP e) {
			return handleEJBException(e, interfaceID);
		}

		Response.ResponseBuilder respBuilder;
		if (SST_LAUF_STATUS_FINISHED != sstLaufDto.getStatus()) {
			respBuilder = Response.serverError();
			respBuilder.header(X_HV_ERROR_CODE, HvErrorCode.EJB_EXCEPTION.toString());
			respBuilder.header(X_HV_ERROR_KEY, "interface_import_error");
			respBuilder.header(X_HV_ERROR_VALUE, sstLaufDto.getSmallFehlermeldung());
		} else {
			respBuilder = Response.ok();
		}
		respBuilder.entity(createResponseEntry(sstLaufDto));

		return respBuilder.build();
	}
}
