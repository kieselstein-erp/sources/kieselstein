package com.heliumv.api.worktime;

import com.heliumv.api.production.ProductionEntry;
import com.heliumv.api.production.ProductionWorkstepEntry;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BarcodeInfo {
    private ProductionEntry productionEntry;
    private ProductionWorkstepEntry productionWorkstepEntry;
    private Integer machineId;

    public ProductionEntry getProductionEntry() {
        return productionEntry;
    }

    public void setProductionEntry(ProductionEntry productionEntry) {
        this.productionEntry = productionEntry;
    }

    public ProductionWorkstepEntry getProductionWorkstepEntry() {
        return productionWorkstepEntry;
    }

    public void setProductionWorkstepEntry(ProductionWorkstepEntry productionWorkstepEntry) {
        this.productionWorkstepEntry = productionWorkstepEntry;
    }

    public Integer getMachineId() {
        return machineId;
    }

    public void setMachineId(Integer machineId) {
        this.machineId = machineId;
    }
}
