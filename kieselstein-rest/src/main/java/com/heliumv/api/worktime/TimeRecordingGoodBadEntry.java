package com.heliumv.api.worktime;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TimeRecordingGoodBadEntry extends TimeRecordingEntry {

    private Boolean isGoing = Boolean.FALSE;
    private GoodBadEntryList goodBadEntries;

    public Boolean getIsGoing() { return isGoing; }
    public GoodBadEntryList getGoodBadEntries() {
        return goodBadEntries;
    }

    public void setGoodBadEntries(Boolean isGoing, GoodBadEntryList goodBadEntries) {
        this.isGoing = isGoing;
        this.goodBadEntries = goodBadEntries;
    }
}
