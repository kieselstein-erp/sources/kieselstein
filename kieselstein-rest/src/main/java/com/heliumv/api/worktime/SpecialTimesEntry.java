package com.heliumv.api.worktime;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SpecialTimesEntry {
	private SpecialTimesEnum timeType;
	private long fromDateMs;
	private long toDateMs;
	private boolean halfDay;
	private Integer forStaffId ;

	public SpecialTimesEnum getTimeType() {
		return timeType;
	}
	public void setTimeType(SpecialTimesEnum timeType) {
		this.timeType = timeType;
	}
	public long getFromDateMs() {
		return fromDateMs;
	}
	public void setFromDateMs(long fromDateMs) {
		this.fromDateMs = fromDateMs;
	}
	public long getToDateMs() {
		return toDateMs;
	}
	public void setToDateMs(long toDateMs) {
		this.toDateMs = toDateMs;
	}
	public boolean isHalfDay() {
		return halfDay;
	}
	public void setHalfDay(boolean halfDay) {
		this.halfDay = halfDay;
	}

	/**
	 * Die (optionale) PersonalId f&uuml;r die die Buchung gilt
	 * @return die PersonalId f&uuml;r die die Buchung gelten soll
	 */
	public Integer getForStaffId() { return forStaffId; }
	public void setForStaffId(Integer forStaffId) { this.forStaffId = forStaffId; }
}
