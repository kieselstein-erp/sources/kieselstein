package com.heliumv.api.worktime;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TimeDistributionRecordingEntry  extends  TimeRecordingEntry {
    private Integer productionId ;
    private Integer machineId;
    private Integer productionWorkplanId;
    private Integer workItemId;

    public Integer getProductionId() {
        return productionId;
    }
    public void setProductionId(Integer productionId) {
        this.productionId = productionId;
    }
    public Integer getMachineId() {
        return machineId;
    }
    public void setMachineId(Integer machineId) {
        this.machineId = machineId;
    }
    public Integer getProductionWorkplanId() {
        return productionWorkplanId;
    }
    public void setProductionWorkplanId(Integer productionWorkplanId) {
        this.productionWorkplanId = productionWorkplanId;
    }
    public Integer getWorkItemId() { return workItemId; }
    public void setWorkItemId(Integer workItemId) { this.workItemId = workItemId; }
}
