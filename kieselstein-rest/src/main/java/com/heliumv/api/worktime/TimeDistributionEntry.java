package com.heliumv.api.worktime;

import com.heliumv.api.BaseEntryId;
import com.heliumv.api.production.ProductionEntry;
import com.heliumv.api.production.ProductionWorkstepEntry;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement
public class TimeDistributionEntry extends BaseEntryId {
    private Timestamp tTimeMs;
    private ProductionEntry productionEntry;
    private ProductionWorkstepEntry productionWorkstepEntry;
    private Integer machineIdToUse;

    public Integer getMachineIdToUse() {
        return machineIdToUse;
    }

    public void setMachineIdToUse(Integer machineIdToUse) {
        this.machineIdToUse = machineIdToUse;
    }

    public ProductionWorkstepEntry getProductionWorkstepEntry() {
        return productionWorkstepEntry;
    }

    public void setProductionWorkstepEntry(ProductionWorkstepEntry productionWorkstepEntry) {
        this.productionWorkstepEntry = productionWorkstepEntry;
    }

    public ProductionEntry getProductionEntry() {
        return productionEntry;
    }

    public void setProductionEntry(ProductionEntry productionEntry) {
        this.productionEntry = productionEntry;
    }

    public Timestamp getTTimeMs() {
        return tTimeMs;
    }

    public void setTTimeMs(Timestamp tZeit) {
        this.tTimeMs = tZeit;
    }

}
