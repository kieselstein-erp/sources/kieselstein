package com.heliumv.api.worktime;

import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

@XmlRootElement
public class GoodBadEntry {
    private Integer workstepId = null;
    private BigDecimal goodAmount = null;
    private BigDecimal badAmount = null;
    private Boolean finished = false;

    public GoodBadEntry() {};

    public GoodBadEntry(Integer workstepId, BigDecimal goodAmount, BigDecimal badAmount, Boolean finished) {
        this.workstepId = workstepId;
        this.goodAmount = goodAmount;
        this.badAmount = badAmount;
        this.finished = finished;
    }
    public Integer getWorkstepId() {
        return workstepId;
    }
    public BigDecimal getGoodAmount() {
        return goodAmount;
    }
    public BigDecimal getBadAmount() {
        return badAmount;
    }
    public Boolean getFinished() {return  finished; }

}
