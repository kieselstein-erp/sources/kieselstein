package com.heliumv.api.worktime;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class GoodBadEntryList {
    private List<GoodBadEntry> entries;

    public GoodBadEntryList() {
        this(new ArrayList<GoodBadEntry>());
    }

    public GoodBadEntryList(List<GoodBadEntry> entries) {
        setEntries(entries);
    }

    /**
     * Die (leere) Liste aller GutSchlecht-Eintr&auml;ge
     * @return die (leere) Liste aller GutSchlecht Eintr&auml;ge
     */
    public List<GoodBadEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<GoodBadEntry> entries) {
        if(entries == null) {
            entries = new ArrayList<GoodBadEntry>();
        }
        this.entries = entries;
    }

}
