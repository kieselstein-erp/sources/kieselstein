package com.heliumv.api.machine;

import com.heliumv.api.BaseFLRTransformerFeatureData;
import com.lp.server.personal.service.IMaschineFLRData;
import com.lp.server.system.fastlanereader.service.TableColumnInformation;

public class MachineEntryTransformer extends BaseFLRTransformerFeatureData<MachineEntry, IMaschineFLRData> {

	@Override
	public MachineEntry transformOne(Object[] flrObject,
			TableColumnInformation columnInformation) {
		MachineEntry entry = new MachineEntry(flrObject[columnInformation.getViewIndex("id")]) ;
		entry.setInventoryNumber((String) flrObject[columnInformation.getViewIndex("pers.inventarnummer")]) ;
		entry.setDescription((String) flrObject[columnInformation.getViewIndex("lp.bezeichnung")]) ;
		entry.setIdentificationNumber((String) flrObject[columnInformation.getViewIndex("pers.zeiterfassung.identifikationsnr")]) ;
		entry.setMachineGroupId((Integer) flrObject[columnInformation.getViewIndex("maschinengruppe.id")]) ;
		entry.setMachineGroupDescription((String) flrObject[columnInformation.getViewIndex("maschinengruppe.bezeichnung")]) ;
		entry.setMachineGroupShortDescription((String) flrObject[columnInformation.getViewIndex("maschinengruppe.kbez")]);
		entry.setMachineGroupISort((Integer) flrObject[columnInformation.getViewIndex("isort")]);
		
		return entry;
	}

	@Override
	protected void transformFlr(MachineEntry entry, IMaschineFLRData flrData) {
		if (flrData == null) return;
		
		entry.setPersonalIdStarter(flrData.getPersonalIdStarter());
		entry.setStarttime(flrData.getGestartetUm());
		entry.setProductionWorkplanId(flrData.getLossollarbeitsplanIId());
	}
}
