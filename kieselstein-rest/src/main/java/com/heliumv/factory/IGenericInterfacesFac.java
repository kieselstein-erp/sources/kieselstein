package com.heliumv.factory;

import com.lp.server.system.service.SSTExportResult;
import com.lp.server.system.service.SSTLaufDto;

import java.util.List;
import java.util.Map;

public interface IGenericInterfacesFac {

	SSTExportResult doSSTExport(Integer sstID, String format, Map<String, List<String>> queryParams);

	SSTLaufDto doSSTImport(Integer sstID, String format, Map<String, List<String>> queryParams, String payloadData);
}
