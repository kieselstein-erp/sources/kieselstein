/*******************************************************************************
 * TODO
 ******************************************************************************/
package com.heliumv.factory.impl;

import com.heliumv.factory.BaseCall;
import com.heliumv.factory.IGenericInterfacesFac;
import com.heliumv.factory.IGlobalInfo;
import com.lp.server.system.service.GenericInterfacesFac;
import com.lp.server.system.service.SSTExportResult;
import com.lp.server.system.service.SSTLaufDto;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;


public class GenericInterfacesCall extends BaseCall<GenericInterfacesFac> implements IGenericInterfacesFac {

	@Autowired
	private IGlobalInfo globalInfo ;

	public GenericInterfacesCall() {
		super(GenericInterfacesFac.class) ;
	}

	public SSTExportResult doSSTExport(Integer sstID, String format, Map<String, List<String>> queryParams) {
		GenericInterfacesFac fac = getFac();
		SSTExportResult result = fac.doSSTExportForRESTCallToBytes(sstID, format, queryParams, globalInfo.getTheClientDto());
		return result;
	}

	public SSTLaufDto doSSTImport(Integer sstID, String format, Map<String, List<String>> queryParams, String payloadData) {
		GenericInterfacesFac fac = getFac();
		SSTLaufDto sstLaufDto = fac.doSSTImportForRESTCallFromString(sstID, format, queryParams, payloadData, globalInfo.getTheClientDto());
		return sstLaufDto;
	}
}
