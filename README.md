# Kieselstein

![Kieselstein](./logo.png)

[![GitLab Release](https://gitlab.com/kieselstein-erp/sources/kieselstein/-/badges/release.svg)](https://gitlab.com/kieselstein-erp/sources/kieselstein/-/releases/permalink/latest)
[![GitLab Pipeline](https://gitlab.com/kieselstein-erp/sources/kieselstein/badges/develop/pipeline.svg?ignore_skipped=true)](https://gitlab.com/kieselstein-erp/sources/kieselstein/-/pipelines/latest)
![GitLab Coverage](https://gitlab.com/kieselstein-erp/sources/kieselstein/badges/develop/coverage.svg)

[Dashboard](https://kieselstein-erp.gitlab.io/sources/kieselstein/develop/dashboard/index.html)

Kieselstein is an Open-Source ERP system. It is originally a clone of [HeliumV](https://github.com/heliumv).

## License

Kieselstein is licensed under the [GNU Affero General Public License](LICENSE).