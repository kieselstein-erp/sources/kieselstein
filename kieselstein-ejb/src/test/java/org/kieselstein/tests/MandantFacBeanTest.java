package org.kieselstein.tests;

import java.net.URL;
import java.sql.Timestamp;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Logger;

import com.lp.server.benutzer.service.LogonFac;
import com.lp.server.system.ejbfac.MandantFacBean;
import com.lp.server.system.service.MandantFac;
import com.lp.server.system.service.TheClientDto;
import com.lp.server.util.ServerConfiguration;
import com.lp.util.EJBExceptionLP;
import com.lp.util.Helper;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit5.ArquillianExtension;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.gradle.archive.importer.embedded.EmbeddedGradleImporter;
import org.jboss.shrinkwrap.api.spec.EnterpriseArchive;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.impl.gradle.Gradle;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(ArquillianExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MandantFacBeanTest {
    private final static Logger LOGGER = Logger.getLogger(MandantFacBeanTest.class.getName());

    @ArquillianResource
    private URL deploymentUrl;

    @PersistenceContext
    private EntityManager entityManager;

    @Resource(lookup = "java:/KSDS")
    private DataSource dataSource;

    @EJB
    private MandantFac mandantFac;

    @EJB
    private LogonFac logonFac;

    private TheClientDto client;

    @Deployment
    public static EnterpriseArchive createDeployment() {
        JavaArchive ja = ShrinkWrap.create(EmbeddedGradleImporter.class, "ejb-jar.jar")
                .forThisProjectDirectory()
                .importBuildOutput()
                .as(JavaArchive.class);

        WebArchive wa = ShrinkWrap.create(WebArchive.class, "test.war")
                .addClasses(MandantFacBeanTest.class, MandantFac.class, MandantFacBean.class)
                .addAsWebInfResource("beans.xml");

        Archive<?>[] archives = Gradle.resolver()
                .forProjectDirectory(".")
                .importCompileAndRuntime()
                .resolve()
                .as(JavaArchive.class);

        return ShrinkWrap.create(EnterpriseArchive.class)
                .addAsModule(ja)
                .addAsModule(wa)
                .addAsLibraries(archives)
                .setApplicationXML("application.xml")
                .addAsManifestResource("jboss-deployment-structure.xml");
    }

    @BeforeEach
    public void prepare() throws Exception {
        final String user = "Admin" + LogonFac.USERNAMEDELIMITER + Helper.getPCName() + LogonFac.USERNAMEDELIMITER;
        final String name = user.substring(0, user.indexOf("|"));
        final String password = "admin";

        client = logonFac.logon(user, Helper
                        .getMD5Hash((name + password).toCharArray()),
                new Locale("de", "AT"), "001", ServerConfiguration.getApplicationVersion(),
                new Timestamp(System.currentTimeMillis()));
    }

    @Test
    public void getLocaleDesHauptmandanten() throws Exception {
        Locale l = mandantFac.getLocaleDesHauptmandanten();
        assertEquals("de_AT", l.toString());
    }

    @Test
    public void mandantFindByPrimaryKey() throws Exception {
        assertThrows(EJBExceptionLP.class, () -> mandantFac.mandantFindByPrimaryKey(null, client));

        assertThrows(EJBExceptionLP.class, () -> mandantFac.mandantFindByPrimaryKey("SHOULD_NOT_EXIST", client));

        assertNotNull(mandantFac.mandantFindByPrimaryKey("001", client));
    }

    @Test
    public void testWithLookup() throws Exception {
        Properties jndiProperties = createJndiProperties(deploymentUrl.getHost(), 8080);
        Context ctx = new InitialContext(jndiProperties);
        // Example: lpserver/ejb/MandantFacBean!com.lp.server.system.service.MandantFac
        MandantFac mandantFac = (MandantFac) ctx.lookup("lpserver/ejb/" + MandantFac.class.getSimpleName() + "Bean!" + MandantFac.class.getName());
        Locale l = mandantFac.getLocaleDesHauptmandanten();
        System.out.println("[Lookup] Locale:" + l);
    }

    private Properties createJndiProperties(String host, int port) {
        Properties jndiProperties = new Properties();
        jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
        String providerUrl = String.format("remote+http://%s:%d/", host, port);
        jndiProperties.put(Context.PROVIDER_URL, providerUrl);
        return jndiProperties;
    }
}