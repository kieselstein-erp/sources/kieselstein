package com.lp.server.eingangsrechnung.bl.erechnung;

import oasis.names.specification.ubl.schema.xsd.invoice_2.InvoiceType;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;

class ERechnungUnmarshallerFacadeTest {


    @Test
    void it_should_succeed() throws IOException, ParserConfigurationException, SAXException, JAXBException {
        var path = "/" + getClass().getPackageName().replace('.','/') + "/";
        var fileName = "UblInvoice.xml";
        String xml = IOUtils.toString(getClass().getResourceAsStream(path + fileName), StandardCharsets.UTF_8);

        Object object = ERechnungUnmarshallerFacade.getInstance().transform(xml);

        assertThat(object).isInstanceOf(InvoiceType.class);
    }
}