package com.lp.server.eingangsrechnung.ejbfac;

import com.lp.server.eingangsrechnung.service.ERechnungDto;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ERechnungFacBeanTest {

    @Test
    void it_should_succeed() throws IOException, ParseException {
        // ARRANGE
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

        ERechnungDto expected = new ERechnungDto();
        //Rechnungsdatum
        expected.setBelegdatum(new java.sql.Date(sdf.parse("25.06.2024").getTime()));
        //Rechnungsnummer
        expected.setLieferantenRechnungsnummer("5802451964");
        //Nettobetrag
        expected.setNettoBetrag(new BigDecimal("207.14").setScale(2, RoundingMode.HALF_UP));
        expected.setLieferantIban("DE25600501010002077055");
        expected.setLieferantenRechnungsnummer("5802451964");
        expected.setWaehrung("EUR");
        expected.setRechnungsArt("380");
        expected.setUstBetrag(new BigDecimal("39.36").setScale(2, RoundingMode.HALF_UP));
        expected.setBruttoBetrag(new BigDecimal("246.50").setScale(2, RoundingMode.HALF_UP));
        expected.setKaeuferUid("DE279130624");
        expected.setLieferantBic("SOLADEST600");
        expected.setVerkaeuferUid("DE811502037");
        var stammdaten = new ERechnungDto.LieferantStammdaten();
        stammdaten.setLandKurz("DE");
        stammdaten.setName("HAHN+KOLB Werkzeuge GmbH");
        stammdaten.setPlz("71636");
        stammdaten.setOrt("Ludwigsburg");
        stammdaten.setStrasse("Schlieffenstraße 40");
        expected.setLieferantStammdaten(stammdaten);

        var steuer = new ERechnungDto.ERechnungSteuerDto("S", new BigDecimal("19"), new BigDecimal("39.36"));
        expected.setSteuerSaetze(List.of(steuer));

        var path = "/" + getClass().getPackageName().replace(".","/") + "/";
        var fileName = "UblInvoice.xml";
        var xml = IOUtils.toByteArray(getClass().getResourceAsStream(path + fileName));

        // ACT
        ERechnungDto actual = new ERechnungFacBean().parseFileContentToDto(xml);

        // ASSERT
        assertThat(actual).isNotNull();
        assertThat(actual)
                .usingComparatorForType(BigDecimal::compareTo, BigDecimal.class)
                .usingRecursiveComparison()
                .ignoringFields("pdfContent", "xmlContent")
                .isEqualTo(expected);

        //TODO assertThat(actual.getPdfContent()).hasSize(54421);
        assertThat(new String(actual.getPdfContent(), StandardCharsets.UTF_8)).startsWith("%PDF");
        //TODO assertThat(actual.getXmlContent()).startsWith("<?xml").hasSize(10381);
    }
}
