@XmlSchema(
    namespace = "urn:iso:std:iso:20022:tech:xsd:camt.053.001.02",
    elementFormDefault = XmlNsForm.QUALIFIED)
package com.lp.server.schema.iso20022.camt053V02;

import javax.xml.bind.annotation.*;
