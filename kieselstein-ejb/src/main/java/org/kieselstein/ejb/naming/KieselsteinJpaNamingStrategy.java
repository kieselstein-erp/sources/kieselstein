
package org.kieselstein.ejb.naming;

import org.apache.log4j.Logger;
import org.hibernate.boot.model.naming.*;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.joining;


public class KieselsteinJpaNamingStrategy extends ImplicitNamingStrategyJpaCompliantImpl {

    private static Logger LOGGER = Logger.getLogger(KieselsteinJpaNamingStrategy.class);

    private static final long serialVersionUID = 1L;

    private static final String DELIMITER3 = "_";
    private static final String DELIMITER2 = "_";
    private static final String DELIMITER1 = "_";

    @Override
    public Identifier determineUniqueKeyName(ImplicitUniqueKeyNameSource source) {
        Identifier userProvidedIdentifier = null; //source.getUserProvidedIdentifier();

        Identifier identifier = userProvidedIdentifier == null
                ? toIdentifier(generateHashedConstraintName("UK", source.getTableName(), source.getColumnNames()),
                source.getBuildingContext())
                : userProvidedIdentifier;

        LOGGER.info("Generated uk name='" + identifier + "'.");
        return identifier;
    }

    @Override
    public Identifier determinePrimaryKeyJoinColumnName(ImplicitPrimaryKeyJoinColumnNameSource source) {
        Identifier identifier = toIdentifier("PK_" + source.getReferencedTableName() +
                "_" + source.getReferencedPrimaryKeyColumnName(), source.getBuildingContext());
        LOGGER.info("Generated pk name='" + identifier + "'.");
        return identifier;
    }

    @Override
    public Identifier determineForeignKeyName(ImplicitForeignKeyNameSource source) {
        String string = "FK_" + source.getTableName() +
                "_" + colString(source.getColumnNames()) +
                "_" + source.getReferencedTableName() +
                "_" + colString(source.getReferencedColumnNames());

        if (string.length() > 63) {
            string = string.substring(0, 63);
        }
        Identifier identifier = toIdentifier(string, source.getBuildingContext());
        LOGGER.info("Generated fk name='" + identifier + "'.");
        if (identifier.getText().length() > 63) {
            throw new UnsupportedOperationException("Identifier '" + identifier + "' is longer than chars.");
        }
        return identifier;
    }

    private static String colString(List<Identifier> columns) {
        if (columns.isEmpty()){
            return "UNKNOWN";
        }
        return columns.stream()
                .sorted(comparing(Identifier::getCanonicalName))
                .map(Objects::toString)
                .collect(joining("_"));
    }

    private static String generateHashedConstraintName(final String prefix, final Identifier tableName,
                                                       final List<Identifier> columnNames) {
        // final String tn = cutSchema(tableName);
        String name = prefix + DELIMITER1 + tableName;

        for (final Identifier columnName : columnNames)
            name += DELIMITER2 + columnName;

        return name; // checkLen(name);
    }




}
