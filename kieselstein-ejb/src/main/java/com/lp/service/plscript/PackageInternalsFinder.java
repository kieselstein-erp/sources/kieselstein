package com.lp.service.plscript;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.jar.JarEntry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.tools.JavaFileObject;

/**
 * @author atamur
 * @since 15-Oct-2009
 */
class PackageInternalsFinder {
	private final ClassLoader classLoader;
	private static final String CLASS_FILE_EXTENSION = ".class";
	private static final Map<String, String> deflatedJars = new HashMap<>();
	private static final ReentrantLock deflatedLock = new ReentrantLock();
	
	public PackageInternalsFinder(ClassLoader classLoader) {
		this.classLoader = classLoader;
	}

	public List<JavaFileObject> find(String packageName) throws IOException {
		String javaPackageName = packageName.replaceAll("\\.", "/");

		List<JavaFileObject> result = new ArrayList<>();

		Enumeration<URL> urlEnumeration = classLoader.getResources(javaPackageName);
		while (urlEnumeration.hasMoreElements()) { // one URL for each jar on the classpath that has the given package
			URL packageFolderURL = urlEnumeration.nextElement();
			result.addAll(listUnder(packageName, packageFolderURL));
		}

		return result;
	}

	private Collection<JavaFileObject> listUnder(String packageName, URL packageFolderURL) {
		File directory = new File(packageFolderURL.getFile());
		if (directory.isDirectory()) { // browse local .class files - useful for local execution
			return processDir(packageName, directory);
		} else { // browse a jar file
			return processJar(packageFolderURL);
		} // maybe there can be something else for more involved class loaders
	}

	private List<JavaFileObject> processJar(URL packageFolderURL) {
		try {
			URLConnection urlConnection = packageFolderURL.openConnection();
			if(urlConnection instanceof JarURLConnection) {
				return processJarUrl(packageFolderURL);
			} else {
				return processVfsUrl(packageFolderURL);
			}
		} catch (Exception e) {
			throw new RuntimeException("Wasn't able to open " + packageFolderURL + " as a jar file", e);
		}
	}

	private List<JavaFileObject> processJarUrl(URL packageFolderURL) throws Exception {
		String jarUri = packageFolderURL.toExternalForm().split("!")[0];
		JarURLConnection jarConn = (JarURLConnection) packageFolderURL.openConnection();
		return processJarUrlConnection(jarUri, jarConn);
	}

	private List<JavaFileObject> processJarUrlConnection(
			String jarUri, JarURLConnection jarConn) throws Exception {
		String rootEntryName = jarConn.getEntryName();
		if(rootEntryName == null) {
			rootEntryName = "";
		}
		int rootEnd = rootEntryName.length() + 1;

		List<JavaFileObject> result = new ArrayList<>();
		Enumeration<JarEntry> entryEnum = jarConn.getJarFile().entries();
		while (entryEnum.hasMoreElements()) {
			JarEntry jarEntry = entryEnum.nextElement();
			String name = jarEntry.getName();
			
			if (name.startsWith(rootEntryName) && name.indexOf('/', rootEnd) == -1
					&& name.endsWith(CLASS_FILE_EXTENSION)) {
				URI uri = URI.create(jarUri + "!/" + name);
				String binaryName = name.replaceAll("/", ".");
				binaryName = binaryName.replaceAll(CLASS_FILE_EXTENSION + "$", "");

				result.add(new CustomJavaFileObject(binaryName, uri));
			}
		}
		
		return result;	
	}
	
	
	private File deflateJar(ZipEntry zipEntry, InputStream is) throws IOException {
		deflatedLock.lock();

		try {
			String filename = deflatedJars.get(zipEntry.getName());
			if(filename != null) {
				File f = new File(filename);
				f.deleteOnExit();
				return f;
			}

			File f = File.createTempFile("hv_" + zipEntry.getName()
					.replace("/", "_")
					.replace(".jar", ""), ".jar");
			f.deleteOnExit();
			FileOutputStream fos = new FileOutputStream(f);

			byte[] buffer = new byte[8192];
			int count;
			while((count = is.read(buffer)) != -1) {
				fos.write(buffer, 0, count);
			}
			
			fos.close();
			
			deflatedJars.put(zipEntry.getName(), f.getAbsolutePath());
			return f;
		} finally {
			deflatedLock.unlock();
		}
	}

	private List<JavaFileObject> processJarFromFile(File f, String jarPackageName) throws Exception {
		String jarUri = "jar:file:" + f.getAbsolutePath().replace('\\',	'/') + "!/" + jarPackageName;
		URL url = new URL(jarUri);
		jarUri = url.toExternalForm().split("!")[0];
		JarURLConnection jarConn = (JarURLConnection) url.openConnection();

		try {
			// packagename gesucht, der im jar nicht enthalten ist
			ZipEntry z = jarConn.getJarFile().getEntry(jarPackageName);
			if(z == null) {
				return Collections.emptyList();
			}
		} catch(FileNotFoundException e) {
			return Collections.emptyList();
		}

		return processJarUrlConnection(jarUri, jarConn);
	}
	
	private List<JavaFileObject> processVfsUrl(URL packageFolderURL) throws Exception {
		String vfsUri = packageFolderURL.toExternalForm();
		if(!vfsUri.startsWith("vfs:")) {
			return Collections.emptyList();
		}

		final String EAR_PREFIX = "kieselstein";
		final String URI_EAR_PREFIX = "/" + EAR_PREFIX;
		if(vfsUri.contains(URI_EAR_PREFIX) && vfsUri.indexOf(URI_EAR_PREFIX) < vfsUri.indexOf(".ear/")) {
			String earName = null;
			String[] parts = vfsUri.split("/");
			for (String part : parts) {
				if (part.startsWith(EAR_PREFIX) && part.endsWith(".ear")) {
					earName = part;
					break;
				}
			}

			if (earName != null) {
				String jarName = vfsUri.substring(vfsUri.indexOf(earName) + earName.length());
				int jarIndex = jarName.indexOf(".jar");
				if(jarIndex == -1) return Collections.emptyList();

				String jarFilename = jarName.substring(0, jarIndex + 4);
				String jarPackageName = "";
				if(jarName.length() > (jarIndex + 4 + 1)) {
					jarPackageName = jarName.substring(jarIndex + 4 + 1);
				}

				if (vfsUri.startsWith("vfs:") && vfsUri.contains(".jar/")) {
					// Check if the Jar-File exists (for local development).
					File f = new File(vfsUri.substring(4, vfsUri.indexOf(".jar/")) + ".jar");
					if (f.exists()) {
						return processJarFromFile(f, jarPackageName);
					}
				}
				String earFile = "../standalone/deployments/" + earName;

				ZipFile zipFile = new ZipFile(earFile);
				ZipEntry zipEntry = zipFile.getEntry((jarFilename.startsWith("/") ? jarFilename.substring(1) : jarFilename));
				InputStream is = zipFile.getInputStream(zipEntry);

				File f = deflateJar(zipEntry, is);
				zipFile.close();
				return processJarFromFile(f, jarPackageName);
			}
		}
		return Collections.emptyList();
	}
	
	
	private List<JavaFileObject> processDir(String packageName, File directory) {
		List<JavaFileObject> result = new ArrayList<>();
		File[] childFiles = directory.listFiles();
		if (childFiles != null) {
			for (File childFile : childFiles) {
				if (childFile.isFile()) {
					// We only want the .class files.
					if (childFile.getName().endsWith(CLASS_FILE_EXTENSION)) {
						String binaryName = packageName + "." + childFile.getName();
						binaryName = binaryName.replaceAll(CLASS_FILE_EXTENSION + "$", "");
						result.add(new CustomJavaFileObject(binaryName, childFile.toURI()));
					}
				}
			}
		}

		return result;
	}
}