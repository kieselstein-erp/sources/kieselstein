package com.lp.server.eingangsrechnung.bl.erechnung;

import com.lp.server.eingangsrechnung.service.ERechnungDto;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PaymentMeansType;
import oasis.names.specification.ubl.schema.xsd.invoice_2.InvoiceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

class UblInvoiceMapper implements ERechnungMapper<InvoiceType> {

    private static Logger LOG = LoggerFactory.getLogger(UblInvoiceMapper.class) ;

    @Override
    public ERechnungDto map(InvoiceType invoice) {

        ERechnungDto eRechnung = new ERechnungDto();

        try {
            invoice.getPaymentMeans().stream()
                    .filter(p -> p.getPayeeFinancialAccount() != null).findFirst()
                    .map(PaymentMeansType::getPayeeFinancialAccount)
                    .ifPresent(account -> {
                        var iban = account.getID().getValue() != null ?
                                account.getID().getValue().replaceAll("[^a-zA-Z0-9]", "").toUpperCase() : null;
                        eRechnung.setLieferantIban(iban);

                        var bic = Optional.ofNullable(account.getFinancialInstitutionBranch())
                                .map(bank -> bank.getID().getValue())
                                .map(bankBic -> bankBic.replaceAll("[^a-zA-Z0-9]", "").toUpperCase())
                                .orElse(null);
                        eRechnung.setLieferantBic(bic);
                    });

            Date rechnungsDatum = invoice.getIssueDate().getValue().toGregorianCalendar().getTime();

            String waehrung = invoice.getDocumentCurrencyCode().getValue();
            String rechnungsArt = invoice.getInvoiceTypeCode().getValue();

            BigDecimal nettoBetrag = Optional.ofNullable(invoice.getLegalMonetaryTotal())
                    .map(t -> t.getTaxExclusiveAmount().getValue()).orElse(null);
            BigDecimal bruttoBetrag = Optional.ofNullable(invoice.getLegalMonetaryTotal())
                    .map(t -> t.getTaxInclusiveAmount().getValue()).orElse(null);
            BigDecimal nUstBetrag = invoice.getTaxTotal().get(0).getTaxAmount().getValue();
            String lieferantenRechnungsnummer = invoice.getID().getValue();


            eRechnung.setBelegdatum(new java.sql.Date(rechnungsDatum.getTime()));
            eRechnung.setNettoBetrag(nettoBetrag);
            eRechnung.setBruttoBetrag(bruttoBetrag);
            eRechnung.setRechnungsArt(rechnungsArt);
            eRechnung.setWaehrung(waehrung);
            eRechnung.setUstBetrag(nUstBetrag);
            eRechnung.setLieferantenRechnungsnummer(lieferantenRechnungsnummer);

            eRechnung.setSteuerSaetze(invoice.getTaxTotal().stream()
                    .flatMap(total -> total.getTaxSubtotal().stream())
                    .map(subtotal -> new ERechnungDto.ERechnungSteuerDto(
                            subtotal.getTaxCategory().getID().getValue(),
                            subtotal.getTaxCategory().getPercent().getValue(),
                            subtotal.getTaxAmount().getValue()))
                    .collect(Collectors.toList()));

            invoice.getAccountingSupplierParty().getParty().getPartyTaxScheme().stream().findFirst()
                    .ifPresent(partyTaxSchemeType -> eRechnung.setVerkaeuferUid(partyTaxSchemeType.getCompanyID().getValue()));
            invoice.getAccountingCustomerParty().getParty().getPartyTaxScheme().stream().findFirst().ifPresent(
                    partyTaxScheme -> eRechnung.setKaeuferUid(partyTaxScheme.getCompanyID().getValue()));

            eRechnung.setLieferantStammdaten(new ERechnungDto.LieferantStammdaten());
            invoice.getAccountingSupplierParty().getParty().getPartyName().stream().findFirst()
                    .ifPresent(party -> eRechnung.getLieferantStammdaten().setName(party.getName().getValue()));
            var supplierPostalAddress = invoice.getAccountingSupplierParty().getParty().getPostalAddress();
            eRechnung.getLieferantStammdaten().setOrt(supplierPostalAddress.getCityName().getValue());
            eRechnung.getLieferantStammdaten().setStrasse(supplierPostalAddress.getStreetName().getValue());
            eRechnung.getLieferantStammdaten().setPlz(supplierPostalAddress.getPostalZone().getValue());
            eRechnung.getLieferantStammdaten().setLandKurz(supplierPostalAddress.getCountry().getIdentificationCode().getValue());

        } catch (Exception ex) {
            LOG.warn("UBL Invoice could not be completely mapped", ex);
        }

        return eRechnung;
    }
}
