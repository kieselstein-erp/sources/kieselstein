package com.lp.server.eingangsrechnung.bl.erechnung;

import com.lp.server.eingangsrechnung.bl.ERechnungUnmarshaller;
import com.lp.server.util.XMLUnmarshaller;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import un.unece.uncefact.data.standard.crossindustryinvoice._100.CrossIndustryInvoiceType;

import javax.xml.bind.JAXBException;

public class CrossIndustryInvoiceUnmarshaller<T extends CrossIndustryInvoiceType> extends XMLUnmarshaller<T> implements ERechnungUnmarshaller<T> {

    private Class<T> xmlClass;

    public CrossIndustryInvoiceUnmarshaller(Class<T> xmlClass) {
        super();
        this.xmlClass = xmlClass;
    }

    @Override
    public T unmarshal(Document content) throws JAXBException {
        return unmarshal(content, xmlClass);
    }

    @Override
    public T unmarshal(String content) throws JAXBException, SAXException {
        return unmarshal(content, xmlClass);
    }

    @Override
    public Class getTargetType() {
        return CrossIndustryInvoiceType.class;
    }
}
