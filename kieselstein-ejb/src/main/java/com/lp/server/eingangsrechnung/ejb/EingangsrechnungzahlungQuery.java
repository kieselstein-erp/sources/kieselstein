package com.lp.server.eingangsrechnung.ejb;

import java.util.List;

import javax.persistence.EntityManager;

import com.lp.layer.hibernate.HvTypedQuery;

public class EingangsrechnungzahlungQuery {

	public static final String ByIAuszug = "EingangsrechnungzahlungFindByIAuszug";
	
	public static List<Eingangsrechnungzahlung> byIAuszug(EntityManager em, Integer iAuszug) {
		return HvTypedQuery.<Eingangsrechnungzahlung>namedQuery(em, ByIAuszug, iAuszug).getResultList();
	}
}
