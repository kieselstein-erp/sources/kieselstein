package com.lp.server.eingangsrechnung.bl.erechnung;

import com.lp.server.util.logger.ILPLogger;
import com.lp.server.util.logger.LPLogService;
import oasis.names.specification.ubl.schema.xsd.invoice_2.InvoiceType;
import org.apache.commons.io.FilenameUtils;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.xmlgraphics.util.MimeConstants;
import org.xml.sax.SAXException;
import un.unece.uncefact.data.standard.crossindustryinvoice._100.CrossIndustryInvoiceType;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ERechnungPdfCreator {
    private final Map<Class, String> xRechnungXslMap = new HashMap<>();
    private final Map<Class, String> foXslMap = new HashMap<>();
    private final Map<Class, String> pdfConf = new HashMap<>();
    private final TransformerFactory transformerFactory;
    private final Map<Class, FopFactory> fopFactory = new HashMap<>();

    private final ILPLogger log = LPLogService.getInstance().getLogger(this.getClass());

    public ERechnungPdfCreator() {
        xRechnungXslMap.put(InvoiceType.class, "/com/lp/server/res/xrechnung/xsl/ubl-invoice-xr.xsl");
        foXslMap.put(InvoiceType.class, "/com/lp/server/res/xrechnung/xsl/xr-pdf.xsl");
        pdfConf.put(InvoiceType.class, "/com/lp/server/res/xrechnung/fop/fop.xconf");

        xRechnungXslMap.put(CrossIndustryInvoiceType.class, "/com/lp/server/res/xrechnung/xsl/cii-xr.xsl");
        foXslMap.put(CrossIndustryInvoiceType.class, "/com/lp/server/res/xrechnung/xsl/xr-pdf.xsl");
        pdfConf.put(CrossIndustryInvoiceType.class, "/com/lp/server/res/xrechnung/fop/fop.xconf");

        transformerFactory = TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl", null);
        loadFopFactories();
    }

    private void loadFopFactories() {
        pdfConf.forEach((key, value) -> {
            try {
                var basePath = FilenameUtils.getFullPath(value);

                fopFactory.put(key, FopFactory.newInstance(Objects.requireNonNull(
                        this.getClass().getResource(basePath)).toURI(),
                        this.getClass().getResourceAsStream(value)));
            } catch (URISyntaxException | IOException | SAXException ex) {
                log.error(String.format("could not load config for PDF creation: %s", value), ex);
            }
        });
    }

    public byte[] create(byte[] content, Class type) throws Exception {
        byte[] xRechnungContent = xRechnungTransformer(content, xRechnungXslMap.get(type));
        byte[] foContent = foTransformer(xRechnungContent, foXslMap.get(type));
        return pdfTransformer(foContent, fopFactory.get(type));
    }

    private byte[] pdfTransformer(byte[] xRechnungFo, FopFactory fopFactory) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        OutputStream out = new BufferedOutputStream(baos);

        Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, out);
        Transformer transformer = transformerFactory.newTransformer();

        StreamSource src = new StreamSource(new BufferedInputStream(new ByteArrayInputStream(xRechnungFo)));
        SAXResult res = new SAXResult(fop.getDefaultHandler());

        transformer.transform(src, res);
        out.flush();
        return baos.toByteArray();
    }

    private byte[] xRechnungTransformer(byte[] invoice, String xslPath) throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        InputStream isXsl = this.getClass().getResourceAsStream(xslPath);
        StreamSource xslSource = new StreamSource(isXsl);
        xslSource.setSystemId(Objects.requireNonNull(this.getClass().getResource(xslPath)).toURI().toString());

        StreamSource streamSource = new StreamSource(new ByteArrayInputStream(invoice));

        StreamResult target = new StreamResult(bos);
        Transformer transformer = transformerFactory.newTransformer(xslSource);
        transformer.transform(streamSource, target);

        return bos.toByteArray();
    }

    private byte[] foTransformer(byte[] xRechnungContent, String xslPath) throws Exception {
        StreamSource xsl = new StreamSource(this.getClass().getResourceAsStream(xslPath));
        xsl.setSystemId(Objects.requireNonNull(this.getClass().getResource(xslPath)).toURI().toString());

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Transformer transformer = transformerFactory.newTransformer(xsl);

        transformer.transform(new StreamSource(new ByteArrayInputStream(xRechnungContent)), new StreamResult(outputStream));
        return outputStream.toByteArray();
    }
}
