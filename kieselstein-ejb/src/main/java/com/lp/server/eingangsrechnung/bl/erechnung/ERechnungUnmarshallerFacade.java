package com.lp.server.eingangsrechnung.bl.erechnung;

import com.lp.server.eingangsrechnung.bl.ERechnungUnmarshaller;
import com.lp.server.eingangsrechnung.bl.UblInvoiceUnmarshaller;
import io.konik.zugferd.Invoice;
import oasis.names.specification.ubl.schema.xsd.invoice_2.InvoiceType;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import un.unece.uncefact.data.standard.crossindustryinvoice._100.CrossIndustryInvoiceType;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

public class ERechnungUnmarshallerFacade {

    private static ERechnungUnmarshallerFacade instance;
    private static final Map<String, ERechnungUnmarshaller> NAMESPACE_AND_UNMARSHALLER = new HashMap<>() {{
                put("urn:oasis:names:specification:ubl:schema:xsd:Invoice-2", new UblInvoiceUnmarshaller<>(InvoiceType.class));
                put("urn:un:unece:uncefact:data:standard:CrossIndustryInvoice:100", new CrossIndustryInvoiceUnmarshaller<>(CrossIndustryInvoiceType.class));
                put("urn:ferd:CrossIndustryDocument:invoice:1p0", new Zugferd1Unmarshaller<>(Invoice.class));
            }};

    private ERechnungUnmarshallerFacade() {

    }

    public static ERechnungUnmarshallerFacade getInstance() {
        if (instance == null) {
            instance = new ERechnungUnmarshallerFacade();
        }
        return instance;
    }

    public Object transform(String content) throws ParserConfigurationException, IOException, SAXException, JAXBException {
        content = content.trim().replaceFirst("^([\\W]+)<","<");
        Document doc = parse(content);
        String ns = getNamespace(doc);

        return unmarshal(ns, doc);
    }

    private Object unmarshal(String ns, Document doc) throws JAXBException {
        return NAMESPACE_AND_UNMARSHALLER.get(ns).unmarshal(doc);
    }

    private String getNamespace(Document doc) {
        Element root = doc.getDocumentElement();
        return root.getNamespaceURI();
    }

    private Document parse(String content) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        return factory
                .newDocumentBuilder()
                .parse(new InputSource(new StringReader(content)));
    }

}

