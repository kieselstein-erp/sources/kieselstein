package com.lp.server.eingangsrechnung.service;

import com.lp.server.system.service.TheClientDto;

import javax.ejb.Remote;

@Remote
public interface ERechnungFac {

    ERechnungDto parseFileContentToDto(byte[] content);

    void createPartnerBankAusERechnung(Integer partnerId, ERechnungDto eRechnungDto, TheClientDto clientDto);
}
