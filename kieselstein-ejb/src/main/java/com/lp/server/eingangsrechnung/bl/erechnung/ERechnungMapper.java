package com.lp.server.eingangsrechnung.bl.erechnung;

import com.lp.server.eingangsrechnung.service.ERechnungDto;


public interface ERechnungMapper<T> {
    ERechnungDto map(T invoice);
}
