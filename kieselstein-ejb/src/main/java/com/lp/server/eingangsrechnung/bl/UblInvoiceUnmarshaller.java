package com.lp.server.eingangsrechnung.bl;

import com.lp.server.util.XMLUnmarshaller;
import oasis.names.specification.ubl.schema.xsd.invoice_2.InvoiceType;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;

public class UblInvoiceUnmarshaller<T extends InvoiceType> extends XMLUnmarshaller<T> implements ERechnungUnmarshaller<T> {

    private Class<T> xmlClass;

    public UblInvoiceUnmarshaller(Class<T> xmlClass) {
        super();
        this.xmlClass = xmlClass;
    }

    @Override
    public T unmarshal(Document content) throws JAXBException {
        return unmarshal(content, xmlClass);
    }

    @Override
    public T unmarshal(String content) throws JAXBException, SAXException {
        return unmarshal(content, xmlClass);
    }

    @Override
    public Class getTargetType() {
        return InvoiceType.class;
    }

}
