package com.lp.server.eingangsrechnung.bl.erechnung;

import com.lp.server.eingangsrechnung.service.ERechnungDto;
import io.konik.zugferd.Invoice;
import io.konik.zugferd.entity.CreditorFinancialAccount;
import io.konik.zugferd.entity.FinancialInstitution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 20241002: ZUGferd ist not EN 16931 compliant.
 */
@Deprecated
public class Zugferd1Mapper implements ERechnungMapper<Invoice> {

    private static final Logger LOG = LoggerFactory.getLogger(Zugferd1Mapper.class);


    @Override
    public ERechnungDto map(Invoice invoice) {
        var eRechnung = new ERechnungDto();

        try {
            var header = invoice.getHeader();
            var trade = invoice.getTrade();
            var settlement = trade.getSettlement();
            var monetarySummation = trade.getSettlement().getMonetarySummation();

            eRechnung.setLieferantenRechnungsnummer(header.getInvoiceNumber());
            eRechnung.setBelegdatum(new java.sql.Date(header.getIssued().getTime()));
            eRechnung.setWaehrung(trade.getSettlement().getCurrency().getCurrency().getCurrencyCode());
            eRechnung.setRechnungsArt(header.getCode().getCode());

            eRechnung.setNettoBetrag(monetarySummation.getTaxBasisTotal().getValue());
            eRechnung.setBruttoBetrag(monetarySummation.getGrandTotal().getValue());
            eRechnung.setUstBetrag(monetarySummation.getTaxTotal().getValue());

            eRechnung.setSteuerSaetze(settlement.getTradeTax().stream().map(tax ->
                    new ERechnungDto.ERechnungSteuerDto(
                            tax.getCategory().getCode(),
                            tax.getPercentage(),
                            tax.getCalculated().getValue()
                    )
            ).collect(Collectors.toList()));

            trade.getSettlement().getPaymentMeans().stream().findFirst().ifPresent(
                    paymentMeans -> {
                        var iban = Optional.ofNullable(paymentMeans.getPayeeAccount()).map(CreditorFinancialAccount::getIban).orElse(null);
                        iban = iban != null ? iban.replaceAll("[^a-zA-Z0-9]","").toUpperCase() : null;

                        var bic = Optional.ofNullable(paymentMeans.getPayeeInstitution()).map(FinancialInstitution::getBic).orElse(null);

                        eRechnung.setLieferantIban(iban);
                        eRechnung.setLieferantBic(bic);
                    }
            );

        } catch (Exception ex) {
            LOG.warn("Zugferd1 could not be completely mapped", ex);
        }

        return eRechnung;
    }
}
