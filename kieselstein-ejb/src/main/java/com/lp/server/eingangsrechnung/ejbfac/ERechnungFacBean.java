package com.lp.server.eingangsrechnung.ejbfac;

import com.lp.server.eingangsrechnung.bl.erechnung.ERechnungMapperFacade;
import com.lp.server.eingangsrechnung.bl.erechnung.ERechnungPdfCreator;
import com.lp.server.eingangsrechnung.bl.erechnung.ERechnungUnmarshallerFacade;
import com.lp.server.eingangsrechnung.service.ERechnungDto;
import com.lp.server.eingangsrechnung.service.ERechnungFac;
import com.lp.server.partner.service.BankDto;
import com.lp.server.partner.service.PartnerDto;
import com.lp.server.partner.service.PartnerFac;
import com.lp.server.partner.service.PartnerbankDto;
import com.lp.server.system.service.TheClientDto;
import com.lp.server.util.Facade;
import com.lp.util.EJBExceptionLP;
import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDDocumentNameDictionary;

import javax.ejb.Stateless;
import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Stateless
public class ERechnungFacBean extends Facade implements ERechnungFac {

    private final ERechnungUnmarshallerFacade unmarshaller = ERechnungUnmarshallerFacade.getInstance();
    private final ERechnungMapperFacade eRechnungMapperFacade = new ERechnungMapperFacade();
    private final ERechnungPdfCreator eRechnungPdfCreator = new ERechnungPdfCreator();

    private final static String ZUGFERD_V1 = "ZUGFeRD-invoice.xml";
    private final static String ZUGFERD_V2 = "zugferd-invoice.xml";
    private final static String ZUGFERD_V2_3 = "factur-x.xml";
    private final static String X_RECHNUNG = "xrechnung.xml";

    private final static List<String> SUPPORTED_FILES = List.of(ZUGFERD_V1, ZUGFERD_V2, ZUGFERD_V2_3, X_RECHNUNG);

    @Override
    public ERechnungDto parseFileContentToDto(final byte[] content) {
        try {
            Object eRechnung;
            byte[] pdf;
            String xmlContent;
            // Sometimes control characters occur at the beginning of xml, reason not clear yet, replacing the beginning helps.
            String strContent = new String(content, StandardCharsets.UTF_8)
                    .trim().replaceFirst("^([\\W]+)<","<");;

            if (strContent.startsWith("%PDF")) {
                xmlContent = getXmlAttachmentFromPdf(content);
                eRechnung = unmarshaller.transform(xmlContent);
                pdf = content;
            } else if (strContent.startsWith("<?xml")) {
                eRechnung = unmarshaller.transform(strContent);
                pdf = eRechnungPdfCreator.create(content, eRechnung.getClass());
                xmlContent = strContent;
            } else {
                throw new EJBExceptionLP(EJBExceptionLP.FEHLER, "Die Datei ist keine XML- oder PDF-Datei");
            }

            if(eRechnung == null) {
                throw new EJBExceptionLP(EJBExceptionLP.FEHLER, "Datei konnte nicht verarbeitet werden.");
            }

            ERechnungDto eRechnungDto = eRechnungMapperFacade.map(eRechnung);
            eRechnungDto.setPdfContent(pdf);
            eRechnungDto.setXmlContent(xmlContent);
            return eRechnungDto;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String getXmlAttachmentFromPdf(final byte[] content) throws Exception {
        var strContent = new String(Arrays.copyOf(content, 10));
        if (strContent.startsWith("%PDF")) {
            try (var pdf = Loader.loadPDF(content)) {
                var dictionary = new PDDocumentNameDictionary(pdf.getDocumentCatalog());
                if (dictionary.getEmbeddedFiles() != null) {
                    var names = dictionary.getEmbeddedFiles().getNames();
                    var fileSpec = names.entrySet().stream()
                            .filter(e -> SUPPORTED_FILES.stream()
                                    .anyMatch(s -> s.equalsIgnoreCase(e.getKey())))
                            .map(Map.Entry::getValue).findFirst();

                    if (fileSpec.isEmpty()) {
                        fileSpec = names.values().stream()
                                .filter(e -> SUPPORTED_FILES.stream().anyMatch(s -> s.equalsIgnoreCase(e.getFile())))
                                .findFirst();
                    }

                    if(fileSpec.isPresent()) {
                        return new String(fileSpec.get().getEmbeddedFile().toByteArray(), StandardCharsets.UTF_8);
                    }
                }
            }
            throw new EJBExceptionLP(EJBExceptionLP.FEHLER, "PDF-Datei enthält keinen gültigen XML Anhang");
        }
        return null;
    }

    @Override
    public void createPartnerBankAusERechnung(Integer partnerId, ERechnungDto eRechnungDto, TheClientDto clientDto) {
        try {
            if (StringUtils.isBlank(eRechnungDto.getLieferantBic())) {
                throw new EJBExceptionLP(EJBExceptionLP.FEHLER_BEIM_ANLEGEN, "BIC nicht vorhanden. Keine Analage einer neuen Bankverbindung aus ERechnung moeglich");
            }

            var banken = getBankFac().bankFindByBIC(eRechnungDto.getLieferantBic(), clientDto);
            var partnerBankDto = new PartnerbankDto();

            banken.stream().findFirst().ifPresentOrElse(
                    b -> partnerBankDto.setBankPartnerIId(b.getPartnerIId()),
                    () -> {
                        try {
                            partnerBankDto.setBankPartnerIId(neueBankAnlegen(eRechnungDto.getLieferantBic(), clientDto));
                        } catch (RemoteException e) {
                            throw new RuntimeException(e);
                        }
                    }
            );

            if (partnerBankDto.getBankPartnerIId() == null) {
                throw new EJBExceptionLP(EJBExceptionLP.FEHLER_BEIM_ANLEGEN, "Bank konnte nicht angelegt werden.");
            }
            partnerBankDto.setPartnerIId(partnerId);
            partnerBankDto.setCIban(eRechnungDto.getLieferantIban());
            partnerBankDto.setISort(getBankFac().getMaxISort(partnerId) + 1);

            getBankFac().createPartnerbank(partnerBankDto, clientDto);
        } catch (Exception e) {
            throw new EJBExceptionLP(EJBExceptionLP.FEHLER, e);
        }
    }

    private Integer neueBankAnlegen(final String bic, final TheClientDto clientDto) throws RemoteException {
        var partnerDto = new PartnerDto();
        partnerDto.setCKbez(bic);
        partnerDto.setCName1nachnamefirmazeile1(bic);
        partnerDto.setPartnerartCNr(PartnerFac.PARTNERART_SONSTIGES);
        partnerDto.setLocaleCNrKommunikation(clientDto.getLocMandantAsString());
        partnerDto.setBVersteckt(false);

        var bankDto = new BankDto();
        bankDto.setCBic(bic);
        bankDto.setPartnerDto(partnerDto);

        return getBankFac().createBank(bankDto, clientDto);

    }

}
