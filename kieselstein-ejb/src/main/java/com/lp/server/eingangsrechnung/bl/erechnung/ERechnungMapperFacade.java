package com.lp.server.eingangsrechnung.bl.erechnung;

import com.lp.server.eingangsrechnung.service.ERechnungDto;
import io.konik.zugferd.Invoice;
import oasis.names.specification.ubl.schema.xsd.invoice_2.InvoiceType;
import un.unece.uncefact.data.standard.crossindustryinvoice._100.CrossIndustryInvoiceType;

import java.util.Map;

public class ERechnungMapperFacade {
    private final Map<Class, ERechnungMapper> eRechnungMappers = Map.of(
            InvoiceType.class, new UblInvoiceMapper(),
            CrossIndustryInvoiceType.class, new CiiInvoiceTypeMapper(),
            Invoice.class, new Zugferd1Mapper()
    );

    public ERechnungDto map(Object invoice) {
        return eRechnungMappers.get(invoice.getClass()).map(invoice);
    }

}
