package com.lp.server.eingangsrechnung.bl;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;

public interface ERechnungUnmarshaller<T> {
    T unmarshal(Document content) throws JAXBException;
    T unmarshal(String content) throws JAXBException, SAXException;

    Class getTargetType();
}
