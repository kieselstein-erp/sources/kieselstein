package com.lp.server.eingangsrechnung.bl.erechnung;

import com.lp.server.eingangsrechnung.service.ERechnungDto;
import com.lp.server.system.service.SystemFac;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import un.unece.uncefact.data.standard.crossindustryinvoice._100.CrossIndustryInvoiceType;
import un.unece.uncefact.data.standard.qualifieddatatype._100.CountryIDType;
import un.unece.uncefact.data.standard.unqualifieddatatype._100.AmountType;
import un.unece.uncefact.data.standard.unqualifieddatatype._100.CodeType;
import un.unece.uncefact.data.standard.unqualifieddatatype._100.TextType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class CiiInvoiceTypeMapper implements ERechnungMapper<CrossIndustryInvoiceType> {

    /**
     *
     * 102
     * CCYYMMDD
     * Calendar date: C = Century ; Y = Year ; M = Month ; D = Day.
     *
     * 203
     * CCYYMMDDHHMM
     * Calendar date including time with minutes: C=Century; Y=Year; M=Month; D=Day; H=Hour; M=Minutes.
     *
     * 205
     * CCYYMMDDHHMMZHHMM
     * Calendar date including time and time zone expressed in hours and minutes.
     * ZHHMM = time zone given as offset from Coordinated Universal Time (UTC).
     *
     * https://service.unece.org/trade/uncefact/publication/Transport-Logistics/MMT-RDM/HTML/001.htm
     */
    public static final Map<String, SimpleDateFormat> DATE_FORMATS = Map.of(
            "102", new SimpleDateFormat("yyyyMMdd"),
            "203", new SimpleDateFormat("yyyyMMddHHmm"),
            "205", new SimpleDateFormat("yyyyMMddHHmmZ")
    );

    public static final Map<String, String> CII_UNITS_TO_KIESELSTEIN_UNITS = Map.ofEntries(
            Map.entry(SystemFac.EINHEIT_TAG.trim(), "DAY"),
            Map.entry(SystemFac.EINHEIT_STUNDE.trim(), "HUR"),
            Map.entry(SystemFac.EINHEIT_KILOGRAMM.trim(), "KGM"),
            Map.entry(SystemFac.EINHEIT_KILOMETER.trim(), "KMT"),
            Map.entry(SystemFac.EINHEIT_LITER.trim(), "LTR"),
            Map.entry(SystemFac.EINHEIT_METER.trim(), "MTR"),
            Map.entry(SystemFac.EINHEIT_QUADRATMETER.trim(), "MTK"),
            Map.entry(SystemFac.EINHEIT_KUBIKMETER.trim(), "MTQ"),
            Map.entry(SystemFac.EINHEIT_MILLIMETER.trim(), "MMT"),
            Map.entry(SystemFac.EINHEIT_QUADRATMILLIMETER.trim(), "MMK"),
            Map.entry(SystemFac.EINHEIT_MINUTE.trim(), "MIN"),
            Map.entry(SystemFac.EINHEIT_MONAT.trim(), "MON"),
            Map.entry(SystemFac.EINHEIT_SEKUNDE.trim(), "SEC"),
            Map.entry(SystemFac.EINHEIT_STUECK.trim(), "H87"),
            Map.entry(SystemFac.EINHEIT_WOCHE.trim(), "WEE")
    );

    private static final Logger LOG = LoggerFactory.getLogger(UblInvoiceMapper.class) ;

    @Override
    public ERechnungDto map(CrossIndustryInvoiceType invoice) {
        var eRechnung = new ERechnungDto();

        try {

            var headerTradeSettlement = invoice.getSupplyChainTradeTransaction().getApplicableHeaderTradeSettlement();
            var headerTradeAgreement = invoice.getSupplyChainTradeTransaction().getApplicableHeaderTradeAgreement();
            var monetarySummation = headerTradeSettlement.getSpecifiedTradeSettlementHeaderMonetarySummation();
            var exchangedDocument = invoice.getExchangedDocument();
            var buyerTradeParty = headerTradeAgreement.getBuyerTradeParty();
            var sellerTradeParty = headerTradeAgreement.getSellerTradeParty();

            headerTradeSettlement.getSpecifiedTradeSettlementPaymentMeans().stream().findFirst().ifPresent(
                    paymentMeans -> {
                        var bic = StringUtils.trimToNull(Optional.ofNullable(paymentMeans
                                .getPayeeSpecifiedCreditorFinancialInstitution()).map(b -> b.getBICID().getValue()).orElse(null));
                        bic = bic != null ? bic.replaceAll("[^a-zA-Z0-9]", "").toUpperCase() : null;

                        var iban = StringUtils.trimToNull(Optional.ofNullable(paymentMeans
                                .getPayeePartyCreditorFinancialAccount()).map(a -> a.getIBANID().getValue()).orElse(null));
                        iban = iban != null ? iban.replaceAll("[^a-zA-Z0-9]", "").toUpperCase() : null;

                        eRechnung.setLieferantBic(bic);
                        eRechnung.setLieferantIban(iban);
                    }
            );


            Optional<String> rechnungsFormat = Optional.ofNullable(exchangedDocument.getIssueDateTime().getDateTimeString().getFormat());
            rechnungsFormat.ifPresent(format -> {
                try {
                    var datum = DATE_FORMATS.get(format).parse(exchangedDocument.getIssueDateTime().getDateTimeString().getValue());
                    eRechnung.setBelegdatum(new java.sql.Date(datum.getTime()));
                } catch (ParseException e) {
                    LOG.warn("Rechnungsdatum konnte nicht eingelesen werden.", e);
                }
            });

            String waehrung = headerTradeSettlement.getInvoiceCurrencyCode().getValue();
            String rechnungsArt = exchangedDocument.getTypeCode().getValue();

            monetarySummation.getTaxBasisTotalAmount().stream().findFirst().ifPresent(amount -> eRechnung.setNettoBetrag(amount.getValue()));
            monetarySummation.getGrandTotalAmount().stream().findFirst().ifPresent(amount -> eRechnung.setBruttoBetrag(amount.getValue()));
            eRechnung.setRechnungsArt(rechnungsArt);
            eRechnung.setWaehrung(waehrung);
            monetarySummation.getTaxTotalAmount().stream().findFirst().ifPresent(amount -> eRechnung.setUstBetrag(amount.getValue()));
            eRechnung.setLieferantenRechnungsnummer(exchangedDocument.getID().getValue());

            eRechnung.setSteuerSaetze(headerTradeSettlement.getApplicableTradeTax().stream()
                    .map(tax -> new ERechnungDto.ERechnungSteuerDto(
                            tax.getCategoryCode().getValue(),
                            tax.getRateApplicablePercent().getValue(),
                            tax.getCalculatedAmount().stream().findFirst().map(AmountType::getValue).orElse(null)
                    )).collect(Collectors.toList()));

            var verkaeuferUid = sellerTradeParty.getSpecifiedTaxRegistration().stream().findFirst()
                    .map(taxRegistration -> taxRegistration.getID().getValue()).orElse(null);

            var kaeuferUid = buyerTradeParty.getSpecifiedTaxRegistration().stream().findFirst()
                    .map(taxRegistration -> taxRegistration.getID().getValue()).orElse(null);

            eRechnung.setVerkaeuferUid(verkaeuferUid);
            eRechnung.setKaeuferUid(kaeuferUid);

            eRechnung.setLieferantStammdaten(new ERechnungDto.LieferantStammdaten());
            eRechnung.getLieferantStammdaten().setName(sellerTradeParty.getName().getValue());

            var sellerAddress = sellerTradeParty.getPostalTradeAddress();
            eRechnung.getLieferantStammdaten().setOrt(Optional.ofNullable(sellerAddress.getCityName()).map(TextType::getValue).orElse(null));
            eRechnung.getLieferantStammdaten().setStrasse(Optional.ofNullable(sellerAddress.getStreetName()).map(TextType::getValue).orElse(null));
            eRechnung.getLieferantStammdaten().setPlz(Optional.ofNullable(sellerAddress.getPostcodeCode()).map(CodeType::getValue).orElse(null));
            eRechnung.getLieferantStammdaten().setLandKurz(Optional.ofNullable(sellerAddress.getCountryID()).map(CountryIDType::getValue).orElse(null));

        } catch(Exception ex) {
            LOG.warn("CII Invoice could not be completely mapped", ex);
        }

        return eRechnung;
    }
}
