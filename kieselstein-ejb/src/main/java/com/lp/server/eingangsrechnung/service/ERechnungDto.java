package com.lp.server.eingangsrechnung.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

public class ERechnungDto implements Serializable {
    private Date belegdatum;
    private BigDecimal nettoBetrag;
    private BigDecimal bruttoBetrag;
    private String lieferantIban;
    private String lieferantBic;
    private String rechnungsArt;
    private String waehrung;
    private String lieferantenRechnungsnummer;
    private BigDecimal ustBetrag;
    private String verkaeuferUid;
    private String kaeuferUid;
    private byte[] pdfContent;
    private String xmlContent;

    private List<ERechnungSteuerDto> steuerSaetze;
    private LieferantStammdaten lieferantStammdaten;

    public void setBelegdatum(Date belegdatum) {
        this.belegdatum = belegdatum;
    }

    public Date getBelegdatum() {
        return belegdatum;
    }

    public void setLieferantIban(String lieferantIban) {
        this.lieferantIban = lieferantIban;
    }

    public String getLieferantIban() {
        return lieferantIban;
    }

    public void setRechnungsArt(String rechnungsArt) {
        this.rechnungsArt = rechnungsArt;
    }

    public String getRechnungsArt() {
        return rechnungsArt;
    }

    public void setWaehrung(String waehrung) {
        this.waehrung = waehrung;
    }

    public String getWaehrung() {
        return waehrung;
    }

    public BigDecimal getNettoBetrag() {
        return nettoBetrag;
    }

    public void setNettoBetrag(BigDecimal nettoBetrag) {
        this.nettoBetrag = nettoBetrag;
    }

    public String getLieferantenRechnungsnummer() {
        return lieferantenRechnungsnummer;
    }

    public void setLieferantenRechnungsnummer(String lieferantenRechnungsnummer) {
        this.lieferantenRechnungsnummer = lieferantenRechnungsnummer;
    }

    public void setUstBetrag(BigDecimal ustBetrag) {
        this.ustBetrag = ustBetrag;
    }

    public BigDecimal getUstBetrag() {
        return ustBetrag;
    }

    public BigDecimal getBruttoBetrag() {
        return bruttoBetrag;
    }

    public void setBruttoBetrag(BigDecimal bruttoBetrag) {
        this.bruttoBetrag = bruttoBetrag;
    }

    public List<ERechnungSteuerDto> getSteuerSaetze() {
        return steuerSaetze;
    }

    public void setSteuerSaetze(List<ERechnungSteuerDto> steuerSaetze) {
        this.steuerSaetze = steuerSaetze;
    }

    public String getVerkaeuferUid() {
        return verkaeuferUid;
    }

    public void setVerkaeuferUid(String verkaeuferUid) {
        this.verkaeuferUid = verkaeuferUid;
    }

    public String getKaeuferUid() {
        return kaeuferUid;
    }

    public void setKaeuferUid(String kaeuferUid) {
        this.kaeuferUid = kaeuferUid;
    }

    public byte[] getPdfContent() {
        return pdfContent;
    }

    public void setPdfContent(byte[] pdfContent) {
        this.pdfContent = pdfContent;
    }

    public LieferantStammdaten getLieferantStammdaten() {
        return lieferantStammdaten;
    }

    public void setLieferantStammdaten(LieferantStammdaten lieferantStammdaten) {
        this.lieferantStammdaten = lieferantStammdaten;
    }

    public String getLieferantBic() {
        return lieferantBic;
    }

    public void setLieferantBic(String lieferantBic) {
        this.lieferantBic = lieferantBic;
    }

    public String getXmlContent() {
        return xmlContent;
    }

    public void setXmlContent(String xmlContent) {
        this.xmlContent = xmlContent;
    }

    public static class ERechnungSteuerDto implements Serializable {
        private String id;
        private BigDecimal prozentSatz;
        private BigDecimal steuerBetrag;

        public ERechnungSteuerDto(String id, BigDecimal prozentSatz, BigDecimal steuerBetrag) {
            this.id = id;
            this.prozentSatz = prozentSatz;
            this.steuerBetrag = steuerBetrag;
        }

        public BigDecimal getSteuerBetrag() {
            return steuerBetrag;
        }

        public String getId() {
            return id;
        }

        public BigDecimal getProzentSatz() {
            return prozentSatz;
        }

    }

    public static class LieferantStammdaten implements Serializable{
        private String plz;
        private String ort;
        private String strasse;
        private String name;
        private String landKurz;

        public String getLandKurz() {
            return landKurz;
        }

        public void setLandKurz(String landKurz) {
            this.landKurz = landKurz;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOrt() {
            return ort;
        }

        public void setOrt(String ort) {
            this.ort = ort;
        }

        public String getPlz() {
            return plz;
        }

        public void setPlz(String plz) {
            this.plz = plz;
        }

        public String getStrasse() {
            return strasse;
        }

        public void setStrasse(String strasse) {
            this.strasse = strasse;
        }
    }
}
