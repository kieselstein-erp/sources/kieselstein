package com.lp.server.eingangsrechnung.bl.erechnung;

import com.lp.server.eingangsrechnung.bl.ERechnungUnmarshaller;
import io.konik.InvoiceTransformer;
import io.konik.zugferd.Invoice;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

/**
 * 20241002: ZUGferd ist not EN 16931 compliant.
 */
@Deprecated
public class Zugferd1Unmarshaller<T extends Invoice> implements ERechnungUnmarshaller<T> {

    private InvoiceTransformer invoiceTransformer = new InvoiceTransformer();
    private Class<T> xmlClass;

    public Zugferd1Unmarshaller(Class<T> xmlClass) {
        this.xmlClass = xmlClass;
    }

    @Override
    public T unmarshal(Document content) throws JAXBException {
        try {
            var transformerFactory = TransformerFactory.newInstance();
            var transformer = transformerFactory.newTransformer();
            var writer = new StringWriter();
            transformer.transform(new DOMSource(content), new StreamResult(writer));
            var xmlString = writer.toString();
            return (T) invoiceTransformer
                    .toModel(new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8)));
        } catch(Exception e) {
            throw new RuntimeException("XML Document ist not a valid ZUGferd1 file");
        }
    }

    @Override
    public T unmarshal(String content) throws JAXBException, SAXException {
        return (T) invoiceTransformer
                .toModel(new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8)));
    }

    @Override
    public Class getTargetType() {
        return this.xmlClass;
    }
}
