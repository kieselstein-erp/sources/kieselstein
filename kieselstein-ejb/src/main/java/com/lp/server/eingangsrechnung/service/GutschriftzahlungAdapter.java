package com.lp.server.eingangsrechnung.service;

import com.lp.server.rechnung.service.RechnungzahlungDto;
import com.lp.server.util.BelegZahlungAdapter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

public class GutschriftzahlungAdapter extends BelegZahlungAdapter implements Serializable {

    private static final long serialVersionUID = 1L;

    private RechnungzahlungDto reZahlungDto;
    private Boolean bErledigt;

    public GutschriftzahlungAdapter() {
        reZahlungDto = new RechnungzahlungDto();
        bErledigt = false;
    }

    @Override
    public Integer getRechnungIId() {
        return reZahlungDto.getRechnungIId();
    }

    @Override
    public void setRechnungIId(Integer rechnungIId) {
        reZahlungDto.setRechnungIId(rechnungIId);
    }

    @Override
    public Date getDZahldatum() {
        return reZahlungDto.getDZahldatum();
    }

    @Override
    public void setDZahldatum(Date dZahldatum) {
        reZahlungDto.setDZahldatum(dZahldatum);
    }

    @Override
    public String getZahlungsartCNr() {
        return reZahlungDto.getZahlungsartCNr();
    }

    @Override
    public void setZahlungsartCNr(String zahlungsartCNr) {
        reZahlungDto.setZahlungsartCNr(zahlungsartCNr);
    }

    @Override
    public Integer getBankkontoIId() {
        return reZahlungDto.getBankkontoIId();
    }

    @Override
    public void setBankkontoIId(Integer bankkontoIId) {
        reZahlungDto.setBankkontoIId(bankkontoIId);
    }

    @Override
    public Integer getIAuszug() {
        return reZahlungDto.getIAuszug();
    }

    @Override
    public void setIAuszug(Integer iAuszug) {
        reZahlungDto.setIAuszug(iAuszug);
    }

    @Override
    public BigDecimal getNKurs() {
        return reZahlungDto.getNKurs();
    }

    @Override
    public void setNKurs(BigDecimal nKurs) {
        reZahlungDto.setNKurs(nKurs);
    }

    @Override
    public BigDecimal getNBetrag() {
        return reZahlungDto.getNBetrag();
    }

    @Override
    public void setNBetrag(BigDecimal nBetrag) {
        reZahlungDto.setNBetrag(nBetrag);
    }

    @Override
    public BigDecimal getNBetragfw() {
        return reZahlungDto.getNBetragfw();
    }

    @Override
    public void setNBetragfw(BigDecimal nBetragfw) {
        reZahlungDto.setNBetragfw(nBetragfw);
    }

    @Override
    public BigDecimal getNBetragUst() {
        return reZahlungDto.getNBetragUst();
    }

    @Override
    public void setNBetragUst(BigDecimal nBetragUst) {
        reZahlungDto.setNBetragUst(nBetragUst);
    }

    @Override
    public BigDecimal getNBetragUstfw() {
        return reZahlungDto.getNBetragUstfw();
    }

    @Override
    public void setNBetragUstfw(BigDecimal nBetragUstfw) {
        reZahlungDto.setNBetragUstfw(nBetragUstfw);
    }

    @Override
    public BigDecimal getBruttoBetrag() {
        return getNBetrag().add(getNBetragUst());
    }

    @Override
    public String getKommentar() {
        return reZahlungDto.getCKommentar();
    }

    @Override
    public void setKommentar(String cKommentar) {
        reZahlungDto.setCKommentar(cKommentar);
    }

    @Override
    public Boolean isBErledigt() {
        return bErledigt;
    }

    @Override
    public void setBErledigt(Boolean bErledigt) {
        this.bErledigt = bErledigt;
    }

    @Override
    public Object getRawBelegZahlungDto() {
        return reZahlungDto;
    }

    @Override
    public Boolean getBKursuebersteuert() {
        return null;
    }

    @Override
    public void setBKursuebersteuert(Boolean bKursuebersteuert) {

    }
}
