/*******************************************************************************
 * kieselsteinERP, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2023 - 2024 Kieselstein ERP eG, X-Net Services GmbH
 * based on: HELIUM V, Copyright (C) 2004 - 2022 HELIUM V IT-Solutions GmbH
 * <p>
 * distributed under the terms of GNU Affero General Public License, version 3
 * with supplements. See LICENSE file in base source directory for details.
 * <p>
 * Contact: help@kieselstein-erp.org
 ******************************************************************************/
package com.lp.server.fertigung.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

public class LosPlanungvorschlagDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer iId;
	private Integer lossollarbeitsplanIId;
	private Integer maschineIId;
	private Long lRuestzeit;
	private Long lStueckzeit;
	private Integer iReihung;
	private Timestamp tPlanBeginn;
	private Integer personalIIdAendern;
	private Integer personalIIdAnlegen;
	private Timestamp tAnlegen;
	private Timestamp tAendern;
	private String cLosText;

	public String getcLosText() {
		return cLosText;
	}

	public void setcLosText(String xText) {
		this.cLosText = xText;
	}



	public Integer getIId() {
		return iId;
	}

	public void setIId(Integer iId) {
		this.iId = iId;
	}

	public Integer getLossollarbeitsplanIId() {
		return lossollarbeitsplanIId;
	}

	public void setLossollarbeitsplanIId(Integer lossollarbeitsplanIId) {
		this.lossollarbeitsplanIId = lossollarbeitsplanIId;
	}

	public Integer getMaschineIId() {
		return maschineIId;
	}

	public void setMaschineIId(Integer maschineIId) {
		this.maschineIId = maschineIId;
	}

	public Long getlRuestzeit() {
		return lRuestzeit;
	}

	public void setlRuestzeit(Long lRuestzeit) {
		this.lRuestzeit = lRuestzeit;
	}

	public Long getlStueckzeit() {
		return lStueckzeit;
	}

	public void setlStueckzeit(Long lStueckzeit) {
		this.lStueckzeit = lStueckzeit;
	}

	public Integer getiReihung() {
		return iReihung;
	}

	public void setiReihung(Integer iReihung) {
		this.iReihung = iReihung;
	}

	public Timestamp gettPlanBeginn() {
		return tPlanBeginn;
	}

	public void settPlanBeginn(Timestamp tPlanBeginn) {
		this.tPlanBeginn = tPlanBeginn;
	}

	public Integer getPersonalIIdAendern() {
		return personalIIdAendern;
	}

	public void setPersonalIIdAendern(Integer personalIIdAendern) {
		this.personalIIdAendern = personalIIdAendern;
	}

	public Integer getPersonalIIdAnlegen() {
		return personalIIdAnlegen;
	}

	public void setPersonalIIdAnlegen(Integer personalIIdAnlegen) {
		this.personalIIdAnlegen = personalIIdAnlegen;
	}

	public Timestamp getTAnlegen() {
		return tAnlegen;
	}

	public void setTAnlegen(Timestamp tAnlegen) {
		this.tAnlegen = tAnlegen;
	}

	public Timestamp getTAendern() {
		return tAendern;
	}

	public void setTAendern(Timestamp tAendern) {
		this.tAendern = tAendern;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof LosPlanungvorschlagDto))
			return false;
		LosPlanungvorschlagDto that = (LosPlanungvorschlagDto) obj;
		if (!Objects.equals(that.iId, this.iId))
			return false;
		if (!Objects.equals(that.lossollarbeitsplanIId, this.lossollarbeitsplanIId))
			return false;
		if (!Objects.equals(that.maschineIId, this.maschineIId))
			return false;
		if (!Objects.equals(that.lRuestzeit, this.lRuestzeit))
			return false;
		if (!Objects.equals(that.lStueckzeit, this.lStueckzeit))
			return false;
		if (!Objects.equals(that.iReihung, this.iReihung))
			return false;
		if (!Objects.equals(that.tPlanBeginn, this.tPlanBeginn))
			return false;
		if (!Objects.equals(that.personalIIdAnlegen, this.personalIIdAnlegen))
			return false;
		if (!Objects.equals(that.personalIIdAendern, this.personalIIdAendern))
			return false;
		if (!Objects.equals(that.tAnlegen, this.tAnlegen))
			return false;
		if (!Objects.equals(that.tAendern, this.tAendern))
			return false;
		if (!Objects.equals(that.cLosText, this.cLosText))
			return false;
		return true;
	}

	public int hashCode() {
		int result = 17;
		result = 37 * result + this.iId.hashCode();
		result = 37 * result + this.lossollarbeitsplanIId.hashCode();
		result = 37 * result + this.maschineIId.hashCode();
		result = 37 * result + this.lRuestzeit.hashCode();
		result = 37 * result + this.lStueckzeit.hashCode();
		result = 37 * result + this.iReihung.hashCode();
		return result;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(iId).append(", ");
		sb.append(lossollarbeitsplanIId).append(", ");
		sb.append(maschineIId).append(", ");
		sb.append(lRuestzeit).append(", ");
		sb.append(lStueckzeit).append(", ");
		sb.append(iReihung).append(", ");
		sb.append(cLosText).append(", ");
		return sb.toString();
	}
}
