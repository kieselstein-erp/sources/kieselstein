/*******************************************************************************
 * kieselsteinERP, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2023 - 2024 Kieselstein ERP eG, X-Net Services GmbH
 * based on: HELIUM V, Copyright (C) 2004 - 2022 HELIUM V IT-Solutions GmbH
 * <p>
 * distributed under the terms of GNU Affero General Public License, version 3
 * with supplements. See LICENSE file in base source directory for details.
 * <p>
 * Contact: help@kieselstein-erp.org
 ******************************************************************************/
package com.lp.server.fertigung.service;

import com.lp.server.system.service.TheClientDto;
import com.lp.util.EJBExceptionLP;

import javax.ejb.Remote;
import java.rmi.RemoteException;
import java.util.Date;

@Remote
public interface LosPlanungVorschlagFac {

	int MOVE_PLANNING_UP = 1;
	int MOVE_PLANNING_DOWN = 2;

	String LOCKME_FERT_PLANUNGVORSCHLAG = "lockme_fert_planungvorschlag";

	LosPlanungvorschlagDto findByPrimaryKey(Integer iId) throws EJBExceptionLP, RemoteException;

	boolean checkMaterialComplete(Integer losId, String relevantLagerArt);

	boolean generatePlanningSuggestions(
			Integer iMaschinenGruppeID, boolean bMaterialKomplett, Date startDate, Date maxLosBeginDate,
			String minimalLosStatus, TheClientDto theClientDto
	) throws EJBExceptionLP;

	void movePlanningOrderToReihen(
			Integer iKey, Integer newTargetReihe,
			TheClientDto clientDto
	);

	void movePlanningOrder(
			Integer iKey, int moveAction,
			TheClientDto clientDto
	);

	void saveLosTermine(Integer iMaschineID, TheClientDto clientDto);

	void changeLosPlanungVorschlag(Integer iKey, Integer newMaschineID, Integer newReihung,
								   Long newRuestzeit, Long newStueckzeit, String cLosText, TheClientDto clientDto);

	/**
	 * Returns all IDs of the Maschines where a Planning Suggestion exists.
	 * @return IDs of the Maschine as Array where a Planning Suggestion exists.
	 */
	Integer[] getAllPlannedMaschineIDs();

	/**
	 * Deletes the Planning Suggestion for the given Key and reorders all Planning Suggestions
	 * for the Maschine of the Planning Suggestion.
	 * @param iKey The Primary Key of the Planning Suggestion which should be deleted.
	 */
	void deletePlanningSuggestion(Integer iKey, TheClientDto clientDto);

	/**
	 * Deletes all Planning Suggestions.
	 */
	void deleteAllPlanningSuggestions(TheClientDto clientDto);
	
}
