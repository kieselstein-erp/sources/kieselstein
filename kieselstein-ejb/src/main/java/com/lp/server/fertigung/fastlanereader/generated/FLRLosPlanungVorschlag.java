/*******************************************************************************
 * kieselsteinERP, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2023 - 2024 Kieselstein ERP eG, X-Net Services GmbH
 * based on: HELIUM V, Copyright (C) 2004 - 2022 HELIUM V IT-Solutions GmbH
 * <p>
 * distributed under the terms of GNU Affero General Public License, version 3
 * with supplements. See LICENSE file in base source directory for details.
 * <p>
 * Contact: help@kieselstein-erp.org
 ******************************************************************************/
package com.lp.server.fertigung.fastlanereader.generated;

import com.lp.server.personal.fastlanereader.generated.FLRMaschine;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;


/** @author Hibernate CodeGenerator */
public class FLRLosPlanungVorschlag implements Serializable {

    /** identifier field */
    private Integer i_id;

    /** nullable persistent field */
    private Integer los_sollarbeitsplan_id;

    /** nullable persistent field */
    private Integer maschine_i_id;

    /** nullable persistent field */
    private BigDecimal l_ruestzeit;

    /** nullable persistent field */
    private BigDecimal l_stueckzeit;


    /** nullable persistent field */
    private Integer i_reihung;


    /** nullable persistent field */
    private FLRLossollarbeitsplan flrLossollarbeitsplan;


    /** nullable persistent field */
    private FLRMaschine flrmaschine;

    private Timestamp t_plan_beginn;

    private Integer personal_i_id_anlegen;
    private Integer personal_i_id_aendern;
    private Timestamp t_anlegen;
    private Timestamp t_aendern;
    private Integer i_maschinenversatz_ms;

    private String cLosText;


    /** full constructor */
    public FLRLosPlanungVorschlag(Integer los_sollarbeitsplan_id, Integer maschine_i_id, BigDecimal l_ruestzeit,
                                  BigDecimal l_stueckzeit, Integer i_reihung, FLRLossollarbeitsplan flrLossollarbeitsplan,
                                  FLRMaschine flrmaschine, Timestamp t_plan_beginn, Integer personal_i_id_anlegen,
                                  Integer personal_i_id_aendern, Timestamp t_anlegen, Timestamp t_aendern, Integer i_maschinenversatz_ms, String cLosText) {
        this.los_sollarbeitsplan_id = los_sollarbeitsplan_id;
        this.maschine_i_id = maschine_i_id;
        this.l_ruestzeit = l_ruestzeit;
        this.l_stueckzeit = l_stueckzeit;
        this.i_reihung = i_reihung;
        this.flrLossollarbeitsplan = flrLossollarbeitsplan;
        this.flrmaschine = flrmaschine;
        this.t_plan_beginn = t_plan_beginn;
        this.personal_i_id_anlegen = personal_i_id_anlegen;
        this.personal_i_id_aendern = personal_i_id_aendern;
        this.t_anlegen = t_anlegen;
        this.t_aendern = t_aendern;
        this.i_maschinenversatz_ms = i_maschinenversatz_ms;
        this.cLosText = cLosText;
    }

    /** default constructor */
    public FLRLosPlanungVorschlag() {
    }

    public Integer getI_id() {
        return this.i_id;
    }

    public void setI_id(Integer i_id) {
        this.i_id = i_id;
    }

    public Integer getLos_sollarbeitsplan_id() {
        return this.los_sollarbeitsplan_id;
    }

    public void setLos_sollarbeitsplan_id(Integer los_sollarbeitsplan_id) {
        this.los_sollarbeitsplan_id = los_sollarbeitsplan_id;
    }

    public Integer getMaschine_i_id() {
        return this.maschine_i_id;
    }

    public void setMaschine_i_id(Integer maschine_i_id) {
        this.maschine_i_id = maschine_i_id;
    }

    public BigDecimal getL_ruestzeit() {
        return this.l_ruestzeit;
    }

    public void setL_ruestzeit(BigDecimal l_ruestzeit) {
        this.l_ruestzeit = l_ruestzeit;
    }

    public BigDecimal getL_stueckzeit() {
        return this.l_stueckzeit;
    }

    public void setL_stueckzeit(BigDecimal l_stueckzeit) {
        this.l_stueckzeit = l_stueckzeit;
    }

    public Integer getI_reihung() {
        return this.i_reihung;
    }

    public void setI_reihung(Integer i_reihung) {
        this.i_reihung = i_reihung;
    }

    public FLRLossollarbeitsplan getFlrLossollarbeitsplan() {
        return flrLossollarbeitsplan;
    }

    public void setFlrLossollarbeitsplan(FLRLossollarbeitsplan flrLossollarbeitsplan) {
        this.flrLossollarbeitsplan = flrLossollarbeitsplan;
    }

    public FLRMaschine getFlrmaschine() {
        return this.flrmaschine;
    }

    public void setFlrmaschine(FLRMaschine flrmaschine) {
        this.flrmaschine = flrmaschine;
    }

    public Timestamp getT_plan_beginn() {
        return t_plan_beginn;
    }

    public void setT_plan_beginn(Timestamp t_plan_beginn) {
        this.t_plan_beginn = t_plan_beginn;
    }

    public Integer getPersonal_i_id_anlegen() {
        return personal_i_id_anlegen;
    }

    public void setPersonal_i_id_anlegen(Integer personal_i_id_anlegen) {
        this.personal_i_id_anlegen = personal_i_id_anlegen;
    }

    public Integer getPersonal_i_id_aendern() {
        return personal_i_id_aendern;
    }

    public void setPersonal_i_id_aendern(Integer personal_i_id_aendern) {
        this.personal_i_id_aendern = personal_i_id_aendern;
    }

    public Timestamp getT_anlegen() {
        return t_anlegen;
    }

    public void setT_anlegen(Timestamp t_anlegen) {
        this.t_anlegen = t_anlegen;
    }

    public Timestamp getT_aendern() {
        return t_aendern;
    }

    public void setT_aendern(Timestamp t_aendern) {
        this.t_aendern = t_aendern;
    }

    public Integer getI_maschinenversatz_ms() {
        return i_maschinenversatz_ms;
    }

    public void setI_maschinenversatz_ms(Integer i_maschinenversatz_ms) {
        this.i_maschinenversatz_ms = i_maschinenversatz_ms;
    }

    public String getcLosText() {
        return cLosText;
    }

    public void setcLosText(String cLosText) {
        this.cLosText = cLosText;
    }


    public String toString() {
        return new ToStringBuilder(this)
            .append("i_id", getI_id())
            .toString();
    }

}
