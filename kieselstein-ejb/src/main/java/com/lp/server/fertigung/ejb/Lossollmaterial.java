/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 * 
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 * 
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: developers@heliumv.com
 ******************************************************************************/
package com.lp.server.fertigung.ejb;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.*;

import com.lp.server.artikel.ejb.Artikel;
import com.lp.server.system.service.ITablenames;

@NamedQueries({
		@NamedQuery(name = LossollmaterialQuery.ByLosIId, query = "SELECT OBJECT(o) FROM Lossollmaterial o WHERE o.losIId=?1"),
		@NamedQuery(name = "LossollmaterialfindByLosIIdArtikelIId", query = "SELECT OBJECT(o) FROM Lossollmaterial o WHERE o.losIId=?1 AND o.artikelIId=?2 ORDER BY o.iSort ASC"),
		@NamedQuery(name = "LossollmaterialfindByLossollmaterialIIdOriginal", query = "SELECT OBJECT(o) FROM Lossollmaterial o WHERE o.lossollmaterialIIdOriginal=?1 ORDER BY o.iSort ASC"),
		@NamedQuery(name = "LossollmaterialfindByLosIIdOrderByISort", query = "SELECT OBJECT(o) FROM Lossollmaterial o WHERE o.losIId=?1 ORDER BY o.iSort ASC"),
		@NamedQuery(name = "LossollmaterialMaxISortFindByLosIId", query = "SELECT MAX(o.iSort) FROM Lossollmaterial o WHERE o.losIId=?1"),
		@NamedQuery(name = LossollmaterialQuery.ByLosIIdArtklaIIds, query = "SELECT OBJECT(o) FROM Lossollmaterial o LEFT JOIN o.artikel a WHERE o.losIId=:losId AND a.artklaIId IN (:artklaIds) ORDER BY o.iSort ASC"),
		@NamedQuery(name = LossollmaterialQuery.MaterialMengenByLosIDLagerart,
				query = "SELECT o.iId, o.losIId, o.nMenge, " +
						"  coalesce((select sum(istMat.nMenge) from Losistmaterial istMat where istMat.lossollmaterialIId=o.iId),0), " +
						"  coalesce((select sum(artLager.nLagerstand) from Artikellager artLager LEFT JOIN artLager.lager lg LEFT JOIN artLager.artikel art where art.iId=o.artikelIId AND lg.lagerartCNr in (:relevanteLagerArt)),0) " +
						"FROM Lossollmaterial o " +
						"LEFT JOIN o.artikel a " +
						"LEFT JOIN a.artkla aa " +
						"WHERE o.losIId=:losId AND aa.bProduktionsvorschlagMaterialKomplett = true " +
						"ORDER BY o.iSort"),
		@NamedQuery(
				name = "LosSollMaterialFindByArtikelIDAndLosStatus",
				query = "SELECT OBJECT(o) FROM Lossollmaterial o WHERE o.artikelIId=:artikelIId AND o.los.statusCNr in (:losStatusCNr) ORDER BY o.losIId, o.iSort ASC"
		),
		@NamedQuery(name = "LosSollMaterialWithJoinedData",
				query = "SELECT lsm, los, art FROM Lossollmaterial lsm " +
						"LEFT JOIN lsm.los los " +
						"LEFT OUTER JOIN lsm.artikel art " +
						"WHERE lsm.iId IN (:ids)"
		),
		@NamedQuery(name = "LosSollMaterialArtikelIDs",
				query = "SELECT distinct lsm.artikelIId FROM Lossollmaterial lsm " +
						"WHERE lsm.iId IN (:ids)" +
						"  AND lsm.artikelIId IS NOT NULL"
		),
		@NamedQuery(name = "LosSollMaterialFindForImport",
				query = "SELECT OBJECT (lsm) FROM Lossollmaterial lsm " +
						"WHERE (lsm.losIId = :losIId OR :losIId IS NULL)" +
						"  AND (lsm.los.cNr = :loscNr OR :loscNr IS NULL)" +
						"  AND (lsm.los.cNr LIKE :trutosLoscNr OR :trutosLoscNr IS NULL)" +
						"  AND (lsm.artikelIId = :artikelIId OR :artikelIId IS NULL) " +
						"  AND (lsm.iSort = :iSort OR :iSort IS NULL) "
		),
		@NamedQuery(name = "LosSollMaterialLastTruTopsExportFindForImport",
				query = "SELECT lsm, " +
						"       (select max (att.tExportBeginn) from ArtikelTruTops att where att.artikelIId = a.iId and att.tExportBeginn is not null) " +
						"FROM Lossollmaterial lsm " +
						"LEFT JOIN lsm.artikel a " +
						"LEFT JOIN a.artkla aa " +
						"WHERE lsm.losIId=:losId AND aa.bTops <> 0 " +
						"ORDER BY lsm.losIId, lsm.iSort"
		),
})
@Entity
@Table(name = ITablenames.FERT_LOSSOLLMATERIAL)
public class Lossollmaterial implements Serializable {
	@Id
	@Column(name = "I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer iId;

	@Column(name = "N_MENGE", columnDefinition = "NUMERIC(17,6) NOT NULL")
	private BigDecimal nMenge;
	
	@Column(name = "B_DRINGEND", columnDefinition = "SMALLINT NOT NULL")
	private Short bDringend;
	

	public Short getBDringend() {
		return bDringend;
	}

	public void setBDringend(Short bDringend) {
		this.bDringend = bDringend;
	}

	@Column(name = "B_RUESTMENGE", columnDefinition = "SMALLINT NOT NULL")
	private Short bRuestmenge;

	public Short getBRuestmenge() {
		return bRuestmenge;
	}

	public void setBRuestmenge(Short bRuestmenge) {
		this.bRuestmenge = bRuestmenge;
	}

	@Column(name = "F_DIMENSION1", columnDefinition = "DOUBLE PRECISION")
	private Float fDimension1;

	@Column(name = "F_DIMENSION2", columnDefinition = "DOUBLE PRECISION")
	private Float fDimension2;

	@Column(name = "F_DIMENSION3", columnDefinition = "DOUBLE PRECISION")
	private Float fDimension3;

	@Column(name = "C_POSITION", columnDefinition = "VARCHAR(3000)")
	private String cPosition;

	@Column(name = "C_KOMMENTAR", columnDefinition = "VARCHAR(80)")
	private String cKommentar;

	@Column(name = "I_LFDNUMMER", columnDefinition = "INTEGER")
	private Integer iLfdnummer;

	@Column(name = "I_SORT", columnDefinition = "INTEGER NOT NULL")
	private Integer iSort;

	@Column(name = "B_NACHTRAEGLICH", columnDefinition = "SMALLINT NOT NULL")
	private Short bNachtraeglich;

	@Column(name = "N_SOLLPREIS", columnDefinition = "NUMERIC(17,6) NOT NULL")
	private BigDecimal nSollpreis;

	@Column(name = "T_AENDERN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAendern;

	@Column(name = "LOS_I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer losIId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "los_i_id", insertable = false, updatable = false)
	private Los los;

	@Column(name = "EINHEIT_C_NR", columnDefinition = "CHAR(15) NOT NULL")
	private String einheitCNr;

	@Column(name = "PERSONAL_I_ID_AENDERN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAendern;

	@Column(name = "MONTAGEART_I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer montageartIId;

	@Column(name = "ARTIKEL_I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer artikelIId;

	@Column(name = "LOSSOLLMATERIAL_I_ID_ORIGINAL", columnDefinition = "INTEGER")
	private Integer lossollmaterialIIdOriginal;
	
	@Column(name = "N_MENGE_STKLPOS", columnDefinition = "NUMERIC(17,6)")
	private BigDecimal nMengeStklPos;

	@Column(name = "EINHEIT_C_NR_STKLPOS", columnDefinition = "CHAR(15)")
	private String einheitCNrStklPos;
	
	@Column(name = "N_MENGE_PRO_LOS", columnDefinition = "NUMERIC(23,12)")
	private BigDecimal nMengeProLos;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ARTIKEL_I_ID", referencedColumnName = "I_ID", insertable = false, updatable = false)
	private Artikel artikel;
	
	@Column(name = "T_EXPORT_BEGINN", columnDefinition = "TIMESTAMP")
	private Timestamp tExportBeginn;
	
	@Column(name = "T_EXPORT_ENDE", columnDefinition = "TIMESTAMP")
	private Timestamp tExportEnde;
	
	@Column(name = "C_FEHLERCODE", columnDefinition = "VARCHAR(1024)")
	private String cFehlercode;
	
	@Column(name = "C_FEHLERTEXT", columnDefinition = "VARCHAR(3000)")
	private String cFehlertext;

	public Integer getLossollmaterialIIdOriginal() {
		return lossollmaterialIIdOriginal;
	}

	public void setLossollmaterialIIdOriginal(Integer lossollmaterialIIdOriginal) {
		this.lossollmaterialIIdOriginal = lossollmaterialIIdOriginal;
	}

	@Column(name = "I_BEGINNTERMINOFFSET", columnDefinition = "INTEGER NOT NULL")
	private Integer iBeginnterminoffset;

	public Integer getIBeginnterminoffset() {
		return iBeginnterminoffset;
	}

	public void setIBeginnterminoffset(Integer iBeginnterminoffset) {
		this.iBeginnterminoffset = iBeginnterminoffset;
	}

	private static final long serialVersionUID = 1L;

	public Los getLos() {
		return los;
	}

	public void setLos(Los los) {
		this.los = los;
	}

	public Lossollmaterial() {
		super();
	}

	public Lossollmaterial(Integer id, Integer losIId2, Integer artikelIId2,
			BigDecimal menge, String einheitCNr2, Integer montageartIId2,
			Integer sort, Short nachtraeglich, BigDecimal sollpreis,
			Integer personalIIdAendern2, Integer iBeginnterminoffset,
			Short bRuestmenge,Short bDringend, BigDecimal nMengeProLos) {
		setIId(id);
		setLosIId(losIId2);
		setArtikelIId(artikelIId2);
		setNMenge(menge);
		setEinheitCNr(einheitCNr2);
		setMontageartIId(montageartIId2);
		setISort(sort);
		setBNachtraeglich(nachtraeglich);
		setPersonalIIdAendern(personalIIdAendern2);
		setNSollpreis(sollpreis);
		// Setzen der NOT NULL felder
		Timestamp now = new Timestamp(System.currentTimeMillis());
		this.setTAendern(now);
		setIBeginnterminoffset(iBeginnterminoffset);
		setBRuestmenge(bRuestmenge);
		setBDringend(bDringend);
		setNMengeProLos(nMengeProLos);
	}

	public Integer getIId() {
		return this.iId;
	}

	public void setIId(Integer iId) {
		this.iId = iId;
	}

	public BigDecimal getNMenge() {
		return this.nMenge;
	}

	public void setNMenge(BigDecimal nMenge) {
		this.nMenge = nMenge;
	}

	public Float getFDimension1() {
		return this.fDimension1;
	}

	public void setFDimension1(Float fDimension1) {
		this.fDimension1 = fDimension1;
	}

	public Float getFDimension2() {
		return this.fDimension2;
	}

	public void setFDimension2(Float fDimension2) {
		this.fDimension2 = fDimension2;
	}

	public Float getFDimension3() {
		return this.fDimension3;
	}

	public void setFDimension3(Float fDimension3) {
		this.fDimension3 = fDimension3;
	}

	public String getCPosition() {
		return this.cPosition;
	}

	public void setCPosition(String cPosition) {
		this.cPosition = cPosition;
	}

	public String getCKommentar() {
		return this.cKommentar;
	}

	public void setCKommentar(String cKommentar) {
		this.cKommentar = cKommentar;
	}

	public Integer getILfdnummer() {
		return this.iLfdnummer;
	}

	public void setILfdnummer(Integer iLfdnummer) {
		this.iLfdnummer = iLfdnummer;
	}

	public Integer getISort() {
		return this.iSort;
	}

	public void setISort(Integer iSort) {
		this.iSort = iSort;
	}

	public Short getBNachtraeglich() {
		return this.bNachtraeglich;
	}

	public void setBNachtraeglich(Short bNachtraeglich) {
		this.bNachtraeglich = bNachtraeglich;
	}

	public BigDecimal getNSollpreis() {
		return this.nSollpreis;
	}

	public void setNSollpreis(BigDecimal nSollpreis) {
		this.nSollpreis = nSollpreis;
	}

	public Timestamp getTAendern() {
		return this.tAendern;
	}

	public void setTAendern(Timestamp tAendern) {
		this.tAendern = tAendern;
	}

	public Integer getLosIId() {
		return this.losIId;
	}

	public void setLosIId(Integer losIId) {
		this.losIId = losIId;
	}

	public String getEinheitCNr() {
		return this.einheitCNr;
	}

	public void setEinheitCNr(String einheitCNr) {
		this.einheitCNr = einheitCNr;
	}

	public Integer getPersonalIIdAendern() {
		return this.personalIIdAendern;
	}

	public void setPersonalIIdAendern(Integer personalIIdAendern) {
		this.personalIIdAendern = personalIIdAendern;
	}

	public Integer getMontageartIId() {
		return this.montageartIId;
	}

	public void setMontageartIId(Integer montageartIId) {
		this.montageartIId = montageartIId;
	}

	public Integer getArtikelIId() {
		return this.artikelIId;
	}

	public void setArtikelIId(Integer artikelIId) {
		this.artikelIId = artikelIId;
	}

	public BigDecimal getNMengeStklPos() {
		return nMengeStklPos;
	}
	
	public void setNMengeStklPos(BigDecimal nMengeStklPos) {
		this.nMengeStklPos = nMengeStklPos;
	}
	
	public String getEinheitCNrStklPos() {
		return einheitCNrStklPos;
	}
	
	public void setEinheitCNrStklPos(String einheitCNrStklPos) {
		this.einheitCNrStklPos = einheitCNrStklPos;
	}

	public BigDecimal getNMengeProLos() {
		return nMengeProLos;
	}

	public void setNMengeProLos(BigDecimal nMengeProLos) {
		this.nMengeProLos = nMengeProLos;
	}

	public void setTExportBeginn(Timestamp tExportBeginn) {
		this.tExportBeginn = tExportBeginn;
	}
	public Timestamp getTExportBeginn() {
		return tExportBeginn;
	}
	
	public void setTExportEnde(Timestamp tExportEnde) {
		this.tExportEnde = tExportEnde;
	}
	public Timestamp getTExportEnde() {
		return tExportEnde;
	}
	
	public void setCFehlertext(String cFehlertext) {
		this.cFehlertext = cFehlertext;
	}
	public String getCFehlertext() {
		return cFehlertext;
	}
	
	public void setCFehlercode(String cFehlercode) {
		this.cFehlercode = cFehlercode;
	}
	public String getCFehlercode() {
		return cFehlercode;
	}
}
