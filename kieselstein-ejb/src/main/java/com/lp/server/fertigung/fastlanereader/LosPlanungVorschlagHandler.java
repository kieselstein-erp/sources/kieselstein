/*******************************************************************************
 * kieselsteinERP, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2023 - 2024 Kieselstein ERP eG, X-Net Services GmbH
 * based on: HELIUM V, Copyright (C) 2004 - 2022 HELIUM V IT-Solutions GmbH
 * <p>
 * distributed under the terms of GNU Affero General Public License, version 3
 * with supplements. See LICENSE file in base source directory for details.
 * <p>
 * Contact: help@kieselstein-erp.org
 ******************************************************************************/
package com.lp.server.fertigung.fastlanereader;

import com.lp.server.artikel.fastlanereader.generated.FLRArtikelliste;
import com.lp.server.artikel.fastlanereader.generated.FLRArtikellistespr;
import com.lp.server.artikel.service.ArtikelFac;
import com.lp.server.fertigung.fastlanereader.generated.FLRLos;
import com.lp.server.fertigung.fastlanereader.generated.FLRLosPlanungVorschlag;
import com.lp.server.fertigung.fastlanereader.generated.FLRLossollarbeitsplan;
import com.lp.server.fertigung.service.FertigungFac;
import com.lp.server.fertigung.service.LosPlanungVorschlagFac;
import com.lp.server.partner.fastlanereader.generated.FLRPartner;
import com.lp.server.personal.fastlanereader.generated.FLRMaschine;
import com.lp.server.system.pkgenerator.format.LpBelegnummerFormat;
import com.lp.server.system.pkgenerator.format.LpDefaultBelegnummerFormat;
import com.lp.server.system.service.ParameterFac;
import com.lp.server.system.service.ParametermandantDto;
import com.lp.server.util.Facade;
import com.lp.server.util.fastlanereader.FLRSessionFactory;
import com.lp.server.util.fastlanereader.UseCaseHandler;
import com.lp.server.util.fastlanereader.service.query.*;
import com.lp.util.DoppelIcon;
import com.lp.util.EJBExceptionLP;
import com.lp.util.Helper;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.swing.Icon;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.DateFormatSymbols;
import java.util.*;

import static com.lp.server.artikel.service.LagerFac.LAGERART_HAUPTLAGER;
import static com.lp.server.benutzer.service.RechteFac.RECHT_FERT_LOS_R;
import static com.lp.server.util.fastlanereader.service.query.QueryParameters.FLR_BREITE_SHARE_WITH_REST;

/**
 * A UseCaseHandler for the fert_losplanungvorschlag Table.
 * @author MKr, DSk
 */
public class LosPlanungVorschlagHandler extends UseCaseHandler {
	private static final long serialVersionUID = -5509950138201206209L;
	
	private static final String FLR_PROPERTY = "flr_los_planung_vorschlag.";

	private static final String FLR_FLRLOS = "flrlos";
	private static final String FLR_MASCHINE = "flrmaschine";
	private static final String FLR_ARTIKELSTKL = "artikelstkl";
	private static final String FLR_KUNDE_FLRPARTNER = "kunde_flrpartner";

	private static final String FLR_KUNDE_FLRPARTNER_K = "kunde_flrpartner_K";
	private static final String FLR_TAT_ARTIKEL = "tat_artikel";

	private static final String FLR_FROM_CLAUSE = " from FLRLosPlanungVorschlag flr_los_planung_vorschlag " +
			"LEFT OUTER JOIN flr_los_planung_vorschlag.flrmaschine AS " + FLR_MASCHINE + " " +
			"LEFT OUTER JOIN flr_los_planung_vorschlag.flrLossollarbeitsplan " +
			"LEFT OUTER JOIN flr_los_planung_vorschlag.flrLossollarbeitsplan.flrlos AS " + FLR_FLRLOS + " " +
			"LEFT OUTER JOIN flr_los_planung_vorschlag.flrLossollarbeitsplan.flrlos.flrstueckliste.flrartikel AS " + FLR_ARTIKELSTKL + " " +
			"LEFT OUTER JOIN flr_los_planung_vorschlag.flrLossollarbeitsplan.flrlos.flrkunde.flrpartner AS " + FLR_KUNDE_FLRPARTNER_K + " "+
			"LEFT OUTER JOIN flr_los_planung_vorschlag.flrLossollarbeitsplan.flrlos.flrauftrag.flrkunde.flrpartner AS " + FLR_KUNDE_FLRPARTNER + " " +
			"LEFT OUTER JOIN flr_los_planung_vorschlag.flrLossollarbeitsplan.flrartikelliste_taetigkeit AS " + FLR_TAT_ARTIKEL + " ";
	private static final String FLR_SORTQUERY = "select " + FLR_PROPERTY + "i_id" + FLR_FROM_CLAUSE;

	private static final String SQL_OR = " OR ";

	boolean bSuchenInklusiveKbez = true;
	int bLosnummerAuftragsbezogen = 0;

	private String getFromClause() {
		return FLR_FROM_CLAUSE;
	}

	private String buildWhereForPermissions(HashMap<String, Object> params) {
		String where;
		if (theClientDto != null) {
			where = " EXISTS (select pr.i_id from FLRRollerecht pr, FLRTheClient cl " +
					"where pr.flrsystemrolle.i_id = cl.flrsystemrolle.i_id" +
					"  AND pr.flrsystemrolle.i_id = :systemrolleIId " +
					"  AND pr.flrrecht.c_nr = :rechtCNr" +
					"  AND cl.flrsystemrolle.i_id = :systemrolleIId" +
					"  AND cl.cnr = :idClientUser) ";
			params.put("systemrolleIId", theClientDto.getSystemrolleIId());
			params.put("rechtCNr", getRequiredPermissionName());
			params.put("idClientUser", theClientDto.getIDUser());
		} else {
			where = " 1 = 2 ";
		}
		return where;
	}

	private String buildWhereClause(HashMap<String, Object> params) {
		StringBuffer where = new StringBuffer(" WHERE ");


		if (this.getQuery() != null && this.getQuery().getFilterBlock() != null && this.getQuery().getFilterBlock().filterKrit != null) {
			FilterBlock filterBlock = this.getQuery().getFilterBlock();
			FilterKriterium[] filterKriterien = filterBlock.filterKrit;
			String booleanOperator = filterBlock.boolOperator;

			where.append(buildWhereForPermissions(params));

			for (int i = 0; i < filterKriterien.length; i++) {
				if (filterKriterien[i].isKrit) {
					where.append(" ").append(booleanOperator);
					String filterKriterium = filterKriterien[i].value.toLowerCase();
					String filterOperator = filterKriterien[i].operator;
					String filterCriteriaValue = removeSQLQuotas(filterKriterium);


					if (filterKriterien[i].kritName.equals("flrlos.flrauftrag.flrkunde.flrpartner.c_name1nachnamefirmazeile1")) {
						buildWhereFirma_Nachname(where, filterOperator, params, filterCriteriaValue);
						continue;
					}

					if (filterKriterien[i].kritName.equals(ArtikelFac.FLR_ARTIKELLISTE_C_VOLLTEXT)) {
						buildWhereVolltext(where, params, filterKriterien, i);
						continue;
					}

					if (filterKriterien[i].kritName.equals(FLR_MASCHINE + ".c_bez")) {
						buildWhereMaschinenGruppe(where, filterOperator, params, filterKriterium, filterCriteriaValue);
						continue;
					}

					if (filterKriterien[i].kritName.equals(FLR_FLRLOS + "." + FertigungFac.FLR_LOS_STATUS_C_NR)) {
						buildWhereLosStatus(where, filterOperator, params, filterKriterien[i].value);
						continue;
					}

					if (filterKriterien[i].kritName.equals("flrlos." + FertigungFac.FLR_LOS_C_NR)) {
						buildWhereLosNR(where, filterKriterien, i, params);
					}
				}
			}
		}
		return where.toString();

	}


	private void buildWhereFirma_Nachname(StringBuffer where, String filterOperator, HashMap<String, Object> params, String filterCriteriaValue) {
		final String kunde_c_name1nachnamefirmazeile1 = " (lower(" + FLR_KUNDE_FLRPARTNER + ".c_name1nachnamefirmazeile1))";
		final String kunde_c_name2vornamefirmazeile2 = " (lower(" + FLR_KUNDE_FLRPARTNER + ".c_name2vornamefirmazeile2))";
		final String kunde_c_name1nachnamefirmazeile1_k = " (lower(" + FLR_KUNDE_FLRPARTNER_K + ".c_name1nachnamefirmazeile1))";
		final String kunde_c_name2vornamefirmazeile2_k = " (lower(" + FLR_KUNDE_FLRPARTNER_K + ".c_name2vornamefirmazeile2))";

		where.append(" (");
		where.append(kunde_c_name1nachnamefirmazeile1).append(" ").append(filterOperator).append(" :inputParamKunde");
		where.append(SQL_OR);
		where.append(kunde_c_name2vornamefirmazeile2).append(" ").append(filterOperator).append(" :inputParamKunde");
		where.append(SQL_OR);
		where.append(kunde_c_name1nachnamefirmazeile1_k).append(" ").append(filterOperator).append(" :inputParamKunde");
		where.append(SQL_OR);
		where.append(kunde_c_name2vornamefirmazeile2_k).append(" ").append(filterOperator).append(" :inputParamKunde");

		if (bSuchenInklusiveKbez) {
			String kunde_c_bez = " (lower(" + FLR_KUNDE_FLRPARTNER + ".c_kbez))";
			String kunde_c_bez_k = " (lower(" + FLR_KUNDE_FLRPARTNER_K + ".c_kbez))";

			where.append(SQL_OR);
			where.append(kunde_c_bez).append(" ").append(filterOperator).append(" :inputParamKunde");
			where.append(SQL_OR);
			where.append(kunde_c_bez_k).append(" ").append(filterOperator).append(" :inputParamKunde");
		}
		params.put("inputParamKunde", filterCriteriaValue);
		where.append(") ");
	}


	private void buildWhereVolltext(StringBuffer where, HashMap<String, Object> params, FilterKriterium[] filterKriterien, int index) {
		final String[] relevantFields = {
				"lower(Coalesce(" + FLR_ARTIKELSTKL + ".c_nr, ''))",
				"lower(Coalesce("+ FLR_FLRLOS + ".c_nr, ''))",
				"lower(Coalesce(" + FLR_KUNDE_FLRPARTNER + ".c_kbez, ''))",
				"lower(Coalesce(" + FLR_KUNDE_FLRPARTNER_K + ".c_kbez, ''))"
		};

		String[] InputTeile = filterKriterien[index].value.toLowerCase().split(" ");

		where.append("(");
		for (int p = 0; p < InputTeile.length; p++) {

			if (InputTeile[p].startsWith("-")) {
				where.append(" NOT ");
				InputTeile[p] = InputTeile[p].substring(1);
			}

			for (int i = 0; i < relevantFields.length; i++) {
				if (i > 0) {
					where.append(SQL_OR);
				}
				where.append("(").append(relevantFields[i]).append(" like :inputParamVolltext").append(p).append(")");
			}

			params.put("inputParamVolltext" + p, "%" + InputTeile[p] + "%");
			if (p < InputTeile.length - 1) {
				where.append(" AND ");
			}
		}
		where.append(") ");
	}

	private void buildWhereMaschinenGruppe(StringBuffer where, String filterOperator, HashMap<String, Object> params, String filterKriterium, String filterCriteriaValue) {
		String flr_maschine;
		if (filterCriteriaValue.contains("m")) {
			filterCriteriaValue = filterKriterium.replaceAll("m", "");
			flr_maschine = FLR_MASCHINE + ".i_id";
		} else {
			filterCriteriaValue = filterKriterium.replaceAll("g", "");
			flr_maschine = FLR_MASCHINE + ".maschinengruppe_i_id";
		}

		where.append(" ").append(flr_maschine).append(" ").append(filterOperator).append(" ").append(":inputParamMaschine");
		params.put("inputParamMaschine", Integer.parseInt(filterCriteriaValue));

	}


	private void buildWhereLosStatus(StringBuffer where, String filterOperator, HashMap<String, Object> params, String filterCriteriaValue){
		String flr_status = FLR_FLRLOS + "." + FertigungFac.FLR_LOS_STATUS_C_NR;
		where.append(" ").append(flr_status).append(" ").append(filterOperator).append(" ").append(":inputParamLosStatus");
		params.put("inputParamLosStatus", filterCriteriaValue);
	}

	private void buildWhereLosNR(StringBuffer where, FilterKriterium [] filterKriterien, int i, HashMap<String, Object> params) {
		try {
			FilterKriterium fkLocal = new FilterKriterium(filterKriterien[i].kritName, filterKriterien[i].isKrit, filterKriterien[i].value, filterKriterien[i].operator, filterKriterien[i].isBIgnoreCase());
			LpBelegnummerFormat f = getBelegnummerGeneratorObj().getBelegnummernFormat(theClientDto.getMandant());
			LpDefaultBelegnummerFormat defaultFormat = (LpDefaultBelegnummerFormat) f;

			// declare the postfix and bpostFixVorhanden
			String postfix = "-___";
			boolean bPostfixVorhanden = false;
			if (fkLocal.value != null && fkLocal.value.contains("-")) {
				postfix = fkLocal.value.substring(fkLocal.value.indexOf("-"));

				//Wenn Jaheszahl angehaengt, dann diese entfernen
				if(postfix.contains(",")) {
					String jahr=postfix.substring(postfix.indexOf(","));
					if(jahr.endsWith("'")) {
						jahr=jahr.substring(0,jahr.length()-1);
					}

					postfix=postfix.substring(0, postfix.indexOf(","));

					fkLocal.value = fkLocal.value.substring(0, fkLocal.value.indexOf("-")) +jahr+ "'";

				}else {
					fkLocal.value = fkLocal.value.substring(0, fkLocal.value.indexOf("-")) + "'";
				}

				bPostfixVorhanden = true;
			}

			String sValue = super.buildWhereBelegnummer(fkLocal, false);
			if (!istBelegnummernInJahr("FLRLos", sValue)) {
				sValue = super.buildWhereBelegnummer(fkLocal, true);
			}

			if (bLosnummerAuftragsbezogen >= 1) {
				if (sValue != null) {
					sValue = sValue.substring(0, defaultFormat.getStellenGeschaeftsjahr() + 2) + "_"
							+ sValue.substring(defaultFormat.getStellenGeschaeftsjahr() + 3);

				}
			}

			sValue = removeSQLQuotas(sValue);

			if (bLosnummerAuftragsbezogen == 0) {

				String kritName = removeSQLQuotas(fkLocal.kritName);
				String operator = fkLocal.operator;
				where.append(" ").append(kritName).append(" ").append(operator).append(" ").append(":inputParamLosNrSValue");
				params.put("inputParamLosNrSValue", sValue);
			} else {
				int iStellenGesamtAuftragsbezogen = getStellenGesamtAuftragsBezogen(defaultFormat);

				String losnummer = super.buildWhereBelegnummer(fkLocal, false);
				String valueBelegnummernfilterAuftragsbezogen = losnummer.substring(0, defaultFormat.getStellenGeschaeftsjahr() + 2);
				postfix = removeSQLQuotas(postfix);
				String krit;
				if (fkLocal.value.indexOf(",") > 0) {
					krit = removeSQLQuotas(
							fkLocal.value.substring(0, fkLocal.value.indexOf(",")).replaceAll("%", "")
					) + postfix;
				} else {
					krit = removeSQLQuotas(fkLocal.value.replaceAll("%", "")) + postfix;
				}

				if (iStellenGesamtAuftragsbezogen - krit.length() >= 0) {
					valueBelegnummernfilterAuftragsbezogen = Helper.fitString2Length(valueBelegnummernfilterAuftragsbezogen, iStellenGesamtAuftragsbezogen - krit.length() + 1, '0') + krit + "'";
				} else {
					valueBelegnummernfilterAuftragsbezogen = Helper.fitString2Length(valueBelegnummernfilterAuftragsbezogen, 1, '0') + krit + "'";
				}

				if (valueBelegnummernfilterAuftragsbezogen.length() > 14 && !valueBelegnummernfilterAuftragsbezogen.startsWith("'")) {
					valueBelegnummernfilterAuftragsbezogen = "'" + valueBelegnummernfilterAuftragsbezogen;
				}

				if (!istBelegnummernInJahr("FLRLos", valueBelegnummernfilterAuftragsbezogen)
						&& !istBelegnummernInJahr("FLRLos", valueBelegnummernfilterAuftragsbezogen.replaceFirst("0", "").substring(
						0, valueBelegnummernfilterAuftragsbezogen.length() - 2) + "_'")) {
					losnummer = super.buildWhereBelegnummer(fkLocal, true);
					valueBelegnummernfilterAuftragsbezogen = losnummer.substring(0,
							defaultFormat.getStellenGeschaeftsjahr() + 2);

					if (fkLocal.value.indexOf(",") > 0) {
						krit = removeSQLQuotas(fkLocal.value.substring(0, fkLocal.value.indexOf(","))
								.replaceAll("%", "")) + postfix;
					} else {
						krit = removeSQLQuotas(fkLocal.value.replaceAll("%", "")) + postfix;
					}

					if (iStellenGesamtAuftragsbezogen - krit.length() >= 0) {
						valueBelegnummernfilterAuftragsbezogen = Helper.fitString2Length(valueBelegnummernfilterAuftragsbezogen, iStellenGesamtAuftragsbezogen - krit.length() + 1, '0') + krit + "'";
					} else {
						valueBelegnummernfilterAuftragsbezogen = Helper.fitString2Length(valueBelegnummernfilterAuftragsbezogen, 1, '0') + krit + "'";
					}

				}
				// Wenn '-' vorhanden, dann nur nach
				// Auftragsbezogenen Nummer suchen
				String kritName = fkLocal.kritName;
				String operator = fkLocal.operator;

				if (bPostfixVorhanden) {
					where.append(" ").append(kritName).append(" ").append(operator).append(" ").append(":inputParamLosNrAuftragsB").append(" ");
					params.put("inputParamLosNrAuftragsB", valueBelegnummernfilterAuftragsbezogen);
				} else {
					where.append("(").append(" (").append(kritName).append(" ").append(operator).append(" ").append(":inputParamLosNrSValue").append(SQL_OR);
					where.append(" ").append(kritName).append(" ").append(operator).append(" ").append(":inputParamLosNrAuftragsB").append(SQL_OR);
					where.append(" ").append(kritName).append(" ").append(operator).append(" ").append(":inputParamLosNrAuftragsBWithPF").append("_'").append(") ").append(SQL_OR);

					String valueBelegnummernfilterAuftragsbezogenWithPostFix = valueBelegnummernfilterAuftragsbezogen.replaceFirst("0", "").substring(0, valueBelegnummernfilterAuftragsbezogen.length() - 2);
					params.put("inputParamLosNrSValue", sValue);
					params.put("inputParamLosNrAuftragsB", valueBelegnummernfilterAuftragsbezogen);
					params.put("inputParamLosNrAuftragsBWithPF", valueBelegnummernfilterAuftragsbezogenWithPostFix);

					fkLocal.kritName = "flrlos." + FertigungFac.FLR_LOSREPORT_FLRAUFTRAG + ".c_nr";
					whereAuftrag(where, fkLocal);
					where.append(")");
				}

			}

		} catch (Exception ex) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER_FLR, ex);
		}
	}

	private int getStellenGesamtAuftragsBezogen(LpDefaultBelegnummerFormat defaultFormat) {
		int iStellenGesamtAuftragsbezogen = defaultFormat.getStellenGeschaeftsjahr() + defaultFormat.getStellenLfdNummer() + 5;

		// Losnummer kann maximal 12-stellig sein
		if (defaultFormat.getStellenGeschaeftsjahr() == 2) {
			if (iStellenGesamtAuftragsbezogen > 12) {

				iStellenGesamtAuftragsbezogen = 12;

			}
		}

		if (defaultFormat.getStellenGeschaeftsjahr() == 4) {
			if (iStellenGesamtAuftragsbezogen > 13) {

				iStellenGesamtAuftragsbezogen = 13;

			}
		}

		return iStellenGesamtAuftragsbezogen;
	}

	private void whereAuftrag(StringBuffer where, FilterKriterium filterKriterium) {
		try {

			String sValue = super.buildWhereBelegnummer(filterKriterium, false);

			// Belegnummernsuche auch in "altem" Jahr, wenn im
			// neuen noch keines vorhanden ist
			if (!istBelegnummernInJahr("FLRAuftrag", sValue)) {
				sValue = super.buildWhereBelegnummer(filterKriterium, true);
			}
			where.append(" ").append(filterKriterium.kritName);
			where.append(" ").append(filterKriterium.operator);
			where.append(" ").append(sValue);
		} catch (Exception ex) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER_FLR, ex);
		}
	}


	private String buildOrderByClause() {
		if (getQuery() == null)
			return "";

		String orderByCnr = FLR_PROPERTY + "i_reihung";
		StringBuilder orderBy = new StringBuilder();
		SortierKriterium[] kriterien = getQuery().getSortKrit();
		boolean sortAdded = false;
		if (kriterien != null && kriterien.length > 0) {
			for (SortierKriterium krit : kriterien) {
				if (krit.isKrit) {
					if (sortAdded)
						orderBy.append(", ");
					
					sortAdded = true;
					orderBy.append(orderPart(FLR_PROPERTY + krit.kritName, krit.value));
				}
			}
		} else {
			orderBy.append(orderPart(FLR_PROPERTY + "t_plan_beginn", "ASC")).append(", ");
			orderBy.append(orderPart(FLR_MASCHINE + ".c_bez", "ASC")).append(", ");
			orderBy.append(orderPart(FLR_PROPERTY + "i_reihung", "ASC")).append(", ");
			orderBy.append(orderPart(FLR_PROPERTY + "i_maschinenversatz_ms", "ASC"));
			sortAdded = true;
		}
		
		if (orderBy.indexOf(orderByCnr) < 0) {
			if (sortAdded) {
				orderBy.append(", ");
			}
			orderBy.append(orderPart(orderByCnr, "ASC"));
			sortAdded = true;
		}
		
		if (sortAdded) {
			orderBy.insert(0, " ORDER BY ");
		}
		return orderBy.toString();
	}
	
	private String orderPart(String column, String order) {
		return column + " " + (Helper.isStringEmpty(order) ? "" : (order + " "));
	}
	
	@Override
	protected long getRowCountFromDataBase() {
		HashMap<String, Object> params = new HashMap<>();
		String queryString = "select count(*) " + getFromClause() + this.buildWhereClause(params);
		return getRowCountFromDataBaseByQuery(queryString, params);
	}

	@Override
	public QueryResult sort(SortierKriterium[] sortierKriterien, Object selectedId) throws EJBExceptionLP {
		HashMap<String, Object> params = new HashMap<>();
		return defaultSort(sortierKriterien, selectedId,
				FLR_SORTQUERY + this.buildWhereClause(params) + buildOrderByClause(), params);
	}

	private Object getLosStatusIconValue(final Map<Integer, String> allZusatzstatus, FLRLos los) throws RemoteException {
		if (!allZusatzstatus.isEmpty()) {
			DoppelIcon ia = new DoppelIcon();
			Object[] oIcon1 = getStatusMitUebersetzung(los.getStatus_c_nr());
			if (oIcon1 != null) {
				if (oIcon1.length > 0) {
					ia.setIcon1((String) oIcon1[0]);
				}
				if (oIcon1.length > 1) {
					ia.setTooltip1((String) oIcon1[1]);
				}
			}

			// Juengsten Zusatzstatus holen
			Integer zusatzstatusIId = getFertigungFac().getJuengstenZusatzstatuseinesLoses(los.getI_id());
			if (zusatzstatusIId != null) {
				allZusatzstatus.get(zusatzstatusIId);
				String zusatzstatus = Helper.fitString2Length(allZusatzstatus.get(zusatzstatusIId), 15, ' ');
				ia.setIcon2(zusatzstatus);
				ia.setTooltip2(zusatzstatus);
			}

			return ia;
		} else {
			return getStatusMitUebersetzung(los.getStatus_c_nr());
		}
	}

	@Override
	public QueryResult getPageAt(Integer rowIndex) throws EJBExceptionLP {
		QueryResult result;
		SessionFactory factory = FLRSessionFactory.getFactory();
		Session session = null;
		
		try {
			int colCount = getTableInfo().getColumnClasses().length + 1;
			int pageSize = getLimit();
			int startIndex = getStartIndex(rowIndex, pageSize);
			int endIndex = startIndex + pageSize - 1;

			session = factory.openSession();
			session = setFilter(session);
			HashMap<String, Object> params = new HashMap<>();
			String queryString = this.getFromClause() + this.buildWhereClause(params) + this.buildOrderByClause();

			Query query = session.createQuery(queryString);
			for (Map.Entry<String, Object> set: params.entrySet()) {
				query.setParameter(set.getKey(), set.getValue());
			}

			query.setFirstResult(startIndex);
			query.setMaxResults(pageSize);
			List<Object[]> resultList = query.list();

			Object[][] rows = new Object[resultList.size()][colCount];

			LosPlanungVorschlagFac losPlanungVorschlagFac = getLosPlanungVorschlagFac();

			final String[] kurzeWochentage = new DateFormatSymbols(theClientDto.getLocUi()).getShortWeekdays();
			final Map<Integer, String> allZusatzstatus = getFertigungFac().getAllZusatzstatus(theClientDto);

			int row = 0;
			int col = 0;

			HashMap<Integer, HashMap<Date, BigDecimal>> maschineVerfuegbarkeit = new HashMap<>();

			for (Object[] results : resultList) {
				FLRLosPlanungVorschlag planungVorschlag = (FLRLosPlanungVorschlag) results[0];
				FLRMaschine maschine = (FLRMaschine) results[1];
				FLRLossollarbeitsplan losSollArbeitsplan = (FLRLossollarbeitsplan) results[2];
				FLRLos los = (FLRLos) results[3];
				FLRArtikelliste stklArtikel = (FLRArtikelliste) results[4];
				FLRPartner kunde = (FLRPartner) results[5];
				FLRPartner kunde2 = (FLRPartner) results[6];
				FLRArtikelliste taetigkeitArtikel = (FLRArtikelliste) results[7];

				if (kunde == null && kunde2 != null) {
					kunde = kunde2;
				}

				rows[row][col++] = planungVorschlag.getI_id();
				rows[row][col++] = planungVorschlag.getI_reihung();

				BigDecimal maschineverfuegbareStunden = null;
				if (maschine != null) {
					rows[row][col++] = maschine.getC_bez();

					// Get the Available Hourse of the Machine for that day and store it in Hashmaps,
					// so we don't have to reload it multiple times from the database.
					if (!maschineVerfuegbarkeit.containsKey(maschine.getI_id())) {
						maschineVerfuegbarkeit.put(maschine.getI_id(), new HashMap<>());
					}
					HashMap<Date, BigDecimal> maschineVerfuegbarkeitStunden = maschineVerfuegbarkeit.get(maschine.getI_id());
					java.sql.Date sqlDate = new java.sql.Date(planungVorschlag.getT_plan_beginn().getTime());
					if (!maschineVerfuegbarkeitStunden.containsKey(sqlDate)) {
						BigDecimal verfuegbareStunden = getMaschineFac().getVerfuegbarkeitInStundenZuDatum(
								maschine.getI_id(), sqlDate, theClientDto
						);
						maschineVerfuegbarkeitStunden.put(sqlDate, verfuegbareStunden);
					}
					maschineverfuegbareStunden = maschineVerfuegbarkeitStunden.get(sqlDate);
				} else {
					rows[row][col++] = null;
				}

				Integer losID = null;
				BigDecimal losgroesse = null;
				if (los != null) {
					losID = los.getI_id();
					losgroesse = los.getN_losgroesse();
					rows[row][col++] = los.getC_nr();
					rows[row][col++] = getLosStatusIconValue(allZusatzstatus, los);
					rows[row][col++] = losgroesse;
				} else {
					rows[row][col++] = null;
					rows[row][col++] = null;
					rows[row][col++] = null;
				}

				Integer aufspannung = null;
				if (losSollArbeitsplan != null) {
					aufspannung = losSollArbeitsplan.getI_aufspannung();
					rows[row][col++] = losSollArbeitsplan.getI_arbeitsgangsnummer();
					rows[row][col++] = losSollArbeitsplan.getI_unterarbeitsgang();
				} else {
					rows[row][col++] = null;
					rows[row][col++] = null;
				}

				if (taetigkeitArtikel != null) {
					rows[row][col++] = taetigkeitArtikel.getC_nr();
					Object[] bezArray = taetigkeitArtikel.getArtikelsprset().toArray();
					if (bezArray.length > 0) {
						rows[row][col++]  = ((FLRArtikellistespr)bezArray[0]).getC_bez();
					} else {
						rows[row][col++] = null;
					}
				} else {
					rows[row][col++] = null;
					rows[row][col++] = null;
				}

				if (stklArtikel != null) {
					rows[row][col++] = stklArtikel.getC_nr();
					Object[] bezArray = stklArtikel.getArtikelsprset().toArray();
					if (bezArray.length > 0) {
						rows[row][col++]  = ((FLRArtikellistespr)bezArray[0]).getC_bez();
					} else {
						rows[row][col++] = null;
					}
				} else {
					rows[row][col++] = null;
					rows[row][col++] = null;
				}

				rows[row][col++] = (kunde != null) ? kunde.getC_kbez() : null;

				Timestamp tPlanBeginn = planungVorschlag.getT_plan_beginn();
				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis(tPlanBeginn.getTime());

				rows[row][col++] = kurzeWochentage[cal.get(Calendar.DAY_OF_WEEK)];
				rows[row][col++] = tPlanBeginn;
				rows[row][col++] = maschineverfuegbareStunden;

				rows[row][col++] = Helper.berechneGesamtzeitInStunden(
						planungVorschlag.getL_ruestzeit().longValue(),
						planungVorschlag.getL_stueckzeit().longValue(),
						losgroesse, BigDecimal.ONE, aufspannung
				);

				rows[row][col++] = losPlanungVorschlagFac.checkMaterialComplete(losID, LAGERART_HAUPTLAGER);

				// TODO Append Missing Columns:
				/*
      				-- ev. Anzeige der Tages Reststunden damit man die Berechnung prüfen kann
      				-- Abliefertermin aus dem zugeordneten Auftrag mit Prüfung auf das theoretische Los-Ende (inkl. Firmenzeitmodell) -> ev. ein eigener Prüflauf, wenn das je Los zu lange dauern würde (oder das machen wir im Druck)
      			*/

				col = 0;
				row++;
			}
			result = new QueryResult(rows, getRowCount(), startIndex, endIndex, 0);
		} catch (HibernateException | RemoteException e) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER_FLR, e);
		} finally {
			closeSession(session);
		}
		
		return result;
	}
	
	@Override
	public TableInfo getTableInfo() {
		if (super.getTableInfo() == null) {
			String mandant = theClientDto.getMandant();
			Locale locUI = theClientDto.getLocUi();
			setupParameters();

			TableInfo ti = new TableInfo(new Class[0], new Object[0], new int[0], new String[0]);
			ti.spalteHinzufuegen(Integer.class, getTextRespectUISpr("proj.id", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, "i_id");
			ti.spalteHinzufuegen(Integer.class, getTextRespectUISpr("fert.reihung", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, "i_reihung");
			ti.spalteHinzufuegen(String.class, getTextRespectUISpr("lp.maschine", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, "flrmaschine.c_bez");
			ti.spalteHinzufuegen(String.class, getTextRespectUISpr("lp.losnr", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, "flrLossollarbeitsplan.flrlos.c_nr");
			ti.spalteHinzufuegen(Icon.class, getTextRespectUISpr("lp.status", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, "flrLossollarbeitsplan.flrlos.status_c_nr");
			ti.spalteHinzufuegen(BigDecimal.class, getTextRespectUISpr("fert.losgroesse", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, "flrLossollarbeitsplan.flrlos.n_losgroesse");
			ti.spalteHinzufuegen(Integer.class, getTextRespectUISpr("fert.ag", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, "flrLossollarbeitsplan.i_arbeitsgangsnummer");
			ti.spalteHinzufuegen(Integer.class, getTextRespectUISpr("fert.uag", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, "flrLossollarbeitsplan.i_unterarbeitsgang");
			ti.spalteHinzufuegen(String.class, getTextRespectUISpr("fert.offeneags.artikelnummertaetigkeit", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, "flrLossollarbeitsplan.flrartikelliste_taetigkeit.c_nr");
			ti.spalteHinzufuegen(String.class, getTextRespectUISpr("fert.offeneags.bezeichnungtaetigkeit", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, Facade.NICHT_SORTIERBAR);
			ti.spalteHinzufuegen(String.class, getTextRespectUISpr("fert.offeneags.artikelnummerstkl", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, "flrLossollarbeitsplan.flrlos.flrstueckliste.flrartikel.c_nr");
			ti.spalteHinzufuegen(String.class, getTextRespectUISpr("fert.offeneags.bezeichnungstkl", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, Facade.NICHT_SORTIERBAR);
			ti.spalteHinzufuegen(String.class, getTextRespectUISpr("lp.kunde", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, Facade.NICHT_SORTIERBAR);
			ti.spalteHinzufuegen(String.class, getTextRespectUISpr("lp.tag", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, "t_plan_beginn");
			ti.spalteHinzufuegen(Date.class, getTextRespectUISpr("lp.datum", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, "t_plan_beginn");
			ti.spalteHinzufuegen(BigDecimal.class, getTextRespectUISpr("fert.los.planung.maschine.stunden", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, Facade.NICHT_SORTIERBAR);
			ti.spalteHinzufuegen(BigDecimal.class, getTextRespectUISpr("stkl.gesamtzeit", mandant, locUI), FLR_BREITE_SHARE_WITH_REST, Facade.NICHT_SORTIERBAR);
			ti.spalteHinzufuegen(Boolean.class, "Material vorhanden", FLR_BREITE_SHARE_WITH_REST, Facade.NICHT_SORTIERBAR);

			// TODO Append Missing Columns:
				/*
      				-- Verfügbare Stunden der Maschine für diesen Tag
      				-- ev. Anzeige der Tages Reststunden damit man die Berechnung prüfen kann
      				-- Ident der Tätigkeit
      				-- Bezeichnung der Tätigkeit
      				-- Verfügbarkeit des Materials und oder der Materialgruppe (SMT-Material)
      				-- Abliefertermin aus dem zugeordneten Auftrag mit Prüfung auf das theoretische Los-Ende (inkl. Firmenzeitmodell) -> ev. ein eigener Prüflauf, wenn das je Los zu lange dauern würde (oder das machen wir im Druck)
      			*/

			setTableInfo(ti);
		}
		return super.getTableInfo();
	}

	private void setupParameters() {
		try {
			ParametermandantDto parameter = getParameterFac().getMandantparameter(theClientDto.getMandant(),
					ParameterFac.KATEGORIE_ALLGEMEIN, ParameterFac.PARAMETER_SUCHEN_INKLUSIVE_KBEZ);
			bSuchenInklusiveKbez = (java.lang.Boolean) parameter.getCWertAsObject();

			parameter = getParameterFac().getMandantparameter(theClientDto.getMandant(),
					ParameterFac.KATEGORIE_FERTIGUNG, ParameterFac.PARAMETER_LOSNUMMER_AUFTRAGSBEZOGEN);
			bLosnummerAuftragsbezogen = (Integer) parameter.getCWertAsObject();

		} catch (RemoteException ex) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER, ex);
		}
	}

	@Override
	protected String getRequiredPermissionName() {
		return RECHT_FERT_LOS_R;
	}
}
