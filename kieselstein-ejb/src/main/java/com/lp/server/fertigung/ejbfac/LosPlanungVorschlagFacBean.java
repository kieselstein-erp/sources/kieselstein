/*******************************************************************************
 * kieselsteinERP, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2023 - 2024 Kieselstein ERP eG, X-Net Services GmbH
 * based on: HELIUM V, Copyright (C) 2004 - 2022 HELIUM V IT-Solutions GmbH
 * <p>
 * distributed under the terms of GNU Affero General Public License, version 3
 * with supplements. See LICENSE file in base source directory for details.
 * <p>
 * Contact: help@kieselstein-erp.org
 ******************************************************************************/
package com.lp.server.fertigung.ejbfac;

import com.google.common.util.concurrent.AtomicDouble;
import com.lp.server.fertigung.ejb.*;
import com.lp.server.fertigung.service.*;
import com.lp.layer.hibernate.HvTypedQuery;
import com.lp.server.personal.ejb.Maschine;
import com.lp.server.personal.ejb.Personal;
import com.lp.server.system.pkgenerator.PKConst;
import com.lp.server.system.service.*;
import com.lp.server.util.Facade;
import com.lp.util.EJBExceptionLP;
import com.lp.util.Helper;
import org.jboss.ejb3.annotation.TransactionTimeout;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.*;

import static com.lp.server.artikel.service.LagerFac.LAGERART_HAUPTLAGER;
import static com.lp.server.benutzer.service.RechteFac.RECHT_FERT_LOS_CUD;
import static com.lp.server.fertigung.service.FertigungFac.*;

@Stateless
public class LosPlanungVorschlagFacBean extends Facade implements LosPlanungVorschlagFac {
	@PersistenceContext
	private EntityManager em;

	@Resource
	private SessionContext context;


	@Override
	public LosPlanungvorschlagDto findByPrimaryKey(Integer iId) throws EJBExceptionLP, RemoteException {
		LosPlanungVorschlag planungVorschlag = em.find(LosPlanungVorschlag.class, iId);
		if (planungVorschlag == null) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER_BEI_FINDBYPRIMARYKEY, "");
		}
		LosPlanungvorschlagDto dto = new LosPlanungvorschlagDto();
		dto.setIId(planungVorschlag.getIId());
		dto.setLossollarbeitsplanIId(planungVorschlag.getLosSollArbeitsplanIId());
		dto.setMaschineIId(planungVorschlag.getMaschineIId());
		dto.setlRuestzeit(planungVorschlag.getlRuestzeit());
		dto.setlStueckzeit(planungVorschlag.getlStueckzeit());
		dto.setiReihung(planungVorschlag.getiReihung());
		dto.settPlanBeginn(planungVorschlag.gettPlanBeginn());
		dto.setPersonalIIdAnlegen(planungVorschlag.getPersonalIIdAnlegen());
		dto.setPersonalIIdAendern(planungVorschlag.getPersonalIIdAendern());
		dto.setTAnlegen(planungVorschlag.getTAnlegen());
		dto.setTAendern(planungVorschlag.getTAendern());
		dto.setcLosText(planungVorschlag.getcLosText());
		return dto;
	}

	@Override
	public boolean checkMaterialComplete(Integer losId, String relevantLagerArt) {
		List<Object[]> materials = LossollmaterialQuery.listMaterialMengenByLosIDLagerart(
				em, losId, Collections.singletonList(relevantLagerArt)
		);
		for (Object[] values : materials) {
			BigDecimal sollMenge = (BigDecimal) values[2];
			BigDecimal istMenge = (BigDecimal) values[3];
			BigDecimal lagerMenge = (BigDecimal) values[4];
			if (sollMenge.compareTo(istMenge.add(lagerMenge)) > 0) {
				return false;
			}
		}
		System.out.println(materials);
		return true;
	}

	private Date calculateToday() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	private void lockMaschineForPlanningSuggestion(Integer maschineID, TheClientDto clientDto) {
		TheJudgeFac judge = getTheJudgeFac();
		String lockWas = maschineID.toString();
		boolean existsLock = false;
        try {
			LockMeDto[] locks = judge.findByWerWas(LOCKME_FERT_PLANUNGVORSCHLAG, lockWas);
			for (LockMeDto l: locks) {
				if (!l.getCUsernr().trim().equals(clientDto.getIDUser())) {
					Personal lockPerson = em.find(Personal.class, l.getPersonalIIdLocker());
					String lockStr = (lockPerson != null ? lockPerson.getCKurzzeichen() : l.getPersonalIIdLocker().toString());
					throw new EJBExceptionLP(EJBExceptionLP.FEHLER_DATENSATZ_GELOCKT,
							"FEHLER_DATENSATZ_GELOCKT", lockStr);
				} else {
					existsLock = true;
				}
			}
        } catch (RemoteException e) {
            e.printStackTrace();
        }
		if (!existsLock) {
			LockMeDto newLock = new LockMeDto(LOCKME_FERT_PLANUNGVORSCHLAG, lockWas, clientDto.getIDUser());
			newLock.setPersonalIIdLocker(clientDto.getIDPersonal());
            try {
                judge.addLockedObject(newLock, clientDto);
            } catch (RemoteException e) {
                throw new RuntimeException(e);
            }
        }

    }

	private void removeLockMaschineForPlanningSuggestion(Integer maschineID, TheClientDto clientDto) {
		TheJudgeFac judge = getTheJudgeFac();
		String lockWas = maschineID.toString();
		LockMeDto oldLock = new LockMeDto(LOCKME_FERT_PLANUNGVORSCHLAG, lockWas, clientDto.getIDUser());
		judge.removeLockedObjectIfExists(oldLock);
    }

	private Date calculateNextPlaningDate(Integer maschineID, Date currentPlannedDate,
										  Los currentLos, Lossollarbeitsplan currentAG,
										  Long ruestzeit, Long stueckzeit,
										  AtomicDouble allReadyPlanedHours, HashMap<Date, BigDecimal> maschineVerfuegbareStundenMap,
										  TheClientDto clientDto) {
		if (!maschineVerfuegbareStundenMap.containsKey(currentPlannedDate)) {
			BigDecimal verfuegbareStunden = getMaschineFac().getVerfuegbarkeitInStundenZuDatum(
					maschineID, new java.sql.Date(currentPlannedDate.getTime()), clientDto
			);
			maschineVerfuegbareStundenMap.put(currentPlannedDate, verfuegbareStunden);
		}
		double verfuegbarkeitInStunden = maschineVerfuegbareStundenMap.get(currentPlannedDate).doubleValue();

		BigDecimal gesamtZeit = Helper.berechneGesamtzeitInStunden(
				ruestzeit, stueckzeit, currentLos.getNLosgroesse(), BigDecimal.ONE, currentAG.getIAufspannung()
		);

		allReadyPlanedHours.set(allReadyPlanedHours.doubleValue() + gesamtZeit.doubleValue());

		Date nextPlanningDate = currentPlannedDate;
		if (verfuegbarkeitInStunden <= allReadyPlanedHours.doubleValue()) {
			// The AG won't be finished on the day, go through the next days until the AG can be settled.
			Calendar c = Calendar.getInstance();
			c.setTime(currentPlannedDate);

			allReadyPlanedHours.set(allReadyPlanedHours.doubleValue() - verfuegbarkeitInStunden);
			c.add(Calendar.DATE, 1);
			nextPlanningDate = c.getTime();
			if (!maschineVerfuegbareStundenMap.containsKey(nextPlanningDate)) {
				BigDecimal verfuegbareStunden = getMaschineFac().getVerfuegbarkeitInStundenZuDatum(
						maschineID, new java.sql.Date(nextPlanningDate.getTime()), clientDto
				);
				maschineVerfuegbareStundenMap.put(nextPlanningDate, verfuegbareStunden);
			}
			double newVerfuegbarkeitInStunden = maschineVerfuegbareStundenMap.get(nextPlanningDate).doubleValue();

			int foundDaysWith0Hours = 0;
			while (newVerfuegbarkeitInStunden <= allReadyPlanedHours.doubleValue()) {
				if (newVerfuegbarkeitInStunden == 0) {
					foundDaysWith0Hours++;
				} else {
					foundDaysWith0Hours = 0;
				}

				if (foundDaysWith0Hours > 30) {
					// When 30 Days in follow 0 Hours for the maschine was found raise a Exception.
					Maschine maschine = em.find(Maschine.class, maschineID);
					throw new EJBExceptionLP(EJBExceptionLP.FEHLER_LOS_PLANNUNG_MASCHINEN_ZETMODELL_FEHLT,
							"FEHLER_LOS_PLANNUNG_MASCHINEN_ZETMODELL_FEHLT", maschine.getCBez());
				}
				allReadyPlanedHours.set(allReadyPlanedHours.doubleValue() - newVerfuegbarkeitInStunden);
				c.add(Calendar.DATE, 1);
				nextPlanningDate = c.getTime();
				if (!maschineVerfuegbareStundenMap.containsKey(nextPlanningDate)) {
					BigDecimal verfuegbareStunden = getMaschineFac().getVerfuegbarkeitInStundenZuDatum(
							maschineID, new java.sql.Date(nextPlanningDate.getTime()), clientDto
					);
					maschineVerfuegbareStundenMap.put(nextPlanningDate, verfuegbareStunden);
				}
				newVerfuegbarkeitInStunden = maschineVerfuegbareStundenMap.get(nextPlanningDate).doubleValue();
			}
		}
		return nextPlanningDate;
	}

	@Override
	public boolean generatePlanningSuggestions(
			Integer iMaschinenGruppeID, boolean bMaterialKomplett, Date startDate, Date maxLosBeginDate,
			String minimalLosStatus, TheClientDto clientDto
	) throws EJBExceptionLP {
		checkPermissions(RECHT_FERT_LOS_CUD, clientDto);
		List<Integer> maschineIds = new ArrayList<>();
		HvTypedQuery<Maschine> query;
		if (iMaschinenGruppeID == null) {
			// If no MaschineGruppeID is given handle all Maschines.
			query = new HvTypedQuery<>(em.createNamedQuery("MaschineGetAll"));
		} else {
			query = new HvTypedQuery<>(em.createNamedQuery("MaschinefindByMaschinengruppeIId"));
			query.setParameter("maschinengruppeIId", iMaschinenGruppeID);
		}

		List<String> relevantStatus;
		if (STATUS_AUSGEGEBEN.equals(minimalLosStatus)) {
			relevantStatus = Arrays.asList(STATUS_AUSGEGEBEN, STATUS_IN_PRODUKTION, STATUS_TEILERLEDIGT);
		} else if (STATUS_IN_PRODUKTION.equals(minimalLosStatus)) {
			relevantStatus = Arrays.asList(STATUS_IN_PRODUKTION, STATUS_TEILERLEDIGT);
		} else if (STATUS_TEILERLEDIGT.equals(minimalLosStatus)) {
			relevantStatus = Collections.singletonList(STATUS_TEILERLEDIGT);
		} else {
			relevantStatus = Arrays.asList(STATUS_ANGELEGT, STATUS_AUSGEGEBEN, STATUS_IN_PRODUKTION, STATUS_TEILERLEDIGT);
		}

		for (Maschine m : query.getResultList()) {
			maschineIds.add(m.getIId());
			lockMaschineForPlanningSuggestion(m.getIId(), clientDto);
		}

		// Delete All Existing Entries for the given Machines.
		em.createNamedQuery("LosPlanungVorschlagDeleteByMaschine").setParameter(
				"maschineIds", maschineIds
		).executeUpdate();

		if (startDate == null) {
			// Use TODAY as Date when no startDate is given.
			startDate = calculateToday();
		}

		// Select all relevant AGs for the Planning
		List<Lossollarbeitsplan> ags = LossollarbeitsplanQuery.listNichtFertigeByMaschineAndDatum(
				em, maschineIds, maxLosBeginDate, relevantStatus
		);

		HashMap<Integer, Date> lastDateMap = new HashMap<>();
		HashMap<Integer, BigDecimal> lastDateHoursMap = new HashMap<>();
		HashMap<Integer, Integer> lastReiheForMaschineMap = new HashMap<>();
		HashMap<Integer, HashMap<Date, BigDecimal>> maschineVerfuegbarkeit = new HashMap<>();

		for (Integer maschineID : maschineIds) {
			lastDateMap.put(maschineID, startDate);
			lastDateHoursMap.put(maschineID, BigDecimal.ZERO);
			lastReiheForMaschineMap.put(maschineID, 0);
			maschineVerfuegbarkeit.put(maschineID, new HashMap<>());
		}

		for (Lossollarbeitsplan ag : ags) {
			boolean isAGRelevant;

			if (bMaterialKomplett) {
				// Checks of necessary Materials.
				isAGRelevant = checkMaterialComplete(ag.getLosIId(), LAGERART_HAUPTLAGER);
			} else {
				isAGRelevant = true;
			}

			Integer maschineID = ag.getMaschineIId();

			if (isAGRelevant) {
				Date plannedDate = lastDateMap.get(maschineID);
				AtomicDouble allReadyPlanedHours = new AtomicDouble(lastDateHoursMap.get(maschineID).doubleValue());
				Integer reihe = lastReiheForMaschineMap.get(maschineID);
				Integer versatzMS = (int) (allReadyPlanedHours.doubleValue() * 60 * 60 * 1000);
				Los agLos = ag.getLos();
				Date nextPlaningDate = calculateNextPlaningDate(
						maschineID, plannedDate, agLos, ag, ag.getLRuestzeit(), ag.getLStueckzeit(),
						allReadyPlanedHours, maschineVerfuegbarkeit.get(maschineID), clientDto
				);
				reihe = reihe + 1;

				LosPlanungVorschlag planungVorschlag = new LosPlanungVorschlag(
						getPKGeneratorObj().getNextPrimaryKey(PKConst.PK_LOS_PLANUNG_VORSCHLAG),
						ag.getIId(), maschineID, ag.getLRuestzeit(), ag.getLStueckzeit(),
						reihe, new Timestamp(plannedDate.getTime()), versatzMS, agLos.getXText(), clientDto.getIDPersonal()
				);

				em.persist(planungVorschlag);

				lastDateMap.put(maschineID, nextPlaningDate);
				lastDateHoursMap.put(maschineID, BigDecimal.valueOf(allReadyPlanedHours.doubleValue()));
				lastReiheForMaschineMap.put(maschineID, reihe);
			}
		}

		for (Integer maschineID : maschineIds) {
			removeLockMaschineForPlanningSuggestion(maschineID, clientDto);
		}

		em.flush();

		return true;
	}

	private boolean changePlanningOrder(LosPlanungVorschlag planung, Integer reihe, Date plannedDate, Integer versatzMS, TheClientDto clientDto) {
		Timestamp newPlanBeginn = new Timestamp(plannedDate.getTime());
		System.out.println("changePlanningOrder newReihe " + reihe + " oldReihe: " + planung.getiReihung());
		if (!Objects.equals(reihe, planung.getiReihung()) ||
			!Objects.equals(planung.gettPlanBeginn(), newPlanBeginn) ||
			!Objects.equals(versatzMS, planung.getiMaschinenversatzMs())) {
			planung.setiReihung(reihe);
			planung.settPlanBeginn(newPlanBeginn);
			planung.setiMaschinenversatzMs(versatzMS);
			planung.setPersonalIIdAendern(clientDto.getIDPersonal());
			planung.setTAendern(new Timestamp(System.currentTimeMillis()));
			em.merge(planung);
			return true;
		}
		return false;
	}

	private void reorderPlanningForMachine(Integer machineID, Date startDate, TheClientDto clientDto) {
		Query query = em.createNamedQuery("LosPlanungVorschlagForSavePlanningDatesFindByMaschineOrderdByReihung");
		query.setParameter("maschineIds", Collections.singletonList(machineID));

		Date nextPlaningDate = startDate;
		AtomicDouble allReadyPlanedHours = new AtomicDouble(0);
		int reihe = 0;

		HashMap<Date, BigDecimal> maschineVerfuegbarkeit = new HashMap<>();

		for (Object o : query.getResultList()) {
			Object[] results = (Object[])o;
			LosPlanungVorschlag planung = (LosPlanungVorschlag)results[0];
			Lossollarbeitsplan ag = (Lossollarbeitsplan)results[1];
			Los los = (Los)results[2];

			if (nextPlaningDate == null) {
				// Get The first Date for the Start of the planing.
				nextPlaningDate = planung.gettPlanBeginn();
			}

			Date plannedDate = nextPlaningDate;

			reihe = reihe + 1;
			Integer versatzMS = (int) (allReadyPlanedHours.doubleValue() * 60 * 60 * 1000);
			changePlanningOrder(planung, reihe, plannedDate, versatzMS, clientDto);
			nextPlaningDate = calculateNextPlaningDate(
					machineID, plannedDate, los, ag, planung.getlRuestzeit(), planung.getlStueckzeit(),
					allReadyPlanedHours, maschineVerfuegbarkeit, clientDto);
		}
	}

	@Override
	public void movePlanningOrderToReihen(
			Integer iKey, Integer newTargetReihe,
			TheClientDto clientDto
	) throws EJBExceptionLP {
		checkPermissions(RECHT_FERT_LOS_CUD, clientDto);
		LosPlanungVorschlag planung2Move = em.find(LosPlanungVorschlag.class, iKey);
		lockMaschineForPlanningSuggestion(planung2Move.getMaschineIId(), clientDto);
		boolean handledGivenPlanung = false;

		Query query = em.createNamedQuery("LosPlanungVorschlagForSavePlanningDatesFindByMaschineOrderdByPlanBeginn");
		query.setParameter("maschineIds", Collections.singletonList(planung2Move.getMaschineIId()));

		Date nextPlaningDate = null;
		AtomicDouble allReadyPlanedHours = new AtomicDouble(0);
		int reihe = 0;

		HashMap<Date, BigDecimal> maschineVerfuegbarkeit = new HashMap<>();

		for (Object o : query.getResultList()) {
			Object[] results = (Object[])o;
			LosPlanungVorschlag planung = (LosPlanungVorschlag)results[0];
			Lossollarbeitsplan ag = (Lossollarbeitsplan)results[1];
			Los los = (Los)results[2];

			if (nextPlaningDate == null) {
				// Get The first Date for the Start of the planing.
				nextPlaningDate = planung.gettPlanBeginn();
			}

			Date plannedDate = nextPlaningDate;
			Integer maschineID = planung.getMaschineIId();
			boolean isTargetReihe = (newTargetReihe != null && reihe + 1 == newTargetReihe);
			boolean isNotGivenKey = !planung.getIId().equals(iKey);
			if (isTargetReihe || isNotGivenKey) {
				reihe = reihe + 1;
			}

			if (isTargetReihe) {
				handledGivenPlanung = true;
				Integer versatzMS = (int) (allReadyPlanedHours.doubleValue() * 60 * 60 * 1000);
				nextPlaningDate = calculateNextPlaningDate(maschineID, plannedDate,
						planung2Move.getLossollarbeitsplan().getLos(), planung2Move.getLossollarbeitsplan(),
						planung2Move.getlRuestzeit(), planung2Move.getlStueckzeit(),
						allReadyPlanedHours, maschineVerfuegbarkeit, clientDto
				);
				changePlanningOrder(planung2Move, reihe, plannedDate, versatzMS, clientDto);
				plannedDate = nextPlaningDate;
				if (isNotGivenKey) {
					// Calculate the next values.
					reihe = reihe + 1;

				}
			}

			if (isNotGivenKey) {
				Integer versatzMS = (int) (allReadyPlanedHours.doubleValue() * 60 * 60 * 1000);
				nextPlaningDate = calculateNextPlaningDate(maschineID, plannedDate, los, ag,
						planung.getlRuestzeit(), planung.getlStueckzeit(),
						allReadyPlanedHours, maschineVerfuegbarkeit, clientDto
				);
				changePlanningOrder(planung, reihe, plannedDate, versatzMS, clientDto);
			}
		}

		if (!handledGivenPlanung) {
			reihe = reihe + 1;
			Date plannedDate;
			if (nextPlaningDate == null) {
				Timestamp minPlanungBeginn;
				try {
					// Get the Date of the first Planungsvorschlag or use the today date.
					Query minPlanungBeginquery = em.createNamedQuery("LosPlanungVorschlagMinBegin");
					minPlanungBeginn = (Timestamp) minPlanungBeginquery.getSingleResult();
				} catch (NoResultException e) {
					minPlanungBeginn = null;
				}

				if (minPlanungBeginn == null) {
					plannedDate = calculateToday();
				} else {
					plannedDate = new Date(minPlanungBeginn.getTime());
				}
			} else {
				plannedDate = nextPlaningDate;
			}
			Integer versatzMS = (int) (allReadyPlanedHours.doubleValue() * 60 * 60 * 1000);
			changePlanningOrder(planung2Move, reihe, plannedDate, versatzMS, clientDto);
		}

		removeLockMaschineForPlanningSuggestion(planung2Move.getMaschineIId(), clientDto);
		em.flush();
	}

	@Override
	public void movePlanningOrder(
			Integer iKey, int moveAction,
			TheClientDto clientDto
	) throws EJBExceptionLP {
		checkPermissions(RECHT_FERT_LOS_CUD, clientDto);
		LosPlanungVorschlag planung2Move = em.find(LosPlanungVorschlag.class, iKey);

		Integer newTargetReihe = null;
		if (moveAction == MOVE_PLANNING_UP) {
			newTargetReihe = Math.max(1, planung2Move.getiReihung() - 1);
		} else if (moveAction == MOVE_PLANNING_DOWN) {
			newTargetReihe = planung2Move.getiReihung() + 1;
		}
		movePlanningOrderToReihen(iKey, newTargetReihe, clientDto);
	}

	@Override
	@TransactionTimeout(3600)
	public void saveLosTermine(Integer iMaschineID, TheClientDto clientDto) {
		checkPermissions(RECHT_FERT_LOS_CUD, clientDto);
		boolean bAutomatischeErmittlungLosEnde = false;
		Integer iAgBeginnAutomatischErmittelnInStd = 0;
		boolean bReihenfolgenplanung = false;
		try {
			ParametermandantDto parameter = getParameterFac().getMandantparameter(clientDto.getMandant(),
					ParameterFac.KATEGORIE_FERTIGUNG, ParameterFac.PARAMETER_AUTOMATISCHE_ERMITTLUNG_LOS_ENDE);
			bAutomatischeErmittlungLosEnde = (Boolean) parameter.getCWertAsObject();

			parameter = getParameterFac().getMandantparameter(clientDto.getMandant(),
					ParameterFac.KATEGORIE_FERTIGUNG, ParameterFac.PARAMETER_AUTOMATISCHE_ERMITTLUNG_AG_BEGINN);
			iAgBeginnAutomatischErmittelnInStd = ((Integer) parameter.getCWertAsObject());

			parameter = getParameterFac().getMandantparameter(clientDto.getMandant(),
					ParameterFac.KATEGORIE_FERTIGUNG, ParameterFac.PARAMETER_REIHENFOLGENPLANUNG);
			bReihenfolgenplanung = (Boolean) parameter.getCWertAsObject();

		} catch (RemoteException e) {
			throwEJBExceptionLPRespectOld(e);
		}

		List<Integer> maschineIds;
		if (iMaschineID == null) {
			HvTypedQuery<Integer> machineIDQuery = new HvTypedQuery<>(em.createNamedQuery("LosPlanungsVorschlagMaschineIDs"));
            maschineIds = new ArrayList<>(machineIDQuery.getResultList());
		} else {
			maschineIds = Collections.singletonList(iMaschineID);
		}

		for (Integer maschineID: maschineIds) {
			lockMaschineForPlanningSuggestion(maschineID, clientDto);
		}

		Query query = em.createNamedQuery("LosPlanungVorschlagForSavePlanningDatesFindByMaschineOrderdByReihung");
		query.setParameter("maschineIds", maschineIds);

		for (Object o : query.getResultList()) {
			Object[] results = (Object[])o;
			LosPlanungVorschlag planung = (LosPlanungVorschlag)results[0];
			Lossollarbeitsplan ag = (Lossollarbeitsplan)results[1];
			Los los = (Los)results[2];
			Integer maschinenversatztage = ag.getIMaschinenversatztage();
			if (maschinenversatztage == null) {
				maschinenversatztage = 0;
			}
			Date losBeginn = Helper.addiereTageZuDatum(planung.gettPlanBeginn(), maschinenversatztage * -1);
			int delta = Helper.getDifferenzInTagen(los.getTProduktionsbeginn(), losBeginn);
			Date losEnde = Helper.addiereTageZuDatum(los.getTProduktionsende(), delta);
			if (losEnde != los.getTProduktionsende() || losBeginn != los.getTProduktionsbeginn()) {
				getFertigungFac().terminVerschieben(
						los, new Timestamp(losBeginn.getTime()), new Timestamp(losEnde.getTime()), los.getTMaterialVollstaendigBis(),
						bAutomatischeErmittlungLosEnde, iAgBeginnAutomatischErmittelnInStd,
						bReihenfolgenplanung, clientDto
				);
			}
			if (ag.getMaschineIId().intValue() != planung.getMaschineIId().intValue() ||
				!Objects.equals(ag.getLRuestzeit(), planung.getlRuestzeit()) ||
				!Objects.equals(ag.getLStueckzeit(), planung.getlStueckzeit()) ||
				!Objects.equals(ag.getIMaschinenversatzMs(), planung.getiMaschinenversatzMs())
			) {
				// Save the Planned Data to the AG.
				ag.setMaschineIId(planung.getMaschineIId());

				ag.setLRuestzeit(planung.getlRuestzeit());
				ag.setLStueckzeit(planung.getlStueckzeit());
				BigDecimal gesamtZeit = Helper.berechneGesamtzeitInStunden(
						planung.getlRuestzeit(), planung.getlStueckzeit(),
						los.getNLosgroesse(), BigDecimal.ONE, ag.getIAufspannung()
				);
				ag.setNGesamtzeit(gesamtZeit);

				ag.setIMaschinenversatzMs(planung.getiMaschinenversatzMs());
				ag.setPersonalIIdAendern(clientDto.getIDPersonal());
				ag.setTAendern(new Timestamp(System.currentTimeMillis()));
				em.merge(ag);
			}

			if (!Objects.equals(los.getXText(), planung.getcLosText())) {
				// Save the Text to the Los.
				los.setXText(planung.getcLosText());
				los.setPersonalIIdAendern(clientDto.getIDPersonal());
				los.setTAendern(new Timestamp(System.currentTimeMillis()));
				em.merge(los);
			}
		}

		em.createNamedQuery("LosPlanungVorschlagDeleteByMaschine").setParameter(
				"maschineIds", maschineIds
		).executeUpdate();

		for (Integer maschineID: maschineIds) {
			removeLockMaschineForPlanningSuggestion(maschineID, clientDto);
		}

		em.flush();
	}

	@Override
	public void changeLosPlanungVorschlag(Integer iKey, Integer newMaschineID, Integer newReihung,
										  Long newRuestzeit, Long newStueckzeit,  String cLosText, TheClientDto clientDto) {

		checkPermissions(RECHT_FERT_LOS_CUD, clientDto);
		LosPlanungVorschlag planung2Move = em.find(LosPlanungVorschlag.class, iKey);
		Integer oldMachineID = planung2Move.getMaschineIId();
		boolean maschineChanged = !newMaschineID.equals(oldMachineID);
		boolean workingTimeChanged = !newRuestzeit.equals(planung2Move.getlRuestzeit()) || !newStueckzeit.equals(planung2Move.getlStueckzeit());
		boolean textChanged = !Objects.equals(cLosText, planung2Move.getcLosText());
		if (maschineChanged) {
			lockMaschineForPlanningSuggestion(oldMachineID, clientDto);
			lockMaschineForPlanningSuggestion(newMaschineID, clientDto);

			// Maschine is changed so it must recalculate all plannings of the old and new maschine.
			Date minPlanungBeginn;
			try {
				// Get the Date of the first Planungsvorschlag or use the today date.
				Query minPlanungBeginquery = em.createNamedQuery("LosPlanungVorschlagMinBegin");
				minPlanungBeginn = (Timestamp) minPlanungBeginquery.getSingleResult();
			} catch (NoResultException e) {
				minPlanungBeginn = null;
			}

			// Change the Maschine-ID & Reihung
			planung2Move.setiReihung(newReihung);
			planung2Move.setMaschineIId(newMaschineID);
			planung2Move.setlRuestzeit(newRuestzeit);
			planung2Move.setlStueckzeit(newStueckzeit);
			planung2Move.setcLosText(cLosText);
			planung2Move.setPersonalIIdAendern(clientDto.getIDPersonal());
			planung2Move.setTAendern(new Timestamp(System.currentTimeMillis()));
			em.merge(planung2Move);
			em.flush();
			// Reorder the Planning of the old and new maschine
			reorderPlanningForMachine(oldMachineID, minPlanungBeginn, clientDto);
			reorderPlanningForMachine(newMaschineID, minPlanungBeginn, clientDto);
			removeLockMaschineForPlanningSuggestion(oldMachineID, clientDto);
			removeLockMaschineForPlanningSuggestion(newMaschineID, clientDto);
		} else if (!newReihung.equals(planung2Move.getiReihung()) ||
				   workingTimeChanged || textChanged) {

			if (workingTimeChanged) {
				planung2Move.setlRuestzeit(newRuestzeit);
				planung2Move.setlStueckzeit(newStueckzeit);
			}
			if (textChanged) {
				planung2Move.setcLosText(cLosText);
			}

			boolean persistPlaning = workingTimeChanged || textChanged;
			if (persistPlaning) {
				planung2Move.setPersonalIIdAendern(clientDto.getIDPersonal());
				planung2Move.setTAendern(new Timestamp(System.currentTimeMillis()));
				em.merge(planung2Move);
				em.flush();
			}

			// Reordering all Plannings for the Maschine.
			movePlanningOrderToReihen(iKey, newReihung, clientDto);
		}
	}

	@Override
	public Integer[] getAllPlannedMaschineIDs() {
		HvTypedQuery<Integer> machineIDQuery = new HvTypedQuery<>(em.createNamedQuery("LosPlanungsVorschlagMaschineIDs"));
		List<Integer> maschineIds = new ArrayList<>(machineIDQuery.getResultList());
		Integer[] tmpArray = new Integer[maschineIds.size()];
		return maschineIds.toArray(tmpArray);
	}

	@Override
	public void deletePlanningSuggestion(Integer iKey, TheClientDto clientDto) {
		if (iKey != null) {
			checkPermissions(RECHT_FERT_LOS_CUD, clientDto);
			LosPlanungVorschlag toRemove = em.find(LosPlanungVorschlag.class, iKey);
			if (toRemove == null) {
				throw new EJBExceptionLP(EJBExceptionLP.FEHLER_BEI_FINDBYPRIMARYKEY, "");
			}
			Integer maschineID = toRemove.getMaschineIId();
			try {
				lockMaschineForPlanningSuggestion(maschineID, clientDto);
				em.remove(toRemove);
				reorderPlanningForMachine(maschineID, null, clientDto);
				em.flush();
			} catch (EntityExistsException er) {
				throw new EJBExceptionLP(EJBExceptionLP.FEHLER_BEIM_LOESCHEN, er);
			} finally {
				removeLockMaschineForPlanningSuggestion(maschineID, clientDto);
			}
		}
	}

	@Override
	public void deleteAllPlanningSuggestions(TheClientDto clientDto) {
		HvTypedQuery<Integer> machineIDQuery = new HvTypedQuery<>(em.createNamedQuery("LosPlanungsVorschlagMaschineIDs"));
		List<Integer> maschineIds = new ArrayList<>(machineIDQuery.getResultList());
		if (!maschineIds.isEmpty()) {
			checkPermissions(RECHT_FERT_LOS_CUD, clientDto);
			for (Integer maschineID: maschineIds) {
				lockMaschineForPlanningSuggestion(maschineID, clientDto);
			}
			em.createNamedQuery("LosPlanungVorschlagDeleteByMaschine").setParameter(
					"maschineIds", maschineIds
			).executeUpdate();

			for (Integer maschineID: maschineIds) {
				removeLockMaschineForPlanningSuggestion(maschineID, clientDto);
			}
		}
	}
}
