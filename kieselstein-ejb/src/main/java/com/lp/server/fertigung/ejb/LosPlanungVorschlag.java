/*******************************************************************************
 * kieselsteinERP, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2023 - 2024 Kieselstein ERP eG, X-Net Services GmbH
 * based on: HELIUM V, Copyright (C) 2004 - 2022 HELIUM V IT-Solutions GmbH
 * <p>
 * distributed under the terms of GNU Affero General Public License, version 3
 * with supplements. See LICENSE file in base source directory for details.
 * <p>
 * Contact: help@kieselstein-erp.org
 ******************************************************************************/
package com.lp.server.fertigung.ejb;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

@NamedQueries({
	@NamedQuery(name = "LosPlanungVorschlagDeleteByMaschine",
		query = "DELETE FROM LosPlanungVorschlag o WHERE o.maschineIId IN (:maschineIds) "
	),
	@NamedQuery(name = "LosPlanungVorschlagForSavePlanningDatesFindByMaschineOrderdByPlanBeginn",
			query = "SELECT lpv, lsap, los FROM LosPlanungVorschlag lpv " +
					"LEFT JOIN lpv.lossollarbeitsplan lsap " +
					"LEFT JOIN lsap.los los " +
					"WHERE lpv.maschineIId IN (:maschineIds) " +
					"ORDER BY lpv.tPlanBeginn, COALESCE(lpv.iReihung, 99999999999), lpv.maschineIId"
	),
	@NamedQuery(name = "LosPlanungVorschlagForSavePlanningDatesFindByMaschineOrderdByReihung",
			query = "SELECT lpv, lsap, los FROM LosPlanungVorschlag lpv " +
					"LEFT JOIN lpv.lossollarbeitsplan lsap " +
					"LEFT JOIN lsap.los los " +
					"WHERE lpv.maschineIId IN (:maschineIds) " +
					"ORDER BY COALESCE(lpv.iReihung, 99999999999), lpv.tPlanBeginn, lpv.maschineIId"
	),
	@NamedQuery(name = "LosPlanungVorschlagMinBegin",
			query = "SELECT min(lpv.tPlanBeginn) FROM LosPlanungVorschlag lpv"
	),
	@NamedQuery(name = "LosPlanungsVorschlagMaschineIDs",
			query = "SELECT DISTINCT lpv.maschineIId FROM LosPlanungVorschlag lpv"
	)
})
@Entity
@Table(name = "FERT_LOSPLANUNGVORSCHLAG")
public class LosPlanungVorschlag implements Serializable {
	@Id
	@Column(name = "I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer iId;

	@Column(name = "LOSSOLLARBEITSPLAN_I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer losSollArbeitsplanIId;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "LOSSOLLARBEITSPLAN_I_ID", referencedColumnName = "I_ID", insertable = false, updatable = false)
	private Lossollarbeitsplan lossollarbeitsplan;

	@Column(name = "MASCHINE_I_ID", columnDefinition = "INTEGER")
	private Integer maschineIId;

	@Column(name = "L_RUESTZEIT", columnDefinition = "BIGINT NOT NULL")
	private Long lRuestzeit;

	@Column(name = "L_STUECKZEIT", columnDefinition = "BIGINT NOT NULL")
	private Long lStueckzeit;

	@Column(name = "I_REIHUNG", columnDefinition = "INTEGER")
	private Integer iReihung;

	@Column(name = "T_PLAN_BEGINN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tPlanBeginn;

	@Column(name = "PERSONAL_I_ID_ANLEGEN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAnlegen;

	@Column(name = "PERSONAL_I_ID_AENDERN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAendern;

	@Column(name = "T_ANLEGEN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAnlegen;

	@Column(name = "T_AENDERN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAendern;

	@Column(name = "I_MASCHINENVERSATZ_MS", columnDefinition = "INTEGER")
	private Integer iMaschinenversatzMs;

	@Column(name = "C_LOS_TEXT", columnDefinition = "TEXT")
	private String cLosText;

	public LosPlanungVorschlag() {super();}

	public LosPlanungVorschlag(Integer id, Integer losSollArbeitsplanIId, Integer maschineIId,
							   Long lRuestzeit, Long lStueckzeit, Integer iReihung, Timestamp tPlanBeginn,
							   Integer iMaschinenversatzMs, String cLosText, Integer personalIIdAnlegen) {
		this();
		setIId(id);
		setLosSollArbeitsplanIId(losSollArbeitsplanIId);
		setMaschineIId(maschineIId);
		setlRuestzeit(lRuestzeit);
		setlStueckzeit(lStueckzeit);
		setiReihung(iReihung);
		settPlanBeginn(tPlanBeginn);
		setcLosText(cLosText);

		setPersonalIIdAnlegen(personalIIdAnlegen);
		setPersonalIIdAendern(personalIIdAnlegen);
		setiMaschinenversatzMs(iMaschinenversatzMs);
		// Setzen der NOT NULL Felder
		Timestamp now = new Timestamp(System.currentTimeMillis());
		this.setTAendern(now);
		this.setTAnlegen(now);


	}

	public Integer getIId() {
		return this.iId;
	}

	public void setIId(Integer iId) {
		this.iId = iId;
	}

	public Integer getLosSollArbeitsplanIId() {
		return losSollArbeitsplanIId;
	}

	public void setLosSollArbeitsplanIId(Integer losSollArbeitsplanIId) {
		this.losSollArbeitsplanIId = losSollArbeitsplanIId;
	}

	public Lossollarbeitsplan getLossollarbeitsplan() {
		return lossollarbeitsplan;
	}

	public Integer getMaschineIId() {
		return maschineIId;
	}

	public void setMaschineIId(Integer maschineIId) {
		this.maschineIId = maschineIId;
	}

	public Long getlRuestzeit() {
		return lRuestzeit;
	}

	public void setlRuestzeit(Long lRuestzeit) {
		this.lRuestzeit = lRuestzeit;
	}

	public Long getlStueckzeit() {
		return lStueckzeit;
	}

	public void setlStueckzeit(Long lStueckzeit) {
		this.lStueckzeit = lStueckzeit;
	}

	public Integer getiReihung() {
		return iReihung;
	}

	public void setiReihung(Integer iReihung) {
		this.iReihung = iReihung;
	}

	public Timestamp gettPlanBeginn() {
		return tPlanBeginn;
	}

	public void settPlanBeginn(Timestamp tPlanBeginn) {
		this.tPlanBeginn = tPlanBeginn;
	}

	public Integer getPersonalIIdAnlegen() {
		return personalIIdAnlegen;
	}

	public void setPersonalIIdAnlegen(Integer personalIIdAnlegen) {
		this.personalIIdAnlegen = personalIIdAnlegen;
	}

	public Integer getPersonalIIdAendern() {
		return personalIIdAendern;
	}

	public void setPersonalIIdAendern(Integer personalIIdAendern) {
		this.personalIIdAendern = personalIIdAendern;
	}

	public Timestamp getTAnlegen() {
		return tAnlegen;
	}

	public void setTAnlegen(Timestamp tAnlegen) {
		this.tAnlegen = tAnlegen;
	}

	public Timestamp getTAendern() {
		return tAendern;
	}

	public void setTAendern(Timestamp tAendern) {
		this.tAendern = tAendern;
	}

	public Integer getiMaschinenversatzMs() {
		return iMaschinenversatzMs;
	}

	public void setiMaschinenversatzMs(Integer iMaschinenversatzMs) {
		this.iMaschinenversatzMs = iMaschinenversatzMs;
	}

	public String getcLosText() {
		return cLosText;
	}

	public void setcLosText(String cLosText) {
		this.cLosText = cLosText;
	}

}
