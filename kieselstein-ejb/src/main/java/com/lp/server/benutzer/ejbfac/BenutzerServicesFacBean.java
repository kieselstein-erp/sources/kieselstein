/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 * 
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 * 
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: developers@heliumv.com
 ******************************************************************************/
package com.lp.server.benutzer.ejbfac;

import java.rmi.RemoteException;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.lp.server.benutzer.fastlanereader.generated.FLRRollerecht;
import com.lp.server.benutzer.service.BenutzerDto;
import com.lp.server.benutzer.service.BenutzerServicesFac;
import com.lp.server.benutzer.service.BenutzermandantsystemrolleDto;
import com.lp.server.system.ejb.ParametermandantPK;
import com.lp.server.system.ejb.Text;
import com.lp.server.system.ejbfac.ParameterFacBean;
import com.lp.server.system.fastlanereader.generated.FLRArbeitsplatzparameter;
import com.lp.server.system.service.AnwenderDto;
import com.lp.server.system.service.ArbeitsplatzparameterDto;
import com.lp.server.system.service.ParameterFac;
import com.lp.server.system.service.ParametermandantDto;
import com.lp.server.system.service.SystemFac;
import com.lp.server.system.service.TheClientDto;
import com.lp.server.util.Facade;
import com.lp.server.util.HelperServer;
import com.lp.server.util.HvOptional;
import com.lp.server.util.fastlanereader.FLRSessionFactory;
import com.lp.util.EJBExceptionLP;
import com.lp.util.Helper;

@Singleton
@Local(BenutzerServicesFacLocal.class)
public class BenutzerServicesFacBean extends Facade implements BenutzerServicesFac, BenutzerServicesFacLocal {
	public SessionFactory getSessionFactory() {

		return factory;
	}

	@PersistenceContext(unitName = "ejb")
	private EntityManager em;

	@PersistenceUnit
	private SessionFactory factory;

	private HashMap<Integer, Set<String>> hmRolleRechte = null;
	private HashMap<String, HashMap<String, ArbeitsplatzparameterDto>> workplaceParameters = null;
	private HashMap<String, ArbeitsplatzparameterDto> defaultWorkplaceParameters = null;

	private HashMap<String, HashMap<String, HashMap<String, String>>> hmLPText = null;
	private ParameterMandantCachingProvider cacheParametermandant = new ParameterMandantCachingProvider();

	private final Set<String> hsParameterMandantZeitabhaenigCNr = createParameterMandantZeitabhaenigCNr();

	public synchronized void reloadRolleRechte() {
		boolean subscriptionAbgelaufen = false;
		try {
			AnwenderDto anwenderDto = getSystemFac().anwenderFindByPrimaryKey(SystemFac.PK_HAUPTMANDANT_IN_LP_ANWENDER);
			subscriptionAbgelaufen = anwenderDto.getTSubscription() == null ? false
					: anwenderDto.getTSubscription().before(getTimestamp());
		} catch (RemoteException | EJBExceptionLP e) {
			myLogger.error("", e);
		}

        hmRolleRechte = new HashMap<>();
		Session session = getNewSession();

		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<FLRRollerecht> cq = builder.createQuery(FLRRollerecht.class);
		cq.from(FLRRollerecht.class);
		List<FLRRollerecht> resultList = session.createQuery(cq).getResultList();

		for (FLRRollerecht rollerecht : resultList) {
			Integer systemrolleIId = rollerecht.getFlrsystemrolle().getI_id();
			String rechtCNr = rollerecht.getFlrrecht().getC_nr().trim();

			if (subscriptionAbgelaufen && rechtCNr.endsWith("CUD"))
				rechtCNr = rechtCNr.substring(0, rechtCNr.lastIndexOf("CUD")) + "R";

			Set<String> hmSystemrolle;

			if (hmRolleRechte.containsKey(systemrolleIId)) {
				hmSystemrolle = hmRolleRechte.get(systemrolleIId);
			} else {
				hmSystemrolle = new HashSet<>();
			}

			hmSystemrolle.add(rechtCNr);
			hmRolleRechte.put(systemrolleIId, hmSystemrolle);
		}

	}

	public void reloadArbeitsplatzparameter() {
		workplaceParameters = new HashMap<>();

		defaultWorkplaceParameters = new HashMap<>();

		Session session = getNewSession();

		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<FLRArbeitsplatzparameter> cq = builder.createQuery(FLRArbeitsplatzparameter.class);
		cq.from(FLRArbeitsplatzparameter.class);
		List<FLRArbeitsplatzparameter> resultList = session.createQuery(cq).getResultList();

		for (FLRArbeitsplatzparameter flrArbeitsplatzparameter : resultList) {
			String arbeitsplatz = flrArbeitsplatzparameter.getFlrarbeitsplatz().getC_pcname().toLowerCase();
			boolean is_default = flrArbeitsplatzparameter.getFlrarbeitsplatz().getB_default();

			if (!workplaceParameters.containsKey(arbeitsplatz)) {
				workplaceParameters.put(arbeitsplatz, new HashMap<>());
			}

			ArbeitsplatzparameterDto apDto = new ArbeitsplatzparameterDto();

			apDto.setArbeitsplatzIId(flrArbeitsplatzparameter.getI_id());
			apDto.setCWert(flrArbeitsplatzparameter.getC_wert());
			apDto.setParameterCNr(flrArbeitsplatzparameter.getFlrparameter().getC_nr());

			if (is_default) {
				defaultWorkplaceParameters.put(apDto.getParameterCNr(), apDto);
			} else {
				workplaceParameters.get(arbeitsplatz).put(apDto.getParameterCNr(), apDto);
			}
		}
	}

	public ArbeitsplatzparameterDto holeArbeitsplatzparameter(String cPcname, String parameterCNr) {
		if (workplaceParameters == null || defaultWorkplaceParameters == null) {
			reloadArbeitsplatzparameter();
		}

		if (workplaceParameters.containsKey(cPcname.toLowerCase())) {
			ArbeitsplatzparameterDto dto = workplaceParameters.get(cPcname.toLowerCase()).get(parameterCNr);
			if (dto != null) {
				return dto;
			}
		}

		ArbeitsplatzparameterDto defaultParameter = defaultWorkplaceParameters.get(parameterCNr);
		if (defaultParameter != null) {
			return defaultParameter;
		}

		return null;
	}

	public void reloadUebersteuertenText() {

		hmLPText = new HashMap<String, HashMap<String, HashMap<String, String>>>();

		Query query = em.createNamedQuery("TextfindAll");
		Collection<?> c = query.getResultList();

		Iterator<?> resultListIterator = c.iterator();

		while (resultListIterator.hasNext()) {
			Text text = (Text) resultListIterator.next();

			String mandantCNr = text.getPk().getMandantCNr();

			HashMap<String, HashMap<String, String>> hmMandant = null;

			if (hmLPText.containsKey(mandantCNr)) {
				hmMandant = hmLPText.get(mandantCNr);

			} else {

				hmMandant = new HashMap<String, HashMap<String, String>>();

			}

			HashMap<String, String> hmLocale = null;

			String localeCNr = text.getPk().getLocaleCNr();
			if (hmMandant.containsKey(localeCNr)) {
				hmLocale = hmMandant.get(localeCNr);
			} else {

				hmLocale = new HashMap<String, String>();
			}

			hmLocale.put(text.getPk().getCToken(), text.getCText());

			hmMandant.put(localeCNr, hmLocale);
			hmLPText.put(mandantCNr, hmMandant);

		}

	}

	public String getTextRespectUISpr(String sTokenI, String mandantCNr, Locale loI) {

		if (hmLPText == null) {
			reloadUebersteuertenText();
		}

		HashMap<String, HashMap<String, String>> hmMandant = hmLPText.get(mandantCNr);

		String locale = Helper.locale2String(loI);

		if (hmMandant != null && hmMandant.containsKey(locale)) {

			HashMap<String, String> hmLocale = hmMandant.get(locale);

			if (hmLocale != null && hmLocale.containsKey(sTokenI)) {
				return hmLocale.get(sTokenI);
			}

		}

		return HelperServer.getTextRespectUISpr(sTokenI, loI);

	}

	public String getTextRespectUISpr(String sTokenI, String mandantCNr, Locale loI, Object... replacements) {
		String msg = getTextRespectUISpr(sTokenI, mandantCNr, loI);
		return MessageFormat.format(msg, replacements);
	}

	public boolean hatRecht(String rechtCNr, TheClientDto theClientDto) {

		if (hmRolleRechte == null) {
			reloadRolleRechte();
		}
		Integer systemrolleIId = theClientDto.getSystemrolleIId();
		Set<String> hmRechte = hmRolleRechte.get(systemrolleIId);

		if (hmRechte == null) {
			return false;
		}

        return hmRechte.contains(rechtCNr.trim());
	}

	public boolean hatRechtInZielmandant(String rechtCNr, String mandantCNrZiel, TheClientDto theClientDto) {

		String benutzername = theClientDto.getBenutzername().trim().substring(0,
				theClientDto.getBenutzername().indexOf("|"));

		BenutzerDto benutzerDto = getBenutzerFac().benutzerFindByCBenutzerkennungOhneExc(benutzername);

		if (benutzerDto != null && benutzerDto.getIId() != null) {
			BenutzermandantsystemrolleDto benutzermandantsystemrolleDto = getBenutzerFac()
					.benutzermandantsystemrolleFindByBenutzerIIdMandantCNrOhneExc(benutzerDto.getIId(), mandantCNrZiel);

			if (benutzermandantsystemrolleDto != null) {

				Session session = FLRSessionFactory.getFactory().openSession();
				org.hibernate.Criteria crit = session.createCriteria(FLRRollerecht.class);

				crit.createAlias("flrsystemrolle", "s")
						.add(Restrictions.eq("s.i_id", benutzermandantsystemrolleDto.getSystemrolleIId()));

				crit.createAlias("flrrecht", "r").add(Restrictions.eq("r.c_nr", rechtCNr));

				List<?> results = crit.list();
				Iterator<?> resultListIterator = results.iterator();

				if (resultListIterator.hasNext()) {
					return true;
				} else {
					return false;
				}

			}

		}

		return false;

	}

	public ParametermandantDto getMandantparameter(String mandant_c_nr, String cKategorieI,
			String mandantparameter_c_nr) {
		return getMandantparameter(mandant_c_nr, cKategorieI, mandantparameter_c_nr, null);
	}

	private ParametermandantDto setzeWertZumZeitpunkt(ParametermandantDto parametermandantDto,
			java.sql.Timestamp tZeitpunkt) {

		if (tZeitpunkt != null) {

			ParametermandantDto pmDtoClone = ParametermandantDto.clone(parametermandantDto);

			pmDtoClone.setCWert(pmDtoClone.getCWertZumZeitpunkt(tZeitpunkt));
			return pmDtoClone;

		} else {
			return parametermandantDto;
		}

	}

	private HvOptional<ParametermandantDto> getParametermandantFromMap(String mandant_c_nr,
			String mandantparameter_c_nr, String cKategorieI) {
		ParametermandantPK pk = new ParametermandantPK(mandantparameter_c_nr, mandant_c_nr, cKategorieI);
		return cacheParametermandant.get(pk);

	}

	public ParametermandantDto getMandantparameter(String mandant_c_nr, String cKategorieI,
			String mandantparameter_c_nr, java.sql.Timestamp tZeitpunkt) {

		HvOptional<ParametermandantDto> parametermandantDto = getParametermandantFromMap(mandant_c_nr,
				mandantparameter_c_nr, cKategorieI);
		if (parametermandantDto.isPresent()) {
			return setzeWertZumZeitpunkt(parametermandantDto.get(), tZeitpunkt);
		}

		// Wenn nich gefunden, dann beim Hauptmandant suchen
		try {
			AnwenderDto anwenderDto = getSystemFac()
					.anwenderFindByPrimaryKey(new Integer(SystemFac.PK_HAUPTMANDANT_IN_LP_ANWENDER));

			parametermandantDto = getParametermandantFromMap(anwenderDto.getMandantCNrHauptmandant(),
					mandantparameter_c_nr, cKategorieI);

			if (parametermandantDto.isPresent()) {
				return setzeWertZumZeitpunkt(parametermandantDto.get(), tZeitpunkt);
			}

		} catch (RemoteException e) {
			throwEJBExceptionLPRespectOld(e);
		}

		// Parameter auch nicht als Hauptparameter verfuegbar

		String[][] progMandantParameter = ParameterFacBean.progMandantParameter;
		ParametermandantDto parametermandant = null;
		for (int i = 0; i < progMandantParameter.length; i++) {
			if (cKategorieI.equals(progMandantParameter[i][0])
					&& mandantparameter_c_nr.equals(progMandantParameter[i][1])) {
				parametermandant = new ParametermandantDto();
				parametermandant.setCDatentyp(progMandantParameter[i][3]);
				parametermandant.setCNr(mandantparameter_c_nr);
				parametermandant.setCKategorie(cKategorieI);
				parametermandant.setCWert(progMandantParameter[i][2]);
				parametermandant.setCBemerkungsmall(progMandantParameter[i][4]);
				parametermandant.setCBemerkunglarge(progMandantParameter[i][5]);
				break;
			}
		}
		if (parametermandant == null) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER_BEI_FIND, new Exception(
					"Mandantparameter " + cKategorieI + "." + mandantparameter_c_nr + " kann nicht gefunden werden !"));
		}

		return parametermandant;

	}

	public Set<String> getMandantparameterZeitabhaenigCNr() {
		return hsParameterMandantZeitabhaenigCNr;
	}

	@Override
	public boolean hatRechtOder(TheClientDto theClientDto, String[] rechtCnrs) {
		for (String rechtCnr : rechtCnrs) {
			if (hatRecht(rechtCnr, theClientDto)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean hatRechtUnd(TheClientDto theClientDto, String[] rechtCnrs) {
		for (String rechtCnr : rechtCnrs) {
			if (!hatRecht(rechtCnr, theClientDto)) {
				return false;
			}
		}
		return true;
	}

	public void markMandantparameterModified(ParametermandantPK pk) {
		cacheParametermandant.invalidate(pk);
	}

	@Override
	public void markAllMandantenparameterModified() {
		cacheParametermandant.invalidateAll();
	}

	private Set<String> createParameterMandantZeitabhaenigCNr() {
		Set<String> parametermandantZeitabhaenigCNr = new HashSet<String>();
		parametermandantZeitabhaenigCNr.add(ParameterFac.MATERIALGEMEINKOSTENFAKTOR);
		parametermandantZeitabhaenigCNr.add(ParameterFac.PARAMETER_GEMEINKOSTENFAKTOR);
		parametermandantZeitabhaenigCNr.add(ParameterFac.PARAMETER_VORSTEUER_BEI_WE_EINSTANDSPREIS);
		parametermandantZeitabhaenigCNr.add(ParameterFac.ARBEITSZEITGEMEINKOSTENFAKTOR);
		parametermandantZeitabhaenigCNr.add(ParameterFac.FERTIGUNGSGEMEINKOSTENFAKTOR);
		parametermandantZeitabhaenigCNr.add(ParameterFac.ENTWICKLUNGSGEMEINKOSTENFAKTOR);
		parametermandantZeitabhaenigCNr.add(ParameterFac.VERWALTUNGSGEMEINKOSTENFAKTOR);
		parametermandantZeitabhaenigCNr.add(ParameterFac.VERTRIEBSGEMEINKOSTENFAKTOR);
		parametermandantZeitabhaenigCNr.add(ParameterFac.PARAMETER_AUFSCHLAG1);
		parametermandantZeitabhaenigCNr.add(ParameterFac.PARAMETER_AUFSCHLAG2);
		parametermandantZeitabhaenigCNr.add(ParameterFac.PARAMETER_BELEGNUMMERNFORMAT_HISTORISCH);
		return Collections.unmodifiableSet(parametermandantZeitabhaenigCNr);
	}

	private class ParameterMandantCachingProvider {

		private Map<ParametermandantPK, ParametermandantDto> cache = new ConcurrentHashMap<ParametermandantPK, ParametermandantDto>();

		public HvOptional<ParametermandantDto> get(ParametermandantPK key) {
			if (cache.containsKey(key))
				return HvOptional.of(cache.get(key));
			HvOptional<ParametermandantDto> newVal = create(key);
			if (newVal.isPresent()) {
				cache.put(key, newVal.get());
			}
			return newVal;
		}

		public void invalidate(ParametermandantPK key) {
			cache.remove(key);
		}

		public void invalidateAll() {
			cache.clear();
		}

		private HvOptional<ParametermandantDto> create(ParametermandantPK key) {
			try {
				HvOptional<ParametermandantDto> optional = getParameterFac()
						.parametermandantFindByPrimaryKeyOptional(key);
				if (optional.isPresent()) {
					ParametermandantDto dto = optional.get();
					if (BenutzerServicesFacBean.this.getMandantparameterZeitabhaenigCNr().contains(dto.getCNr())) {
						dto.setTmWerteGueltigab(getParameterFac().parametermandantgueltigabGetWerteZumZeitpunkt(
								dto.getMandantCMandant(), dto.getCNr(), dto.getCKategorie()));
					}
					return HvOptional.of(dto);
				}
				return optional;
			} catch (RemoteException e) {
				throw new EJBExceptionLP(e);
			}

		}
	}
}