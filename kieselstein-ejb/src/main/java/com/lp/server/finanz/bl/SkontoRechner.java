package com.lp.server.finanz.bl;

import com.lp.server.finanz.service.FinanzFac;
import com.lp.server.rechnung.service.SkontoDto;
import com.lp.server.system.service.ZahlungszielDto;
import com.lp.util.Helper;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SkontoRechner {

    public SkontoDto berechneSkontiertenOffenenBetrag(Date belegDatum, BigDecimal gesamtBetrag, BigDecimal offenerBetrag, ZahlungszielDto zahlungszielDto) {
        if (zahlungszielDto == null) {
            return null;
        }

        return berechneLetztAktuellesSkonto(belegDatum, gesamtBetrag, offenerBetrag, zahlungszielDto);
    }

    private SkontoDto berechneLetztAktuellesSkonto(Date belegDatum, BigDecimal gesamtBetrag, BigDecimal offenerBetrag, ZahlungszielDto zahlungszielDto) {
        var skontos = Stream.of(berechneSkonto(belegDatum, gesamtBetrag, offenerBetrag, zahlungszielDto.getSkontoAnzahlTage1(), zahlungszielDto.getSkontoProzentsatz1()),
                        berechneSkonto(belegDatum, gesamtBetrag, offenerBetrag, zahlungszielDto.getSkontoAnzahlTage2(), zahlungszielDto.getSkontoProzentsatz2()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .sorted(Comparator.comparing(SkontoDto::getSkontoDatum))
                .collect(Collectors.toList());

        Date now = new Date(System.currentTimeMillis());

        //Finde das aktuell gültige Skonto oder retourniere das letzte
        return skontos.stream()
                .filter(skonto -> !skonto.getSkontoDatum().before(now))
                .findFirst()
                .or(() -> skontos.stream().reduce((o1, o2) -> o2))
                .orElse(null);

    }

    private Optional<SkontoDto> berechneSkonto(Date belegDatum, BigDecimal gesamtBetrag, BigDecimal offenerBetrag,
                                                      Integer skontoTage, BigDecimal skontoProzentsatz) {
        if (belegDatum == null || gesamtBetrag == null || skontoTage == null || skontoProzentsatz == null) {
            return Optional.empty();
        }

        var skontoDatum = Helper.addiereTageZuDatum(belegDatum, skontoTage);
        var skonto = berechneSkontoWert(gesamtBetrag, skontoProzentsatz);

        return Optional.of(new SkontoDto(
                skontoDatum,
                Objects.requireNonNullElse(offenerBetrag, gesamtBetrag).subtract(skonto)
        ));
    }

    public BigDecimal berechneSkontoWert(BigDecimal wert, BigDecimal prozent) {
        if (prozent == null)
            return wert;
        return Helper.getProzentWert(wert, prozent, FinanzFac.NACHKOMMASTELLEN);
    }

    public BigDecimal berechneSkontiertenWert(BigDecimal wert, BigDecimal prozent) {
        if (prozent == null || prozent.compareTo(new BigDecimal("0")) == 0)
            return wert;
        return Helper.getWertPlusProzent(wert, prozent, FinanzFac.NACHKOMMASTELLEN);
    }
}
