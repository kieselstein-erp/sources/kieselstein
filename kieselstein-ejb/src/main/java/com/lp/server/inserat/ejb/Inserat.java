/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 * 
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 * 
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: developers@heliumv.com
 ******************************************************************************/
package com.lp.server.inserat.ejb;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.lp.server.system.service.ITablenames;

@NamedQueries({
		@NamedQuery(name = "InseratfindByMandantCNrCnr", query = "SELECT OBJECT (o) FROM Inserat o WHERE o.mandantCNr=?1 AND o.cNr=?2"),
		@NamedQuery(name = "InseratfindByLieferantIId", query = "SELECT OBJECT (o) FROM Inserat o WHERE o.lieferantIId=?1"),
		@NamedQuery(name = "InseratfindByAnsprechpartnerIIdLieferant", query = "SELECT OBJECT (o) FROM Inserat o WHERE o.ansprechpartnerIIdLieferant=?1"),
		@NamedQuery(name = "InseratfindByBestellpositionIId", query = "SELECT OBJECT (o) FROM Inserat o WHERE o.bestellpositionIId=?1") })
@Entity
@Table(name = ITablenames.IV_INSERAT)
public class Inserat implements Serializable {
	@Id
	@Column(name = "I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer iId;

	@Column(name = "MANDANT_C_NR", columnDefinition = "VARCHAR(3) NOT NULL")
	private String mandantCNr;

	@Column(name = "C_NR", columnDefinition = "VARCHAR(15) NOT NULL")
	private String cNr;

	@Column(name = "T_BELEGDATUM", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tBelegdatum;

	@Column(name = "F_KD_RABATT", columnDefinition = "DOUBLE PRECISION NOT NULL")
	private Double fKdRabatt;
	@Column(name = "F_KD_ZUSATZRABATT", columnDefinition = "DOUBLE PRECISION NOT NULL")
	private Double fKdZusatzrabatt;
	@Column(name = "F_KD_NACHLASS", columnDefinition = "DOUBLE PRECISION NOT NULL")
	private Double fKdNachlass;

	@Column(name = "C_BEZ", columnDefinition = "VARCHAR(300)")
	private String cBez;

	@Column(name = "C_RUBRIK", columnDefinition = "VARCHAR(40)")
	private String cRubrik;

	@Column(name = "C_RUBRIK2", columnDefinition = "VARCHAR(40)")
	private String cRubrik2;

	@Column(name = "C_STICHWORT", columnDefinition = "VARCHAR(40)")
	private String cStichwort;

	@Column(name = "C_STICHWORT2", columnDefinition = "VARCHAR(40)")
	private String cStichwort2;

	public String getCStichwort() {
		return cStichwort;
	}

	public void setCStichwort(String cStichwort) {
		this.cStichwort = cStichwort;
	}

	public String getCStichwort2() {
		return cStichwort2;
	}

	public void setCStichwort2(String cStichwort2) {
		this.cStichwort2 = cStichwort2;
	}

	@Column(name = "C_MEDIUM", columnDefinition = "VARCHAR(80)")
	private String cMedium;

	@Column(name = "X_ANHANG", columnDefinition = "TEXT")
	private String xAnhang;

	@Column(name = "LIEFERANT_I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer lieferantIId;

	@Column(name = "ANSPRECHPARTNER_I_ID_LIEFERANT", columnDefinition = "INTEGER")
	private Integer ansprechpartnerIIdLieferant;
	@Column(name = "F_LF_RABATT", columnDefinition = "DOUBLE PRECISION NOT NULL")
	private Double fLFRabatt;
	@Column(name = "F_LF_ZUSATZRABATT", columnDefinition = "DOUBLE PRECISION NOT NULL")
	private Double fLfZusatzrabatt;
	@Column(name = "F_LF_NACHLASS", columnDefinition = "DOUBLE PRECISION NOT NULL")
	private Double fLfNachlass;
	@Column(name = "X_ANHANG_LF", columnDefinition = "TEXT")
	private String xAnhangLf;

	@Column(name = "T_TERMIN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tTermin;

	@Column(name = "ARTIKEL_I_ID_INSERATART", columnDefinition = "INTEGER NOT NULL")
	private Integer artikelIIdInseratart;

	@Column(name = "BESTELLPOSITION_I_ID", columnDefinition = "INTEGER")
	private Integer bestellpositionIId;

	public Integer getBestellpositionIId() {
		return bestellpositionIId;
	}

	public void setBestellpositionIId(Integer bestellpositionIId) {
		this.bestellpositionIId = bestellpositionIId;
	}

	@Column(name = "STATUS_C_NR", columnDefinition = "CHAR(15) NOT NULL")
	private String statusCNr;

	@Column(name = "PERSONAL_I_ID_VERTRETER", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdVertreter;

	@Column(name = "N_MENGE", columnDefinition = "NUMERIC(17,6) NOT NULL")
	private BigDecimal nMenge;

	@Column(name = "N_NETTOEINZELPREIS_EK", columnDefinition = "NUMERIC(17,6) NOT NULL")
	private BigDecimal nNettoeinzelpreisEk;

	@Column(name = "N_NETTOEINZELPREIS_VK", columnDefinition = "NUMERIC(17,6) NOT NULL")
	private BigDecimal nNettoeinzelpreisVk;

	@Column(name = "T_ERSCHIENEN", columnDefinition = "TIMESTAMP")
	private Timestamp tErschienen;
	@Column(name = "PERSONAL_I_ID_ERSCHIENEN", columnDefinition = "INTEGER")
	private Integer personalIIdErschienen;
	@Column(name = "T_VERRECHNEN", columnDefinition = "TIMESTAMP")
	private Timestamp tVerrechnen;
	@Column(name = "PERSONAL_I_ID_VERRECHNEN", columnDefinition = "INTEGER")
	private Integer personalIIdVerrechnen;
	
	@Column(name = "T_MANUELLVERRECHNEN", columnDefinition = "TIMESTAMP")
	private Timestamp tManuellverrechnen;
	public Timestamp getTManuellverrechnen() {
		return tManuellverrechnen;
	}

	public void setTManuellverrechnen(Timestamp tManuellverrechnen) {
		this.tManuellverrechnen = tManuellverrechnen;
	}

	public Integer getPersonalIIdManuellverrechnen() {
		return personalIIdManuellverrechnen;
	}

	public void setPersonalIIdManuellverrechnen(Integer personalIIdManuellverrechnen) {
		this.personalIIdManuellverrechnen = personalIIdManuellverrechnen;
	}

	@Column(name = "PERSONAL_I_ID_MANUELLVERRECHNEN", columnDefinition = "INTEGER")
	private Integer personalIIdManuellverrechnen;
	

	@Column(name = "C_GESTOPPT", columnDefinition = "VARCHAR(80)")
	private String cGestoppt;

	@Column(name = "B_DRUCK_BESTELLUNG_LF", columnDefinition = "SMALLINT NOT NULL")
	private Short bDruckBestellungLf;
	@Column(name = "B_DRUCK_BESTELLUNG_KD", columnDefinition = "SMALLINT NOT NULL")
	private Short bDruckBestellungKd;
	@Column(name = "B_DRUCK_RECHNUNG_KD", columnDefinition = "SMALLINT NOT NULL")
	private Short bDruckRechnungKd;
	@Column(name = "T_TERMIN_BIS", columnDefinition = "TIMESTAMP")
	private Timestamp tTerminBis;

	@Column(name = "PERSONAL_I_ID_MANUELLERLEDIGT", columnDefinition = "INTEGER")
	private Integer personalIIdManuellerledigt;
	@Column(name = "T_MANUELLERLEDIGT", columnDefinition = "TIMESTAMP")
	private Timestamp tManuellerledigt;

	@Column(name = "PERSONAL_I_ID_GESTOPPT", columnDefinition = "INTEGER")
	private Integer personalIIdGestoppt;
	@Column(name = "T_GESTOPPT", columnDefinition = "TIMESTAMP")
	private Timestamp tGestoppt;

	
	public Integer getPersonalIIdGestoppt() {
		return personalIIdGestoppt;
	}

	public void setPersonalIIdGestoppt(Integer personalIIdGestoppt) {
		this.personalIIdGestoppt = personalIIdGestoppt;
	}

	public Timestamp getTGestoppt() {
		return tGestoppt;
	}

	public void setTGestoppt(Timestamp tGestoppt) {
		this.tGestoppt = tGestoppt;
	}

	public Timestamp getTManuellerledigt() {
		return this.tManuellerledigt;
	}

	public void setTManuellerledigt(Timestamp tManuellerledigt) {
		this.tManuellerledigt = tManuellerledigt;
	}
	
	public Integer getPersonalIIdManuellerledigt() {
		return this.personalIIdManuellerledigt;
	}

	public void setPersonalIIdManuellerledigt(Integer personalIIdManuellerledigt) {
		this.personalIIdManuellerledigt = personalIIdManuellerledigt;
	}
	public Short getBDruckBestellungLf() {
		return bDruckBestellungLf;
	}

	public void setBDruckBestellungLf(Short bDruckBestellungLf) {
		this.bDruckBestellungLf = bDruckBestellungLf;
	}

	public Short getBDruckBestellungKd() {
		return bDruckBestellungKd;
	}

	public void setBDruckBestellungKd(Short bDruckBestellungKd) {
		this.bDruckBestellungKd = bDruckBestellungKd;
	}

	public Short getBDruckRechnungKd() {
		return bDruckRechnungKd;
	}

	public void setBDruckRechnungKd(Short bDruckRechnungKd) {
		this.bDruckRechnungKd = bDruckRechnungKd;
	}

	public Timestamp getTTerminBis() {
		return tTerminBis;
	}

	public void setTTerminBis(Timestamp tTerminBis) {
		this.tTerminBis = tTerminBis;
	}

	@Column(name = "T_ANLEGEN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAnlegen;

	@Column(name = "T_AENDERN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAendern;

	@Column(name = "PERSONAL_I_ID_AENDERN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAendern;

	@Column(name = "PERSONAL_I_ID_ANLEGEN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAnlegen;

	@Column(name = "B_WERTAUFTEILEN", columnDefinition = "SMALLINT NOT NULL")
	private Short bWertaufteilen;
	
	public Short getBWertaufteilen() {
		return bWertaufteilen;
	}

	public void setBWertaufteilen(Short bWertaufteilen) {
		this.bWertaufteilen = bWertaufteilen;
	}

	private static final long serialVersionUID = 1L;

	public Inserat(Integer id, String nr, String mandantCNr, Double fKdRabatt,
			Double fKdZusatzrabatt, Double fKdNachlass, Integer lieferantIId,
			Timestamp tBelegdatum, Double fLfRabatt, Double fLfZusatzrabatt,
			Double fLfNachlass, Timestamp tTermin, String statusCNr,
			Integer artikelIIdInseratart, Integer personalIIdAnlegen,
			Integer personalIIdAendern, Integer personalIIdVertreter,
			BigDecimal nMenge, BigDecimal nPreisEK, BigDecimal nPReisVK,
			Timestamp tAnlegen, Timestamp tAendern, Short bDruckBestellungLf,
			Short bDruckBestellungKd, Short bDruckRechnungKd,Short bWertaufteilen) {
		setIId(id);
		setCNr(nr);
		setMandantCNr(mandantCNr);
		setTBelegdatum(tBelegdatum);
		setFKdRabatt(fKdRabatt);
		setFKdZusatzrabatt(fKdZusatzrabatt);
		setFKdNachlass(fKdNachlass);
		setLieferantIId(lieferantIId);
		setFLFRabatt(fLfRabatt);
		setFLfZusatzrabatt(fLfZusatzrabatt);
		setFLfNachlass(fLfNachlass);
		setTTermin(tTermin);
		setStatusCNr(statusCNr);
		setArtikelIIdInseratart(artikelIIdInseratart);
		setPersonalIIdVertreter(personalIIdVertreter);
		setNMenge(nMenge);
		setNNettoeinzelpreisEk(nPreisEK);
		setNNettoeinzelpreisVk(nPReisVK);

		setPersonalIIdAnlegen(personalIIdAnlegen);
		setPersonalIIdAendern(personalIIdAendern);
		setTAnlegen(tAnlegen);
		setTAendern(tAendern);
		setBDruckBestellungKd(bDruckBestellungKd);
		setBDruckBestellungLf(bDruckBestellungLf);
		setBDruckRechnungKd(bDruckRechnungKd);
		setBWertaufteilen(bWertaufteilen);

	}

	public Integer getIId() {
		return iId;
	}

	public void setIId(Integer iId) {
		this.iId = iId;
	}

	public String getMandantCNr() {
		return mandantCNr;
	}

	public void setMandantCNr(String mandantCNr) {
		this.mandantCNr = mandantCNr;
	}

	public String getCNr() {
		return cNr;
	}

	public void setCNr(String cNr) {
		this.cNr = cNr;
	}

	public Timestamp getTBelegdatum() {
		return tBelegdatum;
	}

	public void setTBelegdatum(Timestamp tBelegdatum) {
		this.tBelegdatum = tBelegdatum;
	}

	public Double getFKdRabatt() {
		return fKdRabatt;
	}

	public void setFKdRabatt(Double fKdRabatt) {
		this.fKdRabatt = fKdRabatt;
	}

	public Double getFKdZusatzrabatt() {
		return fKdZusatzrabatt;
	}

	public void setFKdZusatzrabatt(Double fKdZusatzrabatt) {
		this.fKdZusatzrabatt = fKdZusatzrabatt;
	}

	public Double getFKdNachlass() {
		return fKdNachlass;
	}

	public void setFKdNachlass(Double fKdNachlass) {
		this.fKdNachlass = fKdNachlass;
	}

	public String getCBez() {
		return cBez;
	}

	public void setCBez(String cBez) {
		this.cBez = cBez;
	}

	public String getCRubrik() {
		return cRubrik;
	}

	public void setCRubrik(String cRubrik) {
		this.cRubrik = cRubrik;
	}

	public String getCRubrik2() {
		return cRubrik2;
	}

	public void setCRubrik2(String cRubrik2) {
		this.cRubrik2 = cRubrik2;
	}

	public String getCMedium() {
		return cMedium;
	}

	public void setCMedium(String cMedium) {
		this.cMedium = cMedium;
	}

	public String getXAnhang() {
		return xAnhang;
	}

	public void setXAnhang(String xAnhang) {
		this.xAnhang = xAnhang;
	}

	public Integer getLieferantIId() {
		return lieferantIId;
	}

	public void setLieferantIId(Integer lieferantIId) {
		this.lieferantIId = lieferantIId;
	}

	public Integer getAnsprechpartnerIIdLieferant() {
		return ansprechpartnerIIdLieferant;
	}

	public void setAnsprechpartnerIIdLieferant(
			Integer ansprechpartnerIIdLieferant) {
		this.ansprechpartnerIIdLieferant = ansprechpartnerIIdLieferant;
	}

	public Double getFLFRabatt() {
		return fLFRabatt;
	}

	public void setFLFRabatt(Double fLFRabatt) {
		this.fLFRabatt = fLFRabatt;
	}

	public Double getFLfZusatzrabatt() {
		return fLfZusatzrabatt;
	}

	public void setFLfZusatzrabatt(Double fLfZusatzrabatt) {
		this.fLfZusatzrabatt = fLfZusatzrabatt;
	}

	public Double getFLfNachlass() {
		return fLfNachlass;
	}

	public void setFLfNachlass(Double fLfNachlass) {
		this.fLfNachlass = fLfNachlass;
	}

	public String getXAnhangLf() {
		return xAnhangLf;
	}

	public void setXAnhangLf(String xAnhangLf) {
		this.xAnhangLf = xAnhangLf;
	}

	public Timestamp getTTermin() {
		return tTermin;
	}

	public void setTTermin(Timestamp tTermin) {
		this.tTermin = tTermin;
	}

	public Integer getArtikelIIdInseratart() {
		return artikelIIdInseratart;
	}

	public void setArtikelIIdInseratart(Integer artikelIIdInseratart) {
		this.artikelIIdInseratart = artikelIIdInseratart;
	}

	public String getStatusCNr() {
		return statusCNr;
	}

	public void setStatusCNr(String statusCNr) {
		this.statusCNr = statusCNr;
	}

	public Integer getPersonalIIdVertreter() {
		return personalIIdVertreter;
	}

	public void setPersonalIIdVertreter(Integer personalIIdVertreter) {
		this.personalIIdVertreter = personalIIdVertreter;
	}

	public BigDecimal getNMenge() {
		return nMenge;
	}

	public void setNMenge(BigDecimal nMenge) {
		this.nMenge = nMenge;
	}

	public BigDecimal getNNettoeinzelpreisEk() {
		return nNettoeinzelpreisEk;
	}

	public void setNNettoeinzelpreisEk(BigDecimal nNettoeinzelpreisEk) {
		this.nNettoeinzelpreisEk = nNettoeinzelpreisEk;
	}

	public BigDecimal getNNettoeinzelpreisVk() {
		return nNettoeinzelpreisVk;
	}

	public void setNNettoeinzelpreisVk(BigDecimal nNettoeinzelpreisVk) {
		this.nNettoeinzelpreisVk = nNettoeinzelpreisVk;
	}

	public Timestamp getTErschienen() {
		return tErschienen;
	}

	public void setTErschienen(Timestamp tErschienen) {
		this.tErschienen = tErschienen;
	}

	public Integer getPersonalIIdErschienen() {
		return personalIIdErschienen;
	}

	public void setPersonalIIdErschienen(Integer personalIIdErschienen) {
		this.personalIIdErschienen = personalIIdErschienen;
	}

	public Timestamp getTVerrechnen() {
		return tVerrechnen;
	}

	public void setTVerrechnen(Timestamp tVerrechnen) {
		this.tVerrechnen = tVerrechnen;
	}

	public Integer getPersonalIIdVerrechnen() {
		return personalIIdVerrechnen;
	}

	public void setPersonalIIdVerrechnen(Integer personalIIdVerrechnen) {
		this.personalIIdVerrechnen = personalIIdVerrechnen;
	}

	public String getCGestoppt() {
		return cGestoppt;
	}

	public void setCGestoppt(String cGestoppt) {
		this.cGestoppt = cGestoppt;
	}

	public Timestamp getTAnlegen() {
		return tAnlegen;
	}

	public void setTAnlegen(Timestamp tAnlegen) {
		this.tAnlegen = tAnlegen;
	}

	public Timestamp getTAendern() {
		return tAendern;
	}

	public void setTAendern(Timestamp tAendern) {
		this.tAendern = tAendern;
	}

	public Integer getPersonalIIdAendern() {
		return personalIIdAendern;
	}

	public void setPersonalIIdAendern(Integer personalIIdAendern) {
		this.personalIIdAendern = personalIIdAendern;
	}

	public Integer getPersonalIIdAnlegen() {
		return personalIIdAnlegen;
	}

	public void setPersonalIIdAnlegen(Integer personalIIdAnlegen) {
		this.personalIIdAnlegen = personalIIdAnlegen;
	}

	public Inserat() {

	}

}
