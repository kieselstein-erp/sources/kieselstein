package com.lp.server.util;

import java.util.Iterator;
import java.util.List;

public abstract class PositionNumberCachingAdapter extends PositionNumberAdapter {
    private Integer cachedHeadId;
    private List<?> positionList;

    @Override
    public Iterator<?> getPositionsIteratorForHeadIId(Integer headIId) {
        if (headIId == null) return null;

        if (positionList == null || !headIId.equals(cachedHeadId)) {
            positionList = getPositionsListForHeadIIdImpl(headIId);
            cachedHeadId = headIId;
        }

        return positionList.iterator();
    }

    protected abstract Iterator<?> getPositionsIteratorForHeadIIdImpl(Integer headIIdNotNull);

    protected abstract List<?> getPositionsListForHeadIIdImpl(Integer headIIdNotNull);
}
