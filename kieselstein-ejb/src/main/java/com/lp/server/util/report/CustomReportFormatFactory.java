package com.lp.server.util.report;

import net.sf.jasperreports.engine.util.DefaultFormatFactory;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import static com.lp.util.Helper.applyDecimalFormatSymbolsForLocale;

public class CustomReportFormatFactory extends DefaultFormatFactory {
    @Override
    public NumberFormat createNumberFormat(String pattern, Locale locale) {
        NumberFormat nf = super.createNumberFormat(pattern, locale);
        if (nf instanceof DecimalFormat) {
            DecimalFormat df = (DecimalFormat) nf;
            DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();
            df.setDecimalFormatSymbols(applyDecimalFormatSymbolsForLocale(dfs, locale));
        }
        return nf;
    }
}

