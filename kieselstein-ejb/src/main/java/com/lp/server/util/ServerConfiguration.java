/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 *  
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 *
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *   
 * Contact: developers@heliumv.com
 *******************************************************************************/
package com.lp.server.util;

import java.io.File;
import java.util.ResourceBundle;

public class ServerConfiguration {
	private static ResourceBundle cfgBundle = null;
	private static final String APPLICATION_PROPERTIES = "application";
	
	private static ResourceBundle config() {
		if (cfgBundle == null) {
			cfgBundle = ResourceBundle.getBundle(APPLICATION_PROPERTIES);
		}
		return cfgBundle ;
	}

	public static boolean getIsDevEnvironment() {
		return "true".equalsIgnoreCase(System.getenv("KIESELSTEIN_DEV"));
	}

	public static String getApplicationVersion() {
		return config().getString("application.version");
	}

	public static String getApplicationVersionHash() {
		return config().getString("application.version.hash");
	}

	public static String getStandardReports() {
		return config().getString("dist.reports");
	}

	public static String getUserReports() {
		return System.getenv("KIESELSTEIN_DATA") + File.separator + "reports";
	}

	/**
	 * Das Basis-Verzeichnis in dem die SSL Zertifikate liegen
	 * 
	 * @return das Basis-Verzeichnis der SSL-Zertifikate
	 */
	public static String getSSLCertificateDir() {
		return System.getenv("KIESELSTEIN_DATA") + File.separator + "certs";
	}
	
	/**
	 * Das Basis-Verzeichnis der Ruby Script-Dateien
	 * @return das Basis-Verzeichnis der Script-Dateien
	 */
	public static String getStandardScripts() {
		return config().getString("dist.scripts");
	}

	public static String getUserScripts() {
		return System.getenv("KIESELSTEIN_DATA") + File.separator + "scripts";
	}
	
	/**
	 * Der Benutzername des Admin-Benutzers
	 * @return der Name (Account-Name) des Admin-Users
	 */
	public static String getAdminUsername() {
		return config().getString("user.admin") ;
	}
	
	/**
	 * Webapp spezifische Settings ermitteln
	 * 
	 * @param appType ist die App
	 * @param token der Key f&uuml;r den der Wert ermittelt werden soll
	 * @return den Wert f&uuml;r das Token der jeweiligen App
	 */
	public static String getAppTypeProperty(int appType, String token) {
		return config().getString("extapp." + appType + "." + token) ;
	}
	
	/**
	 * Die gecacheten Werte l&ouml;schen</br>
	 * <p>Soll im Falle eines Falles erm&ouml;glichen, dass neue Werte
	 * eingestellt werden k&ouml;nnen, ohne den Server deswegen neu
	 * starten zu m&uuml;ssen</p>
	 */
	public static void clearCache() {
		ResourceBundle.clearCache(); 
		cfgBundle = null ;
	}
}
