package com.lp.server.util.report;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRExportProgressMonitor;
import net.sf.jasperreports.engine.export.JRXmlExporterContext;
import net.sf.jasperreports.engine.util.JRXmlWriteHelper;
import net.sf.jasperreports.export.*;
import net.sf.jasperreports.export.parameters.ParametersXmlExporterOutput;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class JRXExporter extends JRAbstractExporter<ReportExportConfiguration, ExporterConfiguration, XmlExporterOutput, JRXmlExporterContext> {
    protected JRXmlWriteHelper xmlWriter;
    protected String version;

    public JRXExporter() {
        this(DefaultJasperReportsContext.getInstance());
    }

    public JRXExporter(JasperReportsContext jasperReportsContext) {
        super(jasperReportsContext);
        this.exporterContext = new JRXExporter.ExporterContext();
    }

    protected Class<ExporterConfiguration> getConfigurationInterface() {
        return ExporterConfiguration.class;
    }

    protected Class<ReportExportConfiguration> getItemConfigurationInterface() {
        return ReportExportConfiguration.class;
    }

    protected void ensureOutput() {
        if (this.exporterOutput == null) {
            this.exporterOutput = new ParametersXmlExporterOutput(this.getJasperReportsContext(), this.getParameters(), this.getCurrentJasperPrint());
        }
    }

    public void exportReport() throws JRException {
        this.ensureJasperReportsContext();
        this.ensureInput();
        this.initExport();
        this.ensureOutput();

        Writer writer = this.getExporterOutput().getWriter();

        try {
            this.exportReportToStream(writer);
        } catch (IOException e) {
            throw new JRRuntimeException(e);
        } finally {
            this.getExporterOutput().close();
            this.resetExportContext();
        }
    }

    protected void initExport() {
        super.initExport();
    }

    protected void initReport() {
        super.initReport();
    }

    @Override
    public String getExporterKey() {
        return "org.kieselstein.jr.xml";
    }

    @Override
    public String getExporterPropertiesPrefix() {
        return "org.kieselstein.jr.xml.";
    }

    protected void exportReportToStream(Writer writer) throws JRException, IOException {
        this.xmlWriter = new JRXmlWriteHelper(writer);
        this.xmlWriter.writeProlog(this.getExporterOutput().getEncoding());

        this.setCurrentExporterInputItem(this.exporterInput.getItems().get(0));
        List<JRPrintPage> pages = this.jasperPrint.getPages();
        JRAbstractExporter<ReportExportConfiguration, ExporterConfiguration, XmlExporterOutput, JRXmlExporterContext>.PageRange pageRange = this.getPageRange();
        int startPageIndex = pageRange != null && pageRange.getStartPageIndex() != null ? pageRange.getStartPageIndex() : 0;
        int endPageIndex = pageRange != null && pageRange.getEndPageIndex() != null ? pageRange.getEndPageIndex() : pages.size() - 1;

        if (pages != null && !pages.isEmpty()) {
            for (int i = startPageIndex; i <= endPageIndex; ++i) {
                if (Thread.interrupted()) {
                    throw new ExportInterruptedException();
                }

                JRPrintPage page = pages.get(i);
                this.exportPage(page);
            }
        }

        writer.flush();
    }

    protected void exportPage(JRPrintPage page) throws IOException {
        List<JRPrintElement> elements = page.getElements();
        if (elements != null && !elements.isEmpty()) {
            for (JRPrintElement element : elements) {
                JRPrintText text = (JRPrintText) element;

                if (text.getOriginalText() != null) {
                    this.xmlWriter.getUnderlyingWriter().write(text.getOriginalText());
                }
            }
        }

        JRExportProgressMonitor progressMonitor = this.getCurrentItemConfiguration().getProgressMonitor();
        if (progressMonitor != null) {
            progressMonitor.afterPageExport();
        }
    }

    protected class ExporterContext extends JRAbstractExporter<ReportExportConfiguration, ExporterConfiguration, XmlExporterOutput, JRXmlExporterContext>.BaseExporterContext implements JRXmlExporterContext {
        protected ExporterContext() {
            super();
        }
    }
}
