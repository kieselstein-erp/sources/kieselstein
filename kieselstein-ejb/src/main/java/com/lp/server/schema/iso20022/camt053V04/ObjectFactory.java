//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Aenderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2020.05.17 um 06:36:00 PM CEST 
//


package com.lp.server.schema.iso20022.camt053V04;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.lp.server.schema.iso20022.camt053V04 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Document_QNAME = new QName("urn:iso:std:iso:20022:tech:xsd:camt.053.001.04", "Document");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.lp.server.schema.iso20022.camt053V04
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CA04Document }
     * 
     */
    public CA04Document createCA04Document() {
        return new CA04Document();
    }

    /**
     * Create an instance of {@link CA04DateAndDateTimeChoice }
     * 
     */
    public CA04DateAndDateTimeChoice createCA04DateAndDateTimeChoice() {
        return new CA04DateAndDateTimeChoice();
    }

    /**
     * Create an instance of {@link CA04ChargesRecord2 }
     * 
     */
    public CA04ChargesRecord2 createCA04ChargesRecord2() {
        return new CA04ChargesRecord2();
    }

    /**
     * Create an instance of {@link CA04OriginalBusinessQuery1 }
     * 
     */
    public CA04OriginalBusinessQuery1 createCA04OriginalBusinessQuery1() {
        return new CA04OriginalBusinessQuery1();
    }

    /**
     * Create an instance of {@link CA04TaxInformation3 }
     * 
     */
    public CA04TaxInformation3 createCA04TaxInformation3() {
        return new CA04TaxInformation3();
    }

    /**
     * Create an instance of {@link CA04TaxRecordDetails1 }
     * 
     */
    public CA04TaxRecordDetails1 createCA04TaxRecordDetails1() {
        return new CA04TaxRecordDetails1();
    }

    /**
     * Create an instance of {@link CA04PartyIdentification43 }
     * 
     */
    public CA04PartyIdentification43 createCA04PartyIdentification43() {
        return new CA04PartyIdentification43();
    }

    /**
     * Create an instance of {@link CA04DocumentAdjustment1 }
     * 
     */
    public CA04DocumentAdjustment1 createCA04DocumentAdjustment1() {
        return new CA04DocumentAdjustment1();
    }

    /**
     * Create an instance of {@link CA04GenericOrganisationIdentification1 }
     * 
     */
    public CA04GenericOrganisationIdentification1 createCA04GenericOrganisationIdentification1() {
        return new CA04GenericOrganisationIdentification1();
    }

    /**
     * Create an instance of {@link CA04CardEntry1 }
     * 
     */
    public CA04CardEntry1 createCA04CardEntry1() {
        return new CA04CardEntry1();
    }

    /**
     * Create an instance of {@link CA04GroupHeader58 }
     * 
     */
    public CA04GroupHeader58 createCA04GroupHeader58() {
        return new CA04GroupHeader58();
    }

    /**
     * Create an instance of {@link CA04TotalsPerBankTransactionCode3 }
     * 
     */
    public CA04TotalsPerBankTransactionCode3 createCA04TotalsPerBankTransactionCode3() {
        return new CA04TotalsPerBankTransactionCode3();
    }

    /**
     * Create an instance of {@link CA04AmountAndCurrencyExchange3 }
     * 
     */
    public CA04AmountAndCurrencyExchange3 createCA04AmountAndCurrencyExchange3() {
        return new CA04AmountAndCurrencyExchange3();
    }

    /**
     * Create an instance of {@link CA04TaxCharges2 }
     * 
     */
    public CA04TaxCharges2 createCA04TaxCharges2() {
        return new CA04TaxCharges2();
    }

    /**
     * Create an instance of {@link CA04StructuredRemittanceInformation9 }
     * 
     */
    public CA04StructuredRemittanceInformation9 createCA04StructuredRemittanceInformation9() {
        return new CA04StructuredRemittanceInformation9();
    }

    /**
     * Create an instance of {@link CA04AccountInterest2 }
     * 
     */
    public CA04AccountInterest2 createCA04AccountInterest2() {
        return new CA04AccountInterest2();
    }

    /**
     * Create an instance of {@link CA04TaxAmountAndType1 }
     * 
     */
    public CA04TaxAmountAndType1 createCA04TaxAmountAndType1() {
        return new CA04TaxAmountAndType1();
    }

    /**
     * Create an instance of {@link CA04CashAccountType2Choice }
     * 
     */
    public CA04CashAccountType2Choice createCA04CashAccountType2Choice() {
        return new CA04CashAccountType2Choice();
    }

    /**
     * Create an instance of {@link CA04OrganisationIdentification8 }
     * 
     */
    public CA04OrganisationIdentification8 createCA04OrganisationIdentification8() {
        return new CA04OrganisationIdentification8();
    }

    /**
     * Create an instance of {@link CA04GenericIdentification3 }
     * 
     */
    public CA04GenericIdentification3 createCA04GenericIdentification3() {
        return new CA04GenericIdentification3();
    }

    /**
     * Create an instance of {@link CA04GenericIdentification1 }
     * 
     */
    public CA04GenericIdentification1 createCA04GenericIdentification1() {
        return new CA04GenericIdentification1();
    }

    /**
     * Create an instance of {@link CA04InterestType1Choice }
     * 
     */
    public CA04InterestType1Choice createCA04InterestType1Choice() {
        return new CA04InterestType1Choice();
    }

    /**
     * Create an instance of {@link CA04CardAggregated1 }
     * 
     */
    public CA04CardAggregated1 createCA04CardAggregated1() {
        return new CA04CardAggregated1();
    }

    /**
     * Create an instance of {@link CA04RemittanceInformation7 }
     * 
     */
    public CA04RemittanceInformation7 createCA04RemittanceInformation7() {
        return new CA04RemittanceInformation7();
    }

    /**
     * Create an instance of {@link CA04ActiveOrHistoricCurrencyAnd13DecimalAmount }
     * 
     */
    public CA04ActiveOrHistoricCurrencyAnd13DecimalAmount createCA04ActiveOrHistoricCurrencyAnd13DecimalAmount() {
        return new CA04ActiveOrHistoricCurrencyAnd13DecimalAmount();
    }

    /**
     * Create an instance of {@link CA04CreditorReferenceType2 }
     * 
     */
    public CA04CreditorReferenceType2 createCA04CreditorReferenceType2() {
        return new CA04CreditorReferenceType2();
    }

    /**
     * Create an instance of {@link CA04PointOfInteraction1 }
     * 
     */
    public CA04PointOfInteraction1 createCA04PointOfInteraction1() {
        return new CA04PointOfInteraction1();
    }

    /**
     * Create an instance of {@link CA04BranchAndFinancialInstitutionIdentification5 }
     * 
     */
    public CA04BranchAndFinancialInstitutionIdentification5 createCA04BranchAndFinancialInstitutionIdentification5() {
        return new CA04BranchAndFinancialInstitutionIdentification5();
    }

    /**
     * Create an instance of {@link CA04PaymentCard4 }
     * 
     */
    public CA04PaymentCard4 createCA04PaymentCard4() {
        return new CA04PaymentCard4();
    }

    /**
     * Create an instance of {@link CA04RemittanceLocation2 }
     * 
     */
    public CA04RemittanceLocation2 createCA04RemittanceLocation2() {
        return new CA04RemittanceLocation2();
    }

    /**
     * Create an instance of {@link CA04EntryTransaction4 }
     * 
     */
    public CA04EntryTransaction4 createCA04EntryTransaction4() {
        return new CA04EntryTransaction4();
    }

    /**
     * Create an instance of {@link CA04BalanceType12 }
     * 
     */
    public CA04BalanceType12 createCA04BalanceType12() {
        return new CA04BalanceType12();
    }

    /**
     * Create an instance of {@link CA04TransactionPrice3Choice }
     * 
     */
    public CA04TransactionPrice3Choice createCA04TransactionPrice3Choice() {
        return new CA04TransactionPrice3Choice();
    }

    /**
     * Create an instance of {@link CA04CardIndividualTransaction1 }
     * 
     */
    public CA04CardIndividualTransaction1 createCA04CardIndividualTransaction1() {
        return new CA04CardIndividualTransaction1();
    }

    /**
     * Create an instance of {@link CA04FinancialIdentificationSchemeName1Choice }
     * 
     */
    public CA04FinancialIdentificationSchemeName1Choice createCA04FinancialIdentificationSchemeName1Choice() {
        return new CA04FinancialIdentificationSchemeName1Choice();
    }

    /**
     * Create an instance of {@link CA04ReferredDocumentInformation3 }
     * 
     */
    public CA04ReferredDocumentInformation3 createCA04ReferredDocumentInformation3() {
        return new CA04ReferredDocumentInformation3();
    }

    /**
     * Create an instance of {@link CA04CashBalanceAvailabilityDate1 }
     * 
     */
    public CA04CashBalanceAvailabilityDate1 createCA04CashBalanceAvailabilityDate1() {
        return new CA04CashBalanceAvailabilityDate1();
    }

    /**
     * Create an instance of {@link CA04DiscountAmountAndType1 }
     * 
     */
    public CA04DiscountAmountAndType1 createCA04DiscountAmountAndType1() {
        return new CA04DiscountAmountAndType1();
    }

    /**
     * Create an instance of {@link CA04BatchInformation2 }
     * 
     */
    public CA04BatchInformation2 createCA04BatchInformation2() {
        return new CA04BatchInformation2();
    }

    /**
     * Create an instance of {@link CA04TaxAmountType1Choice }
     * 
     */
    public CA04TaxAmountType1Choice createCA04TaxAmountType1Choice() {
        return new CA04TaxAmountType1Choice();
    }

    /**
     * Create an instance of {@link CA04Charges4 }
     * 
     */
    public CA04Charges4 createCA04Charges4() {
        return new CA04Charges4();
    }

    /**
     * Create an instance of {@link CA04CurrencyExchange5 }
     * 
     */
    public CA04CurrencyExchange5 createCA04CurrencyExchange5() {
        return new CA04CurrencyExchange5();
    }

    /**
     * Create an instance of {@link CA04TaxAmount1 }
     * 
     */
    public CA04TaxAmount1 createCA04TaxAmount1() {
        return new CA04TaxAmount1();
    }

    /**
     * Create an instance of {@link CA04TransactionDates2 }
     * 
     */
    public CA04TransactionDates2 createCA04TransactionDates2() {
        return new CA04TransactionDates2();
    }

    /**
     * Create an instance of {@link CA04CashBalanceAvailability2 }
     * 
     */
    public CA04CashBalanceAvailability2 createCA04CashBalanceAvailability2() {
        return new CA04CashBalanceAvailability2();
    }

    /**
     * Create an instance of {@link CA04FinancialInstitutionIdentification8 }
     * 
     */
    public CA04FinancialInstitutionIdentification8 createCA04FinancialInstitutionIdentification8() {
        return new CA04FinancialInstitutionIdentification8();
    }

    /**
     * Create an instance of {@link CA04BankTransactionCodeStructure6 }
     * 
     */
    public CA04BankTransactionCodeStructure6 createCA04BankTransactionCodeStructure6() {
        return new CA04BankTransactionCodeStructure6();
    }

    /**
     * Create an instance of {@link CA04BankTransactionCodeStructure4 }
     * 
     */
    public CA04BankTransactionCodeStructure4 createCA04BankTransactionCodeStructure4() {
        return new CA04BankTransactionCodeStructure4();
    }

    /**
     * Create an instance of {@link CA04BankTransactionCodeStructure5 }
     * 
     */
    public CA04BankTransactionCodeStructure5 createCA04BankTransactionCodeStructure5() {
        return new CA04BankTransactionCodeStructure5();
    }

    /**
     * Create an instance of {@link CA04AmountRangeBoundary1 }
     * 
     */
    public CA04AmountRangeBoundary1 createCA04AmountRangeBoundary1() {
        return new CA04AmountRangeBoundary1();
    }

    /**
     * Create an instance of {@link CA04CashAccount24 }
     * 
     */
    public CA04CashAccount24 createCA04CashAccount24() {
        return new CA04CashAccount24();
    }

    /**
     * Create an instance of {@link CA04CashAccount25 }
     * 
     */
    public CA04CashAccount25 createCA04CashAccount25() {
        return new CA04CashAccount25();
    }

    /**
     * Create an instance of {@link CA04TaxRecord1 }
     * 
     */
    public CA04TaxRecord1 createCA04TaxRecord1() {
        return new CA04TaxRecord1();
    }

    /**
     * Create an instance of {@link CA04DiscountAmountType1Choice }
     * 
     */
    public CA04DiscountAmountType1Choice createCA04DiscountAmountType1Choice() {
        return new CA04DiscountAmountType1Choice();
    }

    /**
     * Create an instance of {@link CA04ReferredDocumentType2 }
     * 
     */
    public CA04ReferredDocumentType2 createCA04ReferredDocumentType2() {
        return new CA04ReferredDocumentType2();
    }

    /**
     * Create an instance of {@link CA04YieldedOrValueType1Choice }
     * 
     */
    public CA04YieldedOrValueType1Choice createCA04YieldedOrValueType1Choice() {
        return new CA04YieldedOrValueType1Choice();
    }

    /**
     * Create an instance of {@link CA04SupplementaryData1 }
     * 
     */
    public CA04SupplementaryData1 createCA04SupplementaryData1() {
        return new CA04SupplementaryData1();
    }

    /**
     * Create an instance of {@link CA04IdentificationSource3Choice }
     * 
     */
    public CA04IdentificationSource3Choice createCA04IdentificationSource3Choice() {
        return new CA04IdentificationSource3Choice();
    }

    /**
     * Create an instance of {@link CA04TransactionQuantities2Choice }
     * 
     */
    public CA04TransactionQuantities2Choice createCA04TransactionQuantities2Choice() {
        return new CA04TransactionQuantities2Choice();
    }

    /**
     * Create an instance of {@link CA04PostalAddress6 }
     * 
     */
    public CA04PostalAddress6 createCA04PostalAddress6() {
        return new CA04PostalAddress6();
    }

    /**
     * Create an instance of {@link CA04CashDeposit1 }
     * 
     */
    public CA04CashDeposit1 createCA04CashDeposit1() {
        return new CA04CashDeposit1();
    }

    /**
     * Create an instance of {@link CA04ContactDetails2 }
     * 
     */
    public CA04ContactDetails2 createCA04ContactDetails2() {
        return new CA04ContactDetails2();
    }

    /**
     * Create an instance of {@link CA04EntryDetails3 }
     * 
     */
    public CA04EntryDetails3 createCA04EntryDetails3() {
        return new CA04EntryDetails3();
    }

    /**
     * Create an instance of {@link CA04Price2 }
     * 
     */
    public CA04Price2 createCA04Price2() {
        return new CA04Price2();
    }

    /**
     * Create an instance of {@link CA04TaxAuthorisation1 }
     * 
     */
    public CA04TaxAuthorisation1 createCA04TaxAuthorisation1() {
        return new CA04TaxAuthorisation1();
    }

    /**
     * Create an instance of {@link CA04ReturnReason5Choice }
     * 
     */
    public CA04ReturnReason5Choice createCA04ReturnReason5Choice() {
        return new CA04ReturnReason5Choice();
    }

    /**
     * Create an instance of {@link CA04CardSecurityInformation1 }
     * 
     */
    public CA04CardSecurityInformation1 createCA04CardSecurityInformation1() {
        return new CA04CardSecurityInformation1();
    }

    /**
     * Create an instance of {@link CA04NumberAndSumOfTransactions1 }
     * 
     */
    public CA04NumberAndSumOfTransactions1 createCA04NumberAndSumOfTransactions1() {
        return new CA04NumberAndSumOfTransactions1();
    }

    /**
     * Create an instance of {@link CA04NumberAndSumOfTransactions4 }
     * 
     */
    public CA04NumberAndSumOfTransactions4 createCA04NumberAndSumOfTransactions4() {
        return new CA04NumberAndSumOfTransactions4();
    }

    /**
     * Create an instance of {@link CA04FromToAmountRange }
     * 
     */
    public CA04FromToAmountRange createCA04FromToAmountRange() {
        return new CA04FromToAmountRange();
    }

    /**
     * Create an instance of {@link CA04TaxPeriod1 }
     * 
     */
    public CA04TaxPeriod1 createCA04TaxPeriod1() {
        return new CA04TaxPeriod1();
    }

    /**
     * Create an instance of {@link CA04TransactionReferences3 }
     * 
     */
    public CA04TransactionReferences3 createCA04TransactionReferences3() {
        return new CA04TransactionReferences3();
    }

    /**
     * Create an instance of {@link CA04GenericFinancialIdentification1 }
     * 
     */
    public CA04GenericFinancialIdentification1 createCA04GenericFinancialIdentification1() {
        return new CA04GenericFinancialIdentification1();
    }

    /**
     * Create an instance of {@link CA04ProprietaryQuantity1 }
     * 
     */
    public CA04ProprietaryQuantity1 createCA04ProprietaryQuantity1() {
        return new CA04ProprietaryQuantity1();
    }

    /**
     * Create an instance of {@link CA04ProprietaryBankTransactionCodeStructure1 }
     * 
     */
    public CA04ProprietaryBankTransactionCodeStructure1 createCA04ProprietaryBankTransactionCodeStructure1() {
        return new CA04ProprietaryBankTransactionCodeStructure1();
    }

    /**
     * Create an instance of {@link CA04PointOfInteractionCapabilities1 }
     * 
     */
    public CA04PointOfInteractionCapabilities1 createCA04PointOfInteractionCapabilities1() {
        return new CA04PointOfInteractionCapabilities1();
    }

    /**
     * Create an instance of {@link CA04OriginalAndCurrentQuantities1 }
     * 
     */
    public CA04OriginalAndCurrentQuantities1 createCA04OriginalAndCurrentQuantities1() {
        return new CA04OriginalAndCurrentQuantities1();
    }

    /**
     * Create an instance of {@link CA04TransactionParties3 }
     * 
     */
    public CA04TransactionParties3 createCA04TransactionParties3() {
        return new CA04TransactionParties3();
    }

    /**
     * Create an instance of {@link CA04PaymentReturnReason2 }
     * 
     */
    public CA04PaymentReturnReason2 createCA04PaymentReturnReason2() {
        return new CA04PaymentReturnReason2();
    }

    /**
     * Create an instance of {@link CA04CreditorReferenceType1Choice }
     * 
     */
    public CA04CreditorReferenceType1Choice createCA04CreditorReferenceType1Choice() {
        return new CA04CreditorReferenceType1Choice();
    }

    /**
     * Create an instance of {@link CA04ProprietaryDate2 }
     * 
     */
    public CA04ProprietaryDate2 createCA04ProprietaryDate2() {
        return new CA04ProprietaryDate2();
    }

    /**
     * Create an instance of {@link CA04RateType4Choice }
     * 
     */
    public CA04RateType4Choice createCA04RateType4Choice() {
        return new CA04RateType4Choice();
    }

    /**
     * Create an instance of {@link CA04DatePeriodDetails }
     * 
     */
    public CA04DatePeriodDetails createCA04DatePeriodDetails() {
        return new CA04DatePeriodDetails();
    }

    /**
     * Create an instance of {@link CA04NameAndAddress10 }
     * 
     */
    public CA04NameAndAddress10 createCA04NameAndAddress10() {
        return new CA04NameAndAddress10();
    }

    /**
     * Create an instance of {@link CA04TotalTransactions4 }
     * 
     */
    public CA04TotalTransactions4 createCA04TotalTransactions4() {
        return new CA04TotalTransactions4();
    }

    /**
     * Create an instance of {@link CA04PersonIdentificationSchemeName1Choice }
     * 
     */
    public CA04PersonIdentificationSchemeName1Choice createCA04PersonIdentificationSchemeName1Choice() {
        return new CA04PersonIdentificationSchemeName1Choice();
    }

    /**
     * Create an instance of {@link CA04ProprietaryReference1 }
     * 
     */
    public CA04ProprietaryReference1 createCA04ProprietaryReference1() {
        return new CA04ProprietaryReference1();
    }

    /**
     * Create an instance of {@link CA04BankToCustomerStatementV04 }
     * 
     */
    public CA04BankToCustomerStatementV04 createCA04BankToCustomerStatementV04() {
        return new CA04BankToCustomerStatementV04();
    }

    /**
     * Create an instance of {@link CA04TransactionInterest3 }
     * 
     */
    public CA04TransactionInterest3 createCA04TransactionInterest3() {
        return new CA04TransactionInterest3();
    }

    /**
     * Create an instance of {@link CA04ImpliedCurrencyAmountRangeChoice }
     * 
     */
    public CA04ImpliedCurrencyAmountRangeChoice createCA04ImpliedCurrencyAmountRangeChoice() {
        return new CA04ImpliedCurrencyAmountRangeChoice();
    }

    /**
     * Create an instance of {@link CA04ActiveOrHistoricCurrencyAndAmount }
     * 
     */
    public CA04ActiveOrHistoricCurrencyAndAmount createCA04ActiveOrHistoricCurrencyAndAmount() {
        return new CA04ActiveOrHistoricCurrencyAndAmount();
    }

    /**
     * Create an instance of {@link CA04MessageIdentification2 }
     * 
     */
    public CA04MessageIdentification2 createCA04MessageIdentification2() {
        return new CA04MessageIdentification2();
    }

    /**
     * Create an instance of {@link CA04TechnicalInputChannel1Choice }
     * 
     */
    public CA04TechnicalInputChannel1Choice createCA04TechnicalInputChannel1Choice() {
        return new CA04TechnicalInputChannel1Choice();
    }

    /**
     * Create an instance of {@link CA04ClearingSystemIdentification2Choice }
     * 
     */
    public CA04ClearingSystemIdentification2Choice createCA04ClearingSystemIdentification2Choice() {
        return new CA04ClearingSystemIdentification2Choice();
    }

    /**
     * Create an instance of {@link CA04TrackData1 }
     * 
     */
    public CA04TrackData1 createCA04TrackData1() {
        return new CA04TrackData1();
    }

    /**
     * Create an instance of {@link CA04OrganisationIdentificationSchemeName1Choice }
     * 
     */
    public CA04OrganisationIdentificationSchemeName1Choice createCA04OrganisationIdentificationSchemeName1Choice() {
        return new CA04OrganisationIdentificationSchemeName1Choice();
    }

    /**
     * Create an instance of {@link CA04CurrencyAndAmountRange2 }
     * 
     */
    public CA04CurrencyAndAmountRange2 createCA04CurrencyAndAmountRange2() {
        return new CA04CurrencyAndAmountRange2();
    }

    /**
     * Create an instance of {@link CA04DateOrDateTimePeriodChoice }
     * 
     */
    public CA04DateOrDateTimePeriodChoice createCA04DateOrDateTimePeriodChoice() {
        return new CA04DateOrDateTimePeriodChoice();
    }

    /**
     * Create an instance of {@link CA04Product2 }
     * 
     */
    public CA04Product2 createCA04Product2() {
        return new CA04Product2();
    }

    /**
     * Create an instance of {@link CA04BalanceType5Choice }
     * 
     */
    public CA04BalanceType5Choice createCA04BalanceType5Choice() {
        return new CA04BalanceType5Choice();
    }

    /**
     * Create an instance of {@link CA04GenericIdentification32 }
     * 
     */
    public CA04GenericIdentification32 createCA04GenericIdentification32() {
        return new CA04GenericIdentification32();
    }

    /**
     * Create an instance of {@link CA04OtherIdentification1 }
     * 
     */
    public CA04OtherIdentification1 createCA04OtherIdentification1() {
        return new CA04OtherIdentification1();
    }

    /**
     * Create an instance of {@link CA04CorporateAction9 }
     * 
     */
    public CA04CorporateAction9 createCA04CorporateAction9() {
        return new CA04CorporateAction9();
    }

    /**
     * Create an instance of {@link CA04CashBalance3 }
     * 
     */
    public CA04CashBalance3 createCA04CashBalance3() {
        return new CA04CashBalance3();
    }

    /**
     * Create an instance of {@link CA04PointOfInteractionComponent1 }
     * 
     */
    public CA04PointOfInteractionComponent1 createCA04PointOfInteractionComponent1() {
        return new CA04PointOfInteractionComponent1();
    }

    /**
     * Create an instance of {@link CA04TransactionIdentifier1 }
     * 
     */
    public CA04TransactionIdentifier1 createCA04TransactionIdentifier1() {
        return new CA04TransactionIdentifier1();
    }

    /**
     * Create an instance of {@link CA04CreditLine2 }
     * 
     */
    public CA04CreditLine2 createCA04CreditLine2() {
        return new CA04CreditLine2();
    }

    /**
     * Create an instance of {@link CA04AccountSchemeName1Choice }
     * 
     */
    public CA04AccountSchemeName1Choice createCA04AccountSchemeName1Choice() {
        return new CA04AccountSchemeName1Choice();
    }

    /**
     * Create an instance of {@link CA04SecuritiesAccount13 }
     * 
     */
    public CA04SecuritiesAccount13 createCA04SecuritiesAccount13() {
        return new CA04SecuritiesAccount13();
    }

    /**
     * Create an instance of {@link CA04ProprietaryPrice2 }
     * 
     */
    public CA04ProprietaryPrice2 createCA04ProprietaryPrice2() {
        return new CA04ProprietaryPrice2();
    }

    /**
     * Create an instance of {@link CA04PersonIdentification5 }
     * 
     */
    public CA04PersonIdentification5 createCA04PersonIdentification5() {
        return new CA04PersonIdentification5();
    }

    /**
     * Create an instance of {@link CA04GenericIdentification20 }
     * 
     */
    public CA04GenericIdentification20 createCA04GenericIdentification20() {
        return new CA04GenericIdentification20();
    }

    /**
     * Create an instance of {@link CA04ActiveCurrencyAndAmount }
     * 
     */
    public CA04ActiveCurrencyAndAmount createCA04ActiveCurrencyAndAmount() {
        return new CA04ActiveCurrencyAndAmount();
    }

    /**
     * Create an instance of {@link CA04TransactionAgents3 }
     * 
     */
    public CA04TransactionAgents3 createCA04TransactionAgents3() {
        return new CA04TransactionAgents3();
    }

    /**
     * Create an instance of {@link CA04Party11Choice }
     * 
     */
    public CA04Party11Choice createCA04Party11Choice() {
        return new CA04Party11Choice();
    }

    /**
     * Create an instance of {@link CA04SupplementaryDataEnvelope1 }
     * 
     */
    public CA04SupplementaryDataEnvelope1 createCA04SupplementaryDataEnvelope1() {
        return new CA04SupplementaryDataEnvelope1();
    }

    /**
     * Create an instance of {@link CA04Rate3 }
     * 
     */
    public CA04Rate3 createCA04Rate3() {
        return new CA04Rate3();
    }

    /**
     * Create an instance of {@link CA04ProprietaryParty3 }
     * 
     */
    public CA04ProprietaryParty3 createCA04ProprietaryParty3() {
        return new CA04ProprietaryParty3();
    }

    /**
     * Create an instance of {@link CA04Pagination }
     * 
     */
    public CA04Pagination createCA04Pagination() {
        return new CA04Pagination();
    }

    /**
     * Create an instance of {@link CA04SecurityIdentification14 }
     * 
     */
    public CA04SecurityIdentification14 createCA04SecurityIdentification14() {
        return new CA04SecurityIdentification14();
    }

    /**
     * Create an instance of {@link CA04ClearingSystemMemberIdentification2 }
     * 
     */
    public CA04ClearingSystemMemberIdentification2 createCA04ClearingSystemMemberIdentification2() {
        return new CA04ClearingSystemMemberIdentification2();
    }

    /**
     * Create an instance of {@link CA04CardSequenceNumberRange1 }
     * 
     */
    public CA04CardSequenceNumberRange1 createCA04CardSequenceNumberRange1() {
        return new CA04CardSequenceNumberRange1();
    }

    /**
     * Create an instance of {@link CA04FinancialInstrumentQuantityChoice }
     * 
     */
    public CA04FinancialInstrumentQuantityChoice createCA04FinancialInstrumentQuantityChoice() {
        return new CA04FinancialInstrumentQuantityChoice();
    }

    /**
     * Create an instance of {@link CA04ReferredDocumentType1Choice }
     * 
     */
    public CA04ReferredDocumentType1Choice createCA04ReferredDocumentType1Choice() {
        return new CA04ReferredDocumentType1Choice();
    }

    /**
     * Create an instance of {@link CA04DisplayCapabilities1 }
     * 
     */
    public CA04DisplayCapabilities1 createCA04DisplayCapabilities1() {
        return new CA04DisplayCapabilities1();
    }

    /**
     * Create an instance of {@link CA04PlainCardData1 }
     * 
     */
    public CA04PlainCardData1 createCA04PlainCardData1() {
        return new CA04PlainCardData1();
    }

    /**
     * Create an instance of {@link CA04AccountIdentification4Choice }
     * 
     */
    public CA04AccountIdentification4Choice createCA04AccountIdentification4Choice() {
        return new CA04AccountIdentification4Choice();
    }

    /**
     * Create an instance of {@link CA04DateTimePeriodDetails }
     * 
     */
    public CA04DateTimePeriodDetails createCA04DateTimePeriodDetails() {
        return new CA04DateTimePeriodDetails();
    }

    /**
     * Create an instance of {@link CA04AmountAndDirection35 }
     * 
     */
    public CA04AmountAndDirection35 createCA04AmountAndDirection35() {
        return new CA04AmountAndDirection35();
    }

    /**
     * Create an instance of {@link CA04DateAndPlaceOfBirth }
     * 
     */
    public CA04DateAndPlaceOfBirth createCA04DateAndPlaceOfBirth() {
        return new CA04DateAndPlaceOfBirth();
    }

    /**
     * Create an instance of {@link CA04GenericPersonIdentification1 }
     * 
     */
    public CA04GenericPersonIdentification1 createCA04GenericPersonIdentification1() {
        return new CA04GenericPersonIdentification1();
    }

    /**
     * Create an instance of {@link CA04GenericAccountIdentification1 }
     * 
     */
    public CA04GenericAccountIdentification1 createCA04GenericAccountIdentification1() {
        return new CA04GenericAccountIdentification1();
    }

    /**
     * Create an instance of {@link CA04Purpose2Choice }
     * 
     */
    public CA04Purpose2Choice createCA04Purpose2Choice() {
        return new CA04Purpose2Choice();
    }

    /**
     * Create an instance of {@link CA04TaxParty1 }
     * 
     */
    public CA04TaxParty1 createCA04TaxParty1() {
        return new CA04TaxParty1();
    }

    /**
     * Create an instance of {@link CA04TaxParty2 }
     * 
     */
    public CA04TaxParty2 createCA04TaxParty2() {
        return new CA04TaxParty2();
    }

    /**
     * Create an instance of {@link CA04PriceRateOrAmountChoice }
     * 
     */
    public CA04PriceRateOrAmountChoice createCA04PriceRateOrAmountChoice() {
        return new CA04PriceRateOrAmountChoice();
    }

    /**
     * Create an instance of {@link CA04BalanceSubType1Choice }
     * 
     */
    public CA04BalanceSubType1Choice createCA04BalanceSubType1Choice() {
        return new CA04BalanceSubType1Choice();
    }

    /**
     * Create an instance of {@link CA04RemittanceAmount2 }
     * 
     */
    public CA04RemittanceAmount2 createCA04RemittanceAmount2() {
        return new CA04RemittanceAmount2();
    }

    /**
     * Create an instance of {@link CA04ReportingSource1Choice }
     * 
     */
    public CA04ReportingSource1Choice createCA04ReportingSource1Choice() {
        return new CA04ReportingSource1Choice();
    }

    /**
     * Create an instance of {@link CA04ChargeType3Choice }
     * 
     */
    public CA04ChargeType3Choice createCA04ChargeType3Choice() {
        return new CA04ChargeType3Choice();
    }

    /**
     * Create an instance of {@link CA04BranchData2 }
     * 
     */
    public CA04BranchData2 createCA04BranchData2() {
        return new CA04BranchData2();
    }

    /**
     * Create an instance of {@link CA04CardTransaction1 }
     * 
     */
    public CA04CardTransaction1 createCA04CardTransaction1() {
        return new CA04CardTransaction1();
    }

    /**
     * Create an instance of {@link CA04AccountStatement4 }
     * 
     */
    public CA04AccountStatement4 createCA04AccountStatement4() {
        return new CA04AccountStatement4();
    }

    /**
     * Create an instance of {@link CA04ProprietaryAgent3 }
     * 
     */
    public CA04ProprietaryAgent3 createCA04ProprietaryAgent3() {
        return new CA04ProprietaryAgent3();
    }

    /**
     * Create an instance of {@link CA04CreditorReferenceInformation2 }
     * 
     */
    public CA04CreditorReferenceInformation2 createCA04CreditorReferenceInformation2() {
        return new CA04CreditorReferenceInformation2();
    }

    /**
     * Create an instance of {@link CA04CardTransaction1Choice }
     * 
     */
    public CA04CardTransaction1Choice createCA04CardTransaction1Choice() {
        return new CA04CardTransaction1Choice();
    }

    /**
     * Create an instance of {@link CA04AmountAndCurrencyExchangeDetails3 }
     * 
     */
    public CA04AmountAndCurrencyExchangeDetails3 createCA04AmountAndCurrencyExchangeDetails3() {
        return new CA04AmountAndCurrencyExchangeDetails3();
    }

    /**
     * Create an instance of {@link CA04InterestRecord1 }
     * 
     */
    public CA04InterestRecord1 createCA04InterestRecord1() {
        return new CA04InterestRecord1();
    }

    /**
     * Create an instance of {@link CA04ReportEntry4 }
     * 
     */
    public CA04ReportEntry4 createCA04ReportEntry4() {
        return new CA04ReportEntry4();
    }

    /**
     * Create an instance of {@link CA04AmountAndCurrencyExchangeDetails4 }
     * 
     */
    public CA04AmountAndCurrencyExchangeDetails4 createCA04AmountAndCurrencyExchangeDetails4() {
        return new CA04AmountAndCurrencyExchangeDetails4();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CA04Document }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:iso:std:iso:20022:tech:xsd:camt.053.001.04", name = "Document")
    public JAXBElement<CA04Document> createDocument(CA04Document value) {
        return new JAXBElement<CA04Document>(_Document_QNAME, CA04Document.class, null, value);
    }

}
