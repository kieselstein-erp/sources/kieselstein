@XmlSchema(
        namespace = "http://www.opentrans.org/XMLSchema/1.0",
        elementFormDefault = XmlNsForm.QUALIFIED)
package com.lp.server.schema.opentrans.cc.orderresponse;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;