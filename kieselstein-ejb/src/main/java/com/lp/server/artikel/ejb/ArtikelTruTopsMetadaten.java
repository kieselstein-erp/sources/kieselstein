package com.lp.server.artikel.ejb;

import com.lp.server.system.service.ITablenames;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@NamedQueries({
	@NamedQuery(name = ArtikelTruTopsMetadatenQuery.ByArtikelTruTopsIId,
			query = "SELECT OBJECT(o) from ArtikelTruTopsMetadaten o WHERE o.artikelTruTopsIId = :artikelTruTopsId"),
	@NamedQuery(name = ArtikelTruTopsMetadatenQuery.ByArtikelIId,
		query = "SELECT OBJECT(o) from ArtikelTruTopsMetadaten o LEFT JOIN o.artikelTruTops a WHERE a.artikelIId = :artikelid ORDER BY o.iId")
})
@Entity
@Table(name = ITablenames.WW_ARTIKELTRUTOPSMETADATEN)
public class ArtikelTruTopsMetadaten implements Serializable {
	private static final long serialVersionUID = -5530350649006668145L;

	@Id
	@Column(name = "I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer iId;
	
	@Column(name = "ARTIKELTRUTOPS_I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer artikelTruTopsIId;
	
	@Column(name = "C_FILENAME", columnDefinition = "VARCHAR(1024)")
	private String cFilename;
	
	@Column(name = "I_SIZE", columnDefinition = "INTEGER")
	private Integer iSize;
	
	@Column(name = "T_CREATION", columnDefinition = "TIMESTAMP")
	private Timestamp tCreation;
	
	@Column(name = "T_MODIFICATION", columnDefinition = "TIMESTAMP")
	private Timestamp tModification;
	
	@Column(name = "C_HASH", columnDefinition = "VARCHAR(1024)")
	private String cHash;

	@Column(name = "C_FULL_PATH", columnDefinition = "VARCHAR(1024)")
	private String cFullPath;

	@Column(name = "T_AENDERN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAendern;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ARTIKELTRUTOPS_I_ID", referencedColumnName = "I_ID", insertable = false, updatable = false)
	private ArtikelTruTops artikelTruTops;
	
	public ArtikelTruTopsMetadaten() {
	}


	public Integer getIId() {
		return iId;
	}


	public void setIId(Integer iId) {
		this.iId = iId;
	}


	public Integer getArtikelTruTopsIId() {
		return artikelTruTopsIId;
	}


	public void setArtikelTruTopsIId(Integer artikelTruTopsIId) {
		this.artikelTruTopsIId = artikelTruTopsIId;
	}


	public String getCFilename() {
		return cFilename;
	}


	public void setCFilename(String cFilename) {
		this.cFilename = cFilename;
	}


	public Integer getISize() {
		return iSize;
	}


	public void setISize(Integer iSize) {
		this.iSize = iSize;
	}


	public Timestamp getTCreation() {
		return tCreation;
	}


	public void setTCreation(Timestamp tCreation) {
		this.tCreation = tCreation;
	}


	public Timestamp getTModification() {
		return tModification;
	}


	public void setTModification(Timestamp tModification) {
		this.tModification = tModification;
	}


	public String getCHash() {
		return cHash;
	}


	public void setCHash(String cHash) {
		this.cHash = cHash;
	}

	public String getFullPath() {
		return cFullPath;
	}

	public void setFullPath(String cFullPath) {
		this.cFullPath = cFullPath;
	}

	public Timestamp getAendern() {
		return tAendern;
	}

	public void setAendern(Timestamp tAendern) {
		this.tAendern = tAendern;
	}

}
