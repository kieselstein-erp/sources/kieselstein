package com.lp.server.artikel.service;

import com.lp.server.system.service.AbstractBaseDto;

import java.sql.Timestamp;

public class ArtikelTruTopsMetadatenDto extends AbstractBaseDto {
	private static final long serialVersionUID = 8865404923368336000L;

	private Integer artikelTruTopsIId;
	private String cFilename;
	private Integer iSize;
	private Timestamp tCreation;
	private Timestamp tModification;
	private String cHash;
	private String cFullPath;

	public ArtikelTruTopsMetadatenDto() {
	}

	public Integer getArtikelTruTopsIId() {
		return artikelTruTopsIId;
	}

	public void setArtikelTruTopsIId(Integer artikelTruTopsIId) {
		this.artikelTruTopsIId = artikelTruTopsIId;
	}

	public String getCFilename() {
		return cFilename;
	}

	public void setCFilename(String cFilename) {
		this.cFilename = cFilename;
	}

	public Integer getISize() {
		return iSize;
	}

	public void setISize(Integer iSize) {
		this.iSize = iSize;
	}

	public Timestamp getTCreation() {
		return tCreation;
	}

	public void setTCreation(Timestamp tCreation) {
		this.tCreation = tCreation;
	}

	public Timestamp getTModification() {
		return tModification;
	}

	public void setTModification(Timestamp tModification) {
		this.tModification = tModification;
	}

	public String getCHash() {
		return cHash;
	}

	public void setCHash(String cHash) {
		this.cHash = cHash;
	}

	public String getFullPath() {
		return cFullPath;
	}

	public void setFullPath(String cFullPath) {
		this.cFullPath = cFullPath;
	}

}
