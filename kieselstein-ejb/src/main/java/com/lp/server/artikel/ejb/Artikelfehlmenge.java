/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 * 
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 * 
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: developers@heliumv.com
 ******************************************************************************/
package com.lp.server.artikel.ejb;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
		@NamedQuery(
				name = "ArtikelfehlmengefindByBelegartCNrBelegartPositionIId",
				query = "SELECT OBJECT(o) FROM Artikelfehlmenge o WHERE o.cBelegartnr=:belegartCNr AND o.iBelegartpositionid=:belegartpositionIId"
		),
		@NamedQuery(
				name = "ArtikelfehlmengefindByBelegartCNrBelegartPositionIIdBuchungsart",
				query = "SELECT OBJECT(o) FROM Artikelfehlmenge o WHERE o.cBelegartnr=:belegartCNr AND o.iBelegartpositionid=:belegartpositionIId AND o.cBuchungsart = :buchungsart"
		),
		@NamedQuery(
				name = "ArtikelfehlmengeSumMengeByBelegartCNrBelegartPositionIId",
				query = "SELECT SUM(nMenge) FROM Artikelfehlmenge WHERE cBelegartnr = :belegartnr AND iBelegartpositionid = :belegartposid"
		),
		@NamedQuery(
				name = "ArtikelfehlmengeSumMengeByArtikelIdMandantCNr",
				query = "SELECT SUM(nMenge) FROM Artikelfehlmenge " +
						"WHERE artikelIId = :artikelId" +
						"  AND EXISTS(SELECT 69 FROM Los l, Lossollmaterial lsm WHERE l.id = lsm.losIId AND l.mandantCNr = :mandantCNr)"
		),
		@NamedQuery(
				name = "ArtikelfehlmengeSumMengeByArtikelIdMandantCNrPartnerIdStandort",
				query = "SELECT SUM(nMenge) FROM Artikelfehlmenge " +
						"WHERE artikelIId = :artikelId" +
						"  AND EXISTS(SELECT 69 FROM Los l, Lossollmaterial lsm, Loslagerentnahme lle, Lager la " +
						"             WHERE lle.losIId = l.id " +
						"               AND l.id = lsm.losIId " +
						"               AND la.id = lle.lagerIId" +
						"               AND l.mandantCNr = :mandantCNr " +
						"               AND la.partnerIIdStandort = :partnerIIdStandort )"
		),
		@NamedQuery(
				name = "ArtikelfehlmengeNurPositiveSumMengeByArtikelIdMandantCNr",
				query = "SELECT SUM(nMenge) FROM Artikelfehlmenge " +
						"WHERE artikelIId = :artikelId" +
						"  AND EXISTS(SELECT 69 FROM Los l, Lossollmaterial lsm WHERE l.id = lsm.losIId AND l.mandantCNr = :mandantCNr) " +
						"  AND nMenge > 0"
		),
		@NamedQuery(
				name = "ArtikelfehlmengeNurSumMengeByArtikelIdMandantCNrBuchungsart",
				query = "SELECT SUM(nMenge) FROM Artikelfehlmenge " +
						"WHERE artikelIId = :artikelId" +
						"  AND EXISTS(SELECT 69 FROM Los l, Lossollmaterial lsm WHERE l.id = lsm.losIId AND l.mandantCNr = :mandantCNr) " +
						"  AND cBuchungsart = :buchungsart"
		),
})
@Entity
@Table(name = "WW_ARTIKELFEHLMENGE")
public class Artikelfehlmenge implements Serializable {
	@Id
	@Column(name = "I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer iId;

	@Column(name = "C_BUCHUNGSART", columnDefinition = "VARCHAR(5) NOT NULL")
	private String cBuchungsart;

	@Column(name = "C_BELEGARTNR", columnDefinition = "CHAR(15) NOT NULL")
	private String cBelegartnr;

	@Column(name = "I_BELEGARTPOSITIONID", columnDefinition = "INTEGER NOT NULL")
	private Integer iBelegartpositionid;

	@Column(name = "N_MENGE", columnDefinition = "NUMERIC(17,6) NOT NULL")
	private BigDecimal nMenge;

	@Column(name = "T_LIEFERTERMIN", columnDefinition = "TIMESTAMP NOT NULL")
	private Date tLiefertermin;

	@Column(name = "ARTIKEL_I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer artikelIId;

	private static final long serialVersionUID = 1L;

	public Artikelfehlmenge() {
		super();
	}

	public Artikelfehlmenge(Integer id,
			String belegartnr,
			String buchungsart,
			Integer belegartpositionid,
			Integer artikelIId,
			BigDecimal menge,
			Date liefertermin
	) {
		setIId(id);
		setCBuchungsart(buchungsart);
		setCBelegartnr(belegartnr);
		setIBelegartpositionid(belegartpositionid);
		setArtikelIId(artikelIId);
		setNMenge(menge);
		setTLiefertermin(liefertermin);
	}
	

	public Integer getIId() {
		return this.iId;
	}

	public void setIId(Integer iId) {
		this.iId = iId;
	}

	public String getCBuchungsart() {
		return this.cBuchungsart;
	}

	public void setCBuchungsart(String cBuchungsart) {
		this.cBuchungsart = cBuchungsart;
	}

	public String getCBelegartnr() {
		return this.cBelegartnr;
	}

	public void setCBelegartnr(String cBelegartnr) {
		this.cBelegartnr = cBelegartnr;
	}

	public Integer getIBelegartpositionid() {
		return this.iBelegartpositionid;
	}

	public void setIBelegartpositionid(Integer iBelegartpositionid) {
		this.iBelegartpositionid = iBelegartpositionid;
	}

	public BigDecimal getNMenge() {
		return this.nMenge;
	}

	public void setNMenge(BigDecimal nMenge) {
		this.nMenge = nMenge;
	}

	public Date getTLiefertermin() {
		return this.tLiefertermin;
	}

	public void setTLiefertermin(Date tLiefertermin) {
		this.tLiefertermin = tLiefertermin;
	}

	public Integer getArtikelIId() {
		return this.artikelIId;
	}

	public void setArtikelIId(Integer artikelIId) {
		this.artikelIId = artikelIId;
	}

}
