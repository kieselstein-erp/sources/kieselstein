/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 *
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 *
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact: developers@heliumv.com
 ******************************************************************************/
package com.lp.server.artikel.service;

import com.lp.server.artikel.ejb.Artgru;
import com.lp.server.artikel.ejb.Artkla;
import com.lp.server.artikel.ejb.Material;
import com.lp.server.artikel.ejb.Shopgruppe;
import com.lp.server.partner.ejb.Lfliefergruppe;
import com.lp.server.system.service.HvDtoLogClass;
import com.lp.server.system.service.HvDtoLogComplex;
import com.lp.server.system.service.HvDtoLogIdCnr;
import com.lp.server.system.service.HvDtoLogIgnore;
import com.lp.server.util.IIId;
import com.lp.util.Helper;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@HvDtoLogClass(name = HvDtoLogClass.ARTIKEL)
public class ArtikelDto implements Serializable, IIId {
    private static final long serialVersionUID = 1L;

    private Integer iId;
    private String cNr;
    private Integer herstellerIId;
    private Integer artgruIId;
    private Integer artklaIId;
    private Integer laseroberflaecheIId;
    private BigDecimal nPreisZugehoerigerartikel;
    private Integer waffenkaliberIId;
    private Integer waffentypIId;
    private Integer waffentypFeinIId;
    private Integer waffenkategorieIId;
    private Integer waffenzusatzIId;
    private Integer waffenausfuehrungIId;
    private Short bMeldepflichtig;
    private Short bBewilligungspflichtig;
    private String cFreigabeZuerueckgenommen;
    private Integer personalIIdFreigabe;
    private Timestamp tFreigabe;
    private Short bMultiplikatorInvers;
    private Short bMultiplikatorAufrunden;
    private Short bBevorzugt;
    private Integer iPassiveReisezeit;
    private Integer iLaengeminSnrchnr;
    private Integer iLaengemaxSnrchnr;
    private Integer iExternerArbeitsgang;
    private BigDecimal nMindestverkaufsmenge;
    private BigDecimal nVerpackungsmittelmenge;
    private Integer verpackungsmittelIId;
    private Short bKeineLagerzubuchung;
    private Short bRahmenartikel;
    private Double fFertigungsVpe;
    private String cEccn;
    private Double fMultiplikatorZugehoerigerartikel;
    private Short bAzinabnachkalk;
    private Double fUeberproduktion;
    private Short bKalkulatorisch;
    private Integer personalIIdLetztewartung;
    private Timestamp tLetztewartung;
    private Integer lfliefergruppeIId;
    private Double fDetailprozentmindeststand;
    private Short bKommissionieren;
    private Short bBestellmengeneinheitInvers;
    private Integer shopgruppeIId;
    private Short bReinemannzeit;
    private Short bNurzurinfo;
    private Short bWerbeabgabepflichtig;
    private Short bVerleih;
    private String artikelartCNr;
    private String mandantCNr;
    private Short bDokumentenpflicht;
    private String cRevision;
    private String cIndex;
    private Short bSeriennrtragend;
    private Short bLagerbewertet;
    private Timestamp tAnlegen;
    private Integer farbcodeIId;
    private BigDecimal nUmrechnungsfaktor;
    private Integer personalIIdAnlegen;
    private Timestamp tAendern;
    private Integer personalIIdAendern;
    private String einheitCNrBestellung;
    private String cArtikelbezhersteller;
    private String cArtikelnrhersteller;
    private String einheitCNr;
    private String cReferenznr;
    private Double fLagermindest;
    private Double fLagersoll;
    private Double fFertigungssatzgroesse;
    private Short bLagerbewirtschaftet;
    private Double fVerpackungsmenge;
    private Double fVerschnittfaktor;
    private Double fVerschnittbasis;
    private Double fJahresmenge;
    private Integer mwstsatzbezIId;
    private Integer materialIId;
    private Double fGewichtkg;
    private Double fMaterialgewicht;
    private Short bAntistatic;
    private Integer artikelIIdZugehoerig;
    private Double fVertreterprovisionmax;
    private Double fMinutenfaktor1;
    private Double fMinutenfaktor2;
    private Double fMindestdeckungsbeitrag;
    private String cVerkaufseannr;
    private String cWarenverkehrsnummer;
    private Short bRabattierbar;
    private Integer iGarantiezeit;
    private SollverkaufDto sollverkaufDto;
    private GeometrieDto geometrieDto;
    private VerpackungDto verpackungDto;
    private MontageDto montageDto;
    private ArtikelsprDto artikelsprDto;
    private ArtgruDto artgruDto;
    private ArtklaDto artklaDto;
    private Short bChargennrtragend;
    private HerstellerDto herstellerDto;
    private Short bVersteckt;
    private String cVerpackungseannr;
    private Integer artikelIIdErsatz;
    private Integer landIIdUrsprungsland;
    private Integer iWartungsintervall;
    private Integer iSofortverbrauch;
    private Short bVkpreispflichtig;
    private Short bWepinfoAnAnforderer;
    private Short bSummeInBestellung;
    private Double fAufschlagProzent;
    private BigDecimal nAufschlagBetrag;
    private BigDecimal nVerschnittmenge;
    private Double fStromverbrauchtyp;
    private Double fStromverbrauchmax;
    private Double fMaxfertigungssatzgroesse;
    private Integer vorzugIId;
    private String cUL;
    private Integer reachIId;
    private Integer rohsIId;
    private Integer automotiveIId;
    private Integer medicalIId;
    private Integer referenceArticleId;

    public Integer getReferenceArticleId() {
        return referenceArticleId;
    }

    public void setReferenceArticleId(Integer referenceArticleId) {
        this.referenceArticleId = referenceArticleId;
    }

    public Integer getLaseroberflaecheIId() {
        return laseroberflaecheIId;
    }

    public void setLaseroberflaecheIId(Integer laseroberflaecheIId) {
        this.laseroberflaecheIId = laseroberflaecheIId;
    }

    public BigDecimal getNPreisZugehoerigerartikel() {
        return nPreisZugehoerigerartikel;
    }

    public void setNPreisZugehoerigerartikel(BigDecimal nPreisZugehoerigerartikel) {
        this.nPreisZugehoerigerartikel = nPreisZugehoerigerartikel;
    }

    public Integer getWaffenkaliberIId() {
        return waffenkaliberIId;
    }

    public void setWaffenkaliberIId(Integer waffenkaliberIId) {
        this.waffenkaliberIId = waffenkaliberIId;
    }

    public Integer getWaffentypIId() {
        return waffentypIId;
    }

    public void setWaffentypIId(Integer waffentypIId) {
        this.waffentypIId = waffentypIId;
    }

    public Integer getWaffentypFeinIId() {
        return waffentypFeinIId;
    }

    public void setWaffentypFeinIId(Integer waffentypFeinIId) {
        this.waffentypFeinIId = waffentypFeinIId;
    }

    public Integer getWaffenkategorieIId() {
        return waffenkategorieIId;
    }

    public void setWaffenkategorieIId(Integer waffenkategorieIId) {
        this.waffenkategorieIId = waffenkategorieIId;
    }

    public Integer getWaffenzusatzIId() {
        return waffenzusatzIId;
    }

    public void setWaffenzusatzIId(Integer waffenzusatzIId) {
        this.waffenzusatzIId = waffenzusatzIId;
    }

    public Integer getWaffenausfuehrungIId() {
        return waffenausfuehrungIId;
    }

    public void setWaffenausfuehrungIId(Integer waffenausfuehrungIId) {
        this.waffenausfuehrungIId = waffenausfuehrungIId;
    }

    public Short getBMeldepflichtig() {
        return bMeldepflichtig;
    }

    public void setBMeldepflichtig(Short bMeldepflichtig) {
        this.bMeldepflichtig = bMeldepflichtig;
    }

    public Short getBBewilligungspflichtig() {
        return bBewilligungspflichtig;
    }

    public void setBBewilligungspflichtig(Short bBewilligungspflichtig) {
        this.bBewilligungspflichtig = bBewilligungspflichtig;
    }

    public String getCFreigabeZuerueckgenommen() {
        return cFreigabeZuerueckgenommen;
    }

    public void setCFreigabeZuerueckgenommen(String cFreigabeZuerueckgenommen) {
        this.cFreigabeZuerueckgenommen = cFreigabeZuerueckgenommen;
    }

    public Integer getPersonalIIdFreigabe() {
        return personalIIdFreigabe;
    }

    public void setPersonalIIdFreigabe(Integer personalIIdFreigabe) {
        this.personalIIdFreigabe = personalIIdFreigabe;
    }

    public Timestamp getTFreigabe() {
        return tFreigabe;
    }

    public void setTFreigabe(Timestamp tFreigabe) {
        this.tFreigabe = tFreigabe;
    }

    public Short getBMultiplikatorInvers() {
        return bMultiplikatorInvers;
    }

    public void setBMultiplikatorInvers(Short bMultiplikatorInvers) {
        this.bMultiplikatorInvers = bMultiplikatorInvers;
    }

    public Short getBMultiplikatorAufrunden() {
        return bMultiplikatorAufrunden;
    }

    public void setBMultiplikatorAufrunden(Short bMultiplikatorAufrunden) {
        this.bMultiplikatorAufrunden = bMultiplikatorAufrunden;
    }

    public Short getBBevorzugt() {
        return bBevorzugt;
    }

    public void setBBevorzugt(Short bBevorzugt) {
        this.bBevorzugt = bBevorzugt;
    }

    public Integer getIPassiveReisezeit() {
        return iPassiveReisezeit;
    }

    public void setIPassiveReisezeit(Integer iPassiveReisezeit) {
        this.iPassiveReisezeit = iPassiveReisezeit;
    }

    public Integer getILaengeminSnrchnr() {
        return iLaengeminSnrchnr;
    }

    public void setILaengeminSnrchnr(Integer iLaengeminSnrchnr) {
        this.iLaengeminSnrchnr = iLaengeminSnrchnr;
    }

    public Integer getILaengemaxSnrchnr() {
        return iLaengemaxSnrchnr;
    }

    public void setILaengemaxSnrchnr(Integer iLaengemaxSnrchnr) {
        this.iLaengemaxSnrchnr = iLaengemaxSnrchnr;
    }

    public Integer getIExternerArbeitsgang() {
        return iExternerArbeitsgang;
    }

    public void setIExternerArbeitsgang(Integer iExternerArbeitsgang) {
        this.iExternerArbeitsgang = iExternerArbeitsgang;
    }

    public BigDecimal getNMindestverkaufsmenge() {
        return nMindestverkaufsmenge;
    }

    public void setNMindestverkaufsmenge(BigDecimal nMindestverkaufsmenge) {
        this.nMindestverkaufsmenge = nMindestverkaufsmenge;
    }

    public BigDecimal getNVerpackungsmittelmenge() {
        return nVerpackungsmittelmenge;
    }

    public void setNVerpackungsmittelmenge(BigDecimal nVerpackungsmittelmenge) {
        this.nVerpackungsmittelmenge = nVerpackungsmittelmenge;
    }

    public Integer getVerpackungsmittelIId() {
        return verpackungsmittelIId;
    }

    public void setVerpackungsmittelIId(Integer verpackungsmittelIId) {
        this.verpackungsmittelIId = verpackungsmittelIId;
    }

    public Short getBKeineLagerzubuchung() {
        return bKeineLagerzubuchung;
    }

    public void setBKeineLagerzubuchung(Short bKeineLagerzubuchung) {
        this.bKeineLagerzubuchung = bKeineLagerzubuchung;
    }

    public Short getBRahmenartikel() {
        return bRahmenartikel;
    }

    public void setBRahmenartikel(Short bRahmenartikel) {
        this.bRahmenartikel = bRahmenartikel;
    }

    public Double getFFertigungsVpe() {
        return fFertigungsVpe;
    }

    public void setFFertigungsVpe(Double fFertigungsVpe) {
        this.fFertigungsVpe = fFertigungsVpe;
    }

    public String getCEccn() {
        return cEccn;
    }

    public void setCEccn(String cEccn) {
        this.cEccn = cEccn;
    }

    public Double getFMultiplikatorZugehoerigerartikel() {
        return fMultiplikatorZugehoerigerartikel;
    }

    public void setFMultiplikatorZugehoerigerartikel(Double fMultiplikatorZugehoerigerartikel) {
        this.fMultiplikatorZugehoerigerartikel = fMultiplikatorZugehoerigerartikel;
    }

    public Short getBAzinabnachkalk() {
        return bAzinabnachkalk;
    }

    public void setBAzinabnachkalk(Short bAzinabnachkalk) {
        this.bAzinabnachkalk = bAzinabnachkalk;
    }

    public Double getFUeberproduktion() {
        return fUeberproduktion;
    }

    public void setFUeberproduktion(Double fUeberproduktion) {
        this.fUeberproduktion = fUeberproduktion;
    }

    public Short getBKalkulatorisch() {
        return bKalkulatorisch;
    }

    public void setBKalkulatorisch(Short bKalkulatorisch) {
        this.bKalkulatorisch = bKalkulatorisch;
    }

    public Timestamp getTLetztewartung() {
        return tLetztewartung;
    }

    public void setTLetztewartung(Timestamp tLetztewartung) {
        this.tLetztewartung = tLetztewartung;
    }

    public Integer getPersonalIIdLetztewartung() {
        return personalIIdLetztewartung;
    }

    public void setPersonalIIdLetztewartung(Integer personalIIdLetztewartung) {
        this.personalIIdLetztewartung = personalIIdLetztewartung;
    }

    @HvDtoLogIdCnr(entityClass = Lfliefergruppe.class)
    public Integer getLfliefergruppeIId() {
        return lfliefergruppeIId;
    }

    public void setLfliefergruppeIId(Integer lfliefergruppeIId) {
        this.lfliefergruppeIId = lfliefergruppeIId;
    }

    public Double getFDetailprozentmindeststand() {
        return fDetailprozentmindeststand;
    }

    public void setFDetailprozentmindeststand(Double fDetailprozentmindeststand) {
        this.fDetailprozentmindeststand = fDetailprozentmindeststand;
    }

    public Short getBKommissionieren() {
        return bKommissionieren;
    }

    public void setBKommissionieren(Short bKommissionieren) {
        this.bKommissionieren = bKommissionieren;
    }

    public Short getbBestellmengeneinheitInvers() {
        return bBestellmengeneinheitInvers;
    }

    public void setbBestellmengeneinheitInvers(Short bBestellmengeneinheitInvers) {
        this.bBestellmengeneinheitInvers = bBestellmengeneinheitInvers;
    }

    @HvDtoLogIdCnr(entityClass = Shopgruppe.class)
    public Integer getShopgruppeIId() {
        return shopgruppeIId;
    }

    public void setShopgruppeIId(Integer shopgruppeIId) {
        this.shopgruppeIId = shopgruppeIId;
    }

    public Short getbReinemannzeit() {
        return bReinemannzeit;
    }

    public void setbReinemannzeit(Short bReinemannzeit) {
        this.bReinemannzeit = bReinemannzeit;
    }

    public Short getbNurzurinfo() {
        return bNurzurinfo;
    }

    public void setbNurzurinfo(Short bNurzurinfo) {
        this.bNurzurinfo = bNurzurinfo;
    }

    public Short getBWerbeabgabepflichtig() {
        return bWerbeabgabepflichtig;
    }

    public void setBWerbeabgabepflichtig(Short bWerbeabgabepflichtig) {
        this.bWerbeabgabepflichtig = bWerbeabgabepflichtig;
    }

    public Short getBVerleih() {
        return bVerleih;
    }

    public void setBVerleih(Short bVerleih) {
        this.bVerleih = bVerleih;
    }

    public String getCRevision() {
        return cRevision;
    }

    public void setCRevision(String revision) {
        cRevision = revision;
    }

    public String getCIndex() {
        return cIndex;
    }

    public void setCIndex(String index) {
        cIndex = index;
    }

    public Short getBDokumentenpflicht() {
        return bDokumentenpflicht;
    }

    public void setBDokumentenpflicht(Short dokumentenpflicht) {
        bDokumentenpflicht = dokumentenpflicht;
    }

    public Short getBVkpreispflichtig() {
        return bVkpreispflichtig;
    }

    public void setBVkpreispflichtig(Short bVkpreispflichtig) {
        this.bVkpreispflichtig = bVkpreispflichtig;
    }

    public Short getBWepinfoAnAnforderer() {
        return bWepinfoAnAnforderer;
    }

    public void setBWepinfoAnAnforderer(Short bWepinfoAnAnforderer) {
        this.bWepinfoAnAnforderer = bWepinfoAnAnforderer;
    }

    public Short getBSummeInBestellung() {
        return bSummeInBestellung;
    }

    public void setBSummeInBestellung(Short bSummeInBestellung) {
        this.bSummeInBestellung = bSummeInBestellung;
    }

    public Double getFAufschlagProzent() {
        return fAufschlagProzent;
    }

    public void setFAufschlagProzent(Double fAufschlagProzent) {
        this.fAufschlagProzent = fAufschlagProzent;
    }

    public BigDecimal getNVerschnittmenge() {
        return nVerschnittmenge;
    }

    public void setNVerschnittmenge(BigDecimal nVerschnittmenge) {
        this.nVerschnittmenge = nVerschnittmenge;
    }

    public BigDecimal getNAufschlagBetrag() {
        return nAufschlagBetrag;
    }

    public void setNAufschlagBetrag(BigDecimal nAufschlagBetrag) {
        this.nAufschlagBetrag = nAufschlagBetrag;
    }

    public Double getFStromverbrauchtyp() {
        return fStromverbrauchtyp;
    }

    public void setFStromverbrauchtyp(Double fStromverbrauchtyp) {
        this.fStromverbrauchtyp = fStromverbrauchtyp;
    }

    public Double getFStromverbrauchmax() {
        return fStromverbrauchmax;
    }

    public void setFStromverbrauchmax(Double fStromverbrauchmax) {
        this.fStromverbrauchmax = fStromverbrauchmax;
    }

    public Integer getISofortverbrauch() {
        return iSofortverbrauch;
    }

    public void setISofortverbrauch(Integer sofortverbrauch) {
        iSofortverbrauch = sofortverbrauch;
    }

    public Integer getIWartungsintervall() {
        return iWartungsintervall;
    }

    public void setIWartungsintervall(Integer wartungsintervall) {
        iWartungsintervall = wartungsintervall;
    }

    public Integer getIId() {
        return iId;
    }

    public void setIId(Integer iId) {
        this.iId = iId;
    }

    public String getCNr() {
        return cNr;
    }

    public void setCNr(String cNr) {
        this.cNr = cNr;
    }

    public String getArtikelartCNr() {
        return artikelartCNr;
    }

    public void setArtikelartCNr(String artikelartCNr) {
        this.artikelartCNr = artikelartCNr;
    }

    public Timestamp getTAnlegen() {
        return tAnlegen;
    }

    public void setTAnlegen(Timestamp tAnlegen) {
        this.tAnlegen = tAnlegen;
    }

    public Integer getPersonalIIdAnlegen() {
        return personalIIdAnlegen;
    }

    public void setPersonalIIdAnlegen(Integer personalIIdAnlegen) {
        this.personalIIdAnlegen = personalIIdAnlegen;
    }

    @HvDtoLogIgnore
    public Timestamp getTAendern() {
        return tAendern;
    }

    public void setTAendern(Timestamp tAendern) {
        this.tAendern = tAendern;
    }

    public Integer getPersonalIIdAendern() {
        return personalIIdAendern;
    }

    public void setPersonalIIdAendern(Integer personalIIdAendern) {
        this.personalIIdAendern = personalIIdAendern;
    }

    public String getCArtikelbezhersteller() {
        return cArtikelbezhersteller;
    }

    public void setCArtikelbezhersteller(String cArtikelbezhersteller) {
        this.cArtikelbezhersteller = cArtikelbezhersteller;
    }

    public String getCArtikelnrhersteller() {
        return cArtikelnrhersteller;
    }

    public void setCArtikelnrhersteller(String cArtikelnrhersteller) {
        this.cArtikelnrhersteller = cArtikelnrhersteller;
    }

    public String getEinheitCNr() {
        return einheitCNr;
    }

    public void setEinheitCNr(String einheitCNr) {
        this.einheitCNr = einheitCNr;
    }

    public String getCReferenznr() {
        return cReferenznr;
    }

    public void setCReferenznr(String cReferenznr) {
        this.cReferenznr = cReferenznr;
    }

    public Double getFLagermindest() {
        return fLagermindest;
    }

    public void setFLagermindest(Double fLagermindest) {
        this.fLagermindest = fLagermindest;
    }

    public Double getFLagersoll() {
        return fLagersoll;
    }

    public void setFLagersoll(Double fLagersoll) {
        this.fLagersoll = fLagersoll;
    }

    public Double getFFertigungssatzgroesse() {
        return this.fFertigungssatzgroesse;
    }

    public void setFFertigungssatzgroesse(Double fFertigungssatzgroesse) {
        this.fFertigungssatzgroesse = fFertigungssatzgroesse;
    }

    public Double getFMaxfertigungssatzgroesse() {
        return fMaxfertigungssatzgroesse;
    }

    public void setFMaxfertigungssatzgroesse(Double fMaxfertigungssatzgroesse) {
        this.fMaxfertigungssatzgroesse = fMaxfertigungssatzgroesse;
    }

    public Double getFVerpackungsmenge() {
        return fVerpackungsmenge;
    }

    public void setFVerpackungsmenge(Double fVerpackungsmenge) {
        this.fVerpackungsmenge = fVerpackungsmenge;
    }

    public Double getFVerschnittfaktor() {
        return fVerschnittfaktor;
    }

    public void setFVerschnittfaktor(Double fVerschnittfaktor) {
        this.fVerschnittfaktor = fVerschnittfaktor;
    }

    public Double getFVerschnittbasis() {
        return fVerschnittbasis;
    }

    public void setFVerschnittbasis(Double fVerschnittbasis) {
        this.fVerschnittbasis = fVerschnittbasis;
    }

    public Double getFJahresmenge() {
        return fJahresmenge;
    }

    public void setFJahresmenge(Double fJahresmenge) {
        this.fJahresmenge = fJahresmenge;
    }

    public Integer getMwstsatzbezIId() {
        return mwstsatzbezIId;
    }

    public void setMwstsatzbezIId(Integer mwstsatzbezIId) {
        this.mwstsatzbezIId = mwstsatzbezIId;
    }

    public Double getFGewichtkg() {
        return fGewichtkg;
    }

    public void setFGewichtkg(Double fGewichtkg) {
        this.fGewichtkg = fGewichtkg;
    }

    public Double getFMaterialgewicht() {
        return fMaterialgewicht;
    }

    public void setFMaterialgewicht(Double fMaterialgewicht) {
        this.fMaterialgewicht = fMaterialgewicht;
    }

    public Short getBAntistatic() {
        return bAntistatic;
    }

    public void setBAntistatic(Short bAntistatic) {
        this.bAntistatic = bAntistatic;
    }

    public Integer getArtikelIIdZugehoerig() {
        return artikelIIdZugehoerig;
    }

    public void setArtikelIIdZugehoerig(Integer artikelIIdZugehoerig) {
        this.artikelIIdZugehoerig = artikelIIdZugehoerig;
    }

    public Double getFVertreterprovisionmax() {
        return fVertreterprovisionmax;
    }

    public void setFVertreterprovisionmax(Double fVertreterprovisionmax) {
        this.fVertreterprovisionmax = fVertreterprovisionmax;
    }

    public Double getFMinutenfaktor1() {
        return fMinutenfaktor1;
    }

    public void setFMinutenfaktor1(Double fMinutenfaktor1) {
        this.fMinutenfaktor1 = fMinutenfaktor1;
    }

    public Double getFMinutenfaktor2() {
        return fMinutenfaktor2;
    }

    public void setFMinutenfaktor2(Double fMinutenfaktor2) {
        this.fMinutenfaktor2 = fMinutenfaktor2;
    }

    public Double getFMindestdeckungsbeitrag() {
        return fMindestdeckungsbeitrag;
    }

    public void setFMindestdeckungsbeitrag(Double fMindestdeckungsbeitrag) {
        this.fMindestdeckungsbeitrag = fMindestdeckungsbeitrag;
    }

    public String getCVerkaufseannr() {
        return cVerkaufseannr;
    }

    public void setCVerkaufseannr(String cVerkaufseannr) {
        this.cVerkaufseannr = cVerkaufseannr;
    }

    public String getCWarenverkehrsnummer() {
        return cWarenverkehrsnummer;
    }

    public void setCWarenverkehrsnummer(String cWarenverkehrsnummer) {
        this.cWarenverkehrsnummer = cWarenverkehrsnummer;
    }

    public Short getBRabattierbar() {
        return bRabattierbar;
    }

    public void setBRabattierbar(Short bRabattierbar) {
        this.bRabattierbar = bRabattierbar;
    }

    public Integer getIGarantiezeit() {
        return iGarantiezeit;
    }

    public void setIGarantiezeit(Integer iGarantiezeit) {
        this.iGarantiezeit = iGarantiezeit;
    }

    @HvDtoLogComplex
    public ArtikelsprDto getArtikelsprDto() {
        return artikelsprDto;
    }

    public void setArtikelsprDto(ArtikelsprDto artikelsprDto) {
        this.artikelsprDto = artikelsprDto;
    }

    @HvDtoLogIgnore
    public MontageDto getMontageDto() {
        return montageDto;
    }

    public void setMontageDto(MontageDto montageDto) {
        this.montageDto = montageDto;
    }

    @HvDtoLogIgnore
    public VerpackungDto getVerpackungDto() {
        return verpackungDto;
    }

    public void setVerpackungDto(VerpackungDto verpackungDto) {
        this.verpackungDto = verpackungDto;
    }

    @HvDtoLogIgnore
    public GeometrieDto getGeometrieDto() {
        return geometrieDto;
    }

    public void setGeometrieDto(GeometrieDto geometrieDto) {
        this.geometrieDto = geometrieDto;
    }

    @HvDtoLogIgnore
    public SollverkaufDto getSollverkaufDto() {
        return sollverkaufDto;
    }

    public void setSollverkaufDto(SollverkaufDto sollverkaufDto) {
        this.sollverkaufDto = sollverkaufDto;
    }

    @HvDtoLogIgnore
    public ArtgruDto getArtgruDto() {
        return artgruDto;
    }

    public void setArtgruDto(ArtgruDto artgruDto) {
        this.artgruDto = artgruDto;
    }

    @HvDtoLogIgnore
    public ArtklaDto getArtklaDto() {
        return artklaDto;
    }

    public void setArtklaDto(ArtklaDto artklaDto) {
        this.artklaDto = artklaDto;
    }

    public Short getBChargennrtragend() {
        return bChargennrtragend;
    }

    public void setBChargennrtragend(Short bChargennrtragend) {
        this.bChargennrtragend = bChargennrtragend;
    }

    public boolean isChargennrtragend() {
        if (null == bChargennrtragend) return false;
        return bChargennrtragend > 0;
    }

    public Short getBSeriennrtragend() {
        return bSeriennrtragend;
    }

    public void setBSeriennrtragend(Short bSeriennrtragend) {
        this.bSeriennrtragend = bSeriennrtragend;
    }

    public boolean isSeriennrtragend() {
        if (null == bSeriennrtragend) return false;
        return bSeriennrtragend > 0;
    }

    public Integer getHerstellerIId() {
        return herstellerIId;
    }

    public void setHerstellerIId(Integer herstellerIId) {
        this.herstellerIId = herstellerIId;
    }

    @HvDtoLogIdCnr(entityClass = Artkla.class)
    public Integer getArtklaIId() {
        return artklaIId;
    }

    public void setArtklaIId(Integer artklaIId) {
        this.artklaIId = artklaIId;
    }

    @HvDtoLogIdCnr(entityClass = Artgru.class)
    public Integer getArtgruIId() {
        return artgruIId;
    }

    public void setArtgruIId(Integer artgruIId) {
        this.artgruIId = artgruIId;
    }

    @HvDtoLogIdCnr(entityClass = Material.class)
    public Integer getMaterialIId() {
        return materialIId;
    }

    public void setMaterialIId(Integer materialIId) {
        this.materialIId = materialIId;
    }

    @HvDtoLogIgnore
    public HerstellerDto getHerstellerDto() {
        return herstellerDto;
    }

    public void setHerstellerDto(HerstellerDto herstellerDto) {
        this.herstellerDto = herstellerDto;
    }

    public Short getBLagerbewertet() {
        return bLagerbewertet;
    }

    public void setBLagerbewertet(Short bLagerbewertet) {
        this.bLagerbewertet = bLagerbewertet;
    }

    public Short getBLagerbewirtschaftet() {
        return bLagerbewirtschaftet;
    }

    public void setBLagerbewirtschaftet(Short bLagerbewirtschaftet) {
        this.bLagerbewirtschaftet = bLagerbewirtschaftet;
    }

    public boolean isLagerbewirtschaftet() {
        if (null == bLagerbewirtschaftet) return false;
        return bLagerbewirtschaftet > 0;
    }

    public String getMandantCNr() {
        return mandantCNr;
    }

    public void setMandantCNr(String mandantCNr) {
        this.mandantCNr = mandantCNr;
    }

    public Integer getFarbcodeIId() {
        return farbcodeIId;
    }

    public void setFarbcodeIId(Integer farbcodeIId) {
        this.farbcodeIId = farbcodeIId;
    }

    public String getEinheitCNrBestellung() {
        return einheitCNrBestellung;
    }

    public void setEinheitCNrBestellung(String einheitCNrBestellung) {
        this.einheitCNrBestellung = einheitCNrBestellung;
    }

    public BigDecimal getNUmrechnungsfaktor() {
        return nUmrechnungsfaktor;
    }

    public void setNUmrechnungsfaktor(BigDecimal nUmrechnungsfaktor) {
        this.nUmrechnungsfaktor = nUmrechnungsfaktor;
    }

    public Short getBVersteckt() {
        return bVersteckt;
    }

    public void setBVersteckt(Short bVersteckt) {
        this.bVersteckt = bVersteckt;
    }

    public String getCVerpackungseannr() {
        return cVerpackungseannr;
    }

    public void setCVerpackungseannr(String cVerpackungseannr) {
        this.cVerpackungseannr = cVerpackungseannr;
    }

    public Integer getArtikelIIdErsatz() {
        return artikelIIdErsatz;
    }

    public void setArtikelIIdErsatz(Integer artikelIIdErsatz) {
        this.artikelIIdErsatz = artikelIIdErsatz;
    }

    public Integer getLandIIdUrsprungsland() {
        return landIIdUrsprungsland;
    }

    public void setLandIIdUrsprungsland(Integer landIIdUrsprungsland) {
        this.landIIdUrsprungsland = landIIdUrsprungsland;
    }

    public Integer getVorzugIId() {
        return vorzugIId;
    }

    public void setVorzugIId(Integer vorzugIId) {
        this.vorzugIId = vorzugIId;
    }

    public String getCUL() {
        return cUL;
    }

    public void setCUL(String cUL) {
        this.cUL = cUL;
    }

    public Integer getReachIId() {
        return reachIId;
    }

    public void setReachIId(Integer reachIId) {
        this.reachIId = reachIId;
    }

    public Integer getRohsIId() {
        return rohsIId;
    }

    public void setRohsIId(Integer rohsIId) {
        this.rohsIId = rohsIId;
    }

    public Integer getAutomotiveIId() {
        return automotiveIId;
    }

    public void setAutomotiveIId(Integer automotiveIId) {
        this.automotiveIId = automotiveIId;
    }

    public Integer getMedicalIId() {
        return medicalIId;
    }

    public void setMedicalIId(Integer medicalIId) {
        this.medicalIId = medicalIId;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ArtikelDto)) {
            return false;
        }
        ArtikelDto that = (ArtikelDto) obj;
        if (!(Objects.equals(that.iId, this.iId))) {
            return false;
        }
        if (!(Objects.equals(that.cNr, this.cNr))) {
            return false;
        }
        if (!(Objects.equals(that.herstellerIId, this.herstellerIId))) {
            return false;
        }
        if (!(Objects.equals(that.cArtikelbezhersteller, this.cArtikelbezhersteller))) {
            return false;
        }
        if (!(Objects.equals(that.cArtikelnrhersteller, this.cArtikelnrhersteller))) {
            return false;
        }
        if (!(Objects.equals(that.artgruIId, this.artgruIId))) {
            return false;
        }
        if (!(Objects.equals(that.artklaIId, this.artklaIId))) {
            return false;
        }
        if (!(Objects.equals(that.artikelartCNr, this.artikelartCNr))) {
            return false;
        }
        if (!(Objects.equals(that.einheitCNr, this.einheitCNr))) {
            return false;
        }
        if (!(Objects.equals(that.bSeriennrtragend, this.bSeriennrtragend))) {
            return false;
        }
        if (!(Objects.equals(that.cReferenznr, this.cReferenznr))) {
            return false;
        }
        if (!(Objects.equals(that.fLagermindest, this.fLagermindest))) {
            return false;
        }
        if (!(Objects.equals(that.fLagersoll, this.fLagersoll))) {
            return false;
        }
        if (!(Objects.equals(that.fVerpackungsmenge, this.fVerpackungsmenge))) {
            return false;
        }
        if (!(Objects.equals(that.fVerschnittfaktor, this.fVerschnittfaktor))) {
            return false;
        }
        if (!(Objects.equals(that.fVerschnittbasis, this.fVerschnittbasis))) {
            return false;
        }
        if (!(Objects.equals(that.fJahresmenge, this.fJahresmenge))) {
            return false;
        }
        if (!(Objects.equals(that.mwstsatzbezIId, this.mwstsatzbezIId))) {
            return false;
        }
        if (!(Objects.equals(that.materialIId, this.materialIId))) {
            return false;
        }
        if (!(Objects.equals(that.fGewichtkg, this.fGewichtkg))) {
            return false;
        }
        if (!(Objects.equals(that.fMaterialgewicht, this.fMaterialgewicht))) {
            return false;
        }
        if (!(Objects.equals(that.bAntistatic, this.bAntistatic))) {
            return false;
        }
        if (!(Objects.equals(that.artikelIIdZugehoerig, this.artikelIIdZugehoerig))) {
            return false;
        }
        if (!(Objects.equals(that.fVertreterprovisionmax, this.fVertreterprovisionmax))) {
            return false;
        }
        if (!(Objects.equals(that.fMinutenfaktor1, this.fMinutenfaktor1))) {
            return false;
        }
        if (!(Objects.equals(that.fMinutenfaktor2, this.fMinutenfaktor2))) {
            return false;
        }
        if (!(Objects.equals(that.fMindestdeckungsbeitrag, this.fMindestdeckungsbeitrag))) {
            return false;
        }
        if (!(Objects.equals(that.cVerkaufseannr, this.cVerkaufseannr))) {
            return false;
        }
        if (!(Objects.equals(that.cWarenverkehrsnummer, this.cWarenverkehrsnummer))) {
            return false;
        }
        if (!(Objects.equals(that.bRabattierbar, this.bRabattierbar))) {
            return false;
        }
        if (!(Objects.equals(that.iGarantiezeit, this.iGarantiezeit))) {
            return false;
        }
        if (!(Objects.equals(that.tAnlegen, this.tAnlegen))) {
            return false;
        }
        if (!(Objects.equals(that.personalIIdAnlegen, this.personalIIdAnlegen))) {
            return false;
        }
        if (!(Objects.equals(that.tAendern, this.tAendern))) {
            return false;
        }
        if (!(Objects.equals(that.personalIIdAendern, this.personalIIdAendern))) {
            return false;
        }
        if (!(Objects.equals(that.fFertigungssatzgroesse, this.fFertigungssatzgroesse))) {
            return false;
        }
        return Objects.equals(that.fMaxfertigungssatzgroesse, this.fMaxfertigungssatzgroesse);
    }

    public boolean istArtikelSnrOderchargentragend() {
        return Helper.short2boolean(getBSeriennrtragend()) || Helper.short2boolean(getBChargennrtragend());
    }

    /**
     * Einzeilige Artikelbezeichnung fuer die UI Darstellung zusammenbauen. <br>
     * Wird beispielsweise in FLR Listen verwendet.
     *
     * @return String
     */
    public String formatArtikelbezeichnung() {
        StringBuffer sbBez = new StringBuffer();

        if (getCNr() != null && getArtikelartCNr() != null) {
            if (!getArtikelartCNr().equals(ArtikelFac.ARTIKELART_HANDARTIKEL)) {
                sbBez.append(getCNr());
            }

            if (getArtikelsprDto() != null) {
                if (getArtikelsprDto().getCBez() != null && getArtikelsprDto().getCBez().length() > 0) {
                    sbBez.append(" " + getArtikelsprDto().getCBez());
                } else if (getArtikelsprDto().getCZbez() != null && getArtikelsprDto().getCZbez().length() > 0) {
                    sbBez.append(" " + getArtikelsprDto().getCZbez());
                }
            }
        }

        return sbBez.toString();
    }

    public String formatArtikelbezeichnungMitZusatzbezeichnung() {
        StringBuffer sbBez = new StringBuffer();

        if (getCNr() != null && getArtikelartCNr() != null) {
            if (!getArtikelartCNr().equals(ArtikelFac.ARTIKELART_HANDARTIKEL)) {
                sbBez.append(getCNr());
            }

            if (getArtikelsprDto() != null) {
                if (getArtikelsprDto().getCBez() != null && getArtikelsprDto().getCBez().length() > 0) {
                    sbBez.append(" " + getArtikelsprDto().getCBez());
                }

                if (getArtikelsprDto().getCZbez() != null && getArtikelsprDto().getCZbez().length() > 0) {
                    sbBez.append(" " + getArtikelsprDto().getCZbez());
                }
            }
        }

        return sbBez.toString();
    }

    public String formatArtikelbezeichnungMitZusatzbezeichnungUndReferenznummer() {
        StringBuffer sbBez = new StringBuffer();

        if (getCNr() != null && getArtikelartCNr() != null) {
            if (!getArtikelartCNr().equals(ArtikelFac.ARTIKELART_HANDARTIKEL)) {
                sbBez.append(getCNr());

                if (getCReferenznr() != null) {
                    sbBez.append(", " + getCReferenznr() + ", ");
                }
            }

            if (getArtikelsprDto() != null) {
                if (getArtikelsprDto().getCBez() != null && getArtikelsprDto().getCBez().length() > 0) {
                    sbBez.append(" " + getArtikelsprDto().getCBez());
                }

                if (getArtikelsprDto().getCZbez() != null && getArtikelsprDto().getCZbez().length() > 0) {
                    sbBez.append(" " + getArtikelsprDto().getCZbez());
                }
            }
        }

        return sbBez.toString();
    }

    /**
     * Nur Bezeichnung ohne Artikelnummer zusammenbauen. <br>
     * Wird beispielsweise in FLR Listen verwendet.
     *
     * @return String
     */
    public String formatBezeichnung() {
        StringBuffer sbBez = new StringBuffer();
        if (getArtikelsprDto() != null) {
            if (getArtikelsprDto().getCBez() != null && getArtikelsprDto().getCBez().length() > 0) {
                sbBez.append(getArtikelsprDto().getCBez());
                // MR 20080109: ZBez an CBez anhaengen, nur wenn CBez vorhanden
                if (getArtikelsprDto().getCZbez() != null && getArtikelsprDto().getCZbez().length() > 0) {
                    sbBez.append(" " + getArtikelsprDto().getCZbez());
                }
            }
        }
        return sbBez.toString();
    }

    @HvDtoLogIgnore
    public String getKbezAusSpr() {
        if (getArtikelsprDto() != null) {
            return getArtikelsprDto().getCKbez();
        } else {
            return null;
        }
    }

    @HvDtoLogIgnore
    public String getCBezAusSpr() {
        if (getArtikelsprDto() != null) {
            return getArtikelsprDto().getCBez();
        } else {
            return null;
        }
    }

    @HvDtoLogIgnore
    public String getCZBezAusSpr() {
        if (getArtikelsprDto() != null) {
            return getArtikelsprDto().getCZbez();
        } else {
            return null;
        }
    }

    @HvDtoLogIgnore
    public String getCZBez2AusSpr() {
        if (getArtikelsprDto() != null) {
            return getArtikelsprDto().getCZbez2();
        } else {
            return null;
        }
    }

    @HvDtoLogIgnore
    public String getCKBezAusSpr() {
        if (getArtikelsprDto() != null) {
            return getArtikelsprDto().getCKbez();
        } else {
            return null;
        }
    }

    /**
     * Ist es ein Arbeitszeitartikel?
     *
     * @return true wenn es sich um einen Arbeitszeitartikel handelt
     */
    @HvDtoLogIgnore
    public boolean isArbeitszeit() {
        return ArtikelFac.ARTIKELART_ARBEITSZEIT.equals(getArtikelartCNr());
    }

    public int hashCode() {
        int result = 17;
        result = 37 * result + this.iId.hashCode();
        result = 37 * result + this.cNr.hashCode();
        result = 37 * result + this.herstellerIId.hashCode();
        result = 37 * result + this.cArtikelbezhersteller.hashCode();
        result = 37 * result + this.cArtikelnrhersteller.hashCode();
        result = 37 * result + this.artgruIId.hashCode();
        result = 37 * result + this.artklaIId.hashCode();
        result = 37 * result + this.artikelartCNr.hashCode();
        result = 37 * result + this.einheitCNr.hashCode();
        result = 37 * result + this.bSeriennrtragend.hashCode();
        result = 37 * result + this.cReferenznr.hashCode();
        result = 37 * result + this.fLagermindest.hashCode();
        result = 37 * result + this.fLagersoll.hashCode();
        result = 37 * result + this.fFertigungssatzgroesse.hashCode();
        result = 37 * result + this.fVerpackungsmenge.hashCode();
        result = 37 * result + this.fVerschnittfaktor.hashCode();
        result = 37 * result + this.fVerschnittbasis.hashCode();
        result = 37 * result + this.fJahresmenge.hashCode();
        result = 37 * result + this.mwstsatzbezIId.hashCode();
        result = 37 * result + this.materialIId.hashCode();
        result = 37 * result + this.fGewichtkg.hashCode();
        result = 37 * result + this.fMaterialgewicht.hashCode();
        result = 37 * result + this.bAntistatic.hashCode();
        result = 37 * result + this.artikelIIdZugehoerig.hashCode();
        result = 37 * result + this.fVertreterprovisionmax.hashCode();
        result = 37 * result + this.fMinutenfaktor1.hashCode();
        result = 37 * result + this.fMinutenfaktor2.hashCode();
        result = 37 * result + this.fMindestdeckungsbeitrag.hashCode();
        result = 37 * result + this.cVerkaufseannr.hashCode();
        result = 37 * result + this.cWarenverkehrsnummer.hashCode();
        result = 37 * result + this.bRabattierbar.hashCode();
        result = 37 * result + this.iGarantiezeit.hashCode();
        result = 37 * result + this.tAnlegen.hashCode();
        result = 37 * result + this.personalIIdAnlegen.hashCode();
        result = 37 * result + this.tAendern.hashCode();
        result = 37 * result + this.personalIIdAendern.hashCode();
        return result;
    }

    public String toString() {
        String returnString = "";
        returnString += iId;
        returnString += ", " + cNr;
        returnString += ", " + herstellerIId;
        returnString += ", " + cArtikelbezhersteller;
        returnString += ", " + cArtikelnrhersteller;
        returnString += ", " + artgruIId;
        returnString += ", " + artklaIId;
        returnString += ", " + artikelartCNr;
        returnString += ", " + einheitCNr;
        returnString += ", " + bSeriennrtragend;
        returnString += ", " + cReferenznr;
        returnString += ", " + fLagermindest;
        returnString += ", " + fLagersoll;
        returnString += ", " + fFertigungssatzgroesse;
        returnString += ", " + fVerpackungsmenge;
        returnString += ", " + fVerschnittfaktor;
        returnString += ", " + fVerschnittbasis;
        returnString += ", " + fJahresmenge;
        returnString += ", " + mwstsatzbezIId;
        returnString += ", " + materialIId;
        returnString += ", " + fGewichtkg;
        returnString += ", " + fMaterialgewicht;
        returnString += ", " + bAntistatic;
        returnString += ", " + artikelIIdZugehoerig;
        returnString += ", " + fVertreterprovisionmax;
        returnString += ", " + fMinutenfaktor1;
        returnString += ", " + fMinutenfaktor2;
        returnString += ", " + fMindestdeckungsbeitrag;
        returnString += ", " + cVerkaufseannr;
        returnString += ", " + cWarenverkehrsnummer;
        returnString += ", " + bRabattierbar;
        returnString += ", " + iGarantiezeit;
        returnString += ", " + tAnlegen;
        returnString += ", " + personalIIdAnlegen;
        returnString += ", " + tAendern;
        returnString += ", " + personalIIdAendern;
        return returnString;
    }
}
