/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 * 
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 * 
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: developers@heliumv.com
 ******************************************************************************/
package com.lp.server.artikel.fastlanereader;

import java.awt.Color;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.*;

import javax.persistence.TypedQuery;
import javax.swing.Icon;

import com.lp.server.artikel.service.*;
import com.lp.server.system.service.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.lp.server.artikel.fastlanereader.generated.FLRArtikelsperren;
import com.lp.server.benutzer.service.RechteFac;
import com.lp.server.fertigung.service.FertigungFac;
import com.lp.server.partner.service.KundeDto;
import com.lp.server.system.fastlanereader.service.TableColumnInformation;
import com.lp.server.system.jcr.service.PrintInfoDto;
import com.lp.server.system.jcr.service.docnode.DocNodeArtikel;
import com.lp.server.system.jcr.service.docnode.DocPath;
import com.lp.server.util.Facade;
import com.lp.server.util.HelperServer;
import com.lp.server.util.QueryFeature;
import com.lp.server.util.fastlanereader.FLRSessionFactory;
import com.lp.server.util.fastlanereader.UseCaseHandler;
import com.lp.server.util.fastlanereader.service.query.FilterBlock;
import com.lp.server.util.fastlanereader.service.query.FilterKriterium;
import com.lp.server.util.fastlanereader.service.query.QueryParameters;
import com.lp.server.util.fastlanereader.service.query.QueryParametersFeatures;
import com.lp.server.util.fastlanereader.service.query.QueryResult;
import com.lp.server.util.fastlanereader.service.query.SortierKriterium;
import com.lp.server.util.fastlanereader.service.query.TableInfo;
import com.lp.util.EJBExceptionLP;
import com.lp.util.Helper;
import com.lp.util.siprefixparser.DefaultSiPrefixParser;

/**
 * <p>
 * Hier wird die FLR Funktionalit&auml;t f&uuml;r den Artikel implementiert. Pro
 * UseCase gibt es einen Handler.
 * </p>
 * <p>
 * Copright Logistik Pur Software GmbH (c) 2004-2007
 * </p>
 * <p>
 * Erstellungsdatum 2004-08-14
 * </p>
 * <p>
 * </p>
 * 
 * @author ck
 * @version 1.0
 */

public class ArtikellisteHandler extends UseCaseHandler {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	boolean bAbmessungenStattZusatzbezeichnung = false;
	boolean bArtikelgruppeStattZusatzbezeichnung = false;
	int bVkPreisStattGestpreis = 0;
	boolean bVkPreisLief1preis = false;
	boolean bLief1Infos = false;
	boolean bTextsucheInklusiveArtikelnummer = false;
	boolean bTextsucheInklusiveIndexRevision = false;
	boolean bTextsucheInklusiveHersteller = false;
	private boolean bArtikelgruppeAnzeigen = false;
	private boolean bZusatzbezeichnung2Anzeigen = false;
	private boolean bLagerplaetzeAnzeigen = false;
	private boolean bUebermengenAnzeigen = false;
	private boolean bArtikelklasseAnzeigen = false;
	private boolean bKurzbezeichnungAnzeigen = false;
	private boolean bDarfPreiseSehen = false;
	private boolean bReferenznummerAnzeigen = false;
	private boolean bHerstellernummerAnzeigen = false;
	private boolean bMaterialAnzeigen = false;
	private boolean bLagerstandDesAnderenMandantenAnzeigen = false;
	private boolean bVerfuegbarkeitAnzeigen = false;
	private boolean bShowReferenceArticle = false;
	private int lagerIIdZusaetzlichesLager = -1;
	private LagerDto lagerDtoZusaetzlichesLager = null;
	private boolean bArtikelsucheInclReferenznummer = false;
	private String lagerCNrZusaetzlichesLager = null;
	private boolean bDualUse = false;
	private boolean bSelectArtikelGruppe = false;

	boolean bArtikelfreigabe = false;

	private String artikelnummerOhne = null;

	private int vkPreisliste = -1;
	private Feature cachedFeature = null;
	private String cachedWhereClause = null;
	HashMap<String, Object> cachedWhereParams = new HashMap<>();
	final private String[] RELEVANT_ZUSATZFUNKTIONEN = {
			MandantFac.ZUSATZFUNKTION_ZENTRALER_ARTIKELSTAMM,
			MandantFac.ZUSATZFUNKTION_ARTIKELFREIGABE,
			MandantFac.ZUSATZFUNKTION_DUAL_USE,
			MandantFac.ZUSATZFUNKTION_GETRENNTE_LAGER
	};
	final private HashMap<String, Boolean> darfAnwenderAufZusatzfunktionZugreifenMap = new HashMap<>();

	private class Feature extends QueryFeature<IArtikellisteFLRData> {
		private boolean featureEinheitCnr = false;
		private boolean featureKundenartikelnummer = false;
		private boolean featureSoko = false;

		private Integer kundeId;
		private Date sokoDate;
		private Integer partnerId;

		public Feature() {
			if (getQuery() instanceof QueryParametersFeatures) {
				initializeFeature((QueryParametersFeatures) getQuery());
			}
		}

		@Override
		protected void initializeFeature(QueryParametersFeatures query) {
			featureEinheitCnr = query.hasFeature(ArtikellisteHandlerFeature.EINHEIT_CNR);
			featureKundenartikelnummer = query.hasFeatureValue(ArtikellisteHandlerFeature.KUNDENARTIKELNUMMER_CNR);
			featureSoko = query.hasFeature(ArtikellisteHandlerFeature.SOKO);

			if (featureKundenartikelnummer) {
				try {
					partnerId = Integer
							.parseInt(query.getFeatureValue(ArtikellisteHandlerFeature.KUNDENARTIKELNUMMER_CNR));
				} catch (NumberFormatException e) {
					myLogger.error("Feature '" + ArtikellisteHandlerFeature.KUNDENARTIKELNUMMER_CNR
							+ "' hat keine auswertbare PartnerId! (Deaktiviere Feature)");
					featureKundenartikelnummer = false;
				}
			}
		}

		@Override
		protected IArtikellisteFLRData[] createFlrData(int rows) {
			return new ArtikellisteFLRDataDto[rows];
		}

		public boolean hasFeatureEinheitCnr() {
			return featureEinheitCnr;
		}

		public boolean hasFeatureKundenartikelCnr() {
			return featureKundenartikelnummer;
		}

		public boolean hasFeatureSoko() {
			return featureSoko;
		}

		private Integer getKundeIdFromPartner(Integer partnerId) throws RemoteException {
			if (kundeId == null) {
				KundeDto kundeDto = getKundeFac().kundeFindByiIdPartnercNrMandantOhneExc(partnerId,
						theClientDto.getMandant(), theClientDto);
				kundeId = kundeDto.getIId();
			}
			return kundeId;
		}

		private Date getSokoDate() {
			if (sokoDate == null) {
				sokoDate = new Date(getTimestamp().getTime());
			}
			return sokoDate;
		}

		public void buildFeatureData(int row, Object o[]) {
			if (hasFeatureEinheitCnr()) {
				setFlrDataObject(row, new ArtikellisteFLRDataDto((String) o[22]));
			}

			if (hasFeatureKundenartikelCnr()) {
				getFlrDataObject(row).setKundenartikelCnr((String) o[27]);
			}
		}

		public Integer getKundeId() {
			try {
				return hasFeatureKundenartikelCnr() ? getKundeIdFromPartner(partnerId) : null;
			} catch (RemoteException e) {
				throw new EJBExceptionLP(EJBExceptionLP.FEHLER_FLR, e);
			}
		}
	}

	private Feature getFeature() {
		if (cachedFeature == null) {
			cachedFeature = new Feature();
		}
		return cachedFeature;
	}

	private boolean darfAnwenderAufZusatzfunktionZugreifen(String zusatzfunktionCNr, TheClientDto clientDto) {
		if (!darfAnwenderAufZusatzfunktionZugreifenMap.containsKey(zusatzfunktionCNr)) {
			darfAnwenderAufZusatzfunktionZugreifenMap.putAll(getMandantFac().getAnwenderAnwenderZustzfunktionenZugriffsRechteMap(RELEVANT_ZUSATZFUNKTIONEN, clientDto));
		}
		return darfAnwenderAufZusatzfunktionZugreifenMap.get(zusatzfunktionCNr);
	}

	@Override
	public QueryResult setQuery(QueryParameters queryParameters) throws EJBExceptionLP {
		cachedWhereClause = null;
		cachedWhereParams.clear();
		// Reset the Map, so it will be reloaded on the next call.
		darfAnwenderAufZusatzfunktionZugreifenMap.clear();
		return super.setQuery(queryParameters);
	}

	private java.sql.Date calculateToday() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return new java.sql.Date(calendar.getTime().getTime());
	}

	public QueryResult getPageAt(Integer rowIndex) throws EJBExceptionLP {
		QueryResult result = null;
		SessionFactory factory = FLRSessionFactory.getFactory();
		Session session = null;
		Session subSession = null;

		Iterator resultListIterator = null;
		List resultList = null;

		try {
			int colCount = getTableInfo().getColumnClasses().length + 1;
			int pageSize = getLimit();
			int startIndex = getStartIndex(rowIndex, pageSize);
			int endIndex = startIndex + pageSize - 1;

			session = factory.openSession();
			session = setFilter(session);
			HashMap<String, Object> params = new HashMap<>();
			String whereClause = this.buildWhereClause(params);
			String orderByClause = this.buildOrderByClause();
			boolean bSelectGeoDaten = this.bAbmessungenStattZusatzbezeichnung || whereClause.contains("geo.");
			boolean bSelectArtikelGruppe = this.bSelectArtikelGruppe || whereClause.contains("ag.");
			boolean bSelectArtikelKlasse = this.bArtikelklasseAnzeigen || whereClause.contains("ak.");
			boolean bSelectMaterialNr = this.bMaterialAnzeigen || whereClause.contains("mat.");
			boolean bSelectLagerplatz = this.bLagerplaetzeAnzeigen || whereClause.contains("lagerplaetze.");
			String groupByClause = this.buildGroupByClause(bSelectGeoDaten, bSelectArtikelGruppe, bSelectArtikelKlasse, bSelectMaterialNr);
			String artikelQueryString = "SELECT artikelliste.i_id FROM FLRArtikelliste AS artikelliste "
					+ buildJoinClause(bSelectGeoDaten, bSelectArtikelGruppe, bSelectArtikelKlasse, bSelectMaterialNr, bSelectLagerplatz)
					+ whereClause
					+ groupByClause
					+ orderByClause;

			TypedQuery<Integer> selectIDsQuery = this.prepareQuery(session, artikelQueryString, params, Integer.class);
			selectIDsQuery.setFirstResult(startIndex);
			selectIDsQuery.setMaxResults(pageSize);

			List<Integer> artikelIDs = selectIDsQuery.getResultList();

			HashMap hmRabattsatzFixpreis = new HashMap();
			HashMap hmPreisbasis = new HashMap();
			HashMap<Integer, String> hmKommentarTooltip = new HashMap<>();

			if (!artikelIDs.isEmpty()) {
				final String whereCondition = " WHERE artikelliste.i_id IN (:artikel_id) ";
				final String inArtikel_i_id = " pl.artikel_i_id IN (:artikel_id) ";
				String inKommentar = " ko.artikelkommentar.artikel_i_id IN (:artikel_id) ";

				session.close();
				session = factory.openSession();
				session = setFilter(session);
				HashMap<String, Object> fromClauseParams = new HashMap<>();
				String queryString = this.getFromClause(fromClauseParams, bSelectGeoDaten, bSelectArtikelGruppe, bSelectArtikelKlasse, bSelectMaterialNr, bSelectLagerplatz)
						+ whereCondition
						+ groupByClause
						+ orderByClause;
				org.hibernate.query.Query<Object[]> query = this.prepareQuery(session, queryString, fromClauseParams, Object[].class);
				query.setParameterList("artikel_id", artikelIDs);
				resultList = query.getResultList();

				subSession = factory.openSession();
				String rabattsatzFixpreis = "SELECT pl.artikel_i_id, pl.n_artikelstandardrabattsatz ,pl.n_artikelfixpreis FROM FLRVkpfartikelpreis AS pl  WHERE pl.vkpfartikelpreisliste_i_id="
						+ vkPreisliste + " AND " + inArtikel_i_id + "  ORDER BY t_preisgueltigab DESC ";
				Query queryRabattsatzFixpreis = subSession.createQuery(rabattsatzFixpreis);
				queryRabattsatzFixpreis.setParameterList("artikel_id", artikelIDs);

				List resultListRabattsatzFixpreis = queryRabattsatzFixpreis.list();

				Iterator resultListIteratorRabattsatzFixpreis = resultListRabattsatzFixpreis.iterator();
				while (resultListIteratorRabattsatzFixpreis.hasNext()) {
					Object[] o = (Object[]) resultListIteratorRabattsatzFixpreis.next();
					if (!hmRabattsatzFixpreis.containsKey(o[0])) {
						hmRabattsatzFixpreis.put(o[0], o);
					}
				}

				String preisbasis;

				HashMap<String, Object> paramsPreisbasis = new HashMap<>();
				if (bVkPreisLief1preis) {
					// TODO Maybe Improve: Select only the last-values from the database.
					preisbasis = "SELECT pl.artikel_i_id, pl.n_nettopreis FROM FLRArtikellieferant AS pl WHERE "
							+ inArtikel_i_id + " ORDER BY i_sort ASC ";
				} else {
					// TODO Maybe Improve: Select only the last-values from the database.
					preisbasis = "SELECT pl.artikel_i_id, pl.n_verkaufspreisbasis FROM FLRVkpfartikelverkaufspreisbasis AS pl  WHERE "
							+ inArtikel_i_id + " AND t_verkaufspreisbasisgueltigab <= :t_verkaufspreisbasisgueltigab"
							+ " ORDER BY t_verkaufspreisbasisgueltigab DESC";
					paramsPreisbasis.put("t_verkaufspreisbasisgueltigab", calculateToday());
				}
				org.hibernate.query.Query<Object[]> queryPreisbasis = this.prepareQuery(subSession, preisbasis, paramsPreisbasis, Object[].class);
				queryPreisbasis.setParameterList("artikel_id", artikelIDs);

				List resultListPreisbasis = queryPreisbasis.list();

				Iterator resultListIteratorPreisbasis = resultListPreisbasis.iterator();
				while (resultListIteratorPreisbasis.hasNext()) {
					Object[] o = (Object[]) resultListIteratorPreisbasis.next();
					if (!hmPreisbasis.containsKey(o[0])) {
						hmPreisbasis.put(o[0], o[1]);
					}
				}

				String kommentare = "SELECT ko.artikelkommentar.artikel_i_id, ko.x_kommentar, ko.artikelkommentar.flrartikelkommentarart.c_nr FROM FLRArtikelkommentarspr AS ko WHERE "
						+ inKommentar + " AND ko.locale_c_nr = :client_locale AND ko.artikelkommentar.flrartikelkommentarart.b_tooltip=1 ";
				Query queryKommentare = subSession.createQuery(kommentare);
				queryKommentare.setParameterList("artikel_id", artikelIDs);
				queryKommentare.setParameter("client_locale", theClientDto.getLocUiAsString());

				List resultListKommentare = queryKommentare.list();

				Iterator resultListIteratorKommentare = resultListKommentare.iterator();
				while (resultListIteratorKommentare.hasNext()) {
					Object[] o = (Object[]) resultListIteratorKommentare.next();

					if (o[2] != null) {
						String kommentar = "<b>" + (String) o[2] + ":</b>\n" + (String) o[1];

						String kommentarVorhanden = "";
						if (hmKommentarTooltip.containsKey(o[0])) {
							kommentarVorhanden = hmKommentarTooltip.get(o[0]) + "<br><br>" + kommentar;
						} else {
							kommentarVorhanden = kommentar;
						}

						hmKommentarTooltip.put((Integer) o[0], kommentarVorhanden);
					}

				}
			}

			Object[][] rows = (resultList != null ? new Object[resultList.size()][colCount] : new Object[0][colCount]);
			String[] tooltipData = (resultList != null ? new String[resultList.size()] : new String[0]);
			if (resultList != null) {
				resultListIterator = resultList.iterator();

				int row = 0;

				getFeature().setFlrRowCount(rows.length);

				int index_uebermenge = -1;

				if (bUebermengenAnzeigen) {
					index_uebermenge = getTableColumnInformation().getViewIndex("uebermenge");
				}

				while (resultListIterator.hasNext()) {
					Object o[] = (Object[]) resultListIterator.next();
					Integer artikelID = (Integer) o[0];
					String artikelNr = (String) o[1];
					String artikelBezeichnung = (String) o[2];
					BigDecimal lagerStand = (BigDecimal) o[3];
					BigDecimal gestehungspreis = (BigDecimal) o[4];
					boolean lagerBewirtschaftet = Helper.short2boolean((Short) o[5]);
					String artikelZusatzBezeichnung = (String) o[6];
					FLRArtikelsperren as = (FLRArtikelsperren) o[7];
					BigDecimal artikelLagerGestehungspreis = (BigDecimal) o[8];
					String breiteText = (String) o[9];
					Double breite = (Double) o[10];
					Double hoehe = (Double) o[11];
					Double tiefe = (Double) o[12];
					String stuecklistenArtCNr = (String) o[13];
					String artikelGruppeCNr = (String) o[14];
					Long lAnzahlReklamationen = (Long) o[15];
					String artikelKlasseCNr = (String) o[17];
					boolean lagerbewirtschaftet = Helper.short2boolean((Short) o[18]);
					String vorzugsteil = (String) o[23];
					String herstellernummer = (String) o[24];
					String herstellerbezeichnung = (String) o[25];
					BigDecimal lagerstandAndererMandant = (BigDecimal) o[26];
					BigDecimal fehlmenge = (BigDecimal) o[27];
					BigDecimal reservierungen = (BigDecimal) o[28];
					boolean bVersteckt = Helper.short2boolean((Short) o[29]);
					BigDecimal artikelLagerLagerstand = (BigDecimal) o[30];
					String materialCNr = (String) o[31];
					Long lAnzahlProjekteMitSperrstatus = (Long) o[32];
					Timestamp freigabe = (Timestamp) o[33];
					boolean meldePflichtig = Helper.short2Boolean((Short) o[34]);
					boolean bewilligungsPflichtig = Helper.short2Boolean((Short) o[35]);
					String einheit = (String) o[36];
					Long countForecast = (Long) o[37];
					Integer referenceArticleId = (Integer) o[38];
					BigDecimal uebermengeSum = (BigDecimal) o[39];

					Object[] rowToAddCandidate = new Object[colCount];

					rowToAddCandidate[getTableColumnInformation().getViewIndex("i_id")] = artikelID;
					rowToAddCandidate[getTableColumnInformation().getViewIndex("artikel.artikelnummerlang")] = artikelNr;
					rowToAddCandidate[getTableColumnInformation().getViewIndex("lp.stuecklistenart")] = stuecklistenArtCNr != null
							? stuecklistenArtCNr.trim()
							: null;

					if (bKurzbezeichnungAnzeigen) {
						prepareKurzbezeichnung(o, rowToAddCandidate);
					}

					rowToAddCandidate[getTableColumnInformation().getViewIndex("bes.artikelbezeichnung")] = artikelBezeichnung;

					if (bMaterialAnzeigen) {
						rowToAddCandidate[getTableColumnInformation().getViewIndex("lp.material")] = materialCNr;
					}

					if (bAbmessungenStattZusatzbezeichnung) {
						prepareAbmessungen(breiteText, breite, hoehe, tiefe, rowToAddCandidate);
					} else if (bArtikelgruppeStattZusatzbezeichnung) {
						prepareArtikelGruppeInAbmessung(artikelGruppeCNr, rowToAddCandidate);
					} else {
						rowToAddCandidate[getTableColumnInformation().getViewIndex("artikel.zusatzbez")] = artikelZusatzBezeichnung;
					}

					if (bReferenznummerAnzeigen) {
						prepareReferenznummer(o, rowToAddCandidate);
					}
					if (bZusatzbezeichnung2Anzeigen) {
						prepareZusatzbezeichnung2(o, rowToAddCandidate);
					}
					if (bKurzbezeichnungAnzeigen) {
						prepareKurzbezeichnung(o, rowToAddCandidate);
					}

					if (bShowReferenceArticle) {
						if (referenceArticleId != null) {
							ArtikelDto aDto = getArtikelFac().artikelFindByPrimaryKey(referenceArticleId, theClientDto);
							rowToAddCandidate[getTableColumnInformation().getViewIndex("referenzartikel")] = aDto.getCNr();
						}
					}

					if (bArtikelgruppeAnzeigen) {
						prepareArtikelGruppe(artikelGruppeCNr, rowToAddCandidate);
					}
					if (bArtikelklasseAnzeigen) {
						prepareArtikelKlasse(artikelKlasseCNr, rowToAddCandidate);
					}

					// Verfuegbarkeit
					if (bVerfuegbarkeitAnzeigen) {
						if (lagerStand == null) {
							lagerStand = BigDecimal.ZERO;
						}

						if (fehlmenge == null) {
							fehlmenge = BigDecimal.ZERO;
						}

						if (reservierungen == null) {
							reservierungen = BigDecimal.ZERO;
						}

						rowToAddCandidate[getTableColumnInformation().getViewIndex("lp.verfuegbar")] = lagerStand
								.subtract(fehlmenge.add(reservierungen));
					}

					if (bUebermengenAnzeigen && uebermengeSum != null) {
						rowToAddCandidate[index_uebermenge] = uebermengeSum.negate();
					}

					if (bArtikelklasseAnzeigen) {
						prepareArtikelKlasse(artikelKlasseCNr, rowToAddCandidate);
					}

					if (bVkPreisStattGestpreis == 1 || bVkPreisStattGestpreis == 3) {
						prepareVkPreis(hmRabattsatzFixpreis, hmPreisbasis, artikelID, rowToAddCandidate);
					}

					if (bVkPreisStattGestpreis == 2 || bVkPreisStattGestpreis == 3) {
						ArtikellieferantDto alDto = getArtikelFac().getArtikelEinkaufspreisDesBevorzugtenLieferanten(
								artikelID, BigDecimal.ONE, theClientDto.getSMandantenwaehrung(), theClientDto);
						if (alDto != null) {
							rowToAddCandidate[getTableColumnInformation().getViewIndex("lp.lief1preis")] = alDto
									.getNNettopreis();
						}
					}

					if (gestehungspreis != null && lagerBewirtschaftet) {

						BigDecimal bdLagerstandZusaetzlichesLager = BigDecimal.ZERO;

						if (lagerIIdZusaetzlichesLager != -1) {
							if (artikelLagerLagerstand != null) {
								bdLagerstandZusaetzlichesLager = artikelLagerLagerstand;
								rowToAddCandidate[getTableColumnInformation()
										.getViewIndex("zus_lager")] = bdLagerstandZusaetzlichesLager;
							}

						}

						if (lagerStand != null) {
							/// SP8041
							if (lagerDtoZusaetzlichesLager != null
									&& Helper.short2boolean(lagerDtoZusaetzlichesLager.getBKonsignationslager())) {
								rowToAddCandidate[getTableColumnInformation().getViewIndex("lp.lagerstand")] = lagerStand;
							} else {
								rowToAddCandidate[getTableColumnInformation().getViewIndex("lp.lagerstand")] = lagerStand
										.subtract(bdLagerstandZusaetzlichesLager);
							}

						}
					} else {
						rowToAddCandidate[getTableColumnInformation().getViewIndex("lp.lagerstand")] = new BigDecimal(0);
						if (lagerIIdZusaetzlichesLager != -1) {
							rowToAddCandidate[getTableColumnInformation().getViewIndex("zus_lager")] = new BigDecimal(0);
						}
					}

					if (bLagerplaetzeAnzeigen) {
						prepareLagerplaetze(artikelID, rowToAddCandidate);
					}

					if (bLagerstandDesAnderenMandantenAnzeigen) {

						rowToAddCandidate[getTableColumnInformation()
								.getViewIndex("artikel.lagerstand.anderermandant")] = lagerstandAndererMandant;
					} else {

						if (bDarfPreiseSehen) {
							// Gestehungspreis holen
							if (gestehungspreis != null
									&& rowToAddCandidate[getTableColumnInformation().getViewIndex("lp.lagerstand")] != null
									&& ((BigDecimal) rowToAddCandidate[getTableColumnInformation()
									.getViewIndex("lp.lagerstand")]).doubleValue() > 0) {
								gestehungspreis = gestehungspreis.divide(
										new BigDecimal(((BigDecimal) rowToAddCandidate[getTableColumnInformation()
												.getViewIndex("lp.lagerstand")]).doubleValue()),
										4, BigDecimal.ROUND_HALF_EVEN);
							} else {
								// Projekt 10870: WH: Wenn kein Gestpreis
								// zustandekommt,
								// dann Gestpreis des Hauptlagers anzeigen
								if (lagerBewirtschaftet && artikelLagerGestehungspreis != null) {
									gestehungspreis = artikelLagerGestehungspreis;
								} else {
									gestehungspreis = new BigDecimal(0);
								}
							}
							if (bVkPreisStattGestpreis == 0 || bVkPreisStattGestpreis == 3) {
								rowToAddCandidate[getTableColumnInformation()
										.getViewIndex("lp.gestpreis")] = gestehungspreis;
							}
						}
					}

					// PJ20048
					if (bLief1Infos && bDarfPreiseSehen) {
						ArtikellieferantDto alDto = getArtikelFac().getArtikelEinkaufspreisDesBevorzugtenLieferanten(
								artikelID, BigDecimal.ONE, theClientDto.getSMandantenwaehrung(), theClientDto);
						if (alDto != null) {
							rowToAddCandidate[getTableColumnInformation().getViewIndex("artikel.auswahl.lieferant")] = alDto
									.getLieferantDto().getPartnerDto().getCKbez();
							rowToAddCandidate[getTableColumnInformation()
									.getViewIndex("artikel.auswahl.lief1preis")] = alDto.getNNettopreis();
							rowToAddCandidate[getTableColumnInformation().getViewIndex("artikel.auswahl.wbz")] = alDto
									.getIWiederbeschaffungszeit();
						}
					}

					Boolean hatOffeneReklamationen = (lAnzahlReklamationen != null) && lAnzahlReklamationen.intValue() > 0;

					Boolean hatProjekteMitSperren = (lAnzahlProjekteMitSperrstatus != null)
							&& lAnzahlProjekteMitSperrstatus.intValue() > 0;

					if (as != null || hatOffeneReklamationen || hatProjekteMitSperren) {
						String gesperrt = null;

						if (as != null) {
							gesperrt = as.getFlrsperren().getC_bez();
						}

						if (hatOffeneReklamationen) {
							rowToAddCandidate[getTableColumnInformation().getViewIndex("Icon")] = getStatusMitUebersetzung(
									gesperrt, new java.sql.Timestamp(System.currentTimeMillis()), "R");
						} else if (hatProjekteMitSperren) {
							rowToAddCandidate[getTableColumnInformation().getViewIndex("Icon")] = getStatusMitUebersetzung(
									gesperrt, new java.sql.Timestamp(System.currentTimeMillis()), "O");
						} else {
							rowToAddCandidate[getTableColumnInformation().getViewIndex("Icon")] = gesperrt;
						}

					}
					// PJ18548
					rowToAddCandidate[getTableColumnInformation().getViewIndex("artikel.vorzugsteil")] = vorzugsteil;

					if (bArtikelfreigabe) {
						if (freigabe != null) {
							rowToAddCandidate[getTableColumnInformation()
									.getViewIndex("IconFreigabe")] = FertigungFac.STATUS_ERLEDIGT;
						}
					}

					if (bDualUse) {
						rowToAddCandidate[getTableColumnInformation().getViewIndex("artikel.meldepflichtig")] = meldePflichtig;
						rowToAddCandidate[getTableColumnInformation().getViewIndex("artikel.bewilligungspflichtig")] = bewilligungsPflichtig;
					}

					if (einheit != null) {
						rowToAddCandidate[getTableColumnInformation().getViewIndex("lp.einheit")] = einheit.trim();
					}

					if (bHerstellernummerAnzeigen) {
						rowToAddCandidate[getTableColumnInformation().getViewIndex("lp.herstellernummer")] = herstellernummer;
						rowToAddCandidate[getTableColumnInformation().getViewIndex("lp.herstellerbezeichnung")] = herstellerbezeichnung;

					}

					if (!lagerbewirtschaftet) {
						rowToAddCandidate[getTableColumnInformation().getViewIndex("Color")] = Color.BLUE;

						if (bVersteckt) {
							rowToAddCandidate[getTableColumnInformation().getViewIndex("Color")] = new Color(0, 150, 255);
						}

					} else {

						if (countForecast != null && countForecast > 0) {
							// PJ21975 FORECAST
							rowToAddCandidate[getTableColumnInformation().getViewIndex("Color")] = new Color(255, 161, 50);
						} else {
							if (bVersteckt) {
								rowToAddCandidate[getTableColumnInformation().getViewIndex("Color")] = Color.LIGHT_GRAY;
							}
						}

					}

					rows[row] = rowToAddCandidate;

					// PJ18205
					String tooltip = hmKommentarTooltip.get(artikelID);
					if (tooltip != null) {
						String text = tooltip;
						text = text.replaceAll("\n", "<br>");
						text = "<html>" + text + "</html>";
						tooltipData[row] = text;
					}

					getFeature().buildFeatureData(row, o);
					row++;
				}
			}

			if (getFeature().hasFeatureEinheitCnr()) {
				ArtikellisteQueryResult artikellisteResult = new ArtikellisteQueryResult(rows, getRowCount(),
						startIndex, endIndex, 0);
				artikellisteResult.setFlrData(getFeature().getFlrData());
				result = artikellisteResult;
			} else {
				result = new QueryResult(rows, this.getRowCount(), startIndex, endIndex, 0, tooltipData);
			}
		} catch (Exception e) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER_FLR, e);
		} finally {
			closeSession(session);
			closeSession(subSession);
		}
		return result;
	}

	protected void prepareArtikelGruppeInAbmessung(String artikelGruppeCNr, Object[] target) {
		target[getTableColumnInformation().getViewIndex("lp.artikelgruppeInAbmessung")] = artikelGruppeCNr;
	}

	protected void prepareArtikelGruppe(String artikelGruppeCNr, Object[] target) {
		target[getTableColumnInformation().getViewIndex("lp.artikelgruppe")] = artikelGruppeCNr;
	}

	protected void prepareArtikelKlasse(String artikelKlasseCNr, Object[] target) {
		target[getTableColumnInformation().getViewIndex("lp.artikelklasse")] = artikelKlasseCNr;
	}

	protected void prepareKurzbezeichnung(Object[] source, Object[] target) {
		target[getTableColumnInformation().getViewIndex("artikel.kurzbez")] = source[16];
	}

	protected void prepareReferenznummer(Object[] source, Object[] target) {
		target[getTableColumnInformation().getViewIndex("lp.referenznummer")] = source[21];
	}

	protected void prepareZusatzbezeichnung2(Object[] source, Object[] target) {
		target[getTableColumnInformation().getViewIndex("artikel.zusatzbez2")] = source[20];
	}

	protected void prepareLagerplaetze(Integer artikelIId, Object[] target) throws RemoteException {
		// PJ 17762 Lagerplaetze manuell anzeigen
		// TODO Maybe prefetch this this if necessary, when it's still slow.
		target[getTableColumnInformation().getViewIndex("lp.lagerplatz")] = getLagerFac()
				.getLagerplaezteEinesArtikels(artikelIId, null);
	}

	protected void prepareVkPreis(HashMap hmRabattsatzFixpreis, HashMap hmPreisbasis, Integer artikelID,
								  Object[] target) {
		// VK-Preis Berechnen
		BigDecimal vkPreis = null;

		BigDecimal vkPreisbasis = new BigDecimal(0);
		BigDecimal vkRabattsatz = new BigDecimal(0);
		BigDecimal vkFixpreis = new BigDecimal(0);

		if (hmRabattsatzFixpreis.containsKey(artikelID)) {
			Object[] oTemp = (Object[]) hmRabattsatzFixpreis.get(artikelID);
			vkRabattsatz = (BigDecimal) oTemp[1];
			vkFixpreis = (BigDecimal) oTemp[2];
		}
		if (hmPreisbasis.containsKey(artikelID)) {
			vkPreisbasis = (BigDecimal) hmPreisbasis.get(artikelID);
		}

		if (vkFixpreis != null) {
			vkPreis = vkFixpreis;
		} else {
			if (vkRabattsatz != null && vkPreisbasis != null) {
				BigDecimal bdRabattsumme = vkPreisbasis.multiply(vkRabattsatz.movePointLeft(2));
				vkPreis = vkPreisbasis.subtract(bdRabattsumme);
			} else {
				vkPreis = vkPreisbasis;
			}
		}
		target[getTableColumnInformation().getViewIndex("lp.vkpreis")] = vkPreis;
	}

	protected void prepareAbmessungen(String breiteText, Double breite, Double hoehe, Double tiefe, Object[] target) {

		String abmessungen = "";

		// Breitetext
		if (breiteText != null) {
			abmessungen += breiteText;
		}

		// Breite
		if (breite != null) {
			abmessungen += breite + "x";
		}
		if (hoehe != null) {
			abmessungen += hoehe + "x";
		}
		if (tiefe != null) {
			abmessungen += tiefe;
		}

		// TODO: Eigentlich falsch wenn es keine Abmessungen gibt, aber ein
		// Breitetext vom Anwender erfasst wurde der mit "x" endet (ghp)
		target[getTableColumnInformation().getViewIndex("lp.abmessungen")] = StringUtils.stripEnd(abmessungen, "x");
	}

	protected long getRowCountFromDataBase() {
		HashMap<String, Object> params = new HashMap<>();
		String whereClause = buildWhereClause(params);
		boolean bSelectGeoDaten = this.bAbmessungenStattZusatzbezeichnung || whereClause.contains("geo.");
		boolean bSelectArtikelGruppe = this.bSelectArtikelGruppe || whereClause.contains("ag.");
		boolean bSelectArtikelKlasse = this.bArtikelklasseAnzeigen || whereClause.contains("ak.");
		boolean bSelectMaterialNr = this.bMaterialAnzeigen || whereClause.contains("mat.");
		boolean bSelectLagerplatz = this.bLagerplaetzeAnzeigen || whereClause.contains("lagerplaetze.");
		String queryString = "SELECT  COUNT(distinct artikelliste.i_id) FROM com.lp.server.artikel.fastlanereader.generated.FLRArtikelliste AS artikelliste "
				+ buildJoinClause(bSelectGeoDaten, bSelectArtikelGruppe, bSelectArtikelKlasse, bSelectMaterialNr, bSelectLagerplatz)
				+ whereClause;
		return getRowCountFromDataBaseByQuery(queryString, params);
	}

	protected String buildJoinClause(boolean bSelectGeoDaten, boolean bSelectArtikelGruppe, boolean bSelectArtikelKlasse, boolean bSelectMaterialNr, boolean bSelectLagerplatz) {
		String joinClause = (bSelectGeoDaten ? " LEFT OUTER JOIN artikelliste.flrgeometrie AS geo " :"" )
				+ " LEFT OUTER JOIN artikelliste.artikellieferantset AS artikellieferantset "
				+ " LEFT OUTER JOIN artikelliste.artikelsprset AS aspr "
				+ (bSelectArtikelGruppe ? " LEFT OUTER JOIN artikelliste.flrartikelgruppe AS ag " : "")
				+ (bSelectArtikelKlasse ? " LEFT OUTER JOIN artikelliste.flrartikelklasse AS ak " : "")
				+ " LEFT OUTER JOIN artikelliste.flrvorzug AS vz "
				+ (bSelectMaterialNr ? " LEFT OUTER JOIN artikelliste.flrmaterial AS mat " : "")
				+ ((bSelectLagerplatz) ? " LEFT OUTER JOIN artikelliste.artikellagerplatzset as lagerplaetze" : "");

		if (darfAnwenderAufZusatzfunktionZugreifen(MandantFac.ZUSATZFUNKTION_ZENTRALER_ARTIKELSTAMM, theClientDto)) {
			joinClause += " LEFT OUTER JOIN artikelliste.stuecklisten_mandantenunabhaengig AS stuecklisten ";
		} else {
			joinClause += " LEFT OUTER JOIN artikelliste.stuecklisten AS stuecklisten ";
		}

		if (getFeature().hasFeatureKundenartikelCnr() || getFeature().hasFeatureSoko()) {
			joinClause += " LEFT OUTER JOIN artikelliste.kundesokoset2 AS soko ";
		}

		return joinClause;
	}

	/**
	 * builds the where clause of the HQL (Hibernate Query Language) statement using
	 * the current query.
	 *
	 * @return the HQL where clause.
	 */

	private String buildWhereClause(HashMap<String, Object> params) {
		if (cachedWhereClause != null) {
			params.putAll(cachedWhereParams);
			return cachedWhereClause;
		}

		StringBuffer where = new StringBuffer();

		if (getQuery() != null && getQuery().getFilterBlock() != null
				&& getQuery().getFilterBlock().filterKrit != null) {

			FilterBlock filterBlock = getQuery().getFilterBlock();
			FilterKriterium[] filterKriterien = getQuery().getFilterBlock().filterKrit;
			String booleanOperator = filterBlock.boolOperator;
			boolean filterAdded = false;
			boolean bvolltext = false;

			for (FilterKriterium filterKriterium : filterKriterien) {
				if (filterKriterium.isKrit) {
					String paramName = filterKriterium.kritName.replaceAll("\\.", "_");
					if (filterAdded) {
						where.append(" ").append(booleanOperator);
					}
					filterAdded = true;
					Class<?> paramType = String.class;
					// TODO [2024-08-24] ek: WICHTIG b_bevorzugt was treated as String
					if ("artikelliste.b_versteckt".equals(filterKriterium.kritName) || "artikelliste.b_bevorzugt".equals(filterKriterium.kritName)) {
						paramType = Short.class;
					} else if ("artikelliste.i_id".equals(filterKriterium.kritName)) {
						paramType = Integer.class;
					}
					Object filterValue = parseFilterParamValue(filterKriterium, paramType);

					if (filterKriterium.kritName.equals("artikelliste.c_nr")) {
						String filterString = ((String) filterValue).toLowerCase();

						// PJ21406
						if (artikelnummerOhne != null) {
							filterKriterium.kritName = "REPLACE(" + filterKriterium.kritName + ",'" + artikelnummerOhne + "','')";
							filterValue = filterString.replaceAll(artikelnummerOhne, "");
						}

						if (bArtikelsucheInclReferenznummer) {
							where.append(" (lower(artikelliste.c_nr) ").append(filterKriterium.operator).append(" :").append(paramName);
							where.append(" OR lower(artikelliste.c_referenznr) ").append(filterKriterium.operator).append(" :").append(paramName).append(")");
							params.put(paramName, filterValue);
							continue;
						}
					} else if (filterKriterium.kritName.equals("artikelliste." + ArtikelFac.FLR_ARTIKELLISTE_STUECKLISTE_PARTNER_ID)) {
						where.append("(stuecklisten.partner_i_id IS NULL OR stuecklisten.partner_i_id=:").append(paramName).append(")");
						params.put(paramName, Integer.parseInt(filterKriterium.value));
						bTextsucheInklusiveArtikelnummer = true;
						continue;
					} else if (filterKriterium.kritName.equals(ArtikelFac.FLR_ARTIKELLISTE_SHOPGRUPPE_ID)) {
						buildFilterShopGruppe(filterKriterium, where);
						continue;
					} else if (filterKriterium.kritName.equals("NUR_ARTIKEL_MIT_ERSATZTYPEN")) {
						where.append(" EXISTS (SELECT 7 FROM FLRErsatztypen et WHERE et.artikel_i_id = artikelliste.i_id) ");
						continue;
					} else if (filterKriterium.kritName.equals("c_volltext")) {
						filterKriterium.kritName = "aspr.c_bez";
					}

					if (filterKriterium.kritName.equals("aspr.c_bez")) {
						filterKriterium.kritName = "aspr.c_bez";
						bvolltext = true;
					} else {
						bvolltext = false;
					}

					if (bvolltext && !filterKriterium.kritName.equals(ArtikelFac.FLR_ARTIKELLIEFERANT_C_ARTIKELNRLIEFERANT)) {

						StringBuffer suchstring = new StringBuffer("lower(coalesce(aspr.c_bez,'')||' '||coalesce(aspr.c_kbez,'')||' '||coalesce(aspr.c_zbez,'')||' '||coalesce(aspr.c_zbez2,''))");
						if (bAbmessungenStattZusatzbezeichnung) {
							suchstring.append("||' '||lower(coalesce(geo.c_breitetext,'')||coalesce(cast(geo.f_breite as string),'')||case  WHEN geo.f_hoehe IS NULL THEN '' ELSE 'x' END||coalesce(cast(geo.f_hoehe as string),'')||case  WHEN geo.f_tiefe IS NULL THEN '' ELSE 'x' END||coalesce(cast(geo.f_tiefe as string),''))");
						}

						if (bTextsucheInklusiveArtikelnummer) {
							suchstring.append("||' '||lower(artikelliste.c_nr)");
						}

						if (bTextsucheInklusiveHersteller) {
							suchstring.append("||' '||lower(coalesce(artikelliste.c_artikelnrhersteller,''))||' '||lower(coalesce(artikelliste.c_artikelbezhersteller,''))");
						}
						if (bTextsucheInklusiveIndexRevision) {
							suchstring.append("||' '||lower(coalesce(artikelliste.c_index,''))||' '||lower(coalesce(artikelliste.c_revision,''))");
						}

						String[] teile = filterKriterium.value.trim().split(" ");

						if (getSiSortKrit()) {
							List<String> siList = new ArrayList<String>();
							List<String> nonSiList = new ArrayList<String>();

							for (String teil : teile) {
								if (getSiValue(teil) != null) {
									siList.add(teil);
								} else {
									nonSiList.add(teil);
								}
							}

							int counter = 0;
							for (String nonSiEntry : nonSiList) {
								String vollTextParamName = "VOLLTEXT_SUCHE_" + counter;

								if (nonSiEntry.startsWith("-")) {
									where.append(" NOT ");
									nonSiEntry = nonSiEntry.substring(1);
								}
								String compareVale = "%" + nonSiEntry.toLowerCase() + "%";

								where.append("(");
								where.append(suchstring).append(" like :").append(vollTextParamName);
								params.put(vollTextParamName, compareVale);
								where.append(")");

								if (counter < nonSiList.size() - 1)
									where.append(" AND ");

								counter++;
							}

							if (!siList.isEmpty()) {
								if (!nonSiList.isEmpty())
									where.append(" AND coalesce(aspr.c_siwert,'')");
								else
									where.append(" coalesce(aspr.c_siwert,'')");

								where.append(" like ");
								where.append("(");
								where.append("'" + getSiValue(siList.get(0)) + "'");
								where.append(")");
							}
						} else {

							where.append("(");

							for (int p = 0; p < teile.length; p++) {
								String vollTextParamName = "VOLLTEXT_SUCHE_" + p;

								if (teile[p].startsWith("-")) {
									where.append(" NOT ");
									teile[p] = teile[p].substring(1);
								}
								String compareVale = "%" + teile[p].toLowerCase() + "%";

								where.append("lower(").append(suchstring).append(") like :").append(vollTextParamName);
								params.put(vollTextParamName, compareVale);
								if (p < teile.length - 1) {
									where.append(" AND ");
								}
							}

							where.append(")");

						}

					} else if (filterKriterium.kritName
							.equals(ArtikelFac.FLR_ARTIKELLIEFERANT_C_ARTIKELNRLIEFERANT)) {

						String suchstring = "lower(coalesce(artikellieferantset.c_artikelnrlieferant,'')||' '||coalesce(artikelliste.c_artikelnrhersteller,'')||' '||coalesce(artikelliste.c_artikelbezhersteller,'')||' '||coalesce(artikellieferantset."
								+ ArtikelFac.FLR_ARTIKELLIEFERANT_C_BEZBEILIEFERANT + ",''))";

						String[] teile = filterKriterium.value.toLowerCase().split(" ");
						where.append("(");

						for (int p = 0; p < teile.length; p++) {

							if (teile[p].startsWith("-")) {
								where.append(" NOT ");
								teile[p] = teile[p].substring(1);
							}
							if (getQuery().getIsApi()) {
								where.append("lower(" + suchstring + ") like " + teile[p].toLowerCase());
							} else {
								where.append("lower(" + suchstring + ") like '%" + teile[p].toLowerCase() + "%'");
							}
							if (p < teile.length - 1) {
								where.append(" AND ");
							}
						}

						where.append(") ");
					} else if (filterKriterium.kritName.equals("akag")) {
						String lowerFilterKritValue = ((String) filterValue).toLowerCase();
						where.append(" (lower(artikelliste.flrartikelgruppe.c_nr)");
						where.append(" ").append(filterKriterium.operator).append(" :").append(paramName);
						where.append(" OR lower(artikelliste.flrartikelklasse.c_nr)");
						where.append(" ").append(filterKriterium.operator).append(" :").append(paramName).append(")");
						params.put(paramName, lowerFilterKritValue);
					} else if (filterKriterium.kritName.equals(ArtikelFac.FLR_ARTIKELLISTE_FLRHERSTELLER)) {
						String lowerFilterKritValue = ((String) filterValue).toLowerCase();
						where.append(" (lower(artikelliste.flrhersteller.flrpartner.c_name1nachnamefirmazeile1)");
						where.append(" ").append(filterKriterium.operator).append(" :").append(paramName);
						where.append(" OR lower(artikelliste.flrhersteller.flrpartner.c_name2vornamefirmazeile2)");
						where.append(" ").append(filterKriterium.operator).append(" :").append(paramName).append(")");
						params.put(paramName, lowerFilterKritValue);
					} else if (filterKriterium.kritName.equals("ag.i_id")) {
						where.append(" ag.i_id ");
						where.append(filterKriterium.operator);
						String value = filterKriterium.value.toLowerCase().trim();
						if (FilterKriterium.OPERATOR_IN.equals(filterKriterium.operator)) {
							if (!(value == null || value.length() == 0)) {
								if (value.startsWith("(") && value.endsWith(")")) {
									where.append(" " + value);
								} else {
									where.append(" (" + value + ")");
								}
							}
						} else {
							where.append(value);
						}
					} else if (filterKriterium.kritName.startsWith("(SELECT artikellager.n_lagerstand")) {
						where.append(filterKriterium.kritName);
					} else {
						if (filterKriterium.isBIgnoreCase()) {
							where.append(" LOWER(").append(filterKriterium.kritName).append(")");
						} else {
							where.append(" ").append(filterKriterium.kritName);
						}
						where.append(" ").append(filterKriterium.operator);
						where.append(" :").append(paramName);
						params.put(paramName, filterValue);
					}
				}
			}

			if (filterAdded) {
				where.insert(0, " WHERE ");
			}
		}

		cachedWhereClause = where.toString();
		cachedWhereParams.putAll(params);
		return cachedWhereClause;
	}

	private void buildFilterShopGruppe(FilterKriterium filter, StringBuffer where) {
		if ("[0]".equals(filter.value)) {
			where.append(" (artikelliste.shopgruppe_i_id IS NOT NULL OR soko.kunde_i_id = " + getFeature().getKundeId()
					+ ")");
			bTextsucheInklusiveArtikelnummer = true;
			return;
		}
		if ("[1]".equals(filter.value)) {
			where.append(
					" (artikelliste.shopgruppe_i_id IS NULL AND soko.kunde_i_id = " + getFeature().getKundeId() + ")");
			bTextsucheInklusiveArtikelnummer = true;
			return;
		}
		if (FilterKriterium.OPERATOR_IN.equals(filter.operator)) {
			where.append(" artikelliste.shopgruppe_i_id IN " + filter.value);
			bTextsucheInklusiveArtikelnummer = true;
			return;
		}

		where.append(" artikelliste.shopgruppe_i_id = " + filter.value);
	}

	private String buildGroupByClause(boolean bSelectGeoDaten, boolean bSelectArtikelGruppe, boolean bSelectArtikelKlasse, boolean bSelectMaterialNr) {
		return " GROUP BY artikelliste.i_id,artikelliste.c_nr, aspr.c_bez, artikelliste.b_lagerbewirtschaftet, aspr.c_zbez"
				+ (bSelectGeoDaten ? ", geo.c_breitetext, geo.f_breite , geo.f_hoehe , geo.f_tiefe": "")
				+ ", stuecklisten.stuecklisteart_c_nr"
				+ (bSelectArtikelGruppe ? ", ag.c_nr": "")
				+ ", aspr.c_kbez"
				+ (bSelectArtikelKlasse ? ", ak.c_nr" : "")
				+ ", aspr.c_siwert, aspr.c_zbez2, artikelliste.c_referenznr, artikelliste.einheit_c_nr, vz.c_nr, artikelliste.c_artikelnrhersteller"
				+ " , artikelliste.c_artikelbezhersteller, artikelliste.b_versteckt"
				+ (bSelectMaterialNr ? ", mat.c_nr" : "")
				+ ", artikelliste.t_freigabe,artikelliste.b_meldepflichtig,artikelliste.b_bewilligungspflichtig "
				+ (getFeature().hasFeatureKundenartikelCnr() ? ", soko.c_kundeartikelnummer" : "");
	}

	/**
	 * builds the HQL (Hibernate Query Language) order by clause using the sort
	 * criterias contained in the current query.
	 * 
	 * @return the HQL order by clause.
	 */
	private String buildOrderByClause() {
		StringBuffer orderBy = new StringBuffer("");
		if (getQuery() != null) {
			SortierKriterium[] kriterien = getQuery().getSortKrit();
			boolean sortAdded = false;
			if (kriterien != null && kriterien.length > 0) {
				for (int i = 0; i < kriterien.length; i++) {
					if (!kriterien[i].kritName.endsWith(Facade.NICHT_SORTIERBAR)) {
						if (kriterien[i].isKrit) {
							if (sortAdded) {
								orderBy.append(", ");
							}
							sortAdded = true;
							orderBy.append(kriterien[i].kritName);
							orderBy.append(" ");
							orderBy.append(kriterien[i].value);
						}
					}
				}
			} else {
				// no sort criteria found, add default sort
				orderBy.append("artikelliste.c_nr ASC ");
				sortAdded = true;
			}
			if (orderBy.indexOf("artikelliste.i_id") < 0) {
				// unique sort required because otherwise rowNumber of
				// selectedId
				// within sort() method may be different from the position of
				// selectedId
				// as returned in the page of getPageAt().
				if (sortAdded) {
					orderBy.append(", ");
				}
				orderBy.append(" artikelliste.i_id ");
				sortAdded = true;
			}
			if (sortAdded) {
				orderBy.insert(0, " ORDER BY ");
			}
		}
		return orderBy.toString();
	}

	/**
	 * get the basic from clause for the HQL statement.
	 * 
	 * @return the from clause.
	 */
	private String getFromClause(HashMap<String, Object> params, boolean bSelectGeoDaten, boolean bSelectArtikelGruppe, boolean bSelectArtikelKlasse, boolean bSelectMaterialNr, boolean bSelectLagerplatz) throws Exception {
		params.put("client_mandant", theClientDto.getMandant());
		params.put("LAGERART_WERTGUTSCHRIFT", LagerFac.LAGERART_WERTGUTSCHRIFT);
		params.put("STATUS_ANGELEGT", LocaleFac.STATUS_ANGELEGT);
		params.put("STATUS_STORNIERT", LocaleFac.STATUS_STORNIERT);
		params.put("STATUS_FREIGEGEBEN", LocaleFac.STATUS_FREIGEGEBEN);
		params.put("lagerIIdZusaetzlichesLager", lagerIIdZusaetzlichesLager);
		params.put("HauptlagerDesMandantenID", getLagerFac().getHauptlagerDesMandanten(
				theClientDto,
				darfAnwenderAufZusatzfunktionZugreifen(MandantFac.ZUSATZFUNKTION_ZENTRALER_ARTIKELSTAMM, theClientDto),
				darfAnwenderAufZusatzfunktionZugreifen(MandantFac.ZUSATZFUNKTION_GETRENNTE_LAGER, theClientDto)
		).getIId());

		if (bUebermengenAnzeigen) {
			params.put("buchungsart_uebermenge", FehlmengeFac.BUCHUNGSART_UEBERMENGE);
		}

		return "SELECT artikelliste.i_id, artikelliste.c_nr, aspr.c_bez, (SELECT sum(artikellager.n_lagerstand) FROM FLRArtikellager AS artikellager WHERE artikellager.compId.artikel_i_id=artikelliste.i_id AND artikellager.flrlager.mandant_c_nr=:client_mandant"
				+ "  AND artikellager.flrlager.b_konsignationslager=0 AND artikellager.flrlager.lagerart_c_nr NOT IN(:LAGERART_WERTGUTSCHRIFT)),"
				+ " (SELECT sum(artikellager.n_lagerstand*artikellager.n_gestehungspreis) FROM FLRArtikellager AS artikellager WHERE artikellager.compId.artikel_i_id=artikelliste.i_id AND artikellager.flrlager.b_konsignationslager=0 AND artikellager.flrlager.lagerart_c_nr NOT IN(:LAGERART_WERTGUTSCHRIFT)), artikelliste.b_lagerbewirtschaftet, aspr.c_zbez, (SELECT s FROM FLRArtikelsperren as s WHERE s.artikel_i_id=artikelliste.i_id AND s.i_sort=1) as sperren,(SELECT al.n_gestehungspreis FROM FLRArtikellager as al WHERE al.compId.lager_i_id=:HauptlagerDesMandantenID"
				+ " AND al.compId.artikel_i_id=artikelliste.i_id) as gestpreishauptlager, "
				+ (bAbmessungenStattZusatzbezeichnung ? "geo.c_breitetext, geo.f_breite , geo.f_hoehe , geo.f_tiefe" : "cast(null as text), cast(null as float), cast(null as float), cast(null as float)")
				+", stuecklisten.stuecklisteart_c_nr"
				+ (bSelectArtikelGruppe ? ", ag.c_nr": ", cast(null as text)") + ",(SELECT COUNT(*) FROM FLRReklamation r WHERE r.flrartikel.i_id=artikelliste.i_id AND r.status_c_nr=:STATUS_ANGELEGT), aspr.c_kbez "
				+ (bSelectArtikelKlasse ? ",ak.c_nr ": ", cast(null as text)") + ", artikelliste.b_lagerbewirtschaftet,"
				+ (bAbmessungenStattZusatzbezeichnung ? "lower(coalesce(geo.c_breitetext,'')||coalesce(cast(geo.f_breite as string),'')||case  WHEN geo.f_hoehe IS NULL THEN '' ELSE 'x' END||coalesce(cast(geo.f_hoehe as string),'')||case  WHEN geo.f_tiefe IS NULL THEN '' ELSE 'x' END||coalesce(cast(geo.f_tiefe as string),''))" : "cast(null as text)")
				+ ", aspr.c_zbez2, artikelliste.c_referenznr, artikelliste.einheit_c_nr, vz.c_nr, artikelliste.c_artikelnrhersteller, artikelliste.c_artikelbezhersteller, (SELECT sum(artikellager.n_lagerstand) FROM FLRArtikellager AS artikellager WHERE artikellager.compId.artikel_i_id=artikelliste.i_id  AND artikellager.flrlager.b_konsignationslager=0 AND artikellager.flrlager.mandant_c_nr<>:client_mandant AND artikellager.flrlager.lagerart_c_nr NOT IN(:LAGERART_WERTGUTSCHRIFT))"
				+ (getFeature().hasFeatureKundenartikelCnr() ? ", soko.c_kundeartikelnummer" : "")
				+ " , (SELECT sum(fm.n_menge) FROM FLRFehlmenge AS fm WHERE fm.artikel_i_id=artikelliste.i_id), (SELECT sum(ar.n_menge) FROM FLRArtikelreservierung AS ar WHERE ar.flrartikel.i_id=artikelliste.i_id), artikelliste.b_versteckt, (SELECT artikellager.n_lagerstand FROM FLRArtikellager AS artikellager WHERE artikellager.compId.artikel_i_id=artikelliste.i_id AND artikellager.flrlager.i_id=:lagerIIdZusaetzlichesLager) "
				+ (bSelectMaterialNr ? ", mat.c_nr": ", cast(null as text)") + ",(SELECT COUNT(*) FROM FLRProjekt r WHERE r.flrartikel.i_id=artikelliste.i_id AND r.flrprojektstatus.status_c_nr <>:STATUS_STORNIERT AND r.flrprojektstatus.b_gesperrt=1 ), artikelliste.t_freigabe, artikelliste.b_meldepflichtig, artikelliste.b_bewilligungspflichtig, artikelliste.einheit_c_nr,(SELECT count(*) FROM FLRForecastposition AS fp WHERE fp.flrartikel.i_id=artikelliste.i_id AND fp.flrforecastauftrag.status_c_nr=:STATUS_FREIGEGEBEN),artikelliste.i_id_referenzartikel "
				+ (bUebermengenAnzeigen ? ", (SELECT sum(ueberm.n_menge) FROM FLRFehlmenge AS ueberm WHERE ueberm.c_buchungsart=:buchungsart_uebermenge AND ueberm.artikel_i_id=artikelliste.i_id) " : ", cast(null as float) ")
				+ " FROM FLRArtikelliste AS artikelliste"
				+ buildJoinClause(bSelectGeoDaten, bSelectArtikelGruppe, bSelectArtikelKlasse, bSelectMaterialNr, bSelectLagerplatz);
	}

	public Session setFilter(Session session) {
		session = super.setFilter(session);
		String sMandant = theClientDto.getMandant();
		if (darfAnwenderAufZusatzfunktionZugreifen(MandantFac.ZUSATZFUNKTION_ZENTRALER_ARTIKELSTAMM, theClientDto)) {
			session.enableFilter("filterMandant").setParameter("paramMandant", getSystemFac().getHauptmandant());
		} else {
			session.enableFilter("filterMandant").setParameter("paramMandant", sMandant);
		}

		if (getFeature().hasFeatureKundenartikelCnr() || getFeature().hasFeatureSoko()) {
			session.enableFilter("filterKundeId").setParameter("paramKundeId", getFeature().getKundeId());
			session.enableFilter("filterGueltig").setParameter("paramGueltig",
					Helper.cutDate(getFeature().getSokoDate()));
		}

		return session;
	}

	public QueryResult sort(SortierKriterium[] sortierKriterien, Object selectedIdI) throws EJBExceptionLP {

		getQuery().setSortKrit(sortierKriterien);

		QueryResult result = null;
		int rowNumber = 0;
		ScrollableResults scrollableResult = null;
		if (selectedIdI instanceof Integer) {
			if (((Integer) selectedIdI).intValue() >= 0) {
				if (getQuery().getIsApi()) {
					rowNumber = (Integer) selectedIdI;
				} else {
					SessionFactory factory = FLRSessionFactory.getFactory();
					Session session = null;

					try {
						session = factory.openSession();
						session = setFilter(session);

						String queryString = null;
						HashMap<String, Object> params = new HashMap<>();
						try {
							String whereClause = buildWhereClause(params);
							boolean bSelectGeoDaten = this.bAbmessungenStattZusatzbezeichnung || whereClause.contains("geo.");
							boolean bSelectArtikelGruppe = this.bSelectArtikelGruppe || whereClause.contains("ag.");
							boolean bSelectArtikelKlasse = this.bArtikelklasseAnzeigen || whereClause.contains("ak.");
							boolean bSelectMaterialNr = this.bMaterialAnzeigen || whereClause.contains("mat.");
							boolean bSelectLagerplatz = this.bLagerplaetzeAnzeigen || whereClause.contains("lagerplaetze.");
							queryString = "SELECT artikelliste.i_id FROM FLRArtikelliste AS artikelliste "
									+ buildJoinClause(bSelectGeoDaten, bSelectArtikelGruppe, bSelectArtikelKlasse, bSelectMaterialNr, bSelectLagerplatz)
									+ whereClause
									+ buildGroupByClause(bSelectGeoDaten, bSelectArtikelGruppe, bSelectArtikelKlasse, bSelectMaterialNr)
									+ buildOrderByClause();
						} catch (Exception ex) {
							throw new EJBExceptionLP(EJBExceptionLP.FEHLER_FLR, ex);
						}

						org.hibernate.query.Query<Integer> query = prepareQuery(session, queryString, params, Integer.class);
						scrollableResult = query.scroll();

						if (scrollableResult != null) {
							scrollableResult.beforeFirst();
							while (scrollableResult.next()) {
								Integer id = scrollableResult.getInteger(0);
								if (selectedIdI.equals(id)) {
									rowNumber = scrollableResult.getRowNumber();
									break;
								}
							}
						}
					} catch (HibernateException e) {
						throw new EJBExceptionLP(EJBExceptionLP.FEHLER_FLR, e);
					} finally {
						try {
							if (session != null)
								session.close();
						} catch (HibernateException he) {
							throw new EJBExceptionLP(EJBExceptionLP.FEHLER_FLR, he);
						}
					}
				}
			}
		}
		if (rowNumber < 0) {
			rowNumber = 0;
		}

		result = getPageAt(rowNumber);
		result.setIndexOfSelectedRow(rowNumber);

		return result;
	}

	public TableInfo getTableInfo() {
		TableInfo info = super.getTableInfo();
		if (info != null)
			return info;

		setupParameters();
		setTableColumnInformation(createColumnInformation(theClientDto.getMandant(), theClientDto.getLocUi()));

		TableColumnInformation c = getTableColumnInformation();
		info = new TableInfo(c.getClasses(), c.getHeaderNames(), c.getWidths(), c.getDbColumNames(),
				c.getHeaderToolTips());
		setTableInfo(info);
		return info;
	}

	private TableColumnInformation createColumnInformation(String mandant, Locale locUi) {
		TableColumnInformation columns = new TableColumnInformation();

		columns.add("i_id", Integer.class, "i_id", -1, "i_id");
		columns.add("artikel.artikelnummerlang", String.class,
				getTextRespectUISpr("artikel.artikelnummerlang", mandant, locUi), QueryParameters.FLR_BREITE_L,
				"artikelliste.c_nr");

		columns.add("lp.stuecklistenart", String.class, getTextRespectUISpr("artikel.stuecklistenart", mandant, locUi),
				QueryParameters.FLR_BREITE_S, "stuecklisten.stuecklisteart_c_nr",
				getTextRespectUISpr("artikel.stuecklistenart.tooltip", mandant, locUi));
		if (bKurzbezeichnungAnzeigen) {
			columns.add("artikel.kurzbez", String.class, getTextRespectUISpr("artikel.kurzbez", mandant, locUi),
					QueryParameters.FLR_BREITE_XM, "aspr.c_kbez");
		}

		columns.add("bes.artikelbezeichnung", String.class,
				getTextRespectUISpr("bes.artikelbezeichnung", mandant, locUi), QueryParameters.FLR_BREITE_XL,
				"aspr.c_bez");

		if (bMaterialAnzeigen) {
			columns.add("lp.material", String.class, getTextRespectUISpr("lp.material", mandant, locUi),
					QueryParameters.FLR_BREITE_XM, "mat.c_nr");
		}

		if (bAbmessungenStattZusatzbezeichnung || bArtikelgruppeStattZusatzbezeichnung) {
			/*
			 * Unsauber Definition. Mit den Parametern ist es m&ouml;glich beide Variante zu
			 * setzen. Es soll aber nur eine von beiden funktionieren. So programmiert, dass
			 * bei beiden immer die Abmessung gewinnt.
			 */
			if (bAbmessungenStattZusatzbezeichnung) {
				columns.add("lp.abmessungen", String.class, getTextRespectUISpr("lp.abmessungen", mandant, locUi),
						QueryParameters.FLR_BREITE_SHARE_WITH_REST, "aspr.c_zbez");
			} else {
				columns.add("lp.artikelgruppeInAbmessung", String.class,
						getTextRespectUISpr("lp.artikelgruppe", mandant, locUi),
						QueryParameters.FLR_BREITE_SHARE_WITH_REST, "ag.c_nr");
			}
		} else {
			columns.add("artikel.zusatzbez", String.class, getTextRespectUISpr("artikel.zusatzbez", mandant, locUi),
					QueryParameters.FLR_BREITE_SHARE_WITH_REST, "aspr.c_zbez");
		}

		if (bReferenznummerAnzeigen) {
			columns.add("lp.referenznummer", String.class, getTextRespectUISpr("lp.referenznummer", mandant, locUi),
					QueryParameters.FLR_BREITE_XM, "artikelliste.c_referenznr");
		}

		if (bZusatzbezeichnung2Anzeigen) {
			columns.add("artikel.zusatzbez2", String.class, getTextRespectUISpr("artikel.zusatzbez2", mandant, locUi),
					QueryParameters.FLR_BREITE_L, "aspr.c_zbez2");
		}

		if (bShowReferenceArticle) {
			columns.add("referenzartikel", String.class, getTextRespectUISpr("artikel.referenz", mandant, locUi), QueryParameters.FLR_BREITE_XM, "artikelliste.i_id_referenzartikel");
		}

		if (bArtikelgruppeAnzeigen) {
			columns.add("lp.artikelgruppe", String.class, getTextRespectUISpr("lp.artikelgruppe", mandant, locUi),
					QueryParameters.FLR_BREITE_L, "ag.c_nr");
		}
		if (bArtikelklasseAnzeigen) {
			columns.add("lp.artikelklasse", String.class, getTextRespectUISpr("lp.artikelklasse", mandant, locUi),
					QueryParameters.FLR_BREITE_L, "ak.c_nr");
		}
		if (bLagerplaetzeAnzeigen) {
			columns.add("lp.lagerplatz", String.class, getTextRespectUISpr("lp.lagerplatz", mandant, locUi),
					QueryParameters.FLR_BREITE_SHARE_WITH_REST, Facade.NICHT_SORTIERBAR);
		}

		columns.add("lp.lagerstand", BigDecimal.class,
				getTextRespectUISpr(
						bLagerstandDesAnderenMandantenAnzeigen ? "artikel.lagerstand.eigenermandant" : "lp.lagerstand",
						mandant, locUi),
				QueryParameters.FLR_BREITE_M, Facade.NICHT_SORTIERBAR);

		if (bUebermengenAnzeigen) {
			columns.add("uebermenge", BigDecimal.class, getTextRespectUISpr("fert.buchungsart.uebermenge", mandant, locUi),
					QueryParameters.FLR_BREITE_SHARE_WITH_REST, Facade.NICHT_SORTIERBAR);
		}

		columns.add("lp.einheit", String.class, getTextRespectUISpr("lp.einheit", mandant, locUi),
				QueryParameters.FLR_BREITE_XS, "artikelliste.einheit_c_nr");

		if (lagerIIdZusaetzlichesLager != -1 && lagerCNrZusaetzlichesLager != null) {
			columns.add("zus_lager", BigDecimal.class, lagerCNrZusaetzlichesLager, QueryParameters.FLR_BREITE_M,
					Facade.NICHT_SORTIERBAR);
		}

		if (bVerfuegbarkeitAnzeigen) {
			columns.add("lp.verfuegbar", BigDecimal.class, getTextRespectUISpr("lp.verfuegbar", mandant, locUi),
					QueryParameters.FLR_BREITE_M, Facade.NICHT_SORTIERBAR);
		}

		if (bLagerstandDesAnderenMandantenAnzeigen == true) {
			columns.add("artikel.lagerstand.anderermandant", BigDecimal.class,
					getTextRespectUISpr("artikel.lagerstand.anderermandant", mandant, locUi),
					QueryParameters.FLR_BREITE_M, Facade.NICHT_SORTIERBAR);
		} else {

			if (bVkPreisStattGestpreis == 0) {
				columns.add("lp.gestpreis", BigDecimal.class, getTextRespectUISpr("lp.gestpreis", mandant, locUi),
						QueryParameters.FLR_BREITE_M, Facade.NICHT_SORTIERBAR);
			} else if (bVkPreisStattGestpreis == 1) {
				columns.add("lp.vkpreis", BigDecimal.class, getTextRespectUISpr("lp.vkpreis", mandant, locUi),
						QueryParameters.FLR_BREITE_M, Facade.NICHT_SORTIERBAR);
			} else if (bVkPreisStattGestpreis == 2) {
				columns.add("lp.lief1preis", BigDecimal.class,
						getTextRespectUISpr("artikel.label.lief1Preis", mandant, locUi), QueryParameters.FLR_BREITE_M,
						Facade.NICHT_SORTIERBAR);
			} else if (bVkPreisStattGestpreis == 3) {
				columns.add("lp.gestpreis", BigDecimal.class, getTextRespectUISpr("lp.gestpreis", mandant, locUi),
						QueryParameters.FLR_BREITE_M, Facade.NICHT_SORTIERBAR);
				columns.add("lp.vkpreis", BigDecimal.class, getTextRespectUISpr("lp.vkpreis", mandant, locUi),
						QueryParameters.FLR_BREITE_M, Facade.NICHT_SORTIERBAR);
				columns.add("lp.lief1preis", BigDecimal.class,
						getTextRespectUISpr("artikel.label.lief1Preis", mandant, locUi), QueryParameters.FLR_BREITE_M,
						Facade.NICHT_SORTIERBAR);
			}

		}

		if (bLief1Infos) {
			columns.add("artikel.auswahl.lieferant", String.class,
					getTextRespectUISpr("artikel.auswahl.lieferant", mandant, locUi), QueryParameters.FLR_BREITE_M,
					Facade.NICHT_SORTIERBAR);
			columns.add("artikel.auswahl.lief1preis", BigDecimal.class,
					getTextRespectUISpr("artikel.auswahl.lief1preis", mandant, locUi), QueryParameters.FLR_BREITE_M,
					Facade.NICHT_SORTIERBAR);
			columns.add("artikel.auswahl.wbz", Integer.class,
					getTextRespectUISpr("artikel.auswahl.wbz", mandant, locUi), QueryParameters.FLR_BREITE_S,
					Facade.NICHT_SORTIERBAR);
		}

		columns.add("Icon", SperrenIcon.class, getTextRespectUISpr("artikel.sperre", mandant, locUi),
				QueryParameters.FLR_BREITE_XS, Facade.NICHT_SORTIERBAR,
				getTextRespectUISpr("artikel.sperre.tooltip", mandant, locUi));

		columns.add("artikel.vorzugsteil", String.class, getTextRespectUISpr("artikel.vorzugsteil", mandant, locUi),
				QueryParameters.FLR_BREITE_S, "vz.c_nr");

		if (bArtikelfreigabe) {
			columns.add("IconFreigabe", Icon.class, getTextRespectUISpr("ww.freigabe", mandant, locUi),
					QueryParameters.FLR_BREITE_S, "artikelliste.t_freigabe");
		}

		if (bDualUse) {
			columns.add("artikel.meldepflichtig", Boolean.class,
					getTextRespectUISpr("artikel.meldepflichtig", mandant, locUi), QueryParameters.FLR_BREITE_XXS,
					"artikelliste.b_meldepflichtig");

			columns.add("artikel.bewilligungspflichtig", Boolean.class,
					getTextRespectUISpr("artikel.bewilligungspflichtig", mandant, locUi),
					QueryParameters.FLR_BREITE_XXS, "artikelliste.b_bewilligungspflichtig");
		}

		if (bHerstellernummerAnzeigen) {
			columns.add("lp.herstellernummer", String.class, getTextRespectUISpr("lp.herstellernummer", mandant, locUi),
					QueryParameters.FLR_BREITE_XM, "artikelliste.c_artikelnrhersteller");
			columns.add("lp.herstellerbezeichnung", String.class,
					getTextRespectUISpr("lp.herstellerbezeichnung", mandant, locUi), QueryParameters.FLR_BREITE_XM,
					"artikelliste.c_artikelbezhersteller");
		}

		columns.add("Color", Color.class, "", 1, "");

		return columns;
	}

	private Integer getIntegerParameter(String category, String parameter) throws RemoteException {
		ParametermandantDto param = getParameterFac().getMandantparameter(theClientDto.getMandant(), category,
				parameter);
		return (Integer) param.getCWertAsObject();
	}

	private Boolean getBooleanParameter(String category, String parameter) throws RemoteException {
		ParametermandantDto param = getParameterFac().getMandantparameter(theClientDto.getMandant(), category,
				parameter);
		return (Boolean) param.getCWertAsObject();
	}

	private void setupParameters() {
		try {
			bAbmessungenStattZusatzbezeichnung = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_ABMESSUNGEN_STATT_ZUSATZBEZ_IN_AUSWAHLLISTE);
			bTextsucheInklusiveArtikelnummer = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_TEXTSUCHE_INKLUSIVE_ARTIKELNUMMER);
			bTextsucheInklusiveIndexRevision = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_TEXTSUCHE_INKLUSIVE_INDEX_REVISION);
			bTextsucheInklusiveHersteller = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_ARTIKELSUCHE_MIT_HERSTELLER);

			bArtikelgruppeStattZusatzbezeichnung = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_ARTIKELGRUPPE_STATT_ZUSATZBEZ_IN_AUSWAHLLISTE);

			bArtikelsucheInclReferenznummer = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_ARTIKELSUCHE_MIT_REFERENZNUMMER);

			bVkPreisStattGestpreis = getIntegerParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_VKPREIS_STATT_GESTPREIS_IN_ARTIKELAUSWAHL);
			bLagerstandDesAnderenMandantenAnzeigen = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_LAGERSTAND_DES_ANDEREN_MANDANTEN_ANZEIGEN);

			bVkPreisLief1preis = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_VKPREISBASIS_IST_LIEF1PREIS);
			bLief1Infos = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_LIEF1INFO_IN_ARTIKELAUSWAHLLISTE);

			bArtikelgruppeAnzeigen = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_ANZEIGEN_ARTIKELGRUPPE_IN_AUSWAHLLISTE);

			bSelectArtikelGruppe = (bArtikelgruppeStattZusatzbezeichnung || bArtikelgruppeAnzeigen);

			bZusatzbezeichnung2Anzeigen = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_ANZEIGEN_ZUSATZBEZ2_IN_AUSWAHLLISTE);
			bArtikelklasseAnzeigen = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_ANZEIGEN_ARTIKELKLASSE_IN_AUSWAHLLISTE);
			bVerfuegbarkeitAnzeigen = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_VERFUEGBARKEIT_IN_AUSWAHLLISTE);
			bKurzbezeichnungAnzeigen = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_ANZEIGEN_KURZBEZEICHNUNG_IN_AUSWAHLLISTE);
			bReferenznummerAnzeigen = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_ANZEIGEN_REFERENZNUMMER_IN_AUSWAHLLISTE);
			bHerstellernummerAnzeigen = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_ANZEIGEN_HERSTELLER_IN_AUSWAHLLISTE);
			bMaterialAnzeigen = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_MATERIAL_IN_AUSWAHLLISTE);
			bUebermengenAnzeigen = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.ANZEIGEN_UEBERMENGE_ARTIKEL_AUSWAHLLISTE);
			bLagerplaetzeAnzeigen = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_ANZEIGEN_LAGERPLATZ_IN_AUSWAHLLISTE);
			bShowReferenceArticle = getBooleanParameter(ParameterFac.KATEGORIE_ARTIKEL,
					ParameterFac.PARAMETER_ANZEIGEN_REFERENZ_ARTIKEL_IN_AUSWAHLLISTE);

			VkpfartikelpreislisteDto[] vkpfartikelpreislisteDtos = getVkPreisfindungFac()
					.vkpfartikelpreislisteFindByMandantCNr(theClientDto.getMandant());

			if (vkpfartikelpreislisteDtos != null && vkpfartikelpreislisteDtos.length > 0) {
				vkPreisliste = vkpfartikelpreislisteDtos[0].getIId();
			}

			bDarfPreiseSehen = getBenutzerServicesFac().hatRecht(RechteFac.RECHT_LP_DARF_PREISE_SEHEN_EINKAUF,
					theClientDto);

			ParametermandantDto param = getParameterFac().getMandantparameter(theClientDto.getMandant(),
					ParameterFac.KATEGORIE_ARTIKEL, ParameterFac.PARAMETER_ZUSAETZLICHES_LAGER_IN_AUSWAHLLISTE);
			String lagerCNr = (String) param.getCWertAsObject();

			if (lagerCNr != null && lagerCNr.length() > 0) {
				try {
					LagerDto lDto = getLagerFac().lagerFindByCNrByMandantCNrOhneExc(lagerCNr,
							theClientDto.getMandant());
					if (lDto != null) {
						lagerIIdZusaetzlichesLager = lDto.getIId();
						lagerCNrZusaetzlichesLager = lDto.getCNr();
						lagerDtoZusaetzlichesLager = lDto;
					}
				} catch (Throwable e) {
					// Nicht gefunden
				}
			}

			ParametermandantDto paramOhne = getParameterFac().getMandantparameter(theClientDto.getMandant(),
					ParameterFac.KATEGORIE_ARTIKEL, ParameterFac.PARAMETER_ARTIKELSUCHE_OHNE);
			artikelnummerOhne = (String) paramOhne.getCWertAsObject();

			if (darfAnwenderAufZusatzfunktionZugreifen(MandantFac.ZUSATZFUNKTION_ARTIKELFREIGABE, theClientDto)) {
				bArtikelfreigabe = true;
			}

			if (darfAnwenderAufZusatzfunktionZugreifen(MandantFac.ZUSATZFUNKTION_DUAL_USE, theClientDto)) {
				bDualUse = true;
			}

		} catch (RemoteException ex) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER, ex);
		}
	}

	public PrintInfoDto getSDocPathAndPartner(Object key) {
		ArtikelDto artikelDto = null;
		try {
			artikelDto = getArtikelFac().artikelFindByPrimaryKeySmall((Integer) key, theClientDto);
		} catch (Exception e) {
			// Nicht gefunden
		}
		if (artikelDto != null) {
			DocPath docPath = new DocPath(new DocNodeArtikel(artikelDto));
			return new PrintInfoDto(docPath, null, getSTable());
		} else {
			return null;
		}
	}

	public String getSTable() {
		return "ARTIKEL";
	}

	private String getSiValue(String searchString) {
		DefaultSiPrefixParser spa = new DefaultSiPrefixParser();

		// first located si value
		return HelperServer.getDBValueFromBigDecimal(spa.parseFirst(searchString), 60);
	}

	private boolean getSiSortKrit() {
		SortierKriterium[] sortierKriterien = getQuery().getSortKrit();
		if (sortierKriterien == null)
			return false;

		for (SortierKriterium sortierKriterium : sortierKriterien) {
			if (sortierKriterium.kritName.equals("aspr.c_siwert")) {
				return true;
			}
		}
		return false;
	}
}
