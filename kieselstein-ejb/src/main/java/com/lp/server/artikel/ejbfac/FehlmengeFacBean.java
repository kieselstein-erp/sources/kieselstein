/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 * 
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 * 
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: developers@heliumv.com
 ******************************************************************************/
package com.lp.server.artikel.ejbfac;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.*;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.*;
import javax.persistence.criteria.*;

import com.lp.server.fertigung.fastlanereader.generated.FLRLos;
import com.lp.server.system.service.*;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.lp.server.artikel.ejb.Artikelfehlmenge;
import com.lp.server.artikel.ejb.Fasession;
import com.lp.server.artikel.ejb.Fasessioneintrag;
import com.lp.server.artikel.fastlanereader.generated.FLRArtikelbestellt;
import com.lp.server.artikel.fastlanereader.generated.FLRFehlmenge;
import com.lp.server.artikel.service.ArtikelDto;
import com.lp.server.artikel.service.ArtikelFac;
import com.lp.server.artikel.service.ArtikelfehlmengeDto;
import com.lp.server.artikel.service.ArtikelfehlmengeDtoAssembler;
import com.lp.server.artikel.service.FasessionDto;
import com.lp.server.artikel.service.FehlmengeFac;
import com.lp.server.artikel.service.LagerDto;
import com.lp.server.artikel.service.VerkaufspreisDto;
import com.lp.server.artikel.service.VkpreisfindungDto;
import com.lp.server.auftrag.service.AuftragDto;
import com.lp.server.bestellung.service.BestellpositionDto;
import com.lp.server.bestellung.service.BestellungDto;
import com.lp.server.fertigung.fastlanereader.generated.FLRLossollmaterial;
import com.lp.server.fertigung.service.FertigungFac;
import com.lp.server.fertigung.service.LosDto;
import com.lp.server.fertigung.service.LosistmaterialDto;
import com.lp.server.fertigung.service.LoslagerentnahmeDto;
import com.lp.server.fertigung.service.LossollarbeitsplanDto;
import com.lp.server.fertigung.service.LossollmaterialDto;
import com.lp.server.lieferschein.ejb.Lieferschein;
import com.lp.server.lieferschein.service.LieferscheinDto;
import com.lp.server.lieferschein.service.LieferscheinFac;
import com.lp.server.lieferschein.service.LieferscheinpositionDto;
import com.lp.server.lieferschein.service.LieferscheinpositionFac;
import com.lp.server.partner.service.KundeDto;
import com.lp.server.system.pkgenerator.PKConst;
import com.lp.server.util.Facade;
import com.lp.server.util.fastlanereader.FLRSessionFactory;
import com.lp.util.AufgeloesteFehlmengenDto;
import com.lp.util.EJBExceptionLP;
import com.lp.util.Helper;

@Stateless
public class FehlmengeFacBean extends Facade implements FehlmengeFac {
	@PersistenceContext
	private EntityManager em;

	@Resource
	private SessionContext context;

	public void createArtikelfehlmenge(ArtikelfehlmengeDto artikelfehlmengeDto) throws EJBExceptionLP {
		Integer iId = getPKGeneratorObj().getNextPrimaryKey(PKConst.PK_FEHLMENGE);
		artikelfehlmengeDto.setIId(iId);

		try {
			Artikelfehlmenge artikelfehlmenge = new Artikelfehlmenge(
				artikelfehlmengeDto.getIId(), artikelfehlmengeDto.getCBelegartnr(),
				artikelfehlmengeDto.getCBuchungsart(), artikelfehlmengeDto.getIBelegartpositionid(),
				artikelfehlmengeDto.getArtikelIId(), artikelfehlmengeDto.getNMenge(),
				artikelfehlmengeDto.getTLiefertermin()
			);
			em.persist(artikelfehlmenge);
			em.flush();
		} catch (EntityExistsException ex) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER_BEIM_ANLEGEN, ex);
		}
	}

	private void removeArtikelfehlmenge(ArtikelfehlmengeDto artikelfehlmengeDto) throws EJBExceptionLP {
		if (artikelfehlmengeDto != null) {
			Integer iId = artikelfehlmengeDto.getIId();
			Artikelfehlmenge toRemove = em.find(Artikelfehlmenge.class, iId);
			if (toRemove == null) {
				throw new EJBExceptionLP(EJBExceptionLP.FEHLER_BEI_FINDBYPRIMARYKEY,
						"Fehler bei removeArtikelfehlmenge. Es gibt keine iid " + iId + "\ndto.toString: "
								+ artikelfehlmengeDto.toString());
			}
			try {
				em.remove(toRemove);
				em.flush();
			} catch (EntityExistsException er) {
				throw new EJBExceptionLP(EJBExceptionLP.FEHLER_BEIM_LOESCHEN, er);
			}
		}
	}

	private void updateArtikelfehlmenge(ArtikelfehlmengeDto artikelfehlmengeDto) throws EJBExceptionLP {
		if (artikelfehlmengeDto != null) {
			Integer iId = artikelfehlmengeDto.getIId();
			Artikelfehlmenge artikelfehlmenge = em.find(Artikelfehlmenge.class, iId);

			if (artikelfehlmenge == null) {
				throw new EJBExceptionLP(
						EJBExceptionLP.FEHLER_BEIM_UPDATE,
						"Fehler bei updateArtikelfehlmenge. Es gibt keine iid " + iId + "\ndto.toString: "
								+ artikelfehlmengeDto.toString()
				);
			}
			updateArtikelfehlmengeFromArtikelfehlmengeDto(artikelfehlmenge, artikelfehlmengeDto);
		}
	}

	public void aktualisiereFehlmenge(String belegartCNr, Integer belegpositionIId, TheClientDto theClientDto)
			throws EJBExceptionLP, RemoteException {
		if (belegartCNr.equals(LocaleFac.BELEGART_LOS)) {
			// die position holen
			LossollmaterialDto losmat = getFertigungFac().lossollmaterialFindByPrimaryKeyOhneExc(belegpositionIId);
			if (losmat == null) {
				// entweder falsche id uebergeben oder die position wurde gerade
				// geloescht
				// also auch den eintrag in der fehlmengenliste loeschen
				try {
					removeArtikelfehlmenge(LocaleFac.BELEGART_LOS, belegpositionIId);

				} catch (Throwable ex) {
					// nix tun
					// das kann passieren wenn eine position geloescht wird, die
					// keinen artikel hat
				}
				return;
			}

			LosDto losDto = getFertigungFac().losFindByPrimaryKey(losmat.getLosIId());

			boolean bFehlmengeZulaessig = List.of(
				FertigungFac.STATUS_AUSGEGEBEN, FertigungFac.STATUS_IN_PRODUKTION,
				FertigungFac.STATUS_TEILERLEDIGT, FertigungFac.STATUS_GESTOPPT
			).contains(
				losDto.getStatusCNr()
			);

			ArtikelfehlmengeDto aFehlmengeDto;
			boolean bFehlmengeExists;

			// Eintrag suchen
			try {
				Query query = em.createNamedQuery("ArtikelfehlmengefindByBelegartCNrBelegartPositionIId");
				query.setParameter("belegartCNr", LocaleFac.BELEGART_LOS);
				query.setParameter("belegartpositionIId", losmat.getIId());
				Artikelfehlmenge aFehlmenge = (Artikelfehlmenge) query.getSingleResult();
				aFehlmengeDto = assembleArtikelfehlmengeDto(aFehlmenge);
				aFehlmengeDto.setArtikelIId(losmat.getArtikelIId());
				bFehlmengeExists = true;

			} catch (NoResultException ex) {
				bFehlmengeExists = false;
				// gibts noch nicht, also muss ein neuer her
				aFehlmengeDto = new ArtikelfehlmengeDto();
				aFehlmengeDto.setArtikelIId(losmat.getArtikelIId());
				aFehlmengeDto.setCBelegartnr(LocaleFac.BELEGART_LOS);
				aFehlmengeDto.setIBelegartpositionid(losmat.getIId());
			}
			// Weitere Aktionen nur dann, wenn das anlegen einer Fehlmenge
			// auch zulaessig ist
			if (bFehlmengeZulaessig) {
				java.sql.Date dTermin;

				BigDecimal bdAusgegeben = getFertigungFac().getAusgegebeneMenge(
						losmat.getIId(), null, theClientDto
				);

				BigDecimal newFehlmenge;
				String buchungsart;

				if (losmat.getNMenge().doubleValue() > 0) {
					BigDecimal fehlmenge = losmat.getNMenge().subtract(bdAusgegeben);
					if (fehlmenge.doubleValue() < 0) {
						ParametermandantDto parameter = getParameterFac().getMandantparameter(
								theClientDto.getMandant(), ParameterFac.KATEGORIE_FERTIGUNG, ParameterFac.PARAMETER_LOS_UEBERMENGE_ERLAUBEN
						);

						// If the parameter setting "LOS_UEBERMENGE_ERLAUBEN" is disabled - we don't create a ArtikelFehlmenge with
						// booking type 'Uebermenge'
						if (!parameter.asBoolean()) {
							fehlmenge = BigDecimal.ZERO;
						}

						dTermin = losDto.getTProduktionsende();
						buchungsart = FehlmengeFac.BUCHUNGSART_UEBERMENGE;
					} else {
						dTermin = getFertigungFac().getProduktionsbeginnAnhandZugehoerigemArbeitsgang(
								losDto.getTProduktionsbeginn(), losmat.getIId(), theClientDto
						);
						// TODO: WERNER ANRUFEN WEGEN OFFSET.. IM ALTEN CODE WAR ES AUCH BEI NEGATIVEN SOLLENGE ELSE UNTEN - MACHT DOCH NUR BEI FEHLMENGE SINN
						dTermin = Helper.addiereTageZuDatum(dTermin, losmat.getIBeginnterminoffset());
						buchungsart = FehlmengeFac.BUCHUNGSART_FEHLMENGE;
					}

					newFehlmenge = fehlmenge;

				} else {
					// Negative Fehlmenge: produktionsende
					dTermin = losDto.getTProduktionsende();
					BigDecimal fehlmenge = losmat.getNMenge().abs().subtract(bdAusgegeben.abs());
					buchungsart = FehlmengeFac.BUCHUNGSART_FEHLMENGE;

					if (fehlmenge.doubleValue() > 0) {
						newFehlmenge = losmat.getNMenge().subtract(bdAusgegeben);
					} else {
						newFehlmenge = BigDecimal.ZERO;
					}
				}

				// eintraege mit menge 0 loeschen bzw. gar nicht speichern
				if (newFehlmenge.doubleValue() == 0) {
					// steht in der db
					if (bFehlmengeExists) {
						removeArtikelfehlmenge(aFehlmengeDto);
					}
				} else {

					aFehlmengeDto.setTLiefertermin(dTermin);
					aFehlmengeDto.setNMenge(newFehlmenge);
					aFehlmengeDto.setCBuchungsart(buchungsart);

					if (bFehlmengeExists) {
						updateArtikelfehlmenge(aFehlmengeDto);
					} else {
						createArtikelfehlmenge(aFehlmengeDto);
					}
				}
			} else {
				if (bFehlmengeExists) {
					removeArtikelfehlmenge(aFehlmengeDto);
				}
			}
		}
	}

	public FasessionDto fasessionFindByPrimaryKey(Integer iId) {
		try {
			Fasession fasession = em.find(Fasession.class, iId);
			FasessionDto dto = new FasessionDto();
			dto.setIId(fasession.getIId());
			dto.setPersonalIId(fasession.getPersonalIId());
			dto.settBeginn(fasession.getTBeginn());
			dto.settGedruckt(fasession.getTGedruckt());
			return dto;
		} catch (Exception ex) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER_BEI_FINDBYPRIMARYKEY, ex);
		}
	}

	public ArtikelfehlmengeDto artikelfehlmengeFindByPrimaryKey(Integer iId) throws EJBExceptionLP {
		try {
			Artikelfehlmenge artikelfehlmenge = em.find(Artikelfehlmenge.class, iId);
			if (artikelfehlmenge == null) {
				throw new EJBExceptionLP(EJBExceptionLP.FEHLER_BEI_FINDBYPRIMARYKEY,
						"Fehler bei artikelfehlmengeFindByPrimaryKey. Es gibt keine iid " + iId);
			}
			return assembleArtikelfehlmengeDto(artikelfehlmenge);
		} catch (Exception ex) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER_BEI_FINDBYPRIMARYKEY, ex);
		}
	}

	private void updateArtikelfehlmengeFromArtikelfehlmengeDto(
			Artikelfehlmenge artikelfehlmenge,
			ArtikelfehlmengeDto artikelfehlmengeDto
	) {
		artikelfehlmenge.setCBelegartnr(artikelfehlmengeDto.getCBelegartnr());
		artikelfehlmenge.setCBuchungsart(artikelfehlmengeDto.getCBuchungsart());
		artikelfehlmenge.setIBelegartpositionid(artikelfehlmengeDto.getIBelegartpositionid());
		artikelfehlmenge.setArtikelIId(artikelfehlmengeDto.getArtikelIId());
		artikelfehlmenge.setNMenge(artikelfehlmengeDto.getNMenge());
		artikelfehlmenge.setTLiefertermin(artikelfehlmengeDto.getTLiefertermin());
		em.merge(artikelfehlmenge);
		em.flush();
	}

	private ArtikelfehlmengeDto assembleArtikelfehlmengeDto(Artikelfehlmenge artikelfehlmenge) {
		return ArtikelfehlmengeDtoAssembler.createDto(artikelfehlmenge);
	}

	public BigDecimal getAnzahlFehlmengeEinesArtikels(Integer artikelIId, TheClientDto theClientDto)
			throws EJBExceptionLP {
		return getAnzahlFehlmengeEinesArtikels(artikelIId, theClientDto.getMandant(), null);
	}

	public BigDecimal getAnzahlFehlmengeEinesArtikels(
			Integer artikelIId, String mandantCNr,
			Integer partnerIIdStandort
	) {
		TypedQuery<BigDecimal> query;

		if (partnerIIdStandort == null) {
			query = em.createNamedQuery("ArtikelfehlmengeSumMengeByArtikelIdMandantCNr", BigDecimal.class);
		} else {
			query = em.createNamedQuery("ArtikelfehlmengeSumMengeByArtikelIdMandantCNrPartnerIdStandort", BigDecimal.class);
			query.setParameter("partnerIIdStandort", partnerIIdStandort);
		}

		query.setParameter("artikelId", artikelIId);
		query.setParameter("mandantCNr", mandantCNr);

		BigDecimal result = query.getSingleResult();
		if (result == null) {
			result = BigDecimal.ZERO;
		}

		return result;
	}

	public BigDecimal getAnzahlderPositivenFehlmengenEinesArtikels(Integer artikelIId, TheClientDto theClientDto) {
		TypedQuery<BigDecimal> query = em.createNamedQuery("ArtikelfehlmengeNurPositiveSumMengeByArtikelIdMandantCNr", BigDecimal.class);
		query.setParameter("artikelId", artikelIId);
		query.setParameter("mandantCNr", theClientDto.getMandant());

		BigDecimal result = query.getSingleResult();
		if (result == null) {
			result = BigDecimal.ZERO;
		}

		return result;
	}

	public BigDecimal getAnzahlderUebermengenEinesArtikels(Integer artikelIId, TheClientDto theClientDto) {
		TypedQuery<BigDecimal> query = em.createNamedQuery("ArtikelfehlmengeNurSumMengeByArtikelIdMandantCNrBuchungsart", BigDecimal.class);
		query.setParameter("artikelId", artikelIId);
		query.setParameter("mandantCNr", theClientDto.getMandant());
		query.setParameter("buchungsart", FehlmengeFac.BUCHUNGSART_UEBERMENGE);

		BigDecimal result = query.getSingleResult();
		if (result == null) {
			result = BigDecimal.ZERO;
		}

		return result;
	}

	@org.jboss.ejb3.annotation.TransactionTimeout(1000)
	public TreeMap<String, BigDecimal> fuelleFehlmengenDesAnderenMandantenNach(String mandantCNr_Zielmandant,
			java.sql.Timestamp tStichtag, TheClientDto theClientDto) {

		TreeMap<String, BigDecimal> snrChnrArtikelAusgelassen = new TreeMap<String, BigDecimal>();

		tStichtag = Helper.cutTimestamp(Helper.addiereTageZuTimestamp(tStichtag, 1));

		try {

			HashMap<Integer, BigDecimal> hmArtikelMitFehlmengenUndReservierungen = new HashMap<Integer, BigDecimal>();

			LagerDto lagerDto_Hautptlager_Quellmandant = getLagerFac()
					.getHauptlagerEinesMandanten(theClientDto.getMandant());
			LagerDto lagerDto_Hautptlager_Zielmandant = getLagerFac()
					.getHauptlagerEinesMandanten(mandantCNr_Zielmandant);

			SessionFactory factory = FLRSessionFactory.getFactory();
			Session session = factory.openSession();

			String sQuery = "SELECT sum(fm.n_menge), artikel_i_id FROM FLRFehlmenge fm WHERE fm.flrlossollmaterial.flrlos.mandant_c_nr='"
					+ mandantCNr_Zielmandant + "' AND (fm.n_menge > 0 OR fm.c_buchungsart = :buchungsart_uebermenge) AND fm.t_liefertermin<'"
					+ Helper.formatTimestampWithSlashes(tStichtag) + "' GROUP BY fm.artikel_i_id";

			org.hibernate.Query query = session.createQuery(sQuery);
			query.setParameter("buchungsart_uebermenge", FehlmengeFac.BUCHUNGSART_UEBERMENGE);

			List<?> list = query.list();
			Iterator it = list.iterator();

			Integer lieferscheinIId_Ziellager = null;

			while (it.hasNext()) {
				Object[] o = (Object[]) it.next();

				BigDecimal summeFehlmenge = (BigDecimal) o[0];
				Integer artikelIId = (Integer) o[1];

				hmArtikelMitFehlmengenUndReservierungen.put(artikelIId, summeFehlmenge);

			}
			session.close();
			session = factory.openSession();
			// Es muessen auch Reservierungen ohne Fehlmengen beruecksichtigt
			// werden

			sQuery = " SELECT distinct ar.flrartikel.i_id FROM FLRArtikelreservierung ar WHERE ar.n_menge >0 AND ar.t_liefertermin<'"
					+ Helper.formatTimestampWithSlashes(tStichtag) + "'";

			query = session.createQuery(sQuery);

			list = query.list();
			it = list.iterator();

			while (it.hasNext()) {
				Integer artikelIId = (Integer) it.next();

				// SP2968
				// Reservierungen hinzufuegen
				BigDecimal bdReserviert = getReservierungFac().getAnzahlReservierungen(artikelIId, tStichtag,
						mandantCNr_Zielmandant);

				if (bdReserviert.doubleValue() > 0) {
					// Wenn noch nicht in Liste
					if (!hmArtikelMitFehlmengenUndReservierungen.containsKey(artikelIId)) {
						hmArtikelMitFehlmengenUndReservierungen.put(artikelIId, bdReserviert);
					} else {
						BigDecimal bdVorhanden = hmArtikelMitFehlmengenUndReservierungen.get(artikelIId);
						hmArtikelMitFehlmengenUndReservierungen.put(artikelIId, bdVorhanden.add(bdReserviert));
					}
				}
			}

			session.close();

			Iterator itHm = hmArtikelMitFehlmengenUndReservierungen.keySet().iterator();
			while (itHm.hasNext()) {

				Integer artikelIId = (Integer) itHm.next();

				// SP2991
				ArtikelDto aDto = getArtikelFac().artikelFindByPrimaryKeySmall(artikelIId, theClientDto);
				if (Helper.short2Boolean(aDto.getBLagerbewirtschaftet()) == true) {

					BigDecimal summeFehlmengeUndReservierungen = (BigDecimal) hmArtikelMitFehlmengenUndReservierungen
							.get(artikelIId);

					BigDecimal bdLagerstandQuellmandant = getLagerFac().getLagerstand(artikelIId,
							lagerDto_Hautptlager_Quellmandant.getIId(), theClientDto);
					ArtikelDto artikelDto = getArtikelFac().artikelFindByPrimaryKeySmall(artikelIId, theClientDto);

					BigDecimal bdLagerstandBeiZielMandant = getLagerFac().getLagerstand(artikelIId,
							lagerDto_Hautptlager_Zielmandant.getIId(), theClientDto);

					BigDecimal bdBestellt = getSummeBestellungenZielmandant(mandantCNr_Zielmandant, tStichtag,
							artikelIId);

					// Bestelltliste abziehen
					summeFehlmengeUndReservierungen = summeFehlmengeUndReservierungen.subtract(bdBestellt);

					// Nur wenn bei Zielmandant nicht genug auf Lager ist
					if (bdLagerstandBeiZielMandant.doubleValue() < summeFehlmengeUndReservierungen.doubleValue()
							&& bdLagerstandQuellmandant.doubleValue() > 0
							&& Helper.short2boolean(artikelDto.getBLagerbewirtschaftet())) {

						if (Helper.short2boolean(artikelDto.getBSeriennrtragend()) == true
								|| Helper.short2boolean(artikelDto.getBChargennrtragend()) == true) {

							snrChnrArtikelAusgelassen.put(artikelDto.formatArtikelbezeichnung(),
									summeFehlmengeUndReservierungen);

						} else {

							BigDecimal bdAbzubuchendeMenge = summeFehlmengeUndReservierungen
									.subtract(bdLagerstandBeiZielMandant);

							if (bdLagerstandQuellmandant.doubleValue() > 0) {

								BigDecimal bdUmbuchen = BigDecimal.ZERO;

								if (bdLagerstandQuellmandant.doubleValue() >= bdAbzubuchendeMenge.doubleValue()) {

									bdUmbuchen = bdAbzubuchendeMenge;
									bdAbzubuchendeMenge = BigDecimal.ZERO;
								} else {
									bdUmbuchen = bdLagerstandQuellmandant;
									bdAbzubuchendeMenge = bdAbzubuchendeMenge.subtract(bdLagerstandQuellmandant);
								}

								if (bdUmbuchen.doubleValue() > 0) {

									// Nun ZiellagerLieferschein erstellen Kunde
									// =
									// Mandant

									MandantDto mDto = getMandantFac().mandantFindByPrimaryKey(mandantCNr_Zielmandant,
											theClientDto);

									KundeDto kDto_Quellmandant = getKundeFac().kundeFindByiIdPartnercNrMandantOhneExc(
											mDto.getPartnerIId(), theClientDto.getMandant(), theClientDto);

									if (kDto_Quellmandant != null) {

										if (lieferscheinIId_Ziellager == null) {

											LieferscheinDto lieferscheinDto = new LieferscheinDto();
											lieferscheinDto.setKundeIIdLieferadresse(kDto_Quellmandant.getIId());
											lieferscheinDto.setKundeIIdRechnungsadresse(kDto_Quellmandant.getIId());
											lieferscheinDto.setLieferscheinartCNr(LieferscheinFac.LSART_FREI);
											lieferscheinDto.setPersonalIIdVertreter(theClientDto.getIDPersonal());

											lieferscheinDto.setLagerIId(lagerDto_Hautptlager_Quellmandant.getIId());

											// Ziellager
											lieferscheinDto.setZiellagerIId(lagerDto_Hautptlager_Zielmandant.getIId());

											lieferscheinDto.setWaehrungCNr(theClientDto.getSMandantenwaehrung());
											lieferscheinDto
													.setFWechselkursmandantwaehrungzubelegwaehrung(new Double(1));

											lieferscheinDto.setTBelegdatum(Helper
													.cutTimestamp(new java.sql.Timestamp(System.currentTimeMillis())));
											lieferscheinDto.setMandantCNr(theClientDto.getMandant());

											if (kDto_Quellmandant.getKostenstelleIId() != null) {
												lieferscheinDto
														.setKostenstelleIId(kDto_Quellmandant.getKostenstelleIId());
											} else {
												lieferscheinDto.setKostenstelleIId(mDto.getIIdKostenstelle());
											}

											lieferscheinDto.setStatusCNr(LocaleFac.STATUS_ANGELEGT);

											lieferscheinDto.setLieferartIId(kDto_Quellmandant.getLieferartIId());
											lieferscheinDto.setSpediteurIId(kDto_Quellmandant.getSpediteurIId());
											lieferscheinDto.setZahlungszielIId(kDto_Quellmandant.getZahlungszielIId());

											lieferscheinIId_Ziellager = getLieferscheinFac()
													.createLieferschein(lieferscheinDto, theClientDto);

										}

										Lieferschein ls = em.find(Lieferschein.class, lieferscheinIId_Ziellager);
										Timestamp belegDatum = ls.getTBelegdatum();

//										MwstsatzDto mwstsatzDtoAktuell = getMandantFac()
//												.mwstsatzFindByMwstsatzbezIIdAktuellster(
//														kDto_Quellmandant.getMwstsatzbezIId(), theClientDto);
										MwstsatzDto mwstsatzDtoAktuell = getMandantFac().mwstsatzZuDatumValidate(
												kDto_Quellmandant.getMwstsatzbezIId(), belegDatum, theClientDto);

										// Position anlegen
										LieferscheinpositionDto lieferscheinposDto = new LieferscheinpositionDto();
										lieferscheinposDto.setLieferscheinIId(lieferscheinIId_Ziellager);
										lieferscheinposDto.setPositionsartCNr(
												LieferscheinpositionFac.LIEFERSCHEINPOSITIONSART_IDENT);
										lieferscheinposDto.setNMenge(bdUmbuchen);

										VkpreisfindungDto vkpreisfindungDto = getVkPreisfindungFac()
												.verkaufspreisfindung(artikelDto.getIId(), kDto_Quellmandant.getIId(),
														bdUmbuchen,
//														new Date(System.currentTimeMillis()),
														new Date(belegDatum.getTime()),
														kDto_Quellmandant.getVkpfArtikelpreislisteIIdStdpreisliste(),
														mwstsatzDtoAktuell.getIId(),
														theClientDto.getSMandantenwaehrung(), theClientDto);

										VerkaufspreisDto kundenVKPreisDto = Helper
												.getVkpreisBerechnet(vkpreisfindungDto);
										BigDecimal preis = BigDecimal.ZERO;
										BigDecimal materialzuschlag = BigDecimal.ZERO;

										if (kundenVKPreisDto != null && kundenVKPreisDto.nettopreis != null) {
											preis = kundenVKPreisDto.nettopreis;
										}

										if (kundenVKPreisDto != null && kundenVKPreisDto.bdMaterialzuschlag != null) {
											materialzuschlag = kundenVKPreisDto.bdMaterialzuschlag;
											lieferscheinposDto
													.setNMaterialzuschlag(kundenVKPreisDto.bdMaterialzuschlag);
										}

										lieferscheinposDto.setNEinzelpreis(preis);

										lieferscheinposDto.setNNettoeinzelpreis(preis.add(materialzuschlag));

										lieferscheinposDto.setFRabattsatz(0.0);
										lieferscheinposDto.setFZusatzrabattsatz(0.0);
										lieferscheinposDto.setBNettopreisuebersteuert(Helper.boolean2Short(false));
										lieferscheinposDto.setArtikelIId(artikelDto.getIId());
										lieferscheinposDto.setEinheitCNr(artikelDto.getEinheitCNr());

										// Ausser der Kunde hat MWST-Satz mit
										// 0%,
										// dann muss
										// dieser
										// verwendet werden

										BigDecimal mwstBetrag = new BigDecimal(0);

										lieferscheinposDto.setMwstsatzIId(mwstsatzDtoAktuell.getIId());

										mwstBetrag = preis.add(materialzuschlag).multiply(
												new BigDecimal(mwstsatzDtoAktuell.getFMwstsatz()).movePointLeft(2));

										lieferscheinposDto
												.setNBruttoeinzelpreis(preis.add(materialzuschlag).add(mwstBetrag));
										lieferscheinposDto.setNMwstbetrag(mwstBetrag);

										getLieferscheinpositionFac().createLieferscheinposition(lieferscheinposDto,
												true, theClientDto);

									} else {
										// FEHLERMELDUNG
										throw new EJBExceptionLP(
												EJBExceptionLP.FEHLER_LIEFERSCHEIN_ANDERN_MANDANT_NACHFUELLEN_MANDANT_KEIN_KUNDE,
												"FEHLER_LIEFERSCHEIN_ANDERN_MANDANT_NACHFUELLEN_MANDANT_KEIN_KUNDE");
									}

								}
							}

						}
					}
				}
			}
		} catch (RemoteException e) {
			throwEJBExceptionLPRespectOld(e);
		}

		return snrChnrArtikelAusgelassen;

	}

	public BigDecimal getSummeBestellungenZielmandant(String mandantCNr_Zielmandant, java.sql.Timestamp tStichtag,
			Integer artikelIId) throws RemoteException {
		BigDecimal bdBestellt = BigDecimal.ZERO;

		Collection<FLRArtikelbestellt> c = (Collection<FLRArtikelbestellt>) getArtikelbestelltFac()
				.getArtikelbestellt(artikelIId, null, new Date(tStichtag.getTime()));

		Iterator itBest = c.iterator();
		while (itBest.hasNext()) {
			FLRArtikelbestellt flr = (FLRArtikelbestellt) itBest.next();

			if (flr.getC_belegartnr().equals(LocaleFac.BELEGART_BESTELLUNG)) {

				BestellpositionDto bestposDto = getBestellpositionFac()
						.bestellpositionFindByPrimaryKeyOhneExc(flr.getI_belegartpositionid());
				if (bestposDto != null) {

					BestellungDto besDto = getBestellungFac().bestellungFindByPrimaryKey(bestposDto.getBelegIId());

					if (besDto.getMandantCNr().equals(mandantCNr_Zielmandant)) {
						bdBestellt = bdBestellt.add(flr.getN_menge());
					}

				}

			}

		}
		return bdBestellt;
	}

	public Integer alleFehlmengenDesMandantenAufloesen(TheClientDto theClientDto) {

		SessionFactory factory = FLRSessionFactory.getFactory();
		Session session = factory.openSession();
		Criteria c = session.createCriteria(FLRFehlmenge.class);

		TreeMap<String, ArrayList<AufgeloesteFehlmengenDto>> tmAufgeloesteFehlmengen = new TreeMap<String, ArrayList<AufgeloesteFehlmengenDto>>();
		ArrayList<AufgeloesteFehlmengenDto> alDaten = new ArrayList<AufgeloesteFehlmengenDto>();

		try {

			// Filter nach Mandant
			c.createCriteria(ArtikelFac.FLR_FEHLMENGE_FLRLOSSOLLMATERIAL)
					.createCriteria(FertigungFac.FLR_LOSSOLLMATERIAL_FLRLOS)
					.add(Restrictions.eq(FertigungFac.FLR_LOS_MANDANT_C_NR, theClientDto.getMandant()))
					.addOrder(Order.asc(FertigungFac.FLR_LOS_T_PRODUKTIONSBEGINN));
			c.add(Restrictions.gt(ArtikelFac.FLR_FEHLMENGE_N_MENGE, BigDecimal.ZERO));

			List<?> list = c.list();
			Iterator it = list.iterator();
			while (it.hasNext()) {
				FLRFehlmenge fm = (FLRFehlmenge) it.next();

				if (Helper.short2boolean(fm.getFlrartikel().getB_seriennrtragend()) == false) {
					if (Helper.short2boolean(fm.getFlrartikel().getB_chargennrtragend()) == false) {
						BigDecimal bdAbzubuchendeMenge = fm.getN_menge();

						LossollmaterialDto sollDto = getFertigungFac()
								.lossollmaterialFindByPrimaryKey(fm.getFlrlossollmaterial().getI_id());
						ArtikelDto artikelDto = getArtikelFac().artikelFindByPrimaryKeySmall(fm.getArtikel_i_id(),
								theClientDto);
						LosDto losDto = getFertigungFac()
								.losFindByPrimaryKey(fm.getFlrlossollmaterial().getFlrlos().getI_id());

						LoslagerentnahmeDto[] loslagerentnahmeDtos = getFertigungFac()
								.loslagerentnahmeFindByLosIId(losDto.getIId());

						for (int i = 0; i < loslagerentnahmeDtos.length; i++) {

							// SP4361
							BigDecimal bdLagerstand = null;
							if (Helper.short2boolean(fm.getFlrartikel().getB_lagerbewirtschaftet())) {
								bdLagerstand = getLagerFac().getLagerstand(fm.getArtikel_i_id(),
										loslagerentnahmeDtos[i].getLagerIId(), theClientDto);
							} else {
								bdLagerstand = new BigDecimal(9999999);
							}

							if (bdLagerstand.doubleValue() > 0) {

								LosistmaterialDto losistmaterialDto = new LosistmaterialDto();
								losistmaterialDto.setLossollmaterialIId(sollDto.getIId());
								losistmaterialDto.setLagerIId(loslagerentnahmeDtos[i].getLagerIId());

								if (bdLagerstand.doubleValue() >= bdAbzubuchendeMenge.doubleValue()) {
									losistmaterialDto.setNMenge(bdAbzubuchendeMenge);
									bdAbzubuchendeMenge = BigDecimal.ZERO;
								} else {
									losistmaterialDto.setNMenge(bdLagerstand);
									bdAbzubuchendeMenge = bdAbzubuchendeMenge.subtract(bdLagerstand);
								}
								losistmaterialDto.setBAbgang(Helper.boolean2Short(true));

								getFertigungFac().gebeMaterialNachtraeglichAus(sollDto, losistmaterialDto, null, true,
										theClientDto);

								LagerDto lagerDto = getLagerFac()
										.lagerFindByPrimaryKey(loslagerentnahmeDtos[i].getLagerIId());

								AufgeloesteFehlmengenDto aufgeloesteFehlmengenDto = new AufgeloesteFehlmengenDto();
								aufgeloesteFehlmengenDto.setArtikelDto(artikelDto);
								aufgeloesteFehlmengenDto.setArtikelCNr(artikelDto.getCNr());
								aufgeloesteFehlmengenDto.setLagerDto(lagerDto);
								aufgeloesteFehlmengenDto.setLagerCNr(lagerDto.getCNr());
								aufgeloesteFehlmengenDto.setLosDto(losDto);
								aufgeloesteFehlmengenDto.setLosCNr(losDto.getCNr());
								aufgeloesteFehlmengenDto.setAufgeloesteMenge(losistmaterialDto.getNMenge());
								aufgeloesteFehlmengenDto.setSSeriennrChnr(null);
								aufgeloesteFehlmengenDto.setLosDto(losDto);

								addAufgeloesteFehlmengeZuSession(aufgeloesteFehlmengenDto, theClientDto);

								if (bdAbzubuchendeMenge.doubleValue() <= 0) {
									break;
								}
							}
						}

					}
				}

			}

		} catch (RemoteException e) {
			throwEJBExceptionLPRespectOld(e);
		}

		return istOffeneFasessionVorhanden(theClientDto);
	}

	public ArrayList getFehlmengen(Integer artikelIId, String mandantCNr, TheClientDto theClientDto)
			throws EJBExceptionLP {
		Session session = null;

		try {
			SessionFactory factory = FLRSessionFactory.getFactory();
			session = factory.openSession();
			Criteria c = session.createCriteria(FLRFehlmenge.class);
			// Filter nach Artikel
			c.add(Restrictions.eq(ArtikelFac.FLR_FEHLMENGE_ARTIKEL_I_ID, artikelIId));
			// Filter nach Mandant
			c.createCriteria(ArtikelFac.FLR_FEHLMENGE_FLRLOSSOLLMATERIAL)
					.createCriteria(FertigungFac.FLR_LOSSOLLMATERIAL_FLRLOS)
					.add(Restrictions.eq(FertigungFac.FLR_LOS_MANDANT_C_NR, mandantCNr));
			// Sortierung nach Liefertermin
			c.addOrder(Order.asc(ArtikelFac.FLR_FEHLMENGE_T_LIEFERTERMIN));
			List<?> list = c.list();
			return new ArrayList(list);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public Integer istOffeneFasessionVorhanden(TheClientDto theClientDto) {
		Query query = em.createNamedQuery(Fasession.FindOffeneSession);
		query.setParameter(1, theClientDto.getIDPersonal());

		Collection<?> cl = query.getResultList();

		if (cl.size() > 0) {

			Integer fasessionIId = ((Fasession) cl.iterator().next()).getIId();

			Query query2 = em.createNamedQuery(Fasessioneintrag.FindByFasessionIId);
			query2.setParameter(1, fasessionIId);

			Collection<?> cl2 = query2.getResultList();

			// SP4459
			if (cl2.size() > 0) {
				return fasessionIId;
			} else {
				schliesseAufgeloesteFehlmengenSessionAb(fasessionIId, theClientDto);
				return null;
			}

		} else {
			return null;
		}
	}

	public TreeMap getAufgeloesteFehlmengenEinerSession(Integer fasessionIId, TheClientDto theClientDto) {

		TreeMap<String, ArrayList> tmAufgeloesteFehlmengen = new TreeMap<String, ArrayList>();

		Query query = em.createNamedQuery(Fasessioneintrag.FindByFasessionIId);
		query.setParameter(1, fasessionIId);

		Collection<?> cl = query.getResultList();

		Iterator it = cl.iterator();
		while (it.hasNext()) {
			Fasessioneintrag eintrag = (Fasessioneintrag) it.next();

			try {

				AufgeloesteFehlmengenDto aufgeloesteFehlmengenDto = new AufgeloesteFehlmengenDto();
				String key = "";
				if (eintrag.getAuftragIId() != null) {
					AuftragDto aDto = getAuftragFac().auftragFindByPrimaryKey(eintrag.getAuftragIId());
					key = "A" + aDto.getCNr();

					aufgeloesteFehlmengenDto.setAuftagDto(aDto);

				} else if (eintrag.getLosIId() != null) {
					LosDto lDto = getFertigungFac().losFindByPrimaryKey(eintrag.getLosIId());
					key = "L" + lDto.getCNr();
					aufgeloesteFehlmengenDto.setLosDto(lDto);
					aufgeloesteFehlmengenDto.setLosCNr(lDto.getCNr());

					if (eintrag.getArtikelIIdOffenerag() != null) {
						ArtikelDto aDtoOffenerAg = getArtikelFac()
								.artikelFindByPrimaryKey(eintrag.getArtikelIIdOffenerag(), theClientDto);
						aufgeloesteFehlmengenDto.setArtikelDtoErsterOffenerAG(aDtoOffenerAg);

						key += " " + aDtoOffenerAg.getCNr();

					}

				}

				if (eintrag.getLieferscheinIId() != null) {

					aufgeloesteFehlmengenDto.setLieferscheinDto(
							getLieferscheinFac().lieferscheinFindByPrimaryKey(eintrag.getLieferscheinIId()));

				}

				ArtikelDto aDto = getArtikelFac().artikelFindByPrimaryKey(eintrag.getArtikelIId(), theClientDto);

				LagerDto lagerDto = getLagerFac().lagerFindByPrimaryKey(eintrag.getLagerIId());

				BigDecimal bdLagerstand = null;
				if (aDto.isLagerbewirtschaftet()) {
					bdLagerstand = getLagerFac().getLagerstand(eintrag.getArtikelIId(), lagerDto.getIId(),
							theClientDto);
				} else {
					bdLagerstand = new BigDecimal(9999999);
				}

				aufgeloesteFehlmengenDto.setTAnlegen(eintrag.getTAnlegen());

				aufgeloesteFehlmengenDto.setKurzzeichenPersonalAnlegen(getPersonalFac()
						.personalFindByPrimaryKeySmall(eintrag.getPersonalIIdAnlegen()).getCKurzzeichen());

				aufgeloesteFehlmengenDto.setArtikelDto(aDto);
				aufgeloesteFehlmengenDto.setArtikelCNr(aDto.getCNr());
				aufgeloesteFehlmengenDto.setLagerDto(lagerDto);
				aufgeloesteFehlmengenDto.setLagerstand(bdLagerstand);
				aufgeloesteFehlmengenDto.setLagerCNr(lagerDto.getCNr());
				aufgeloesteFehlmengenDto.setAufgeloesteMenge(eintrag.getNMenge());
				aufgeloesteFehlmengenDto.setSSeriennrChnr(Helper.erzeugeStringArrayAusString(eintrag.getCSnrchnr()));

				if (tmAufgeloesteFehlmengen.containsKey(key)) {
					ArrayList<AufgeloesteFehlmengenDto> al = (ArrayList<AufgeloesteFehlmengenDto>) tmAufgeloesteFehlmengen
							.get(key);
					al.add(aufgeloesteFehlmengenDto);
					tmAufgeloesteFehlmengen.put(key, al);
				} else {
					ArrayList<AufgeloesteFehlmengenDto> al = new ArrayList<AufgeloesteFehlmengenDto>();
					al.add(aufgeloesteFehlmengenDto);
					tmAufgeloesteFehlmengen.put(key, al);
				}

			} catch (RemoteException e) {
				throwEJBExceptionLPRespectOld(e);
			}

		}

		return tmAufgeloesteFehlmengen;
	}

	public void addAufgeloesteFehlmengeZuSession(AufgeloesteFehlmengenDto dto, TheClientDto theClientDto) {

		// Zuerst pruefen obe s fuer mich eine offen Session gibt

		Query query = em.createNamedQuery(Fasession.FindOffeneSession);
		query.setParameter(1, theClientDto.getIDPersonal());

		Collection<?> cl = query.getResultList();

		Fasession fasession = null;

		if (cl.size() > 0) {
			// zu vorhandener hinzufuegen
			fasession = (Fasession) cl.iterator().next();

		} else {
			// Eine neue anlegen

			Integer iId = getPKGeneratorObj().getNextPrimaryKey(PKConst.PK_FASESSION);

			fasession = new Fasession(iId, theClientDto.getIDPersonal(), new Timestamp(System.currentTimeMillis()));
			em.persist(fasession);
			em.flush();
		}

		// Nun Eintrag hinzufuegen
		Fasessioneintrag fasessioneintrag = new Fasessioneintrag(
				getPKGeneratorObj().getNextPrimaryKey(PKConst.PK_FASESSIONEINTRAG), fasession.getIId(),
				dto.getArtikelDto().getIId(), dto.getLagerDto().getIId(), dto.getAufgeloesteMenge(),
				theClientDto.getIDPersonal(), new Timestamp(System.currentTimeMillis()));

		if (dto.getLieferscheinDto() != null) {
			fasessioneintrag.setLieferscheinIId(dto.getLieferscheinDto().getIId());
		}

		if (dto.getAuftagDto() != null) {
			fasessioneintrag.setAuftragIId(dto.getAuftagDto().getIId());
		} else if (dto.getLosDto() != null) {
			fasessioneintrag.setLosIId(dto.getLosDto().getIId());

			// PJ20182
			try {
				LossollarbeitsplanDto[] sollarbeitsplanDtos = getFertigungFac()
						.lossollarbeitsplanFindByLosIId(dto.getLosDto().getIId());

				if (sollarbeitsplanDtos.length > 0) {
					for (int i = 0; i < sollarbeitsplanDtos.length; i++) {
						LossollarbeitsplanDto saDto = sollarbeitsplanDtos[i];
						if (Helper.short2boolean(saDto.getBFertig()) == false) {
							fasessioneintrag.setArtikelIIdOffenerag(saDto.getArtikelIIdTaetigkeit());
							break;
						}
					}
				}
			} catch (RemoteException e) {
				throwEJBExceptionLPRespectOld(e);
			}

		}

		if (dto.getSSeriennrChnr() != null) {
			fasessioneintrag.setCSnrchnr(Helper.erzeugeStringAusStringArray(dto.getSSeriennrChnr()));
		}

		em.persist(fasessioneintrag);
		em.flush();

	}

	public void schliesseAufgeloesteFehlmengenSessionAb(Integer fasessionIId, TheClientDto theClientDto) {

		Fasession fasession = em.find(Fasession.class, fasessionIId);

		if (fasession.getTGedruckt() == null) {
			fasession.setTGedruckt(new Timestamp(System.currentTimeMillis()));

			em.persist(fasession);
			em.flush();
		}

	}

	public ArtikelfehlmengeDto artikelfehlmengeFindByBelegartCNrBelegartPositionIId(
			String belegartCNr, Integer belegartpositionIId, String buchungsart, Boolean handleException
	) throws EJBExceptionLP
	{
		if (belegartpositionIId == null || belegartCNr == null) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER_FELD_DARF_NICHT_NULL_SEIN,
					new Exception("belegartpositionIId == null || belegartCNr == null"));
		}
		try {
			Query query;

			if (buchungsart == null) {
				query = em.createNamedQuery("ArtikelfehlmengefindByBelegartCNrBelegartPositionIId");
			} else {
				query = em.createNamedQuery("ArtikelfehlmengefindByBelegartCNrBelegartPositionIIdBuchungsart");
				query.setParameter("buchungsart", buchungsart);
			}
			query.setParameter("belegartCNr", belegartCNr);
			query.setParameter("belegartpositionIId", belegartpositionIId);

			Artikelfehlmenge artikelfehlmenge = (Artikelfehlmenge) query.getSingleResult();

			if (!handleException && artikelfehlmenge == null) {
				throw new EJBExceptionLP(EJBExceptionLP.FEHLER_BEI_FIND,
						"Fehler bei artikelfehlmengeFindByBelegartCNrBelegartPositionIId (mit buchungsart= " + buchungsart + "). Es gibt keine Artikelfehlmenge"
								+ "mit belegartpositionIId " + belegartpositionIId + " und belegart " + belegartCNr);
			}

			return assembleArtikelfehlmengeDto(artikelfehlmenge);
		} catch (NoResultException ex) {
			if (handleException) {
				return null;
			} else {
				throw ex;
			}
		}
	}

	private void removeArtikelfehlmenge(String belegartCNr, Integer belegartpositionIId) throws EJBExceptionLP {
		myLogger.entry();
		if (belegartCNr == null || belegartpositionIId == null) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER_PKFIELD_IS_NULL,
					new Exception("belegartCNr == null || belegartpositionIId == null"));
		}
		ArtikelfehlmengeDto a = artikelfehlmengeFindByBelegartCNrBelegartPositionIId(belegartCNr, belegartpositionIId, null, false);
		removeArtikelfehlmenge(a);
	}

	/**
	 * Fehlmengen komplett neu erzeugen
	 * 
	 * @param theClientDto der aktuelle Benutzer
	 * @throws EJBExceptionLP
	 */
	@org.jboss.ejb3.annotation.TransactionTimeout(3600)
	public void pruefeFehlmengen(TheClientDto theClientDto) throws EJBExceptionLP {
		Session session = null;

		try {
			session = FLRSessionFactory.getFactory().openSession();

			String hqlDelete = "delete FROM FLRFehlmenge";
			session.createQuery(hqlDelete).executeUpdate();

			session.close();
			session = FLRSessionFactory.getFactory().openSession();

			CriteriaBuilder criteriaBuilder  = session.getCriteriaBuilder();
			CriteriaQuery<Integer> query = criteriaBuilder.createQuery(Integer.class);
			Root<FLRLossollmaterial> root = query.from(FLRLossollmaterial.class);
			Join<FLRLossollmaterial, FLRLos> losJoin = root.join("flrlos", JoinType.INNER);
			query.select(root.get("i_id"));
			query.where(losJoin.get("status_c_nr").in(
					FertigungFac.STATUS_AUSGEGEBEN, FertigungFac.STATUS_IN_PRODUKTION, FertigungFac.STATUS_TEILERLEDIGT, FertigungFac.STATUS_GESTOPPT
			));

			for (Integer losSollMaterialID : session.createQuery(query).getResultList()) {
				aktualisiereFehlmenge(LocaleFac.BELEGART_LOS, losSollMaterialID, theClientDto);
			}
		} catch (RemoteException ex) {
			throwEJBExceptionLPRespectOld(ex);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
}
