package com.lp.server.personal.ejb;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PERS_FAHRZEUGVERWENDUNGSART")
public class Fahrzeugverwendungsart implements Serializable {
	@Id
	@Column(name = "C_NR", columnDefinition = "CHAR(15) NOT NULL")
	private String cNr;

	private static final long serialVersionUID = 1L;

	public Fahrzeugverwendungsart() {
		super();
	}

	public void setCNr(String cNr) {
		this.cNr = cNr;
	}

	public String getCNr() {
		return cNr;
	}   

}
