package com.lp.server.personal.service;

import java.io.Serializable;
import java.math.BigDecimal;

public class ZeitverteilenGutSchlechtDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer lossollarbeitsplanIId = null;
	private BigDecimal gutStueck = null;
	private BigDecimal schlechtStueck=null;
	private Boolean isFertig = false;

	public ZeitverteilenGutSchlechtDto(Integer lossollarbeitsplanIId, BigDecimal gutStueck, BigDecimal schlechtStueck, Boolean isFertig) {
		
		this.lossollarbeitsplanIId = lossollarbeitsplanIId;
		this.gutStueck = gutStueck;
		this.schlechtStueck = schlechtStueck;
		this.isFertig = isFertig;
	}
	public Integer getLossollarbeitsplanIId() {
		return lossollarbeitsplanIId;
	}
	public BigDecimal getGutStueck() {
		return gutStueck;
	}
	public BigDecimal getSchlechtStueck() {
		return schlechtStueck;
	}
	public Boolean getIsFertig() { return  isFertig; }
}
