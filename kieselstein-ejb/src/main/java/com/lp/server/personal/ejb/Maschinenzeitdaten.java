/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 * 
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 * 
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: developers@heliumv.com
 ******************************************************************************/
package com.lp.server.personal.ejb;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries( {
	@NamedQuery(name = "MaschinenzeitdatenfindLetzeOffeneMaschinenzeitdaten", query = "SELECT OBJECT(c) FROM Maschinenzeitdaten c WHERE c.maschineIId = ?1 AND c.tBis IS NULL ORDER BY c.tVon DESC"),
	@NamedQuery(name = "MaschinenzeitdatenfindMaschinenzeitdatenNachZeitpunkt", query = "SELECT OBJECT(c) FROM Maschinenzeitdaten c WHERE c.maschineIId = ?1 AND c.tVon >?2"),
	@NamedQuery(name = "MaschinenzeitdatenfindByLossollarbeitsplanIId", query = "SELECT OBJECT(c) FROM Maschinenzeitdaten c WHERE c.lossollarbeitsplanIId = ?1"),
	@NamedQuery(name = "MaschinenzeitdatenfindByPersonalIIdGestartet", query = "SELECT OBJECT(c) FROM Maschinenzeitdaten c WHERE c.personalIIdGestartet = ?1 AND c.tVon <?2 AND c.tBis IS NULL"),
	@NamedQuery(name = "MaschinenzeitdatenfindLetzeMaschinenzeitdatenNachZeitpunkt", query = "SELECT OBJECT(c) FROM Maschinenzeitdaten c WHERE c.maschineIId = ?1 AND c.tBis > ?2 ORDER BY c.tVon DESC"),
	@NamedQuery(name = "MaschinenzeitdatenfindLetzteByMaschineIIdLossollarbeitsplanIId", query = "SELECT OBJECT(c) FROM Maschinenzeitdaten c WHERE c.maschineIId = ?1 AND c.lossollarbeitsplanIId = ?2 ORDER BY c.tVon DESC")})


@Entity
@Table(name = "PERS_MASCHINENZEITDATEN")
public class Maschinenzeitdaten implements Serializable {
	@Id
	@Column(name = "I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer iId;

	@Column(name = "T_VON", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tVon;

	@Column(name = "T_BIS", columnDefinition = "TIMESTAMP")
	private Timestamp tBis;

	@Column(name = "C_BEMERKUNG", columnDefinition = "VARCHAR(80)")
	private String cBemerkung;

	@Column(name = "T_ANLEGEN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAnlegen;

	@Column(name = "T_AENDERN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAendern;

	@Column(name = "MASCHINE_I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer maschineIId;

	@Column(name = "PERSONAL_I_ID_AENDERN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAendern;

	@Column(name = "PERSONAL_I_ID_ANLEGEN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAnlegen;

	@Column(name = "PERSONAL_I_ID_GESTARTET", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdGestartet;

	@Column(name = "B_PARALLEL", columnDefinition = "SMALLINT NOT NULL")
	private Short bParallel;

	public Short getBParallel() {
		return bParallel;
	}

	public void setBParallel(Short bParallel) {
		this.bParallel = bParallel;
	}

	public Integer getPersonalIIdGestartet() {
		return personalIIdGestartet;
	}

	public void setPersonalIIdGestartet(Integer personalIIdGestartet) {
		this.personalIIdGestartet = personalIIdGestartet;
	}

	@Column(name = "LOSSOLLARBEITSPLAN_I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer lossollarbeitsplanIId;

	
	@Column(name = "PERSONAL_I_ID_ERLEDIGT", columnDefinition = "INTEGER")
	private Integer personalIIdErledigt;
	
	@Column(name = "T_ERLEDIGT", columnDefinition = "TIMESTAMP")
	private Timestamp tErledigt;
	
	@Column(name = "F_VERRECHENBAR", columnDefinition = "DOUBLE PRECISION")
	private Double fVerrechenbar;
	

	public Integer getPersonalIIdErledigt() {
		return personalIIdErledigt;
	}

	public void setPersonalIIdErledigt(Integer personalIIdErledigt) {
		this.personalIIdErledigt = personalIIdErledigt;
	}

	public Timestamp getTErledigt() {
		return tErledigt;
	}

	public void setTErledigt(Timestamp tErledigt) {
		this.tErledigt = tErledigt;
	}

	public Double getFVerrechenbar() {
		return fVerrechenbar;
	}

	public void setFVerrechenbar(Double fVerrechenbar) {
		this.fVerrechenbar = fVerrechenbar;
	}
	
	private static final long serialVersionUID = 1L;

	public Maschinenzeitdaten() {
		super();
	}

	public Maschinenzeitdaten(Integer id, Integer maschineIId, Timestamp tVon,
			Integer personalIIdAnlegen2,
			Integer personalIIdAendern2, Integer lossollarbeitsplanIId,
			java.sql.Timestamp tAendern, java.sql.Timestamp tAnlegen, Integer personalIIdGestartet, Short bParallel) {
		setIId(id);
		setLossollarbeitsplanIId(lossollarbeitsplanIId);
		setTVon(tVon);
		setMaschineIId(maschineIId);
		setPersonalIIdAnlegen(personalIIdAnlegen2);
		setPersonalIIdAendern(personalIIdAendern2);
		setTAendern(tAendern);
		setTAnlegen(tAnlegen);
		setPersonalIIdGestartet(personalIIdGestartet);
		setBParallel(bParallel);

	}

	public Integer getIId() {
		return this.iId;
	}

	public void setIId(Integer iId) {
		this.iId = iId;
	}

	public Timestamp getTAnlegen() {
		return this.tAnlegen;
	}

	public void setTAnlegen(Timestamp tAnlegen) {
		this.tAnlegen = tAnlegen;
	}

	public Timestamp getTAendern() {
		return this.tAendern;
	}

	public void setTAendern(Timestamp tAendern) {
		this.tAendern = tAendern;
	}

	public Integer getMaschineIId() {
		return this.maschineIId;
	}

	public void setMaschineIId(Integer maschineIId) {
		this.maschineIId = maschineIId;
	}

	public Integer getPersonalIIdAendern() {
		return this.personalIIdAendern;
	}

	public void setPersonalIIdAendern(Integer personalIIdAendern) {
		this.personalIIdAendern = personalIIdAendern;
	}

	public Integer getPersonalIIdAnlegen() {
		return this.personalIIdAnlegen;
	}

	public void setPersonalIIdAnlegen(Integer personalIIdAnlegen) {
		this.personalIIdAnlegen = personalIIdAnlegen;
	}

	public Timestamp getTVon() {
		return tVon;
	}

	public Integer getLossollarbeitsplanIId() {
		return lossollarbeitsplanIId;
	}

	public void setLossollarbeitsplanIId(Integer lossollarbeitsplanIId) {
		this.lossollarbeitsplanIId = lossollarbeitsplanIId;
	}

	public void setTVon(Timestamp von) {
		tVon = von;
	}

	public Timestamp getTBis() {
		return tBis;
	}

	public void setTBis(Timestamp bis) {
		tBis = bis;
	}

	public String getCBemerkung() {
		return cBemerkung;
	}

	public void setCBemerkung(String bemerkung) {
		cBemerkung = bemerkung;
	}

}
