package com.lp.server.system.service;

import java.io.Serializable;
import java.math.BigDecimal;

public class RegionDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer iID;

    private String cNr;

    private BigDecimal bdUmsatzziel;

    public Integer getIID() {
        return iID;
    }

    public void setIID(Integer iID) {
        this.iID = iID;
    }

    public String getCNr() {
        return cNr;
    }

    public void setCNr(String cNr) {
        this.cNr = cNr;
    }

    public BigDecimal getBdUmsatzziel() {
        return bdUmsatzziel;
    }

    public void setBdUmsatzziel(BigDecimal bdUmsatzziel) {
        this.bdUmsatzziel = bdUmsatzziel;
    }

    public int hashCode() {
        int result = 17;
        result = 37 * result + cNr.hashCode();
        result = 37 * result + bdUmsatzziel.hashCode();
        return result;
    }

    public String toString() {
        String returnString = "";
        returnString += cNr;
        returnString += ", " + bdUmsatzziel;
        return returnString;
    }
}
