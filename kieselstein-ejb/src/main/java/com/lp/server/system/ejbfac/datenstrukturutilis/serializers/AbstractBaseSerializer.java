package com.lp.server.system.ejbfac.datenstrukturutilis.serializers;

import com.lp.server.system.ejb.DatenStrukturFeld;
import com.lp.server.system.ejbfac.datenstrukturutilis.exports.BaseExportDataUtil;
import com.lp.server.system.service.DatenStrukturDto;
import com.lp.service.HttpStatusCodeException;
import org.apache.commons.io.IOUtils;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

abstract public class AbstractBaseSerializer {

    protected final String encoding = "UTF-8";
    protected final Map<String, Object> globalParameter = new HashMap<>();
    //protected final String encoding = "Windows-1252";

    protected final Map<Integer, Map<String, String>> strukturFeldValueMappings;

    public AbstractBaseSerializer(Map<Integer, Map<String, String>> strukturFeldValueMappings) {
        this.strukturFeldValueMappings = strukturFeldValueMappings;
    }

    public void putGlobalFieldValue(String name, Object value) {
        globalParameter.put(name, value);
    }

    protected String parseOutputValue2String(Object value, String formatMaske) {
        if (value != null) {
            if (formatMaske != null && !formatMaske.isEmpty()) {
                if (value instanceof Date) {
                    if (formatMaske.contains("%t")) {
                        // Support String Format Function Format-Mask
                        int foundParamPos = formatMaske.indexOf("%t");
                        List<Object> params = new ArrayList<>();
                        while (foundParamPos >= 0) {
                            params.add(value);
                            foundParamPos = formatMaske.indexOf("%t", foundParamPos + 1);
                        }
                        return String.format(formatMaske, params.toArray());
                    } else {
                        // Or Use Simple-Date-Formater
                        SimpleDateFormat formatter = new SimpleDateFormat(formatMaske);
                        return formatter.format(value);
                    }
                } else {
                    int foundParamPos = formatMaske.indexOf("%");
                    List<Object> params = new ArrayList<>();
                    while (foundParamPos >= 0) {
                        params.add(value);
                        foundParamPos = formatMaske.indexOf("%", foundParamPos + 1);
                    }
                    return String.format(formatMaske, params.toArray());
                }
            }
            return String.valueOf(value);
        }
        return null;
    }

    protected String getStrukturFeldValueAsString(BaseExportDataUtil dataUtil, DatenStrukturFeld sf, String defaultDateFormatMask, String defaultNumberFormatMask) {
        String value = sf.getFixWert();
        if (sf.getERPFeld() != null && !sf.getERPFeld().isEmpty()) {
            String formatMaske = sf.getFormatMaske();
            Object outputValue = dataUtil.getElementOutputValue(sf.getERPFeld());
            if (outputValue != null && (formatMaske == null || formatMaske.trim().isEmpty())) {
                if (outputValue instanceof Date) {
                    formatMaske = defaultDateFormatMask;
                } else if (outputValue instanceof Number) {
                    formatMaske = defaultNumberFormatMask;
                }
            }

            value = parseOutputValue2String(outputValue, formatMaske);
            if (value == null) {
                value = "";
            }

            if (sf.getMappingId() != null && strukturFeldValueMappings != null && strukturFeldValueMappings.containsKey(sf.getMappingId())) {
                Map<String, String> mapValues = strukturFeldValueMappings.get(sf.getMappingId());
                if (mapValues.containsKey(value)) {
                    value = mapValues.get(value);
                }
            }
        }
        return value;
    }

    protected Object parseValueString(String value, DatenStrukturFeld sf) {
        if (value != null && !value.isEmpty()) {
            if (value.equals(sf.getFixWert()) && this.globalParameter.containsKey(sf.getFixWert())) {
                Object o = this.globalParameter.get(sf.getFixWert());
                if (!(o instanceof String)) {
                    return o;
                } else {
                    value = (String) o;
                }
            }
            if (sf.getFormatMaske() != null && !sf.getFormatMaske().isEmpty()) {
                SimpleDateFormat sdf = new SimpleDateFormat(sf.getFormatMaske().trim());
                try {
                    return sdf.parse(value.trim());
                } catch (ParseException e) {
                    // Is no Valid Date, return the value is will be handled later.
                    return value;
                }
            }
        }
        return value;
    }

    public abstract void exportToFile(String fileName, List<BaseExportDataUtil> data, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder) throws IOException;

    public abstract String exportToString(List<BaseExportDataUtil> data, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder);

    public byte[] exportToBytes(List<BaseExportDataUtil> data, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder) throws UnsupportedEncodingException {
        return exportToString(data, struktur, strukturFelder).getBytes(encoding);
    }

    protected abstract ContentType getContentType();

    public void exportToUrl(String url, List<BaseExportDataUtil> data, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder) throws HttpStatusCodeException, IOException {
        HttpPost httpPost = new HttpPost(url);
        byte[] date = this.exportToBytes(data, struktur, strukturFelder);
        httpPost.setEntity(new ByteArrayEntity(date, this.getContentType()));
        try (CloseableHttpClient client = HttpClients.createDefault(); CloseableHttpResponse response = client.execute(httpPost)) {
            StatusLine statusLine = response.getStatusLine();
            int status = statusLine.getStatusCode();
            if (status < 200 || status >= 300) {
                throw new HttpStatusCodeException(url, "POST", status, statusLine.getReasonPhrase());
            }
        }
    }

    protected Object handleImportDataMappingValues(Object value, DatenStrukturFeld sf) {
        if (value != null && sf.getMappingId() != null && strukturFeldValueMappings != null && strukturFeldValueMappings.containsKey(sf.getMappingId())) {
            Map<String, String> mapValues = strukturFeldValueMappings.get(sf.getMappingId());
            // Handling Mapping-Values
            if (mapValues.containsKey(value.toString())) {
                value = mapValues.get(value.toString());
            }
        }
        return value;
    }

    public abstract List<Map<String, Object>> buildImportDataFromFile(String fileName, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder);

    public abstract List<Map<String, Object>> buildImportDataFromString(String dataString, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder);

    public List<Map<String, Object>>buildImportDataFromUrl(String url, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder) throws UnsupportedEncodingException {
        try {
            URLConnection con = new URL(url).openConnection();
            InputStream in = con.getInputStream();
            String encoding = con.getContentEncoding();
            encoding = encoding == null ? "UTF-8" : encoding;
            String body = IOUtils.toString(in, encoding);
            return buildImportDataFromString(body, struktur, strukturFelder);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
