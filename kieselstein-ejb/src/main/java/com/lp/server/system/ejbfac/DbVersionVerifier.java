package com.lp.server.system.ejbfac;

import com.lp.server.system.service.SystemFac;
import com.lp.server.util.Facade;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class DbVersionVerifier extends Facade {
    @EJB
    private SystemFac systemFac;

    @PostConstruct
    public void init() {
        // systemFac.verifyServerVersion();
    }
}
