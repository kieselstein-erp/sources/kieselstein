package com.lp.server.system.ejbfac;

import com.lp.server.system.ejb.*;
import com.lp.server.system.pkgenerator.PKConst;
import com.lp.server.system.service.*;
import com.lp.server.util.Facade;
import com.lp.util.EJBExceptionLP;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.util.*;

import static com.lp.server.benutzer.service.RechteFac.RECHT_LP_SYSTEM_CUD;

@Stateless
public class DatenStrukturFacBean extends Facade implements DatenStrukturFac {
	@PersistenceContext
	private EntityManager em;

	@Override
	public DatenStrukturDto createStruktur(DatenStrukturDto strukturDto, TheClientDto client) throws EJBExceptionLP {
		checkPermissions(RECHT_LP_SYSTEM_CUD, client);

		Integer id = getPKGeneratorObj().getNextPrimaryKey(PKConst.PK_DATEN_STRUKTUR);
		DatenStruktur instance = new DatenStruktur(
			id, strukturDto.getBezeichnung(), strukturDto.getBereich(), strukturDto.getListItemPfad(), strukturDto.getDefaultDatumFormat(),
			strukturDto.getDefaultNummerFormat(), client.getIDPersonal()
		);

		em.persist(instance);
		em.flush();
		return BaseDtoAssambler.mapEntity2Dto(instance, DatenStrukturDto.class);
	}

	@Override
	public void deleteStruktur(Integer id, TheClientDto client) throws EJBExceptionLP {
		checkPermissions(RECHT_LP_SYSTEM_CUD, client);
		DatenStruktur instance = em.find(DatenStruktur.class, id);
		em.remove(instance);
		em.flush();
	}

	@Override
	public void updateStruktur(Integer id, String bezeichnung, String bereich, String defaultDatumFormat, String defaultNummerFormat, TheClientDto client) throws EJBExceptionLP {
		// TODO CREATE DTO
		checkPermissions(RECHT_LP_SYSTEM_CUD, client);

		DatenStruktur instance = em.find(DatenStruktur.class, id);
		instance.setBezeichnung(bezeichnung);
		instance.setBereich(bereich);
		instance.setDefaultDatumFormat(defaultDatumFormat);
		instance.setDefaultNummerFormat(defaultNummerFormat);
		instance.setPersonalIIdAendern(client.getIDPersonal());
		instance.setAendern(new Timestamp(System.currentTimeMillis()));
		em.merge(instance);
		em.flush();
	}

	@Override
	public DatenStrukturFeldDto createStrukturFeld(DatenStrukturFeldDto datenStrukturFeldDto, TheClientDto client) throws EJBExceptionLP {
		checkPermissions(RECHT_LP_SYSTEM_CUD, client);
		Integer id = getPKGeneratorObj().getNextPrimaryKey(PKConst.PK_DATEN_STRUKTUR_FELD);
		DatenStrukturFeld instance = new DatenStrukturFeld(
				id, datenStrukturFeldDto.getStrukturDatenId(), datenStrukturFeldDto.getERPFeld(),
				datenStrukturFeldDto.getFixWert(), datenStrukturFeldDto.getPfad(), datenStrukturFeldDto.getAttributName(),
				datenStrukturFeldDto.getSort(), datenStrukturFeldDto.getFormatMaske(), datenStrukturFeldDto.getMappingId(),
				client.getIDPersonal()
		);
		em.persist(instance);
		em.flush();
		return DatenStrukturFeldDtoAssembler.createDto(instance);
	}

	public Integer createMapping(String bezeichnung, Map<String, String> mapValues, TheClientDto client) throws EJBExceptionLP {
		checkPermissions(RECHT_LP_SYSTEM_CUD, client);
		Integer id = getPKGeneratorObj().getNextPrimaryKey(PKConst.PK_MAPPING);
		Mapping mapping = new Mapping(id, bezeichnung, client.getIDPersonal());
		em.persist(mapping);

		for (Map.Entry<String, String> entry : mapValues.entrySet()) {
			Integer mw_id = getPKGeneratorObj().getNextPrimaryKey(PKConst.PK_MAPPING_WERT);
			MappingWert mw = new MappingWert(mw_id, id, entry.getKey(), entry.getValue(), client.getIDPersonal());
			em.persist(mw);
		}

		em.flush();
		return id;
	}
}
