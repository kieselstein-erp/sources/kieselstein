package com.lp.server.system.ejb;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@NamedQueries( { @NamedQuery(name = "DatenStrukturFelderFindByStrukturId", query = "SELECT OBJECT(sf) FROM DatenStrukturFeld sf WHERE sf.strukturDatenId = :strukturDatenId ORDER BY sf.iSort") })
@Entity
@Table(name = "LP_DATEN_STRUKTUR_FELD")
public class DatenStrukturFeld implements Serializable {
	@Id
	@Column(name = "I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer iId;

	@Column(name = "STRUKTUR_DATEN_I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer strukturDatenId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "STRUKTUR_DATEN_I_ID", insertable = false, updatable = false)
	private DatenStruktur datenStruktur;

	@Column(name = "C_ERP_FELD", columnDefinition = "VARCHAR(64)")
	private String cERPFeld;

	@Column(name = "C_FIX_WERT", columnDefinition = "VARCHAR(128)")
	private String cFixWert;

	@Column(name = "C_PFAD", columnDefinition = "VARCHAR(128)")
	private String cPfad;

	@Column(name = "C_ATTRIBUT_NAME", columnDefinition = "VARCHAR(128)")
	private String cAttributName;

	@Column(name = "I_SORT", columnDefinition = "INTEGER NOT NULL")
	private Integer iSort;

	@Column(name = "C_FORMAT_MASKE", columnDefinition = "VARCHAR(32)")
	private String cFormatMaske;

	@Column(name = "MAPPING_I_ID", columnDefinition = "INTEGER")
	private Integer mappingIId;

	@Column(name = "PERSONAL_I_ID_ANLEGEN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAnlegen;

	@Column(name = "PERSONAL_I_ID_AENDERN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAendern;

	@Column(name = "T_ANLEGEN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAnlegen ;

	@Column(name = "T_AENDERN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAendern;


	public DatenStrukturFeld() {
		super();
	}

	public DatenStrukturFeld(Integer id, Integer strukturDatenId,
                             String erpFeld, String fixWert, String pfad, String attributName,
							 Integer sort, String formatMaske, Integer mappingId,
                             Integer personalIIdAnlegen) {
		setId(id);
		setStrukturDatenId(strukturDatenId);
		setERPFeld(erpFeld);
		setFixWert(fixWert);
		setPfad(pfad);
		setAttributName(attributName);
		setSort(sort);
		setFormatMaske(formatMaske);
		setPersonalIIdAnlegen(personalIIdAnlegen);
		setPersonalIIdAendern(personalIIdAnlegen);
		setMappingId(mappingId);
		// Setzen der NOT NULL Felder
		Timestamp now = new Timestamp(System.currentTimeMillis());
		setAendern(now);
		setAnlegen(now);
	}

	public Integer getId() {
		return this.iId;
	}

	public void setId(Integer iId) {
		this.iId = iId;
	}

	public Integer getStrukturDatenId() {
		return strukturDatenId;
	}

	public void setStrukturDatenId(Integer strukturDatenId) {
		this.strukturDatenId = strukturDatenId;
	}

	public String getERPFeld() {
		return cERPFeld;
	}

	public void setERPFeld(String cERPFeld) {
		this.cERPFeld = cERPFeld;
	}

	public String getFixWert() {
		return cFixWert;
	}

	public void setFixWert(String cFixWert) {
		this.cFixWert = cFixWert;
	}

	public String getPfad() {
		return cPfad;
	}

	public void setPfad(String cPfad) {
		this.cPfad = cPfad;
	}

	public String getAttributName() {
		return cAttributName;
	}

	public void setAttributName(String cAttributName) {
		this.cAttributName = cAttributName;
	}

	public Integer getSort() {
		return iSort;
	}

	public void setSort(Integer iSort) {
		this.iSort = iSort;
	}

	public String getFormatMaske() {
		return cFormatMaske;
	}

	public void setFormatMaske(String cFormatMaske) {
		this.cFormatMaske = cFormatMaske;
	}

	public Integer getMappingId() {
		return mappingIId;
	}

	public void setMappingId(Integer mappingIId) {
		this.mappingIId = mappingIId;
	}

	public Integer getPersonalIIdAnlegen() {
		return personalIIdAnlegen;
	}

	public void setPersonalIIdAnlegen(Integer personalIIdAnlegen) {
		this.personalIIdAnlegen = personalIIdAnlegen;
	}

	public Integer getPersonalIIdAendern() {
		return personalIIdAendern;
	}

	public void setPersonalIIdAendern(Integer personalIIdAendern) {
		this.personalIIdAendern = personalIIdAendern;
	}

	public Timestamp getAnlegen() {
		return tAnlegen;
	}

	public void setAnlegen(Timestamp tAnlegen) {
		this.tAnlegen = tAnlegen;
	}

	public Timestamp getAendern() {
		return tAendern;
	}

	public void setAendern(Timestamp tAendern) {
		this.tAendern = tAendern;
	}
}
