/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 * 
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 * 
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: developers@heliumv.com
 ******************************************************************************/
package com.lp.server.system.ejb;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LP_ANWENDER")
public class Anwender implements Serializable {
	@Id
	@Column(name = "I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer iId;

	@Column(name = "server_version", columnDefinition = "VARCHAR NOT NULL")
	private String serverVersion;

	@Column(name = "database_version", columnDefinition = "VARCHAR NOT NULL")
	private String databaseVersion;

	@Column(name = "client_version", columnDefinition = "VARCHAR NOT NULL")
	private String clientVersion;

	@Column(name = "T_AENDERN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAendern;

	@Column(name = "MANDANT_C_NR_HAUPTMANDANT", columnDefinition = "VARCHAR(3)")
	private String mandantCNrHauptmandant;

	@Column(name = "PERSONAL_I_ID_AENDERN", columnDefinition = "INTEGER")
	private Integer personalIIdAendern;

	@Column(name = "T_ABLAUF", columnDefinition = "TIMESTAMP")
	private Timestamp tAblauf;
	
	@Column(name = "T_SUBSCRIPTION", columnDefinition = "TIMESTAMP")
	private Timestamp tSubscription;
	
	@Column(name = "O_CODE")
	private byte[] oCode; 
	
 	@Column(name= "O_HASH")
	private byte[] oHash;

 	@Column(name = "O_HINTERGRUND")
	private byte[] oHintergrund;
 	
	@Column(name = "I_SERVERID", columnDefinition = "INTEGER")
	private Integer serverId;

	private static final long serialVersionUID = 1L;

	public Anwender() {
		super();
	}

	public Anwender(Integer id,
			String serverVersion,
			String databaseVersion,
			String clientVersion) {
		setTAendern(new Timestamp(System.currentTimeMillis()));
		setIId(id);
		setServerVersion(serverVersion);
		setDatabaseVersion(databaseVersion);
		setClientVersion(clientVersion);
	}
	
	public Timestamp getTSubscription() {
		return tSubscription;
	}

	public byte[] getOHintergrund() {
		return oHintergrund;
	}

	public Integer getIId() {
		return this.iId;
	}

	public void setIId(Integer iId) {
		this.iId = iId;
	}

	public String getServerVersion() {
		return serverVersion;
	}

	public void setServerVersion(String version) {
		this.serverVersion = version;
	}

	public String getDatabaseVersion() {
		return databaseVersion;
	}

	public void setDatabaseVersion(String version) {
		this.databaseVersion = version;
	}

	public String getClientVersion() {
		return clientVersion;
	}

	public void setClientVersion(String version) {
		this.clientVersion = version;
	}

	public Timestamp getTAendern() {
		return this.tAendern;
	}

	public void setTAendern(Timestamp tAendern) {
		this.tAendern = tAendern;
	}

	public String getMandantCNrHauptmandant() {
		return this.mandantCNrHauptmandant;
	}

	public void setMandantCNrHauptmandant(String mandantCNrHauptmandant) {
		this.mandantCNrHauptmandant = mandantCNrHauptmandant;
	}

	public Integer getPersonalIIdAendern() {
		return this.personalIIdAendern;
	}

	public void setPersonalIIdAendern(Integer personalIIdAendern) {
		this.personalIIdAendern = personalIIdAendern;
	}

	public Timestamp getTAblauf() {
		return tAblauf;
	}

	public byte[] getOCode() {
		return oCode;
	}

	public byte[] getOHash() {
		return oHash;
	}
	
	public Integer getServerId() {
		return serverId;
	}
	
	public void setServerId(Integer serverId) {
		this.serverId = serverId;
	}
}
