/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 * 
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 * 
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: developers@heliumv.com
 ******************************************************************************/
package com.lp.server.system.ejb;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries( {
		@NamedQuery(name = "EinheitfindAll", query = "SELECT OBJECT(o) FROM Einheit o"),
		@NamedQuery(name = "EinheitFindByCNr", query = "SELECT OBJECT(o) FROM Einheit o WHERE o.cNr=?1"),
		@NamedQuery(name = "EinheitFindByCNrOrBezeichnung",
				query = "SELECT OBJECT(o) FROM Einheit o " +
						"WHERE o.cNr = :cNrOrBez " +
						"   OR EXISTS (SELECT 7 FROM Einheitspr spr WHERE spr.pk.einheitCNr = o.cNr AND spr.cBez = :cNrOrBez) " +
						"ORDER BY CASE WHEN o.cNr = :cNrOrBez THEN 1" +
						"              WHEN EXISTS (SELECT 7 FROM Einheitspr spr WHERE spr.pk.einheitCNr = o.cNr AND spr.cBez = :cNrOrBez AND spr.pk.localeCNr = :localeCNr) THEN 2 " +
						"              ELSE 999" +
						"          END")
})
@Entity
@Table(name = "LP_EINHEIT")
public class Einheit implements Serializable {
	@Id
	@Column(name = "C_NR", columnDefinition = "CHAR(15) NOT NULL")
	private String cNr;

	@Column(name = "I_DIMENSION", columnDefinition = "INTEGER NOT NULL")
	private Integer iDimension;

	@Column(name = "T_ANLEGEN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAnlegen;

	@Column(name = "T_AENDERN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAendern;

	@Column(name = "PERSONAL_I_ID_AENDERN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAendern;

	@Column(name = "PERSONAL_I_ID_ANLEGEN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAnlegen;

	@Column(name = "C_ZUGFERD_EINHEIT", columnDefinition = "CHAR(15)")
	private String cZugferdEinheit;

	private static final long serialVersionUID = 1L;

	public Einheit() {
		super();
	}

	public Einheit(String nr, Integer personalIIdAnlegen2,
			Integer personalIIdAendern2, Integer dimension) {
		setCNr(nr);
		// die ts anlegen, aendern nur am server
		setTAnlegen(new Timestamp(System.currentTimeMillis()));
		setTAendern(new Timestamp(System.currentTimeMillis()));

		setPersonalIIdAnlegen(personalIIdAnlegen2);
		setPersonalIIdAendern(personalIIdAendern2);
		setIDimension(dimension);
	}

	public String getCNr() {
		return this.cNr;
	}

	public void setCNr(String cNr) {
		this.cNr = cNr;
	}

	public Integer getIDimension() {
		return this.iDimension;
	}

	public void setIDimension(Integer iDimension) {
		this.iDimension = iDimension;
	}

	public Timestamp getTAnlegen() {
		return this.tAnlegen;
	}

	public void setTAnlegen(Timestamp tAnlegen) {
		this.tAnlegen = tAnlegen;
	}

	public Timestamp getTAendern() {
		return this.tAendern;
	}

	public void setTAendern(Timestamp tAendern) {
		this.tAendern = tAendern;
	}

	public Integer getPersonalIIdAendern() {
		return this.personalIIdAendern;
	}

	public void setPersonalIIdAendern(Integer personalIIdAendern) {
		this.personalIIdAendern = personalIIdAendern;
	}

	public Integer getPersonalIIdAnlegen() {
		return this.personalIIdAnlegen;
	}

	public void setPersonalIIdAnlegen(Integer personalIIdAnlegen) {
		this.personalIIdAnlegen = personalIIdAnlegen;
	}

	public String getCZugferdEinheit() {
		return cZugferdEinheit;
	}

	public void setCZugferdEinheit(String cZugferdEinheit) {
		this.cZugferdEinheit = cZugferdEinheit;
	}

}
