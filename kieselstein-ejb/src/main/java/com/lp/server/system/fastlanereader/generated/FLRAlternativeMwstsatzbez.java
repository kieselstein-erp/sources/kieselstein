/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 * 
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 * 
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: developers@heliumv.com
 ******************************************************************************/
package com.lp.server.system.fastlanereader.generated;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.Objects;

/** @author Hibernate CodeGenerator */
public class FLRAlternativeMwstsatzbez implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	/** identifier field */
	private Integer i_id;

	private Integer mwstsatzbez_id;
	private Integer alternaive_mwstsatzbez_id;


	private com.lp.server.system.fastlanereader.generated.FLRMwstsatzbez flrmwstsatzbez;

	private com.lp.server.system.fastlanereader.generated.FLRMwstsatzbez flrmwstsatzAlternativebez;


	/** full constructor */
	public FLRAlternativeMwstsatzbez(
			Integer mwstsatzbez_id,
			Integer alternaive_mwstsatzbez_id,
			com.lp.server.system.fastlanereader.generated.FLRMwstsatzbez flrmwstsatzbez,
			com.lp.server.system.fastlanereader.generated.FLRMwstsatzbez flrmwstsatzAlternativebez) {
		this.mwstsatzbez_id = mwstsatzbez_id;
		this.alternaive_mwstsatzbez_id = alternaive_mwstsatzbez_id;
		this.flrmwstsatzbez = flrmwstsatzbez;
		this.flrmwstsatzAlternativebez = flrmwstsatzAlternativebez;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		FLRAlternativeMwstsatzbez that = (FLRAlternativeMwstsatzbez) o;
		return Objects.equals(i_id, that.i_id) && Objects.equals(mwstsatzbez_id, that.mwstsatzbez_id) && Objects.equals(alternaive_mwstsatzbez_id, that.alternaive_mwstsatzbez_id) && Objects.equals(flrmwstsatzbez, that.flrmwstsatzbez) && Objects.equals(flrmwstsatzAlternativebez, that.flrmwstsatzAlternativebez);
	}

	@Override
	public int hashCode() {
		return Objects.hash(i_id, mwstsatzbez_id, alternaive_mwstsatzbez_id, flrmwstsatzbez, flrmwstsatzAlternativebez);
	}

	/** default constructor */
	public FLRAlternativeMwstsatzbez() {
	}

	public FLRMwstsatzbez getFlrmwstsatzbez() {
		return flrmwstsatzbez;
	}

	public void setFlrmwstsatzbez(FLRMwstsatzbez flrmwstsatzbez) {
		this.flrmwstsatzbez = flrmwstsatzbez;
	}

	public FLRMwstsatzbez getFlrmwstsatzAlternativebez() {
		return flrmwstsatzAlternativebez;
	}

	public void setFlrmwstsatzAlternativebez(FLRMwstsatzbez flrmwstsatzAlternativebez) {
		this.flrmwstsatzAlternativebez = flrmwstsatzAlternativebez;
	}

	public Integer getI_id() {
		return this.i_id;
	}

	public void setI_id(Integer i_id) {
		this.i_id = i_id;
	}

	public Integer getMwstsatzbez_id() {
		return mwstsatzbez_id;
	}

	public void setMwstsatzbez_id(Integer mwstsatzbez_id) {
		this.mwstsatzbez_id = mwstsatzbez_id;
	}

	public Integer getAlternaive_mwstsatzbez_id() {
		return alternaive_mwstsatzbez_id;
	}

	public void setAlternaive_mwstsatzbez_id(Integer alternaive_mwstsatzbez_id) {
		this.alternaive_mwstsatzbez_id = alternaive_mwstsatzbez_id;
	}


	public String toString() {
		return new ToStringBuilder(this).append("i_id", getI_id()).toString();
	}

}
