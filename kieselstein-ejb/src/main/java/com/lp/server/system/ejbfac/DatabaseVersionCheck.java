package com.lp.server.system.ejbfac;

import com.lp.server.system.service.SystemFac;
import com.lp.server.util.Facade;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class DatabaseVersionCheck extends Facade {
    @EJB
    private SystemFac systemFac;

    @PostConstruct
    public void init() {
       boolean doCheck = true;
       String skipDatabaseVersionCheck = System.getenv("SKIP_DATABASE_VERSION_CHECK");
       if (skipDatabaseVersionCheck != null) {
           doCheck = !"true".equalsIgnoreCase(skipDatabaseVersionCheck);
       }
       if (doCheck) {
           // Maybe catch exception and shut down server instead of letting it run
           // just so we can display the user a message...
           systemFac.verifyServerVersion();
       }
    }
}
