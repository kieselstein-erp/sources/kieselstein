package com.lp.server.system.fastlanereader;

import com.lp.server.system.fastlanereader.generated.FLRRegion;
import com.lp.server.system.service.SystemFac;
import com.lp.server.util.fastlanereader.FLRSessionFactory;
import com.lp.server.util.fastlanereader.UseCaseHandler;
import com.lp.server.util.fastlanereader.service.query.*;
import com.lp.util.EJBExceptionLP;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

public class RegionHandler extends UseCaseHandler {

    private static final long serialVersionUID = 1L;
    private final String FROM_CLAUSE = " from FLRRegion region ";
    private final String SELECT_COUNT = "select count(*) ";

    @Override
    protected long getRowCountFromDataBase() {
        long rowCount = 0;
        SessionFactory factory = FLRSessionFactory.getFactory();
        Session session = null;
        try {
            session = factory.openSession();
            session = setFilter(session);
            String queryString = SELECT_COUNT + FROM_CLAUSE + this.buildWhereClause();

            Query query = session.createQuery(queryString);
            List<?> rowCountResult = query.list();
            if (rowCountResult != null && !rowCountResult.isEmpty()) {
                rowCount = ((Long) rowCountResult.get(0));
            }
        } catch (Exception e) {
            throw new EJBExceptionLP(EJBExceptionLP.FEHLER, e);
        } finally {
            closeSession(session);
        }
        return rowCount;
    }

    @Override
    public QueryResult sort(SortierKriterium[] sortierKriterien, Object selectedId) throws EJBExceptionLP {

        this.getQuery().setSortKrit(sortierKriterien);

        QueryResult result = null;
        int rowNumber = 0;

        if (selectedId != null) {
            SessionFactory factory = FLRSessionFactory.getFactory();
            Session session = null;

            try {
                session = factory.openSession();
                session = setFilter(session);
                String queryString = "select region.i_id from FLRRegion region " + this.buildWhereClause() + this.buildOrderByClause();

                Query query = session.createQuery(queryString);
                ScrollableResults scrollableResult = query.scroll();
                if (scrollableResult != null) {
                    scrollableResult.beforeFirst();
                    while (scrollableResult.next()) {
                        Integer id = scrollableResult.getInteger(0);
                        if (selectedId.equals(id)) {
                            rowNumber = scrollableResult.getRowNumber();
                            break;
                        }
                    }
                }

            } catch (Exception e) {
                throw new EJBExceptionLP(EJBExceptionLP.FEHLER_FLR, e);
            } finally {
                closeSession(session);
            }
        }

        if (rowNumber < 0 || rowNumber >= this.getRowCount()) {
            rowNumber = 0;
        }

        result = this.getPageAt(new Integer(rowNumber));
        result.setIndexOfSelectedRow(rowNumber);

        return result;
    }

    private String buildOrderByClause(){
        StringBuffer orderBy = new StringBuffer("");
        if (this.getQuery() != null) {
            SortierKriterium[] kriterien = this.getQuery().getSortKrit();
            boolean sortAdded = false;
            if (kriterien != null && kriterien.length > 0) {
                for (int i = 0; i < kriterien.length; i++) {
                    if (kriterien[i].isKrit) {
                        if (sortAdded) {
                            orderBy.append(", ");
                        }
                        sortAdded = true;
                        orderBy.append(" region." + kriterien[i].kritName + " " + kriterien[i].value);
                    }
                }
            } else {
                orderBy.append(" region.i_id ASC ");
                sortAdded = true;
            }
            if (orderBy.indexOf(" region.i_id") < 0) {
                if (sortAdded) {
                    orderBy.append(", ");
                }
                orderBy.append(" region.i_id ");
            }
            orderBy.insert(0, " ORDER BY ");
        }
        return orderBy.toString();
    }
    @Override
    public QueryResult getPageAt(Integer rowIndex) throws EJBExceptionLP {
        QueryResult result = null;
        SessionFactory factory = FLRSessionFactory.getFactory();
        Session session = null;
        try {
            int colCount = getTableInfo().getColumnClasses().length;
            int pageSize = getLimit();
            int startIndex = getStartIndex(rowIndex, pageSize);
            int endIndex = startIndex + pageSize - 1;

            session = factory.openSession();
            session = setFilter(session);
            String queryString = FROM_CLAUSE + this.buildWhereClause() + this.buildOrderByClause();

            Query query = session.createQuery(queryString);

            query.setFirstResult(startIndex);
            query.setMaxResults(pageSize);
            List<?> resultList = query.list();
            Iterator<?> resultListIterator = resultList.iterator();
            Object[][] rows = new Object[resultList.size()][colCount];
            int row = 0, col= 0;
            while (resultListIterator.hasNext()) {
                FLRRegion region = (FLRRegion) resultListIterator.next();
                rows[row][col++] = region.getI_id();
                rows[row][col++] = region.getC_nr();
                rows[row][col++] = region.getN_umsatzziel();
                col = 0;
                row++;
            }
            result = new QueryResult(rows, this.getRowCount(), startIndex, endIndex, 0);
        } catch (Exception e) {
            throw new EJBExceptionLP(EJBExceptionLP.FEHLER_FLR, e);
        } finally {
            closeSession(session);
        }
        return result;
    }

    /**
     * buildWhereClause
     *
     * @return String
     */
    private String buildWhereClause() {
        StringBuffer where = new StringBuffer("");

        if (this.getQuery() != null && this.getQuery().getFilterBlock() != null
                && this.getQuery().getFilterBlock().filterKrit != null) {

            FilterBlock filterBlock = this.getQuery().getFilterBlock();
            FilterKriterium[] filterKriterien = this.getQuery().getFilterBlock().filterKrit;
            String booleanOperator = filterBlock.boolOperator;
            boolean filterAdded = false;

            for (int i = 0; i < filterKriterien.length; i++) {
                if (filterKriterien[i].isKrit) {
                    if (filterAdded) {
                        where.append(" " + booleanOperator);
                    }
                    filterAdded = true;
                    if (filterKriterien[i].isBIgnoreCase()) {
                        where.append(" lower(region." + filterKriterien[i].kritName + ")");
                    } else {
                        where.append(" region." + filterKriterien[i].kritName);
                    }
                    where.append(" " + filterKriterien[i].operator);
                    if (filterKriterien[i].isBIgnoreCase()) {
                        where.append(" " + filterKriterien[i].value.toLowerCase());
                    } else {
                        where.append(" " + filterKriterien[i].value);
                    }
                }
            }
            if (filterAdded) {
                where.insert(0, " WHERE");
            }
        }

        return where.toString();
    }

    public TableInfo getTableInfo() {
        if(super.getTableInfo() == null){
            Class[] classes = new Class[] {Integer.class, String.class, BigDecimal.class};
            String[] colums = new String[] {"i_id", getTextRespectUISpr("lp.region", theClientDto.getMandant(), theClientDto.getLocUi()),
                getTextRespectUISpr("lp.umsatzziel", theClientDto.getMandant(), theClientDto.getLocUi())};
            int[] columnWidths = new int[] {-1, QueryParameters.FLR_BREITE_M, QueryParameters.FLR_BREITE_M};
            String[] columnNames = new String[] {SystemFac.FLR_REGIONID, SystemFac.FLR_REGIONCNR, SystemFac.FLR_REGION_UMSATZZIEL};
            setTableInfo(new TableInfo(classes, colums, columnWidths, columnNames));
        }
        return super.getTableInfo();
    }
}
