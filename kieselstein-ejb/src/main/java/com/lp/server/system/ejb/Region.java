package com.lp.server.system.ejb;

import com.lp.server.system.service.ITablenames;
import com.lp.server.system.service.RegionDto;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@NamedQueries( { @NamedQuery(name = RegionQuery.ByCnr, query = "SELECT OBJECT (o) FROM Region o WHERE o.cNr = ?1"),
                 @NamedQuery(name = RegionQuery.ByIId, query = "SELECT OBJECT (o) FROM Region o WHERE o.iId = ?1")})
@Entity
@Table(name = ITablenames.LP_REGION)
public class Region implements Serializable {


    @Id
    @Column(name = "I_ID", columnDefinition = "INTEGER NOT NULL")
    private Integer iId;

    @Column(name = "C_NR", columnDefinition = "VARCHAR(50) NOT NULL")
    private String cNr;

    @Column(name = "N_UMSATZZIEL", columnDefinition = "NUMERIC(15,2)")
    private BigDecimal bdUmsatzziel;

    public Region() {super();}
    public Region (RegionDto regionDto){
        setiId(regionDto.getIID());
        setcNr(regionDto.getCNr());
        setBdUmsatzziel(regionDto.getBdUmsatzziel());
    }
    public Integer getiId() {
        return iId;
    }

    public void setiId(Integer iId) {
        this.iId = iId;
    }

    public String getcNr() {
        return cNr;
    }

    public void setcNr(String cNr) {
        this.cNr = cNr;
    }

    public BigDecimal getBdUmsatzziel() {
        return bdUmsatzziel;
    }

    public void setBdUmsatzziel(BigDecimal bdUmsatzziel) {
        this.bdUmsatzziel = bdUmsatzziel;
    }
}
