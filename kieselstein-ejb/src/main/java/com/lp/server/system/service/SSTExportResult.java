package com.lp.server.system.service;

public class SSTExportResult extends AbstractBaseDto {

	private static final long serialVersionUID = 1L;
	private String format;
	private byte[] data;
	private SSTLaufDto sstRun;

	public String getFormat() {
		return format;
	}

	public void setFormat(String cFormat) {
		this.format = cFormat;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public SSTLaufDto getSstRun() {
		return sstRun;
	}

	public void setSstRun(SSTLaufDto sstRun) {
		this.sstRun = sstRun;
	}

}
