package com.lp.server.system.automatikjob;

import com.lp.server.system.service.GenericInterfacesFac;
import com.lp.server.system.service.SSTLaufDto;
import com.lp.server.system.service.SSTParameterDto;
import com.lp.server.system.service.TheClientDto;
import com.lp.util.EJBExceptionLP;

import java.sql.Timestamp;
import java.util.Collection;

import static com.lp.server.system.service.GenericInterfacesFac.SST_AUSLOESER_SHOP_TIMER;
import static com.lp.server.system.service.GenericInterfacesFac.SST_TYP_EXPORT;

public class AutomatikjobSSTExport extends AutomatikjobBasis {


	@Override
	public boolean performJob(TheClientDto client) {
		GenericInterfacesFac fac = getGenericInterfacesFac();
		for (SSTParameterDto sst : fac.getSSTParameterByTypAndAusloeser(SST_TYP_EXPORT, SST_AUSLOESER_SHOP_TIMER)) {
			Collection<Integer> referenceIDs = fac.getSSTExportReferenceIDs(sst, client);
			if (!referenceIDs.isEmpty()) {
				Timestamp startTimestamp = new Timestamp(System.currentTimeMillis());
				try {
					fac.doSSTExport(sst, referenceIDs, client);
				} catch (EJBExceptionLP e) {
					// if an EJBExceptionLP will be thrown than, there is no SSTRun-Instance created so create it manually.
					SSTLaufDto sstRun = fac.createSSTLaufStart(startTimestamp, sst);
					fac.handleSSTLaufException(sstRun, e, null);
				}
			}
		}
		return false;
	}

}
