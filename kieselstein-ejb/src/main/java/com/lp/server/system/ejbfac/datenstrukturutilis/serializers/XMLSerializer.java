package com.lp.server.system.ejbfac.datenstrukturutilis.serializers;

import com.lp.server.system.ejb.DatenStrukturFeld;
import com.lp.server.system.ejbfac.datenstrukturutilis.exports.BaseExportDataUtil;
import com.lp.server.system.service.DatenStrukturDto;
import org.apache.http.Consts;
import org.apache.http.entity.ContentType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.ejb.EJBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.util.*;

public class XMLSerializer extends AbstractBaseSerializer {

    protected final ContentType contentType = ContentType.create("application/xml", Consts.UTF_8);

    public XMLSerializer(Map<Integer, Map<String, String>> strukturFeldValueMappings) {
        super(strukturFeldValueMappings);
    }

    private String trimXMLPath(String path) {
        if (path != null) {
            path = path.trim();
            if (path.startsWith("/")) {
                path = path.substring(1);
            }
            if (path.endsWith("/")) {
                path = path.substring(0, path.length() - 1);
            }
            return path.trim();
        }
        return null;
    }

    private class SingleNodeList implements NodeList {
        private final Node node;
        public SingleNodeList(Node node) {
            this.node = node;
        }

        @Override
        public Node item(int index) {
            return node;
        }

        @Override
        public int getLength() {
            return 1;
        }
    }

    private Collection<Element> getElementsByName(Collection<Element> elements, Node parent, Document doc, String name) {
        NodeList nodeList = null;

        if (parent == null && doc != null) {
            System.out.println("getElementsByName: START: " + name);
            nodeList = new SingleNodeList(doc.getDocumentElement());
        } else if (parent != null) {
            System.out.println("getElementsByName: RECURSION: " + name);
            nodeList = parent.getChildNodes();
        }

        int posSep = name.indexOf("/");
        String targetNodeName = name;
        String restName = null;
        if (posSep > 0) {
            targetNodeName = name.substring(0, posSep).trim();
            restName = name.substring(posSep + 1).trim();
        }

        if (nodeList != null) {
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node currentNode = nodeList.item(i);
                if (currentNode.getNodeType() == Node.ELEMENT_NODE && targetNodeName.equals(currentNode.getNodeName())) {
                    if (restName != null) {
                        getElementsByName(elements, currentNode, doc, restName);
                    } else {
                        System.out.println("getElementsByName.Found Item: " + targetNodeName);
                        elements.add((Element) currentNode);
                    }
                }
            }
        }

        return elements;
    }

    private Collection<Element> findNextElementByName(Collection<Element> elements, Node root, Document doc, String name) {
        if (name != null && name.contains("/")) {
            String[] tokens = name.split("/");
            Node tmpNode = root;
            Node foundParent = null;
            String relativePath = null;
            while (tmpNode != null && tmpNode.getParentNode() != null && foundParent == null) {
                for (int counter = tokens.length - 1; counter >= 0; counter--) {
                    if (tmpNode.getNodeType() == Node.ELEMENT_NODE && tmpNode.getNodeName().equals(tokens[counter])) {
                        // Found a Token which fits, check all other Tokens also from this Node.
                        Node tmpParent = tmpNode.getParentNode();
                        boolean isValid = true;
                        for (int counter2 = counter - 1 ; counter2 >= 0; counter2--) {
                            if (tmpParent == null || tmpParent.getNodeType() != Node.ELEMENT_NODE || !tmpParent.getNodeName().equals(tokens[counter2])) {
                                isValid = false;
                                break;
                            } else {
                                tmpParent = tmpParent.getParentNode();
                            }
                        }
                        if (isValid) {
                            StringBuilder relPath = new StringBuilder();
                            for (int counter2 = counter + 1; counter2 < tokens.length; counter2++) {
                                if (relPath.length() > 0) {
                                    relPath.append("/");
                                }
                                relPath.append(tokens[counter2]);
                            }
                            relativePath = relPath.toString();
                            foundParent = tmpNode;
                        }
                    }
                }
                tmpNode = tmpNode.getParentNode();
            }
            if (foundParent != null && relativePath != null && !relativePath.isEmpty()) {
                return getElementsByName(elements, foundParent, doc, relativePath);
            } else {
                return getElementsByName(elements, null, doc, name);
            }
        } else {
            return getElementsByName(elements, null, doc, name);
        }
    }

    private void addElementValueToMap(Map<String, Object> map, Node parent, Document doc, DatenStrukturFeld sf, String relativePath, boolean findNearestItemFromParent) {
        Collection<Element> elements = new ArrayList<>();
        if (parent != null && findNearestItemFromParent) {
            findNextElementByName(elements, parent, doc, relativePath);
        } else {
            getElementsByName(elements, parent, doc, relativePath);
        }

        Object val;

        if (elements.isEmpty()) {
            if (relativePath != null && relativePath.isEmpty() && parent != null && parent.getNodeType() == Node.ELEMENT_NODE) {
                val = super.handleImportDataMappingValues(getElementValue((Element) parent, sf), sf);
            } else {
                val = super.handleImportDataMappingValues(getElementValue(null, sf), sf);
            }
        } else if (elements.size() == 1) {
            Element element = elements.iterator().next();
            val = super.handleImportDataMappingValues(getElementValue(element, sf), sf);
        } else {
            Object[] values = new Object[elements.size()];
            int i = 0;
            for (Element element : elements) {
                values[i] = super.handleImportDataMappingValues(getElementValue(element, sf), sf);
                i++;
            }
            val = values;
        }
        map.put(sf.getERPFeld().trim(), val);
    }

    private void addElementValueToMap(Map<String, Object> map, Node parent, Document doc, DatenStrukturFeld sf, String relativePath) {
        addElementValueToMap(map, parent, doc, sf, relativePath, false);
    }

    private Object getElementValue(Element e, DatenStrukturFeld sf) {
        if (e == null) {
            return parseValueString(sf.getFixWert(), sf);
        } else {
            if (sf.getAttributName() != null && !sf.getAttributName().isEmpty()) {
                String s = e.getAttribute(sf.getAttributName().trim());
                if (s.isEmpty()) {
                    s = sf.getFixWert();
                }
                return parseValueString(s, sf);
            } else {
                return parseValueString(e.getTextContent(), sf);
            }
        }
    }

    private String getCompleteListPath(List<DatenStrukturFeld> strukturFelder, String strukturListPath) {
        boolean isValidPath = true;
        String tmpListPath = strukturListPath;
        String tmpListPathWithSlash = strukturListPath + "/";
        while (isValidPath) {
            String nextToken = null;
            for (DatenStrukturFeld sf : strukturFelder) {
                String xmlPath = trimXMLPath(sf.getPfad());
                if (xmlPath != null) {
                    if (xmlPath.equals(tmpListPath)) {
                        nextToken = "";
                    } else if (xmlPath.startsWith(tmpListPathWithSlash)) {
                        nextToken = trimXMLPath(xmlPath.substring(tmpListPath.length()));
                        int posNextSep = nextToken.indexOf("/");
                        if (posNextSep > 0) {
                            nextToken = nextToken.substring(0, posNextSep);
                        }
                    } else {
                        isValidPath = false;
                        break;
                    }
                }
            }
            if (isValidPath) {
                strukturListPath = tmpListPath;
                if (nextToken != null && !nextToken.isEmpty()) {
                    tmpListPath = strukturListPath + "/" + nextToken;
                    tmpListPathWithSlash = tmpListPath + "/";
                } else {
                    break;
                }
            }
        }
        return trimXMLPath(strukturListPath);
    }

    private List<Map<String, Object>> parseXMLDocument(Document doc, Map<String, Object> fixValues, String strukturListPath, List<DatenStrukturFeld> strukturFelder) {
        Map<String, Object> fixData = (fixValues != null ? new HashMap<>(fixValues) : new HashMap<>());
        List<Map<String, Object>> dataList = new ArrayList<>();
        strukturListPath = trimXMLPath(strukturListPath);
        List<DatenStrukturFeld> listStrukturFelder = new ArrayList<>();
        List<DatenStrukturFeld> listStrukturFelderNotInListTree = new ArrayList<>();
        for (DatenStrukturFeld sf : strukturFelder) {
            String xmlPath = trimXMLPath(sf.getPfad());
            if (sf.getERPFeld() != null && !sf.getERPFeld().isEmpty() && xmlPath != null) {
                if (xmlPath.startsWith(strukturListPath + "/") || xmlPath.equals(strukturListPath)) {
                    listStrukturFelder.add(sf);
                } else if (strukturListPath != null && !strukturListPath.isEmpty()) {
                    listStrukturFelderNotInListTree.add(sf);
                } else {
                    addElementValueToMap(fixData, null, doc, sf, xmlPath);
                }
            }
        }

        if (listStrukturFelder.isEmpty()) {
            dataList.add(fixData);
        } else {
            strukturListPath = getCompleteListPath(listStrukturFelder, strukturListPath);
            List<Element> elements = new ArrayList<>();
            for (Element parentNode : getElementsByName(elements, null, doc, strukturListPath)) {
                Map<String, Object> data = new HashMap<>(fixData);
                for (DatenStrukturFeld sf : listStrukturFelder) {
                    String relativePath = trimXMLPath(trimXMLPath(sf.getPfad()).substring(strukturListPath.length()));
                    addElementValueToMap(data, parentNode, doc, sf, relativePath);
                }
                for (DatenStrukturFeld sf : listStrukturFelderNotInListTree) {
                    addElementValueToMap(data, parentNode, doc, sf, sf.getPfad(), true);
                }
                dataList.add(data);
            }
        }

        return dataList;
    }

    private Element initXMLElementForPath(Map<String, Element> createdElements, String path, Document doc) {
        path = trimXMLPath(path);
        String[] xmlPfadTokens = path.split("/");
        StringBuilder sbPfad = new StringBuilder();
        Element element = null;
        for (String pfadToken : xmlPfadTokens) {
            sbPfad.append(pfadToken);
            if (!createdElements.containsKey(sbPfad.toString())) {
                Element tmpElement = doc.createElement(pfadToken);
                if (element != null) {
                    element.appendChild(tmpElement);
                }
                element = tmpElement;
                createdElements.put(sbPfad.toString(), tmpElement);
            } else {
                element = createdElements.get(sbPfad.toString());
            }
            sbPfad.append("/");
        }
        return element;
    }

    private Element buildXMLElement(BaseExportDataUtil datenUtil, List<DatenStrukturFeld> strukturFelder, Document doc,
                                    String relativePath, String defaultDateFormatMask, String defaultNumberFormatMask) {
        HashMap<String, Element> createdElements = new HashMap<>();
        Element root = null;
        for (DatenStrukturFeld sf : strukturFelder) {
            String xmlPath = sf.getPfad().trim();
            if (relativePath != null) {
                relativePath = relativePath.trim();
                if (!relativePath.isEmpty() && xmlPath.startsWith(relativePath)) {
                    xmlPath = xmlPath.substring(relativePath.trim().length());
                }
            }
            Element element = initXMLElementForPath(createdElements, xmlPath, doc);
            String value = getStrukturFeldValueAsString(datenUtil, sf, defaultDateFormatMask, defaultNumberFormatMask);

            if (sf.getAttributName() != null && !sf.getAttributName().isEmpty()) {
                element.setAttribute(sf.getAttributName(), value);
            } else {
                element.appendChild(doc.createTextNode(value));
            }
            root = element;
            while (root.getParentNode() != null) {
                root = (Element) root.getParentNode();
            }
        }
        return root;
    }

    private void appendXMLDocumentListElementForStruktur(Element parent, Document doc, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder, BaseExportDataUtil datenUtil) {
        Element element = buildXMLElement(
                datenUtil, strukturFelder, doc, struktur.getListItemPfad(), struktur.getDefaultDatumFormat(), struktur.getDefaultNummerFormat()
        );
        parent.appendChild(element);
    }

    private Document createXMLDocumentForStruktur(DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder, BaseExportDataUtil datenUtil) {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        Document doc;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.newDocument();
        } catch (ParserConfigurationException e) {
            throw new EJBException(e);
        }

        Element element = buildXMLElement(
                datenUtil, strukturFelder, doc, null, struktur.getDefaultDatumFormat(), struktur.getDefaultNummerFormat()
        );
        doc.appendChild(element);
        return doc;
    }

    private Document buildDocument(List<BaseExportDataUtil> data, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder) throws XPathExpressionException {
        List<DatenStrukturFeld> strukturFelderDetails = null;
        Document doc = null;
        Element detailParentElement = null;

        for (BaseExportDataUtil dataUtil : data) {
            if (doc == null) {
                // First Row create the Document.
                doc = createXMLDocumentForStruktur(struktur, strukturFelder, dataUtil);
            } else {
                String strukturListItem = trimXMLPath(struktur.getListItemPfad());
                if (strukturFelderDetails == null) {
                    // Get All Relevant Detail-Stuktur-Fields
                    strukturFelderDetails = new ArrayList<>();
                    if (strukturListItem != null) {
                        for (DatenStrukturFeld sf: strukturFelder) {
                            String xmlItem = trimXMLPath(sf.getPfad());
                            if (xmlItem != null) {
                                if (xmlItem.startsWith(strukturListItem + "/")) {
                                    strukturFelderDetails.add(sf);
                                }
                            }
                        }
                    }
                }

                if (detailParentElement == null) {
                    // Get the Parent Node for the detail Elements.
                    String xPathString = "/" + strukturListItem;
                    XPath xPath = XPathFactory.newInstance().newXPath();
                    NodeList nodes = (NodeList)xPath.evaluate(xPathString, doc, XPathConstants.NODESET);
                    detailParentElement = (nodes.getLength() > 0 ? (Element) nodes.item(0) : null);
                }

                if (!strukturFelderDetails.isEmpty() && detailParentElement != null) {
                    appendXMLDocumentListElementForStruktur(
                            detailParentElement, doc,
                            struktur, strukturFelderDetails, dataUtil
                    );
                } else {
                    throw new RuntimeException("Not Supported Document Update Right now");
                }
            }
        }

        return doc;

    }

    private Document readXMLFile(String filename) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        return db.parse(filename);
    }

    private Document parseXMLString(String xmlString) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xmlString));
        return db.parse(is);
    }

    private void exportDOMSourceToWriter(DOMSource source, StreamResult result) throws TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.ENCODING, encoding);
        transformer.transform(source, result);
    }

    @Override
    public void exportToFile(String fileName, List<BaseExportDataUtil> data, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder) throws IOException {
        try {
            Document doc = buildDocument(data, struktur, strukturFelder);
            DOMSource domSource = new DOMSource(doc);
            FileWriter writer = new FileWriter(fileName);
            StreamResult result = new StreamResult(writer);
            exportDOMSourceToWriter(domSource, result);
        } catch (TransformerException | XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String exportToString(List<BaseExportDataUtil> data, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder) {
        try {
            Document doc = buildDocument(data, struktur, strukturFelder);
            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            exportDOMSourceToWriter(domSource, result);
            return writer.toString();
        } catch (TransformerException | XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    public byte[] exportToBytes(List<BaseExportDataUtil> data, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder) {
        try {
            Document doc = buildDocument(data, struktur, strukturFelder);
            DOMSource domSource = new DOMSource(doc);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            StreamResult result = new StreamResult(bos);
            exportDOMSourceToWriter(domSource, result);
            return bos.toByteArray();
        } catch (TransformerException | XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected ContentType getContentType() {
        return contentType;
    }

    private List<DatenStrukturFeld> initImportFields(List<DatenStrukturFeld> strukturFelder, Map<String, Object> fixValues) {
        List<DatenStrukturFeld> xmlRelevantFelder = new ArrayList<>();
        for (DatenStrukturFeld sf : strukturFelder) {
            String xmlPath = trimXMLPath(sf.getPfad());
            if (sf.getERPFeld() != null && !sf.getERPFeld().isEmpty()) {
                if (xmlPath != null) {
                    xmlRelevantFelder.add(sf);
                } else if (sf.getFixWert() != null && !sf.getFixWert().isEmpty()) {
                    fixValues.put(sf.getERPFeld().trim(), parseValueString(sf.getFixWert(), sf));
                }
            }
        }
        return xmlRelevantFelder;
    }

    public List<Map<String, Object>> buildImportDataFromFile(String fileName, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder) {
        Map<String, Object> fixValues = new HashMap<>(this.globalParameter);
        List<DatenStrukturFeld> xmlRelevantFelder = initImportFields(strukturFelder, fixValues);
        try {
            Document doc = readXMLFile(fileName);
            return parseXMLDocument(doc, fixValues, struktur.getListItemPfad(), xmlRelevantFelder);
        } catch (ParserConfigurationException | IOException | SAXException ignore) {
        }
        return null;
    }

    public List<Map<String, Object>> buildImportDataFromString(String dataString, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder) {
        Map<String, Object> fixValues = new HashMap<>(this.globalParameter);
        List<DatenStrukturFeld> xmlRelevantFelder = initImportFields(strukturFelder, fixValues);
        try {
            Document doc = parseXMLString(dataString);
            return parseXMLDocument(doc, fixValues, struktur.getListItemPfad(), xmlRelevantFelder);
        } catch (ParserConfigurationException | IOException | SAXException ignore) {
        }
        return null;
    }

}
