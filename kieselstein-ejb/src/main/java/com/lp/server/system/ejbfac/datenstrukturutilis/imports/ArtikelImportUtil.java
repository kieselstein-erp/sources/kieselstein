package com.lp.server.system.ejbfac.datenstrukturutilis.imports;

import com.lp.server.artikel.service.ArtikelDto;
import com.lp.server.artikel.service.ArtikelFac;
import com.lp.server.artikel.service.ArtikelTruTopsDto;
import com.lp.server.artikel.service.ArtikelTruTopsMetadatenDto;
import com.lp.server.system.service.GenericInterfacesFac;
import com.lp.server.system.service.TheClientDto;
import com.lp.server.util.ArtikelId;
import com.lp.server.util.HvOptional;
import com.lp.util.EJBExceptionLP;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;

public class ArtikelImportUtil extends AbstractBaseImportDataUtil {
    private final ArtikelFac artikelFac;

    public ArtikelImportUtil(EntityManager em, TheClientDto client, String sstSection, Date timeStamp, String defaultDateTimeFormat, ArtikelFac artikelFac) {
        super(em, client, sstSection, timeStamp, defaultDateTimeFormat);
        this.artikelFac = artikelFac;
    }

    private String buildArtikelKeyString(Map<String, Object> data) {
        final String[] keyFields = {
                GenericInterfacesFac.FELD_ARTIKEL_ID,
                GenericInterfacesFac.FELD_ARTIKEL_CNR,
                GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_NR
        };

        return buildDataKeyString(data, keyFields);
    }

    private ArtikelDto getArtikelDto(Map<String, Object> data) throws RemoteException {
        Integer artID = getIntegerFromDataMap(data, GenericInterfacesFac.FELD_ARTIKEL_ID);
        String artNr = getStringFromDataMap(data, GenericInterfacesFac.FELD_ARTIKEL_CNR);
        String artTruTopsNr = getStringFromDataMap(data, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_NR);

        ArtikelDto artikelDto = null;
        if (artID != null) {
            artikelDto = artikelFac.artikelFindByPrimaryKey(artID, client);
        } else if (artNr != null && !artNr.isEmpty()) {
            artikelDto = artikelFac.artikelFindByCNrOhneExc(artNr, client);
        } else if (artTruTopsNr != null && !artTruTopsNr.isEmpty()) {
            artNr = artTruTopsNr.replace("-", "");
            artikelDto = artikelFac.artikelFindByCNrOhneExc(artNr, client);
        }
        if (artikelDto == null) {
            String errorMessage = "Es konnte kein Artikel ermittelt werten (" + buildArtikelKeyString(data) + ")!";
            throw new EJBExceptionLP(EJBExceptionLP.FEHLER_NO_UNIQUE_RESULT, errorMessage);
        }

        return artikelDto;
    }

    private ArtikelTruTopsDto getOrCreateArtikelTruTopsDto(Map<String, Object> data) throws RemoteException {
        ArtikelDto artikelDto = getArtikelDto(data);

        HvOptional<ArtikelTruTopsDto> opt = artikelFac.artikelTruTopsFindByArtikelId(new ArtikelId(artikelDto.getIId()));
        if (opt.isPresent()) {
            return opt.get();
        } else {
            ArtikelTruTopsDto artTruTops = new ArtikelTruTopsDto();
            artTruTops.setArtikelIId(artikelDto.getIId());
            return artTruTops;
        }
    }

    private ArtikelTruTopsMetadatenDto getOrCreateArtikelTruTopsMetaDatenDto(ArtikelTruTopsDto artTruTops) throws RemoteException {
        List<ArtikelTruTopsMetadatenDto> metadatenDtos = artikelFac.artikelTruTopsMetadatenFindByArtikelId(new ArtikelId(artTruTops.getArtikelIId()));
        if (metadatenDtos.size() == 1) {
            return metadatenDtos.get(0);
        } else if (metadatenDtos.size() > 1) {
            // Return the last entry for that article.
            ArtikelTruTopsMetadatenDto retDto = null;
            for (ArtikelTruTopsMetadatenDto dto : metadatenDtos) {
                if (retDto == null) {
                    retDto = dto;
                } else if (retDto.getIId() < dto.getIId()) {
                    retDto = dto;
                }
            }
            return retDto;
        }

        // No MetaData DTO Found create a new one.
        ArtikelTruTopsMetadatenDto dto = new ArtikelTruTopsMetadatenDto();
        dto.setArtikelTruTopsIId(artTruTops.getIId());
        return dto;
    }

    private void handleUpdateOrCreateArtikelTruTupsMetaDaten(Map<String, Object> data, ArtikelTruTopsDto artTruTops) throws ParseException, RemoteException {
        ArtikelTruTopsMetadatenDto dto = getOrCreateArtikelTruTopsMetaDatenDto(artTruTops);
        boolean dataChanged = (dto.getIId() == null);
        if (data.containsKey(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_PFAD)) {
            String fullPath = getStringFromDataMap(data, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_PFAD);
            dataChanged |= !Objects.equals(fullPath, dto.getFullPath());
            dto.setFullPath(fullPath);
            if (fullPath != null && (fullPath.contains("\\") || fullPath.contains("/"))) {
                // Extract the filename from the full-path.
                String[] parts = (fullPath.contains("/") ? fullPath.split("/") : fullPath.split("\\\\"));
                String fileName = parts[parts.length -1];
                if (fileName != null && !fileName.isEmpty()) {
                    dataChanged |= !Objects.equals(fileName, dto.getCFilename());
                    dto.setCFilename(fileName);
                }
            }
        }

        if (data.containsKey(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_HASH)) {
            String cHash = getStringFromDataMap(data, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_HASH);
            dataChanged |= !Objects.equals(cHash, dto.getCHash());
            dto.setCHash(cHash);
        }
        if (data.containsKey(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_GROESSE)) {
            Integer size = getIntegerFromDataMap(data, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_GROESSE);
            dataChanged |= !Objects.equals(size, dto.getISize());
            dto.setISize(size);
        }
        if (data.containsKey(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_ERSTELLT)) {
            Timestamp creation = getTimeStampFromDataMap(data, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_ERSTELLT, this.defaultDateTimeFormat);
            dataChanged |= !Objects.equals(creation, dto.getTCreation());
            dto.setTCreation(creation);
        }
        if (data.containsKey(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_MODIFIZIERT)) {
            Timestamp changed = getTimeStampFromDataMap(data, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_ERSTELLT, this.defaultDateTimeFormat);
            dataChanged |= !Objects.equals(changed, dto.getTModification());
            dto.setTModification(changed);
        }

        // Create or Update the Metadata for the TruTops Article.
        if (dto.getIId() == null) {
            artikelFac.createArtikelTruTopsMetadaten(dto, client);
        } else if (dataChanged) {
            artikelFac.updateArtikelTruTopsMetadaten(dto);
        }
    }

    private void handleUpdateOrCreateArtikelTruTups(Map<String, Object> data) throws ParseException, RemoteException {
        ArtikelTruTopsDto dto = getOrCreateArtikelTruTopsDto(data);
        boolean dataChanged = (dto.getIId() == null);

        if (data.containsKey(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_EXPORT_ENDE)) {
            Timestamp exportEnde = getTimeStampFromDataMap(data, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_EXPORT_ENDE, this.defaultDateTimeFormat);
            dataChanged |= !Objects.equals(exportEnde, dto.getTExportEnde());
            dto.setTExportEnde(exportEnde);
        }
        if (data.containsKey(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_FEHLER_CODE)) {
            String fehlerCode = getStringFromDataMap(data, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_FEHLER_CODE);
            dataChanged |= !Objects.equals(fehlerCode, dto.getCFehlercode());
            dto.setCFehlercode(fehlerCode);
        }
        if (data.containsKey(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_FEHLER_TEXT)) {
            String fehlerText = getStringFromDataMap(data, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_FEHLER_TEXT);
            dataChanged |= !Objects.equals(fehlerText, dto.getCFehlertext());
            dto.setCFehlertext(fehlerText);
        }
        if (dto.getIId() == null) {
            Integer newID = artikelFac.createArtikelTruTops(dto, client);
            dto.setIId(newID);
        } else if (dataChanged) {
            artikelFac.updateArtikelTruTops(dto);
        }

        if (data.containsKey(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_PFAD) ||
            data.containsKey(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_HASH) ||
            data.containsKey(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_GROESSE) ||
            data.containsKey(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_ERSTELLT) ||
            data.containsKey(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_MODIFIZIERT)
        ) {
            handleUpdateOrCreateArtikelTruTupsMetaDaten(data, dto);
        }
    }

    private void handleUpdateTruTupsLog(Map<String, Object> data) throws RemoteException {
        String artikelNr;
        try {
            ArtikelDto artikelDto = getArtikelDto(data);
            artikelNr = artikelDto.getCNr();
        } catch (EJBExceptionLP ignore) {
            // If there is no Valid-Article, don't care here these would be logged in the updateTrumphtopslog Method.
            artikelNr = getStringFromDataMap(data, GenericInterfacesFac.FELD_ARTIKEL_CNR);
            if (artikelNr == null || artikelNr.isEmpty()) {
                String artTruTopsNr = getStringFromDataMap(data, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_NR);
                if (artTruTopsNr != null) {
                    artikelNr =  artTruTopsNr.replace("-", "");
                }
            }
        }

        String kurzbezeichnungMaterial = getStringFromDataMap(data, GenericInterfacesFac.FELD_TRU_TOPS_LOG_MATERIAL_ARTIKEL_KURZ_BEZEICHNUNG);
        String importfileName = getStringFromDataMap(data, GenericInterfacesFac.GLOBAL_FELD_IMPORT_FILE_NAME);
        BigDecimal gewicht = getBigDecimalFromDataMap(data, GenericInterfacesFac.FELD_TRU_TOPS_LOG_TEIL_GEWICHT);
        Double bearbeitungsDauerMinute = getDoubleFromDataMap(data, GenericInterfacesFac.FELD_TRU_TOPS_LOG_BEARBEITUNG_DAUER_MINUTE);
        BigDecimal laserkostenProStunde = getBigDecimalFromDataMap(data, GenericInterfacesFac.FELD_TRU_TOPS_LOG_LASER_KOSTE_STUNDE);
        Integer lagerIId = getIntegerFromDataMap(data, GenericInterfacesFac.FELD_TRU_TOPS_LOG_LAGER_ID);
        double breiteArtikel = getDoubleFromDataMap(data, GenericInterfacesFac.FELD_TRU_TOPS_LOG_BREITE_ARTIKEL);
        double laengeArtikel = getDoubleFromDataMap(data, GenericInterfacesFac.FELD_TRU_TOPS_LOG_LAENGE_ARTIKEL);
        double hoeheArtikel = getDoubleFromDataMap(data, GenericInterfacesFac.FELD_TRU_TOPS_LOG_HOEHE_ARTIKEL);
        int mehrverbrauchLaserInMM = getIntegerFromDataMap(data, GenericInterfacesFac.FELD_TRU_TOPS_LOG_MEHRVERBRAUCH_LASER_IN_MM);
        boolean kalkulationsart1 = false;
        long bearbeitungsDauer = 0;

        if (bearbeitungsDauerMinute != null) {
            // Bearbeitung-Zeit nach Millisekunden umrechnen.
            bearbeitungsDauer = (long) (bearbeitungsDauerMinute * 60000.0D);
        }

        artikelFac.updateTrumphtopslog(artikelNr, kurzbezeichnungMaterial, importfileName, gewicht,
                bearbeitungsDauer, laserkostenProStunde, lagerIId, client.getMandant(), kalkulationsart1,
                mehrverbrauchLaserInMM, breiteArtikel, laengeArtikel, hoeheArtikel, client);
    }

    public void importData(Collection<Map<String, Object>> data) throws ParseException, RemoteException {
        if (data != null && !data.isEmpty()) {
            for (Map<String, Object> row : data) {
                if (GenericInterfacesFac.SST_BEREICH_WW_ARTIKEL_TRU_TOPS.equals(sstSection)) {
                    handleUpdateOrCreateArtikelTruTups(row);
                } else if (GenericInterfacesFac.SST_BEREICH_WW_ARTIKEL_TRU_TOPS_LOG.equals(sstSection)) {
                    handleUpdateTruTupsLog(row);
                } else {
                    throw new IllegalArgumentException("Nicht unterstützter Schnittstellen Quell-Typ: " + sstSection);
                }
            }
        }
    }
}