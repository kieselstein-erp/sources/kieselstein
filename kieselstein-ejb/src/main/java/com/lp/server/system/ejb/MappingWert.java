package com.lp.server.system.ejb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "LP_MAPPING_WERT")
public class MappingWert implements Serializable {
	@Id
	@Column(name = "I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer iId;

	@Column(name = "mapping_i_id", columnDefinition = "INTEGER NOT NULL")
	private Integer mappingIId;

	@Column(name = "C_QUELLWERT", columnDefinition = "VARCHAR(64) NOT NULL")
	private String cQuellwert;

	@Column(name = "C_ZIELWERT", columnDefinition = "VARCHAR(128) NOT NULL")
	private String cZielwert;

	@Column(name = "PERSONAL_I_ID_ANLEGEN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAnlegen;

	@Column(name = "PERSONAL_I_ID_AENDERN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAendern;

	@Column(name = "T_ANLEGEN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAnlegen ;

	@Column(name = "T_AENDERN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAendern;


	public MappingWert() {
		super();
	}

	public MappingWert(Integer id, Integer mappingIId, String cQuellwert, String cZielwert,
                       Integer personalIIdAnlegen) {
		setId(id);
		setMappingId(mappingIId);
		setQuellwert(cQuellwert);
		setZielwert(cZielwert);
		setPersonalIIdAnlegen(personalIIdAnlegen);
		setPersonalIIdAendern(personalIIdAnlegen);
		// Setzen der NOT NULL Felder
		Timestamp now = new Timestamp(System.currentTimeMillis());
		setAendern(now);
		setAnlegen(now);
	}

	public Integer getId() {
		return this.iId;
	}

	public void setId(Integer iId) {
		this.iId = iId;
	}

	public Integer getMappingId() {
		return this.mappingIId;
	}

	public void setMappingId(Integer mappingIId) {
		this.mappingIId = mappingIId;
	}

	public String getQuellwert() {
		return cQuellwert;
	}

	public void setQuellwert(String cQuellwert) {
		this.cQuellwert = cQuellwert;
	}

	public String getZielwert() {
		return cZielwert;
	}

	public void setZielwert(String cZielwert) {
		this.cZielwert = cZielwert;
	}

	public Integer getPersonalIIdAnlegen() {
		return personalIIdAnlegen;
	}

	public void setPersonalIIdAnlegen(Integer personalIIdAnlegen) {
		this.personalIIdAnlegen = personalIIdAnlegen;
	}

	public Integer getPersonalIIdAendern() {
		return personalIIdAendern;
	}

	public void setPersonalIIdAendern(Integer personalIIdAendern) {
		this.personalIIdAendern = personalIIdAendern;
	}

	public Timestamp getAnlegen() {
		return tAnlegen;
	}

	public void setAnlegen(Timestamp tAnlegen) {
		this.tAnlegen = tAnlegen;
	}

	public Timestamp getAendern() {
		return tAendern;
	}

	public void setAendern(Timestamp tAendern) {
		this.tAendern = tAendern;
	}
}
