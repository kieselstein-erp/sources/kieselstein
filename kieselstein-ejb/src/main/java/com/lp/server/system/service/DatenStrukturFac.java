package com.lp.server.system.service;

import com.lp.util.EJBExceptionLP;

import javax.ejb.Remote;
import java.util.*;

@Remote
public interface DatenStrukturFac {

	DatenStrukturDto createStruktur(DatenStrukturDto strukturDto, TheClientDto client) throws EJBExceptionLP;

	void deleteStruktur(Integer id, TheClientDto client) throws EJBExceptionLP;

	void updateStruktur(Integer id, String bezeichnung, String bereich, String defaultDatumFormat, String defaultNummerFormat, TheClientDto client) throws EJBExceptionLP;

	DatenStrukturFeldDto createStrukturFeld(DatenStrukturFeldDto datenStrukturFeldDto, TheClientDto client) throws EJBExceptionLP;

	Integer createMapping(String bezeichnung, Map<String, String> mapValues, TheClientDto client) throws EJBExceptionLP;
}