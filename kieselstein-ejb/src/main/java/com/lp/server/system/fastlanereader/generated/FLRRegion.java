package com.lp.server.system.fastlanereader.generated;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;
import java.math.BigDecimal;

public class FLRRegion implements Serializable {
    private Integer i_id;
    private String c_nr;

    private BigDecimal n_umsatzziel;

    public FLRRegion(Integer i_id, String c_nr, BigDecimal n_umsatzziel){
        this.i_id = i_id;
        this.c_nr = c_nr;
        this.n_umsatzziel = n_umsatzziel;
    }

    public FLRRegion(){  }

    public Integer getI_id() {
        return i_id;
    }

    public void setI_id(Integer i_id) {
        this.i_id = i_id;
    }

    public String getC_nr() {
        return c_nr;
    }

    public void setC_nr(String c_nr) {
        this.c_nr = c_nr;
    }

    public BigDecimal getN_umsatzziel() {
        return n_umsatzziel;
    }

    public void setN_umsatzziel(BigDecimal n_umsatzziel) {
        this.n_umsatzziel = n_umsatzziel;
    }

    public String toString() {
        return new ToStringBuilder(this)
                .append("i_id", getI_id())
                .toString();
    }
}
