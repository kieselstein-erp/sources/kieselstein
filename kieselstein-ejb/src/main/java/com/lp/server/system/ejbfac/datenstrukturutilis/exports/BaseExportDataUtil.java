package com.lp.server.system.ejbfac.datenstrukturutilis.exports;

import com.lp.server.system.service.GenericInterfacesFac;
import com.lp.server.system.service.TheClientDto;

import javax.persistence.EntityManager;
import java.text.SimpleDateFormat;
import java.util.*;

public abstract class BaseExportDataUtil {

    protected final EntityManager em;
    protected final TheClientDto client;
    protected final Date timeStamp;
    protected String dataSection;
    protected final String localeString;
    protected Map<String, Map<Integer, Object>> prefetchedData;

    public BaseExportDataUtil(EntityManager em, TheClientDto client, Date timeStamp) {
        this.em = em;
        this.client = client;
        this.localeString = getLocaleFromClient(client);
        if (timeStamp != null) {
            this.timeStamp = timeStamp;
        } else {
            this.timeStamp = new Date();
        }
    }

    protected static String getLocaleFromClient(TheClientDto client) {
        if (client != null && client.getLocUiAsString() != null && !client.getLocUiAsString().trim().isEmpty()) {
            return client.getLocUiAsString().trim();
        } else {
            return  "deAT";
        }
    }

    protected boolean isValueCached(String fieldName) {
        String key = dataSection + ":" + fieldName;
        return (prefetchedData != null && prefetchedData.containsKey(key));
    }

    public Object getPrefetchedValue(String fieldName) {
        if (isValueCached(fieldName)) {
            Integer key = getInstanceKey();
            if (key != null) {
                String fieldKey = dataSection + ":" + fieldName;
                return prefetchedData.get(fieldKey).get(key);
            }
        }
        return null;
    }

    protected void setPrefetchedDataValue(Map<String, Map<Integer, Object>> prefetchedData, String fieldName, Integer instanceKey, Object value) {
        String key = dataSection + ":" + fieldName;
        if (instanceKey != null) {
            if (prefetchedData.containsKey(key)) {
                prefetchedData.get(key).put(instanceKey, value);
            } else {
                Map<Integer, Object> map = new HashMap<>();
                map.put(instanceKey, value);
                prefetchedData.put(key, map);
            }
        }
    }

    protected void setPrefetchedDataMap(Map<String, Map<Integer, Object>> prefetchedData) {
        this.prefetchedData = prefetchedData;
    }

    public Object getGlobalOutputValues(String fieldName) {
        if (GenericInterfacesFac.GLOBAL_FELD_ZEITSTEMPEL_STR1.equals(fieldName)) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            return sdf.format(timeStamp);
        } else {
            throw new IllegalArgumentException("Unsupported Field-Name: " + fieldName);
        }
    }

    public static void initPrefetchData(List<BaseExportDataUtil> utils, Set<String> strukturFields, Collection<Integer> referenceKeys) {
        Map<String, Map<Integer, Object>> prefetchedData = new HashMap<>();
        if (utils != null && utils.size() > 1) {
            BaseExportDataUtil util = utils.get(0);
            util.prefetchInstanceValues(prefetchedData, referenceKeys, strukturFields);
            for (BaseExportDataUtil u : utils) {
                u.setPrefetchedDataMap(prefetchedData);
            }
        }
    }

    public void prefetchInstanceValues(Map<String, Map<Integer, Object>> prefetchedData, Collection<Integer> referenceKeys, Set<String> strukturFields) {
        // Can be Overwritten if Detail-Data should be prefetched.
    }

    public abstract Integer getInstanceKey();

    public abstract Object getElementOutputValue(String erpFeld);
}
