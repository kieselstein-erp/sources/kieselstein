package com.lp.server.system.service;

public class DatenStrukturDto extends AbstractBaseDto {

	private static final long serialVersionUID = 1L;
	private String cBezeichnung;
	private String cBereich;
	private String cListItemPfad;
	private String cDefaultDatumFormat;
	private String cDefaultNummerFormat;

	public String getBezeichnung() {
		return cBezeichnung;
	}

	public void setBezeichnung(String cBezeichnung) {
		this.cBezeichnung = cBezeichnung;
	}

	public String getBereich() {
		return cBereich;
	}

	public void setBereich(String cBereich) {
		this.cBereich = cBereich;
	}

	public String getListItemPfad() {
		return cListItemPfad;
	}

	public void setListItemPfad(String cListItemPfad) {
		this.cListItemPfad = cListItemPfad;
	}

	public String getDefaultDatumFormat() {
		return cDefaultDatumFormat;
	}

	public void setDefaultDatumFormat(String cDefaultDatumFormat) {
		this.cDefaultDatumFormat = cDefaultDatumFormat;
	}

	public String getDefaultNummerFormat() {
		return cDefaultNummerFormat;
	}

	public void setDefaultNummerFormat(String cDefaultNummerFormat) {
		this.cDefaultNummerFormat = cDefaultNummerFormat;
	}
}
