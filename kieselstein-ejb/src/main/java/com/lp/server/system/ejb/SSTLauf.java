package com.lp.server.system.ejb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "LP_SST_LAUF")
public class SSTLauf implements Serializable {
	@Id
	@Column(name = "I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer iId;

	@Column(name = "SST_I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer sstId;

	@Column(name = "T_ZEITSTEMPEL_START", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tZeitstempelStart ;

	@Column(name = "T_ZEITSTEMPEL_ENDE", columnDefinition = "TIMESTAMP")
	private Timestamp tZeitstempelEnde ;

	@Column(name = "I_STATUS", columnDefinition = "SMALLINT NOT NULL")
	private Short iStatus;

	@Column(name = "I_ANZAHL_DATENSAETZE", columnDefinition = "INTEGER")
	private Integer iAnzahlDatensaetze;

	@Column(name = "C_FEHLERMELDUNG", columnDefinition = "TEXT")
	private String cFehlermeldung;


	public SSTLauf() {
		super();
	}

	public SSTLauf(Integer id, Integer sstId, Timestamp zeitstempelStart,
				   Timestamp zeitstempelEnde, Short status, Integer anzahlDatensaetze, String fehlermeldung) {
		setId(id);
		setSstId(sstId);
		setZeitstempelStart(zeitstempelStart);
		setZeitstempelEnde(zeitstempelEnde);
		setStatus(status);
		setAnzahlDatensaetze(anzahlDatensaetze);
		setFehlermeldung(fehlermeldung);
	}

	public Integer getId() {
		return this.iId;
	}

	public void setId(Integer iId) {
		this.iId = iId;
	}

	public Integer getSstId() {
		return sstId;
	}

	public void setSstId(Integer sstId) {
		this.sstId = sstId;
	}

	public Timestamp getZeitstempelStart() {
		return tZeitstempelStart;
	}

	public void setZeitstempelStart(Timestamp tZeitstempelStart) {
		this.tZeitstempelStart = tZeitstempelStart;
	}

	public Timestamp getZeitstempelEnde() {
		return tZeitstempelEnde;
	}

	public void setZeitstempelEnde(Timestamp tZeitstempelEnde) {
		this.tZeitstempelEnde = tZeitstempelEnde;
	}

	public Short getStatus() {
		return iStatus;
	}

	public void setStatus(Short iStatus) {
		this.iStatus = iStatus;
	}

	public Integer getAnzahlDatensaetze() {
		return iAnzahlDatensaetze;
	}

	public void setAnzahlDatensaetze(Integer iAnzahlDatensaetze) {
		this.iAnzahlDatensaetze = iAnzahlDatensaetze;
	}

	public String getFehlermeldung() {
		return cFehlermeldung;
	}

	public void setFehlermeldung(String cFehlermeldung) {
		this.cFehlermeldung = cFehlermeldung;
	}
}
