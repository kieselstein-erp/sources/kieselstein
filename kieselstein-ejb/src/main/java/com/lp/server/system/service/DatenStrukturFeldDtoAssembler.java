package com.lp.server.system.service;

import com.lp.server.system.ejb.DatenStrukturFeld;

public class DatenStrukturFeldDtoAssembler {
	public static DatenStrukturFeldDto createDto(DatenStrukturFeld bean) {
		DatenStrukturFeldDto dto = new DatenStrukturFeldDto();
		if (bean != null) {
			dto.setIId(bean.getId());
			dto.setStrukturDatenId(bean.getStrukturDatenId());
			dto.setERPFeld(bean.getERPFeld());
			dto.setFixWert(bean.getFixWert());
			dto.setPfad(bean.getPfad());
			dto.setAttributName(bean.getAttributName());
			dto.setSort(bean.getSort());
			dto.setFormatMaske(bean.getFormatMaske());
			dto.setMappingId(bean.getMappingId());
		}
		return dto;
	}
}
