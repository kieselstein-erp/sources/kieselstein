package com.lp.server.system.ejb;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@NamedQueries(
	{
		@NamedQuery(
			name = "SSTParameterWithStrukturFindByAusloeserUndTyp",
			query = "SELECT sst, struk FROM SSTParameter sst LEFT JOIN sst.datenStruktur struk WHERE sst.bAktiv = true AND sst.iAusloeser = :ausloeser AND sst.cTyp = :typ ORDER BY sst.iSort, sst.iId"
		),
		@NamedQuery(
			name = "SSTParameterWithStrukturFindByID",
			query = "SELECT sst, struk FROM SSTParameter sst LEFT JOIN sst.datenStruktur struk WHERE sst.iId = :iId"
		),
		@NamedQuery(
			name = "SSTParameterExistsStrukturERPFeld",
			query = "SELECT count(distinct sst.iId) FROM SSTParameter sst " +
					"LEFT JOIN sst.datenStruktur struk " +
					"LEFT JOIN struk.felder sfeld " +
					"WHERE sst.iId = :iId " +
					"  AND sfeld.cERPFeld = :cERPFeld"
		)
	}
)
@Entity
@Table(name = "LP_SST_PARAMETER")
public class SSTParameter implements Serializable {
	@Id
	@Column(name = "I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer iId;

	@Column(name = "C_TRANSFER_TYP", columnDefinition = "VARCHAR(12) NOT NULL")
	private String cTransferTyp;

	@Column(name = "STRUKTUR_DATEN_I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer strukturDatenId;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "STRUKTUR_DATEN_I_ID", referencedColumnName = "I_ID", insertable = false, updatable = false)
	private DatenStruktur datenStruktur;

	@Column(name = "I_AUSLOESER", columnDefinition = "SMALLINT NOT NULL")
	private Short iAusloeser;

	@Column(name = "C_QUELLNAME", columnDefinition = "varchar(256)")
	private String cQuellname;

	@Column(name = "C_TYP", columnDefinition = "VARCHAR(1) NOT NULL")
	private String cTyp;  /* I=Import, E=Export*/

	@Column(name = "I_DATEN_MODUS", columnDefinition = "SMALLINT NOT NULL")
	private Short iDatenModus;  /* 1=alle, 2=geänderte*/

	@Column(name = "C_FORMAT", columnDefinition = "VARCHAR(5) NOT NULL")
	private String cFormat;

	@Column(name = "C_BEREICH", columnDefinition = "VARCHAR(32) NOT NULL")
	private String cBereich;

	@Column(name = "I_SORT", columnDefinition = "INTEGER NOT NULL")
	private Integer iSort;

	@Column(name = "C_BEZEICHNUNG", columnDefinition = "VARCHAR(64) NOT NULL")
	private String cBezeichnung;

	@Column(name = "I_NACHVERARBEITUNG_MODUS", columnDefinition = "SMALLINT NOT NULL")
	private Short iNachverarbeitungModus;

	@Column(name = "B_AKTIV", columnDefinition = "BOOLEAN NOT NULL")
	private boolean bAktiv;

	@Column(name = "PERSONAL_I_ID_ANLEGEN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAnlegen;

	@Column(name = "PERSONAL_I_ID_AENDERN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAendern;

	@Column(name = "T_ANLEGEN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAnlegen ;

	@Column(name = "T_AENDERN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAendern;


	public SSTParameter() {
		super();
	}

	public SSTParameter(Integer id, String transfertyp, Integer strukturDatenId, Short ausloeser,
						String quellname, String typ, Short datenModus, String format, String bereich, Integer sort,
						String bezeichnung, Short nachverarbeitungModus, boolean aktiv,
                        Integer personalIIdAnlegen) {
		setId(id);
		setTransferTyp(transfertyp);
		setStrukturDatenId(strukturDatenId);
		setAusloser(ausloeser);
		setQuellname(quellname);
		setTyp(typ);
		setDatenModus(datenModus);
		setFormat(format);
		setBereich(bereich);
		setSort(sort);
		setBezeichnung(bezeichnung);
		setNachverarbeitungModus(nachverarbeitungModus);
		setAktiv(aktiv);
		setPersonalIIdAnlegen(personalIIdAnlegen);
		setPersonalIIdAendern(personalIIdAnlegen);
		// Setzen der NOT NULL Felder
		Timestamp now = new Timestamp(System.currentTimeMillis());
		setAendern(now);
		setAnlegen(now);
	}

	public Integer getId() {
		return this.iId;
	}

	public void setId(Integer iId) {
		this.iId = iId;
	}

	public String getTransferTyp() {
		return cTransferTyp;
	}

	public void setTransferTyp(String cTransferTyp) {
		this.cTransferTyp = cTransferTyp;
	}

	public Integer getStrukturDatenId() {
		return strukturDatenId;
	}

	public void setStrukturDatenId(Integer strukturDatenId) {
		this.strukturDatenId = strukturDatenId;
	}

	public Short getAusloeser() {
		return iAusloeser;
	}

	public void setAusloser(Short iAusloeser) {
		this.iAusloeser = iAusloeser;
	}

	public String getQuellname() {
		return cQuellname;
	}

	public void setQuellname(String cQuellname) {
		this.cQuellname = cQuellname;
	}

	public String getTyp() {
		return cTyp;
	}

	public void setTyp(String cTyp) {
		this.cTyp = cTyp;
	}

	public Short getDatenModus() {
		return iDatenModus;
	}

	public void setDatenModus(Short iDatenModus) {
		this.iDatenModus = iDatenModus;
	}

	public String getFormat() {
		return cFormat;
	}

	public void setFormat(String cFormat) {
		this.cFormat = cFormat;
	}

	public String getBereich() {
		return cBereich;
	}

	public void setBereich(String cBereich) {
		this.cBereich = cBereich;
	}

	public Integer getSort() {
		return iSort;
	}

	public void setSort(Integer iSort) {
		this.iSort = iSort;
	}

	public String getBezeichnung() {
		return cBezeichnung;
	}

	public void setBezeichnung(String cBezeichnung) {
		this.cBezeichnung = cBezeichnung;
	}

	public Short getNachverarbeitungModus() {
		return iNachverarbeitungModus;
	}

	public void setNachverarbeitungModus(Short iNachverarbeitungModus) {
		this.iNachverarbeitungModus = iNachverarbeitungModus;
	}

	public boolean getAktiv() {
		return bAktiv;
	}

	public void setAktiv(boolean bAktiv) {
		this.bAktiv = bAktiv;
	}

	public Integer getPersonalIIdAnlegen() {
		return personalIIdAnlegen;
	}

	public void setPersonalIIdAnlegen(Integer personalIIdAnlegen) {
		this.personalIIdAnlegen = personalIIdAnlegen;
	}

	public Integer getPersonalIIdAendern() {
		return personalIIdAendern;
	}

	public void setPersonalIIdAendern(Integer personalIIdAendern) {
		this.personalIIdAendern = personalIIdAendern;
	}

	public Timestamp getAnlegen() {
		return tAnlegen;
	}

	public void setAnlegen(Timestamp tAnlegen) {
		this.tAnlegen = tAnlegen;
	}

	public Timestamp getAendern() {
		return tAendern;
	}

	public void setAendern(Timestamp tAendern) {
		this.tAendern = tAendern;
	}
}
