package com.lp.server.system.ejb;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "LP_DATEN_STRUKTUR")
public class DatenStruktur implements Serializable {
	@Id
	@Column(name = "I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer iId;

	@OneToMany(mappedBy = "datenStruktur", fetch = FetchType.LAZY)
	private List<DatenStrukturFeld> felder = new ArrayList<DatenStrukturFeld>();

	@Column(name = "C_BEZEICHNUNG", columnDefinition = "VARCHAR(64) NOT NULL")
	private String cBezeichnung;

	@Column(name = "C_BEREICH", columnDefinition = "VARCHAR(32) NOT NULL")
	private String cBereich;

	@Column(name = "C_LIST_ITEM_PFAD", columnDefinition = "VARCHAR(128)")
	private String cListItemPfad;

	@Column(name = "C_DEFAULT_DATUM_FORMAT", columnDefinition = "VARCHAR(32)")
	private String cDefaultDatumFormat;

	@Column(name = "C_DEFAULT_NUMMER_FORMAT", columnDefinition = "VARCHAR(16)")
	private String cDefaultNummerFormat;

	@Column(name = "PERSONAL_I_ID_ANLEGEN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAnlegen;

	@Column(name = "PERSONAL_I_ID_AENDERN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAendern;

	@Column(name = "T_ANLEGEN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAnlegen ;

	@Column(name = "T_AENDERN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAendern;


	public DatenStruktur() {
		super();
	}

	public DatenStruktur(Integer id, String bezeichnung,
                         String bereich, String listItemPfad, String defaultDatumFormat, String defaultNummerFormat,
						 Integer personalIIdAnlegen) {
		setId(id);
		setBezeichnung(bezeichnung);
		setBereich(bereich);
		setListItemPfad(listItemPfad);
		setDefaultDatumFormat(defaultDatumFormat);
		setDefaultNummerFormat(defaultNummerFormat);
		setPersonalIIdAnlegen(personalIIdAnlegen);
		setPersonalIIdAendern(personalIIdAnlegen);
		// Setzen der NOT NULL Felder
		Timestamp now = new Timestamp(System.currentTimeMillis());
		setAendern(now);
		setAnlegen(now);
	}

	public Integer getId() {
		return this.iId;
	}

	public void setId(Integer iId) {
		this.iId = iId;
	}

	public String getBezeichnung() {
		return cBezeichnung;
	}

	public void setBezeichnung(String cBezeichnung) {
		this.cBezeichnung = cBezeichnung;
	}

	public String getBereich() {
		return cBereich;
	}

	public void setBereich(String cBereich) {
		this.cBereich = cBereich;
	}

	public String getListItemPfad() {
		return cListItemPfad;
	}

	public void setListItemPfad(String cListItemPfad) {
		this.cListItemPfad = cListItemPfad;
	}

	public String getDefaultDatumFormat() {
		return cDefaultDatumFormat;
	}

	public void setDefaultDatumFormat(String cDefaultDatumFormat) {
		this.cDefaultDatumFormat = cDefaultDatumFormat;
	}

	public String getDefaultNummerFormat() {
		return cDefaultNummerFormat;
	}

	public void setDefaultNummerFormat(String cDefaultNummerFormat) {
		this.cDefaultNummerFormat = cDefaultNummerFormat;
	}

	public Integer getPersonalIIdAnlegen() {
		return personalIIdAnlegen;
	}

	public void setPersonalIIdAnlegen(Integer personalIIdAnlegen) {
		this.personalIIdAnlegen = personalIIdAnlegen;
	}

	public Integer getPersonalIIdAendern() {
		return personalIIdAendern;
	}

	public void setPersonalIIdAendern(Integer personalIIdAendern) {
		this.personalIIdAendern = personalIIdAendern;
	}

	public Timestamp getAnlegen() {
		return tAnlegen;
	}

	public void setAnlegen(Timestamp tAnlegen) {
		this.tAnlegen = tAnlegen;
	}

	public Timestamp getAendern() {
		return tAendern;
	}

	public void setAendern(Timestamp tAendern) {
		this.tAendern = tAendern;
	}
}
