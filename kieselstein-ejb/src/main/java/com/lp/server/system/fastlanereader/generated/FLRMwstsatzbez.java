/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 * 
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 * 
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: developers@heliumv.com
 ******************************************************************************/
package com.lp.server.system.fastlanereader.generated;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang.builder.ToStringBuilder;

/** @author Hibernate CodeGenerator */
public class FLRMwstsatzbez implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	/** identifier field */
	private Integer i_id;

	/** persistent field */
	private String mandant_c_nr;

	/** persistent field */
	private String c_bezeichnung;

	private Integer finanzamt_i_id;

	private Set<FLRMwstsatzbez> flrAlternativeMwstsatzbezSet;

	/** full constructor */
	public FLRMwstsatzbez(String mandant_c_nr, String c_bezeichnung, int finanzamtIId,  Set<FLRMwstsatzbez> flrAlternativeMwstsatzbezSet) {
		this.mandant_c_nr = mandant_c_nr;
		this.c_bezeichnung = c_bezeichnung;
		this.flrAlternativeMwstsatzbezSet = flrAlternativeMwstsatzbezSet;
		this.finanzamt_i_id = finanzamtIId;
	}

	/** default constructor */
	public FLRMwstsatzbez() {
	}


	public Integer getFinanzamt_i_id() {
		return finanzamt_i_id;
	}

	public void setFinanzamt_i_id(Integer finanzamt_i_id) {
		this.finanzamt_i_id = finanzamt_i_id;
	}

	public Set<FLRMwstsatzbez> getFlrAlternativeMwstsatzbezSet() {
		return flrAlternativeMwstsatzbezSet;
	}

	public void setFlrAlternativeMwstsatzbezSet(Set<FLRMwstsatzbez> flrAlternativeMwstsatzbezSet) {
		this.flrAlternativeMwstsatzbezSet = flrAlternativeMwstsatzbezSet;
	}

	public Integer getI_id() {
		return this.i_id;
	}

	public void setI_id(Integer i_id) {
		this.i_id = i_id;
	}

	public String getMandant_c_nr() {
		return this.mandant_c_nr;
	}

	public void setMandant_c_nr(String mandant_c_nr) {
		this.mandant_c_nr = mandant_c_nr;
	}

	public String getC_bezeichnung() {
		return this.c_bezeichnung;
	}

	public void setC_bezeichnung(String c_bezeichnung) {
		this.c_bezeichnung = c_bezeichnung;
	}

	public String toString() {
		return new ToStringBuilder(this).append("i_id", getI_id()).toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		FLRMwstsatzbez that = (FLRMwstsatzbez) o;
		return Objects.equals(i_id, that.i_id) && Objects.equals(mandant_c_nr, that.mandant_c_nr) && Objects.equals(c_bezeichnung, that.c_bezeichnung) && Objects.equals(flrAlternativeMwstsatzbezSet, that.flrAlternativeMwstsatzbezSet);
	}

	@Override
	public int hashCode() {
		return Objects.hash(i_id, mandant_c_nr, c_bezeichnung, flrAlternativeMwstsatzbezSet);
	}

}
