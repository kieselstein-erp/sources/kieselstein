package com.lp.server.system.service;

public class DatenStrukturFeldDto extends AbstractBaseDto {

	private static final long serialVersionUID = 1L;
	private Integer strukturDatenId;
	private String cERPFeld;
	private String cFixWert;
	private String cPfad;
	private String cAttributName;
	private Integer iSort;
	private String cFormatMaske;
	private Integer mappingIId;

	public Integer getStrukturDatenId() {
		return strukturDatenId;
	}

	public void setStrukturDatenId(Integer strukturDatenId) {
		this.strukturDatenId = strukturDatenId;
	}

	public String getERPFeld() {
		return cERPFeld;
	}

	public void setERPFeld(String cERPFeld) {
		this.cERPFeld = cERPFeld;
	}

	public String getFixWert() {
		return cFixWert;
	}

	public void setFixWert(String cFixWert) {
		this.cFixWert = cFixWert;
	}

	public String getPfad() {
		return cPfad;
	}

	public void setPfad(String cPfad) {
		this.cPfad = cPfad;
	}

	public String getAttributName() {
		return cAttributName;
	}

	public void setAttributName(String cAttributName) {
		this.cAttributName = cAttributName;
	}

	public Integer getSort() {
		return iSort;
	}

	public void setSort(Integer iSort) {
		this.iSort = iSort;
	}

	public String getFormatMaske() {
		return cFormatMaske;
	}

	public void setFormatMaske(String cFormatMaske) {
		this.cFormatMaske = cFormatMaske;
	}

	public Integer getMappingId() {
		return mappingIId;
	}

	public void setMappingId(Integer mappingIId) {
		this.mappingIId = mappingIId;
	}
}
