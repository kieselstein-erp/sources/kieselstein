package com.lp.server.system.ejbfac.datenstrukturutilis.imports;

import com.lp.server.system.service.TheClientDto;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

public abstract class AbstractBaseImportDataUtil {

    final protected EntityManager em;
    final protected TheClientDto client;
    final protected Date timeStamp;
    final protected String sstSection;
    final protected String localeString;
    final protected String defaultDateTimeFormat;

    public AbstractBaseImportDataUtil(EntityManager em, TheClientDto client, String sstSection, Date timeStamp, String defaultDateTimeFormat) {
        this.em = em;
        this.client = client;
        this.sstSection = sstSection;
        this.localeString = getLocaleFromClient(client);
        if (timeStamp != null) {
            this.timeStamp = timeStamp;
        } else {
            this.timeStamp = new Date();
        }
        this.defaultDateTimeFormat = defaultDateTimeFormat;
    }

    protected static String getLocaleFromClient(TheClientDto client) {
        if (client != null && client.getLocUiAsString() != null && !client.getLocUiAsString().trim().isEmpty()) {
            return client.getLocUiAsString().trim();
        } else {
            return  "deAT";
        }
    }

    protected String buildDataKeyString(Map<String, Object> data, String[] keyFields) {
        StringBuilder sb = new StringBuilder();
        for (String keyField : keyFields) {
            if (data.containsKey(keyField) && data.get(keyField) != null) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(keyField).append(": ").append(data.get(keyField));
            }
        }

        if (sb.length() == 0) {
            sb.append("Es sind keine Schlüsselfelder für den Import enthalten oder in der Struktur definiert (mögliche Schlüsselfelder: ").append(Arrays.toString(keyFields)).append(")!");
        }

        return sb.toString();
    }

    protected String getStringFromDataMap(Map<String, Object> data, String key) {
        if (data.containsKey(key)) {
            Object value = data.get(key);
            if (value != null) {
                String s = value.toString().trim();
                if (!s.isEmpty()) {
                    return s;
                }
            }
        }
        return null;
    }

    protected Timestamp getTimeStampFromDataMap(Map<String, Object> data, String key, String formatMask) throws ParseException {
        if (data.containsKey(key)) {
            Object value = data.get(key);
            if (value != null) {
                if (value instanceof Timestamp) {
                    return (Timestamp) value;
                } else if (value instanceof Date) {
                    return new Timestamp(((Date) value).getTime());
                } else {
                    String s = value.toString().trim();
                    if (!s.isEmpty()) {
                        if (formatMask != null && !formatMask.trim().isEmpty()) {
                            SimpleDateFormat format = new SimpleDateFormat(formatMask.trim());
                            return new Timestamp(format.parse(s).getTime());
                        } else {
                            return Timestamp.valueOf(s); // TODO Maybe handle more formats.
                        }
                    }
                }
            }
        }
        return null;
    }

    protected Short getShortFromDataMap(Map<String, Object> data, String key) {
        if (data.containsKey(key)) {
            Object value = data.get(key);
            if (value != null) {
                if (value instanceof Short) {
                    return (Short) value;
                } else if (value instanceof Number) {
                    return ((Number) value).shortValue();
                } else {
                    String s = value.toString().trim();
                    if (!s.isEmpty()) {
                        return Short.parseShort(s);
                    }
                }
            }
        }
        return null;
    }

    protected Integer getIntegerFromDataMap(Map<String, Object> data, String key) {
        if (data.containsKey(key)) {
            Object value = data.get(key);
            if (value != null) {
                if (value instanceof Integer) {
                    return (Integer) value;
                } else if (value instanceof Number) {
                    return ((Number) value).intValue();
                } else {
                    String s = value.toString().trim();
                    if (!s.isEmpty()) {
                        return Integer.parseInt(s);
                    }
                }
            }
        }
        return null;
    }

    protected Double getDoubleFromDataMap(Map<String, Object> data, String key) {
        if (data.containsKey(key)) {
            Object value = data.get(key);
            if (value != null) {
                if (value instanceof Double) {
                    return (Double) value;
                } else if (value instanceof Number) {
                    return ((Number) value).doubleValue();
                } else {
                    String s = value.toString().trim();
                    if (!s.isEmpty()) {
                        return Double.parseDouble(s);
                    }
                }
            }
        }
        return null;
    }

    protected BigDecimal getBigDecimalFromDataMap(Map<String, Object> data, String key) {
        if (data.containsKey(key)) {
            Object value = data.get(key);
            if (value != null) {
                if (value instanceof BigDecimal) {
                    return (BigDecimal) value;
                } else if (value instanceof Number) {
                    return BigDecimal.valueOf(((Number) value).doubleValue());
                } else {
                    String s = value.toString().trim();
                    if (!s.isEmpty()) {
                        return new BigDecimal(s);
                    }
                }
            }
        }
        return null;
    }

    public abstract void importData(Collection<Map<String, Object>> data) throws ParseException, RemoteException;
}
