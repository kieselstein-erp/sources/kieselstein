package com.lp.server.system.ejbfac.datenstrukturutilis.serializers;

import com.lp.server.system.ejb.DatenStrukturFeld;
import com.lp.server.system.ejbfac.datenstrukturutilis.exports.BaseExportDataUtil;
import com.lp.server.system.service.DatenStrukturDto;
import org.apache.http.Consts;
import org.apache.http.entity.ContentType;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class CSVSerializer extends AbstractBaseSerializer {

    private final char separator = ',';
    private final boolean createHeader = true;
    protected final ContentType contentType = ContentType.create("text/csv", Consts.UTF_8);
    private final String splitLineRegex;

    public static final int PARSE_INPUT_FIELDS_COL_MAP_MODE_LINE1_ATTRIBUTE = 1;
    protected int parseImportFieldsMode = PARSE_INPUT_FIELDS_COL_MAP_MODE_LINE1_ATTRIBUTE;

    public CSVSerializer(Map<Integer, Map<String, String>> strukturFeldValueMappings) {
        super(strukturFeldValueMappings);
        String otherThanQuote = " [^\"] ";
        String quotedString = String.format(" \" %s* \" ", otherThanQuote);
        this.splitLineRegex = String.format("(?x) " + // enable comments, ignore white spaces
                        "%s          " + // match a separator (comma)
                        "(?=         " + // start positive look ahead
                        "  (?:       " + //   start non-capturing group 1
                        "    %s*     " + //     match 'otherThanQuote' zero or more times
                        "    %s      " + //     match 'quotedString'
                        "  )*        " + //   end group 1 and repeat it zero or more times
                        "  %s*       " + //   match 'otherThanQuote'
                        "  $         " + // match the end of the string
                        ")           ",  // stop positive look ahead
                this.separator, otherThanQuote, quotedString, otherThanQuote);
    }

    private String quoteValueString(String value) {
        if (value == null) {
            return "";
        }
        if (value.indexOf(this.separator) >= 0 || value.contains("\"") || value.contains("'") || value.contains("\\") || value.contains("\n")) {
            if (value.contains("\"")) {
                return String.format("\"%s\"", value.replaceAll("\"", "\"\""));
            }
            return String.format("\"%s\"", value);
        }
        return value;
    }


    List<String> buildCSVLines(List<BaseExportDataUtil> data, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder, boolean createHeader) {
        List<String> lines = new ArrayList<>();
        if (createHeader) {
            StringBuilder sb = new StringBuilder();
            boolean first = true;
            for (DatenStrukturFeld sf : strukturFelder) {
                if (first) {
                    first = false;
                } else {
                    sb.append(separator);
                }
                sb.append("\"").append(sf.getPfad().replaceAll("\"", "\"\"")).append("\"");
            }
            lines.add(sb.toString());
        }
        for (BaseExportDataUtil dataUtil : data) {
            StringBuilder sb = new StringBuilder();
            boolean first = true;
            for (DatenStrukturFeld sf : strukturFelder) {
                if (first) {
                    first = false;
                } else {
                    sb.append(separator);
                }
                String value = getStrukturFeldValueAsString(dataUtil, sf, struktur.getDefaultDatumFormat(), struktur.getDefaultDatumFormat());
                sb.append(quoteValueString(value));
            }
            lines.add(sb.toString());
        }

        return lines;
    }

    @Override
    public void exportToFile(String fileName, List<BaseExportDataUtil> data, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder) throws IOException {
        boolean createHeader = this.createHeader && !new File(fileName).exists();
        List<String> csvLines = buildCSVLines(data, struktur, strukturFelder, createHeader);
        FileWriter fileWriter = new FileWriter(fileName);
        BufferedWriter writer = new BufferedWriter(fileWriter);
        for (String line : csvLines) {
            writer.write(line);
            writer.newLine();
        }
        writer.close();
    }

    @Override
    public String exportToString(List<BaseExportDataUtil> data, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder) {
        List<String> csvLines = buildCSVLines(data, struktur, strukturFelder, createHeader);
        StringBuilder sb = new StringBuilder();
        for (String line : csvLines) {
            sb.append(line);
            sb.append(System.lineSeparator());
        }
        return sb.toString();
    }

    @Override
    protected ContentType getContentType() {
        return this.contentType;
    }

    private List<DatenStrukturFeld> initImportFields(List<DatenStrukturFeld> strukturFelder, Map<String, Object> fixValues) {
        List<DatenStrukturFeld> csvRelevantFelder = new ArrayList<>();
        for (DatenStrukturFeld sf : strukturFelder) {
            if (sf.getERPFeld() != null && !sf.getERPFeld().isEmpty()) {
                if (sf.getPfad() != null && !sf.getPfad().isEmpty()) {
                    csvRelevantFelder.add(sf);
                } else if (sf.getFixWert() != null && !sf.getFixWert().isEmpty()) {
                    fixValues.put(sf.getERPFeld().trim(), parseValueString(sf.getFixWert(), sf));
                }
            }
        }
        return csvRelevantFelder;
    }

    private List<Map<String, Object>> parseCSVLines(Collection<String> lines, Map<String, Object> fixValues, List<DatenStrukturFeld> strukturFelder) {
        Map<String, Object> fixData = (fixValues != null ? new HashMap<>(fixValues) : new HashMap<>());
        for (DatenStrukturFeld sf : strukturFelder) {
            if (sf.getERPFeld() != null && !sf.getERPFeld().isEmpty() && sf.getFixWert() != null && !sf.getFixWert().isEmpty()) {
                fixData.put(sf.getERPFeld(), parseValueString(sf.getFixWert(), sf));
            }
        }
        List<Map<String, Object>> dataList = new ArrayList<>();
        Map<Integer, DatenStrukturFeld> strukturFelderColsMap = new HashMap<>();
        int linesOffset = 0;
        if (parseImportFieldsMode == PARSE_INPUT_FIELDS_COL_MAP_MODE_LINE1_ATTRIBUTE && lines != null && !lines.isEmpty()) {
            linesOffset = 1;
            Optional<String> firstLine = lines.stream().findFirst();
            String[] columnNames = firstLine.get().split(this.splitLineRegex, -1);
            for (int i = 0; i < columnNames.length; i++) {
                String colName = columnNames[i];
                for (DatenStrukturFeld sf : strukturFelder) {
                    if (sf.getERPFeld() != null && !sf.getERPFeld().isEmpty() && colName.equals(sf.getAttributName())) {
                        strukturFelderColsMap.put(i, sf);
                    }
                }
            }
        } else {
            throw new IllegalArgumentException("Not Supported Import Fields Mode " + parseImportFieldsMode + "!");
        }

        if (strukturFelderColsMap.isEmpty()) {
            dataList.add(fixData);
        } else {
            int lineCounter = 0;
            for (String line: lines) {
                if (lineCounter >= linesOffset) {
                    String[] fieldValues = line.split(this.splitLineRegex, -1);
                    Map<String, Object> data = new HashMap<>(fixData);
                    for (int i = 0; i < fieldValues.length; i++) {
                        if (strukturFelderColsMap.containsKey(i)) {
                            String fieldValue = fieldValues[i];
                            if (fieldValue.startsWith("\"") && fieldValue.endsWith("\"")) {
                                fieldValue = fieldValue.substring(1, fieldValue.length() - 1);
                            }
                            fieldValue = fieldValue.replaceAll("\"\"", "\"");
                            DatenStrukturFeld sf = strukturFelderColsMap.get(i);
                            data.put(sf.getERPFeld(), parseValueString(fieldValue, sf));
                        }
                    }
                    dataList.add(data);
                }
                lineCounter++;
            }
        }

        return dataList;
    }

    @Override
    public List<Map<String, Object>> buildImportDataFromFile(String fileName, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder) {
        Map<String, Object> fixValues = new HashMap<>(this.globalParameter);
        List<DatenStrukturFeld> csvRelevantFelder = initImportFields(strukturFelder, fixValues);
        try {
            return parseCSVLines(Files.readAllLines(Paths.get(fileName)), fixValues, csvRelevantFelder);
        } catch (IOException ignore) {
        }
        return null;
    }

    @Override
    public List<Map<String, Object>> buildImportDataFromString(String dataString, DatenStrukturDto struktur, List<DatenStrukturFeld> strukturFelder) {
        Map<String, Object> fixValues = new HashMap<>(this.globalParameter);
        List<DatenStrukturFeld> csvRelevantFelder = initImportFields(strukturFelder, fixValues);
        return parseCSVLines(dataString.lines().collect(Collectors.toUnmodifiableList()), fixValues, csvRelevantFelder);
    }

}