package com.lp.server.system.service;

public class SSTParameterDto extends AbstractBaseDto {

	private static final long serialVersionUID = 1L;
	private String cTransferTyp;
	private Integer strukturDatenId;
	private DatenStrukturDto datenStruktur;
	private Short iAusloeser;
	private String cQuellname;
	private String cTyp;
	private Short iDatenModus;  /* 1=alle, 2=geänderte*/
	private String cFormat;
	private String cBereich;
	private Integer iSort;
	private String cBezeichnung;
	private Short iNachverarbeitungModus;
	private boolean bAktiv;

	public String getTransferTyp() {
		return cTransferTyp;
	}

	public void setTransferTyp(String cTransferTyp) {
		this.cTransferTyp = cTransferTyp;
	}

	public DatenStrukturDto getDatenStruktur() {
		return datenStruktur;
	}

	public void setDatenStruktur(DatenStrukturDto datenStruktur) {
		this.datenStruktur = datenStruktur;
	}

	public Integer getStrukturDatenId() {
		return strukturDatenId;
	}

	public void setStrukturDatenId(Integer strukturDatenId) {
		this.strukturDatenId = strukturDatenId;
	}

	public Short getAusloeser() {
		return iAusloeser;
	}

	public void setAusloser(Short iAusloeser) {
		this.iAusloeser = iAusloeser;
	}

	public String getQuellname() {
		return cQuellname;
	}

	public void setQuellname(String cQuellname) {
		this.cQuellname = cQuellname;
	}

	public String getTyp() {
		return cTyp;
	}

	public void setTyp(String cTyp) {
		this.cTyp = cTyp;
	}

	public Short getDatenModus() {
		return iDatenModus;
	}

	public void setDatenModus(Short iDatenModus) {
		this.iDatenModus = iDatenModus;
	}

	public String getFormat() {
		return cFormat;
	}

	public void setFormat(String cFormat) {
		this.cFormat = cFormat;
	}

	public String getBereich() {
		return cBereich;
	}

	public void setBereich(String cBereich) {
		this.cBereich = cBereich;
	}

	public Integer getSort() {
		return iSort;
	}

	public void setSort(Integer iSort) {
		this.iSort = iSort;
	}

	public String getBezeichnung() {
		return cBezeichnung;
	}

	public void setBezeichnung(String cBezeichnung) {
		this.cBezeichnung = cBezeichnung;
	}

	public Short getNachverarbeitungModus() {
		return iNachverarbeitungModus;
	}

	public void setNachverarbeitungModus(Short iNachverarbeitungModus) {
		this.iNachverarbeitungModus = iNachverarbeitungModus;
	}

	public boolean getAktiv() {
		return bAktiv;
	}

	public void setAktiv(boolean bAktiv) {
		this.bAktiv = bAktiv;
	}

}
