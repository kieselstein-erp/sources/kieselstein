package com.lp.server.system.service;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BaseDtoAssambler {
    private static ModelMapper mapper = null;

    protected static ModelMapper getMapper() {
        if (mapper == null) {
            mapper = new ModelMapper();
            mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        }
        return mapper;
    }


    public static <D extends AbstractBaseDto> D mapEntity2Dto(Object source, Class<D> destinationType){
        D dto = getMapper().map(source, destinationType);
        if (dto.getIId() == null && source != null) {
            // Handle it if there is a name difference in the id property.
            for(Method method : source.getClass().getMethods()) {
                if (method.getParameterTypes().length == 0 &&
                        method.canAccess(source) &&
                        method.getName().equalsIgnoreCase("getid")) {
                    try {
                        dto.setIId((Integer) method.invoke(source));
                    } catch (IllegalAccessException | InvocationTargetException ignore) {
                    }
                    break;
                }
            }
        }
        return dto;
    }
}
