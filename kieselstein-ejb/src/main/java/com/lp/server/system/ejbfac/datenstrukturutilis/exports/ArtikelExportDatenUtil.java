package com.lp.server.system.ejbfac.datenstrukturutilis.exports;

import com.lp.server.artikel.ejb.*;
import com.lp.server.system.service.GenericInterfacesFac;
import com.lp.server.system.service.TheClientDto;
import com.lp.server.util.ArtikelId;
import com.lp.server.util.HvOptional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.*;

public class ArtikelExportDatenUtil extends BaseExportDataUtil {

    private final Artikel artikel;
    private Artikelspr artikelspr = null;
    private ArtikelTruTops artikelTruTops = null;
    private ArtikelTruTopsMetadaten artikelTruTopsMetadaten = null;

    public ArtikelExportDatenUtil (EntityManager em, TheClientDto client, Date timeStamp, String dataSection, Artikel artikel) {
        super(em, client, timeStamp);
        this.dataSection = dataSection;
        this.artikel = artikel;
    }

    private Artikelspr getArtikelsprIntern() {
        if (artikelspr == null) {
            artikelspr = em.find(Artikelspr.class, new ArtikelsprPK(artikel.getIId(), localeString));
            if (artikelspr == null) {
                artikelspr = new Artikelspr();  // Create a Empty Instance so it won't load it from the database more then necessary.
            }
        }
        return artikelspr;
    }

    private ArtikelTruTops getArtikelTruTopsIntern() {
        if (artikelTruTops == null) {
            artikelTruTops = getArtikelTruTops(em, artikel.getIId());
            if (artikelTruTops == null) {
                artikelTruTops = new ArtikelTruTops();  // Create a Empty Instance so it won't load it from the database more then necessary.
            }
        }
        return artikelTruTops;
    }

    private ArtikelTruTopsMetadaten getArtikelTruTopsMetadatenIntern() {
        if (artikelTruTopsMetadaten == null) {
            artikelTruTopsMetadaten = getLastArtikelTruTopsMetadaten(em, artikel.getIId());
            if (artikelTruTopsMetadaten == null) {
                artikelTruTopsMetadaten = new ArtikelTruTopsMetadaten();  // Create a Empty Instance so it won't load it from the database more then necessary.
            }
        }
        return artikelTruTopsMetadaten;
    }

    private static ArtikelTruTops getArtikelTruTops(EntityManager em, Integer artikelID) {
        HvOptional<ArtikelTruTops> entry = ArtikelTruTopsQuery.findByArtikelIId(em, new ArtikelId(artikelID));
        if (entry.isPresent()) {
            return entry.get();
        }
        return null;
    }

    public static ArtikelTruTopsMetadaten getLastArtikelTruTopsMetadaten(EntityManager em, Integer artikelID) {
        List<ArtikelTruTopsMetadaten> entities = ArtikelTruTopsMetadatenQuery.findByArtikelIId(em, new ArtikelId(artikelID));
        ArtikelTruTopsMetadaten retValue = null;
        for (ArtikelTruTopsMetadaten entity : entities) {
            if (retValue == null) {
                retValue = entity;
            } else if (retValue.getIId() < entity.getIId()) {
                retValue = entity;
            }
        }
        return retValue;
    }



    public void prefetchInstanceValues(Map<String, Map<Integer, Object>> prefetchedData, Collection<Integer> referenceKeys, Set<String> strukturFields) {
        if (GenericInterfacesFac.BEREICH_WW_ARTIKEL.equals(dataSection)) {
            if (strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_BEZEICHNUNG) ||
                strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_KURZ_BEZEICHNUNG)) {
                TypedQuery<Artikelspr> query = em.createNamedQuery("ArtikelsprFindAllByArtikelIIdsLocaleCNr", Artikelspr.class);
                query.setParameter("artikelIDs", referenceKeys);
                query.setParameter("locale", getLocaleFromClient(client));
                for (Artikelspr artikelspr : query.getResultList()) {
                    if (strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_BEZEICHNUNG)) {
                        setPrefetchedDataValue(prefetchedData, GenericInterfacesFac.FELD_ARTIKEL_BEZEICHNUNG, artikelspr.getPk().getArtikelIId(), artikelspr.getCBez());
                    }
                    if (strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_KURZ_BEZEICHNUNG)) {
                        setPrefetchedDataValue(prefetchedData, GenericInterfacesFac.FELD_ARTIKEL_BEZEICHNUNG, artikelspr.getPk().getArtikelIId(), artikelspr.getCKbez());
                    }
                }
            }
            if (strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_PFAD) ||
                strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_HASH) ||
                strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_GROESSE) ||
                strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_ERSTELLT) ||
                strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_MODIFIZIERT) ||
                strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_EXPORT_ENDE) ||
                strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_FEHLER_CODE) ||
                strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_FEHLER_TEXT)
            ) {
                Query q = em.createQuery("SELECT att, att.artikelTruTops FROM ArtikelTruTopsMetadaten att WHERE att.artikelTruTops.artikelIId IN (:artikelIDs) ORDER BY att.artikelTruTops.artikelIId, att.iId");
                q.setParameter("artikelIDs", referenceKeys);
                for (Object o : q.getResultList()) {
                    ArtikelTruTopsMetadaten truMat = (ArtikelTruTopsMetadaten) ((Object[])o)[0];
                    ArtikelTruTops artTruTops = (ArtikelTruTops) ((Object[])o)[1];
                    Integer artikelID = artTruTops.getArtikelIId();
                    if (strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_PFAD)) {
                        if (truMat.getFullPath() != null && !truMat.getFullPath().isEmpty()) {
                            setPrefetchedDataValue(prefetchedData, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_PFAD, artikelID, truMat.getFullPath());
                        } else if (artTruTops.getCPfad() != null && !artTruTops.getCPfad().isEmpty()) {
                            // Use as Fallback the Path from the Artikel-Tru-Tops.
                            setPrefetchedDataValue(prefetchedData, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_PFAD, artikelID, artTruTops.getCPfad());
                        }
                    }
                    if (strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_HASH)) {
                        setPrefetchedDataValue(prefetchedData, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_HASH, artikelID, truMat.getCHash());
                    }
                    if (strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_GROESSE)) {
                        setPrefetchedDataValue(prefetchedData, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_GROESSE, artikelID, truMat.getISize());
                    }
                    if (strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_ERSTELLT)) {
                        setPrefetchedDataValue(prefetchedData, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_ERSTELLT, artikelID, truMat.getTCreation());
                    }
                    if (strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_MODIFIZIERT)) {
                        setPrefetchedDataValue(prefetchedData, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_MODIFIZIERT, artikelID, truMat.getTModification());
                    }
                    if (strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_EXPORT_ENDE)) {
                        setPrefetchedDataValue(prefetchedData, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_EXPORT_ENDE, artikelID, artTruTops.getTExportEnde());
                    }
                    if (strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_FEHLER_CODE)) {
                        setPrefetchedDataValue(prefetchedData, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_FEHLER_CODE, artikelID, artTruTops.getCFehlercode());
                    }
                    if (strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_FEHLER_TEXT)) {
                        setPrefetchedDataValue(prefetchedData, GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_FEHLER_TEXT, artikelID, artTruTops.getCFehlertext());
                    }
                }
            }
        }
    }

    public Object getArtikelElementOutputValue(String erpFeld) {
        if (GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_NR.equals(erpFeld)) {
            String artikelCNr = artikel.getCNr();
            if (artikelCNr.matches("[0-9]+[a-zA-Z]$")) {
                return artikelCNr.substring(0, artikelCNr.length() - 1) + "-" + artikelCNr.substring(artikelCNr.length() - 1);
            }
            return artikelCNr;
        } else if (GenericInterfacesFac.FELD_ARTIKEL_ID.equals(erpFeld)) {
            return artikel.getIId();
        } else if (GenericInterfacesFac.FELD_ARTIKEL_CNR.equals(erpFeld)) {
            return artikel.getCNr();
        } else if (GenericInterfacesFac.FELD_ARTIKEL_BEZEICHNUNG.equals(erpFeld)) {
            Artikelspr artikelspr = getArtikelsprIntern();
            if (artikelspr != null) {
                return artikelspr.getCBez();
            }
            return null;
        } else if (GenericInterfacesFac.FELD_ARTIKEL_KURZ_BEZEICHNUNG.equals(erpFeld)) {
            Artikelspr artikelspr = getArtikelsprIntern();
            if (artikelspr != null) {
                return artikelspr.getCKbez();
            }
            return null;
        } else if (GenericInterfacesFac.FELD_ARTIKEL_MATERIAL_TRUTOPS_INFO.equals(erpFeld)) {
            if (artikel.getMaterialIId() != null) {
                Material material = em.find(Material.class, artikel.getMaterialIId());
                Geometrie geo = em.find(Geometrie.class, artikel.getIId());
                Laseroberflaeche laseroberflaeche = (artikel.getLaseroberflaecheIId() != null ? em.find(Laseroberflaeche.class, artikel.getLaseroberflaecheIId()) : null);
                String hoehe = String.format("%05.2f", (geo != null && geo.getFHoehe() != null ? geo.getFHoehe() : 0));
                hoehe = hoehe.replace(",", "");
                return (material != null ? material.getCNr() : "") + hoehe +
                        (laseroberflaeche != null ? laseroberflaeche.getCNr() : "");
            }
            return null;
        } else if (GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_PFAD.equals(erpFeld)) {
            ArtikelTruTopsMetadaten metaDaten = getArtikelTruTopsMetadatenIntern();
            String path = null;
            if (metaDaten.getFullPath() != null) {
                path = getArtikelTruTopsMetadatenIntern().getFullPath();
            } else if (getArtikelTruTopsIntern().getIId() != null) {
                // Use as Fallback the Path from the ArtikelTruTops.
                path = getArtikelTruTopsIntern().getCPfad();
            }
            return path;
        } else if (GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_HASH.equals(erpFeld)) {
            return getArtikelTruTopsMetadatenIntern().getCHash();
        } else if (GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_GROESSE.equals(erpFeld)) {
            return getArtikelTruTopsMetadatenIntern().getISize();
        } else if (GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_ERSTELLT.equals(erpFeld)) {
            return getArtikelTruTopsMetadatenIntern().getTCreation();
        } else if (GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_MODIFIZIERT.equals(erpFeld)) {
            return getArtikelTruTopsMetadatenIntern().getTModification();
        } else if (GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_EXPORT_ENDE.equals(erpFeld)) {
            return getArtikelTruTopsIntern().getTExportEnde();
        } else if (GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_FEHLER_CODE.equals(erpFeld)) {
            return getArtikelTruTopsIntern().getCFehlercode();
        } else if (GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_FEHLER_TEXT.equals(erpFeld)) {
            return getArtikelTruTopsIntern().getCFehlertext();
        } else {
            return getGlobalOutputValues(erpFeld);
        }
    }

    @Override
    public Integer getInstanceKey() {
        if (GenericInterfacesFac.BEREICH_WW_ARTIKEL.equals(dataSection)) {
            return artikel.getIId();
        }
        return null;
    }


    @Override
    public Object getElementOutputValue(String erpFeld) {
        if (isValueCached(erpFeld)) {
            return getPrefetchedValue(erpFeld);
        }if (GenericInterfacesFac.BEREICH_WW_ARTIKEL.equals(dataSection)) {
            return getArtikelElementOutputValue(erpFeld);
        } else {
            throw new IllegalArgumentException("Unsupported Bereich: " + dataSection);
        }
    }

}
