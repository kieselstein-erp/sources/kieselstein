package com.lp.server.system.service;

import com.lp.server.system.ejb.SSTLauf;
import com.lp.util.EJBExceptionLP;

import javax.ejb.Remote;
import java.sql.Timestamp;
import java.util.*;

import static com.lp.server.benutzer.service.RechteFac.*;

@Remote
public interface GenericInterfacesFac {

	String SST_TYP_EXPORT = "E";
	String SST_TYP_IMPORT = "I";

	String SST_TRANSFER_TYP_FILE = "FILE";
	String SST_TRANSFER_TYP_REST = "REST-RECEIVER";
	String SST_TRANSFER_TYP_REST_CALL = "REST-DISPATCHER";

	Short SST_AUSLOESER_SHOP_TIMER = 1;
	Short SST_AUSLOESER_LOS_AUSGABE = 2;
	Short SST_AUSLOESER_REST_CALL = 3;

	Short SST_NACHVERARBEITUNG_MODUS_KEINE = 0;
	Short SST_NACHVERARBEITUNG_MODUS_DATEI_LOESCHEN = 1;
	Short SST_NACHVERARBEITUNG_MODUS_DATEI_UMBENENNEN = 2;

	Short SST_DATEN_MODUS_ALL = 1;
	Short SST_DATEN_MODUS_CHANGED = 2;

	String SST_BEREICH_FERT_TRUMPF_SOLL_MATERIAL = "FERT_TRUMPF_SOLL_MATERIAL";
	String SST_BEREICH_WW_ARTIKEL_TRU_TOPS = "WW_ARTIKEL_TOPS";
	String SST_BEREICH_FERT_TRUMPF_PROD_ORDER = "FERT_TRUMPF_PROD_ORDER";
	String SST_BEREICH_WW_ARTIKEL_TRU_TOPS_LOG = "WW_ARTIKEL_TRU_TOPS_LOG";

	String SST_FORMAT_XML = "XML";
	String SST_FORMAT_CSV = "CSV";

	String BEREICH_FERT_LOSSOLLMATERIAL = "fert_lossollmaterial";
	String BEREICH_WW_ARTIKEL = "ww_artikel";

	Map<String, String> EXPORT_SST_BEREICHE = new HashMap<>(){{
		put(SST_BEREICH_FERT_TRUMPF_SOLL_MATERIAL, RECHT_FERT_LOS_R);
		put(SST_BEREICH_WW_ARTIKEL_TRU_TOPS, RECHT_WW_ARTIKEL_R);
	}};

	Map<String, String> IMPORT_SST_BEREICHE = new HashMap<>(){{
		put(SST_BEREICH_FERT_TRUMPF_PROD_ORDER, RECHT_FERT_LOS_CUD);
		put(SST_BEREICH_WW_ARTIKEL_TRU_TOPS, RECHT_WW_ARTIKEL_CUD);
		put(SST_BEREICH_WW_ARTIKEL_TRU_TOPS_LOG, RECHT_WW_ARTIKEL_CUD);
	}};

	/**********************************************************
	 * Felder welche Global zur Verfügung stehen.
	 **********************************************************/
	String GLOBAL_FELD_ZEITSTEMPEL_STR1 = "ZEITSTEMPEL_STR1";  // Export Zeitstempel im Format "yyyyMMddHHmmss"
	String GLOBAL_FELD_IMPORT_ZEITSTEMPEL = "@@IMPORT_ZEITSTEMPEL@@";
	String GLOBAL_FELD_IMPORT_FILE_NAME = "@@IMPORT_FILE_NAME@@";

	/**********************************************************
	 * Felder für Artikel
	 **********************************************************/
	String FELD_ARTIKEL_TRUTOPS_NR = "ARTIKEL_TRUTOPS_NR";
	String FELD_ARTIKEL_ID = "ARTIKEL_ID";
	String FELD_ARTIKEL_CNR = "ARTIKEL_CNR";
	String FELD_ARTIKEL_BEZEICHNUNG = "ARTIKEL_BEZEICHNUNG";
	String FELD_ARTIKEL_KURZ_BEZEICHNUNG = "ARTIKEL_KURZ_BEZEICHNUNG";
	String FELD_ARTIKEL_MATERIAL_TRUTOPS_INFO = "ARTIKEL_MATERIAL_TRUTOPS_INFO";
	String FELD_ARTIKEL_TRUTOPS_METADATEN_PFAD = "ARTIKEL_TRUTOPS_METADATEN_PFAD";
	String FELD_ARTIKEL_TRUTOPS_METADATEN_HASH = "ARTIKEL_TRUTOPS_METADATEN_HASH";
	String FELD_ARTIKEL_TRUTOPS_METADATEN_GROESSE = "ARTIKEL_TRUTOPS_METADATEN_GROESSE";
	String FELD_ARTIKEL_TRUTOPS_METADATEN_ERSTELLT = "ARTIKEL_TRUTOPS_METADATEN_ERSTELLT";
	String FELD_ARTIKEL_TRUTOPS_METADATEN_MODIFIZIERT = "ARTIKEL_TRUTOPS_METADATEN_MODIFIZIERT";
	String FELD_ARTIKEL_TRUTOPS_EXPORT_ENDE = "ARTIKEL_TRUTOPS_EXPORT_ENDE";
	String FELD_ARTIKEL_TRUTOPS_FEHLER_CODE = "ARTIKEL_TRUTOPS_FEHLER_CODE";
	String FELD_ARTIKEL_TRUTOPS_FEHLER_TEXT = "ARTIKEL_TRUTOPS_FEHLER_TEXT";

	Set<String> BEREICH_ARTIKEL_FELDER = new HashSet<>(){{
		add(FELD_ARTIKEL_ID);
		add(FELD_ARTIKEL_CNR);
		add(FELD_ARTIKEL_BEZEICHNUNG);
		add(FELD_ARTIKEL_KURZ_BEZEICHNUNG);
		add(FELD_ARTIKEL_TRUTOPS_NR);
		add(FELD_ARTIKEL_MATERIAL_TRUTOPS_INFO);
		add(FELD_ARTIKEL_TRUTOPS_METADATEN_PFAD);
		add(FELD_ARTIKEL_TRUTOPS_METADATEN_HASH);
		add(FELD_ARTIKEL_TRUTOPS_METADATEN_GROESSE);
		add(FELD_ARTIKEL_TRUTOPS_METADATEN_ERSTELLT);
		add(FELD_ARTIKEL_TRUTOPS_METADATEN_MODIFIZIERT);
		add(FELD_ARTIKEL_TRUTOPS_EXPORT_ENDE);
		add(FELD_ARTIKEL_TRUTOPS_FEHLER_CODE);
		add(FELD_ARTIKEL_TRUTOPS_FEHLER_TEXT);
	}};

	/**********************************************************
	 * TODO
	 **********************************************************/
	String FELD_TRU_TOPS_LOG_MATERIAL_ARTIKEL_KURZ_BEZEICHNUNG = "TRU_TOPS_LOG_MATERIAL_ARTIKEL_KURZ_BEZEICHNUNG";
	String FELD_TRU_TOPS_LOG_TEIL_GEWICHT = "TRU_TOPS_LOG_TEIL_GEWICHT";
	String FELD_TRU_TOPS_LOG_BEARBEITUNG_DAUER_MINUTE = "TRU_TOPS_LOG_BEARBEITUNG_DAUER_MINUTE";
	String FELD_TRU_TOPS_LOG_LASER_KOSTE_STUNDE = "TRU_TOPS_LOG_LASER_KOSTE_STUNDE";
	String FELD_TRU_TOPS_LOG_LAGER_ID = "TRU_TOPS_LOG_LAGER_ID";
	String FELD_TRU_TOPS_LOG_BREITE_ARTIKEL = "TRU_TOPS_LOG_BREITE_ARTIKEL";
	String FELD_TRU_TOPS_LOG_LAENGE_ARTIKEL = "TRU_TOPS_LOG_LAENGE_ARTIKEL";
	String FELD_TRU_TOPS_LOG_HOEHE_ARTIKEL = "TRU_TOPS_LOG_HOEHE_ARTIKEL";
	String FELD_TRU_TOPS_LOG_MEHRVERBRAUCH_LASER_IN_MM = "TRU_TOPS_LOG_MEHRVERBRAUCH_LASER_IN_MM";

	/**********************************************************
	 * Felder für Kunde
	 **********************************************************/
	String FELD_KUNDEN_NAME = "KUNDEN_NAME";
	String FELD_KUNDEN_DEBITORENKONTO_NR = "KUNDEN_DEBITORENKONTO_NR";

	Set<String> BEREICH_KUNDE_FELDER = new HashSet<String>(){{
		add(FELD_KUNDEN_NAME);
		add(FELD_KUNDEN_DEBITORENKONTO_NR);
	}};

	/**********************************************************
	 * Felder für Los
	 **********************************************************/
	String FELD_LOS_ID = "LOS_ID";
	String FELD_LOS_CNR = "LOS_CNR";
	String FELD_LOS_TRUMPF_FAELLIG_DATUM = "LOS_TRUMPF_FAELLIG_DATUM";

	Set<String> BEREICH_LOS = new HashSet<String>(){{
		addAll(BEREICH_KUNDE_FELDER);
		add(FELD_LOS_ID);
		add(FELD_LOS_CNR);
		add(FELD_LOS_TRUMPF_FAELLIG_DATUM);
	}};

	/**********************************************************
	 * Felder für Los-Soll-Material
	 **********************************************************/
	String FELD_LOSSOLLMATERIAL_ID = "LOSSOLLMATERIAL_ID";
	String FELD_LOSSOLLMATERIAL_MENGE = "LOSSOLLMATERIAL_MENGE";
	String FELD_LOSSOLLMATERIAL_EINHEIT = "LOSSOLLMATERIAL_EINHEIT";
	String FELD_LOSSOLLMATERIAL_SORT = "LOSSOLLMATERIAL_SORT";
	String FELD_LOSSOLLMATERIAL_TRUTOPS_ORDERNO = "LOSSOLLMATERIAL_TRUTOPS_ORDERNO";
	String FELD_LOSSOLLMATERIAL_TRUTOPS_EXPORT_ENDE = "LOSSOLLMATERIAL_TRUTOPS_EXPORT_ENDE";
	String FELD_LOSSOLLMATERIAL_TRUTOPS_FEHLER_CODE = "LOSSOLLMATERIAL_TRUTOPS_EXPORT_FEHLER_CODE";
	String FELD_LOSSOLLMATERIAL_TRUTOPS_FEHLER_TEXT = "LOSSOLLMATERIAL_TRUTOPS_EXPORT_FEHLER_TEXT";

	Set<String> BEREICH_LOS_SOLL_MATERIAL_FELDER = new HashSet<String>(){{
		addAll(BEREICH_LOS);
		addAll(BEREICH_ARTIKEL_FELDER);
		add(FELD_LOSSOLLMATERIAL_ID);
		add(FELD_LOSSOLLMATERIAL_MENGE);
		add(FELD_LOSSOLLMATERIAL_EINHEIT);
		add(FELD_LOSSOLLMATERIAL_SORT);
		add(FELD_LOSSOLLMATERIAL_TRUTOPS_ORDERNO);
		add(FELD_LOSSOLLMATERIAL_TRUTOPS_EXPORT_ENDE);
		add(FELD_LOSSOLLMATERIAL_TRUTOPS_FEHLER_CODE);
		add(FELD_LOSSOLLMATERIAL_TRUTOPS_FEHLER_TEXT);
	}};

	short SST_LAUF_STATUS_STARTED = 0;
	short SST_LAUF_STATUS_FINISHED = 1;
	short SST_LAUF_STATUS_ERROR = -1;

	SSTParameterDto createSSTParameter(SSTParameterDto sstParameterDto, TheClientDto clientDto) throws EJBExceptionLP;

	String testStrukturOutput(Integer id, String format, Integer referenceInstanceID, TheClientDto clientDto) throws EJBExceptionLP;

	Collection<SSTParameterDto> getSSTParameterByTypAndAusloeser(String typ, short ausloeser);

	Collection<Integer> getSSTExportReferenceIDs(SSTParameterDto sst, Collection<Integer> referenceIDs, Timestamp lastExport, TheClientDto client);

	Collection<Integer> getSSTExportReferenceIDs(SSTParameterDto sst, TheClientDto client);

	SSTLaufDto createSSTLaufStart(Timestamp startTimestamp, SSTParameterDto sst);

	SSTLaufDto handleSSTLaufFehlermeldung(SSTLaufDto sstRun, String fehlermeldung, Integer anzahlDatensaetze);

	SSTLaufDto handleSSTLaufException(SSTLaufDto sstRun, Exception e, Integer anzahlDatensaetze);

    SSTLaufDto doSSTExport(SSTParameterDto sst, Collection<Integer> referenceInstanceIDs, TheClientDto client) throws EJBExceptionLP;

	SSTExportResult doSSTExportForRESTCallToBytes(Integer sstID, String format, Map<String, List<String>> queryParams, TheClientDto client) throws EJBExceptionLP;

	SSTLaufDto doSSTImport(SSTParameterDto sst, TheClientDto client) throws EJBExceptionLP;

	SSTLaufDto doSSTImportForRESTCallFromString(Integer sstID, String format, Map<String, List<String>> queryParams, String payloadData, TheClientDto client) throws EJBExceptionLP;

}