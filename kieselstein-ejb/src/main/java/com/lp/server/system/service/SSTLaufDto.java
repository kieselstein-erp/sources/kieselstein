package com.lp.server.system.service;

import java.sql.Timestamp;

public class SSTLaufDto extends AbstractBaseDto {

	private static final long serialVersionUID = 1L;
	private Integer sstId;
	private SSTParameterDto sst;
	private Timestamp tZeitstempelStart ;
	private Timestamp tZeitstempelEnde ;
	private Short iStatus;
	private Integer iAnzahlDatensaetze;
	private String cFehlermeldung;

	public Integer getSstId() {
		return sstId;
	}

	public void setSstId(Integer sstId) {
		this.sstId = sstId;
	}

	public SSTParameterDto getSST() {
		return sst;
	}

	public void setSST(SSTParameterDto sst) {
		this.sst = sst;
	}

	public Timestamp getZeitstempelStart() {
		return tZeitstempelStart;
	}

	public void setZeitstempelStart(Timestamp tZeitstempelStart) {
		this.tZeitstempelStart = tZeitstempelStart;
	}

	public Timestamp getZeitstempelEnde() {
		return tZeitstempelEnde;
	}

	public void setZeitstempelEnde(Timestamp tZeitstempelEnde) {
		this.tZeitstempelEnde = tZeitstempelEnde;
	}

	public Short getStatus() {
		return iStatus;
	}

	public void setStatus(Short iStatus) {
		this.iStatus = iStatus;
	}

	public Integer getAnzahlDatensaetze() {
		return iAnzahlDatensaetze;
	}

	public void setAnzahlDatensaetze(Integer iAnzahlDatensaetze) {
		this.iAnzahlDatensaetze = iAnzahlDatensaetze;
	}

	public String getFehlermeldung() {
		return cFehlermeldung;
	}

	public void setFehlermeldung(String cFehlermeldung) {
		this.cFehlermeldung = cFehlermeldung;
	}

	public String getSmallFehlermeldung() {
		if (cFehlermeldung != null && !cFehlermeldung.trim().isEmpty()) {
			if (cFehlermeldung.contains("\n")) {
				return cFehlermeldung.substring(0, cFehlermeldung.indexOf("\n")).trim();
			} else {
				return cFehlermeldung.trim();
			}
		}
		return null;
	}

}