package com.lp.server.system.automatikjob;

import com.lp.server.system.service.GenericInterfacesFac;
import com.lp.server.system.service.SSTLaufDto;
import com.lp.server.system.service.SSTParameterDto;
import com.lp.server.system.service.TheClientDto;
import com.lp.util.EJBExceptionLP;

import java.sql.Timestamp;

import static com.lp.server.system.service.GenericInterfacesFac.SST_AUSLOESER_SHOP_TIMER;
import static com.lp.server.system.service.GenericInterfacesFac.SST_TYP_IMPORT;


public class AutomatikjobSSTImport extends AutomatikjobBasis {


	@Override
	public boolean performJob(TheClientDto client) {
		GenericInterfacesFac fac = getGenericInterfacesFac();
		for (SSTParameterDto sst : fac.getSSTParameterByTypAndAusloeser(SST_TYP_IMPORT, SST_AUSLOESER_SHOP_TIMER)) {
			Timestamp startTimestamp = new Timestamp(System.currentTimeMillis());
			try {
				fac.doSSTImport(sst, client);
			} catch (EJBExceptionLP e) {
				// if an EJBExceptionLP will be thrown than, there is no SSTRun-Instance created so create it manually.
				SSTLaufDto sstRun = fac.createSSTLaufStart(startTimestamp, sst);
				fac.handleSSTLaufException(sstRun, e, null);
			}

		}
		return false;
	}

}
