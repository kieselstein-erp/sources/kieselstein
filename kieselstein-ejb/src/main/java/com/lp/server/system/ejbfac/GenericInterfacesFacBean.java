package com.lp.server.system.ejbfac;

import com.lp.server.artikel.ejb.Artikel;
import com.lp.server.artikel.ejb.ArtikelTruTops;
import com.lp.server.artikel.ejb.ArtikelTruTopsMetadaten;
import com.lp.server.artikel.ejb.Artkla;
import com.lp.server.fertigung.ejb.Los;
import com.lp.server.fertigung.ejb.Lossollmaterial;
import com.lp.server.system.ejb.*;
import com.lp.server.system.ejbfac.datenstrukturutilis.exports.ArtikelExportDatenUtil;
import com.lp.server.system.ejbfac.datenstrukturutilis.exports.BaseExportDataUtil;
import com.lp.server.system.ejbfac.datenstrukturutilis.exports.FertigungDatenUtil;
import com.lp.server.system.ejbfac.datenstrukturutilis.imports.ArtikelImportUtil;
import com.lp.server.system.ejbfac.datenstrukturutilis.imports.AbstractBaseImportDataUtil;
import com.lp.server.system.ejbfac.datenstrukturutilis.imports.FertigungImportUtil;
import com.lp.server.system.ejbfac.datenstrukturutilis.serializers.AbstractBaseSerializer;
import com.lp.server.system.ejbfac.datenstrukturutilis.serializers.CSVSerializer;
import com.lp.server.system.ejbfac.datenstrukturutilis.serializers.XMLSerializer;
import com.lp.server.system.pkgenerator.PKConst;
import com.lp.server.system.service.*;
import com.lp.server.util.Facade;
import com.lp.util.EJBExceptionLP;
import org.jboss.ejb3.annotation.TransactionTimeout;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.*;
import javax.persistence.criteria.*;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.*;

import static com.lp.server.benutzer.service.RechteFac.*;

@Stateless
public class GenericInterfacesFacBean extends Facade implements GenericInterfacesFac {
	@PersistenceContext
	private EntityManager em;

	@Resource
	private SessionContext ctx;

	public SSTParameterDto createSSTParameter(SSTParameterDto sstParameterDto, TheClientDto clientDto) throws EJBExceptionLP {
		checkPermissions(RECHT_LP_SYSTEM_CUD, clientDto);

		// TODO DSk Create Logic-Checks that the Structures have the necessary fields.
		//
		Integer id = getPKGeneratorObj().getNextPrimaryKey(PKConst.PK_SST_PARAMETER);
		DatenStruktur struktur = em.find(DatenStruktur.class, sstParameterDto.getStrukturDatenId());
		SSTParameter instance = new SSTParameter(
			id, sstParameterDto.getTransferTyp(), struktur.getId(), sstParameterDto.getAusloeser(),
			sstParameterDto.getQuellname(), sstParameterDto.getTyp(), sstParameterDto.getDatenModus(), sstParameterDto.getFormat(),
			sstParameterDto.getBereich(), sstParameterDto.getSort(), sstParameterDto.getBezeichnung(), sstParameterDto.getNachverarbeitungModus(),
			sstParameterDto.getAktiv(), clientDto.getIDPersonal()
		);

		em.persist(instance);
		em.flush();
		return SSTParameterDtoAssembler.createDto(instance, struktur);
	}

	private List<BaseExportDataUtil> getDatenUtilsForStrukturReferences(DatenStrukturDto struktur, Collection<Integer> referenceInstanceIDs, Date startTimeStamp, TheClientDto client) {
		List<BaseExportDataUtil> returnValues = new ArrayList<>();
		if (!referenceInstanceIDs.isEmpty()) {
			if (BEREICH_FERT_LOSSOLLMATERIAL.equals(struktur.getBereich())) {
				TypedQuery<Object[]> losSollMatQuery = em.createNamedQuery("LosSollMaterialWithJoinedData", Object[].class);
				losSollMatQuery.setParameter("ids", referenceInstanceIDs);
				List<Object[]> results = losSollMatQuery.getResultList();

				for (Integer referenceID : referenceInstanceIDs) {
					// Ensure the Result-List has the same order as the given IDs.
					for (Object[] result : results) {
						Lossollmaterial lossollmaterial = (Lossollmaterial) result[0];
						if (lossollmaterial.getIId().equals(referenceID)) {
							Los los = (Los) result[1];
							Artikel artikel = (Artikel) result[2];
							returnValues.add(new FertigungDatenUtil(em, client, startTimeStamp, getFertigungFac(), struktur.getBereich(), los, lossollmaterial, artikel));
							break;
						}
					}
				}

			} else if (BEREICH_WW_ARTIKEL.equals(struktur.getBereich())) {
				TypedQuery<Artikel> artikelQuery = em.createNamedQuery("ArikelFindByIDs", Artikel.class);
				artikelQuery.setParameter("ids", referenceInstanceIDs);
				List<Artikel> results = artikelQuery.getResultList();

				for (Integer referenceID : referenceInstanceIDs) {
					// Ensure the Result-List has the same order as the given IDs.
					for (Artikel artikel : results) {
						if (artikel.getIId().equals(referenceID)) {
							returnValues.add(new ArtikelExportDatenUtil(em, client, startTimeStamp, struktur.getBereich(), artikel));
							break;
						}
					}
				}
			} else {
				throw new IllegalArgumentException("Nicht unterstützter Bereich: " + struktur.getBereich());
			}
		}
		return returnValues;
	}

	private AbstractBaseSerializer getFormatSerializer(String format, Integer strukturID) {
		Map<Integer, Map<String, String>> strukturFeldValueMappings = new HashMap<>();
		TypedQuery<MappingWert> mappingQuery = em.createQuery("SELECT object(mw) FROM MappingWert mw " +
				"WHERE EXISTS (SELECT DISTINCT sf.mappingIId FROM DatenStrukturFeld sf WHERE sf.mappingIId = mw.mappingIId AND sf.strukturDatenId = :strukturID) " +
				"ORDER BY mw.mappingIId, mw.cQuellwert", MappingWert.class);
		mappingQuery.setParameter("strukturID", strukturID);
		for (MappingWert mw : mappingQuery.getResultList()) {
			if (!strukturFeldValueMappings.containsKey(mw.getMappingId())) {
				strukturFeldValueMappings.put(mw.getMappingId(), new HashMap<>());
			}
			strukturFeldValueMappings.get(mw.getMappingId()).put(mw.getQuellwert(), mw.getZielwert());
		}
		if (SST_FORMAT_XML.equalsIgnoreCase(format)) {
			return new XMLSerializer(strukturFeldValueMappings);
		} else if (SST_FORMAT_CSV.equals(format)) {
			return new CSVSerializer(strukturFeldValueMappings);
		} else {
			throw new IllegalArgumentException("Nicht unterstütztes Schnittstellen Format: " + format);
		}
	}

	public String testStrukturOutput(Integer id, String format, Integer referenceInstanceID, TheClientDto client) throws EJBExceptionLP {
		DatenStrukturDto struktur = BaseDtoAssambler.mapEntity2Dto(em.find(DatenStruktur.class, id), DatenStrukturDto.class);

		List<BaseExportDataUtil> dataUtils = getDatenUtilsForStrukturReferences(struktur, Collections.singleton(referenceInstanceID), new Date(), client);
		AbstractBaseSerializer formatSerializer = getFormatSerializer(format, struktur.getIId());
		TypedQuery<DatenStrukturFeld> sFelderQuery = em.createNamedQuery("DatenStrukturFelderFindByStrukturId", DatenStrukturFeld.class);
		sFelderQuery.setParameter("strukturDatenId", struktur.getIId());
		List<DatenStrukturFeld> strukturFelder = sFelderQuery.getResultList();
        return formatSerializer.exportToString(dataUtils,struktur, strukturFelder);
	}

	private String parseQuellName(String originalQuellName, BaseExportDataUtil datenUtil) {
		String newString = originalQuellName;
		int indexParamStart = newString.indexOf("@@");
		while (indexParamStart >= 0) {
			int indexParamEnd = newString.indexOf("@@", indexParamStart + 1);
			if (indexParamStart < indexParamEnd) {
				String paramName = newString.substring(indexParamStart + 2, indexParamEnd).trim();
				newString = newString.replaceAll(
						"@@" + paramName + "@@",
						String.valueOf(datenUtil.getElementOutputValue(paramName))
				);

				indexParamStart = newString.indexOf("@@");
			} else {
				indexParamStart = -1;
			}
		}
		return newString;
	}

	public SSTParameterDto findSSTParameter(Integer id) {
		Query sstParametersQuery = em.createNamedQuery("SSTParameterWithStrukturFindByID");
		sstParametersQuery.setParameter("iId", id);

		for (Object data : sstParametersQuery.getResultList()) {
			SSTParameter sst = (SSTParameter) ((Object[])data)[0];
			DatenStruktur struktur = (DatenStruktur) ((Object[])data)[1];
			return SSTParameterDtoAssembler.createDto(sst, struktur);
		}
		return null;
	}

	private SSTParameterDto findActiveSSTParameter(Integer id) {
		SSTParameterDto sst = findSSTParameter(id);
		if (sst != null && sst.getAktiv()) {
			return sst;
		}
		return null;
	}

	public Collection<SSTParameterDto> getSSTParameterByTypAndAusloeser(String typ, short ausloeser) {
		Query sstParametersQuery = em.createNamedQuery("SSTParameterWithStrukturFindByAusloeserUndTyp");
		sstParametersQuery.setParameter("ausloeser", ausloeser);
		sstParametersQuery.setParameter("typ", typ);
		List<SSTParameterDto> returnValues = new ArrayList<>();

		for (Object data : sstParametersQuery.getResultList()) {
			SSTParameter sst = (SSTParameter) ((Object[])data)[0];
			DatenStruktur struktur = (DatenStruktur) ((Object[])data)[1];
			returnValues.add(SSTParameterDtoAssembler.createDto(sst, struktur));
		}
		return returnValues;
	}

	/**
	 * Adds the Query-Condition for the Changed Data for the Interfaces SST_BEREICH_FERT_TRUMPF_SOLL_MATERIAL & SST_BEREICH_WW_ARTIKEL_TRU_TOPS sections.
	 * This is a complexer scenario, here has to be checked if the Artikel is changed or the MetaData of the TruTops Artikel.
	 */
	private void addArticleTruTopsDataChangedCondition(
			CriteriaBuilder builder, CriteriaQuery<Integer> cq, List<Predicate> conditions, javax.persistence.criteria.Path<Artikel> artikelPath, Timestamp lastExport
	){
		Subquery<ArtikelTruTops> truTopsSubquery = cq.subquery(ArtikelTruTops.class);
		Root<ArtikelTruTops> detailRoot = truTopsSubquery.from(ArtikelTruTops.class);
		Join<ArtikelTruTops, ArtikelTruTopsMetadaten> detailJoin = detailRoot.join("metadaten", JoinType.INNER);
		truTopsSubquery.select(detailRoot).where(
				builder.and(
						builder.equal(detailRoot.get("artikelIId"), artikelPath.get("iId")),
						builder.greaterThanOrEqualTo(detailJoin.get("tAendern"), lastExport)
				)
		);
		conditions.add(
				builder.or(
						builder.greaterThanOrEqualTo(artikelPath.get("tAendern"), lastExport),
						builder.exists(truTopsSubquery)
				)
		);
	}

	public Collection<Integer> getSSTExportReferenceIDs(SSTParameterDto sst, Collection<Integer> referenceIDs, Timestamp lastExport, TheClientDto client) {
		String sstBereich = sst.getBereich();
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Integer> cq = builder.createQuery(Integer.class);
		javax.persistence.criteria.Path<Integer> idPath;

		List<Predicate> conditions = new ArrayList<>();

		if (SST_DATEN_MODUS_CHANGED.equals(sst.getDatenModus())) {
			if (lastExport == null) {
				// No last Export is given select the last one from the SSTLauf.
				CriteriaQuery<Timestamp> lastExportCQ = builder.createQuery(Timestamp.class);
				Root<SSTLauf> sstLaufRoot = lastExportCQ.from(SSTLauf.class);
				javax.persistence.criteria.Path<Timestamp> timestampPath = sstLaufRoot.get("tZeitstempelStart");
				lastExportCQ.select(builder.greatest(timestampPath)).where(
						builder.and(
								builder.equal(sstLaufRoot.get("sstId"), sst.getIId()),
								builder.equal(sstLaufRoot.get("iStatus"), SST_LAUF_STATUS_FINISHED),
								builder.isNotNull(timestampPath)
						)
				);
				lastExport = em.createQuery(lastExportCQ).getSingleResult();
			}
		} else if (!SST_DATEN_MODUS_ALL.equals(sst.getDatenModus())) {
			throw new IllegalArgumentException("Schnittstellen-Datenmodus " + sst.getDatenModus() + " ist nicht Unterstützt!");
		}

		if (SST_BEREICH_FERT_TRUMPF_SOLL_MATERIAL.equals(sstBereich)) {
			Root<Lossollmaterial> root = cq.from(Lossollmaterial.class);
			idPath = root.get("iId");
			Join<Lossollmaterial, Artikel> artikelJoin = root.join("artikel", JoinType.INNER);
			Join<Artikel, Artkla> artklaJoin = artikelJoin.join("artkla", JoinType.INNER);
			conditions.add(builder.notEqual(artklaJoin.get("bTops"), 0));

			if (Objects.equals(sst.getDatenModus(), SST_DATEN_MODUS_CHANGED) && lastExport != null) {
				addArticleTruTopsDataChangedCondition(builder, cq, conditions, artikelJoin, lastExport);
			}
		} else if (SST_BEREICH_WW_ARTIKEL_TRU_TOPS.equals(sstBereich)) {
			Root<Artikel> root = cq.from(Artikel.class);
			idPath = root.get("iId");
			Join<Artikel, Artkla> artklaJoin = root.join("artkla", JoinType.LEFT);
			conditions.add(builder.notEqual(artklaJoin.get("bTops"), 0));
			conditions.add(builder.equal(root.get("mandantCNr"), client.getMandant()));

			if (SST_DATEN_MODUS_CHANGED.equals(sst.getDatenModus()) && lastExport != null) {
				addArticleTruTopsDataChangedCondition(builder, cq, conditions, root, lastExport);
				// TODO DSK Test.
			}
		} else {
			throw new IllegalArgumentException("SST-Bereich " + sstBereich + " ist nicht Unterstützt!");
		}

		if (referenceIDs != null) {
			conditions.add(idPath.in(referenceIDs));
		}

		if (!conditions.isEmpty()) {
			cq.where(builder.and(conditions.toArray(new Predicate[0])));
		}

		cq.select(idPath);
		TypedQuery<Integer> query = em.createQuery(cq);
		return query.getResultList();
	}

	public Collection<Integer> getSSTExportReferenceIDs(SSTParameterDto sst, TheClientDto client) {
		return this.getSSTExportReferenceIDs(sst, null, null, client);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SSTLaufDto createSSTLaufStart(Timestamp startTimestamp, SSTParameterDto sst) {
		SSTLauf sstRun = new SSTLauf(
				getPKGeneratorObj().getNextPrimaryKey(PKConst.PK_SST_LAUF), sst.getIId(),
				startTimestamp, null, SST_LAUF_STATUS_STARTED, null, null
		);
		em.persist(sstRun);
		em.flush();
		SSTLaufDto dto = BaseDtoAssambler.mapEntity2Dto(sstRun, SSTLaufDto.class);
		dto.setSST(sst);
		return dto;
	}

	private SSTLaufDto setSSTLaufEnde(SSTLaufDto sstRun, Integer anzahlDatensaetze) {
		Timestamp now = new Timestamp(System.currentTimeMillis());
		SSTLauf sstRunInstance = em.find(SSTLauf.class, sstRun.getIId());
		sstRunInstance.setZeitstempelEnde(now);
		sstRunInstance.setStatus(SST_LAUF_STATUS_FINISHED);
		sstRunInstance.setAnzahlDatensaetze(anzahlDatensaetze);
		em.merge(sstRunInstance);
		em.flush();
		SSTLaufDto dto = BaseDtoAssambler.mapEntity2Dto(sstRunInstance, SSTLaufDto.class);
		dto.setSST(sstRun.getSST());
		return dto;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SSTLaufDto handleSSTLaufFehlermeldung(SSTLaufDto sstRun, String fehlermeldung, Integer anzahlDatensaetze) {
		Timestamp now = new Timestamp(System.currentTimeMillis());
		SSTLauf sstRunInstance = em.find(SSTLauf.class, sstRun.getIId());
		sstRunInstance.setZeitstempelEnde(now);
		sstRunInstance.setStatus(SST_LAUF_STATUS_ERROR);
		sstRunInstance.setFehlermeldung(fehlermeldung);
		sstRunInstance.setAnzahlDatensaetze(anzahlDatensaetze);

		em.merge(sstRunInstance);
		em.flush();

		SSTLaufDto sstRunDto = BaseDtoAssambler.mapEntity2Dto(sstRunInstance, SSTLaufDto.class);
		sstRunDto.setSST(sstRun.getSST());
		return sstRunDto;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SSTLaufDto handleSSTLaufException(SSTLaufDto sstRun, Exception e, Integer anzahlDatensaetze) {
		final StringWriter sw = new StringWriter();
		final PrintWriter pw = new PrintWriter(sw, true);
		e.printStackTrace(pw);

		return handleSSTLaufFehlermeldung(sstRun, e.getMessage() + "\n\n" + sw, anzahlDatensaetze);
	}

	private void validateSSTParameterBeforeRun(SSTParameterDto sst, Integer sstID, String sstTyp, TheClientDto client) throws EJBExceptionLP {
		if (sst == null && sstID != null) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER_BEI_FINDBYPRIMARYKEY, "SST-Parameter with ID " + sstID + " not found!");
		} else if (sst == null) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER_DTO_IS_NULL, "SSTParameterDto is null!");
		} else if (sstTyp != null && !sstTyp.equalsIgnoreCase(sst.getTyp())) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER_INVALID_TYPE, "Invalid Type! Type is " + sst.getTyp() + ", expected Type " + sstTyp, sstTyp);
		} else if (SST_TYP_EXPORT.equals(sst.getTyp())) {
            checkPermissions(EXPORT_SST_BEREICHE.getOrDefault(sst.getBereich(), RECHT_LP_SYSTEM_R), client);
		} else if (SST_TYP_IMPORT.equals(sst.getTyp())) {
			checkPermissions(IMPORT_SST_BEREICHE.getOrDefault(sst.getBereich(), RECHT_LP_SYSTEM_CUD), client);
		}
	}

	/**
	 * Prefetches Detail-Data to improve Performance when export more than 3 Data Rows.
	 */
	private void prefetchExportDetailData(List<BaseExportDataUtil> datenUtils, Collection<Integer> referenceInstanceIDs, List<DatenStrukturFeld> strukturFelder) {
		if (datenUtils.size() > 3) {
			Set<String> strukturFields = new HashSet<>();
			for (DatenStrukturFeld sf : strukturFelder) {
				if (sf.getERPFeld() != null && !sf.getERPFeld().isEmpty()) {
					strukturFields.add(sf.getERPFeld());
				}
			}
			BaseExportDataUtil.initPrefetchData(datenUtils, strukturFields, referenceInstanceIDs);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SSTLaufDto doSSTExport(SSTParameterDto sst, Collection<Integer> referenceInstanceIDs, TheClientDto client) throws EJBExceptionLP {
		validateSSTParameterBeforeRun(sst, null, SST_TYP_EXPORT, client);

		Timestamp startTimestamp = new Timestamp(System.currentTimeMillis());
		Integer countData = null;
		SSTLaufDto sstRun = ctx.getBusinessObject(GenericInterfacesFac.class).createSSTLaufStart(startTimestamp, sst);

		try {
			Map<String, List<BaseExportDataUtil>>  utilsPerFileName = new HashMap<>();

			List<BaseExportDataUtil> datenUtils = getDatenUtilsForStrukturReferences(
					sst.getDatenStruktur(), referenceInstanceIDs, startTimestamp, client
			);
			countData = datenUtils.size();

			TypedQuery<DatenStrukturFeld> sFelderQuery = em.createNamedQuery("DatenStrukturFelderFindByStrukturId", DatenStrukturFeld.class);
			sFelderQuery.setParameter("strukturDatenId", sst.getDatenStruktur().getIId());
			List<DatenStrukturFeld> strukturFelder = sFelderQuery.getResultList();
			prefetchExportDetailData(datenUtils, referenceInstanceIDs, strukturFelder);

			for (BaseExportDataUtil datenUtil : datenUtils) {
				String targetName = parseQuellName(sst.getQuellname(), datenUtil);
				if (utilsPerFileName.containsKey(targetName)) {
					utilsPerFileName.get(targetName).add(datenUtil);
				} else {
					List<BaseExportDataUtil> list = new ArrayList<>();
					list.add(datenUtil);
					utilsPerFileName.put(targetName, list);
				}
			}

			AbstractBaseSerializer formatSerializer = getFormatSerializer(sst.getFormat(), sst.getStrukturDatenId());
			for (Map.Entry<String,List<BaseExportDataUtil>> entry : utilsPerFileName.entrySet()) {
				if (SST_TRANSFER_TYP_FILE.equalsIgnoreCase(sst.getTransferTyp())) {
					String fileName = entry.getKey();
					String writeFileName = fileName;
					// Create a Temp file for the export, and rename it later to the real target name
					boolean createTmpFile = true;
					if (createTmpFile) {  // Created boolean, for the case it should be someday configureable...
						writeFileName = fileName + ".tmp";
						String baseTempFile = writeFileName;
						int fileCounter = 0;
						while (new File(writeFileName).exists()) {
							// Ensure that the temp File-Name doesn't exist.
							fileCounter++;
							writeFileName = baseTempFile + fileCounter;
						}
					}

					formatSerializer.exportToFile(writeFileName, entry.getValue(), sst.getDatenStruktur(), strukturFelder);
					if (!writeFileName.equals(fileName)) {
						Path targetPath = Paths.get(fileName);
						if (new File(fileName).exists()) {
							// Remove the existing file, before rename the temp file.
							Files.delete(targetPath);
						}
						// Move the temp-file to the target file-name.
						Files.move(Paths.get(writeFileName), targetPath);
					}

				} else if (SST_TRANSFER_TYP_REST_CALL.equalsIgnoreCase(sst.getTransferTyp())) {
					formatSerializer.exportToUrl(entry.getKey(), entry.getValue(), sst.getDatenStruktur(), strukturFelder);
				} else {
					throw new IllegalArgumentException("Nicht unterstützter Schnittstellen Quell-Typ: " + sst.getTransferTyp());
				}
			}

			return setSSTLaufEnde(sstRun, countData);
		} catch (Exception e) {
			return ctx.getBusinessObject(GenericInterfacesFac.class).handleSSTLaufException(sstRun, e, countData);
		}
	}

	private Collection<Integer> parseQueryIDCollection(List<String> ids) {
		List<Integer> idList = new ArrayList<>();
		if (ids.size() == 1) {
			String idString = ids.get(0);
			if (idString.contains(",")) {
				String[] idArray = idString.split(",");
				for (String id : idArray) {
					idList.add(Integer.parseInt(id.trim()));
				}
			} else {
				idList.add(Integer.parseInt(idString.trim()));
			}
		} else {
			for (String id : ids) {
				idList.add(Integer.parseInt(id.trim()));
			}
		}
		return idList;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SSTExportResult doSSTExportForRESTCallToBytes(Integer sstID, String format, Map<String, List<String>> queryParams, TheClientDto client) throws EJBExceptionLP {
		SSTParameterDto sst = findActiveSSTParameter(sstID);
		validateSSTParameterBeforeRun(sst, sstID, SST_TYP_EXPORT, client);

		SSTExportResult result = new SSTExportResult();
		result.setIId(sst.getIId());

		if (format == null || format.isEmpty()) {
			format = sst.getFormat();
		}

		result.setFormat(format);

		Timestamp startTimestamp = new Timestamp(System.currentTimeMillis());
		Integer countData = null;
		SSTLaufDto sstRun = ctx.getBusinessObject(GenericInterfacesFac.class).createSSTLaufStart(startTimestamp, sst);

		try {
			Collection<Integer> referenceIDs;
			if (queryParams != null && queryParams.containsKey("id")) {
				referenceIDs = parseQueryIDCollection(queryParams.get("id"));
			} else {
				referenceIDs = getSSTExportReferenceIDs(sst, client);
			}

			List<BaseExportDataUtil> datenUtils = getDatenUtilsForStrukturReferences(
					sst.getDatenStruktur(), referenceIDs, startTimestamp, client
			);
			countData = datenUtils.size();

			TypedQuery<DatenStrukturFeld> sFelderQuery = em.createNamedQuery("DatenStrukturFelderFindByStrukturId", DatenStrukturFeld.class);
			sFelderQuery.setParameter("strukturDatenId", sst.getStrukturDatenId());
			List<DatenStrukturFeld> strukturFelder = sFelderQuery.getResultList();
			prefetchExportDetailData(datenUtils, referenceIDs, strukturFelder);

			AbstractBaseSerializer formatSerializer = getFormatSerializer(format, sst.getStrukturDatenId());
			result.setData(formatSerializer.exportToBytes(datenUtils, sst.getDatenStruktur(), strukturFelder));
			result.setSstRun(setSSTLaufEnde(sstRun, countData));
			return result;
		} catch (Exception e) {
			result.setSstRun(
				ctx.getBusinessObject(GenericInterfacesFac.class).handleSSTLaufException(sstRun, e, countData)
			);
			return result;
		}
	}

	private Collection<String> getFileNamesFromSSTQuellname(String quellname) {
		if (quellname != null) {
			quellname = quellname.trim();
			File f = new File(quellname);
			if (f.exists() && !f.isFile()) {
				// Is a Valid file so use it.
				return Collections.singletonList(quellname);
			} else if (quellname.contains("*") && quellname.contains(File.separator)) {
				// Check if the File has a Asterisk as Placeholder and look for all valid files now.
				// TODO DSK Maybe expand for Other Parameters in the File-Name as well.
				List<String> fileNames = new ArrayList<>();
				int indexPath = quellname.lastIndexOf(File.separator);
				File path = new File(quellname.substring(0, indexPath));
				if (path.exists() && path.isDirectory()) {
					String searchString = quellname.substring(indexPath + 1);
					String[] searchTokens = searchString.split("\\*");
					String firstToken = searchTokens[0];
					String lastToken = searchTokens[searchTokens.length - 1];
					File[] files = path.listFiles();
					if (files != null) {
						for (File file : files) {
							if (file.isFile()) {
								String fn = file.getName();
								boolean isValid = (fn.startsWith(firstToken) || searchString.startsWith("*")) &&
										(fn.endsWith(lastToken) || searchString.endsWith("*"));
								if (isValid) {
									int lastTknIndex = -1;
									for (String token : searchTokens) {
										int tknIndex = fn.indexOf(token);
										if (tknIndex <= lastTknIndex) {
											isValid = false;
											break;
										}
										lastTknIndex = tknIndex;
									}
								}
								if (isValid) {
									fileNames.add(file.getAbsolutePath());
								}
							}
						}
					}
					return fileNames;
				}
			}
		}
		return Collections.emptyList();
	}

	private AbstractBaseImportDataUtil getImportUtil(SSTParameterDto sst, Timestamp startTimestamp, TheClientDto client) {
		if (SST_BEREICH_FERT_TRUMPF_PROD_ORDER.equals(sst.getBereich())) {
			return new FertigungImportUtil(em, client, sst.getBereich(), startTimestamp, sst.getDatenStruktur().getDefaultDatumFormat(), getFertigungFac());
		} else if (SST_BEREICH_WW_ARTIKEL_TRU_TOPS.equals(sst.getBereich()) ||
				   SST_BEREICH_WW_ARTIKEL_TRU_TOPS_LOG.equals(sst.getBereich())) {
			return new ArtikelImportUtil(em, client, sst.getBereich(), startTimestamp, sst.getDatenStruktur().getDefaultDatumFormat(), getArtikelFac());
		} else {
			throw new IllegalArgumentException("Nicht unterstützter Schnittstellen Bereich: " + sst.getBereich());
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@TransactionTimeout(3600)
	public SSTLaufDto doSSTImport(SSTParameterDto sst, TheClientDto client) throws EJBExceptionLP {
		validateSSTParameterBeforeRun(sst, null, SST_TYP_IMPORT, client);

		Timestamp startTimestamp = new Timestamp(System.currentTimeMillis());
		Integer countData = null;
		SSTLaufDto sstRun = ctx.getBusinessObject(GenericInterfacesFac.class).createSSTLaufStart(startTimestamp, sst);
		Map<String, Exception> errorsPerFile = new HashMap<>();
		Set<String> successFiles = new HashSet<>();

		try {
			AbstractBaseSerializer formatSerializer = getFormatSerializer(sst.getFormat(), sst.getStrukturDatenId());
			if (SST_TRANSFER_TYP_FILE.equals(sst.getTransferTyp())) {
				Collection<String> fileNames = getFileNamesFromSSTQuellname(sst.getQuellname());
				if (!fileNames.isEmpty()) {
					TypedQuery<DatenStrukturFeld> sFelderQuery = em.createNamedQuery("DatenStrukturFelderFindByStrukturId", DatenStrukturFeld.class);
					sFelderQuery.setParameter("strukturDatenId", sst.getDatenStruktur().getIId());
					List<DatenStrukturFeld> strukturFelder = sFelderQuery.getResultList();
					countData = 0;

					for (String fileName : fileNames) {
						Path filePath = Paths.get(fileName);
						File file = filePath.toFile();
						formatSerializer.putGlobalFieldValue(GLOBAL_FELD_IMPORT_ZEITSTEMPEL, startTimestamp);
						formatSerializer.putGlobalFieldValue(GLOBAL_FELD_IMPORT_FILE_NAME, file.getName());
						List<Map<String, Object>> importData = formatSerializer.buildImportDataFromFile(fileName, sst.getDatenStruktur(), strukturFelder);
						countData += importData.size();
						AbstractBaseImportDataUtil importUtil = getImportUtil(sst, startTimestamp, client);

						try {
							importUtil.importData(importData);

							if (SST_NACHVERARBEITUNG_MODUS_DATEI_LOESCHEN.equals(sst.getNachverarbeitungModus())) {
								Files.delete(filePath);
							} else if (SST_NACHVERARBEITUNG_MODUS_DATEI_UMBENENNEN.equals(sst.getNachverarbeitungModus())) {
								String renamedFileName = fileName + ".bak";
								String baseRenamedFile = renamedFileName;
								int fileCounter = 0;
								while (new File(renamedFileName).exists()) {
									// Ensure that the new File-Name doesn't exists.
									fileCounter++;
									renamedFileName = baseRenamedFile + fileCounter;
								}
								Files.move(filePath, Paths.get(renamedFileName));
							}
							successFiles.add(fileName);
						} catch (Exception e) {
							errorsPerFile.put(fileName, e);
							if (SST_NACHVERARBEITUNG_MODUS_DATEI_UMBENENNEN.equals(sst.getNachverarbeitungModus())) {
								String renamedFileName = fileName + ".error";
								Files.move(filePath, Paths.get(renamedFileName));
							}
						}
					}
				}
			} else if (SST_TRANSFER_TYP_REST_CALL.equals(sst.getTransferTyp())) {
				// TODO DSk Test
				TypedQuery<DatenStrukturFeld> sFelderQuery = em.createNamedQuery("DatenStrukturFelderFindByStrukturId", DatenStrukturFeld.class);
				sFelderQuery.setParameter("strukturDatenId", sst.getDatenStruktur().getIId());
				List<DatenStrukturFeld> strukturFelder = sFelderQuery.getResultList();
				AbstractBaseImportDataUtil importUtil = getImportUtil(sst, startTimestamp, client);
				try {
					List<Map<String, Object>> importData = formatSerializer.buildImportDataFromUrl(sst.getQuellname(), sst.getDatenStruktur(), strukturFelder);
					countData = importData.size();
					importUtil.importData(importData);
				} catch (Exception e) {
					errorsPerFile.put(sst.getQuellname(), e);
				}

			} else {
				throw new RuntimeException("Invalid SST-Type: " + sst.getTransferTyp() + "!");
			}

			if (!errorsPerFile.isEmpty()) {
				// Log Error for each File.
				StringBuilder errorMessage = new StringBuilder();
				if (!successFiles.isEmpty()) {
					errorMessage.append("Successfully imported Files: ");
					boolean first = true;
					for (String file: successFiles) {
						if (!first) {
							errorMessage.append(", ");
						}
						errorMessage.append(file);
						first = false;
					}
				}

				for (String file: errorsPerFile.keySet()) {
					if (errorMessage.length() > 0) {
						errorMessage.append("\n\n");
					}
					Exception e = errorsPerFile.get(file);
					errorMessage.append("Error in File: \"").append(file).append("\" Error: ").append(e.getMessage());
					final StringWriter sw = new StringWriter();
					final PrintWriter pw = new PrintWriter(sw, true);
					e.printStackTrace(pw);
					errorMessage.append("\n").append(sw);
				}

				sstRun = ctx.getBusinessObject(GenericInterfacesFac.class).handleSSTLaufFehlermeldung(sstRun, errorMessage.toString(), countData);
			} else {
				sstRun = setSSTLaufEnde(sstRun, countData);
			}

			return sstRun;
		} catch (Exception e) {
			return ctx.getBusinessObject(GenericInterfacesFac.class).handleSSTLaufException(sstRun, e, countData);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SSTLaufDto doSSTImportForRESTCallFromString(Integer sstID, String format, Map<String, List<String>> queryParams, String payloadData, TheClientDto client) throws EJBExceptionLP {
		SSTParameterDto sst = findActiveSSTParameter(sstID);
		validateSSTParameterBeforeRun(sst, sstID, SST_TYP_IMPORT, client);

		Timestamp startTimestamp = new Timestamp(System.currentTimeMillis());
		Integer countData = null;
		SSTLaufDto sstRun = ctx.getBusinessObject(GenericInterfacesFac.class).createSSTLaufStart(startTimestamp, sst);

		try {
			if (format == null || format.isEmpty()) {
				format = sst.getFormat();
			}

			AbstractBaseSerializer formatSerializer = getFormatSerializer(format, sst.getStrukturDatenId());
			TypedQuery<DatenStrukturFeld> sFelderQuery = em.createNamedQuery("DatenStrukturFelderFindByStrukturId", DatenStrukturFeld.class);
			sFelderQuery.setParameter("strukturDatenId", sst.getDatenStruktur().getIId());
			List<DatenStrukturFeld> strukturFelder = sFelderQuery.getResultList();

			List<Map<String, Object>> importData = formatSerializer.buildImportDataFromString(payloadData, sst.getDatenStruktur(), strukturFelder);
			countData = importData.size();
			AbstractBaseImportDataUtil importUtil = getImportUtil(sst, startTimestamp, client);
			importUtil.importData(importData);

			return setSSTLaufEnde(sstRun, countData);
		} catch (Exception e) {
			return ctx.getBusinessObject(GenericInterfacesFac.class).handleSSTLaufException(sstRun, e, countData);
		}
	}

}
