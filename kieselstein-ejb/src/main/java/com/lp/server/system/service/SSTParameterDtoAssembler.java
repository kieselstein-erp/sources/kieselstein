package com.lp.server.system.service;

import com.lp.server.system.ejb.DatenStruktur;
import com.lp.server.system.ejb.SSTParameter;

public class SSTParameterDtoAssembler {
	private static SSTParameterDto createDto(SSTParameter bean) {
		SSTParameterDto dto = new SSTParameterDto();
		if (bean != null) {
			dto.setIId(bean.getId());
			dto.setTransferTyp(bean.getTransferTyp());
			dto.setStrukturDatenId(bean.getStrukturDatenId());
			dto.setAusloser(bean.getAusloeser());
			dto.setQuellname(bean.getQuellname());
			dto.setTyp(bean.getTyp());
			dto.setDatenModus(bean.getDatenModus());
			dto.setFormat(bean.getFormat());
			dto.setBereich(bean.getBereich());
			dto.setSort(bean.getSort());
			dto.setBezeichnung(bean.getBezeichnung());
			dto.setNachverarbeitungModus(bean.getNachverarbeitungModus());
			dto.setAktiv(bean.getAktiv());
		}
		return dto;
	}

	public static SSTParameterDto createDto(SSTParameter sst, DatenStruktur struktur) {
		SSTParameterDto dto = createDto(sst);
		if (struktur != null) {
			dto.setStrukturDatenId(struktur.getId());
			dto.setDatenStruktur(BaseDtoAssambler.mapEntity2Dto(struktur, DatenStrukturDto.class));
		}
		return dto;
	}
}
