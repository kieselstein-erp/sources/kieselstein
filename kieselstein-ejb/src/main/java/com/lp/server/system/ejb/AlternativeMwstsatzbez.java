package com.lp.server.system.ejb;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries(
	{
	@NamedQuery(name = "AlternativeMwstbezFindbyMwstBezId", query = "SELECT OBJECT (o) FROM AlternativeMwstsatzbez o WHERE o.mwstsatzbez_id = :mwstsatzbez_id"),
	@NamedQuery(name = "AlternativeMwstbezFindbyAlternativeMwstBezIdExcludeOwnMWSTBez", query = "SELECT OBJECT (o) FROM AlternativeMwstsatzbez o " +
			"WHERE o.alternative_mwstsatzbez_id in (:alternative_mwstsatzbez_ids) " +
			"  AND o.mwstsatzbez_id <> :ownMWSTBezID"),
	@NamedQuery(name = "AlternativeMwstbezFindbyMwstByBezIdAndAlterBezID", query = "SELECT OBJECT (o) FROM AlternativeMwstsatzbez o " +
			"WHERE o.alternative_mwstsatzbez_id = :alternative_mwstsatzbez_id " +
			"  AND o.mwstsatzbez_id = :mwstsatzbez_id"),
	@NamedQuery(name = "AlternativeMwstbezFindNumberOfMwstbezbyAlternativeMwstBezId", query = "SELECT COUNT(o) FROM AlternativeMwstsatzbez o WHERE o.alternative_mwstsatzbez_id = :alternative_mwstsatzbez_id"),
	}
)
@Entity
@Table(name = "LP_ALTERNATIVE_MWSTSATZBEZ")
public class AlternativeMwstsatzbez implements Serializable {
	@Id
	@Column(name = "I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer iId;

	@Column(name = "MWSTSATZBEZ_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer mwstsatzbez_id;


	@Column(name = "ALTERNATIVE_MWSTSATZBEZ_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer alternative_mwstsatzbez_id;

	private static final long serialVersionUID = 1L;

	public AlternativeMwstsatzbez() {
		super();
	}

	public AlternativeMwstsatzbez(Integer id, Integer mwstsatzbez_id, Integer alternative_mwstsatzbez_id) {
		setIId(id);
		setMwstsatzbez_id(mwstsatzbez_id);
		setAlternative_mwstsatzbez_id(alternative_mwstsatzbez_id);
	}

	public Integer getIId() {
		return this.iId;
	}

	public void setIId(Integer iId) {
		this.iId = iId;
	}

	public Integer getMwstsatzbez_id() {
		return mwstsatzbez_id;
	}

	public void setMwstsatzbez_id(Integer mwstsatzbez_id) {
		this.mwstsatzbez_id = mwstsatzbez_id;
	}

	public Integer getAlternative_mwstsatzbez_id() {
		return alternative_mwstsatzbez_id;
	}

	public void setAlternative_mwstsatzbez_id(Integer alternative_mwstsatzbez_id) {
		this.alternative_mwstsatzbez_id = alternative_mwstsatzbez_id;
	}
}
