package com.lp.server.system.ejb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "LP_MAPPING")
public class Mapping implements Serializable {
	@Id
	@Column(name = "I_ID", columnDefinition = "INTEGER NOT NULL")
	private Integer iId;

	@Column(name = "C_BEZEICHNUNG", columnDefinition = "VARCHAR(64) NOT NULL")
	private String cBezeichnung;

	@Column(name = "PERSONAL_I_ID_ANLEGEN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAnlegen;

	@Column(name = "PERSONAL_I_ID_AENDERN", columnDefinition = "INTEGER NOT NULL")
	private Integer personalIIdAendern;

	@Column(name = "T_ANLEGEN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAnlegen ;

	@Column(name = "T_AENDERN", columnDefinition = "TIMESTAMP NOT NULL")
	private Timestamp tAendern;


	public Mapping() {
		super();
	}

	public Mapping(Integer id, String bezeichnung,
                   Integer personalIIdAnlegen) {
		setId(id);
		setBezeichnung(bezeichnung);
		setPersonalIIdAnlegen(personalIIdAnlegen);
		setPersonalIIdAendern(personalIIdAnlegen);
		// Setzen der NOT NULL Felder
		Timestamp now = new Timestamp(System.currentTimeMillis());
		setAendern(now);
		setAnlegen(now);
	}

	public Integer getId() {
		return this.iId;
	}

	public void setId(Integer iId) {
		this.iId = iId;
	}

	public String getBezeichnung() {
		return cBezeichnung;
	}

	public void setBezeichnung(String cBezeichnung) {
		this.cBezeichnung = cBezeichnung;
	}

	public Integer getPersonalIIdAnlegen() {
		return personalIIdAnlegen;
	}

	public void setPersonalIIdAnlegen(Integer personalIIdAnlegen) {
		this.personalIIdAnlegen = personalIIdAnlegen;
	}

	public Integer getPersonalIIdAendern() {
		return personalIIdAendern;
	}

	public void setPersonalIIdAendern(Integer personalIIdAendern) {
		this.personalIIdAendern = personalIIdAendern;
	}

	public Timestamp getAnlegen() {
		return tAnlegen;
	}

	public void setAnlegen(Timestamp tAnlegen) {
		this.tAnlegen = tAnlegen;
	}

	public Timestamp getAendern() {
		return tAendern;
	}

	public void setAendern(Timestamp tAendern) {
		this.tAendern = tAendern;
	}
}
