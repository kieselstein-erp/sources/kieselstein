package com.lp.server.system.service;

import com.lp.server.util.IIId;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Objects;

public abstract class AbstractBaseDto implements Serializable, IIId {
    protected Integer iId;

    public Integer getIId() {
        return iId;
    }

    public void setIId(Integer iId) {
        this.iId = iId;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof AbstractBaseDto)) {
            return false;
        }
        if (obj.getClass() != this.getClass()) {
            return false;
        }
        AbstractBaseDto that = (AbstractBaseDto) obj;
        try {
            for (Method method : getPropertyGetters()){
                Object value1 = method.invoke(this);
                Object value2 = method.invoke(that);
                if (!Objects.equals(value1, value2)) {
                    return false;
                }
            }
        } catch (InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    private Method[] getPropertyGetters() {
        ArrayList<Method> propertyDescriptors = new ArrayList<>();
        try {
            for (PropertyDescriptor propertyDescriptor : Introspector.getBeanInfo(this.getClass()).getPropertyDescriptors()){
                Method method = propertyDescriptor.getReadMethod();
                if (method != null && method.getParameterCount() == 0 && !"getClass".equals(method.getName())) {
                    propertyDescriptors.add(method);
                }
            }
            return propertyDescriptors.toArray(new Method[0]);
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }

    public int hashCode() {
        int result = 17;
        try {
            for (Method method : this.getPropertyGetters()){
                Object value = method.invoke(this);
                result = 37 * result + (value != null ? value.hashCode() : 0);
            }
        } catch (InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private String buildDataString(char delimiter) {
        try {
            String[] classNamePaths = this.getClass().getName().split("\\.");
            StringBuilder sb = new StringBuilder(classNamePaths[classNamePaths.length - 1]).append(":");
            for (Method method : this.getPropertyGetters()) {
                Object value = method.invoke(this);
                if (value instanceof AbstractBaseDto) {
                    value = ((AbstractBaseDto) value).buildDataString(',');
                }
                String name = (method.getName().startsWith("get") ? method.getName().substring(3) : method.getName());
                sb.append(" ").append(name).append("=").append(value).append(delimiter);
            }
            return sb.toString().trim();
        } catch (InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public String toString() {
        return buildDataString('\n');
    }
}
