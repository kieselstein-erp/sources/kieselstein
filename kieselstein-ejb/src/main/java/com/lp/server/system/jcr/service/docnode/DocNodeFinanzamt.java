package com.lp.server.system.jcr.service.docnode;


import com.lp.server.finanz.service.FinanzamtDto;

import javax.jcr.Node;

public class DocNodeFinanzamt extends DocNodeJCR {

    private static final long serialVersionUID = 5849123268740856594L;
    public DocNodeFinanzamt(Node node) {
        super(node);
    }

    public DocNodeFinanzamt(FinanzamtDto finanzamtDto) {
        super(BELEGART_FINANZBUCHHALTG, finanzamtDto.getPartnerIId(), finanzamtDto.getPartnerDto().getCName1nachnamefirmazeile1(),
                ARTCNR_FINANZAMT, finanzamtDto.getMandantCNr());
    }
}
