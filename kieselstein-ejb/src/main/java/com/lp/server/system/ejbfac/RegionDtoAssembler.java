package com.lp.server.system.ejbfac;

import com.lp.server.system.ejb.Region;
import com.lp.server.system.service.RegionDto;

public class RegionDtoAssembler {
    public static RegionDto createDto(Region region){
        RegionDto regionDto = new RegionDto();
        if(region != null){
            regionDto.setIID(region.getiId());
            regionDto.setCNr(region.getcNr());
            regionDto.setBdUmsatzziel(region.getBdUmsatzziel());
        }
        return regionDto;
    }
}
