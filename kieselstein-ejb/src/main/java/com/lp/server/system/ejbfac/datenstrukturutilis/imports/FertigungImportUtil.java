package com.lp.server.system.ejbfac.datenstrukturutilis.imports;

import com.lp.layer.hibernate.HvTypedQuery;
import com.lp.server.fertigung.ejb.Lossollmaterial;
import com.lp.server.fertigung.service.FertigungFac;
import com.lp.server.fertigung.service.LossollmaterialDto;
import com.lp.server.fertigung.service.LossollmaterialDtoAssembler;
import com.lp.server.system.service.GenericInterfacesFac;
import com.lp.server.system.service.TheClientDto;
import com.lp.server.util.LossollmaterialId;
import com.lp.util.EJBExceptionLP;

import javax.persistence.EntityManager;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.*;

public class FertigungImportUtil extends AbstractBaseImportDataUtil {
    private final FertigungFac fertigungFac;

    public FertigungImportUtil(EntityManager em, TheClientDto client, String sstSection, Date timeStamp, String defaultDateTimeFormat, FertigungFac fertigungFac) {
        super(em, client, sstSection, timeStamp, defaultDateTimeFormat);
        this.fertigungFac = fertigungFac;
    }

    private String buildLosSollMaterialKeyString(Map<String, Object> data) {
        final String[] keyFields = {
                GenericInterfacesFac.FELD_LOSSOLLMATERIAL_ID,
                GenericInterfacesFac.FELD_LOS_ID,
                GenericInterfacesFac.FELD_LOS_CNR,
                GenericInterfacesFac.FELD_LOSSOLLMATERIAL_TRUTOPS_ORDERNO,
                GenericInterfacesFac.FELD_ARTIKEL_ID,
                GenericInterfacesFac.FELD_LOSSOLLMATERIAL_SORT
        };

        return buildDataKeyString(data, keyFields);
    }

    private LossollmaterialDto getFindLosSollMaterialDto(Map<String, Object> data) throws RemoteException {
        HvTypedQuery<Lossollmaterial> query = new HvTypedQuery<>(em.createNamedQuery("LosSollMaterialFindForImport"));
        Integer iId = getIntegerFromDataMap(data, GenericInterfacesFac.FELD_LOSSOLLMATERIAL_ID);
        if (iId != null) {
            return fertigungFac.lossollmaterialFindByPrimaryKey(iId);
        }

        Integer losIId = getIntegerFromDataMap(data, GenericInterfacesFac.FELD_LOS_ID);
        String loscNr = getStringFromDataMap(data, GenericInterfacesFac.FELD_LOS_CNR);
        String truTopsLosOrderNr = getStringFromDataMap(data, GenericInterfacesFac.FELD_LOSSOLLMATERIAL_TRUTOPS_ORDERNO);
        String trutosLoscNr = null;
        Integer artikelIId = getIntegerFromDataMap(data, GenericInterfacesFac.FELD_ARTIKEL_ID);
        Integer iSort = getIntegerFromDataMap(data, GenericInterfacesFac.FELD_LOSSOLLMATERIAL_SORT);

        if (truTopsLosOrderNr != null && truTopsLosOrderNr.contains("_")) {
            int posSep = truTopsLosOrderNr.lastIndexOf("_");
            trutosLoscNr = truTopsLosOrderNr.substring(0, posSep);
            if (iSort == null && truTopsLosOrderNr.length() + 1 > posSep) {
                iSort = Integer.parseInt(truTopsLosOrderNr.substring(posSep + 1).trim());
            }
        }

        query.setParameter("losIId", losIId);
        query.setParameter("loscNr", loscNr);
        query.setParameter("trutosLoscNr", trutosLoscNr);
        query.setParameter("artikelIId", artikelIId);
        query.setParameter("iSort", iSort);
        query.setMaxResults(2);

        List<Lossollmaterial> result = query.getResultList();
        if (result.isEmpty()) {
            String keyString = buildLosSollMaterialKeyString(data);
            String errorMessage = "Es konnte kein LosSollMaterial ermittelt werten (" + keyString + ")!";
            throw new EJBExceptionLP(EJBExceptionLP.FEHLER_NO_UNIQUE_RESULT, errorMessage);
        } else if (result.size() > 1) {
            String keyString = buildLosSollMaterialKeyString(data);
            String errorMessage = "Es konnte das LosSollMaterial nicht eindeutig ermittelt werden (" + keyString + ")!";
            throw new EJBExceptionLP(EJBExceptionLP.FEHLER_NO_UNIQUE_RESULT, errorMessage);
        }
        return LossollmaterialDtoAssembler.createDto(result.get(0));
    }

    private void handleUpdateLosSollMaterialExportEnde(Map<String, Object> data) throws ParseException, RemoteException {
        LossollmaterialDto dto = getFindLosSollMaterialDto(data);
        fertigungFac.updateLossollmaterialExportEnde(
                new LossollmaterialId(dto.getIId()),
                getTimeStampFromDataMap(data, GenericInterfacesFac.FELD_LOSSOLLMATERIAL_TRUTOPS_EXPORT_ENDE, this.defaultDateTimeFormat),
                getStringFromDataMap(data, GenericInterfacesFac.FELD_LOSSOLLMATERIAL_TRUTOPS_FEHLER_CODE),
                getStringFromDataMap(data, GenericInterfacesFac.FELD_LOSSOLLMATERIAL_TRUTOPS_FEHLER_TEXT),
                client
        );
    }

    public void importData(Collection<Map<String, Object>> data) throws ParseException, RemoteException {
        if (data != null && !data.isEmpty()) {
            for (Map<String, Object> row : data) {
                if (GenericInterfacesFac.SST_BEREICH_FERT_TRUMPF_PROD_ORDER.equals(sstSection)) {
                    handleUpdateLosSollMaterialExportEnde(row);
                } else {
                    throw new IllegalArgumentException("Nicht unterstützter Schnittstellen Quell-Typ: " + sstSection);
                }
            }
        }
    }
}