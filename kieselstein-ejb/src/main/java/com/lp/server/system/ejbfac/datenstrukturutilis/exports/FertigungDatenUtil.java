package com.lp.server.system.ejbfac.datenstrukturutilis.exports;

import com.lp.server.artikel.ejb.Artikel;
import com.lp.server.auftrag.ejb.Auftrag;
import com.lp.server.fertigung.ejb.Los;
import com.lp.server.fertigung.ejb.Lossollmaterial;
import com.lp.server.fertigung.service.FertigungFac;
import com.lp.server.fertigung.service.LossollarbeitsplanDto;
import com.lp.server.finanz.ejb.Konto;
import com.lp.server.partner.ejb.Kunde;
import com.lp.server.partner.ejb.Partner;
import com.lp.server.system.service.GenericInterfacesFac;
import com.lp.server.system.service.TheClientDto;
import com.lp.server.util.LosId;
import com.lp.util.Helper;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.rmi.RemoteException;
import java.util.*;

public class FertigungDatenUtil extends BaseExportDataUtil {

    private final FertigungFac fertigungFac;

    private final Los los;
    private final Lossollmaterial lossollmaterial;
    private ArtikelExportDatenUtil artikelDatenUtil;
    private Kunde losKunde = null;

    public FertigungDatenUtil(EntityManager em, TheClientDto client, Date timeStamp, FertigungFac fertigungFac, String dataSection, Los los, Lossollmaterial lossollmaterial, Artikel losSollMaterialArtikel) {
        super(em, client, timeStamp);
        this.fertigungFac = fertigungFac;
        this.dataSection = dataSection;
        this.los = los;
        this.lossollmaterial = lossollmaterial;
        if (losSollMaterialArtikel != null) {
            artikelDatenUtil = new ArtikelExportDatenUtil(
                    em, client, timeStamp, GenericInterfacesFac.BEREICH_WW_ARTIKEL, losSollMaterialArtikel
            );
        }
    }

    private Kunde getLosKundeIntern() {
        if (losKunde == null) {
            if (los.getKundeIId() != null) {
                losKunde = em.find(Kunde.class, los.getKundeIId());
            } else if (los.getAuftragIId() != null) {
                Auftrag a = em.find(Auftrag.class, los.getAuftragIId());
                if (a != null) {
                    if (a.getKundeIIdRechnungsadresse() != null) {
                        losKunde = em.find(Kunde.class, a.getKundeIIdRechnungsadresse());
                    } else if (a.getKundeIIdAuftragsadresse() != null) {
                        losKunde = em.find(Kunde.class, a.getKundeIIdAuftragsadresse());
                    } else if (a.getKundeIIdLieferadresse() != null) {
                        losKunde = em.find(Kunde.class, a.getKundeIIdLieferadresse());
                    }
                }
            }

            if (losKunde == null) {
                losKunde = new Kunde();  // Create a Empty Instance so it wont load it from the database more then necessary.
            }
        }
        return losKunde;
    }

    public Object getKundeElementOutputValue(Kunde kunde, String erpFeld) {
        if (kunde != null && kunde.getIId() != null) {
            if (GenericInterfacesFac.FELD_KUNDEN_NAME.equals(erpFeld)) {
                if (kunde.getPartnerIId() != null) {
                    Partner p = em.find(Partner.class, kunde.getPartnerIId());
                    return p.getCName1nachnamefirmazeile1();
                }
                return null;
            } else if (GenericInterfacesFac.FELD_KUNDEN_DEBITORENKONTO_NR.equals(erpFeld)) {
                if (kunde.getKontoIIdDebitorenkonto() != null) {
                    Konto konto = em.find(Konto.class, kunde.getKontoIIdDebitorenkonto());
                    if (konto != null && konto.getCNr() != null) {
                        return Integer.parseInt(konto.getCNr());
                    }
                }
                return null;
            } else {
                throw new IllegalArgumentException("Unsupported ERP-Feld for Kunde: " + erpFeld);
            }
        } else {
            return null;
        }
    }

    public Object getLosElementOutputValue(String erpFeld) {
        if (los != null) {
            if (GenericInterfacesFac.BEREICH_KUNDE_FELDER.contains(erpFeld)) {
                return getKundeElementOutputValue(getLosKundeIntern(), erpFeld);
            } else if (GenericInterfacesFac.FELD_LOS_ID.equals(erpFeld)) {
                return los.getIId();
            } else if (GenericInterfacesFac.FELD_LOS_CNR.equals(erpFeld)) {
                return los.getCNr();
            } else if (GenericInterfacesFac.FELD_LOS_TRUMPF_FAELLIG_DATUM.equals(erpFeld)) {
                // Special Logic for the calculation get the Start-Date of the first-AG after the first-Tru-Tops-AG.
                List<LossollarbeitsplanDto> truTopsArbeitsplaene = fertigungFac.getTruTopsLossollarbeitsplan(new LosId(los.getIId()));
                if (!truTopsArbeitsplaene.isEmpty()) {
                    LossollarbeitsplanDto truTopsAG = truTopsArbeitsplaene.get(0);
                    LossollarbeitsplanDto[] arbeitsplanDtos;
                    try {
                        arbeitsplanDtos = fertigungFac.lossollarbeitsplanFindByLosIId(los.getIId());
                    } catch (RemoteException e) {
                        throw new RuntimeException(e);
                    }
                    for (LossollarbeitsplanDto dto : arbeitsplanDtos) {
                        if (dto.getLosIId().equals(truTopsAG.getLosIId()) &&
                                ((Objects.equals(dto.getIArbeitsgangnummer(), truTopsAG.getIArbeitsgangnummer()) && dto.getIUnterarbeitsgang() != null && truTopsAG.getIUnterarbeitsgang() != null && dto.getIUnterarbeitsgang() > truTopsAG.getIUnterarbeitsgang()) || (dto.getIArbeitsgangnummer() > truTopsAG.getIArbeitsgangnummer())) ) {
                            return Helper.addiereTageZuDatum(los.getTProduktionsbeginn(), dto.getIMaschinenversatztage());
                        }
                    }
                }
                return null;
            } else {
                return getGlobalOutputValues(erpFeld);
            }
        } else {
            return null;
        }
    }

    public Object getLosSollMaterialElementOutputValue(String erpFeld) {
        if (GenericInterfacesFac.BEREICH_ARTIKEL_FELDER.contains(erpFeld) && artikelDatenUtil != null) {
            return artikelDatenUtil.getElementOutputValue(erpFeld);
        } else if (GenericInterfacesFac.BEREICH_LOS.contains(erpFeld)) {
            return getLosElementOutputValue(erpFeld);
        } else if (GenericInterfacesFac.FELD_LOSSOLLMATERIAL_ID.equals(erpFeld)) {
            return lossollmaterial.getIId();
        } else if (GenericInterfacesFac.FELD_LOSSOLLMATERIAL_MENGE.equals(erpFeld)) {
            return lossollmaterial.getNMenge();
        } else if (GenericInterfacesFac.FELD_LOSSOLLMATERIAL_EINHEIT.equals(erpFeld)) {
            return lossollmaterial.getEinheitCNr().trim();
        } else if (GenericInterfacesFac.FELD_LOSSOLLMATERIAL_SORT.equals(erpFeld)) {
            return lossollmaterial.getISort();
        } else if (GenericInterfacesFac.FELD_LOSSOLLMATERIAL_TRUTOPS_ORDERNO.equals(erpFeld)) {
            String iSort = String.format("%04d", lossollmaterial.getISort());
            String normLosCnr = los.getCNr().replaceAll("[^0-9]", "_");
            return normLosCnr + "_" + iSort;
        } else if (GenericInterfacesFac.FELD_LOSSOLLMATERIAL_TRUTOPS_EXPORT_ENDE.equals(erpFeld)) {
            return lossollmaterial.getTExportEnde();
        } else if (GenericInterfacesFac.FELD_LOSSOLLMATERIAL_TRUTOPS_FEHLER_CODE.equals(erpFeld)) {
            return lossollmaterial.getCFehlercode();
        } else if (GenericInterfacesFac.FELD_LOSSOLLMATERIAL_TRUTOPS_FEHLER_TEXT.equals(erpFeld)) {
            return lossollmaterial.getCFehlertext();
        } else {
            return getGlobalOutputValues(erpFeld);
        }
    }

    @Override
    public void prefetchInstanceValues(Map<String, Map<Integer, Object>> prefetchedData, Collection<Integer> referenceKeys, Set<String> strukturFields) {
        if (GenericInterfacesFac.BEREICH_FERT_LOSSOLLMATERIAL.equals(dataSection)) {
            if (strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_BEZEICHNUNG) ||
                strukturFields.contains(GenericInterfacesFac.FELD_ARTIKEL_TRUTOPS_METADATEN_PFAD)
            ) {
                TypedQuery<Integer> query = em.createNamedQuery("LosSollMaterialArtikelIDs", Integer.class);
                query.setParameter("ids", referenceKeys);
                List<Integer> articleIDs = query.getResultList();
                if (!articleIDs.isEmpty()) {
                    if (artikelDatenUtil != null) {
                        artikelDatenUtil.prefetchInstanceValues(prefetchedData, articleIDs, strukturFields);
                    } else {
                        ArtikelExportDatenUtil tmpArtUtil = new ArtikelExportDatenUtil(em, client, timeStamp, GenericInterfacesFac.BEREICH_WW_ARTIKEL, null);
                        tmpArtUtil.prefetchInstanceValues(prefetchedData, articleIDs, strukturFields);
                    }
                }
            }
        }
    }

    @Override
    protected void setPrefetchedDataMap(Map<String, Map<Integer, Object>> prefetchedData) {
        super.setPrefetchedDataMap(prefetchedData);
        if (artikelDatenUtil != null) {
            artikelDatenUtil.setPrefetchedDataMap(prefetchedData);
        }
    }

    @Override
    public Integer getInstanceKey() {
        if (GenericInterfacesFac.BEREICH_FERT_LOSSOLLMATERIAL.equals(dataSection)) {
            return lossollmaterial.getIId();
        }
        return null;
    }

    @Override
    public Object getElementOutputValue(String erpFeld) {
        if (isValueCached(erpFeld)) {
            return getPrefetchedValue(erpFeld);
        } else if (GenericInterfacesFac.BEREICH_FERT_LOSSOLLMATERIAL.equals(dataSection)) {
            return getLosSollMaterialElementOutputValue(erpFeld);
        } else {
            throw new IllegalArgumentException("Unsupported Bereich: " + dataSection);
        }
    }

}
