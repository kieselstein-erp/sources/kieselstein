package com.lp.server.rechnung.bl;

import com.lp.server.util.XMLMarshaller;
import org.xml.sax.SAXException;
import un.unece.uncefact.data.standard.crossindustryinvoice._100.CrossIndustryInvoiceType;
import un.unece.uncefact.data.standard.crossindustryinvoice._100.ObjectFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

public class CrossIndustryInvoiceMarshaller extends XMLMarshaller<CrossIndustryInvoiceType> {

    public CrossIndustryInvoiceMarshaller() throws JAXBException {
        super();
    }

    @Override
    public String marshal(CrossIndustryInvoiceType invoice) throws JAXBException, SAXException {
        var context = JAXBContext.newInstance(CrossIndustryInvoiceType.class);
        var marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        var writer = new StringWriter();
        marshaller.marshal(new ObjectFactory().createCrossIndustryInvoice(invoice), writer);
        return writer.toString();
    }

    @Override
    protected Class<CrossIndustryInvoiceType> getClazz() {
        return CrossIndustryInvoiceType.class;
    }
}
