package com.lp.server.rechnung.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

public class SkontoDto implements Serializable {
    private final Date skontoDatum;
    private final BigDecimal skontierterBetrag;

    public SkontoDto(Date skontoDatum, BigDecimal skontierterBetrag) {
        this.skontoDatum = skontoDatum;
        this.skontierterBetrag = skontierterBetrag;
    }

    public Date getSkontoDatum() {
        return skontoDatum;
    }

    public BigDecimal getSkontiertenOffenenBetrag() {
        return skontierterBetrag;
    }
}
