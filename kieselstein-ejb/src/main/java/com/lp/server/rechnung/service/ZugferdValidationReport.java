package com.lp.server.rechnung.service;

import java.io.Serializable;

public class ZugferdValidationReport implements Serializable {
    private final String severity;
    private final String id;
    private final String xPathLocation;
    private final String errorText;

    public ZugferdValidationReport(String severity, String id, String xPathLocation, String errorText) {
        this.severity = severity;
        this.id = id;
        this.xPathLocation = xPathLocation;
        this.errorText = errorText;
    }

    public String getSeverity() {
        return severity;
    }

    public String getErrorText() {
        return errorText;
    }

    public String getId() {
        return id;
    }

    public String getxPathLocation() {
        return xPathLocation;
    }
}
