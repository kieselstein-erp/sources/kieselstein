package com.lp.server.rechnung.bl.zugferd;

public enum Untdid5305TaxCodes {
    STANDARD_RATE("S"),
    ZERO_RATE_GOODS("Z"),
    EXEMPT_FROM_TAX("E"),
    VAT_REVERSE_CHARGE("AE"),
    VAT_EXEMPT_INTRA_COMMUNITY("K"),
    FREE_EXPORT_ITEM("G"),
    SERVICES_OUTSIDE_SCOPE_OF_TAX("O");

    private final String taxCode;

    Untdid5305TaxCodes(String taxCode) {

        this.taxCode = taxCode;
    }

    public String getTaxCode() {
        return taxCode;
    }
}
