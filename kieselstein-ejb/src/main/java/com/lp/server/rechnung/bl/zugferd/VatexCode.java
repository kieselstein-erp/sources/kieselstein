package com.lp.server.rechnung.bl.zugferd;

public enum VatexCode {
    INTRA_COMMUNITY_SUPPLY("VATEX-EU-IC"),
    NOT_SUBJECT_TO_VAT("VATEX-EU-O"),
    EXPORT_OUTSIDE_OF_EU("VATEX-EU-G"),
    REVERSE_CHARGE("VATEX-EU-AE");

    private final String code;

    VatexCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
