package com.lp.server.rechnung.ejbfac;

import com.lp.server.rechnung.service.ZugferdValidationReport;
import com.lp.util.EJBExceptionLP;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XsltExecutable;
import org.apache.commons.lang.StringUtils;
import org.apache.tools.ant.filters.StringInputStream;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.ejb.Singleton;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Singleton
public class ZugferdValidatorBean {

    private final Processor processor;
    private final XsltExecutable executable;

    public ZugferdValidatorBean() throws URISyntaxException, SaxonApiException {
        this.processor = new Processor(false);
        var xsltCompiler = processor.newXsltCompiler();
        var xsltPath = "/com/lp/server/res/zugferd/2_3_2/FACTUR-X_EN16931.xslt";
        var xsltSource = new StreamSource(this.getClass().getResourceAsStream(xsltPath));
        xsltSource.setSystemId(Objects.requireNonNull(this.getClass().getResource(xsltPath)).toURI().toString());

        executable = xsltCompiler.compile(xsltSource);
    }

    public void validateZugferd(String xmlStr) {
        var validationReportList = new ArrayList<ZugferdValidationReport>();
        var transformer = executable.load();
        transformer.setSource(new StreamSource(new StringInputStream(xmlStr, "UTF-8")));
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            var serializer = processor.newSerializer(bos);
            transformer.setDestination(serializer);

            transformer.transform();
            var validationOutput = DocumentBuilderFactory
                    .newInstance()
                    .newDocumentBuilder()
                    .parse(new ByteArrayInputStream(bos.toByteArray()));

            XPath xPath = XPathFactory.newInstance().newXPath();
            String expression = "//*[local-name() = 'failed-assert']";

            var failedAsserts = (NodeList) xPath.compile(expression).evaluate(validationOutput, XPathConstants.NODESET);

            if (failedAsserts.getLength() == 0) {
                //no errors found
                return;
            }

            String failId = "";
            String failXpathLocation = "";

            for (int nodeIndex = 0; nodeIndex < failedAsserts.getLength(); nodeIndex++) {
                Node currentFailNode = failedAsserts.item(nodeIndex);
                if (currentFailNode.getAttributes().getNamedItem("id") != null) {
                    failId = currentFailNode.getAttributes().getNamedItem("id").getNodeValue();
                }

                if (currentFailNode.getAttributes().getNamedItem("location") != null) {
                    failXpathLocation = currentFailNode.getAttributes().getNamedItem("location").getNodeValue();
                }

                String severity;
                if (currentFailNode.getAttributes().getNamedItem("flag") != null
                        && currentFailNode.getAttributes().getNamedItem("flag").getNodeValue().equals("warning")) {
                    // the XR issues warnings with flag=warning
                    severity = "warning";
                } else {
                    severity = "error";
                }

                var failDescription = StringUtils.trimToNull(currentFailNode.getTextContent());
                validationReportList.add(new ZugferdValidationReport(severity, failId, failXpathLocation, failDescription));
            }
        } catch (IOException | SaxonApiException | XPathExpressionException | SAXException |
                 ParserConfigurationException e) {
            throw new EJBExceptionLP(EJBExceptionLP.FEHLER_ZUGFERD_VALIDIERUNG, List.of("Die Validierung der ZUGFeRD-Rechnung konnte nicht gestartet werden."), e);
        }

        if (!validationReportList.isEmpty()) {
            var fehlermeldung = "Die erzeugte ZUGFeRD-Rechnung ist nicht valide!";
            throw new EJBExceptionLP(EJBExceptionLP.FEHLER_ZUGFERD_VALIDIERUNG, fehlermeldung, validationReportList.stream().map(v -> v.getErrorText()).toArray());
        }
    }
}
