/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 * 
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 * 
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: developers@heliumv.com
 ******************************************************************************/
package com.lp.server.partner.fastlanereader;

import java.util.*;

import com.lp.util.Helper;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.lp.server.partner.fastlanereader.generated.FLRBank;
import com.lp.server.partner.service.BankFac;
import com.lp.server.util.fastlanereader.FLRSessionFactory;
import com.lp.server.util.fastlanereader.UseCaseHandler;
import com.lp.server.util.fastlanereader.service.query.FilterBlock;
import com.lp.server.util.fastlanereader.service.query.FilterKriterium;
import com.lp.server.util.fastlanereader.service.query.QueryParameters;
import com.lp.server.util.fastlanereader.service.query.QueryResult;
import com.lp.server.util.fastlanereader.service.query.SortierKriterium;
import com.lp.server.util.fastlanereader.service.query.TableInfo;
import com.lp.util.EJBExceptionLP;

/**
 * <p>
 * Diese Klasse kuemmert sich den FLR-Banken.
 * </p>
 * 
 * <p>
 * Copyright Logistik Pur Software GmbH (c) 2004-2007
 * </p>
 * 
 * <p>
 * Erstellungsdatum 03.11.04
 * </p>
 * 
 * <p>
 * 
 * @author $Author: christian $
 *         </p>
 * 
 * @version $Revision: 1.10 $ Date $Date: 2012/08/08 13:45:19 $
 */

public class BankHandler extends UseCaseHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String FLR_SORT_STRING = "select bank.i_id from FLRBank bank ";

	private static final String FLR_FROM_STRING = "from FLRBank bank ";

	/**
	 * gets the data page for the specified row using the current query. The row
	 * at rowIndex will be located in the middle of the page.
	 * 
	 * @see UseCaseHandler#getPageAt(java.lang.Integer)
	 * @param rowIndex
	 *            Integer
	 * @throws EJBExceptionLP
	 * @return QueryResult
	 */
	public QueryResult getPageAt(Integer rowIndex) throws EJBExceptionLP {
		QueryResult result = null;
		SessionFactory factory = FLRSessionFactory.getFactory();
		Session session = null;
		try {
			int colCount = getTableInfo().getColumnClasses().length;
			int pageSize = PAGE_SIZE;
			int startIndex = Math.max(rowIndex.intValue() - (pageSize / 2), 0);
			int endIndex = startIndex + pageSize - 1;

			session = factory.openSession();

			HashMap<String, Object> params = new HashMap<>();
			String queryString = this.getFromClause() + this.buildWhereClause(params) + this.buildOrderByClause();

			Query query = session.createQuery(queryString);
			for (Map.Entry<String, Object> set: params.entrySet()) {
				query.setParameter(set.getKey(), set.getValue());
			}

			query.setFirstResult(startIndex);
			query.setMaxResults(pageSize);
			List<?> resultList = query.list();
			Iterator<?> resultListIterator = resultList.iterator();
			Object[][] rows = new Object[resultList.size()][colCount];
			int row = 0;
			int col = 0;
			while (resultListIterator.hasNext()) {
				FLRBank bank = (FLRBank) resultListIterator.next();
				rows[row][col++] = bank.getI_id();
				rows[row][col++] = bank.getFlrpartner().getC_name1nachnamefirmazeile1();
				rows[row][col++] = bank.getC_blz();
				rows[row][col++] = bank.getC_bic();
				row++;
				col = 0;
			}
			result = new QueryResult(rows, this.getRowCount(), startIndex, endIndex, 0);
		} catch (Exception e) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER_FLR, e);
		} finally {
			try {
				session.close();
			} catch (HibernateException he) {
				throw new EJBExceptionLP(EJBExceptionLP.FEHLER_FLR, he);
			}
		}
		return result;
	}

	/**
	 * gets the total number of rows represented by the current query.
	 * 
	 * @see UseCaseHandler#getRowCountFromDataBase()
	 * @return int
	 */
	@Override
	protected long getRowCountFromDataBase() {
		HashMap<String, Object> params = new HashMap<>();
		String queryString = "select count(*) " + this.getFromClause() + this.buildWhereClause(params);
		return getRowCountFromDataBaseByQuery(queryString, params);
	}
	/**
	 * builds the where clause of the HQL (Hibernate Query Language) statement
	 * using the current query.
	 * 
	 * @return the HQL where clause.
	 */
	private String buildWhereClause(HashMap<String, Object> params) {
		StringBuffer where = new StringBuffer("");

		if (this.getQuery() != null && this.getQuery().getFilterBlock() != null && this.getQuery().getFilterBlock().filterKrit != null) {
			FilterBlock filterBlock = this.getQuery().getFilterBlock();
			FilterKriterium[] filterKriterien = this.getQuery().getFilterBlock().filterKrit;
			String booleanOperator = filterBlock.boolOperator;
			boolean filterAdded = false;

			for (int i = 0; i < filterKriterien.length; i++) {
				if (filterKriterien[i].isKrit) {
					if (filterAdded) {
						where.append(" ").append(booleanOperator).append(" ");
					}
					filterAdded = true;

					Class<?> cls = String.class;
					if (("i_id").equals(filterKriterien[i].kritName)) {
						cls = Integer.class;
					}

					Object filterValue = parseFilterParamValue(filterKriterien[i], cls);

					if (filterKriterien[i].kritName.equals(BankFac.FLR_PARTNERBANK_BLZ)) {
						where.append("(").append("lower(").append("bank.").append(BankFac.FLR_PARTNERBANK_BLZ).append(")").append(" LIKE ").append(":bankBLZorBIC");
						where.append(" OR ").append("lower(").append("bank.").append(BankFac.FLR_PARTNERBANK__C_BIC).append(")").append(" LIKE ").append(":bankBLZorBIC").append(") ");
						params.put("bankBLZorBIC", filterValue);
					} else if (filterKriterien[i].kritName.equals(BankFac.FLR_PARTNERBANK_IBAN)) {
						where.append("EXISTS (SELECT 1 FROM FLRPartnerbank pb WHERE pb.partnerbank_i_id = bank.flrpartner.i_id and lower(c_iban) ").append(filterKriterien[i].operator).append(" :iban").append(") ");
						filterValue = filterValue.toString().replaceAll(" ", "");
						params.put("iban", filterValue);
					} else if (filterKriterien[i].kritName.equals(BankFac.FLR_PARTNER_NAME1NACHNAMEFIRMAZEILE1)) {
						if (filterKriterien[i].isBIgnoreCase()) {
							where.append("(").append("lower(").append("bank.").append(BankFac.FLR_PARTNER_NAME1NACHNAMEFIRMAZEILE1).append(")").append(" ").append(filterKriterien[i].operator).append(" " + ":partnerName1NachnameFirmaZeile1").append(") ");
						} else {
							where.append("(").append(" bank.").append(filterKriterien[i].kritName).append(" ").append(filterKriterien[i].operator).append(" " +" :partnerName1NachnameFirmaZeile1").append(") ");
						}
						params.put("partnerName1NachnameFirmaZeile1", filterValue);
					} else {
						where.append("(").append(" bank.").append(filterKriterien[i].kritName).append(" ").append(filterKriterien[i].operator).append(" :").append(filterKriterien[i].kritName).append(") ");
						params.put(filterKriterien[i].kritName, filterValue);
					}
				}
			}
			if (filterAdded) {
				where.insert(0, " WHERE ");
			}
		}

		return where.toString();
	}

	/**
	 * builds the HQL (Hibernate Query Language) order by clause using the sort
	 * criterias contained in the current query.
	 * 
	 * @return the HQL order by clause.
	 */
	private String buildOrderByClause() {
		StringBuffer orderBy = new StringBuffer();
		if (this.getQuery() != null) {
			SortierKriterium[] kriterien = this.getQuery().getSortKrit();
			boolean sortAdded = false;
			if (kriterien != null && kriterien.length > 0) {
				for (int i = 0; i < kriterien.length; i++) {
					if (kriterien[i].isKrit) {
						if (sortAdded) {
							orderBy.append(", ");
						}
						sortAdded = true;
						orderBy.append("bank.").append(kriterien[i].kritName).append(" ").append(kriterien[i].value);
					}
				}
			} else {
				// no sort criteria found, add default sort
				if (sortAdded) {
					orderBy.append(", ");
				}
				orderBy.append("bank.flrpartner.c_name1nachnamefirmazeile1 ASC ");
				sortAdded = true;
			}
			if (orderBy.indexOf("bank.flrpartner.c_name1nachnamefirmazeile1") < 0) {
				// unique sort required because otherwise rowNumber of
				// selectedId
				// within sort() method may be different from the position of
				// selectedId
				// as returned in the page of getPageAt().
				if (sortAdded) {
					orderBy.append(", ");
				}
				orderBy.append(" bank.flrpartner.c_name1nachnamefirmazeile1 ");
				sortAdded = true;
			}
			if (sortAdded) {
				orderBy.insert(0, " ORDER BY ");
			}
		}
		return orderBy.toString();
	}

	/**
	 * get the basic from clause for the HQL statement.
	 * 
	 * @return the from clause.
	 */
	private String getFromClause() {
		return FLR_FROM_STRING;
	}

	/**
	 * sorts the data described by the current query using the specified sort
	 * criterias. The current query is also updated with the new sort criterias.
	 * 
	 * @see UseCaseHandler#sort(SortierKriterium[], Object)
	 * @throws EJBExceptionLP
	 * @param sortierKriterien
	 *            SortierKriterium[]
	 * @param selectedId
	 *            Object
	 * @return QueryResult
	 */

	@Override
	public QueryResult sort(SortierKriterium[] sortierKriterien, Object selectedId) throws EJBExceptionLP {
		HashMap<String, Object> params = new HashMap<>();
		return defaultSort(sortierKriterien, selectedId, FLR_SORT_STRING + this.buildWhereClause(params) + buildOrderByClause(), params);
	}

	public TableInfo getTableInfo() {
		if (super.getTableInfo() == null) {
			String mandantCNr = theClientDto.getMandant();
			Locale locUI = theClientDto.getLocUi();
			setTableInfo(new TableInfo(
					new Class[] {
							Integer.class,
							String.class,
							String.class,
							String.class
					},
					new String[] {
							"i_id",
							getTextRespectUISpr("lp.name", mandantCNr, locUI),
							getTextRespectUISpr("lp.blz", mandantCNr, locUI),
							getTextRespectUISpr("lp.bic", mandantCNr, locUI)
					},
					new int[] {
							QueryParameters.FLR_BREITE_SHARE_WITH_REST,
							QueryParameters.FLR_BREITE_SHARE_WITH_REST,
							QueryParameters.FLR_BREITE_M,
							QueryParameters.FLR_BREITE_M
					}, new String[] {
							"i_id",
							BankFac.FLR_PARTNER_NAME1NACHNAMEFIRMAZEILE1,
							BankFac.FLR_PARTNERBANK_BLZ, "c_bic"
					}));
		}
		return super.getTableInfo();
	}
}
