/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 * 
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 * 
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: developers@heliumv.com
 ******************************************************************************/
package com.lp.server.auftrag.fastlanereader;

import com.lp.server.artikel.ejb.Artikel;
import com.lp.server.artikel.service.ArtikelFac;
import com.lp.server.auftrag.ejb.Auftragposition;
import com.lp.server.auftrag.fastlanereader.generated.FLRPositionenSichtAuftrag;
import com.lp.server.auftrag.service.AuftragpositionFac;
import com.lp.server.system.fastlanereader.service.TableColumnInformation;
import com.lp.server.system.service.LocaleFac;
import com.lp.server.util.AuftragPositionNumberAdapter;
import com.lp.server.util.Facade;
import com.lp.server.util.PositionNumberHandler;
import com.lp.server.util.fastlanereader.FLRSessionFactory;
import com.lp.server.util.fastlanereader.UseCaseHandler;
import com.lp.server.util.fastlanereader.service.query.*;
import com.lp.util.EJBExceptionLP;
import com.lp.util.Helper;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.EntityManager;
import java.rmi.RemoteException;
import java.util.*;

/**
 * <p>
 * Hier wird die FLR Funktionalitaet fuer die Lieferscheinpositionen
 * implementiert. Pro UseCase gibt es einen Handler.
 *
 * </p>
 * <p>
 * Copright Logistik Pur Software GmbH (c) 2004-2007
 * </p>
 * <p>
 * Erstellungsdatum 2004-10-21
 * </p>
 * <p>
 * </p>
 * 
 * @author Uli Walch
 * @version 1.0
 */

public abstract class PositionenSichtAuftragHandler extends UseCaseHandler {

	private static final long serialVersionUID = 1L;
	protected static final String FLR_PROPERTY = " flrpositionensichtauftrag.";
	private static final String FLR_FROM_CLAUSE = " FROM FLRPositionenSichtAuftrag flrpositionensichtauftrag " +
			"LEFT OUTER JOIN flrpositionensichtauftrag.flrartikel as flrartikel ";
	private static final String FLR_SORTQUERY = "SELECT " + FLR_PROPERTY + AuftragpositionFac.FLR_AUFTRAGPOSITIONSICHTAUFTRAG_I_ID +
			 FLR_FROM_CLAUSE;

	/**
	 * gets the correct code (messages.properties) of the column name depending if its used for LS- or RechungspositionsNr
	 * @return translation code
	 */
	abstract String uebergeordnetePositionsNrCode();

	/**
	 * gets the correct (LS/Rechnung) PositionsNr from the corresponding positionIId
	 */
	abstract Integer getPositionsNr(Integer positionIId);

	/**
	 * get the extended from clause for the HQL statement. Is different for Rechnung/LS
	 */
	abstract String getExtendedFromClause(HashMap<String, Object> params);

	/**
	 * get the from clause for the HQL statement without Rechnung/LS specifics
	 */
	protected String getFromClause(){
		return FLR_FROM_CLAUSE;
	}

	/**
	 * gets the data page for the specified row using the current query. The row at
	 * rowIndex will be located in the middle of the page.
	 * 
	 * @param rowIndex diese Zeile soll selektiert sein
	 * @return QueryResult das Ergebnis der Abfrage
	 * @throws EJBExceptionLP Ausnahme
	 * @see UseCaseHandler#getPageAt(java.lang.Integer)
	 */
	public QueryResult getPageAt(Integer rowIndex) throws EJBExceptionLP {
		QueryResult result = null;
		SessionFactory factory = FLRSessionFactory.getFactory();
		Session session = null;

		try {
			int colCount = getTableInfo().getColumnClasses().length;
			session = factory.openSession();
			HashMap<String, Object> params = new HashMap<>();
			String queryString = this.getExtendedFromClause(params) + this.buildWhereClause(params) + this.buildOrderByClause();
			Query query = session.createQuery(queryString);
			for (Map.Entry<String, Object> set: params.entrySet()) {
				query.setParameter(set.getKey(), set.getValue());
			}

			List<?> resultList = query.list();
			Iterator<?> resultListIterator = resultList.iterator();
			if (getRowCount() != resultList.size()) {
				throw new EJBExceptionLP(EJBExceptionLP.FEHLER_FLR, new Exception("rowCount != resultList.size()"));
			}

			Object[][] rows = new Object[resultList.size()][colCount];
			int row = 0;
			int col = 0;

			// collect necessary data for order positions and items to speed up the order position numbering
			long ts = System.currentTimeMillis();
			EntityManager em = factory.createEntityManager();
			HashMap<Integer, Artikel> artikelHm = new HashMap<Integer, Artikel>();

			List<Auftragposition> auftragpositions = null;
			HashMap<Integer, Auftragposition> auftragpositionsHm = new HashMap<Integer, Auftragposition>();
			try {
				Object[] o = (Object[]) resultList.iterator().next();
				FLRPositionenSichtAuftrag position = (FLRPositionenSichtAuftrag) o[0];
				javax.persistence.Query apquery = em.createNamedQuery("AuftragpositionfindByAuftrag");
				apquery.setParameter(1, position.getAuftrag_i_id());
				auftragpositions = apquery.getResultList();
				for (Auftragposition ap : auftragpositions) {
					auftragpositionsHm.put(ap.getIId(), ap);
					if (ap.getArtikelIId() != null) {
						Artikel artikel = em.find(Artikel.class, ap.getArtikelIId());
						artikelHm.put(ap.getArtikelIId(), artikel);
					}
				}
			} catch (Exception e) {
					System.out.println(e.getMessage());
			}
			PositionNumberHandler positionNumberHandler = new PositionNumberHandler();
			AuftragPositionNumberAdapter adapter = new AuftragPositionNumberAdapter(em, artikelHm, auftragpositions);
			long ts1 = System.currentTimeMillis();
			myLogger.info("Init NumberAdapter Start: " + ts + "Ende: " + ts1 + "Dauer: " + (ts1-ts) + " ms");

			while (resultListIterator.hasNext()) {
				Object[] o = (Object[]) resultListIterator.next();
				FLRPositionenSichtAuftrag position = (FLRPositionenSichtAuftrag) o[0];
				Integer uebergeordnetePositionIId = (Integer) o[1];

				Artikel artikel = null;
				if (auftragpositionsHm.get(position.getI_id()).getArtikelIId() != null) {
					artikel = artikelHm.get(auftragpositionsHm.get(position.getI_id()).getArtikelIId());
				}
				rows[row][col++] = position.getI_id();
				rows[row][col++] = uebergeordnetePositionIId == null ? null : getPositionsNr(uebergeordnetePositionIId);
				rows[row][col++] = positionNumberHandler.getPositionNummer(position.getI_id(), adapter);

				String einheitCNr = "";
				String ident = "";
				String referenznummer = "";
				if (artikel != null) {
					einheitCNr = artikel.getEinheitCNr() == null ? "" : artikel.getEinheitCNr().trim();
					ident = artikel.getCNr();
					referenznummer = artikel.getCReferenznr();
				}
				rows[row][col++] = einheitCNr;
				rows[row][col++] = ident;
				if (bReferenznummerInPositionen) {
					rows[row][col++] = referenznummer;
				}

				rows[row][col++] = getSBezeichnung(position, artikel);
				rows[row][col++] = position.getN_offene_menge();
				rows[row][col++] = position.getAuftragpositionstatus_c_nr();
				rows[row++][col++] = position.getT_uebersteuerterliefertermin();
				col = 0;
			}
			result = new QueryResult(rows, this.getRowCount(), 0, resultList.size(), 0);
		} catch (Exception e) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER_FLR, e);
		} finally {
			closeSession(session);
		}
		return result;
	}

	/**
	 * gets the total number of rows represented by the current query.
	 */
	protected long getRowCountFromDataBase() {
		HashMap<String, Object> params = new HashMap<>();
		String queryString = "select count(*) " + getFromClause() + this.buildWhereClause(params);
		return getRowCountFromDataBaseByQuery(queryString, params);
	}


	/**
	 * builds the where clause of the HQL (Hibernate Query Language) statement using
	 * the current query.
	 * 
	 * @return the HQL where clause.
	 */
	private String buildWhereClause(HashMap<String, Object> params) {
		StringBuffer where = new StringBuffer();

		if (this.getQuery() != null && this.getQuery().getFilterBlock() != null	&& this.getQuery().getFilterBlock().filterKrit != null) {
			FilterBlock filterBlock = this.getQuery().getFilterBlock();
			FilterKriterium[] filterKriterien = this.getQuery().getFilterBlock().filterKrit;
			String booleanOperator = filterBlock.boolOperator;
			boolean filterAdded = false;

            for (FilterKriterium filterKriterium : filterKriterien) {
                if (filterKriterium.isKrit) {
					String filterValueQuotes = filterKriterium.isBIgnoreCase() ? filterKriterium.value.toLowerCase() : filterKriterium.value;
					String filterOperator = filterKriterium.operator;
					String filterValue = removeSQLQuotas(filterValueQuotes);
					String filterName = filterKriterium.kritName;

					if (filterAdded) {
                        where.append(" ").append(booleanOperator);
                    }
                    filterAdded = true;

                    if (filterName.equals("auftragpositionstatus_c_nr")) {
                        where.append(" (" + FLR_PROPERTY + "auftragpositionstatus_c_nr IS NULL OR flrpositionensichtauftrag.auftragpositionstatus_c_nr <> 'Erledigt       '  ) ");
                    } else if (filterName.equals(AuftragpositionFac.FLR_AUFTRAGPOSITIONSICHTAUFTRAG_AUFTRAG_I_ID)) {
						where.append(FLR_PROPERTY).append(filterName).append(filterOperator).append(":").append(filterName);
						params.put(filterName, Integer.parseInt(filterValue));
					}
                }
            }
			if (filterAdded) {
				where.insert(0, " WHERE ");
			}
		}
		return where.toString();
	}

	/**
	 * builds the HQL (Hibernate Query Language) order by clause using the sort
	 * criterias contained in the current query.
	 * 
	 * @return the HQL order by clause.
	 */
	private String buildOrderByClause() {
		if (getQuery() == null)
			return "";

		String orderByCnr = FLR_PROPERTY + "i_sort";
		StringBuilder orderBy = new StringBuilder(" ORDER BY ");
		SortierKriterium[] kriterien = getQuery().getSortKrit();
		boolean sortAdded = false;
		if (kriterien != null && kriterien.length > 0) {
			for (SortierKriterium krit : kriterien) {
				if (krit.isKrit) {
					if (sortAdded)
						orderBy.append(", ");

					sortAdded = true;
					orderBy.append(FLR_PROPERTY).append(krit.kritName).append(" ").append(krit.value);
				}
			}
		}

		if (orderBy.indexOf(orderByCnr) < 0) {
			if (sortAdded) {
				orderBy.append(", ");
			}
			orderBy.append(orderByCnr).append(" ").append("ASC");
		}
		return orderBy.toString();
	}


	/**
	 * sorts the data described by the current query using the specified sort
	 * criterias. The current query is also updated with the new sort criterias.
	 * 
	 * @param sortierKriterien nach diesen Kriterien wird das Ergebnis sortiert
	 * @param selectedId       auf diesem Datensatz soll der Cursor stehen
	 * @return QueryResult das Ergebnis der Abfrage
	 * @throws EJBExceptionLP Ausnahme
	 * @see UseCaseHandler#sort(SortierKriterium[], Object)
	 */
	public QueryResult sort(SortierKriterium[] sortierKriterien, Object selectedId) throws EJBExceptionLP {
		HashMap<String, Object> params = new HashMap<>();
		return defaultSort(sortierKriterien, selectedId,
				FLR_SORTQUERY + this.buildWhereClause(params) + buildOrderByClause(), params);
	}

	private TableColumnInformation createColumnInformation(String mandant, Locale locUi) {
		TableColumnInformation columns = new TableColumnInformation();
		try {
			String mandantCNr = theClientDto.getMandant();
			int iNachkommastellenMenge = getMandantFac().getNachkommastellenMenge(mandantCNr);
			String uebergeordnetePositionsNrCode = uebergeordnetePositionsNrCode();

			columns.add("i_id", Integer.class, "i_id", QueryParameters.FLR_BREITE_SHARE_WITH_REST, "i_id");
			columns.add(uebergeordnetePositionsNrCode, Integer.class, getTextRespectUISpr(uebergeordnetePositionsNrCode, mandant, locUi), QueryParameters.FLR_BREITE_S, Facade.NICHT_SORTIERBAR);
			columns.add("lp.nr", Integer.class, getTextRespectUISpr("lp.nr", mandant, locUi), QueryParameters.FLR_BREITE_XS, Facade.NICHT_SORTIERBAR);
			columns.add("lp.einheit", String.class, getTextRespectUISpr("lp.einheit", mandant, locUi), QueryParameters.FLR_BREITE_XS, AuftragpositionFac.FLR_AUFTRAGPOSITIONSICHTAUFTRAG_FLRARTIKEL + "." + ArtikelFac.FLR_ARTIKEL_EINHEIT_C_NR);
			columns.add("lp.ident", String.class, getTextRespectUISpr("lp.artikel", mandant, locUi), getUIBreiteIdent(), AuftragpositionFac.FLR_AUFTRAGPOSITIONSICHTAUFTRAG_FLRARTIKEL + "." + ArtikelFac.FLR_ARTIKEL_C_NR);
			if (bReferenznummerInPositionen) {
				columns.add("lp.referenznummer", String.class, getTextRespectUISpr("lp.referenznummer", mandant, locUi),
						QueryParameters.FLR_BREITE_XM, AuftragpositionFac.FLR_AUFTRAGPOSITION_FLRARTIKEL + ".c_referenznr");
			}
			columns.add("lp.bezeichnung", String.class, getTextRespectUISpr("lp.bezeichnung", mandant, locUi), QueryParameters.FLR_BREITE_SHARE_WITH_REST, Facade.NICHT_SORTIERBAR);
			columns.add("auft.offen", super.getUIClassBigDecimalNachkommastellen(iNachkommastellenMenge), getTextRespectUISpr("auft.offen", mandant, locUi),
					QueryParameters.FLR_BREITE_M, AuftragpositionFac.FLR_AUFTRAGPOSITIONSICHTAUFTRAG_N_OFFENE_MENGE);
			columns.add("lp.status", String.class, getTextRespectUISpr("lp.status", mandant, locUi),
					QueryParameters.FLR_BREITE_M, AuftragpositionFac.FLR_AUFTRAGPOSITIONSICHTAUFTRAG_AUFTRAGPOSITIONSTATUS_C_NR);
			columns.add("lp.termin", Date.class, getTextRespectUISpr("lp.termin", mandant, locUi),
					QueryParameters.FLR_BREITE_M, AuftragpositionFac.FLR_AUFTRAGPOSITIONSICHTAUFTRAG_T_UEBERSTEUERTERLIEFERTERMIN);

		} catch (RemoteException ex) {
			throw new EJBExceptionLP(EJBExceptionLP.FEHLER, ex);
		}
		return columns;
	}

	public TableInfo getTableInfo() {
		TableInfo info = super.getTableInfo();
		if (info != null)
			return info;

		setTableColumnInformation(createColumnInformation(theClientDto.getMandant(), theClientDto.getLocUi()));

		TableColumnInformation c = getTableColumnInformation();
		info = new TableInfo(c.getClasses(), c.getHeaderNames(), c.getWidths(), c.getDbColumNames(),
				c.getHeaderToolTips());
		setTableInfo(info);
		return info;
	}

	/**
	 * Formats the Bezeichnung of the Position
	 * @param position
	 * @return
	 * @throws RemoteException
	 */
	private String getSBezeichnung(FLRPositionenSichtAuftrag position, Artikel artikel) throws RemoteException {
		String mandantCNr = theClientDto.getMandant();
		Locale locUI = theClientDto.getLocUi();
		String sBezeichnung = null;

		if (position.getTyp_c_nr() == null) {
			if (position.getPositionart_c_nr().equals(LocaleFac.POSITIONSART_TEXTEINGABE)) {
				if (position.getX_textinhalt() != null && !position.getX_textinhalt().isEmpty()) {
					sBezeichnung = Helper.strippHTML(position.getX_textinhalt());
				}
			} else if (position.getPositionart_c_nr().equals(LocaleFac.POSITIONSART_SEITENUMBRUCH)) {
				sBezeichnung = "[" + getTextRespectUISpr("lp.seitenumbruch", mandantCNr, locUI) + "]";
			} else if (position.getPositionart_c_nr().equals(LocaleFac.POSITIONSART_ENDSUMME)) {
				sBezeichnung = "[" + getTextRespectUISpr("lp.endsumme", mandantCNr, locUI) + "]";
			} else if (position.getPositionart_c_nr().equals(LocaleFac.POSITIONSART_POSITION)) {
				sBezeichnung = getTextRespectUISpr("lp.position", mandantCNr, locUI) + " " + position.getC_bez();
			} else if (position.getPositionart_c_nr().equals(LocaleFac.POSITIONSART_TEXTBAUSTEIN)) {
				sBezeichnung = position.getFlrmediastandard().getC_nr();
			} else if (position.getPositionart_c_nr().equals(LocaleFac.POSITIONSART_IDENT)) {
				// sBezeichnung = getArtikelFac().formatArtikelbezeichnungEinzeiligOhneExcUebersteuert(position.getFlrartikel().getI_id(), theClientDto.getLocUi(),position.getC_bez(),position.getC_zbez());
				sBezeichnung = getArtikelFac().formatArtikelbezeichnungEinzeiligOhneExcUebersteuert(artikel.getIId(), theClientDto.getLocUi(),position.getC_bez(),position.getC_zbez());
			} else if (position.getPositionart_c_nr().equals(LocaleFac.POSITIONSART_INTELLIGENTE_ZWISCHENSUMME)) {
				sBezeichnung = "[" + getTextRespectUISpr("lieferscheinsichtauftrag.intelligentezwischensumme", mandantCNr, locUI) + "] " + position.getC_bez();
			} else if (position.getC_bez() != null) {
				sBezeichnung = position.getC_bez();
			}
		} else {
			if (position.getPosition_i_id() == null) {
				if (position.getPositionart_c_nr().equals(LocaleFac.POSITIONSART_POSITION)) {
					sBezeichnung = getTextRespectUISpr("lp.position", mandantCNr, locUI);
					if (position.getC_bez() != null) {
						sBezeichnung = sBezeichnung + " " + position.getC_bez();
					}
				}
			} else {
				if (position.getPositionart_c_nr().equals(LocaleFac.POSITIONSART_POSITION)) {
					if (position.getTyp_c_nr().equals(LocaleFac.POSITIONTYP_EBENE1)) {
						sBezeichnung = getTextRespectUISpr("lp.position", mandantCNr, locUI);
					} else {
						sBezeichnung = "  " + getTextRespectUISpr("lp.position", mandantCNr, locUI);
					}
					if (position.getC_bez() != null) {
						sBezeichnung = sBezeichnung + " " + position.getC_bez();
					}
				} else {
					if (position.getPositionart_c_nr().equals(LocaleFac.POSITIONSART_TEXTEINGABE)) {
						if (position.getTyp_c_nr().equals(LocaleFac.POSITIONTYP_EBENE1) && position.getX_textinhalt() != null && !position.getX_textinhalt().isEmpty()) {
							sBezeichnung = " " + Helper.strippHTML(position.getX_textinhalt());
						}
					} else if (position.getPositionart_c_nr().equals(LocaleFac.POSITIONSART_SEITENUMBRUCH)) {
						sBezeichnung = "[" + getTextRespectUISpr("lp.seitenumbruch", mandantCNr, locUI) + "]";
					} else if (position.getPositionart_c_nr().equals(LocaleFac.POSITIONSART_ENDSUMME)) {
						sBezeichnung = "[" + getTextRespectUISpr("lp.endsumme", mandantCNr, locUI) + "]";
					} else if (position.getPositionart_c_nr().equals(LocaleFac.POSITIONSART_POSITION)) {
						sBezeichnung = getTextRespectUISpr("lp.position", mandantCNr, locUI) + " " + position.getC_bez();
					} else if (position.getPositionart_c_nr().equals(LocaleFac.POSITIONSART_TEXTBAUSTEIN)) {
						sBezeichnung = position.getFlrmediastandard().getC_nr();
					} else if (position.getPositionart_c_nr().equals(LocaleFac.POSITIONSART_IDENT)) {
						if (position.getTyp_c_nr().equals(LocaleFac.POSITIONTYP_EBENE1)) {
							//sBezeichnung = "  " + getArtikelFac().formatArtikelbezeichnungEinzeiligOhneExc(position.getFlrartikel().getI_id(), theClientDto.getLocUi());
							sBezeichnung = "  " + getArtikelFac().formatArtikelbezeichnungEinzeiligOhneExc(artikel.getIId(), theClientDto.getLocUi());
						} else if (position.getTyp_c_nr().equals(LocaleFac.POSITIONTYP_EBENE2)) {
							//sBezeichnung = "    " + getArtikelFac().formatArtikelbezeichnungEinzeiligOhneExc(position.getFlrartikel().getI_id(), theClientDto.getLocUi());
							sBezeichnung = "    " + getArtikelFac().formatArtikelbezeichnungEinzeiligOhneExc(artikel.getIId(), theClientDto.getLocUi());
						}
					}
				}
			}
		}
		return sBezeichnung;
	}
}
