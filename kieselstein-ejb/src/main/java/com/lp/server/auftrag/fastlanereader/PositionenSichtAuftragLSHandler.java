package com.lp.server.auftrag.fastlanereader;

import com.lp.server.lieferschein.service.LieferscheinpositionFac;
import com.lp.server.util.fastlanereader.service.query.FilterKriterium;

import java.util.HashMap;

/**
 * Handler for Panel Lieferschein/Sicht-Auftrag, mostly to get the LSPositionNr
 */
public class PositionenSichtAuftragLSHandler extends PositionenSichtAuftragHandler{

    @Override
    protected String uebergeordnetePositionsNrCode(){
        return "ls.lieferscheinnr.short";
    }
    @Override
    protected Integer getPositionsNr(Integer positionIId) {
        return getLieferscheinpositionFac().getLSPositionNummer(positionIId);
    }

    @Override
    protected String getExtendedFromClause(HashMap<String, Object> params){
        String filterIId = "1 = 2 ";
        if (this.getQuery() != null && this.getQuery().getFilterBlock() != null && this.getQuery().getFilterBlock().filterKrit != null) {
            FilterKriterium[] filterKriterien = this.getQuery().getFilterBlock().filterKrit;
            for (FilterKriterium filter : filterKriterien)
                if (filter.kritName.equals(LieferscheinpositionFac.FLR_LIEFERSCHEIN_I_ID)) {
                    Integer value = Integer.parseInt(filter.value);
                    filterIId = "flrlieferscheinposition." + LieferscheinpositionFac.FLR_LIEFERSCHEIN_I_ID + filter.operator + ":" + LieferscheinpositionFac.FLR_LIEFERSCHEIN_I_ID;
                    params.put(LieferscheinpositionFac.FLR_LIEFERSCHEIN_I_ID, value);
                    break;
                }
        }
        return "SELECT flrpositionensichtauftrag, flrlieferscheinposition.i_id " + getFromClause() +
                "LEFT OUTER JOIN FLRLieferscheinposition flrlieferscheinposition " +
                "ON flrlieferscheinposition.auftragposition_i_id = flrpositionensichtauftrag.i_id " +
                "AND " + filterIId + " ";
    }
}
