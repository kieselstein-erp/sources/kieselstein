package com.lp.server.auftrag.fastlanereader;

import com.lp.server.rechnung.service.RechnungFac;
import com.lp.server.util.PositionNumberHandler;
import com.lp.server.util.RechnungPositionNumberAdapter;
import com.lp.server.util.fastlanereader.service.query.FilterKriterium;
import com.lp.util.EJBExceptionLP;

import java.util.HashMap;

/**
 * Handler for Panel Rechnung/Sicht-Auftrag, mostly to get the RechnungPositionNr
 */
public class PositionenSichtAuftragRechnungHandler extends PositionenSichtAuftragHandler{
    private PositionNumberHandler positionNumberHandler;
    private RechnungPositionNumberAdapter rechnungPositionNumberAdapter = null;

    @Override
    Integer getPositionsNr(Integer positionIId) throws EJBExceptionLP {
        try {
            if (positionNumberHandler == null) {
                positionNumberHandler = new PositionNumberHandler();
            }
            if (rechnungPositionNumberAdapter == null) {
                rechnungPositionNumberAdapter = new RechnungPositionNumberAdapter(em);
            }

            return positionNumberHandler.getPositionNummer(positionIId, rechnungPositionNumberAdapter);
        } catch (Exception e) {
            throw new EJBExceptionLP(EJBExceptionLP.FEHLER_FLR, e);
        }
    }
    @Override
    protected String getExtendedFromClause(HashMap<String, Object> params){
        String filterIId = "1 = 2 ";
        if (this.getQuery() != null && this.getQuery().getFilterBlock() != null && this.getQuery().getFilterBlock().filterKrit != null) {
            FilterKriterium[] filterKriterien = this.getQuery().getFilterBlock().filterKrit;
            for (FilterKriterium filter : filterKriterien)
                if (filter.kritName.equals(RechnungFac.FLR_RECHUNGPOSITION_RECHNUNG_I_ID)) {
                    Integer value = Integer.parseInt(filter.value);
                    filterIId = "flrrechnungposition." + RechnungFac.FLR_RECHUNGPOSITION_RECHNUNG_I_ID + filter.operator + ":" + RechnungFac.FLR_RECHUNGPOSITION_RECHNUNG_I_ID;
                    params.put(RechnungFac.FLR_RECHUNGPOSITION_RECHNUNG_I_ID, value);
                    break;
                }
        }
        return "SELECT flrpositionensichtauftrag, flrrechnungposition.i_id " + getFromClause() +
                "LEFT OUTER JOIN FLRRechnungPosition flrrechnungposition " +
                "ON flrrechnungposition.auftragposition_i_id = flrpositionensichtauftrag.i_id " +
                "AND " + filterIId + " ";
    }
    @Override
    String uebergeordnetePositionsNrCode() {
        return "rechnung.nr.short";
    }
}
