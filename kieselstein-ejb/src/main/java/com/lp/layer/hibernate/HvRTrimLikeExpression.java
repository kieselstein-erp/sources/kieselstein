package com.lp.layer.hibernate;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.engine.spi.TypedValue;

public class HvRTrimLikeExpression implements Criterion {
  private static final long serialVersionUID = -1757885414043309017L;
  
  private final String propertyName;
  
  private final Object value;
  
  public HvRTrimLikeExpression(String propertyName, String value) {
    this.propertyName = propertyName;
    this.value = value;
  }
  
  public HvRTrimLikeExpression(String propertyName, String value, MatchMode matchMode) {
    this(propertyName, matchMode.toMatchString(value));
  }
  
  public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
    String[] columns = criteriaQuery.getColumnsUsingProjection(criteria, this.propertyName);
    if (columns.length != 1)
      throw new HibernateException("RTRIM may only used with single-column properties"); 
    return " rtrim(" + columns[0] + ") like ?";
  }
  
  public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
    return new TypedValue[] { criteriaQuery.getTypedValue(criteria, this.propertyName, this.value.toString().toLowerCase()) };
  }
  
  public String toString() {
    return " rtrim(" + this.propertyName + ") like " + this.value;
  }
}
