package com.lp.layer.hibernate;

import org.hibernate.*;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.ResultTransformer;

import java.util.List;

public class HvTypedCriteria<T> implements Criteria {
  private Criteria c;
  
  public HvTypedCriteria(Criteria c) {
    this.c = c;
  }
  
  public HvTypedCriteria<T> add(Criterion arg0) {
    this.c.add(arg0);
    return this;
  }
  
  public HvTypedCriteria<T> addOrder(Order arg0) {
    this.c.addOrder(arg0);
    return this;
  }
  
  public Criteria createAlias(String arg0, String arg1) throws HibernateException {
    return this.c.createAlias(arg0, arg1);
  }
  
  public Criteria createAlias(String arg0, String arg1, int arg2) throws HibernateException {
    return this.c.createAlias(arg0, arg1, arg2);
  }
  
  public Criteria createCriteria(String arg0) throws HibernateException {
    return this.c.createCriteria(arg0);
  }
  
  public Criteria createCriteria(String arg0, int arg1) throws HibernateException {
    return this.c.createCriteria(arg0, arg1);
  }
  
  public Criteria createCriteria(String arg0, String arg1) throws HibernateException {
    return this.c.createCriteria(arg0, arg1);
  }
  
  public Criteria createCriteria(String arg0, String arg1, int arg2) throws HibernateException {
    return this.c.createCriteria(arg0, arg1, arg2);
  }
  
  public String getAlias() {
    return this.c.getAlias();
  }
  
  public List<T> list() throws HibernateException {
    return this.c.list();
  }
  
  public ScrollableResults scroll() throws HibernateException {
    return this.c.scroll();
  }
  
  public ScrollableResults scroll(ScrollMode arg0) throws HibernateException {
    return this.c.scroll(arg0);
  }
  
  public HvTypedCriteria<T> setCacheMode(CacheMode arg0) {
    this.c.setCacheMode(arg0);
    return this;
  }
  
  public HvTypedCriteria<T> setCacheRegion(String arg0) {
    this.c.setCacheRegion(arg0);
    return this;
  }
  
  public HvTypedCriteria<T> setCacheable(boolean arg0) {
    this.c.setCacheable(arg0);
    return this;
  }
  
  public HvTypedCriteria<T> setComment(String arg0) {
    this.c.setComment(arg0);
    return this;
  }
  
  public HvTypedCriteria<T> setFetchMode(String arg0, FetchMode arg1) throws HibernateException {
    this.c.setFetchMode(arg0, arg1);
    return this;
  }
  
  public HvTypedCriteria<T> setFetchSize(int arg0) {
    this.c.setFetchSize(arg0);
    return this;
  }
  
  public HvTypedCriteria<T> setFirstResult(int arg0) {
    this.c.setFirstResult(arg0);
    return this;
  }
  
  public HvTypedCriteria<T> setFlushMode(FlushMode arg0) {
    this.c.setFlushMode(arg0);
    return this;
  }
  
  public HvTypedCriteria<T> setLockMode(LockMode arg0) {
    this.c.setLockMode(arg0);
    return this;
  }
  
  public HvTypedCriteria<T> setLockMode(String arg0, LockMode arg1) {
    this.c.setLockMode(arg0, arg1);
    return this;
  }
  
  public HvTypedCriteria<T> setMaxResults(int arg0) {
    this.c.setMaxResults(arg0);
    return this;
  }
  
  public HvTypedCriteria<T> setProjection(Projection arg0) {
    this.c.setProjection(arg0);
    return this;
  }
  
  public HvTypedCriteria<T> setResultTransformer(ResultTransformer arg0) {
    this.c.setResultTransformer(arg0);
    return this;
  }
  
  public HvTypedCriteria<T> setTimeout(int arg0) {
    this.c.setTimeout(arg0);
    return this;
  }
  
  public T uniqueResult() throws HibernateException {
    return (T)this.c.uniqueResult();
  }
  
  public HvTypedCriteria<T> addQueryHint(String arg0) {
    this.c.addQueryHint(arg0);
    return this;
  }
  
  public HvTypedCriteria<T> createAlias(String arg0, String arg1, JoinType arg2) throws HibernateException {
    this.c.createAlias(arg0, arg1, arg2);
    return this;
  }
  
  public HvTypedCriteria<T> createAlias(String arg0, String arg1, JoinType arg2, Criterion arg3) throws HibernateException {
    this.c.createAlias(arg0, arg1, arg2, arg3);
    return this;
  }
  
  public Criteria createAlias(String arg0, String arg1, int arg2, Criterion arg3) throws HibernateException {
    this.c.createAlias(arg0, arg1, arg2, arg3);
    return this;
  }
  
  public HvTypedCriteria<T> createCriteria(String arg0, JoinType arg1) throws HibernateException {
    Criteria a = this.c.createCriteria(arg0, arg1);
    return new HvTypedCriteria(a);
  }
  
  public Criteria createCriteria(String arg0, String arg1, JoinType arg2) throws HibernateException {
    Criteria a = this.c.createCriteria(arg0, arg1, arg2);
    return new HvTypedCriteria(a);
  }
  
  public Criteria createCriteria(String arg0, String arg1, JoinType arg2, Criterion arg3) throws HibernateException {
    Criteria a = this.c.createCriteria(arg0, arg1, arg2, arg3);
    return new HvTypedCriteria(a);
  }
  
  public Criteria createCriteria(String arg0, String arg1, int arg2, Criterion arg3) throws HibernateException {
    Criteria a = this.c.createCriteria(arg0, arg1, arg2, arg3);
    return new HvTypedCriteria(a);
  }
  
  public boolean isReadOnly() {
    return this.c.isReadOnly();
  }
  
  public boolean isReadOnlyInitialized() {
    return this.c.isReadOnlyInitialized();
  }
  
  public Criteria setReadOnly(boolean arg0) {
    this.c.setReadOnly(arg0);
    return this;
  }
}
