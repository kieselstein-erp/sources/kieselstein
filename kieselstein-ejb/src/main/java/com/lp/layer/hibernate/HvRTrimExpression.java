package com.lp.layer.hibernate;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.engine.spi.TypedValue;

public class HvRTrimExpression implements Criterion {
  private static final long serialVersionUID = 5435831944290346874L;
  
  private final String propertyName;
  
  private static final TypedValue[] NO_VALUES = new TypedValue[0];
  
  public HvRTrimExpression(String propertyName) {
    this.propertyName = propertyName;
  }
  
  public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
    String[] columns = criteriaQuery.getColumnsUsingProjection(criteria, this.propertyName);
    if (columns.length != 1)
      throw new HibernateException("RTRIM may only used with single-column properties"); 
    return " rtrim(" + columns[0] + ")";
  }
  
  public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
    return NO_VALUES;
  }
  
  public String toString() {
    return " rtrim(" + this.propertyName + ")";
  }
}
