package com.lp.layer.hibernate;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.engine.spi.TypedValue;

public class HvLowerLikeExpression implements Criterion {
  private static final long serialVersionUID = -1757885414043309017L;
  
  private final String propertyName;
  
  private final Object value;
  
  public HvLowerLikeExpression(String propertyName, String value) {
    this.propertyName = propertyName;
    this.value = value;
  }
  
  public HvLowerLikeExpression(String propertyName, String value, MatchMode matchMode) {
    this(propertyName, matchMode.toMatchString(value));
  }
  
  public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
    String[] columns = criteriaQuery.getColumnsUsingProjection(criteria, this.propertyName);
    if (columns.length != 1)
      throw new HibernateException("UPPER may only used with single-column properties"); 
    return " lower(" + columns[0] + ") like ?";
  }
  
  public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
    return new TypedValue[] { criteriaQuery.getTypedValue(criteria, this.propertyName, this.value.toString().toLowerCase()) };
  }
  
  public String toString() {
    return " lower(" + this.propertyName + ") like " + this.value;
  }
}
