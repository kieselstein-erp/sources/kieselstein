package com.lp.util.barcode;

public class PartArbeitsgang extends BarcodePart {

	private final static int Length = 2;
	
	public PartArbeitsgang(String barcode, Integer startIdx) {
		super(barcode, startIdx, Length);
	}
	
	public PartArbeitsgang(BarcodePart predecessor) {
		super(predecessor, Length);
		// length is dynamic, leads from previous part to ".", if barcode contains "." else to end of barcode
		int length = Length;
		if (predecessor.getBarcode().contains(".")) {
			length = predecessor.getBarcode().indexOf(".") - predecessor.getEndIdx();
		} else if (predecessor.getBarcode().endsWith(HvBarcodeTyp.MaschineStopp.getText())
				|| predecessor.getBarcode().endsWith(HvBarcodeTyp.MaschineLaufzeit.getText())
				|| predecessor.getBarcode().endsWith(HvBarcodeTyp.LosAgFertig.getText())) {
			length = predecessor.getBarcode().lastIndexOf("$") - predecessor.getEndIdx();
		} else {
			length = predecessor.getBarcode().length() - predecessor.getEndIdx();
		}
		this.setLength(length);
	}

}
