package com.lp.util.barcode;

import java.io.Serializable;

public class BarcodeMaschineLaufzeit extends HvBarcodeBase implements Serializable  {

    private static final long serialVersionUID = -727993008939609116L;
    private String losCnr;
    private String maschineCnr;
    private String arbeitsgangNr;
    private String unterarbeitsgangNr;

    public BarcodeMaschineLaufzeit(String barcodeRaw) {
        super(HvBarcodeTyp.MaschineLaufzeit, barcodeRaw);
    }

    public String getLosCnr() {
        return losCnr;
    }
    public void setLosCnr(String losCnr) {
        this.losCnr = losCnr;
    }

    public String getMaschineCnr() {
        return maschineCnr;
    }
    public void setMaschineCnr(String maschineCnr) {
        this.maschineCnr = maschineCnr;
    }

    public String getArbeitsgangNr() {
        return arbeitsgangNr;
    }
    public void setArbeitsgangNr(String arbeitsgangNr) {
        this.arbeitsgangNr = arbeitsgangNr;
    }

    public String getUnterarbeitsgangNr() {
        return unterarbeitsgangNr;
    }
    public void setUnterarbeitsgangNr(String unterarbeitsgangNr) {
        this.unterarbeitsgangNr = unterarbeitsgangNr;
    }
}
