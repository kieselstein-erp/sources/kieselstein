/*******************************************************************************
 * HELIUM V, Open Source ERP software for sustained success
 * at small and medium-sized enterprises.
 * Copyright (C) 2004 - 2015 HELIUM V IT-Solutions GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published 
 * by the Free Software Foundation, either version 3 of theLicense, or 
 * (at your option) any later version.
 * 
 * According to sec. 7 of the GNU Affero General Public License, version 3, 
 * the terms of the AGPL are supplemented with the following terms:
 * 
 * "HELIUM V" and "HELIUM 5" are registered trademarks of 
 * HELIUM V IT-Solutions GmbH. The licensing of the program under the 
 * AGPL does not imply a trademark license. Therefore any rights, title and
 * interest in our trademarks remain entirely with us. If you want to propagate
 * modified versions of the Program under the name "HELIUM V" or "HELIUM 5",
 * you may only do so if you have a written permission by HELIUM V IT-Solutions 
 * GmbH (to acquire a permission please contact HELIUM V IT-Solutions
 * at trademark@heliumv.com).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: developers@heliumv.com
 ******************************************************************************/
package com.lp.util;

import java.sql.SQLException;

import javax.sql.ConnectionPoolDataSource;

public class PooledDatasource {
	private static final PooledDatasource OBJ = new PooledDatasource();
//	private static Object datasource;
	private static ConnectionPoolDataSource datasource ;
	private static boolean initialized = false;
	private static javax.sql.PooledConnection pc = null;
	private static String cachedConnectionUrl = null ;
			
	private PooledDatasource() {
	}

	public static PooledDatasource getInstance() {
		return OBJ;
	}

	protected synchronized void initPsql(String url) {
		org.postgresql.ds.PGConnectionPoolDataSource ds = new org.postgresql.ds.PGConnectionPoolDataSource();
		ds.setUrl(url);
		ds.setUser("hvguest");
		ds.setPassword("h4gzfdavfs");

		datasource = ds;
		pc = null ;
		initialized = true;
	}

	public boolean isInitalized() {
		return initialized;
	}
	
	public javax.sql.PooledConnection getPooledConnection() throws SQLException {
//		return ((ConnectionPoolDataSource)datasource).getPooledConnection();
		return datasource.getPooledConnection() ;
	}
	
	public synchronized java.sql.Connection getConnection() throws SQLException {
		if (pc == null) {
//			pc = ((ConnectionPoolDataSource)datasource).getPooledConnection();
			pc = datasource.getPooledConnection() ;
		}
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
		return pc.getConnection();
	}
	
	/**
	 * Setzt die Connection-URL</br>
	 * <p>&Auml;ndert sich die URL wird die darunterliegende Datasource neu 
	 * aufgebaut. Ansonsten wird eine bereits bestehende verwendet</p>
	 * @param url die neue Database-Connection-URL. Kann auch null sein.
	 */
	public synchronized void setUrl(String url) {
		if(url == null) {
			cachedConnectionUrl = null ;
			initialized = false ;
			pc = null ;				
			datasource = null ;
		} else {
			if(!url.equals(cachedConnectionUrl)) {
				initPsql(url);
				cachedConnectionUrl = url ;
			}
			
//			if(cachedConnectionUrl == null) {
//				initializeFrom(url) ;
//			} else {
//				if(url.compareTo(cachedConnectionUrl) != 0) {
//					initializeFrom(url) ;
//				}				
//			}
		}			
	}
}
