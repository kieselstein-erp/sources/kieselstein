#!/bin/sh

export LIQUIBASE_COMMAND_USERNAME=${MAIN_DB_USER:-"postgres"}
export LIQUIBASE_COMMAND_PASSWORD=${MAIN_DB_PASS:-"postgres"}
export LIQUIBASE_COMMAND_URL="jdbc:postgresql://${MAIN_DB_HOST:-"localhost"}:${MAIN_DB_PORT:-"5432"}/${MAIN_DB_NAME:-"KIESELSTEIN"}"
export LIQUIBASE_COMMAND_CHANGELOG_FILE=changelog.xml

exec liquibase "$@"