# Liquibase

> When editing changelogs you have to build the kieselstein-database project and use the generated scripts
> inside the build/resources directory!

[Install](https://docs.liquibase.com/start/install/liquibase-linux-debian-ubuntu.html)

### Existing database

```shell
$ liquibase.sh changelog-sync --label-filter="0.0.12"
$ liquibase.sh update
```

### New database

```shell
$ liquibase.sh update
```

### Check for new migrations

```shell
$ liquibase.sh status
```

### Rollback

```sql
SELECT filename,dateexecuted FROM databasechangelog ORDER BY dateexecuted DESC;
```

```shell
$ liquibase.sh rollback-to-date 2024-01-04'T'13:58:07.019721
```

# DO NOT MODIFY databasechangelog MANUALLY!

# Change liquibase runtime options

```
export JAVA_OPTS="-Dversion=0.0.0"
export LIQUIBASE_COMMAND_USERNAME=postgres
export LIQUIBASE_COMMAND_PASSWORD=postgres
export LIQUIBASE_COMMAND_URL="jdbc:postgresql://localhost:5432/db"
export LIQUIBASE_COMMAND_CHANGELOG_FILE=changelog.xml

liquibase status
```
