@echo off

rem This script assumes that psql is available in the path

psql -h @mainDbHost@ -U @mainDbUser@ -c "CREATE DATABASE \"@mainDbName@\" WITH OWNER = @mainDbUser@ ENCODING = 'UTF8'"
psql -h @mainDbHost@ -U @mainDbUser@ -c "ALTER DATABASE \"@mainDbName@\" SET datestyle TO 'iso, dmy'"

psql -h @docDbHost@ -U @docDbUser@ -c "CREATE DATABASE \"@docDbName@\" WITH OWNER = @docDbUser@ ENCODING = 'UTF8'"
psql -h @docDbHost@ -U @docDbUser@ -c "ALTER DATABASE \"@docDbName@\" SET datestyle TO 'iso, dmy'"

pause