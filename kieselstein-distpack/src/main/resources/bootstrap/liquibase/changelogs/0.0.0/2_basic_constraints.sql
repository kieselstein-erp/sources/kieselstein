ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT bes_bestellung_mahnstufe_i_id_bes_bsmahnstufe_i_id FOREIGN KEY (mahnstufe_i_id, mandant_c_nr) REFERENCES public.bes_bsmahnstufe(i_id, mandant_c_nr);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT fb_konto_mwstsatz_i_id_lp_mwstsatz_i_id FOREIGN KEY (mwstsatz_i_id) REFERENCES public.lp_mwstsatz(i_id);
ALTER TABLE ONLY public.fert_lospruefplan
    ADD CONSTRAINT "fk_$lopp_pruefkombination_i_id_stk_pruefkombination_i_id" FOREIGN KEY (pruefkombination_i_id) REFERENCES public.stk_pruefkombination(i_id);
ALTER TABLE ONLY public.pers_zeitdatenverrechnetzeitraum
    ADD CONSTRAINT "fk_$pzvz_zeitdaten_i_id_pers_zeitdaten_i_id" FOREIGN KEY (zeitdaten_i_id) REFERENCES public.pers_zeitdaten(i_id);
ALTER TABLE ONLY public.pers_zeitdatenverrechnetzeitraum
    ADD CONSTRAINT "fk_$pzvz_zeitdatenverrechnet_i_id_rech_rechnungsposition_i_id" FOREIGN KEY (rechnungposition_i_id) REFERENCES public.rech_rechnungposition(i_id);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT "fk_$stkl_stueckliste_i_id_formelstueckliste_$stkl_i_id" FOREIGN KEY (stueckliste_i_id_formelstueckliste) REFERENCES public.stk_stueckliste(i_id);
ALTER TABLE ONLY public.stk_stklpruefplan
    ADD CONSTRAINT "fk_$stpp_pruefkombination_i_id_stk_pruefkombination_i_id" FOREIGN KEY (pruefkombination_i_id) REFERENCES public.stk_pruefkombination(i_id);
ALTER TABLE ONLY public.pers_maschinenzmtagesart
    ADD CONSTRAINT "fk_$zmta_maschinenzm_i_id_pers_maschinenzm_i_id" FOREIGN KEY (maschinenzm_i_id) REFERENCES public.pers_maschinenzm(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT "fk_abab$_ansprechpartner_i_id_kunde_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id_kunde) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT "fk_abab$_ansprechpartner_i_id_rechnungsadresse_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id_rechnungsadresse) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT "fk_abab$_auftragwiederholungsintervall_c_nr_abwi$_c_nr" FOREIGN KEY (auftragwiederholungsintervall_c_nr) REFERENCES public.auft_auftragwiederholungsintervall(c_nr);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT "fk_abab$_personal_i_id_manuellerledigt_pepe$_i_id" FOREIGN KEY (personal_i_id_manuellerledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT "fk_abag$_ansprechpartner_i_id_rechnungsadresse_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id_rechnungsadresse) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT "fk_abau$_auftragbegruendung_i_id_auft_auftragbegruendung_i_id" FOREIGN KEY (auftragbegruendung_i_id) REFERENCES public.auft_auftragbegruendung(i_id);
ALTER TABLE ONLY public.auft_auftragposition
    ADD CONSTRAINT "fk_abpo$_auftragposition_i_id_rahmenposition_abpo$_i_id" FOREIGN KEY (auftragposition_i_id_rahmenposition) REFERENCES public.auft_auftragposition(i_id);
ALTER TABLE ONLY public.auft_auftragposition
    ADD CONSTRAINT "fk_abpo$_auftragpositionart_c_nr_abpo$art_c_nr" FOREIGN KEY (auftragpositionart_c_nr) REFERENCES public.auft_auftragpositionart(positionsart_c_nr);
ALTER TABLE ONLY public.auft_auftragposition
    ADD CONSTRAINT "fk_abpo$_auftragpositionstatus_c_nr_abpo$status_c_nr" FOREIGN KEY (auftragpositionstatus_c_nr) REFERENCES public.auft_auftragpositionstatus(status_c_nr);
ALTER TABLE ONLY public.auft_auftragposition
    ADD CONSTRAINT "fk_abpo$_mediastandard_i_id_lp_mediastandard_i_id" FOREIGN KEY (mediastandard_i_id) REFERENCES public.lp_mediastandard(i_id);
ALTER TABLE ONLY public.auft_auftragposition
    ADD CONSTRAINT "fk_abpo$_position_i_id_artikelset_abpo$_i_id" FOREIGN KEY (position_i_id_artikelset) REFERENCES public.auft_auftragposition(i_id);
ALTER TABLE ONLY public.auft_auftragposition
    ADD CONSTRAINT "fk_abpo$_position_i_id_zugehoerig_abpo$_i_id" FOREIGN KEY (position_i_id_zugehoerig) REFERENCES public.auft_auftragposition(i_id);
ALTER TABLE ONLY public.auft_auftragpositionart
    ADD CONSTRAINT "fk_abpo$art_positionsart_c_nr_lp_positionsart_c_nr" FOREIGN KEY (positionsart_c_nr) REFERENCES public.lp_positionsart(c_nr);
ALTER TABLE ONLY public.auft_auftragteilnehmer
    ADD CONSTRAINT "fk_abtn$_partner_i_id_auftragteilnehmer_papa$_i_id" FOREIGN KEY (partner_i_id_auftragteilnehmer) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.auft_auftragteilnehmer
    ADD CONSTRAINT "fk_abtn$_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.auft_auftragwiederholungsintervallspr
    ADD CONSTRAINT "fk_abws$_auftragwiederholungsintervall_c_nr_abwi$_c_nr" FOREIGN KEY (auftragwiederholungsintervall_c_nr) REFERENCES public.auft_auftragwiederholungsintervall(c_nr);
ALTER TABLE ONLY public.auft_auftragwiederholungsintervallspr
    ADD CONSTRAINT "fk_abws$_locale_c_nr_lp_locale_c_nr" FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.auft_zahlungsplanmeilenstein
    ADD CONSTRAINT "fk_abzp$_zahlungsplan_i_id_auft_zahlungsplan_i_id" FOREIGN KEY (zahlungsplan_i_id) REFERENCES public.auft_zahlungsplan(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT "fk_afaf$_anfrage_i_id_liefergruppenanfrage_afaf$_i_id" FOREIGN KEY (anfrage_i_id_liefergruppenanfrage) REFERENCES public.anf_anfrage(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT "fk_afaf$_anfrageerledigungsgrund_i_id_anf_afeg$_i_id" FOREIGN KEY (anfrageerledigungsgrund_i_id) REFERENCES public.anf_anfrageerledigungsgrund(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT "fk_afaf$_ansprechpartner_i_id_lieferant_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id_lieferant) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT "fk_afaf$_lieferant_i_id_anfrageadresse_palf$_i_id" FOREIGN KEY (lieferant_i_id_anfrageadresse) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.anf_anfrageposition
    ADD CONSTRAINT "fk_afap$_anfrageposition_i_id_zugehoerig_afap$_i_id" FOREIGN KEY (anfrageposition_i_id_zugehoerig) REFERENCES public.anf_anfrageposition(i_id);
ALTER TABLE ONLY public.anf_anfragepositionlieferdaten
    ADD CONSTRAINT "fk_afpl$_anfrageposition_i_id_afpo$_i_id" FOREIGN KEY (anfrageposition_i_id) REFERENCES public.anf_anfrageposition(i_id);
ALTER TABLE ONLY public.anf_anfragepositionlieferdaten
    ADD CONSTRAINT "fk_afpl$_zertifikatart_i_id_anf_zertifikatart_i_id" FOREIGN KEY (zertifikatart_i_id) REFERENCES public.anf_zertifikatart(i_id);
ALTER TABLE ONLY public.anf_anfrageposition
    ADD CONSTRAINT "fk_afpo$_anfragepositionart_c_nr_afpo$art_c_nr" FOREIGN KEY (anfragepositionart_c_nr) REFERENCES public.anf_anfragepositionart(positionsart_c_nr);
ALTER TABLE ONLY public.anf_anfragepositionart
    ADD CONSTRAINT "fk_afpo$art_positionsart_c_nr_lp_positionsart_c_nr" FOREIGN KEY (positionsart_c_nr) REFERENCES public.lp_positionsart(c_nr);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT "fk_agag$_angeboterledigungsgrund_c_nr_ageg$_c_nr" FOREIGN KEY (angeboterledigungsgrund_c_nr) REFERENCES public.angb_angeboterledigungsgrund(c_nr);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT "fk_agag$_ansprechpartner_i_id_kunde_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id_kunde) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT "fk_agag$_personal_i_id_manuellerledigt_pepe$_i_id" FOREIGN KEY (personal_i_id_manuellerledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.angb_angebotposition
    ADD CONSTRAINT "fk_agap$_kostentraeger_i_id_lp_kostentraeger_i_id" FOREIGN KEY (kostentraeger_i_id) REFERENCES public.lp_kostentraeger(i_id);
ALTER TABLE ONLY public.angb_angeboterledigungsgrundspr
    ADD CONSTRAINT "fk_ageg$spr_angeboterledigungsgrund_c_nr_ageg$_c_nr" FOREIGN KEY (angeboterledigungsgrund_c_nr) REFERENCES public.angb_angeboterledigungsgrund(c_nr);
ALTER TABLE ONLY public.angb_angebotposition
    ADD CONSTRAINT "fk_agpo$_angebotpositionart_c_nr_agpo$art_c_nr" FOREIGN KEY (angebotpositionart_c_nr) REFERENCES public.angb_angebotpositionart(positionsart_c_nr);
ALTER TABLE ONLY public.angb_angebotposition
    ADD CONSTRAINT "fk_agpo$_mediastandard_i_id_lp_mediastandard_i_id" FOREIGN KEY (mediastandard_i_id) REFERENCES public.lp_mediastandard(i_id);
ALTER TABLE ONLY public.angb_angebotpositionart
    ADD CONSTRAINT "fk_agpo$art_positionsart_c_nr_lp_positionsart_c_nr" FOREIGN KEY (positionsart_c_nr) REFERENCES public.lp_positionsart(c_nr);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_anfrageart_c_nr_anf_anfrageart_c_nr FOREIGN KEY (anfrageart_c_nr) REFERENCES public.anf_anfrageart(c_nr);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_anfragestatus_c_nr_anf_anfragestatus_c_nr FOREIGN KEY (anfragestatus_c_nr) REFERENCES public.anf_anfragestatus(status_c_nr);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_anfragetext_i_id_fusstext_anf_anfragetext_i_id FOREIGN KEY (anfragetext_i_id_fusstext) REFERENCES public.anf_anfragetext(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_anfragetext_i_id_kopftext_anf_anfragetext_i_id FOREIGN KEY (anfragetext_i_id_kopftext) REFERENCES public.anf_anfragetext(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT "fk_anf_anfrage_ansprechpartner_i_id_lieferadresse_paas$_i_id" FOREIGN KEY (ansprechpartner_i_id_lieferadresse) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_lfliefergruppe_i_id_part_lfliefergruppe_i_id FOREIGN KEY (lfliefergruppe_i_id) REFERENCES public.part_lfliefergruppe(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_lieferart_i_id_lp_lieferart_i_id FOREIGN KEY (lieferart_i_id) REFERENCES public.lp_lieferart(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_partner_i_id_lieferadresse_part_partner_i_id FOREIGN KEY (partner_i_id_lieferadresse) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_personal_i_id_anfrager_pers_personal_i_id FOREIGN KEY (personal_i_id_anfrager) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_personal_i_id_manuellerledigt_pers_personal_i_id FOREIGN KEY (personal_i_id_manuellerledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_personal_i_id_storniert_pers_personal_i_id FOREIGN KEY (personal_i_id_storniert) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_projekt_i_id_proj_projekt_i_id FOREIGN KEY (projekt_i_id) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_spediteur_i_id_lp_spediteur_i_id FOREIGN KEY (spediteur_i_id) REFERENCES public.lp_spediteur(i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_waehrung_c_nr_anfragewaehrung_lp_waehrung_c_nr FOREIGN KEY (waehrung_c_nr_anfragewaehrung) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT fk_anf_anfrage_zahlungsziel_i_id_lp_zahlungsziel_i_id FOREIGN KEY (zahlungsziel_i_id) REFERENCES public.lp_zahlungsziel(i_id);
ALTER TABLE ONLY public.anf_anfrageartspr
    ADD CONSTRAINT fk_anf_anfrageartspr_anfrageart_c_nr_anf_anfrageart_c_nr FOREIGN KEY (anfrageart_c_nr) REFERENCES public.anf_anfrageart(c_nr);
ALTER TABLE ONLY public.anf_anfrageartspr
    ADD CONSTRAINT fk_anf_anfrageartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.anf_anfrageerledigungsgrund
    ADD CONSTRAINT fk_anf_anfrageerledigungsgrund_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.anf_anfrageposition
    ADD CONSTRAINT fk_anf_anfrageposition_anfrage_i_id_anf_anfrage_i_id FOREIGN KEY (anfrage_i_id) REFERENCES public.anf_anfrage(i_id);
ALTER TABLE ONLY public.anf_anfrageposition
    ADD CONSTRAINT fk_anf_anfrageposition_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.anf_anfrageposition
    ADD CONSTRAINT fk_anf_anfrageposition_einheit_c_nr_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.anf_anfrageposition
    ADD CONSTRAINT "fk_anf_anfrageposition_loso$_i_id_fert_lossollmaterial_i_id" FOREIGN KEY (lossollmaterial_i_id) REFERENCES public.fert_lossollmaterial(i_id);
ALTER TABLE ONLY public.anf_anfrageposition
    ADD CONSTRAINT fk_anf_anfrageposition_mediastandard_i_id_lp_mediastandard_i_id FOREIGN KEY (mediastandard_i_id) REFERENCES public.lp_mediastandard(i_id);
ALTER TABLE ONLY public.anf_anfragestatus
    ADD CONSTRAINT fk_anf_anfragestatus_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.anf_anfragetext
    ADD CONSTRAINT fk_anf_anfragetext_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.anf_anfragetext
    ADD CONSTRAINT fk_anf_anfragetext_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.anf_anfragetext
    ADD CONSTRAINT fk_anf_anfragetext_mediaart_c_nr_lp_mediaart_c_nr FOREIGN KEY (mediaart_c_nr) REFERENCES public.lp_mediaart(c_nr);
ALTER TABLE ONLY public.anf_zertifikatart
    ADD CONSTRAINT fk_anf_zertifikatart_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.angb_akquisestatus
    ADD CONSTRAINT fk_angb_akquisestatus_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_akquisestatus_i_id_angb_akquisestatus_i_id FOREIGN KEY (akquisestatus_i_id) REFERENCES public.angb_akquisestatus(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_angebotart_c_nr_angb_angebotart_c_nr FOREIGN KEY (angebotart_c_nr) REFERENCES public.angb_angebotart(c_nr);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_angeboteinheit_c_nr_angb_angeboteinheit_c_nr FOREIGN KEY (angeboteinheit_c_nr) REFERENCES public.angb_angeboteinheit(einheit_c_nr);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_angebotstatus_c_nr_angb_angebotstatus_c_nr FOREIGN KEY (angebotstatus_c_nr) REFERENCES public.angb_angebotstatus(status_c_nr);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_angebottext_i_id_fusstext_angb_angebottext_i_id FOREIGN KEY (angebottext_i_id_fusstext) REFERENCES public.angb_angebottext(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_angebottext_i_id_kopftext_angb_angebottext_i_id FOREIGN KEY (angebottext_i_id_kopftext) REFERENCES public.angb_angebottext(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT "fk_angb_angebot_ansprechpartner_i_id_lieferadresse_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id_lieferadresse) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_kunde_i_id_angebotsadresse_part_kunde_i_id FOREIGN KEY (kunde_i_id_angebotsadresse) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_kunde_i_id_lieferadresse_part_kunde_i_id FOREIGN KEY (kunde_i_id_lieferadresse) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_lieferart_i_id_lp_lieferart_i_id FOREIGN KEY (lieferart_i_id) REFERENCES public.lp_lieferart(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_personal_i_id_storniert_pers_personal_i_id FOREIGN KEY (personal_i_id_storniert) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_personal_i_id_vertreter2_pers_personal_i_id FOREIGN KEY (personal_i_id_vertreter2) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_personal_i_id_vertreter_pers_personal_i_id FOREIGN KEY (personal_i_id_vertreter) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_projekt_i_id_proj_projekt_i_id FOREIGN KEY (projekt_i_id) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_spediteur_i_id_lp_spediteur_i_id FOREIGN KEY (spediteur_i_id) REFERENCES public.lp_spediteur(i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_waehrung_c_nr_angebotswaehrung_lp_waehrung_c_nr FOREIGN KEY (waehrung_c_nr_angebotswaehrung) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebot_zahlungsziel_i_id_lp_zahlungsziel_i_id FOREIGN KEY (zahlungsziel_i_id) REFERENCES public.lp_zahlungsziel(i_id);
ALTER TABLE ONLY public.angb_angebotartspr
    ADD CONSTRAINT fk_angb_angebotartspr_angebotart_c_nr_angb_angebotart_c_nr FOREIGN KEY (angebotart_c_nr) REFERENCES public.angb_angebotart(c_nr);
ALTER TABLE ONLY public.angb_angebotartspr
    ADD CONSTRAINT fk_angb_angebotartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.angb_angeboteinheit
    ADD CONSTRAINT fk_angb_angeboteinheit_einheit_c_nr_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.angb_angeboterledigungsgrund
    ADD CONSTRAINT fk_angb_angeboterledigungsgrund_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.angb_angeboterledigungsgrundspr
    ADD CONSTRAINT fk_angb_angeboterledigungsgrundspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT fk_angb_angebotn_kunde_i_id_rechnungsadresse_part_kunde_i_id FOREIGN KEY (kunde_i_id_rechnungsadresse) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.angb_angebotposition
    ADD CONSTRAINT fk_angb_angebotposition_agstkl_i_id_as_agstkl_i_id FOREIGN KEY (agstkl_i_id) REFERENCES public.as_agstkl(i_id);
ALTER TABLE ONLY public.angb_angebotposition
    ADD CONSTRAINT fk_angb_angebotposition_angebot_i_id_angb_angebot_i_id FOREIGN KEY (angebot_i_id) REFERENCES public.angb_angebot(i_id);
ALTER TABLE ONLY public.angb_angebotposition
    ADD CONSTRAINT fk_angb_angebotposition_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.angb_angebotposition
    ADD CONSTRAINT fk_angb_angebotposition_einheit_c_nr_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.angb_angebotposition
    ADD CONSTRAINT fk_angb_angebotposition_lieferant_i_id_part_lieferant_i_id FOREIGN KEY (lieferant_i_id) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.angb_angebotposition
    ADD CONSTRAINT fk_angb_angebotposition_mwstsatz_i_id_lp_mwstsatz_i_id FOREIGN KEY (mwstsatz_i_id) REFERENCES public.lp_mwstsatz(i_id);
ALTER TABLE ONLY public.angb_angebotposition
    ADD CONSTRAINT fk_angb_angebotposition_verleih_i_id_ww_verleih_i_id FOREIGN KEY (verleih_i_id) REFERENCES public.ww_verleih(i_id);
ALTER TABLE ONLY public.angb_angebotstatus
    ADD CONSTRAINT fk_angb_angebotstatus_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.angb_angebottext
    ADD CONSTRAINT fk_angb_angebottext_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.angb_angebottext
    ADD CONSTRAINT fk_angb_angebottext_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.angb_angebottext
    ADD CONSTRAINT fk_angb_angebottext_mediaart_c_nr_lp_mediaart_c_nr FOREIGN KEY (mediaart_c_nr) REFERENCES public.lp_mediaart(c_nr);
ALTER TABLE ONLY public.angb_angebotposition
    ADD CONSTRAINT "fk_anpo$_position_i_id_artikelset_anpo$_i_id" FOREIGN KEY (position_i_id_artikelset) REFERENCES public.angb_angebotposition(i_id);
ALTER TABLE ONLY public.angb_angebotposition
    ADD CONSTRAINT "fk_anpo$_position_i_id_zugehoerig_anpo$_i_id" FOREIGN KEY (position_i_id_zugehoerig) REFERENCES public.angb_angebotposition(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT "fk_arar$_partner_i_id_rechnungsadresse_papa$_i_id" FOREIGN KEY (partner_i_id_rechnungsadresse) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT "fk_arar$_personal_i_id_manuellerledigt_pepe$_i_id" FOREIGN KEY (personal_i_id_manuellerledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rech_rechnungkontierung
    ADD CONSTRAINT "fk_arar$kontierung_kostenstelle_i_id_lp_kostenstelle_i_id" FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.rech_rechnungkontierung
    ADD CONSTRAINT "fk_arar$kontierung_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rech_rechnungzahlung
    ADD CONSTRAINT "fk_arar$zahlung_bankverbindung_i_id_fbbv$_i_id" FOREIGN KEY (bankverbindung_i_id) REFERENCES public.fb_bankverbindung(i_id);
ALTER TABLE ONLY public.rech_rechnungzahlung
    ADD CONSTRAINT "fk_arar$zahlung_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rech_rechnungzahlung
    ADD CONSTRAINT "fk_arar$zahlung_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rech_rechnungzahlung
    ADD CONSTRAINT "fk_arar$zahlung_rechnung_i_id_gutschrift_arar$_i_id" FOREIGN KEY (rechnung_i_id_gutschrift) REFERENCES public.rech_rechnung(i_id);
ALTER TABLE ONLY public.rech_gutschriftpositionsart
    ADD CONSTRAINT "fk_argp$_positionsart_c_nr_lp_positionsart_c_nr" FOREIGN KEY (positionsart_c_nr) REFERENCES public.lp_positionsart(c_nr);
ALTER TABLE ONLY public.rech_rechnungposition
    ADD CONSTRAINT "fk_arpo$_auftragposition_i_id_abpo$_i_id" FOREIGN KEY (auftragposition_i_id) REFERENCES public.auft_auftragposition(i_id);
ALTER TABLE ONLY public.rech_rechnungposition
    ADD CONSTRAINT "fk_arpo$_mediastandard_i_id_lp_mediastandard_i_id" FOREIGN KEY (mediastandard_i_id) REFERENCES public.lp_mediastandard(i_id);
ALTER TABLE ONLY public.rech_rechnungposition
    ADD CONSTRAINT "fk_arpo$_rechnung_i_id_gutschrift_arar$_i_id" FOREIGN KEY (rechnung_i_id_gutschrift) REFERENCES public.rech_rechnung(i_id);
ALTER TABLE ONLY public.rech_rechnungposition
    ADD CONSTRAINT "fk_arpo$_rechnungposition_i_id_arpo$_i_id" FOREIGN KEY (rechnungposition_i_id) REFERENCES public.rech_rechnungposition(i_id);
ALTER TABLE ONLY public.rech_rechnungpositionsart
    ADD CONSTRAINT "fk_arpo$sart_positionsart_c_nr_lp_positionsart_c_nr" FOREIGN KEY (positionsart_c_nr) REFERENCES public.lp_positionsart(c_nr);
ALTER TABLE ONLY public.rech_proformarechnungpositionsart
    ADD CONSTRAINT "fk_arpp$_positionsart_c_nr_lp_positionsart_c_nr" FOREIGN KEY (positionsart_c_nr) REFERENCES public.lp_positionsart(c_nr);
ALTER TABLE ONLY public.as_agstkl
    ADD CONSTRAINT "fk_as_agstkl_ansprechpartner_i_id_kunde_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id_kunde) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.as_agstkl
    ADD CONSTRAINT fk_as_agstkl_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.as_agstkl
    ADD CONSTRAINT fk_as_agstkl_datenformat_c_nr_lp_datenformat_c_nr FOREIGN KEY (datenformat_c_nr) REFERENCES public.lp_datenformat(c_nr);
ALTER TABLE ONLY public.as_agstkl
    ADD CONSTRAINT fk_as_agstkl_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.as_agstkl
    ADD CONSTRAINT fk_as_agstkl_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.as_agstkl
    ADD CONSTRAINT fk_as_agstkl_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.as_agstkl
    ADD CONSTRAINT fk_as_agstkl_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.as_agstkl
    ADD CONSTRAINT fk_as_agstkl_projekt_i_id_proj_projekt_i_id FOREIGN KEY (projekt_i_id) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.as_agstkl
    ADD CONSTRAINT fk_as_agstkl_stueckliste_i_id_stk_stueckliste_i_id FOREIGN KEY (stueckliste_i_id) REFERENCES public.stk_stueckliste(i_id);
ALTER TABLE ONLY public.as_agstkl
    ADD CONSTRAINT fk_as_agstkl_waehrung_c_nr_lp_waehrung_c_nr FOREIGN KEY (waehrung_c_nr) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.as_agstklarbeitsplan
    ADD CONSTRAINT fk_as_agstklarbeitsplan_agart_c_nr_stk_agart_c_nr FOREIGN KEY (agart_c_nr) REFERENCES public.stk_agart(c_nr);
ALTER TABLE ONLY public.as_agstklarbeitsplan
    ADD CONSTRAINT fk_as_agstklarbeitsplan_agstkl_i_id_as_agstkl_i_id FOREIGN KEY (agstkl_i_id) REFERENCES public.as_agstkl(i_id);
ALTER TABLE ONLY public.as_agstklarbeitsplan
    ADD CONSTRAINT fk_as_agstklarbeitsplan_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.as_agstklarbeitsplan
    ADD CONSTRAINT fk_as_agstklarbeitsplan_maschine_i_id_pers_maschine_i_id FOREIGN KEY (maschine_i_id) REFERENCES public.pers_maschine(i_id);
ALTER TABLE ONLY public.as_agstklaufschlag
    ADD CONSTRAINT fk_as_agstklaufschlag_agstkl_i_id_as_agstkl_i_id FOREIGN KEY (agstkl_i_id) REFERENCES public.as_agstkl(i_id);
ALTER TABLE ONLY public.as_agstklaufschlag
    ADD CONSTRAINT fk_as_agstklaufschlag_aufschlag_i_id_as_aufschlag_i_id FOREIGN KEY (aufschlag_i_id) REFERENCES public.as_aufschlag(i_id);
ALTER TABLE ONLY public.as_agstklmaterial
    ADD CONSTRAINT fk_as_agstklmaterial_agstkl_i_id_as_agstkl_i_id FOREIGN KEY (agstkl_i_id) REFERENCES public.as_agstkl(i_id);
ALTER TABLE ONLY public.as_agstklmaterial
    ADD CONSTRAINT fk_as_agstklmaterial_material_i_id_ww_material_i_id FOREIGN KEY (material_i_id) REFERENCES public.ww_material(i_id);
ALTER TABLE ONLY public.as_agstklmengenstaffel
    ADD CONSTRAINT fk_as_agstklmengenstaffel_agstkl_i_id_as_agstkl_i_id FOREIGN KEY (agstkl_i_id) REFERENCES public.as_agstkl(i_id);
ALTER TABLE ONLY public.as_agstklmengenstaffel
    ADD CONSTRAINT "fk_as_agstklmengenstaffel_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.as_agstklmengenstaffel_schnellerfassung
    ADD CONSTRAINT fk_as_agstklmengenstaffel_schnellerfassung_agstkl_i_id_as_agstk FOREIGN KEY (agstkl_i_id) REFERENCES public.as_agstkl(i_id);
ALTER TABLE ONLY public.as_agstklposition
    ADD CONSTRAINT fk_as_agstklposition_agstkl_i_id_as_agstkl_i_id FOREIGN KEY (agstkl_i_id) REFERENCES public.as_agstkl(i_id);
ALTER TABLE ONLY public.as_agstklposition
    ADD CONSTRAINT fk_as_agstklposition_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.as_agstklposition
    ADD CONSTRAINT fk_as_agstklposition_einheit_c_nr_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.as_agstklpositionsart
    ADD CONSTRAINT fk_as_agstklpositionsart_positionsart_c_nr_lp_positionsart_c_nr FOREIGN KEY (positionsart_c_nr) REFERENCES public.lp_positionsart(c_nr);
ALTER TABLE ONLY public.as_aufschlag
    ADD CONSTRAINT fk_as_aufschlag_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.as_einkaufsangebot
    ADD CONSTRAINT fk_as_einkaufsangebot_ansprechpartner_i_id_part_ansprechpartner FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.as_einkaufsangebot
    ADD CONSTRAINT fk_as_einkaufsangebot_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.as_einkaufsangebot
    ADD CONSTRAINT fk_as_einkaufsangebot_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.as_einkaufsangebot
    ADD CONSTRAINT "fk_as_einkaufsangebot_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.as_einkaufsangebot
    ADD CONSTRAINT "fk_as_einkaufsangebot_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.as_einkaufsangebotposition
    ADD CONSTRAINT fk_as_einkaufsangebotposition_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.as_einkaufsangebotposition
    ADD CONSTRAINT fk_as_einkaufsangebotposition_einheit_c_nr_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.as_einkaufsangebotposition
    ADD CONSTRAINT fk_as_ekag_positionlieferant_i_id_uebersteuert_menge1 FOREIGN KEY (positionlieferant_i_id_uebersteuert_menge1) REFERENCES public.as_positionlieferant(i_id);
ALTER TABLE ONLY public.as_einkaufsangebotposition
    ADD CONSTRAINT fk_as_ekag_positionlieferant_i_id_uebersteuert_menge2 FOREIGN KEY (positionlieferant_i_id_uebersteuert_menge2) REFERENCES public.as_positionlieferant(i_id);
ALTER TABLE ONLY public.as_einkaufsangebotposition
    ADD CONSTRAINT fk_as_ekag_positionlieferant_i_id_uebersteuert_menge3 FOREIGN KEY (positionlieferant_i_id_uebersteuert_menge3) REFERENCES public.as_positionlieferant(i_id);
ALTER TABLE ONLY public.as_einkaufsangebotposition
    ADD CONSTRAINT fk_as_ekag_positionlieferant_i_id_uebersteuert_menge4 FOREIGN KEY (positionlieferant_i_id_uebersteuert_menge4) REFERENCES public.as_positionlieferant(i_id);
ALTER TABLE ONLY public.as_einkaufsangebotposition
    ADD CONSTRAINT fk_as_ekag_positionlieferant_i_id_uebersteuert_menge5 FOREIGN KEY (positionlieferant_i_id_uebersteuert_menge5) REFERENCES public.as_positionlieferant(i_id);
ALTER TABLE ONLY public.as_ekaglieferant
    ADD CONSTRAINT "fk_as_ekaglieferant_ansprechpartner_i_id_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.as_ekaglieferant
    ADD CONSTRAINT fk_as_ekaglieferant_einkaufsangebot_i_id_as_einkaufsangebot_i_i FOREIGN KEY (einkaufsangebot_i_id) REFERENCES public.as_einkaufsangebot(i_id);
ALTER TABLE ONLY public.as_ekaglieferant
    ADD CONSTRAINT fk_as_ekaglieferant_lieferant_i_id_part_lieferant_i_id FOREIGN KEY (lieferant_i_id) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.as_ekaglieferant
    ADD CONSTRAINT fk_as_ekaglieferant_waehrung_c_nr_lp_waehrung_c_nr FOREIGN KEY (waehrung_c_nr) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.as_ekgruppelieferant
    ADD CONSTRAINT "fk_as_ekgruppe_ansprechpartner_i_id_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.as_ekgruppe
    ADD CONSTRAINT fk_as_ekgruppe_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.as_ekgruppelieferant
    ADD CONSTRAINT fk_as_ekgruppelieferant_ekgruppe_i_id_as_ekgruppe_i_id FOREIGN KEY (ekgruppe_i_id) REFERENCES public.as_ekgruppe(i_id);
ALTER TABLE ONLY public.as_ekgruppelieferant
    ADD CONSTRAINT fk_as_ekgruppelieferant_lieferant_i_id_part_lieferant_i_id FOREIGN KEY (lieferant_i_id) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.as_ekweblieferant
    ADD CONSTRAINT fk_as_ekweblieferant_einkaufsangebot_i_id_as_einkaufsangebot_i_ FOREIGN KEY (einkaufsangebot_i_id) REFERENCES public.as_einkaufsangebot(i_id);
ALTER TABLE ONLY public.as_ekweblieferant
    ADD CONSTRAINT fk_as_ekweblieferant_webpartner_i_id_as_webpartner_i_id FOREIGN KEY (webpartner_i_id) REFERENCES public.as_webpartner(i_id);
ALTER TABLE ONLY public.as_positionlieferant
    ADD CONSTRAINT fk_as_positionlieferant_einkaufsangebotposition_i_id_as_einkauf FOREIGN KEY (einkaufsangebotposition_i_id) REFERENCES public.as_einkaufsangebotposition(i_id);
ALTER TABLE ONLY public.as_positionlieferant
    ADD CONSTRAINT fk_as_positionlieferant_ekaglieferant_i_id_as_ekaglieferant_i_i FOREIGN KEY (ekaglieferant_i_id) REFERENCES public.as_ekaglieferant(i_id);
ALTER TABLE ONLY public.as_webfindchips
    ADD CONSTRAINT fk_as_webfindchips_webpartner_i_id_as_webpartner_i_id FOREIGN KEY (webpartner_i_id) REFERENCES public.as_webpartner(i_id);
ALTER TABLE ONLY public.as_weblieferant
    ADD CONSTRAINT fk_as_weblieferant_webpartner_i_id_as_webpartner_i_id FOREIGN KEY (webpartner_i_id) REFERENCES public.as_webpartner(i_id);
ALTER TABLE ONLY public.as_webpartner
    ADD CONSTRAINT fk_as_webpartner_lieferant_i_id_part_lieferant_i_id FOREIGN KEY (lieferant_i_id) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.as_webpartner
    ADD CONSTRAINT fk_as_webpartner_webabfrage_i_id_as_webabfrage_i_id FOREIGN KEY (webabfrage_i_id) REFERENCES public.as_webabfrage(i_id);
ALTER TABLE ONLY public.as_einkaufsangebotposition
    ADD CONSTRAINT "fk_as_x_agstklpositionsart_c_nr_aspo$sart_c_nr" FOREIGN KEY (agstklpositionsart_c_nr) REFERENCES public.as_agstklpositionsart(positionsart_c_nr);
ALTER TABLE ONLY public.as_einkaufsangebotposition
    ADD CONSTRAINT fk_as_x_einkaufsangebot_i_id_as_einkaufsangebot_i_id FOREIGN KEY (einkaufsangebot_i_id) REFERENCES public.as_einkaufsangebot(i_id);
ALTER TABLE ONLY public.as_einkaufsangebotposition
    ADD CONSTRAINT "fk_asep$_hersteller_i_id_ww_hersteller_i_id" FOREIGN KEY (hersteller_i_id) REFERENCES public.ww_hersteller(i_id);
ALTER TABLE ONLY public.as_einkaufsangebotposition
    ADD CONSTRAINT "fk_asep$_lieferant_i_id_part_lieferant_i_id" FOREIGN KEY (lieferant_i_id) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.as_agstklposition
    ADD CONSTRAINT "fk_aspo$_agstklpositionsart_c_nr_aspo$sart_c_nr" FOREIGN KEY (agstklpositionsart_c_nr) REFERENCES public.as_agstklpositionsart(positionsart_c_nr);
ALTER TABLE ONLY public.auft_auftragauftragdokument
    ADD CONSTRAINT "fk_audo$_auftrag_i_id_auft_auftrag_i_id" FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.auft_auftragauftragdokument
    ADD CONSTRAINT "fk_audo$_auftragdokument_i_id_audo$_i_id" FOREIGN KEY (auftragdokument_i_id) REFERENCES public.auft_auftragdokument(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_angebot_i_id_angb_angebot_i_id FOREIGN KEY (angebot_i_id) REFERENCES public.angb_angebot(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT "fk_auft_auftrag_ansprechpartner_i_id_lieferadresse_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id_lieferadresse) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_auftrag_i_id_rahmenauftrag_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id_rahmenauftrag) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_auftragart_c_nr_auft_auftragart_c_nr FOREIGN KEY (auftragart_c_nr) REFERENCES public.auft_auftragart(c_nr);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_auftragstatus_c_nr_auft_auftragstatus_c_nr FOREIGN KEY (auftragstatus_c_nr) REFERENCES public.auft_auftragstatus(status_c_nr);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_auftragtext_i_id_fusstext_auft_auftragtext_i_id FOREIGN KEY (auftragtext_i_id_fusstext) REFERENCES public.auft_auftragtext(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_auftragtext_i_id_kopftext_auft_auftragtext_i_id FOREIGN KEY (auftragtext_i_id_kopftext) REFERENCES public.auft_auftragtext(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_bestellung_i_id_bes_bestellung_i_id_anderermand FOREIGN KEY (bestellung_i_id_anderermandant) REFERENCES public.bes_bestellung(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_kunde_i_id_auftragsadresse_part_kunde_i_id FOREIGN KEY (kunde_i_id_auftragsadresse) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_kunde_i_id_lieferadresse_part_kunde_i_id FOREIGN KEY (kunde_i_id_lieferadresse) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_kunde_i_id_rechnungsadresse_part_kunde_i_id FOREIGN KEY (kunde_i_id_rechnungsadresse) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_laenderart_c_nr_fb_laenderart_c_nr FOREIGN KEY (laenderart_c_nr) REFERENCES public.fb_laenderart(c_nr);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_lager_i_id_abbuchungslager_ww_lager_i_id FOREIGN KEY (lager_i_id_abbuchungslager) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_lieferart_i_id_lp_lieferart_i_id FOREIGN KEY (lieferart_i_id) REFERENCES public.lp_lieferart(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT "fk_auft_auftrag_personal_i_id_auftragsfreigabe_pep$_i_id" FOREIGN KEY (personal_i_id_auftragsfreigabe) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT "fk_auft_auftrag_personal_i_id_begruendung_pepe$_i_id" FOREIGN KEY (personal_i_id_begruendung) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_personal_i_id_erledigt_pers_personal_i_id FOREIGN KEY (personal_i_id_erledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT "fk_auft_auftrag_personal_i_id_freigabe_pep$_i_id" FOREIGN KEY (personal_i_id_freigabe) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_personal_i_id_storniert_pers_personal_i_id FOREIGN KEY (personal_i_id_storniert) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT "fk_auft_auftrag_personal_i_id_verrechenbar_pepe$_i_id" FOREIGN KEY (personal_i_id_verrechenbar) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_personal_i_id_vertreter2_pers_personal_i_id FOREIGN KEY (personal_i_id_vertreter2) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_personal_i_id_vertreter_pers_personal_i_id FOREIGN KEY (personal_i_id_vertreter) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_projekt_i_id_proj_projekt_i_id FOREIGN KEY (projekt_i_id) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_spediteur_i_id_lp_spediteur_i_id FOREIGN KEY (spediteur_i_id) REFERENCES public.lp_spediteur(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_verrechenbar_i_id_auft_verrechenbar_i_id FOREIGN KEY (verrechenbar_i_id) REFERENCES public.auft_verrechenbar(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_verrechnungsmodell_i_id_rech_verrechnungsmodell FOREIGN KEY (verrechnungsmodell_i_id) REFERENCES public.rech_verrechnungsmodell(i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_waehrung_c_nr_auftragswaehrung_lp_waehrung_c_nr FOREIGN KEY (waehrung_c_nr_auftragswaehrung) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT fk_auft_auftrag_zahlungsziel_i_id_lp_zahlungsziel_i_id FOREIGN KEY (zahlungsziel_i_id) REFERENCES public.lp_zahlungsziel(i_id);
ALTER TABLE ONLY public.auft_auftragartspr
    ADD CONSTRAINT fk_auft_auftragartspr_auftragart_c_nr_auft_auftragart_c_nr FOREIGN KEY (auftragart_c_nr) REFERENCES public.auft_auftragart(c_nr);
ALTER TABLE ONLY public.auft_auftragartspr
    ADD CONSTRAINT fk_auft_auftragartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.auft_auftragkostenstelle
    ADD CONSTRAINT fk_auft_auftragkostenstelle_auftrag_i_id_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.auft_auftragkostenstelle
    ADD CONSTRAINT "fk_auft_auftragkostenstelle_kostenstelle_i_id_$lpks_i_id" FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.auft_auftragposition
    ADD CONSTRAINT fk_auft_auftragposition_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.auft_auftragposition
    ADD CONSTRAINT fk_auft_auftragposition_auftrag_i_id_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.auft_auftragposition
    ADD CONSTRAINT "fk_auft_auftragposition_bestellposition_i_id_bepo$_i_id" FOREIGN KEY (bestellposition_i_id) REFERENCES public.bes_bestellposition(i_id);
ALTER TABLE ONLY public.auft_auftragposition
    ADD CONSTRAINT fk_auft_auftragposition_einheit_c_nr_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.auft_auftragposition
    ADD CONSTRAINT fk_auft_auftragposition_lieferant_i_id_part_lieferant_i_id FOREIGN KEY (lieferant_i_id) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.auft_auftragposition
    ADD CONSTRAINT fk_auft_auftragposition_mwstsatz_i_id_lp_mwstsatz_i_id FOREIGN KEY (mwstsatz_i_id) REFERENCES public.lp_mwstsatz(i_id);
ALTER TABLE ONLY public.auft_auftragposition
    ADD CONSTRAINT fk_auft_auftragposition_verleih_i_id_ww_verleih_i_id FOREIGN KEY (verleih_i_id) REFERENCES public.ww_verleih(i_id);
ALTER TABLE ONLY public.auft_auftragpositionstatus
    ADD CONSTRAINT fk_auft_auftragpositionstatus_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.auft_auftragseriennrn
    ADD CONSTRAINT fk_auft_auftragseriennrn_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.auft_auftragseriennrn
    ADD CONSTRAINT fk_auft_auftragseriennrn_auftragposition_i_id_auft_auftragposit FOREIGN KEY (auftragposition_i_id) REFERENCES public.auft_auftragposition(i_id);
ALTER TABLE ONLY public.auft_auftragstatus
    ADD CONSTRAINT fk_auft_auftragstatus_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.auft_auftragteilnehmer
    ADD CONSTRAINT fk_auft_auftragteilnehmer_auftrag_i_id_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.auft_auftragteilnehmer
    ADD CONSTRAINT fk_auft_auftragteilnehmer_funktion_i_id_lp_funktion_i_id FOREIGN KEY (funktion_i_id) REFERENCES public.lp_funktion(i_id);
ALTER TABLE ONLY public.auft_auftragteilnehmer
    ADD CONSTRAINT fk_auft_auftragteilnehmer_kostenstelle_i_id_lp_kostenstelle_i_i FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.auft_auftragtext
    ADD CONSTRAINT fk_auft_auftragtext_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.auft_auftragtext
    ADD CONSTRAINT fk_auft_auftragtext_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.auft_auftragtext
    ADD CONSTRAINT fk_auft_auftragtext_mediaart_c_nr_lp_mediaart_c_nr FOREIGN KEY (mediaart_c_nr) REFERENCES public.lp_mediaart(c_nr);
ALTER TABLE ONLY public.auft_indexanpassung_log
    ADD CONSTRAINT fk_auft_indexanpassung_log_auftrag_i_id_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.auft_indexanpassung_log
    ADD CONSTRAINT fk_auft_indexanpassung_log_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.auft_meilenstein
    ADD CONSTRAINT fk_auft_meilenstein_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.auft_meilensteinspr
    ADD CONSTRAINT fk_auft_meilensteinspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.auft_meilensteinspr
    ADD CONSTRAINT fk_auft_meilensteinspr_meilenstein_i_id_auft_meilenstein_i_id FOREIGN KEY (meilenstein_i_id) REFERENCES public.auft_meilenstein(i_id);
ALTER TABLE ONLY public.auft_zahlungsplan
    ADD CONSTRAINT fk_auft_zahlungsplan_auftrag_i_id_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.auft_zeitplan
    ADD CONSTRAINT fk_auft_zeitplan_auftrag_i_id_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.auft_zeitplan
    ADD CONSTRAINT "fk_auft_zeitplan_personal_i_id_dauer_erledigt_pep$_i_id" FOREIGN KEY (personal_i_id_dauer_erledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.auft_zeitplan
    ADD CONSTRAINT "fk_auft_zeitplan_personal_i_id_material_erledigt_pep$_i_id" FOREIGN KEY (personal_i_id_material_erledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.auft_zeitplantyp
    ADD CONSTRAINT fk_auft_zeitplantyp_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.auft_zeitplantypdetail
    ADD CONSTRAINT fk_auftzeitplantypdetail_zeitplantyp_i_id_auft_zeitplantyp_i_id FOREIGN KEY (zeitplantyp_i_id) REFERENCES public.auft_zeitplantyp(i_id);
ALTER TABLE ONLY public.auft_auftragposition
    ADD CONSTRAINT "fk_aupo$_kostentraeger_i_id_lp_kostentraeger_i_id" FOREIGN KEY (kostentraeger_i_id) REFERENCES public.lp_kostentraeger(i_id);
ALTER TABLE ONLY public.auto_arbeitszeitstatus
    ADD CONSTRAINT fk_auto_arbeitszeitstatus_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.auto_artikellieferant_webabfrage
    ADD CONSTRAINT fk_auto_artikellieferant_webabfrage_mandant_c_nr_lp_mandant_c_n FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.auto_bedarfsuebernahmeoffenjournal
    ADD CONSTRAINT fk_auto_bedarfsuebernahmeoffenjournal_mandant_c_nr_lp_mandant_c FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.auto_er_import
    ADD CONSTRAINT fk_auto_er_import_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.auto_kpireport
    ADD CONSTRAINT fk_auto_kpireport_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.auto_shopsync_item
    ADD CONSTRAINT fk_auto_shopsync_item_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.auto_shopsync_item
    ADD CONSTRAINT fk_auto_shopsync_item_webshop_id_ww_webshop_i_id FOREIGN KEY (webshop_i_id) REFERENCES public.ww_webshop(i_id);
ALTER TABLE ONLY public.auto_shopsync_order
    ADD CONSTRAINT fk_auto_shopsync_order_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.auto_shopsync_order
    ADD CONSTRAINT fk_auto_shopsync_order_webshop_id_ww_webshop_i_id FOREIGN KEY (webshop_i_id) REFERENCES public.ww_webshop(i_id);
ALTER TABLE ONLY public.auto_wejournal
    ADD CONSTRAINT fk_auto_wejournal_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fert_bedarfsuebernahme
    ADD CONSTRAINT "fk_bdum$_lossollmaterial_i_id_fert_lossollmaterial_i_id" FOREIGN KEY (lossollmaterial_i_id) REFERENCES public.fert_lossollmaterial(i_id);
ALTER TABLE ONLY public.fert_bedarfsuebernahme
    ADD CONSTRAINT "fk_bdum$_personal_i_id_aendern_pers_personal_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_bedarfsuebernahme
    ADD CONSTRAINT "fk_bdum$_personal_i_id_anlegen_pers_personal_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_bedarfsuebernahme
    ADD CONSTRAINT "fk_bdum$_personal_i_id_verbucht_gedruckt_pers_personal_i_id" FOREIGN KEY (personal_i_id_verbucht_gedruckt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bestellposition
    ADD CONSTRAINT "fk_bepo$_personal_i_id_lieferterminbestaetigt_pepe$_i_id" FOREIGN KEY (personal_i_id_lieferterminbestaetigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bestellposition
    ADD CONSTRAINT "fk_bepo$_position_i_id_artikelset_bepo$_i_id" FOREIGN KEY (position_i_id_artikelset) REFERENCES public.bes_bestellposition(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_ansprechpartner_i_id_lieferadresse_ansprechpartner_i_id FOREIGN KEY (ansprechpartner_i_id_lieferadresse) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.bes_bestellposition
    ADD CONSTRAINT fk_bes_bestellposition_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.bes_bestellposition
    ADD CONSTRAINT fk_bes_bestellposition_bestellung_i_id_bes_bestellung_i_id FOREIGN KEY (bestellung_i_id) REFERENCES public.bes_bestellung(i_id);
ALTER TABLE ONLY public.bes_bestellposition
    ADD CONSTRAINT fk_bes_bestellposition_einheit_c_nr_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.bes_bestellposition
    ADD CONSTRAINT fk_bes_bestellposition_gebinde_i_id_ww_gebinde_i_id FOREIGN KEY (gebinde_i_id) REFERENCES public.ww_gebinde(i_id);
ALTER TABLE ONLY public.bes_bestellposition
    ADD CONSTRAINT fk_bes_bestellposition_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.bes_bestellposition
    ADD CONSTRAINT "fk_bes_bestellposition_loso$_i_id_fert_lossollmaterial_i_id" FOREIGN KEY (lossollmaterial_i_id) REFERENCES public.fert_lossollmaterial(i_id);
ALTER TABLE ONLY public.bes_bestellposition
    ADD CONSTRAINT fk_bes_bestellposition_mediastandard_i_id_lp_mediastandard_i_id FOREIGN KEY (mediastandard_i_id) REFERENCES public.lp_mediastandard(i_id);
ALTER TABLE ONLY public.bes_bestellposition
    ADD CONSTRAINT fk_bes_bestellposition_mwstsatz_i_id_lp_mwstsatz_i_id FOREIGN KEY (mwstsatz_i_id) REFERENCES public.lp_mwstsatz(i_id);
ALTER TABLE ONLY public.bes_bestellpositionstatus
    ADD CONSTRAINT fk_bes_bestellpositionstatus_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_anfrage_i_id_anf_anfrage_i_id FOREIGN KEY (anfrage_i_id) REFERENCES public.anf_anfrage(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_auftrag_i_id_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_bestelltext_i_id_fusstext__i_id FOREIGN KEY (bestellungtext_i_id_fusstext) REFERENCES public.bes_bestellungtext(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_bestelltext_i_id_kopftext__i_id FOREIGN KEY (bestellungtext_i_id_kopftext) REFERENCES public.bes_bestellungtext(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_bestellungart_c_nr_bes_bestellungart_c_nr FOREIGN KEY (bestellungart_c_nr) REFERENCES public.bes_bestellungart(c_nr);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_lieferart_i_id_lp_lieferart_i_id FOREIGN KEY (lieferart_i_id) REFERENCES public.lp_lieferart(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_partner_i_id_lieferadresse_part_partner_i_id FOREIGN KEY (partner_i_id_lieferadresse) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_personal_i_id_anforderer_pers_personal_i_id FOREIGN KEY (personal_i_id_anforderer) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_personal_i_id_interneranforderer_pers_persona FOREIGN KEY (personal_i_id_interneranforderer) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_personal_i_id_storniert_pers_personal_i_id FOREIGN KEY (personal_i_id_storniert) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_projekt_i_id_proj_projekt_i_id FOREIGN KEY (projekt_i_id) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_spediteur_i_id_lp_spediteur_i_id FOREIGN KEY (spediteur_i_id) REFERENCES public.lp_spediteur(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT fk_bes_bestellung_zahlungsziel_i_id_lp_zahlungsziel_i_id FOREIGN KEY (zahlungsziel_i_id) REFERENCES public.lp_zahlungsziel(i_id);
ALTER TABLE ONLY public.bes_bestellungartspr
    ADD CONSTRAINT fk_bes_bestellungartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.bes_bestellungstatus
    ADD CONSTRAINT fk_bes_bestellungstatus_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.bes_bestellungtext
    ADD CONSTRAINT fk_bes_bestellungtext_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.bes_bestellungtext
    ADD CONSTRAINT fk_bes_bestellungtext_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.bes_bestellvorschlag
    ADD CONSTRAINT fk_bes_bestellvorschlag_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.bes_bestellvorschlag
    ADD CONSTRAINT fk_bes_bestellvorschlag_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.bes_bestellvorschlag
    ADD CONSTRAINT fk_bes_bestellvorschlag_gebinde_i_id_ww_gebinde_i_id FOREIGN KEY (gebinde_i_id) REFERENCES public.ww_gebinde(i_id);
ALTER TABLE ONLY public.bes_bestellvorschlag
    ADD CONSTRAINT fk_bes_bestellvorschlag_lieferant_i_id_part_lieferant_i_id FOREIGN KEY (lieferant_i_id) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.bes_bestellvorschlag
    ADD CONSTRAINT fk_bes_bestellvorschlag_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.bes_bestellvorschlag
    ADD CONSTRAINT fk_bes_bestellvorschlag_partner_i_id_standort_part_partner_i_id FOREIGN KEY (partner_i_id_standort) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.bes_bestellvorschlag
    ADD CONSTRAINT fk_bes_bestellvorschlag_personal_i_id_bearbeitet_pers_personal_ FOREIGN KEY (personal_i_id_bearbeitet) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bestellvorschlag
    ADD CONSTRAINT fk_bes_bestellvorschlag_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bestellvorschlag
    ADD CONSTRAINT "fk_bes_bestellvorschlag_personal_i_id_vormerkung_pep$_i_id" FOREIGN KEY (personal_i_id_vormerkung) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bestellvorschlag
    ADD CONSTRAINT fk_bes_bestellvorschlag_projekt_i_id_proj_projekt_i_id FOREIGN KEY (projekt_i_id) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.bes_bsmahnlauf
    ADD CONSTRAINT fk_bes_bsmahnlauf_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.bes_bsmahnlauf
    ADD CONSTRAINT fk_bes_bsmahnlauf_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bsmahnlauf
    ADD CONSTRAINT fk_bes_bsmahnlauf_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bsmahnstufe
    ADD CONSTRAINT fk_bes_bsmahnstufe_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.bes_bsmahntext
    ADD CONSTRAINT fk_bes_bsmahntext_bes_bsmahnstufe FOREIGN KEY (bsmahnstufe_i_id, mandant_c_nr) REFERENCES public.bes_bsmahnstufe(i_id, mandant_c_nr);
ALTER TABLE ONLY public.bes_bsmahntext
    ADD CONSTRAINT fk_bes_bsmahntext_bsmahnstufe_i_id_bes_bsmahnstufe_i_id FOREIGN KEY (bsmahnstufe_i_id, mandant_c_nr) REFERENCES public.bes_bsmahnstufe(i_id, mandant_c_nr);
ALTER TABLE ONLY public.bes_bsmahntext
    ADD CONSTRAINT fk_bes_bsmahntext_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.bes_bsmahntext
    ADD CONSTRAINT fk_bes_bsmahntext_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.bes_bsmahnung
    ADD CONSTRAINT fk_bes_bsmahnung_bestellposition_i_id_bes_bestellposition_i_id FOREIGN KEY (bestellposition_i_id) REFERENCES public.bes_bestellposition(i_id);
ALTER TABLE ONLY public.bes_bsmahnung
    ADD CONSTRAINT fk_bes_bsmahnung_bestellung_i_id_bes_bestellung_i_id FOREIGN KEY (bestellung_i_id) REFERENCES public.bes_bestellung(i_id);
ALTER TABLE ONLY public.bes_bsmahnung
    ADD CONSTRAINT fk_bes_bsmahnung_bsmahnlauf_i_id_bes_bsmahnlauf_i_id FOREIGN KEY (bsmahnlauf_i_id) REFERENCES public.bes_bsmahnlauf(i_id);
ALTER TABLE ONLY public.bes_bsmahnung
    ADD CONSTRAINT fk_bes_bsmahnung_bsmahnstufe_i_id_bes_bsmahnstufe_i_id FOREIGN KEY (bsmahnstufe_i_id, mandant_c_nr) REFERENCES public.bes_bsmahnstufe(i_id, mandant_c_nr);
ALTER TABLE ONLY public.bes_bsmahnung
    ADD CONSTRAINT fk_bes_bsmahnung_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.bes_bsmahnung
    ADD CONSTRAINT fk_bes_bsmahnung_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bsmahnung
    ADD CONSTRAINT fk_bes_bsmahnung_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bsmahnung
    ADD CONSTRAINT fk_bes_bsmahnung_personal_i_id_gedruckt_pers_personal_i_id FOREIGN KEY (personal_i_id_gedruckt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bszahlungsplan
    ADD CONSTRAINT fk_bes_bszahlungsplan_bestellung_i_id_bes_bestellung_i_id FOREIGN KEY (bestellung_i_id) REFERENCES public.bes_bestellung(i_id);
ALTER TABLE ONLY public.bes_bszahlungsplan
    ADD CONSTRAINT "fk_bes_bszahlungsplan_personal_i_id_erledigt_pep$_i_id" FOREIGN KEY (personal_i_id_erledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_mahngruppe
    ADD CONSTRAINT fk_bes_mahngruppe_artgru_i_id_ww_artgru_i_id FOREIGN KEY (artgru_i_id) REFERENCES public.ww_artgru(i_id);
ALTER TABLE ONLY public.bes_wareneingang
    ADD CONSTRAINT fk_bes_wareneingang_bestellung_i_id_bes_bestellung_i_id FOREIGN KEY (bestellung_i_id) REFERENCES public.bes_bestellung(i_id);
ALTER TABLE ONLY public.bes_wareneingang
    ADD CONSTRAINT "fk_bes_wareneingang_eingangsrechnung_i_id_erer$_i_id" FOREIGN KEY (eingangsrechnung_i_id) REFERENCES public.er_eingangsrechnung(i_id);
ALTER TABLE ONLY public.bes_wareneingang
    ADD CONSTRAINT fk_bes_wareneingang_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT "fk_bsbs$_ansprechpartner_i_id_abholadresse_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id_abholadresse) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT "fk_bsbs$_ansprechpartner_i_id_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT "fk_bsbs$_bestellung_i_id_rahmenbestellung_bsbs$_i_id" FOREIGN KEY (bestellung_i_id_rahmenbestellung) REFERENCES public.bes_bestellung(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT "fk_bsbs$_bestellungstatus_c_nr_bsbs$status_c_nr" FOREIGN KEY (bestellungstatus_c_nr) REFERENCES public.bes_bestellungstatus(status_c_nr);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT "fk_bsbs$_bestellungtext_i_id_fusstext_bsbs$text_i_id" FOREIGN KEY (bestellungtext_i_id_fusstext) REFERENCES public.bes_bestellungtext(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT "fk_bsbs$_bestellungtext_i_id_kopftext_bsbs$text_i_id" FOREIGN KEY (bestellungtext_i_id_kopftext) REFERENCES public.bes_bestellungtext(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT "fk_bsbs$_lieferant_i_id_bestelladresse_palf$_i_id" FOREIGN KEY (lieferant_i_id_bestelladresse) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT "fk_bsbs$_lieferant_i_id_bestelladresse_part_lieferant_i_id" FOREIGN KEY (lieferant_i_id_bestelladresse) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT "fk_bsbs$_lieferant_i_id_rechnungsadresse_palf$_i_id" FOREIGN KEY (lieferant_i_id_rechnungsadresse) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT "fk_bsbs$_lieferant_i_id_rechnungsadresse_part_lieferant_i_id" FOREIGN KEY (lieferant_i_id_rechnungsadresse) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT "fk_bsbs$_partner_i_id_abholadresse_part_partner_i_id" FOREIGN KEY (partner_i_id_abholadresse) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT "fk_bsbs$_personal_i_id_manuellgeliefert_pepe$_i_id" FOREIGN KEY (personal_i_id_manuellgeliefert) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT "fk_bsbs$_personal_i_id_manuellgeliefert_pers_personal_i_id" FOREIGN KEY (personal_i_id_manuellgeliefert) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT "fk_bsbs$_waehrung_c_nr_bestellungswaehrung_lp_waehrung_c_nr" FOREIGN KEY (waehrung_c_nr_bestellungswaehrung) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.bes_bestellungartspr
    ADD CONSTRAINT "fk_bsbs$artspr_bestellungart_c_nr_bsbs$art_c_nr" FOREIGN KEY (bestellungart_c_nr) REFERENCES public.bes_bestellungart(c_nr);
ALTER TABLE ONLY public.bes_bestellposition
    ADD CONSTRAINT "fk_bspo$_bestellposition_i_id_rahmenposition_bspo$_i_id" FOREIGN KEY (bestellposition_i_id_rahmenposition) REFERENCES public.bes_bestellposition(i_id);
ALTER TABLE ONLY public.bes_bestellposition
    ADD CONSTRAINT "fk_bspo$_bestellpositionart_c_nr_bspo$art_c_nr" FOREIGN KEY (bestellpositionart_c_nr) REFERENCES public.bes_bestellpositionart(positionsart_c_nr);
ALTER TABLE ONLY public.bes_bestellposition
    ADD CONSTRAINT "fk_bspo$_bestellpositionstatus_c_nr_bspo$status_c_nr" FOREIGN KEY (bestellpositionstatus_c_nr) REFERENCES public.bes_bestellpositionstatus(status_c_nr);
ALTER TABLE ONLY public.bes_bestellpositionart
    ADD CONSTRAINT "fk_bspo$art_positionsart_c_nr_lp_positionsart_c_nr" FOREIGN KEY (positionsart_c_nr) REFERENCES public.lp_positionsart(c_nr);
ALTER TABLE ONLY public.bes_wareneingangsposition
    ADD CONSTRAINT "fk_bswp$_bestellposition_i_id_bspo$_i_id" FOREIGN KEY (bestellposition_i_id) REFERENCES public.bes_bestellposition(i_id);
ALTER TABLE ONLY public.bes_wareneingangsposition
    ADD CONSTRAINT "fk_bswp$_bestellposition_id_bes_bestellposition_i_id" FOREIGN KEY (bestellposition_i_id) REFERENCES public.bes_bestellposition(i_id);
ALTER TABLE ONLY public.bes_wareneingangsposition
    ADD CONSTRAINT "fk_bswp$_eingangsrechnung_i_id_uebersteuert_erer$_i_id" FOREIGN KEY (eingangsrechnung_i_id_uebersteuert) REFERENCES public.er_eingangsrechnung(i_id);
ALTER TABLE ONLY public.bes_wareneingangsposition
    ADD CONSTRAINT "fk_bswp$_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_wareneingangsposition
    ADD CONSTRAINT "fk_bswp$_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.bes_wareneingangsposition
    ADD CONSTRAINT "fk_bswp$_wareneingang_i_id_bes_wareneingang_i_id" FOREIGN KEY (wareneingang_i_id) REFERENCES public.bes_wareneingang(i_id);
ALTER TABLE ONLY public.lp_dokumentenlinkbeleg
    ADD CONSTRAINT "fk_dobe$_dokumentenlink_i_id_lp_dokumentenlink_i_id" FOREIGN KEY (dokumentenlink_i_id) REFERENCES public.lp_dokumentenlink(i_id);
ALTER TABLE ONLY public.er_auftragszuordnung
    ADD CONSTRAINT fk_er_auftragszuordnung_auftrag_i_id_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.er_auftragszuordnung
    ADD CONSTRAINT "fk_er_auftragszuordnung_eingangsrechnung_i_id_erer$_i_id" FOREIGN KEY (eingangsrechnung_i_id) REFERENCES public.er_eingangsrechnung(i_id);
ALTER TABLE ONLY public.er_auftragszuordnung
    ADD CONSTRAINT "fk_er_auftragszuordnung_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.er_auftragszuordnung
    ADD CONSTRAINT "fk_er_auftragszuordnung_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.er_auftragszuordnung
    ADD CONSTRAINT fk_er_auftragszuordnung_personal_i_id_erledigt_pers_personal_i_ FOREIGN KEY (personal_i_id_erledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.er_auftragszuordnungverrechnet
    ADD CONSTRAINT fk_er_auftragszuordnungverrechnet_auftragszuordnung_i_id_er_auf FOREIGN KEY (auftragszuordnung_i_id) REFERENCES public.er_auftragszuordnung(i_id);
ALTER TABLE ONLY public.er_auftragszuordnungverrechnet
    ADD CONSTRAINT fk_er_auftragszuordnungverrechnet_rechnungposition_i_id_rech_re FOREIGN KEY (rechnungposition_i_id) REFERENCES public.rech_rechnungposition(i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT fk_er_eingangsrechnung_bestellung_i_id_bes_bestellung_i_id FOREIGN KEY (bestellung_i_id) REFERENCES public.bes_bestellung(i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT fk_er_eingangsrechnung_konto_i_id_fb_konto_i_id FOREIGN KEY (konto_i_id) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT fk_er_eingangsrechnung_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT fk_er_eingangsrechnung_lieferant_i_id_part_lieferant_i_id FOREIGN KEY (lieferant_i_id) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT fk_er_eingangsrechnung_lp_geschaeftsjahr FOREIGN KEY (i_geschaeftsjahr) REFERENCES public.lp_geschaeftsjahr(i_geschaeftsjahr);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT fk_er_eingangsrechnung_mahnstufe_i_id_fb_mahnstufe_i_id FOREIGN KEY (mahnstufe_i_id, mandant_c_nr) REFERENCES public.fb_mahnstufe(i_id, mandant_c_nr);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT fk_er_eingangsrechnung_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT fk_er_eingangsrechnung_mwstsatz_i_id_lp_mwstsatz_i_id FOREIGN KEY (mwstsatz_i_id) REFERENCES public.lp_mwstsatz(i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT fk_er_eingangsrechnung_personal_i_id_abw_bankverbindung_pers_pe FOREIGN KEY (personal_i_id_abw_bankverbindung) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT fk_er_eingangsrechnung_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT fk_er_eingangsrechnung_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT "fk_er_eingangsrechnung_personal_i_id_geprueft_pep$_i_id" FOREIGN KEY (personal_i_id_geprueft) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT fk_er_eingangsrechnung_reversechargeart_i_id_fb_reversechargear FOREIGN KEY (reversechargeart_i_id) REFERENCES public.fb_reversechargeart(i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT fk_er_eingangsrechnung_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.er_eingangsrechnungstatus(status_c_nr);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT fk_er_eingangsrechnung_waehrung_c_nr_lp_waehrung_c_nr FOREIGN KEY (waehrung_c_nr) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT fk_er_eingangsrechnung_zahlungsziel_i_id_lp_zahlungsziel_i_id FOREIGN KEY (zahlungsziel_i_id) REFERENCES public.lp_zahlungsziel(i_id);
ALTER TABLE ONLY public.er_eingangsrechnungartspr
    ADD CONSTRAINT fk_er_eingangsrechnungartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.er_eingangsrechnungstatus
    ADD CONSTRAINT fk_er_eingangsrechnungstatus_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.er_eingangsrechnungtext
    ADD CONSTRAINT fk_er_eingangsrechnungtext_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.er_eingangsrechnungtext
    ADD CONSTRAINT fk_er_eingangsrechnungtext_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.er_eingangsrechnungzahlung
    ADD CONSTRAINT fk_er_eingangsrechungzahlung_buchungdetail_i_id_fb_buchungdetai FOREIGN KEY (buchungdetail_i_id) REFERENCES public.fb_buchungdetail(i_id);
ALTER TABLE ONLY public.er_eingangsrechnungzahlung
    ADD CONSTRAINT fk_er_erzahlung_i_id_gutschrift_er_eingangsrechnung_i_id FOREIGN KEY (eingangsrechnung_i_id_gutschrift) REFERENCES public.er_eingangsrechnung(i_id);
ALTER TABLE ONLY public.er_eingangsrechnungzahlung
    ADD CONSTRAINT fk_er_erzahlung_i_id_gutschrift_er_erzahlung_i_id FOREIGN KEY (eingangsrechnungzahlung_i_id_gutschrift) REFERENCES public.er_eingangsrechnungzahlung(i_id);
ALTER TABLE ONLY public.er_eingangsrechnungzahlung
    ADD CONSTRAINT fk_er_erzahlung_rechnungzahlung_i_id_rech_rezahlung_i_id FOREIGN KEY (rechnungzahlung_i_id) REFERENCES public.rech_rechnungzahlung(i_id);
ALTER TABLE ONLY public.er_kontierung
    ADD CONSTRAINT fk_er_kontierung_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.er_kontierung
    ADD CONSTRAINT fk_er_kontierung_eingangsrechnung_i_id_er_eingangsrechnung_i_id FOREIGN KEY (eingangsrechnung_i_id) REFERENCES public.er_eingangsrechnung(i_id);
ALTER TABLE ONLY public.er_kontierung
    ADD CONSTRAINT fk_er_kontierung_konto_i_id_fb_konto_i_id FOREIGN KEY (konto_i_id) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.er_kontierung
    ADD CONSTRAINT fk_er_kontierung_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.er_kontierung
    ADD CONSTRAINT fk_er_kontierung_mwstsatz_i_id_lp_mwstsatz_i_id FOREIGN KEY (mwstsatz_i_id) REFERENCES public.lp_mwstsatz(i_id);
ALTER TABLE ONLY public.er_kontierung
    ADD CONSTRAINT fk_er_kontierung_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.er_kontierung
    ADD CONSTRAINT fk_er_kontierung_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.er_kontierung
    ADD CONSTRAINT fk_er_kontierung_reversechargeart_i_id_fb_reversechargeart_i_id FOREIGN KEY (reversechargeart_i_id) REFERENCES public.fb_reversechargeart(i_id);
ALTER TABLE ONLY public.er_zahlungsvorschlag
    ADD CONSTRAINT "fk_er_zahlungsvorschlag_zahlungsvorschlaglauf_i_id_erzl$_i_id" FOREIGN KEY (zahlungsvorschlaglauf_i_id) REFERENCES public.er_zahlungsvorschlaglauf(i_id);
ALTER TABLE ONLY public.er_zahlungsvorschlaglauf
    ADD CONSTRAINT fk_er_zahlungsvorschlaglauf_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.er_zahlungsvorschlaglauf
    ADD CONSTRAINT "fk_er_zahlungsvorschlaglauf_personal_i_id_gespeichert_pep$_i_id" FOREIGN KEY (personal_i_id_gespeichert) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT "fk_erer$_auftragwiederholungsintervall_c_nr_abwi$_c_nr" FOREIGN KEY (auftragwiederholungsintervall_c_nr) REFERENCES public.auft_auftragwiederholungsintervall(c_nr);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT "fk_erer$_eingangsrechnung_i_id_nachfolger_erer$_i_id" FOREIGN KEY (eingangsrechnung_i_id_nachfolger) REFERENCES public.er_eingangsrechnung(i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT "fk_erer$_eingangsrechnung_i_id_zollimport_erer$" FOREIGN KEY (eingangsrechnung_i_id_zollimport) REFERENCES public.er_eingangsrechnung(i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT "fk_erer$_eingangsrechnungart_c_nr_erer$art_c_nr" FOREIGN KEY (eingangsrechnungart_c_nr) REFERENCES public.er_eingangsrechnungart(c_nr);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT "fk_erer$_personal_i_id_manuellerledigt_pepe$_i_id" FOREIGN KEY (personal_i_id_manuellerledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT "fk_erer$_personal_i_id_wiederholenderledigt_pers_personal_i_id" FOREIGN KEY (personal_i_id_wiederholenderledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT "fk_erer$_personal_i_id_zollimportpapier_pers_personal_i_id" FOREIGN KEY (personal_i_id_zollimportpapier) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.er_eingangsrechnungartspr
    ADD CONSTRAINT "fk_erer$artspr_eingangsrechnungart_c_nr_erer$art_c_nr" FOREIGN KEY (eingangsrechnungart_c_nr) REFERENCES public.er_eingangsrechnungart(c_nr);
ALTER TABLE ONLY public.er_eingangsrechnungzahlung
    ADD CONSTRAINT "fk_erer$zahlung_bankverbindung_i_id_fbbv$_i_id" FOREIGN KEY (bankverbindung_i_id) REFERENCES public.fb_bankverbindung(i_id);
ALTER TABLE ONLY public.er_eingangsrechnungzahlung
    ADD CONSTRAINT "fk_erer$zahlung_eingangsrechnung_i_id_erer$_i_id" FOREIGN KEY (eingangsrechnung_i_id) REFERENCES public.er_eingangsrechnung(i_id);
ALTER TABLE ONLY public.er_eingangsrechnungzahlung
    ADD CONSTRAINT "fk_erer$zahlung_kassenbuch_i_id_fb_kassenbuch_i_id" FOREIGN KEY (kassenbuch_i_id) REFERENCES public.fb_kassenbuch(i_id);
ALTER TABLE ONLY public.er_eingangsrechnungzahlung
    ADD CONSTRAINT "fk_erer$zahlung_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.er_eingangsrechnungzahlung
    ADD CONSTRAINT "fk_erer$zahlung_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.er_eingangsrechnungzahlung
    ADD CONSTRAINT "fk_erer$zahlung_zahlungsart_c_nr_rech_zahlungsart_c_nr" FOREIGN KEY (zahlungsart_c_nr) REFERENCES public.rech_zahlungsart(c_nr);
ALTER TABLE ONLY public.er_zahlungsvorschlaglauf
    ADD CONSTRAINT "fk_erzl$_bankverbindung_i_id_fbbv$_i_id" FOREIGN KEY (bankverbindung_i_id) REFERENCES public.fb_bankverbindung(i_id);
ALTER TABLE ONLY public.er_zahlungsvorschlaglauf
    ADD CONSTRAINT "fk_erzl$_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.er_zahlungsvorschlag
    ADD CONSTRAINT "fk_erzv$_eingangsrechnung_i_id_er_eingangsrechnung_i_id" FOREIGN KEY (eingangsrechnung_i_id) REFERENCES public.er_eingangsrechnung(i_id);
ALTER TABLE ONLY public.fb_bankverbindung
    ADD CONSTRAINT fk_fb_bankverbindung_bank_i_id_part_bank_i_id FOREIGN KEY (bank_i_id) REFERENCES public.part_bank(partner_i_id);
ALTER TABLE ONLY public.fb_bankverbindung
    ADD CONSTRAINT fk_fb_bankverbindung_konto_i_id_fb_konto_i_id FOREIGN KEY (konto_i_id) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_bankverbindung
    ADD CONSTRAINT fk_fb_bankverbindung_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fb_bankverbindung
    ADD CONSTRAINT fk_fb_bankverbindung_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_bankverbindung
    ADD CONSTRAINT fk_fb_bankverbindung_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_belegart
    ADD CONSTRAINT fk_fb_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.fb_belegbuchung
    ADD CONSTRAINT fk_fb_belegbuchung_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.fb_belegbuchung
    ADD CONSTRAINT fk_fb_belegbuchung_buchung_i_id_fb_buchung_i_id FOREIGN KEY (buchung_i_id) REFERENCES public.fb_buchung(i_id);
ALTER TABLE ONLY public.fb_belegbuchung
    ADD CONSTRAINT fk_fb_belegbuchung_buchung_i_id_zahlung_fb_buchung_i_id FOREIGN KEY (buchung_i_id_zahlung) REFERENCES public.fb_buchung(i_id);
ALTER TABLE ONLY public.fb_buchung
    ADD CONSTRAINT fk_fb_buchung_belegart_c_nr_fb_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.fb_belegart(c_nr);
ALTER TABLE ONLY public.fb_buchung
    ADD CONSTRAINT fk_fb_buchung_buchungsart_c_nr_fb_buchungsart_c_nr FOREIGN KEY (buchungsart_c_nr) REFERENCES public.fb_buchungsart(c_nr);
ALTER TABLE ONLY public.fb_buchung
    ADD CONSTRAINT fk_fb_buchung_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.fb_buchung
    ADD CONSTRAINT fk_fb_buchung_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_buchung
    ADD CONSTRAINT fk_fb_buchung_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_buchung
    ADD CONSTRAINT "fk_fb_buchung_personal_i_id_storniert_pepe$_i_id" FOREIGN KEY (personal_i_id_storniert) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_buchung
    ADD CONSTRAINT fk_fb_buchung_uvaverprobung_i_id_fb_uvaverprobung_i_id FOREIGN KEY (uvaverprobung_i_id) REFERENCES public.fb_uvaverprobung(i_id);
ALTER TABLE ONLY public.fb_buchungdetail
    ADD CONSTRAINT fk_fb_buchungdetail_buchung_i_id_fb_buchung_i_id FOREIGN KEY (buchung_i_id) REFERENCES public.fb_buchung(i_id);
ALTER TABLE ONLY public.fb_buchungdetail
    ADD CONSTRAINT fk_fb_buchungdetail_konto_i_id_fb_konto_i_id FOREIGN KEY (konto_i_id) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_buchungdetail
    ADD CONSTRAINT fk_fb_buchungdetail_konto_i_id_gegenkonto_fb_konto_i_id FOREIGN KEY (konto_i_id_gegenkonto) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_buchungdetail
    ADD CONSTRAINT fk_fb_buchungdetail_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_buchungdetail
    ADD CONSTRAINT fk_fb_buchungdetail_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_buchungsartspr
    ADD CONSTRAINT fk_fb_buchungsartspr_buchungsart_c_nr_fb_buchungsart_c_nr FOREIGN KEY (buchungsart_c_nr) REFERENCES public.fb_buchungsart(c_nr);
ALTER TABLE ONLY public.fb_buchungsartspr
    ADD CONSTRAINT fk_fb_buchungsartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.fb_ergebnisgruppe
    ADD CONSTRAINT fk_fb_ergebnisgruppe_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fb_ergebnisgruppe
    ADD CONSTRAINT fk_fb_ergebnisgruppe_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_ergebnisgruppe
    ADD CONSTRAINT fk_fb_ergebnisgruppe_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_exportdaten
    ADD CONSTRAINT fk_fb_exportdaten_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.fb_exportdaten
    ADD CONSTRAINT fk_fb_exportdaten_exportlauf_i_id_fb_exportlauf_i_id FOREIGN KEY (exportlauf_i_id) REFERENCES public.fb_exportlauf(i_id);
ALTER TABLE ONLY public.fb_exportlauf
    ADD CONSTRAINT fk_fb_exportlauf_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fb_exportlauf
    ADD CONSTRAINT fk_fb_exportlauf_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_iso20022bankverbindung
    ADD CONSTRAINT fk_fb_iso20022bankverbindung_bankverbindung_i_id_fb_bankverbind FOREIGN KEY (bankverbindung_i_id) REFERENCES public.fb_bankverbindung(i_id);
ALTER TABLE ONLY public.fb_iso20022bankverbindung
    ADD CONSTRAINT fk_fb_iso20022bankverbindung_lastschriftschema_i_id_fb_iso20022 FOREIGN KEY (lastschriftschema_i_id) REFERENCES public.fb_iso20022lastschriftschema(i_id);
ALTER TABLE ONLY public.fb_iso20022bankverbindung
    ADD CONSTRAINT fk_fb_iso20022bankverbindung_zahlungsauftragschema_i_id_fb_iso2 FOREIGN KEY (zahlungsauftragschema_i_id) REFERENCES public.fb_iso20022zahlungsauftragschema(i_id);
ALTER TABLE ONLY public.fb_iso20022lastschriftschema
    ADD CONSTRAINT fk_fb_iso20022lastschriftschema_schema_i_id_fb_iso20022schema_i FOREIGN KEY (schema_i_id) REFERENCES public.fb_iso20022schema(i_id);
ALTER TABLE ONLY public.fb_iso20022lastschriftschema
    ADD CONSTRAINT fk_fb_iso20022lastschriftschema_standard_i_id_fb_iso20022standa FOREIGN KEY (standard_i_id) REFERENCES public.fb_iso20022standard(i_id);
ALTER TABLE ONLY public.fb_iso20022zahlungsauftragschema
    ADD CONSTRAINT fk_fb_iso20022zahlungsauftragschema_schema_i_id_fb_iso20022sche FOREIGN KEY (schema_i_id) REFERENCES public.fb_iso20022schema(i_id);
ALTER TABLE ONLY public.fb_iso20022zahlungsauftragschema
    ADD CONSTRAINT fk_fb_iso20022zahlungsauftragschema_standard_i_id_fb_iso20022st FOREIGN KEY (standard_i_id) REFERENCES public.fb_iso20022standard(i_id);
ALTER TABLE ONLY public.fb_kassenbuch
    ADD CONSTRAINT fk_fb_kassenbuch_konto_i_id_fb_konto_i_id FOREIGN KEY (konto_i_id) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_kassenbuch
    ADD CONSTRAINT fk_fb_kassenbuch_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fb_kassenbuch
    ADD CONSTRAINT fk_fb_kassenbuch_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_kassenbuch
    ADD CONSTRAINT fk_fb_kassenbuch_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT fk_fb_konto_ergebnisgruppe_i_id_fb_ergebnisgruppe_i_id FOREIGN KEY (ergebnisgruppe_i_id) REFERENCES public.fb_ergebnisgruppe(i_id);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT "fk_fb_konto_ergebnisgruppe_i_id_negativ_fbeg$_i_id" FOREIGN KEY (ergebnisgruppe_i_id_negativ) REFERENCES public.fb_ergebnisgruppe(i_id);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT fk_fb_konto_finanzamt_i_id_lp_finanzamt_i_id FOREIGN KEY (finanzamt_i_id, mandant_c_nr) REFERENCES public.lp_finanzamt(partner_i_id, mandant_c_nr);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT fk_fb_konto_konto_i_id_weiterfuehrendskonto_fb_konto_i_id FOREIGN KEY (konto_i_id_weiterfuehrendskonto) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT fk_fb_konto_konto_i_id_weiterfuehrendust_fb_konto_i_id FOREIGN KEY (konto_i_id_weiterfuehrendust) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT fk_fb_konto_kontoart_c_nr_fb_kontoart_c_nr FOREIGN KEY (kontoart_c_nr) REFERENCES public.fb_kontoart(c_nr);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT fk_fb_konto_kontotyp_c_nr_fb_kontotyp_c_nr FOREIGN KEY (kontotyp_c_nr) REFERENCES public.fb_kontotyp(c_nr);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT fk_fb_konto_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT fk_fb_konto_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT fk_fb_konto_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT fk_fb_konto_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT "fk_fb_konto_rechenregel_c_nr_weiterfuehrendbilanz_fbrr$_c_nr" FOREIGN KEY (rechenregel_c_nr_weiterfuehrendbilanz) REFERENCES public.fb_rechenregel(c_nr);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT "fk_fb_konto_rechenregel_c_nr_weiterfuehrendskonto_fbrr$_c_nr" FOREIGN KEY (rechenregel_c_nr_weiterfuehrendskonto) REFERENCES public.fb_rechenregel(c_nr);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT "fk_fb_konto_rechenregel_c_nr_weiterfuehrendust_fbrr$_c_nr" FOREIGN KEY (rechenregel_c_nr_weiterfuehrendust) REFERENCES public.fb_rechenregel(c_nr);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT "fk_fb_konto_steuerkategorie_i_id_fbsk$_i_id" FOREIGN KEY (steuerkategorie_i_id) REFERENCES public.fb_steuerkategorie(i_id);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT "fk_fb_konto_steuerkategorie_i_id_reverse_fbsk$_i_id" FOREIGN KEY (steuerkategorie_i_id_reverse) REFERENCES public.fb_steuerkategorie(i_id);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT fk_fb_konto_uvaart_i_id_fb_uvaart_i_id FOREIGN KEY (uvaart_i_id) REFERENCES public.fb_uvaart(i_id);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT fk_fb_konto_waehrung_c_nr_druck_lp_waehrung_c_nr FOREIGN KEY (waehrung_c_nr_druck) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.fb_kontoartspr
    ADD CONSTRAINT fk_fb_kontoartspr_kontoart_c_nr_fb_kontoart_c_nr FOREIGN KEY (kontoart_c_nr) REFERENCES public.fb_kontoart(c_nr);
ALTER TABLE ONLY public.fb_kontoartspr
    ADD CONSTRAINT fk_fb_kontoartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.fb_kontolaenderart
    ADD CONSTRAINT fk_fb_kontolaenderart_konto_i_id_fb_konto_i_id FOREIGN KEY (konto_i_id) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_kontolaenderart
    ADD CONSTRAINT fk_fb_kontolaenderart_konto_i_id_uebersetzt_fb_konto_i_id FOREIGN KEY (konto_i_id_uebersetzt) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_kontolaenderart
    ADD CONSTRAINT fk_fb_kontolaenderart_laenderart_c_nr_fb_laenderart_c_nr FOREIGN KEY (laenderart_c_nr) REFERENCES public.fb_laenderart(c_nr);
ALTER TABLE ONLY public.fb_kontolaenderart
    ADD CONSTRAINT fk_fb_kontolaenderart_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fb_kontolaenderart
    ADD CONSTRAINT fk_fb_kontolaenderart_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_kontolaenderart
    ADD CONSTRAINT fk_fb_kontolaenderart_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_kontolaenderart
    ADD CONSTRAINT fk_fb_kontolaenderart_reversechargeart_i_id_fb_reversechargeart FOREIGN KEY (reversechargeart_i_id) REFERENCES public.fb_reversechargeart(i_id);
ALTER TABLE ONLY public.fb_kontoland
    ADD CONSTRAINT fk_fb_kontoland_konto_i_id_fb_konto_i_id FOREIGN KEY (konto_i_id) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_kontoland
    ADD CONSTRAINT fk_fb_kontoland_land_i_id_lp_land_i_id FOREIGN KEY (land_i_id) REFERENCES public.lp_land(i_id);
ALTER TABLE ONLY public.fb_kontoland
    ADD CONSTRAINT fk_fb_kontoland_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_kontoland
    ADD CONSTRAINT fk_fb_kontoland_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_kontotypspr
    ADD CONSTRAINT fk_fb_kontotypspr_kontotyp_c_nr_fb_kontotyp_c_nr FOREIGN KEY (kontotyp_c_nr) REFERENCES public.fb_kontotyp(c_nr);
ALTER TABLE ONLY public.fb_kontotypspr
    ADD CONSTRAINT fk_fb_kontotypspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.fb_laenderartspr
    ADD CONSTRAINT fk_fb_laenderartspr_laenderart_c_nr_fb_laenderart_c_nr FOREIGN KEY (laenderart_c_nr) REFERENCES public.fb_laenderart(c_nr);
ALTER TABLE ONLY public.fb_laenderartspr
    ADD CONSTRAINT fk_fb_laenderartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.fb_mahnlauf
    ADD CONSTRAINT fk_fb_mahnlauf_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fb_mahnlauf
    ADD CONSTRAINT fk_fb_mahnlauf_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_mahnlauf
    ADD CONSTRAINT fk_fb_mahnlauf_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_mahnspesen
    ADD CONSTRAINT fk_fb_mahnspesen_mahnstufe_i_id_fb_mahnstufe_i_id FOREIGN KEY (mahnstufe_i_id, mandant_c_nr) REFERENCES public.fb_mahnstufe(i_id, mandant_c_nr);
ALTER TABLE ONLY public.fb_mahnspesen
    ADD CONSTRAINT fk_fb_mahnspesen_waehrung_c_nr_lp_waehrung_c_nr FOREIGN KEY (waehrung_c_nr) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.fb_mahnstufe
    ADD CONSTRAINT fk_fb_mahnstufe_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fb_mahntext
    ADD CONSTRAINT fk_fb_mahntext_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.fb_mahntext
    ADD CONSTRAINT fk_fb_mahntext_mahnstufe_i_id_fb_mahnstufe_i_id FOREIGN KEY (mahnstufe_i_id, mandant_c_nr) REFERENCES public.fb_mahnstufe(i_id, mandant_c_nr);
ALTER TABLE ONLY public.fb_mahntext
    ADD CONSTRAINT fk_fb_mahntext_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fb_mahnung
    ADD CONSTRAINT fk_fb_mahnung_mahnlauf_i_id_fb_mahnlauf_i_id FOREIGN KEY (mahnlauf_i_id) REFERENCES public.fb_mahnlauf(i_id);
ALTER TABLE ONLY public.fb_mahnung
    ADD CONSTRAINT fk_fb_mahnung_mahnstufe_i_id_fb_mahnstufe_i_id FOREIGN KEY (mahnstufe_i_id, mandant_c_nr) REFERENCES public.fb_mahnstufe(i_id, mandant_c_nr);
ALTER TABLE ONLY public.fb_mahnung
    ADD CONSTRAINT fk_fb_mahnung_mahnstufe_i_id_letztemahnstufe_fb_mahnstufe_i_id FOREIGN KEY (mahnstufe_i_id_letztemahnstufe, mandant_c_nr) REFERENCES public.fb_mahnstufe(i_id, mandant_c_nr);
ALTER TABLE ONLY public.fb_mahnung
    ADD CONSTRAINT fk_fb_mahnung_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fb_mahnung
    ADD CONSTRAINT fk_fb_mahnung_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_mahnung
    ADD CONSTRAINT fk_fb_mahnung_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_mahnung
    ADD CONSTRAINT fk_fb_mahnung_personal_i_id_gedruckt_pers_personal_i_id FOREIGN KEY (personal_i_id_gedruckt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_mahnung
    ADD CONSTRAINT fk_fb_mahnung_rechnung_i_id_rech_rechnung_i_id FOREIGN KEY (rechnung_i_id) REFERENCES public.rech_rechnung(i_id);
ALTER TABLE ONLY public.fb_reversechargeart
    ADD CONSTRAINT fk_fb_reversechargeart_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fb_reversechargeartspr
    ADD CONSTRAINT fk_fb_reversechargeartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.fb_reversechargeartspr
    ADD CONSTRAINT fk_fb_reversechargeartspr_reversechargeart_i_id_fb_reversecharg FOREIGN KEY (reversechargeart_i_id) REFERENCES public.fb_reversechargeart(i_id);
ALTER TABLE ONLY public.fb_sepakontoauszug
    ADD CONSTRAINT fk_fb_sepakontoauszug_bankverbindung_i_id FOREIGN KEY (bankverbindung_i_id) REFERENCES public.fb_bankverbindung(i_id);
ALTER TABLE ONLY public.fb_sepakontoauszug
    ADD CONSTRAINT fk_fb_sepakontoauszug_personal_i_id_aendern FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_sepakontoauszug
    ADD CONSTRAINT fk_fb_sepakontoauszug_personal_i_id_anlegen FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_sepakontoauszug
    ADD CONSTRAINT fk_fb_sepakontoauszug_personal_i_id_verbuchen FOREIGN KEY (personal_i_id_verbuchen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_steuerkategorie
    ADD CONSTRAINT fk_fb_steuerkategorie_konto_i_id_forderungen_konto_i_id FOREIGN KEY (konto_i_id_forderungen) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_steuerkategorie
    ADD CONSTRAINT fk_fb_steuerkategorie_konto_i_id_kursgewinn_fb_konto_i_id FOREIGN KEY (konto_i_id_kursgewinn) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_steuerkategorie
    ADD CONSTRAINT fk_fb_steuerkategorie_konto_i_id_kursverlust_fb_konto_i_id FOREIGN KEY (konto_i_id_kursverlust) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_steuerkategorie
    ADD CONSTRAINT fk_fb_steuerkategorie_konto_i_id_verbindlichkeiten_konto_i_id FOREIGN KEY (konto_i_id_verbindlichkeiten) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_steuerkategorie
    ADD CONSTRAINT fk_fb_steuerkategorie_reversechargeart_i_id_fb_reversechargeart FOREIGN KEY (reversechargeart_i_id) REFERENCES public.fb_reversechargeart(i_id);
ALTER TABLE ONLY public.fb_steuerkategoriekonto
    ADD CONSTRAINT fk_fb_steuerkategoriekonto_konto_i_id_einfuhrust_fb_konto_i_id FOREIGN KEY (konto_i_id_einfuhrust) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_steuerkategoriekonto
    ADD CONSTRAINT fk_fb_steuerkategoriekonto_konto_i_id_ek_fb_konto_i_id FOREIGN KEY (konto_i_id_ek) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_steuerkategoriekonto
    ADD CONSTRAINT fk_fb_steuerkategoriekonto_konto_i_id_skontoek_fb_konto_i_id FOREIGN KEY (konto_i_id_skontoek) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_steuerkategoriekonto
    ADD CONSTRAINT fk_fb_steuerkategoriekonto_konto_i_id_skontovk_fb_konto_i_id FOREIGN KEY (konto_i_id_skontovk) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_steuerkategoriekonto
    ADD CONSTRAINT fk_fb_steuerkategoriekonto_konto_i_id_vk_fb_konto_i_id FOREIGN KEY (konto_i_id_vk) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.fb_steuerkategoriekonto
    ADD CONSTRAINT fk_fb_steuerkategoriekonto_mwstsatzbez_i_id_lp_mwstsatzbez_i_id FOREIGN KEY (mwstsatzbez_i_id) REFERENCES public.lp_mwstsatzbez(i_id);
ALTER TABLE ONLY public.fb_steuerkategoriekonto
    ADD CONSTRAINT "fk_fb_steuerkategoriekonto_steuerkategorie_i_id_fbsk$_i_id" FOREIGN KEY (steuerkategorie_i_id) REFERENCES public.fb_steuerkategorie(i_id);
ALTER TABLE ONLY public.fb_ustuebersetzung
    ADD CONSTRAINT fk_fb_ustuebersetzung_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fb_ustuebersetzung
    ADD CONSTRAINT fk_fb_ustuebersetzung_mwstsatzbez_i_id_lp_mwstsatzbez_i_id FOREIGN KEY (mwstsatzbez_i_id) REFERENCES public.lp_mwstsatzbez(i_id);
ALTER TABLE ONLY public.fb_ustuebersetzung
    ADD CONSTRAINT fk_fb_ustuebersetzung_reversechargeart_i_id_fb_reversechargeart FOREIGN KEY (reversechargeart_i_id) REFERENCES public.fb_reversechargeart(i_id);
ALTER TABLE ONLY public.fb_uvaart
    ADD CONSTRAINT fk_fb_uvaart_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fb_uvaartspr
    ADD CONSTRAINT fk_fb_uvaartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.fb_uvaartspr
    ADD CONSTRAINT fk_fb_uvaartspr_uvaart_i_id_fb_uvaart_i_id FOREIGN KEY (uvaart_i_id) REFERENCES public.fb_uvaart(i_id);
ALTER TABLE ONLY public.fb_uvaformular
    ADD CONSTRAINT fk_fb_uvaformular_finanzamt_i_id_lp_finanzamt_partner_i_id FOREIGN KEY (finanzamt_i_id, mandant_c_nr) REFERENCES public.lp_finanzamt(partner_i_id, mandant_c_nr);
ALTER TABLE ONLY public.fb_uvaformular
    ADD CONSTRAINT fk_fb_uvaformular_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fb_uvaformular
    ADD CONSTRAINT fk_fb_uvaformular_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_uvaformular
    ADD CONSTRAINT fk_fb_uvaformular_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fb_uvaformular
    ADD CONSTRAINT fk_fb_uvaformular_uvaart_i_id_fb_uvaart_i_id FOREIGN KEY (uvaart_i_id) REFERENCES public.fb_uvaart(i_id);
ALTER TABLE ONLY public.fb_buchungdetail
    ADD CONSTRAINT "fk_fbbd$_buchungdetailart_c_nr_fb_buchungdetailart_c_nr" FOREIGN KEY (buchungdetailart_c_nr) REFERENCES public.fb_buchungdetailart(c_nr);
ALTER TABLE ONLY public.fb_buchung
    ADD CONSTRAINT "fk_fbbu$_geschaeftsjahr_i_geschaeftsjahr_lpgj$_i_geschaeftsjahr" FOREIGN KEY (geschaeftsjahr_i_geschaeftsjahr) REFERENCES public.lp_geschaeftsjahr(i_geschaeftsjahr);
ALTER TABLE ONLY public.fb_ergebnisgruppe
    ADD CONSTRAINT "fk_fbeg$_ergebnisgruppe_i_id_summe_fbeg$_i_id" FOREIGN KEY (ergebnisgruppe_i_id_summe) REFERENCES public.fb_ergebnisgruppe(i_id);
ALTER TABLE ONLY public.fb_uvaverprobung
    ADD CONSTRAINT "fk_fbuv$_finanzamt_i_id_lp_finanzamt_i_id" FOREIGN KEY (finanzamt_i_id, mandant_c_nr) REFERENCES public.lp_finanzamt(partner_i_id, mandant_c_nr);
ALTER TABLE ONLY public.fb_uvaverprobung
    ADD CONSTRAINT "fk_fbuv$_finanzamt_i_id_lp_mandant_c_nr" FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fb_uvaverprobung
    ADD CONSTRAINT "fk_fbuv$_geschaeftsjahr_i_geschaeftsjahr_lpgj$_i_geschaeftsjahr" FOREIGN KEY (geschaeftsjahr_i_geschaeftsjahr) REFERENCES public.lp_geschaeftsjahr(i_geschaeftsjahr);
ALTER TABLE ONLY public.fb_uvaverprobung
    ADD CONSTRAINT "fk_fbuv$_mandant_c_nr_lp_mandant_c_nr" FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fb_uvaverprobung
    ADD CONSTRAINT "fk_fbuv$_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fc_forecastauftrag
    ADD CONSTRAINT "fk_fc_fafa$_fclieferadresse_i_id_fc_fclieferadresse_i_id" FOREIGN KEY (fclieferadresse_i_id) REFERENCES public.fc_fclieferadresse(i_id);
ALTER TABLE ONLY public.fc_fclieferadresse
    ADD CONSTRAINT fk_fc_fclieferadresse_forecast_i_id_fc_forecast_i_id FOREIGN KEY (forecast_i_id) REFERENCES public.fc_forecast(i_id);
ALTER TABLE ONLY public.fc_fclieferadresse
    ADD CONSTRAINT fk_fc_fclieferadresse_importdef_c_nr_pfad_fc_importdef_c_nr FOREIGN KEY (importdef_c_nr_pfad) REFERENCES public.fc_importdef(c_nr);
ALTER TABLE ONLY public.fc_fclieferadresse
    ADD CONSTRAINT fk_fc_fclieferadresse_kommdrucker_i_id_fc_kommdrucker_i_id FOREIGN KEY (kommdrucker_i_id) REFERENCES public.fc_kommdrucker(i_id);
ALTER TABLE ONLY public.fc_fclieferadresse
    ADD CONSTRAINT fk_fc_fclieferadresse_kunde_i_id_lieferadresse_part_kunde_i_id FOREIGN KEY (kunde_i_id_lieferadresse) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.fc_forecast
    ADD CONSTRAINT fk_fc_forecast_importdef_c_nr_cod_fc_importdef_c_nr FOREIGN KEY (importdef_c_nr_cod) REFERENCES public.fc_importdef(c_nr);
ALTER TABLE ONLY public.fc_forecast
    ADD CONSTRAINT fk_fc_forecast_importdef_c_nr_cow_fc_importdef_c_nr FOREIGN KEY (importdef_c_nr_cow) REFERENCES public.fc_importdef(c_nr);
ALTER TABLE ONLY public.fc_forecast
    ADD CONSTRAINT fk_fc_forecast_importdef_c_nr_forecastauftrag_fc_importdef_c_nr FOREIGN KEY (importdef_c_nr_forecastauftrag) REFERENCES public.fc_importdef(c_nr);
ALTER TABLE ONLY public.fc_forecast
    ADD CONSTRAINT fk_fc_forecast_importdef_c_nr_linienabruf_fc_importdef_c_nr FOREIGN KEY (importdef_c_nr_linienabruf) REFERENCES public.fc_importdef(c_nr);
ALTER TABLE ONLY public.fc_forecast
    ADD CONSTRAINT fk_fc_forecast_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.fc_forecast
    ADD CONSTRAINT fk_fc_forecast_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.fc_forecastartspr
    ADD CONSTRAINT fk_fc_forecastartspr_forecastart_c_nr_fc_forecastart_c_nr FOREIGN KEY (forecastart_c_nr) REFERENCES public.fc_forecastart(c_nr);
ALTER TABLE ONLY public.fc_forecastposition
    ADD CONSTRAINT fk_fc_forecastauftrag_forecastauftrag_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.fc_forecastauftrag
    ADD CONSTRAINT "fk_fc_forecastauftrag_personal_i_id_freigabe_pep$_i_id" FOREIGN KEY (personal_i_id_freigabe) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fc_forecastauftrag
    ADD CONSTRAINT fk_fc_forecastauftrag_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.fc_forecastposition
    ADD CONSTRAINT fk_fc_forecastposition_forecastart_c_nr_fc_forecastart_c_nr FOREIGN KEY (forecastart_c_nr) REFERENCES public.fc_forecastart(c_nr);
ALTER TABLE ONLY public.fc_forecastposition
    ADD CONSTRAINT fk_fc_forecastposition_personal_i_id_produktion FOREIGN KEY (personal_i_id_produktion) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fc_forecastposition
    ADD CONSTRAINT fk_fc_forecastposition_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.fc_importdefspr
    ADD CONSTRAINT fk_fc_importdefspr_importdef_c_nr_fc_importdef_c_nr FOREIGN KEY (importdef_c_nr) REFERENCES public.fc_importdef(c_nr);
ALTER TABLE ONLY public.fc_importdefspr
    ADD CONSTRAINT fk_fc_importdefspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.fc_kommdrucker
    ADD CONSTRAINT fk_fc_kommdrucker_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fc_linienabruf
    ADD CONSTRAINT fk_fc_linienabruf_forecastposition_i_id_fc_forecastposition_i_i FOREIGN KEY (forecastposition_i_id) REFERENCES public.fc_forecastposition(i_id);
ALTER TABLE ONLY public.fc_forecastposition
    ADD CONSTRAINT "fk_fcfa$_forecastauftrag_i_id_fc_forecastauftrag_i_id" FOREIGN KEY (forecastauftrag_i_id) REFERENCES public.fc_forecastauftrag(i_id);
ALTER TABLE ONLY public.fert_lossollarbeitsplan
    ADD CONSTRAINT "fk_felo$_personal_i_id_zugeordneter_pepe$_i_id" FOREIGN KEY (personal_i_id_zugeordneter) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_bedarfsuebernahme
    ADD CONSTRAINT fk_fert_bedarfsuebernahme_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.fert_bedarfsuebernahme
    ADD CONSTRAINT fk_fert_bedarfsuebernahme_los_i_id_fert_los_i_id FOREIGN KEY (los_i_id) REFERENCES public.fert_los(i_id);
ALTER TABLE ONLY public.fert_bedarfsuebernahme
    ADD CONSTRAINT fk_fert_bedarfsuebernahme_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.fert_internebestellung
    ADD CONSTRAINT fk_fert_internebestellung_auftrag_i_id_kopfauftrag_auft_auftrag FOREIGN KEY (auftrag_i_id_kopfauftrag) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.fert_internebestellung
    ADD CONSTRAINT fk_fert_internebestellung_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.fert_internebestellung
    ADD CONSTRAINT fk_fert_internebestellung_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fert_internebestellung
    ADD CONSTRAINT fk_fert_internebestellung_stueckliste_i_id_stk_stueckliste_i_id FOREIGN KEY (stueckliste_i_id) REFERENCES public.stk_stueckliste(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_auftrag_i_id_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_auftragposition_i_id_auft_auftragposition_i_id FOREIGN KEY (auftragposition_i_id) REFERENCES public.auft_auftragposition(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_fertigungsgruppe_i_id_stk_fertigungsgruppe_i_id FOREIGN KEY (fertigungsgruppe_i_id) REFERENCES public.stk_fertigungsgruppe(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_forecastposition_i_id_fc_forecastposition_i_id FOREIGN KEY (forecastposition_i_id) REFERENCES public.fc_forecastposition(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_lager_i_id_ziel_ww_lager_i_id FOREIGN KEY (lager_i_id_ziel) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_lagerplatz_i_id_ww_lagerplatz_i_id FOREIGN KEY (lagerplatz_i_id) REFERENCES public.ww_lagerplatz(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_los_i_id_elternlos_fert_los_i_id FOREIGN KEY (los_i_id_elternlos) REFERENCES public.fert_los(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_losbereich_i_id_fert_losbereich_i_id FOREIGN KEY (losbereich_i_id) REFERENCES public.fert_losbereich(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_partner_i_id_fertigungsort_part_partner_i_id FOREIGN KEY (partner_i_id_fertigungsort) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_personal_i_id_ausgabe_pers_personal_i_id FOREIGN KEY (personal_i_id_ausgabe) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_personal_i_id_erledigt_pers_personal_i_id FOREIGN KEY (personal_i_id_erledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_personal_i_id_leitstandstop_pers_personal_i_id FOREIGN KEY (personal_i_id_leitstandstop) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_personal_i_id_manuellerledigt_pers_personal_i_id FOREIGN KEY (personal_i_id_manuellerledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_personal_i_id_material_vollstaendig_pers_personal_i FOREIGN KEY (personal_i_id_material_vollstaendig) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_personal_i_id_nachtraeglich_pers_personal_i_id FOREIGN KEY (personal_i_id_nachtraeglich_geoeffnet) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_personal_i_id_produktionsstop_pers_personal_i_id FOREIGN KEY (personal_i_id_produktionsstop) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_personal_i_id_techniker_pers_personal_i_id FOREIGN KEY (personal_i_id_techniker) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT "fk_fert_los_personal_i_id_vp_etikettengedruckt_gespeichert_pep$" FOREIGN KEY (personal_i_id_vp_etikettengedruckt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_projekt_i_id_proj_projekt_i_id FOREIGN KEY (projekt_i_id) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.fert_losstatus(status_c_nr);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_stueckliste_i_id_stk_stueckliste_i_id FOREIGN KEY (stueckliste_i_id) REFERENCES public.stk_stueckliste(i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT fk_fert_los_wiederholendelose_i_id_fert_wiederholendelose_i_id FOREIGN KEY (wiederholendelose_i_id) REFERENCES public.fert_wiederholendelose(i_id);
ALTER TABLE ONLY public.fert_losablieferung
    ADD CONSTRAINT fk_fert_losablieferung_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.fert_losablieferung
    ADD CONSTRAINT fk_fert_losablieferung_los_i_id_fert_los_i_id FOREIGN KEY (los_i_id) REFERENCES public.fert_los(i_id);
ALTER TABLE ONLY public.fert_losablieferung
    ADD CONSTRAINT fk_fert_losablieferung_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_losgutschlecht
    ADD CONSTRAINT fk_fert_losgutschlecht_fehler_i_id_rekla_fehler_i_id FOREIGN KEY (fehler_i_id) REFERENCES public.rekla_fehler(i_id);
ALTER TABLE ONLY public.fert_losgutschlecht
    ADD CONSTRAINT "fk_fert_losgutschlecht_lossollarbeitsplan_i_id_fela$_i_id" FOREIGN KEY (lossollarbeitsplan_i_id) REFERENCES public.fert_lossollarbeitsplan(i_id);
ALTER TABLE ONLY public.fert_losgutschlecht
    ADD CONSTRAINT "fk_fert_losgutschlecht_maschinenzeitdaten_i_id_pemz$_i_id" FOREIGN KEY (maschinenzeitdaten_i_id) REFERENCES public.pers_maschinenzeitdaten(i_id);
ALTER TABLE ONLY public.fert_losgutschlecht
    ADD CONSTRAINT "fk_fert_losgutschlecht_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_losgutschlecht
    ADD CONSTRAINT "fk_fert_losgutschlecht_personal_i_id_erfasst_pepe$_i_id" FOREIGN KEY (personal_i_id_erfasst) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_losgutschlecht
    ADD CONSTRAINT fk_fert_losgutschlecht_zeitdaten_i_id_pers_zeitdaten_i_id FOREIGN KEY (zeitdaten_i_id) REFERENCES public.pers_zeitdaten(i_id);
ALTER TABLE ONLY public.fert_losistmaterial
    ADD CONSTRAINT fk_fert_losistmaterial_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.fert_losklasse
    ADD CONSTRAINT fk_fert_losklasse_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_losklasse
    ADD CONSTRAINT fk_fert_losklasse_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_losklassespr
    ADD CONSTRAINT fk_fert_losklassespr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.fert_losklassespr
    ADD CONSTRAINT fk_fert_losklassespr_losklasse_i_id_fert_losklasse_i_id FOREIGN KEY (losklasse_i_id) REFERENCES public.fert_losklasse(i_id);
ALTER TABLE ONLY public.fert_losklassespr
    ADD CONSTRAINT fk_fert_losklassespr_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_losklassespr
    ADD CONSTRAINT fk_fert_losklassespr_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_loslagerentnahme
    ADD CONSTRAINT fk_fert_loslagerentnahme_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.fert_loslagerentnahme
    ADD CONSTRAINT fk_fert_loslagerentnahme_los_i_id_fert_los_i_id FOREIGN KEY (los_i_id) REFERENCES public.fert_los(i_id);
ALTER TABLE ONLY public.fert_loslagerentnahme
    ADD CONSTRAINT "fk_fert_loslagerentnahme_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_loslosklasse
    ADD CONSTRAINT fk_fert_loslosklasse_los_i_id_fert_los_i_id FOREIGN KEY (los_i_id) REFERENCES public.fert_los(i_id);
ALTER TABLE ONLY public.fert_loslosklasse
    ADD CONSTRAINT fk_fert_loslosklasse_losklasse_i_id_fert_losklasse_i_id FOREIGN KEY (losklasse_i_id) REFERENCES public.fert_losklasse(i_id);
ALTER TABLE ONLY public.fert_loslosklasse
    ADD CONSTRAINT fk_fert_loslosklasse_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_lospruefplan
    ADD CONSTRAINT fk_fert_lospruefplan_los_i_id_fert_los_i_id FOREIGN KEY (los_i_id) REFERENCES public.fert_los(i_id);
ALTER TABLE ONLY public.fert_lospruefplan
    ADD CONSTRAINT "fk_fert_lospruefplan_lossollmaterial_i_id_kontakt_$stpo_i_id" FOREIGN KEY (lossollmaterial_i_id_kontakt) REFERENCES public.fert_lossollmaterial(i_id);
ALTER TABLE ONLY public.fert_lospruefplan
    ADD CONSTRAINT "fk_fert_lospruefplan_lossollmaterial_i_id_litze2_$stpo_i_id" FOREIGN KEY (lossollmaterial_i_id_litze2) REFERENCES public.fert_lossollmaterial(i_id);
ALTER TABLE ONLY public.fert_lospruefplan
    ADD CONSTRAINT "fk_fert_lospruefplan_lossollmaterial_i_id_litze_$stpo_i_id" FOREIGN KEY (lossollmaterial_i_id_litze) REFERENCES public.fert_lossollmaterial(i_id);
ALTER TABLE ONLY public.fert_lospruefplan
    ADD CONSTRAINT "fk_fert_lospruefplan_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_lospruefplan
    ADD CONSTRAINT "fk_fert_lospruefplan_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_lospruefplan
    ADD CONSTRAINT fk_fert_lospruefplan_pruefart_i_id_stk_pruefart_i_id FOREIGN KEY (pruefart_i_id) REFERENCES public.stk_pruefart(i_id);
ALTER TABLE ONLY public.fert_lospruefplan
    ADD CONSTRAINT "fk_fert_lospruefplan_verschleissteil_i_id_$wwvt_i_id" FOREIGN KEY (verschleissteil_i_id) REFERENCES public.ww_verschleissteil(i_id);
ALTER TABLE ONLY public.fert_lospruefplan
    ADD CONSTRAINT fk_fert_lospruefplan_werkzeug_i_id_ww_werkzeug_i_id FOREIGN KEY (werkzeug_i_id) REFERENCES public.ww_werkzeug(i_id);
ALTER TABLE ONLY public.fert_lossollarbeitsplan
    ADD CONSTRAINT fk_fert_lossollarbeitsplan_agart_c_nr_stk_agart_c_nr FOREIGN KEY (agart_c_nr) REFERENCES public.stk_agart(c_nr);
ALTER TABLE ONLY public.fert_lossollarbeitsplan
    ADD CONSTRAINT fk_fert_lossollarbeitsplan_los_i_id_fert_los_i_id FOREIGN KEY (los_i_id) REFERENCES public.fert_los(i_id);
ALTER TABLE ONLY public.fert_lossollarbeitsplan
    ADD CONSTRAINT "fk_fert_lossollarbeitsplan_lossollmaterial_i_id_loso$_i_id" FOREIGN KEY (lossollmaterial_i_id) REFERENCES public.fert_lossollmaterial(i_id);
ALTER TABLE ONLY public.fert_lossollarbeitsplan
    ADD CONSTRAINT fk_fert_lossollarbeitsplan_maschine_i_id_pers_maschine_i_id FOREIGN KEY (maschine_i_id) REFERENCES public.pers_maschine(i_id);
ALTER TABLE ONLY public.fert_lossollarbeitsplan
    ADD CONSTRAINT fk_fert_lossollarbeitsplan_personal_i_id_fertig_pers_personal_i FOREIGN KEY (personal_i_id_fertig) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_lossollmaterial
    ADD CONSTRAINT fk_fert_lossollmaterial_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.fert_lossollmaterial
    ADD CONSTRAINT fk_fert_lossollmaterial_einheit_c_nr_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.fert_lossollmaterial
    ADD CONSTRAINT fk_fert_lossollmaterial_einheit_c_nr_stklpos_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr_stklpos) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.fert_lossollmaterial
    ADD CONSTRAINT fk_fert_lossollmaterial_los_i_id_fert_los_i_id FOREIGN KEY (los_i_id) REFERENCES public.fert_los(i_id);
ALTER TABLE ONLY public.fert_lossollmaterial
    ADD CONSTRAINT fk_fert_lossollmaterial_montageart_i_id_stk_montageart_i_id FOREIGN KEY (montageart_i_id) REFERENCES public.stk_montageart(i_id);
ALTER TABLE ONLY public.fert_lossollmaterial
    ADD CONSTRAINT "fk_fert_lossollmaterial_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_losstatus
    ADD CONSTRAINT fk_fert_losstatus_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.fert_lostechniker
    ADD CONSTRAINT fk_fert_lostechniker_los_i_id_fert_los_i_id FOREIGN KEY (los_i_id) REFERENCES public.fert_los(i_id);
ALTER TABLE ONLY public.fert_lostechniker
    ADD CONSTRAINT fk_fert_lostechniker_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_loszusatzstatus
    ADD CONSTRAINT fk_fert_loszusatzstatus_los_i_id_fert_los_i_id FOREIGN KEY (los_i_id) REFERENCES public.fert_los(i_id);
ALTER TABLE ONLY public.fert_loszusatzstatus
    ADD CONSTRAINT "fk_fert_loszusatzstatus_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_loszusatzstatus
    ADD CONSTRAINT fk_fert_loszusatzstatus_zusatzstatus_i_id_fert_zusatzstatus_i_i FOREIGN KEY (zusatzstatus_i_id) REFERENCES public.fert_zusatzstatus(i_id);
ALTER TABLE ONLY public.fert_pruefergebins
    ADD CONSTRAINT "fk_fert_pruefergebins_losablieferung_i_id_$loal_i_id" FOREIGN KEY (losablieferung_i_id) REFERENCES public.fert_losablieferung(i_id);
ALTER TABLE ONLY public.fert_pruefergebins
    ADD CONSTRAINT "fk_fert_pruefergebins_lospruefplan_i_id_$lppl_i_id" FOREIGN KEY (lospruefplan_i_id) REFERENCES public.fert_lospruefplan(i_id);
ALTER TABLE ONLY public.fert_pruefergebins
    ADD CONSTRAINT "fk_fert_pruefergebins_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_pruefergebins
    ADD CONSTRAINT "fk_fert_pruefergebins_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_wiederholendelose
    ADD CONSTRAINT fk_fert_wiederholendelose_lager_i_id_ziel_ww_lager_i_id FOREIGN KEY (lager_i_id_ziel) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.fert_wiederholendelose
    ADD CONSTRAINT fk_fert_wiederholendelose_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fert_wiederholendelose
    ADD CONSTRAINT fk_fert_wiederholendelose_stueckliste_i_id_stk_stueckliste_i_id FOREIGN KEY (stueckliste_i_id) REFERENCES public.stk_stueckliste(i_id);
ALTER TABLE ONLY public.fert_zusatzstatus
    ADD CONSTRAINT fk_fert_zusatzstatus_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.fert_internebestellung
    ADD CONSTRAINT "fk_ftib$_partner_i_id_standort_part_partner_i_id" FOREIGN KEY (partner_i_id_standort) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.is_anlage
    ADD CONSTRAINT fk_is_anlage_halle_i_id_is_halle_i_id FOREIGN KEY (halle_i_id) REFERENCES public.is_halle(i_id);
ALTER TABLE ONLY public.is_anlage
    ADD CONSTRAINT fk_is_anlage_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.is_geraet
    ADD CONSTRAINT fk_is_geraet_anlage_i_id_is_anlage_i_id FOREIGN KEY (anlage_i_id) REFERENCES public.is_anlage(i_id);
ALTER TABLE ONLY public.is_geraet
    ADD CONSTRAINT fk_is_geraet_geraetetyp_i_id_is_geraetetyp_i_id FOREIGN KEY (geraetetyp_i_id) REFERENCES public.is_geraetetyp(i_id);
ALTER TABLE ONLY public.is_geraet
    ADD CONSTRAINT fk_is_geraet_gewerk_i_id_is_gewerk_i_id FOREIGN KEY (gewerk_i_id) REFERENCES public.is_gewerk(i_id);
ALTER TABLE ONLY public.is_geraet
    ADD CONSTRAINT fk_is_geraet_halle_i_id_is_halle_i_id FOREIGN KEY (halle_i_id) REFERENCES public.is_halle(i_id);
ALTER TABLE ONLY public.is_geraet
    ADD CONSTRAINT fk_is_geraet_hersteller_i_id_ww_hersteller_i_id FOREIGN KEY (hersteller_i_id) REFERENCES public.ww_hersteller(i_id);
ALTER TABLE ONLY public.is_geraet
    ADD CONSTRAINT fk_is_geraet_ismaschine_i_id_ismaschine_i_id FOREIGN KEY (ismaschine_i_id) REFERENCES public.is_ismaschine(i_id);
ALTER TABLE ONLY public.is_geraet
    ADD CONSTRAINT fk_is_geraet_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.is_geraet
    ADD CONSTRAINT fk_is_geraet_standort_i_id_is_standort_i_id FOREIGN KEY (standort_i_id) REFERENCES public.is_standort(i_id);
ALTER TABLE ONLY public.is_geraetehistorie
    ADD CONSTRAINT fk_is_geraetehistorie_geraet_i_id_is_geraet_i_id FOREIGN KEY (geraet_i_id) REFERENCES public.is_geraet(i_id);
ALTER TABLE ONLY public.is_geraetehistorie
    ADD CONSTRAINT "fk_is_geraetehistorie_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.is_geraetehistorie
    ADD CONSTRAINT "fk_is_geraetehistorie_personal_i_id_techniker_pepe$_i_id" FOREIGN KEY (personal_i_id_techniker) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.is_geraetetyp
    ADD CONSTRAINT fk_is_geraetetyp_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.is_gewerk
    ADD CONSTRAINT fk_is_gewerk_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.is_halle
    ADD CONSTRAINT fk_is_halle_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.is_halle
    ADD CONSTRAINT fk_is_halle_standort_i_id_is_standort_i_id FOREIGN KEY (standort_i_id) REFERENCES public.is_standort(i_id);
ALTER TABLE ONLY public.is_instandhaltung
    ADD CONSTRAINT fk_is_instandhaltung_kategorie_i_id_is_kategorie_i_id FOREIGN KEY (kategorie_i_id) REFERENCES public.is_kategorie(i_id);
ALTER TABLE ONLY public.is_instandhaltung
    ADD CONSTRAINT fk_is_instandhaltung_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.is_ismaschine
    ADD CONSTRAINT fk_is_ismaschine_anlage_i_id_is_anlage_i_id FOREIGN KEY (anlage_i_id) REFERENCES public.is_anlage(i_id);
ALTER TABLE ONLY public.is_ismaschine
    ADD CONSTRAINT fk_is_ismaschine_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.is_kategorie
    ADD CONSTRAINT fk_is_kategorie_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.is_standort
    ADD CONSTRAINT "fk_is_standort_ansprechpartner_i_id_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.is_standort
    ADD CONSTRAINT fk_is_standort_auftrag_i_id_is_standort_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.is_standort
    ADD CONSTRAINT fk_is_standort_instandhaltung_i_id_is_instandhaltung_i_id FOREIGN KEY (instandhaltung_i_id) REFERENCES public.is_instandhaltung(i_id);
ALTER TABLE ONLY public.is_standort
    ADD CONSTRAINT fk_is_standort_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.is_standorttechniker
    ADD CONSTRAINT fk_is_standorttechniker_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.is_standorttechniker
    ADD CONSTRAINT fk_is_standorttechniker_standort_i_id_is_standort_i_id FOREIGN KEY (standort_i_id) REFERENCES public.is_standort(i_id);
ALTER TABLE ONLY public.is_wartungsschritte
    ADD CONSTRAINT fk_is_wartungsliste_artikel_i_id_is_geraet_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.is_wartungsliste
    ADD CONSTRAINT fk_is_wartungsliste_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.is_wartungsliste
    ADD CONSTRAINT fk_is_wartungsliste_geraet_i_id_is_geraet_i_id FOREIGN KEY (geraet_i_id) REFERENCES public.is_geraet(i_id);
ALTER TABLE ONLY public.is_wartungsliste
    ADD CONSTRAINT fk_is_wartungsliste_personal_i_id_veraltet_pers_personal_i_id FOREIGN KEY (personal_i_id_veraltet) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.is_wartungsschritte
    ADD CONSTRAINT fk_is_wartungsschritte_geraet_i_id_is_geraet_i_id FOREIGN KEY (geraet_i_id) REFERENCES public.is_geraet(i_id);
ALTER TABLE ONLY public.is_wartungsschritte
    ADD CONSTRAINT fk_is_wartungsschritte_lieferant_i_id_part_lieferant_i_id FOREIGN KEY (lieferant_i_id) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.is_wartungsschritte
    ADD CONSTRAINT "fk_is_wartungsschritte_personalgruppe_i_id_pegr$_i_id" FOREIGN KEY (personalgruppe_i_id) REFERENCES public.pers_personalgruppe(i_id);
ALTER TABLE ONLY public.is_wartungsschritte
    ADD CONSTRAINT "fk_isws$_auftragwiederholungsintervall_c_nr_abwi$_c_nr" FOREIGN KEY (auftragwiederholungsintervall_c_nr) REFERENCES public.auft_auftragwiederholungsintervall(c_nr);
ALTER TABLE ONLY public.iv_inseratrechnungartikel
    ADD CONSTRAINT "fk_iv_inar$_inseratrechnung_i_id_iv_inseratrechnung_i_id" FOREIGN KEY (inseratrechnung_i_id) REFERENCES public.iv_inseratrechnung(i_id);
ALTER TABLE ONLY public.iv_inseratrechnungartikel
    ADD CONSTRAINT "fk_iv_inar$_rechnungposition_i_id_rech_rechnungposition_i_id" FOREIGN KEY (rechnungposition_i_id) REFERENCES public.rech_rechnungposition(i_id);
ALTER TABLE ONLY public.iv_inserat
    ADD CONSTRAINT "fk_iv_inserat_ansprechpartner_i_id_lieferant_$paan_i_id" FOREIGN KEY (ansprechpartner_i_id_lieferant) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.iv_inserat
    ADD CONSTRAINT fk_iv_inserat_artikel_i_id_inseratart_ww_artikel_i_id FOREIGN KEY (artikel_i_id_inseratart) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.iv_inserat
    ADD CONSTRAINT fk_iv_inserat_bestellposition_i_id FOREIGN KEY (bestellposition_i_id) REFERENCES public.bes_bestellposition(i_id);
ALTER TABLE ONLY public.iv_inserat
    ADD CONSTRAINT fk_iv_inserat_lieferant_i_id_part_lieferant_i_id FOREIGN KEY (lieferant_i_id) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.iv_inserat
    ADD CONSTRAINT fk_iv_inserat_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.iv_inserat
    ADD CONSTRAINT "fk_iv_inserat_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.iv_inserat
    ADD CONSTRAINT "fk_iv_inserat_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.iv_inserat
    ADD CONSTRAINT "fk_iv_inserat_personal_i_id_erschienen_pepe$_i_id" FOREIGN KEY (personal_i_id_erschienen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.iv_inserat
    ADD CONSTRAINT "fk_iv_inserat_personal_i_id_gestoppt_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_gestoppt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.iv_inserat
    ADD CONSTRAINT "fk_iv_inserat_personal_i_id_manuellerledigt_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_manuellerledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.iv_inserat
    ADD CONSTRAINT "fk_iv_inserat_personal_i_id_manuellverrechenbar_pepe$_i_id" FOREIGN KEY (personal_i_id_manuellverrechnen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.iv_inserat
    ADD CONSTRAINT "fk_iv_inserat_personal_i_id_verrechnen_pepe$_i_id" FOREIGN KEY (personal_i_id_verrechnen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.iv_inserat
    ADD CONSTRAINT "fk_iv_inserat_personal_i_id_vertreter_pepe$_i_id" FOREIGN KEY (personal_i_id_vertreter) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.iv_inserat
    ADD CONSTRAINT fk_iv_inserat_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.iv_inseratartikel
    ADD CONSTRAINT fk_iv_inseratartikel_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.iv_inseratartikel
    ADD CONSTRAINT "fk_iv_inseratartikel_bestellposition_i_id_bepo$_i_id" FOREIGN KEY (bestellposition_i_id) REFERENCES public.bes_bestellposition(i_id);
ALTER TABLE ONLY public.iv_inseratartikel
    ADD CONSTRAINT fk_iv_inseratartikel_inserat_i_id_iv_inserat_i_id FOREIGN KEY (inserat_i_id) REFERENCES public.iv_inserat(i_id);
ALTER TABLE ONLY public.iv_inserater
    ADD CONSTRAINT "fk_iv_inserater_eingangsrechnung_i_id_$erer_i_id" FOREIGN KEY (eingangsrechnung_i_id) REFERENCES public.er_eingangsrechnung(i_id);
ALTER TABLE ONLY public.iv_inserater
    ADD CONSTRAINT fk_iv_inserater_inserat_i_id_iv_inserat_i_id FOREIGN KEY (inserat_i_id) REFERENCES public.iv_inserat(i_id);
ALTER TABLE ONLY public.iv_inseratrechnung
    ADD CONSTRAINT "fk_iv_inseratrechnung_ansprechpartner_i_id_$paan_i_id" FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.iv_inseratrechnung
    ADD CONSTRAINT fk_iv_inseratrechnung_inserat_i_id_iv_inserat_i_id FOREIGN KEY (inserat_i_id) REFERENCES public.iv_inserat(i_id);
ALTER TABLE ONLY public.iv_inseratrechnung
    ADD CONSTRAINT fk_iv_inseratrechnung_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.iv_inseratrechnung
    ADD CONSTRAINT "fk_iv_inseratrechnung_rechnungposition_i_id_$repo_i_id" FOREIGN KEY (rechnungposition_i_id) REFERENCES public.rech_rechnungposition(i_id);
ALTER TABLE ONLY public.fb_kontolaenderart
    ADD CONSTRAINT fk_kontolaenderart_finanzamt_i_id_lp_finanzamt_partner_i_id FOREIGN KEY (finanzamt_i_id, mandant_c_nr) REFERENCES public.lp_finanzamt(partner_i_id, mandant_c_nr);
ALTER TABLE ONLY public.kue_bedienerlager
    ADD CONSTRAINT fk_kue_bedienerlager_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.kue_kassaimport
    ADD CONSTRAINT fk_kue_kassaimport_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.kue_kassaimport
    ADD CONSTRAINT fk_kue_kassaimport_speiseplan_i_id_kue_speiseplan_i_id FOREIGN KEY (speiseplan_i_id) REFERENCES public.kue_speiseplan(i_id);
ALTER TABLE ONLY public.kue_kuecheumrechnung
    ADD CONSTRAINT fk_kue_kuecheumrechnung_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.kue_speiseplan
    ADD CONSTRAINT "fk_kue_speiseplan_fertigungsgruppe_i_id_stfg$_i_id" FOREIGN KEY (fertigungsgruppe_i_id) REFERENCES public.stk_fertigungsgruppe(i_id);
ALTER TABLE ONLY public.kue_speiseplan
    ADD CONSTRAINT fk_kue_speiseplan_kassaartikel_i_id_kue_kassaartikel_i_id FOREIGN KEY (kassaartikel_i_id) REFERENCES public.kue_kassaartikel(i_id);
ALTER TABLE ONLY public.kue_speiseplan
    ADD CONSTRAINT fk_kue_speiseplan_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.kue_speiseplan
    ADD CONSTRAINT fk_kue_speiseplan_stueckliste_i_id_stk_stueckliste_i_id FOREIGN KEY (stueckliste_i_id) REFERENCES public.stk_stueckliste(i_id);
ALTER TABLE ONLY public.kue_speiseplanposition
    ADD CONSTRAINT fk_kue_speiseplanposition_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.kue_speiseplanposition
    ADD CONSTRAINT fk_kue_speiseplanposition_speiseplan_i_id_kue_speiseplan_i_id FOREIGN KEY (speiseplan_i_id) REFERENCES public.kue_speiseplan(i_id);
ALTER TABLE ONLY public.kue_tageslos
    ADD CONSTRAINT fk_kue_tageslos_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.kue_tageslos
    ADD CONSTRAINT fk_kue_tageslos_lager_i_id_abbuchung_ww_lager_i_id FOREIGN KEY (lager_i_id_abbuchung) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.fert_internebestellung
    ADD CONSTRAINT "fk_loib$_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.fert_losistmaterial
    ADD CONSTRAINT "fk_loim$_lossollmaterial_i_id_fert_lossollmaterial_i_id" FOREIGN KEY (lossollmaterial_i_id) REFERENCES public.fert_lossollmaterial(i_id);
ALTER TABLE ONLY public.fert_lossollarbeitsplan
    ADD CONSTRAINT "fk_losa$_apkommentar_i_id_stk_apkommentar_i_id" FOREIGN KEY (apkommentar_i_id) REFERENCES public.stk_apkommentar(i_id);
ALTER TABLE ONLY public.fert_lossollmaterial
    ADD CONSTRAINT "fk_loso$_lossollmaterial_i_id_original_loso$_i_id" FOREIGN KEY (lossollmaterial_i_id_original) REFERENCES public.fert_lossollmaterial(i_id);
ALTER TABLE ONLY public.fert_lossollarbeitsplan
    ADD CONSTRAINT "fk_losp$_artikel_i_id_taetigkeit_ww_artikel_i_id" FOREIGN KEY (artikel_i_id_taetigkeit) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.fert_lossollarbeitsplan
    ADD CONSTRAINT "fk_losp$_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_anwender
    ADD CONSTRAINT fk_lp_anwender_mandant_c_nr_hauptmandant_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr_hauptmandant) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_anwender
    ADD CONSTRAINT fk_lp_anwender_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_arbeitsplatz
    ADD CONSTRAINT fk_lp_arbeitsplatz_c_typ_lp_arbeitsplatztyp_c_nr FOREIGN KEY (c_typ) REFERENCES public.lp_arbeitsplatztyp(c_nr);
ALTER TABLE ONLY public.lp_arbeitsplatzkonfiguration
    ADD CONSTRAINT fk_lp_arbeitsplatzkonfiguration_arbeitsplatz_i_id_lp_arbeitspla FOREIGN KEY (arbeitsplatz_i_id) REFERENCES public.lp_arbeitsplatz(i_id);
ALTER TABLE ONLY public.lp_arbeitsplatzparameter
    ADD CONSTRAINT fk_lp_arbeitsplatzparameter_parameter_c_nr_lp_parameter_c_nr FOREIGN KEY (parameter_c_nr) REFERENCES public.lp_parameter(c_nr);
ALTER TABLE ONLY public.lp_automatikjobs
    ADD CONSTRAINT fk_lp_automatikjobtype_i_id FOREIGN KEY (i_automatikjobtype_iid) REFERENCES public.lp_automatikjobtype(i_id);
ALTER TABLE ONLY public.lp_belegartdokument
    ADD CONSTRAINT fk_lp_belegartdokument_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.lp_belegartdokument
    ADD CONSTRAINT fk_lp_belegartdokument_dokument_i_id_lp_dokument_i_id FOREIGN KEY (dokument_i_id) REFERENCES public.lp_dokument(i_id);
ALTER TABLE ONLY public.lp_belegartdokument
    ADD CONSTRAINT fk_lp_belegartdokument_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_belegartmedia
    ADD CONSTRAINT fk_lp_belegartmedia_datenformat_c_nr_lp_datenformat_c_nr FOREIGN KEY (datenformat_c_nr) REFERENCES public.lp_datenformat(c_nr);
ALTER TABLE ONLY public.lp_belegartspr
    ADD CONSTRAINT fk_lp_belegartspr_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.lp_belegartspr
    ADD CONSTRAINT fk_lp_belegartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.lp_direkthilfe
    ADD CONSTRAINT fk_lp_direkthilfe_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_direkthilfe
    ADD CONSTRAINT fk_lp_direkthilfe_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_dokument
    ADD CONSTRAINT fk_lp_dokument_datenformat_c_nr_lp_datenformat_c_nr FOREIGN KEY (datenformat_c_nr) REFERENCES public.lp_datenformat(c_nr);
ALTER TABLE ONLY public.lp_dokument
    ADD CONSTRAINT fk_lp_dokument_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_dokumentbelegart
    ADD CONSTRAINT fk_lp_dokumentbelegart_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_dokumentenlink
    ADD CONSTRAINT fk_lp_dokumentenlink_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.lp_dokumentenlink
    ADD CONSTRAINT fk_lp_dokumentenlink_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_dokumentenlink
    ADD CONSTRAINT fk_lp_dokumentenlink_recht_c_nr_pers_recht_c_nr FOREIGN KEY (recht_c_nr) REFERENCES public.pers_recht(c_nr);
ALTER TABLE ONLY public.lp_dokumentgruppierung
    ADD CONSTRAINT fk_lp_dokumentgruppierung_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_dokumentnichtarchiviert
    ADD CONSTRAINT fk_lp_dokumentnichtarchiviert_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_dokumentschlagwort
    ADD CONSTRAINT fk_lp_dokumentschlagwort_dokument_i_id_lp_dokument_i_id FOREIGN KEY (dokument_i_id) REFERENCES public.lp_dokument(i_id);
ALTER TABLE ONLY public.lp_editor_base_block
    ADD CONSTRAINT fk_lp_editor_base_block_editor_content_id_lp_editor_content_i_i FOREIGN KEY (editor_content_id) REFERENCES public.lp_editor_content(i_id);
ALTER TABLE ONLY public.lp_editor_content
    ADD CONSTRAINT fk_lp_editor_content_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.lp_editor_image_block
    ADD CONSTRAINT fk_lp_editor_image_block_i_id_lp_editor_base_block_i_id FOREIGN KEY (i_id) REFERENCES public.lp_editor_base_block(i_id);
ALTER TABLE ONLY public.lp_editor_image_block
    ADD CONSTRAINT fk_lp_editor_image_block_lp_image_i_id_lp_image_i_id FOREIGN KEY (lp_image_i_id) REFERENCES public.lp_image(i_id);
ALTER TABLE ONLY public.lp_editor_text_block
    ADD CONSTRAINT fk_lp_editor_text_block_i_id_lp_editor_base_block_i_id FOREIGN KEY (i_id) REFERENCES public.lp_editor_base_block(i_id);
ALTER TABLE ONLY public.lp_einheit
    ADD CONSTRAINT fk_lp_einheit_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_einheit
    ADD CONSTRAINT fk_lp_einheit_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_einheitkonvertierung
    ADD CONSTRAINT fk_lp_einheitkonvertierung_einheit_c_nr_von_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr_von) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.lp_einheitkonvertierung
    ADD CONSTRAINT fk_lp_einheitkonvertierung_einheit_c_nr_zu_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr_zu) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.lp_einheitkonvertierung
    ADD CONSTRAINT "fk_lp_einheitkonvertierung_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_einheitkonvertierung
    ADD CONSTRAINT "fk_lp_einheitkonvertierung_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_einheitspr
    ADD CONSTRAINT fk_lp_einheitspr_einheit_c_nr_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.lp_einheitspr
    ADD CONSTRAINT fk_lp_einheitspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.lp_entitylog
    ADD CONSTRAINT fk_lp_entitylog_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.lp_entitylog
    ADD CONSTRAINT fk_lp_entitylog_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_extraliste
    ADD CONSTRAINT fk_lp_extraliste_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.lp_finanzamt
    ADD CONSTRAINT fk_lp_finanzamt_konto_i_id_ebdebitoren_fb_konto_i_id FOREIGN KEY (konto_i_id_ebdebitoren) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.lp_finanzamt
    ADD CONSTRAINT fk_lp_finanzamt_konto_i_id_ebkreditoren_fb_konto_i_id FOREIGN KEY (konto_i_id_ebkreditoren) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.lp_finanzamt
    ADD CONSTRAINT fk_lp_finanzamt_konto_i_id_ebsachkonten_fb_konto_i_id FOREIGN KEY (konto_i_id_ebsachkonten) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.lp_finanzamt
    ADD CONSTRAINT fk_lp_finanzamt_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_finanzamt
    ADD CONSTRAINT fk_lp_finanzamt_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.lp_finanzamt
    ADD CONSTRAINT fk_lp_finanzamt_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_finanzamt
    ADD CONSTRAINT fk_lp_finanzamt_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_funktionspr
    ADD CONSTRAINT fk_lp_funktionspr_funktion_i_id_lp_funktion_i_id FOREIGN KEY (funktion_i_id) REFERENCES public.lp_funktion(i_id);
ALTER TABLE ONLY public.lp_funktionspr
    ADD CONSTRAINT fk_lp_funktionspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.lp_geschaeftsjahr
    ADD CONSTRAINT fk_lp_geschaeftsjahr_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_geschaeftsjahr
    ADD CONSTRAINT "fk_lp_geschaeftsjahr_personal_i_id_sperre_pepe$_i_id" FOREIGN KEY (personal_i_id_sperre) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_geschaeftsjahrmandant
    ADD CONSTRAINT "fk_lp_geschaeftsjahrmandant_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_geschaeftsjahrmandant
    ADD CONSTRAINT "fk_lp_geschaeftsjahrmandant_personal_i_id_sperre_pepe$_i_id" FOREIGN KEY (personal_i_id_sperre) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_hardwareartspr
    ADD CONSTRAINT fk_lp_hardwareartspr_hardwareart_c_nr_lp_hardwareart_c_nr FOREIGN KEY (hardwareart_c_nr) REFERENCES public.lp_hardwareart(c_nr);
ALTER TABLE ONLY public.lp_hardwareartspr
    ADD CONSTRAINT fk_lp_hardwareartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.lp_hvmausercount
    ADD CONSTRAINT fk_lp_hvmausercount_hvmalizenz_i_id_pers_hvmalizenz_i_id FOREIGN KEY (hvmalizenz_i_id) REFERENCES public.pers_hvmalizenz(i_id);
ALTER TABLE ONLY public.lp_image
    ADD CONSTRAINT fk_lp_image_datenformat_c_nr_lp_datenformat_c_nr FOREIGN KEY (datenformat_c_nr) REFERENCES public.lp_datenformat(c_nr);
ALTER TABLE ONLY public.lp_internekopie
    ADD CONSTRAINT fk_lp_internekopie_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.lp_internekopie
    ADD CONSTRAINT fk_lp_internekopie_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.lp_internekopie
    ADD CONSTRAINT fk_lp_internekopie_einheit_c_nr_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.lp_internekopie
    ADD CONSTRAINT fk_lp_internekopie_mediastandard_i_id_lp_mediastandard_i_id FOREIGN KEY (mediastandard_i_id) REFERENCES public.lp_mediastandard(i_id);
ALTER TABLE ONLY public.lp_internekopie
    ADD CONSTRAINT fk_lp_internekopie_montageart_i_id_stk_montageart_i_id FOREIGN KEY (montageart_i_id) REFERENCES public.stk_montageart(i_id);
ALTER TABLE ONLY public.lp_internekopie
    ADD CONSTRAINT fk_lp_internekopie_mwstsatz_i_id_lp_mwstsatz_i_id FOREIGN KEY (mwstsatz_i_id) REFERENCES public.lp_mwstsatz(i_id);
ALTER TABLE ONLY public.lp_internekopie
    ADD CONSTRAINT fk_lp_internekopie_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_internekopie
    ADD CONSTRAINT fk_lp_internekopie_positionsart_c_nr_lp_positionsart_c_nr FOREIGN KEY (positionsart_c_nr) REFERENCES public.lp_positionsart(c_nr);
ALTER TABLE ONLY public.lp_internekopie
    ADD CONSTRAINT fk_lp_internekopie_theclient_c_nr_lp_theclient_c_nr FOREIGN KEY (theclient_c_nr) REFERENCES public.lp_theclient(c_nr);
ALTER TABLE ONLY public.lp_kennungspr
    ADD CONSTRAINT fk_lp_kennungspr_kennung_i_id_lp_kennung_i_id FOREIGN KEY (kennung_i_id) REFERENCES public.lp_kennung(i_id);
ALTER TABLE ONLY public.lp_kennungspr
    ADD CONSTRAINT fk_lp_kennungspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.lp_kostenstelle
    ADD CONSTRAINT fk_lp_kostenstelle_konto_i_id_fb_konto_i_id FOREIGN KEY (konto_i_id) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.lp_kostenstelle
    ADD CONSTRAINT fk_lp_kostenstelle_lager_i_id_ohneabbuchung_ww_lager_i_id FOREIGN KEY (lager_i_id_ohneabbuchung) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.lp_kostenstelle
    ADD CONSTRAINT fk_lp_kostenstelle_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_lp_kostenstelle_part_kunde_kostenstelle_i_id_2 FOREIGN KEY (kostenstelle_i_id, mandant_c_nr) REFERENCES public.lp_kostenstelle(i_id, mandant_c_nr);
ALTER TABLE ONLY public.lp_kostenstelle
    ADD CONSTRAINT fk_lp_kostenstelle_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_kostenstelle
    ADD CONSTRAINT fk_lp_kostenstelle_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_kostentraeger
    ADD CONSTRAINT fk_lp_kostentraeger_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_land
    ADD CONSTRAINT fk_lp_land_land_i_id_gemeinsamespostland_lp_land_i_id FOREIGN KEY (land_i_id_gemeinsamespostland) REFERENCES public.lp_land(i_id);
ALTER TABLE ONLY public.lp_land
    ADD CONSTRAINT fk_lp_land_waehrung_c_nr_lp_waehrung_c_nr FOREIGN KEY (waehrung_c_nr) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.lp_landplzort
    ADD CONSTRAINT fk_lp_landplzort_land_i_id_lp_land_i_id FOREIGN KEY (land_i_id) REFERENCES public.lp_land(i_id);
ALTER TABLE ONLY public.lp_landplzort
    ADD CONSTRAINT fk_lp_landplzort_ort_i_id_lp_ort_i_id FOREIGN KEY (ort_i_id) REFERENCES public.lp_ort(i_id);
ALTER TABLE ONLY public.lp_landspr
    ADD CONSTRAINT fk_lp_landspr_land_i_id_lp_land_i_id FOREIGN KEY (land_i_id) REFERENCES public.lp_land(i_id);
ALTER TABLE ONLY public.lp_landspr
    ADD CONSTRAINT fk_lp_landspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.lp_lieferart
    ADD CONSTRAINT fk_lp_lieferart_artikel_i_id_versand_ww_artikel_i_id FOREIGN KEY (artikel_i_id_versand) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.lp_lieferart
    ADD CONSTRAINT fk_lp_lieferart_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_lieferartspr
    ADD CONSTRAINT fk_lp_lieferartspr_lieferart_i_id_lp_lieferart_i_id FOREIGN KEY (lieferart_i_id) REFERENCES public.lp_lieferart(i_id);
ALTER TABLE ONLY public.lp_lieferartspr
    ADD CONSTRAINT fk_lp_lieferartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.lp_mailproperty
    ADD CONSTRAINT fk_lp_mailproperty_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_mailproperty
    ADD CONSTRAINT fk_lp_mailproperty_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_kunde_i_id_stueckliste_stk_stueckliste_i_id FOREIGN KEY (kunde_i_id_stueckliste) REFERENCES public.stk_stueckliste(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_lager_i_id_ziellager_ww_lager_i_id FOREIGN KEY (lager_i_id_ziellager) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_lieferart_i_id_kunde_lp_lieferart_i_id FOREIGN KEY (lieferart_i_id_kunde) REFERENCES public.lp_lieferart(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_lieferart_i_id_lieferant_lp_lieferart_i_id FOREIGN KEY (lieferart_i_id_lieferant) REFERENCES public.lp_lieferart(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_part_partner FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_partner_i_id_finanzamt_lp_finanzamt_partner_i_id FOREIGN KEY (partner_i_id_finanzamt, c_nr) REFERENCES public.lp_finanzamt(partner_i_id, mandant_c_nr);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_partner_i_id_lieferadresse_part_partner_i_id FOREIGN KEY (partner_i_id_lieferadresse) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_spediteur_i_id_kunde_lp_spediteur_i_id FOREIGN KEY (spediteur_i_id_kunde) REFERENCES public.lp_spediteur(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_spediteur_i_id_lieferant_lp_spediteur_i_id FOREIGN KEY (spediteur_i_id_lieferant) REFERENCES public.lp_spediteur(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_verrechnungsmodell_i_id_rech_verrechnungsmodell_i FOREIGN KEY (verrechnungsmodell_i_id) REFERENCES public.rech_verrechnungsmodell(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_waehrung_c_nr_lp_waehrung_c_nr FOREIGN KEY (waehrung_c_nr) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_zahlungsziel_i_id_anzahlung_lp_zahlungsziel_i_id FOREIGN KEY (zahlungsziel_i_id_anzahlung) REFERENCES public.lp_zahlungsziel(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_zahlungsziel_i_id_kunde_lp_zahlungsziel_i_id FOREIGN KEY (zahlungsziel_i_id_kunde) REFERENCES public.lp_zahlungsziel(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_lp_mandant_zahlungsziel_i_id_lieferant_lp_zahlungsziel_i_id FOREIGN KEY (zahlungsziel_i_id_lieferant) REFERENCES public.lp_zahlungsziel(i_id);
ALTER TABLE ONLY public.lp_mandantagbspr
    ADD CONSTRAINT fk_lp_mandantagbspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.lp_mandantagbspr
    ADD CONSTRAINT fk_lp_mandantagbspr_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_mediaartspr
    ADD CONSTRAINT fk_lp_mediaartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.lp_mediaartspr
    ADD CONSTRAINT fk_lp_mediaartspr_mediaart_c_nr_lp_mediaart_c_nr FOREIGN KEY (mediaart_c_nr) REFERENCES public.lp_mediaart(c_nr);
ALTER TABLE ONLY public.lp_mediastandard
    ADD CONSTRAINT fk_lp_mediastandard_datenformat_c_nr_lp_datenformat_c_nr FOREIGN KEY (datenformat_c_nr) REFERENCES public.lp_datenformat(c_nr);
ALTER TABLE ONLY public.lp_mediastandard
    ADD CONSTRAINT fk_lp_mediastandard_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.lp_mediastandard
    ADD CONSTRAINT fk_lp_mediastandard_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_mediastandard
    ADD CONSTRAINT fk_lp_mediastandard_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_mediastandard
    ADD CONSTRAINT fk_lp_mediastandard_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_modulberechtigung
    ADD CONSTRAINT fk_lp_modulberechtigung_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.lp_modulberechtigung
    ADD CONSTRAINT fk_lp_modulberechtigung_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_mwstsatz
    ADD CONSTRAINT fk_lp_mwstsatz_mwstsatzbez_i_id_lp_mwstsatzbez_i_id FOREIGN KEY (mwstsatzbez_i_id) REFERENCES public.lp_mwstsatzbez(i_id);
ALTER TABLE ONLY public.lp_mwstsatzbez
    ADD CONSTRAINT fk_lp_mwstsatzbez_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_lp_mwstsatzbez_part_kunde_mwstsatz_i_id_2 FOREIGN KEY (mwstsatz_i_id, mandant_c_nr) REFERENCES public.lp_mwstsatzbez(i_id, mandant_c_nr);
ALTER TABLE ONLY public.lp_mwstsatzcode
    ADD CONSTRAINT fk_lp_mwstsatzcode_mwstsatz_i_id_lp_mwstsatz_i_id FOREIGN KEY (mwstsatz_i_id) REFERENCES public.lp_mwstsatz(i_id);
ALTER TABLE ONLY public.lp_mwstsatzcode
    ADD CONSTRAINT fk_lp_mwstsatzcode_reversechargeart_i_id_fb_reversechargeart_i_ FOREIGN KEY (reversechargeart_i_id) REFERENCES public.fb_reversechargeart(i_id);
ALTER TABLE ONLY public.lp_ort
    ADD CONSTRAINT fk_lp_ort_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_panelbeschreibung
    ADD CONSTRAINT fk_lp_panelbeschreibung_artgru_i_id_ww_artgru_i_id FOREIGN KEY (artgru_i_id) REFERENCES public.ww_artgru(i_id);
ALTER TABLE ONLY public.lp_panelbeschreibung
    ADD CONSTRAINT fk_lp_panelbeschreibung_bereich_i_id_lp_proj_bereich_i_id FOREIGN KEY (bereich_i_id) REFERENCES public.proj_bereich(i_id);
ALTER TABLE ONLY public.lp_panelbeschreibung
    ADD CONSTRAINT fk_lp_panelbeschreibung_kostenstelle_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.lp_panelbeschreibung
    ADD CONSTRAINT fk_lp_panelbeschreibung_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_panelbeschreibung
    ADD CONSTRAINT fk_lp_panelbeschreibung_panel_c_nr_lp_panel_c_nr FOREIGN KEY (panel_c_nr) REFERENCES public.lp_panel(c_nr);
ALTER TABLE ONLY public.lp_panelbeschreibung
    ADD CONSTRAINT fk_lp_panelbeschreibung_partnerklasse_i_id_part_partnerklasse_i FOREIGN KEY (partnerklasse_i_id) REFERENCES public.part_partnerklasse(i_id);
ALTER TABLE ONLY public.lp_panelbeschreibung
    ADD CONSTRAINT fk_lp_panelbeschreibung_projekttyp_proj_projekttyp_c_nr FOREIGN KEY (projekttyp_c_nr, mandant_c_nr) REFERENCES public.proj_projekttyp(c_nr, mandant_c_nr);
ALTER TABLE ONLY public.lp_paneldaten
    ADD CONSTRAINT fk_lp_paneldaten_panel_c_nr_lp_panel_c_nr FOREIGN KEY (panel_c_nr) REFERENCES public.lp_panel(c_nr);
ALTER TABLE ONLY public.lp_paneldaten
    ADD CONSTRAINT "fk_lp_paneldaten_panelbeschreibung_i_id_lppb$_i_id" FOREIGN KEY (panelbeschreibung_i_id) REFERENCES public.lp_panelbeschreibung(i_id);
ALTER TABLE ONLY public.lp_panelsperren
    ADD CONSTRAINT fk_lp_panelsperren_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.lp_panelsperren
    ADD CONSTRAINT fk_lp_panelsperren_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_parameteranwender
    ADD CONSTRAINT "fk_lp_parameteranwender_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_parametermandant
    ADD CONSTRAINT fk_lp_parametermandant_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_parametermandant
    ADD CONSTRAINT fk_lp_parametermandant_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_positionsartspr
    ADD CONSTRAINT fk_lp_positionsartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.lp_positionsartspr
    ADD CONSTRAINT fk_lp_positionsartspr_positionsart_c_nr_lp_positionsart_c_nr FOREIGN KEY (positionsart_c_nr) REFERENCES public.lp_positionsart(c_nr);
ALTER TABLE ONLY public.lp_primarykey_belegnr
    ADD CONSTRAINT fk_lp_primarykey_belegnr_lp_primarykey FOREIGN KEY (c_nametabelle) REFERENCES public.lp_primarykey(c_name);
ALTER TABLE ONLY public.lp_primarykey_belegnr
    ADD CONSTRAINT fk_lp_primarykey_belegnr_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_protokoll
    ADD CONSTRAINT fk_lp_protokoll_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_reportkonf
    ADD CONSTRAINT fk_lp_reportkonf_standarddrucker_i_id_lp_standarddrucker_i_id FOREIGN KEY (standarddrucker_i_id) REFERENCES public.lp_standarddrucker(i_id);
ALTER TABLE ONLY public.lp_spediteur
    ADD CONSTRAINT "fk_lp_spediteur_ansprechpartner_i_id_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.lp_spediteur
    ADD CONSTRAINT fk_lp_spediteur_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_lp_spediteur_part_kunde_spediteur_i_id_2 FOREIGN KEY (spediteur_i_id, mandant_c_nr) REFERENCES public.lp_spediteur(i_id, mandant_c_nr);
ALTER TABLE ONLY public.lp_spediteur
    ADD CONSTRAINT fk_lp_spediteur_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.lp_spediteur
    ADD CONSTRAINT fk_lp_spediteur_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_standarddrucker
    ADD CONSTRAINT fk_lp_standardrucker_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_statusspr
    ADD CONSTRAINT fk_lp_statusspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.lp_statusspr
    ADD CONSTRAINT fk_lp_statusspr_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.lp_standarddrucker
    ADD CONSTRAINT fk_lp_stddrucker_reportvariante_i_id_lp_reportvariante_i_id FOREIGN KEY (reportvariante_i_id) REFERENCES public.lp_reportvariante(i_id);
ALTER TABLE ONLY public.lp_text
    ADD CONSTRAINT fk_lp_text_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.lp_text
    ADD CONSTRAINT fk_lp_text_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_thejudge
    ADD CONSTRAINT fk_lp_thejudge_personal_i_id_locker_pers_personal_i_id FOREIGN KEY (personal_i_id_locker) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_versandauftrag
    ADD CONSTRAINT fk_lp_versandauftrag_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.lp_versandanhang
    ADD CONSTRAINT fk_lp_versandauftrag_i_id FOREIGN KEY (versandauftrag_i_id) REFERENCES public.lp_versandauftrag(i_id);
ALTER TABLE ONLY public.lp_versandauftrag
    ADD CONSTRAINT fk_lp_versandauftrag_partner_i_id_empfaenger_part_partner_i_id FOREIGN KEY (partner_i_id_empfaenger) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.lp_versandauftrag
    ADD CONSTRAINT fk_lp_versandauftrag_partner_i_id_sender_part_partner_i_id FOREIGN KEY (partner_i_id_sender) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.lp_versandauftrag
    ADD CONSTRAINT fk_lp_versandauftrag_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_versandauftrag
    ADD CONSTRAINT fk_lp_versandauftrag_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_versandstatus(status_c_nr);
ALTER TABLE ONLY public.lp_versandstatus
    ADD CONSTRAINT fk_lp_versandstatus_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.lp_versandwegberechtigung
    ADD CONSTRAINT fk_lp_versandwegberechtigung_i_id_lp_versandweg_i_id FOREIGN KEY (i_id) REFERENCES public.lp_versandweg(i_id);
ALTER TABLE ONLY public.lp_versandwegberechtigung
    ADD CONSTRAINT fk_lp_versandwegberechtigung_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_versandwegcc
    ADD CONSTRAINT fk_lp_versandwegcc_i_id_lp_versandweg_i_id FOREIGN KEY (i_id) REFERENCES public.lp_versandweg(i_id);
ALTER TABLE ONLY public.lp_versandwegccpartner
    ADD CONSTRAINT fk_lp_versandwegccpartner_i_id_lp_versandweg_i_id FOREIGN KEY (versandweg_i_id) REFERENCES public.lp_versandweg(i_id);
ALTER TABLE ONLY public.lp_versandwegccpartner
    ADD CONSTRAINT fk_lp_versandwegccpartner_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.lp_versandwegpartner
    ADD CONSTRAINT fk_lp_versandwegpartner_i_id_lp_versandweg_i_id FOREIGN KEY (versandweg_i_id) REFERENCES public.lp_versandweg(i_id);
ALTER TABLE ONLY public.lp_versandwegpartner
    ADD CONSTRAINT fk_lp_versandwegpartner_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_versandwegpartner
    ADD CONSTRAINT fk_lp_versandwegpartner_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.lp_versandwegpartnercc
    ADD CONSTRAINT fk_lp_versandwegpartnercc_i_id_lp_versandwegpartner_i_id FOREIGN KEY (i_id) REFERENCES public.lp_versandwegpartner(i_id);
ALTER TABLE ONLY public.lp_versandwegpartnerdesadv
    ADD CONSTRAINT fk_lp_versandwegpartnerdesadv_i_id_lp_versandwegpartner_i_id FOREIGN KEY (i_id) REFERENCES public.lp_versandwegpartner(i_id);
ALTER TABLE ONLY public.lp_versandwegpartneredi4all
    ADD CONSTRAINT fk_lp_versandwegpartneredi4all_i_id_lp_versandwegpartner_i_id FOREIGN KEY (i_id) REFERENCES public.lp_versandwegpartner(i_id);
ALTER TABLE ONLY public.lp_versandwegpartnerlinienabruf
    ADD CONSTRAINT fk_lp_versandwegpartnerlinienabruf_i_id_lp_versandwegpartner_i_ FOREIGN KEY (i_id) REFERENCES public.lp_versandwegpartner(i_id);
ALTER TABLE ONLY public.lp_versandwegpartnerordrsp
    ADD CONSTRAINT fk_lp_versandwegpartnerordrsp_i_id_lp_versandwegpartner_i_id FOREIGN KEY (i_id) REFERENCES public.lp_versandwegpartner(i_id);
ALTER TABLE ONLY public.lp_waehrung
    ADD CONSTRAINT fk_lp_waehrung_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_waehrung
    ADD CONSTRAINT fk_lp_waehrung_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_wechselkurs
    ADD CONSTRAINT fk_lp_wechselkurs_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_wechselkurs
    ADD CONSTRAINT fk_lp_wechselkurs_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.lp_wechselkurs
    ADD CONSTRAINT fk_lp_wechselkurs_waehrung_c_nr_von_lp_waehrung_c_nr FOREIGN KEY (waehrung_c_nr_von) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.lp_wechselkurs
    ADD CONSTRAINT fk_lp_wechselkurs_waehrung_c_nr_zu_lp_waehrung_c_nr FOREIGN KEY (waehrung_c_nr_zu) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.lp_zahlungsziel
    ADD CONSTRAINT fk_lp_zahlungsziel_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_lp_zahlungsziel_part_kunde_zahlungsziel_i_id_2 FOREIGN KEY (zahlungsziel_i_id, mandant_c_nr) REFERENCES public.lp_zahlungsziel(i_id, mandant_c_nr);
ALTER TABLE ONLY public.lp_zahlungszielspr
    ADD CONSTRAINT fk_lp_zahlungszielspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.lp_zahlungszielspr
    ADD CONSTRAINT fk_lp_zahlungszielspr_zahlungsziel_i_id_lp_zahlungsziel_i_id FOREIGN KEY (zahlungsziel_i_id) REFERENCES public.lp_zahlungsziel(i_id);
ALTER TABLE ONLY public.lp_zusatzfunktionberechtigung
    ADD CONSTRAINT fk_lp_zusatzfunktionberechtigung_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.lp_arbeitsplatzparameter
    ADD CONSTRAINT "fk_lpap$_arbeitsplatz_i_id_lpaz$_i_id" FOREIGN KEY (arbeitsplatz_i_id) REFERENCES public.lp_arbeitsplatz(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT "fk_lpmd$_bankverbindung_i_id_mandant_fbbv$_i_id" FOREIGN KEY (bankverbindung_i_id_mandant) REFERENCES public.fb_bankverbindung(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT "fk_lpmd$_mwstsatz_i_id_standardauslandmwstsatz_lpmb$_i_id" FOREIGN KEY (mwstsatz_i_id_standardauslandmwstsatz) REFERENCES public.lp_mwstsatzbez(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT "fk_lpmd$_mwstsatz_i_id_standarddrittlandmwstsatz_lpmb$_i_id" FOREIGN KEY (mwstsatz_i_id_standarddrittlandmwstsatz) REFERENCES public.lp_mwstsatzbez(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT "fk_lpmd$_mwstsatz_i_id_standardinlandmwstsatz_lpmb$_i_id" FOREIGN KEY (mwstsatz_i_id_standardinlandmwstsatz) REFERENCES public.lp_mwstsatzbez(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT "fk_lpmd$_vkpfartikelpreisliste_i_id_wwvp$_i_id" FOREIGN KEY (vkpfartikelpreisliste_i_id) REFERENCES public.ww_vkpfartikelpreisliste(i_id);
ALTER TABLE ONLY public.lp_parametermandantgueltigab
    ADD CONSTRAINT "fk_lpmg$_uk_lp_parametermandant_uk" FOREIGN KEY (c_nr, mandant_c_nr, c_kategorie) REFERENCES public.lp_parametermandant(c_nr, mandant_c_nr, c_kategorie);
ALTER TABLE ONLY public.lp_zusatzfunktionberechtigung
    ADD CONSTRAINT "fk_lpzb$_zusatzfunktion_c_nr_lp_zusatzfunktion_c_nr" FOREIGN KEY (zusatzfunktion_c_nr) REFERENCES public.lp_zusatzfunktion(c_nr);
ALTER TABLE ONLY public.ls_ausliefervorschlag
    ADD CONSTRAINT fk_ls_ausliefervorschlag_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ls_ausliefervorschlag
    ADD CONSTRAINT fk_ls_ausliefervorschlag_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.ls_ausliefervorschlag
    ADD CONSTRAINT fk_ls_ausliefervorschlag_kunde_i_id_lieferadresse_part_kunde_i_ FOREIGN KEY (kunde_i_id_lieferadresse) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.ls_ausliefervorschlag
    ADD CONSTRAINT fk_ls_ausliefervorschlag_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.ls_ausliefervorschlag
    ADD CONSTRAINT fk_ls_ausliefervorschlag_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_begruendung_begruendung_i_id_ls_begruendung_i_id FOREIGN KEY (begruendung_i_id) REFERENCES public.ls_begruendung(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_auftrag_i_id_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_kunde_i_id_lieferadresse_part_kunde_i_id FOREIGN KEY (kunde_i_id_lieferadresse) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_kunde_i_id_rechnungsadresse_part_kunde_i_id FOREIGN KEY (kunde_i_id_rechnungsadresse) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_laenderart_c_nr_fb_laenderart_c_nr FOREIGN KEY (laenderart_c_nr) REFERENCES public.fb_laenderart(c_nr);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_lieferart_i_id_lp_lieferart_i_id FOREIGN KEY (lieferart_i_id) REFERENCES public.lp_lieferart(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_lieferscheinart_c_nr_ls_lieferscheinart_c_nr FOREIGN KEY (lieferscheinart_c_nr) REFERENCES public.ls_lieferscheinart(c_nr);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_personal_i_id_storniert_pers_personal_i_id FOREIGN KEY (personal_i_id_storniert) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_personal_i_id_vertreter_pers_personal_i_id FOREIGN KEY (personal_i_id_vertreter) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_projekt_i_id_proj_projekt_i_id FOREIGN KEY (projekt_i_id) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_rechnung_i_id_rech_rechnung_i_id FOREIGN KEY (rechnung_i_id) REFERENCES public.rech_rechnung(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_spediteur_i_id_lp_spediteur_i_id FOREIGN KEY (spediteur_i_id) REFERENCES public.lp_spediteur(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT fk_ls_lieferschein_zahlungsziel_i_id_lp_zahlungsziel_i_id FOREIGN KEY (zahlungsziel_i_id) REFERENCES public.lp_zahlungsziel(i_id);
ALTER TABLE ONLY public.ls_lieferscheinartspr
    ADD CONSTRAINT fk_ls_lieferscheinartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.ls_lieferscheinposition
    ADD CONSTRAINT fk_ls_lieferscheinposition_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ls_lieferscheinposition
    ADD CONSTRAINT fk_ls_lieferscheinposition_einheit_c_nr_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.ls_lieferscheinposition
    ADD CONSTRAINT fk_ls_lieferscheinposition_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.ls_lieferscheinposition
    ADD CONSTRAINT fk_ls_lieferscheinposition_mwstsatz_i_id_lp_mwstsatz_i_id FOREIGN KEY (mwstsatz_i_id) REFERENCES public.lp_mwstsatz(i_id);
ALTER TABLE ONLY public.ls_lieferscheinposition
    ADD CONSTRAINT fk_ls_lieferscheinposition_verleih_i_id_ww_verleih_i_id FOREIGN KEY (verleih_i_id) REFERENCES public.ww_verleih(i_id);
ALTER TABLE ONLY public.ls_lieferscheinstatus
    ADD CONSTRAINT fk_ls_lieferscheinstatus_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.ls_lieferscheintext
    ADD CONSTRAINT fk_ls_lieferscheintext_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.ls_lieferscheintext
    ADD CONSTRAINT fk_ls_lieferscheintext_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ls_lieferscheintext
    ADD CONSTRAINT fk_ls_lieferscheintext_mediaart_c_nr_lp_mediaart_c_nr FOREIGN KEY (mediaart_c_nr) REFERENCES public.lp_mediaart(c_nr);
ALTER TABLE ONLY public.ls_packstueck
    ADD CONSTRAINT fk_ls_packstueck_forecastposition_i_id FOREIGN KEY (forecastposition_i_id) REFERENCES public.fc_forecastposition(i_id);
ALTER TABLE ONLY public.ls_packstueck
    ADD CONSTRAINT fk_ls_packstueck_lieferschein_i_id_ls_lieferschein_i_id FOREIGN KEY (lieferschein_i_id) REFERENCES public.ls_lieferschein(i_id);
ALTER TABLE ONLY public.ls_packstueck
    ADD CONSTRAINT fk_ls_packstueck_los_i_id_fert_los_i_id FOREIGN KEY (los_i_id) REFERENCES public.fert_los(i_id);
ALTER TABLE ONLY public.ls_packstueck
    ADD CONSTRAINT "fk_ls_packstueck_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ls_verkettet
    ADD CONSTRAINT fk_ls_verkettet_lieferschein_i_id_ls_lieferschein_i_id FOREIGN KEY (lieferschein_i_id) REFERENCES public.ls_lieferschein(i_id);
ALTER TABLE ONLY public.ls_verkettet
    ADD CONSTRAINT fk_ls_verkettet_lieferschein_i_id_verkettet_ls_lieferschein_i_i FOREIGN KEY (lieferschein_i_id_verkettet) REFERENCES public.ls_lieferschein(i_id);
ALTER TABLE ONLY public.ls_lieferscheinposition
    ADD CONSTRAINT "fk_lslp$_wareneingangsposition_i_id_anderermandant_bewe$_i_id" FOREIGN KEY (wareneingangsposition_i_id_anderermandant) REFERENCES public.bes_wareneingangsposition(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT "fk_lsls$_ansprechpartner_i_id_kunde_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id_kunde) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT "fk_lsls$_ansprechpartner_i_id_rechnungsadresse_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id_rechnungsadresse) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT "fk_lsls$_eingangsrechnung_i_id_zollexport_erer$_i_id" FOREIGN KEY (eingangsrechnung_i_id_zollexport) REFERENCES public.er_eingangsrechnung(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT "fk_lsls$_lieferscheinstatus_c_nr_lsls$status_c_nr" FOREIGN KEY (lieferscheinstatus_c_nr) REFERENCES public.ls_lieferscheinstatus(status_c_nr);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT "fk_lsls$_lieferscheintext_i_id_fusstext_lsls$text_i_id" FOREIGN KEY (lieferscheintext_i_id_fusstext) REFERENCES public.ls_lieferscheintext(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT "fk_lsls$_lieferscheintext_i_id_kopftext_lsls$text_i_id" FOREIGN KEY (lieferscheintext_i_id_kopftext) REFERENCES public.ls_lieferscheintext(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT "fk_lsls$_personal_i_id_manuellerledigt_pepe$_i_id" FOREIGN KEY (personal_i_id_manuellerledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT "fk_lsls$_personal_i_id_zollexportpapier_pers_personal_i_id" FOREIGN KEY (personal_i_id_zollexportpapier) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT "fk_lsls$_waehrung_c_nr_lieferscheinwaehrung_lp_waehrung_c_nr" FOREIGN KEY (waehrung_c_nr_lieferscheinwaehrung) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.ls_lieferscheinartspr
    ADD CONSTRAINT "fk_lsls$artspr_lieferscheinart_c_nr_lsls$art_c_nr" FOREIGN KEY (lieferscheinart_c_nr) REFERENCES public.ls_lieferscheinart(c_nr);
ALTER TABLE ONLY public.ls_lieferscheinposition
    ADD CONSTRAINT "fk_lspo$_auftragposition_i_id_abpo$_i_id" FOREIGN KEY (auftragposition_i_id) REFERENCES public.auft_auftragposition(i_id);
ALTER TABLE ONLY public.ls_lieferscheinposition
    ADD CONSTRAINT "fk_lspo$_forecastposition_i_id_fcpo$_i_id" FOREIGN KEY (forecastposition_i_id) REFERENCES public.fc_forecastposition(i_id);
ALTER TABLE ONLY public.ls_lieferscheinposition
    ADD CONSTRAINT "fk_lspo$_kostentraeger_i_id_lp_kostentraeger_i_id" FOREIGN KEY (kostentraeger_i_id) REFERENCES public.lp_kostentraeger(i_id);
ALTER TABLE ONLY public.ls_lieferscheinposition
    ADD CONSTRAINT "fk_lspo$_lieferschein_i_id_lsls$_i_id" FOREIGN KEY (lieferschein_i_id) REFERENCES public.ls_lieferschein(i_id);
ALTER TABLE ONLY public.ls_lieferscheinposition
    ADD CONSTRAINT "fk_lspo$_lieferscheinpositionart_c_nr_lspo$art_c_nr" FOREIGN KEY (lieferscheinpositionart_c_nr) REFERENCES public.ls_lieferscheinpositionart(positionsart_c_nr);
ALTER TABLE ONLY public.ls_lieferscheinposition
    ADD CONSTRAINT "fk_lspo$_mediastandard_i_id_lp_mediastandard_i_id" FOREIGN KEY (mediastandard_i_id) REFERENCES public.lp_mediastandard(i_id);
ALTER TABLE ONLY public.ls_lieferscheinposition
    ADD CONSTRAINT "fk_lspo$_position_i_id_artikelset_lspo$_i_id" FOREIGN KEY (position_i_id_artikelset) REFERENCES public.ls_lieferscheinposition(i_id);
ALTER TABLE ONLY public.ls_lieferscheinposition
    ADD CONSTRAINT "fk_lspo$_position_i_id_zugehoerig_lspo$_i_id" FOREIGN KEY (position_i_id_zugehoerig) REFERENCES public.ls_lieferscheinposition(i_id);
ALTER TABLE ONLY public.ls_lieferscheinpositionart
    ADD CONSTRAINT "fk_lspo$art_positionsart_c_nr_lp_positionsart_c_nr" FOREIGN KEY (positionsart_c_nr) REFERENCES public.lp_positionsart(c_nr);
ALTER TABLE ONLY public.pers_maschinemaschinenzm
    ADD CONSTRAINT "fk_mamz$_maschinenzm_i_id_pers_maschinenzm_i_id" FOREIGN KEY (maschinenzm_i_id) REFERENCES public.pers_maschinenzm(i_id);
ALTER TABLE ONLY public.media_emailattachment
    ADD CONSTRAINT fk_media_emailattachment_media_iid_media_store_iid FOREIGN KEY (media_i_id) REFERENCES public.media_store(i_id);
ALTER TABLE ONLY public.media_inbox
    ADD CONSTRAINT fk_media_inbox_media_id_media_store_i_d FOREIGN KEY (media_i_id) REFERENCES public.media_store(i_id);
ALTER TABLE ONLY public.media_store
    ADD CONSTRAINT fk_media_store_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.media_storebeleg
    ADD CONSTRAINT fk_media_storebeleg_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.media_storebeleg
    ADD CONSTRAINT fk_media_storebeleg_media_iid_media_store_iid FOREIGN KEY (media_i_id) REFERENCES public.media_store(i_id);
ALTER TABLE ONLY public.lp_mwstsatzbez
    ADD CONSTRAINT fk_mwstsatzbez_finanzamt_i_id_lp_finanzamt_partner_i_id FOREIGN KEY (finanzamt_i_id, mandant_c_nr) REFERENCES public.lp_finanzamt(partner_i_id, mandant_c_nr);
ALTER TABLE ONLY public.part_ansprechpartnerfunktionspr
    ADD CONSTRAINT "fk_paaf$spr_ansprechpartnerfunktion_i_id_paaf$_i_id" FOREIGN KEY (ansprechpartnerfunktion_i_id) REFERENCES public.part_ansprechpartnerfunktion(i_id);
ALTER TABLE ONLY public.part_ansprechpartner
    ADD CONSTRAINT "fk_paap$_ansprechpartnerfunktion_i_id_paaf$_i_id" FOREIGN KEY (ansprechpartnerfunktion_i_id) REFERENCES public.part_ansprechpartnerfunktion(i_id);
ALTER TABLE ONLY public.part_ansprechpartner
    ADD CONSTRAINT "fk_paap$_newslettergrund_i_id_part_newslettergrund_c_nr" FOREIGN KEY (newslettergrund_i_id) REFERENCES public.part_newslettergrund(i_id);
ALTER TABLE ONLY public.part_ansprechpartner
    ADD CONSTRAINT "fk_paap$_partner_i_id_ansprechpartner_papa$_i_id" FOREIGN KEY (partner_i_id_ansprechpartner) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_ansprechpartner
    ADD CONSTRAINT "fk_paap$_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_kundesokomengenstaffel
    ADD CONSTRAINT "fk_pakm$_kundesoko_i_id_pakk$_i_id" FOREIGN KEY (kundesoko_i_id) REFERENCES public.part_kundesoko(i_id);
ALTER TABLE ONLY public.part_partnerkommunikation
    ADD CONSTRAINT "fk_pako$_kommunikationsart_c_nr_part_kommunikationsart_c_nr" FOREIGN KEY (kommunikationsart_c_nr) REFERENCES public.part_kommunikationsart(c_nr);
ALTER TABLE ONLY public.part_kommunikationsartspr
    ADD CONSTRAINT "fk_paks$_kommunikationsart_c_nr_part_kommunikationsart_c_nr" FOREIGN KEY (kommunikationsart_c_nr) REFERENCES public.part_kommunikationsart(c_nr);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT "fk_paku$_vkpfartikelpreisliste_i_id_stdpreisliste_wwvp$_i_id" FOREIGN KEY (vkpfartikelpreisliste_i_id_stdpreisliste, mandant_c_nr) REFERENCES public.ww_vkpfartikelpreisliste(i_id, mandant_c_nr);
ALTER TABLE ONLY public.part_kundesachbearbeiter
    ADD CONSTRAINT "fk_paku$sachbearbeiter_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_kundesachbearbeiter
    ADD CONSTRAINT "fk_paku$sachbearbeiter_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT "fk_palf$_partner_i_id_rechnungsadresse_papa$_i_id" FOREIGN KEY (partner_i_id_rechnungsadresse) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_lfliefergruppespr
    ADD CONSTRAINT "fk_palg$spr_lfliefergruppe_i_id_palg$_i_id" FOREIGN KEY (lfliefergruppe_i_id) REFERENCES public.part_lfliefergruppe(i_id);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT "fk_papa$_newslettergrund_i_id_part_newslettergrund_i_id" FOREIGN KEY (newslettergrund_i_id) REFERENCES public.part_newslettergrund(i_id);
ALTER TABLE ONLY public.part_partnerklassespr
    ADD CONSTRAINT "fk_papa$klassespr_partnerklasse_i_id_papa$klasse_i_id" FOREIGN KEY (partnerklasse_i_id) REFERENCES public.part_partnerklasse(i_id);
ALTER TABLE ONLY public.part_partnerkommentar
    ADD CONSTRAINT "fk_papk$_partnerkommentarart_i_id_papk$art_i_id" FOREIGN KEY (partnerkommentarart_i_id) REFERENCES public.part_partnerkommentarart(i_id);
ALTER TABLE ONLY public.part_partnerkommentardruck
    ADD CONSTRAINT "fk_papk$partnerkommentar_i_id_papk$_i_id" FOREIGN KEY (partnerkommentar_i_id) REFERENCES public.part_partnerkommentar(i_id);
ALTER TABLE ONLY public.part_anredespr
    ADD CONSTRAINT fk_part_anredespr_anrede_c_nr_part_anrede_c_nr FOREIGN KEY (anrede_c_nr) REFERENCES public.part_anrede(c_nr);
ALTER TABLE ONLY public.part_anredespr
    ADD CONSTRAINT fk_part_anredespr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.part_ansprechpartner
    ADD CONSTRAINT fk_part_ansprechpartner_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_ansprechpartneradressbuch
    ADD CONSTRAINT fk_part_ansprechpartneradressbuch_ansprechpartner_i_id_part_par FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.part_ansprechpartnerfunktionspr
    ADD CONSTRAINT fk_part_ansprechpartnerfunktionspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.part_bank
    ADD CONSTRAINT fk_part_bank_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_beauskunftung
    ADD CONSTRAINT fk_part_beauskunftung_identifikation_i_id_part_identifikation_i FOREIGN KEY (identifikation_i_id) REFERENCES public.part_identifikation(i_id);
ALTER TABLE ONLY public.part_beauskunftung
    ADD CONSTRAINT fk_part_beauskunftung_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_branchespr
    ADD CONSTRAINT fk_part_branchespr_branche_i_id_part_branche_i_id FOREIGN KEY (branche_i_id) REFERENCES public.part_branche(i_id);
ALTER TABLE ONLY public.part_branchespr
    ADD CONSTRAINT fk_part_branchespr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.part_dsgvokategoriespr
    ADD CONSTRAINT fk_part_dsgvokategoriespr_dsgvokategorie_i_id_part_dsgvokategor FOREIGN KEY (dsgvokategorie_i_id) REFERENCES public.part_dsgvokategorie(i_id);
ALTER TABLE ONLY public.part_dsgvotext
    ADD CONSTRAINT fk_part_dsgvotext_dsgvokategorie_i_id_part_dsgvokategorie_i_id FOREIGN KEY (dsgvokategorie_i_id) REFERENCES public.part_dsgvokategorie(i_id);
ALTER TABLE ONLY public.part_dsgvotext
    ADD CONSTRAINT fk_part_dsgvotext_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.part_geodaten
    ADD CONSTRAINT fk_part_geodaten_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_identifikationspr
    ADD CONSTRAINT fk_part_identifikationspr_identifikation_i_id_part_identifikati FOREIGN KEY (identifikation_i_id) REFERENCES public.part_identifikation(i_id);
ALTER TABLE ONLY public.part_kommunikationsartspr
    ADD CONSTRAINT fk_part_kommunikationsartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.part_kontakt
    ADD CONSTRAINT fk_part_kontakt_ansprechpartner_i_id_part_ansprechpartner_i_id FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.part_kontakt
    ADD CONSTRAINT fk_part_kontakt_kontaktart_i_id_part_kontaktart_i_id FOREIGN KEY (kontaktart_i_id) REFERENCES public.part_kontaktart(i_id);
ALTER TABLE ONLY public.kue_kassaimport
    ADD CONSTRAINT fk_part_kontakt_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.part_kontakt
    ADD CONSTRAINT fk_part_kontakt_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_kontakt
    ADD CONSTRAINT fk_part_kontakt_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_kontakt
    ADD CONSTRAINT fk_part_kontakt_personal_i_id_zugewiesener_pers_personal_i_id FOREIGN KEY (personal_i_id_zugewiesener) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_i_id__fb_konto_konto_i_id_erloesekonto FOREIGN KEY (konto_i_id_erloesekonto) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_konto_i_id_debitorenkonto_fb_konto_i_id FOREIGN KEY (konto_i_id_debitorenkonto) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_konto_i_id_erloesekonto_fb_konto_i_id FOREIGN KEY (konto_i_id_erloesekonto) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_laenderart_c_nr_fb_laenderart_c_nr FOREIGN KEY (laenderart_c_nr) REFERENCES public.fb_laenderart(c_nr);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_lager_i_id_abbuchungslager_ww_lager_i_id FOREIGN KEY (lager_i_id_abbuchungslager) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_lieferart_i_id_lp_lieferart_i_id FOREIGN KEY (lieferart_i_id, mandant_c_nr) REFERENCES public.lp_lieferart(i_id, mandant_c_nr);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_partner_i_id_rechnungsadresse_part_partner_i_id FOREIGN KEY (partner_i_id_rechnungsadresse) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_partnerbank_i_id_part_partnerbank_i_id FOREIGN KEY (partnerbank_i_id) REFERENCES public.part_partnerbank(i_id);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_personal_i_id_bekommeprovision_pers_personal_i_id FOREIGN KEY (personal_i_id_bekommeprovision) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_reversechargeart_i_id_fb_reversechargeart_i_id FOREIGN KEY (reversechargeart_i_id) REFERENCES public.fb_reversechargeart(i_id);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_spediteur_i_id_lp_spediteur_i_id FOREIGN KEY (spediteur_i_id) REFERENCES public.lp_spediteur(i_id);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_verrechnungsmodell_i_id_rech_verrechnungsmodell_i FOREIGN KEY (verrechnungsmodell_i_id) REFERENCES public.rech_verrechnungsmodell(i_id);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_waehrung_c_nr_lp_waehrung_c_nr FOREIGN KEY (waehrung_c_nr) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT fk_part_kunde_zahlungsziel_i_id_lp_zahlungsziel_i_id FOREIGN KEY (zahlungsziel_i_id) REFERENCES public.lp_zahlungsziel(i_id);
ALTER TABLE ONLY public.part_kundekennung
    ADD CONSTRAINT fk_part_kundekennung_kennung_i_id_lp_kennung_i_id FOREIGN KEY (kennung_i_id) REFERENCES public.lp_kennung(i_id);
ALTER TABLE ONLY public.part_kundekennung
    ADD CONSTRAINT fk_part_kundekennung_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.part_kundematerial
    ADD CONSTRAINT fk_part_kundematerial_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.part_kundematerial
    ADD CONSTRAINT fk_part_kundematerial_material_i_id_notierung_ww_material_i_id FOREIGN KEY (material_i_id_notierung) REFERENCES public.ww_material(i_id);
ALTER TABLE ONLY public.part_kundematerial
    ADD CONSTRAINT fk_part_kundematerial_material_i_id_ww_material_i_id FOREIGN KEY (material_i_id) REFERENCES public.ww_material(i_id);
ALTER TABLE ONLY public.part_kundesachbearbeiter
    ADD CONSTRAINT fk_part_kundesachbearbeiter_funktion_i_id_lp_funktion_i_id FOREIGN KEY (funktion_i_id) REFERENCES public.lp_funktion(i_id);
ALTER TABLE ONLY public.part_kundesachbearbeiter
    ADD CONSTRAINT fk_part_kundesachbearbeiter_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.part_kundesachbearbeiter
    ADD CONSTRAINT fk_part_kundesachbearbeiter_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_kundesoko
    ADD CONSTRAINT fk_part_kundesoko_artgru_i_id_ww_artgru_i_id FOREIGN KEY (artgru_i_id) REFERENCES public.ww_artgru(i_id);
ALTER TABLE ONLY public.part_kundesoko
    ADD CONSTRAINT fk_part_kundesoko_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.part_kundesoko
    ADD CONSTRAINT fk_part_kundesoko_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.part_kundesoko
    ADD CONSTRAINT fk_part_kundesoko_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_kundespediteur
    ADD CONSTRAINT fk_part_kundespediteur_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.part_kundespediteur
    ADD CONSTRAINT fk_part_kundespediteur_spediteur_i_id_lp_spediteur_i_id FOREIGN KEY (spediteur_i_id) REFERENCES public.lp_spediteur(i_id);
ALTER TABLE ONLY public.part_kurzbrief
    ADD CONSTRAINT "fk_part_kurzbrief_ansprechpartner_i_id_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.part_kurzbrief
    ADD CONSTRAINT fk_part_kurzbrief_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.part_kurzbrief
    ADD CONSTRAINT fk_part_kurzbrief_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.part_kurzbrief
    ADD CONSTRAINT fk_part_kurzbrief_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_kurzbrief
    ADD CONSTRAINT fk_part_kurzbrief_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_kurzbrief
    ADD CONSTRAINT fk_part_kurzbrief_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_lflfliefergruppe
    ADD CONSTRAINT "fk_part_lflfliefergruppe_lfliefergruppe_i_id_palg$_i_id" FOREIGN KEY (lfliefergruppe_i_id) REFERENCES public.part_lfliefergruppe(i_id);
ALTER TABLE ONLY public.part_lflfliefergruppe
    ADD CONSTRAINT fk_part_lflfliefergruppe_lieferant_i_id_part_lieferant_i_id FOREIGN KEY (lieferant_i_id) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.part_lfliefergruppe
    ADD CONSTRAINT fk_part_lfliefergruppe_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.part_lfliefergruppespr
    ADD CONSTRAINT fk_part_lfliefergruppespr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT fk_part_lieferant_konto_i_id_kreditorenkonto_fb_konto_i_id FOREIGN KEY (konto_i_id_kreditorenkonto) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT fk_part_lieferant_konto_i_id_warenkonto_fb_konto_i_id FOREIGN KEY (konto_i_id_warenkonto) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT fk_part_lieferant_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT fk_part_lieferant_lager_i_id_zubuchungslager_ww_lager_i_id FOREIGN KEY (lager_i_id_zubuchungslager) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT fk_part_lieferant_lieferart_i_id_lp_lieferart_i_id FOREIGN KEY (lieferart_i_id) REFERENCES public.lp_lieferart(i_id);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT fk_part_lieferant_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT fk_part_lieferant_mwstsatz_i_id_lp_mwstsatzbez_i_id FOREIGN KEY (mwstsatz_i_id) REFERENCES public.lp_mwstsatzbez(i_id);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT fk_part_lieferant_partner_i_id_lieferadresse_part_partner_i_id FOREIGN KEY (partner_i_id_lieferadresse) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT fk_part_lieferant_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT "fk_part_lieferant_partner_i_id_rechnungsadresse_papa$_i_id" FOREIGN KEY (partner_i_id_rechnungsadresse) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT fk_part_lieferant_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT fk_part_lieferant_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT fk_part_lieferant_personal_i_id_freigabe_pers_personal_i_id FOREIGN KEY (personal_i_id_freigabe) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT fk_part_lieferant_reversechargeart_i_id_fb_reversechargeart_i_i FOREIGN KEY (reversechargeart_i_id) REFERENCES public.fb_reversechargeart(i_id);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT fk_part_lieferant_spediteur_i_id_lp_spediteur_i_id FOREIGN KEY (spediteur_i_id) REFERENCES public.lp_spediteur(i_id);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT fk_part_lieferant_waehrung_c_nr_lp_waehrung_c_nr FOREIGN KEY (waehrung_c_nr) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT fk_part_lieferant_zahlungsziel_i_id_lp_zahlungsziel_i_id FOREIGN KEY (zahlungsziel_i_id) REFERENCES public.lp_zahlungsziel(i_id);
ALTER TABLE ONLY public.part_lieferantbeurteilung
    ADD CONSTRAINT fk_part_lieferantbeurteilung_lieferant_i_id_part_lieferant_i_id FOREIGN KEY (lieferant_i_id) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.part_lieferantbeurteilung
    ADD CONSTRAINT "fk_part_lieferantbeurteilung_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_liefermengen
    ADD CONSTRAINT fk_part_liefermengen_artikel_i_id_ww_artikel_i_id FOREIGN KEY (kunde_i_id_lieferadresse) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.part_liefermengen
    ADD CONSTRAINT fk_part_liefermengen_kunde_i_id_lieferadresse_part_kunde_i_id FOREIGN KEY (kunde_i_id_lieferadresse) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT fk_part_partner_anrede_c_nr_part_anrede_c_nr FOREIGN KEY (anrede_c_nr) REFERENCES public.part_anrede(c_nr);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT fk_part_partner_branche_i_id_part_branche_i_id FOREIGN KEY (branche_i_id) REFERENCES public.part_branche(i_id);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT fk_part_partner_lager_i_id_ziellager_ww_lager_i_id FOREIGN KEY (lager_i_id_ziellager) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT fk_part_partner_land_i_id_abweichendesustland_lp_land_i_id FOREIGN KEY (land_i_id_abweichendesustland) REFERENCES public.lp_land(i_id);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT fk_part_partner_landplzort_i_id_lp_landplzort_i_id FOREIGN KEY (landplzort_i_id) REFERENCES public.lp_landplzort(i_id);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT fk_part_partner_landplzort_i_id_postfach_lp_landplzort_i_id FOREIGN KEY (landplzort_i_id_postfach) REFERENCES public.lp_landplzort(i_id);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT fk_part_partner_locale_c_nr_kommunikation_lp_locale_c_nr FOREIGN KEY (locale_c_nr_kommunikation) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT fk_part_partner_partner_i_id_eigentuemer_part_partner_i_id FOREIGN KEY (partner_i_id_eigentuemer) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT fk_part_partner_partner_i_id_vater_part_partner_i_id FOREIGN KEY (partner_i_id_vater) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT fk_part_partner_partnerart_c_nr_part_partnerart_c_nr FOREIGN KEY (partnerart_c_nr) REFERENCES public.part_partnerart(c_nr);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT fk_part_partner_partnerklasse_i_id_part_partnerklasse_i_id FOREIGN KEY (partnerklasse_i_id) REFERENCES public.part_partnerklasse(i_id);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT fk_part_partner_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT fk_part_partner_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT fk_part_partner_rechtsform_i_id_lp_rechtsform_i_id FOREIGN KEY (rechtsform_i_id) REFERENCES public.lp_rechtsform(i_id);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT fk_part_partner_versandweg_i_id_versandweg_i_id FOREIGN KEY (versandweg_i_id) REFERENCES public.lp_versandweg(i_id);
ALTER TABLE ONLY public.part_partneradressbuch
    ADD CONSTRAINT fk_part_partneradressbuch_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_partnerartspr
    ADD CONSTRAINT fk_part_partnerartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.part_partnerartspr
    ADD CONSTRAINT fk_part_partnerartspr_partnerart_c_nr_part_partnerart_c_nr FOREIGN KEY (partnerart_c_nr) REFERENCES public.part_partnerart(c_nr);
ALTER TABLE ONLY public.part_partnerbank
    ADD CONSTRAINT fk_part_partnerbank_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_partnerbank
    ADD CONSTRAINT fk_part_partnerbank_partnerbank_i_id_part_partner_i_id FOREIGN KEY (partnerbank_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_partnerbank
    ADD CONSTRAINT fk_part_partnerbank_partnerbank_i_id_part_partnerbank_i_id FOREIGN KEY (partnerbank_i_id) REFERENCES public.part_bank(partner_i_id);
ALTER TABLE ONLY public.part_partnerbank
    ADD CONSTRAINT fk_part_partnerbank_waehrung_c_nr_lp_waehrung_c_nr FOREIGN KEY (waehrung_c_nr) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.part_partnerbild
    ADD CONSTRAINT fk_part_partnerbild_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_partnerklassespr
    ADD CONSTRAINT fk_part_partnerklassespr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.part_partnerkommentar
    ADD CONSTRAINT fk_part_partnerkommentar_datenformat_c_nr_lp_datenformat_c_nr FOREIGN KEY (datenformat_c_nr) REFERENCES public.lp_datenformat(c_nr);
ALTER TABLE ONLY public.part_partnerkommentar
    ADD CONSTRAINT fk_part_partnerkommentar_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_partnerkommentar
    ADD CONSTRAINT "fk_part_partnerkommentar_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_partnerkommentardruck
    ADD CONSTRAINT fk_part_partnerkommentardruck_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.part_partnerkommunikation
    ADD CONSTRAINT fk_part_partnerkommunikation_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.part_partnerkommunikation
    ADD CONSTRAINT fk_part_partnerkommunikation_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_paselektion
    ADD CONSTRAINT fk_part_paselektion_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_paselektion
    ADD CONSTRAINT fk_part_paselektion_selektion_i_id_part_selektion_i_id FOREIGN KEY (selektion_i_id) REFERENCES public.part_selektion(i_id);
ALTER TABLE ONLY public.part_selektion
    ADD CONSTRAINT fk_part_selektion_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.part_selektionspr
    ADD CONSTRAINT fk_part_selektionspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.part_selektionspr
    ADD CONSTRAINT fk_part_selektionspr_selektion_i_id_part_selektion_i_id FOREIGN KEY (selektion_i_id) REFERENCES public.part_selektion(i_id);
ALTER TABLE ONLY public.part_serienbrief
    ADD CONSTRAINT "fk_part_serienbrief_ansprechpartnerfunktion_i_id_paaf$_i_id" FOREIGN KEY (ansprechpartnerfunktion_i_id) REFERENCES public.part_ansprechpartnerfunktion(i_id);
ALTER TABLE ONLY public.part_serienbrief
    ADD CONSTRAINT fk_part_serienbrief_branche_i_id_part_branche_i_id FOREIGN KEY (branche_i_id) REFERENCES public.part_branche(i_id);
ALTER TABLE ONLY public.part_serienbrief
    ADD CONSTRAINT fk_part_serienbrief_land_i_id_lp_land_i_id FOREIGN KEY (land_i_id) REFERENCES public.lp_land(i_id);
ALTER TABLE ONLY public.part_serienbrief
    ADD CONSTRAINT fk_part_serienbrief_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.part_serienbrief
    ADD CONSTRAINT fk_part_serienbrief_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.part_serienbrief
    ADD CONSTRAINT fk_part_serienbrief_partnerklasse_i_id_part_partnerklasse_i_id FOREIGN KEY (partnerklasse_i_id) REFERENCES public.part_partnerklasse(i_id);
ALTER TABLE ONLY public.part_serienbrief
    ADD CONSTRAINT fk_part_serienbrief_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_serienbrief
    ADD CONSTRAINT fk_part_serienbrief_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.part_serienbriefselektion
    ADD CONSTRAINT fk_part_serienbriefselektion_part_serienbrief FOREIGN KEY (serienbrief_i_id) REFERENCES public.part_serienbrief(i_id);
ALTER TABLE ONLY public.part_serienbriefselektion
    ADD CONSTRAINT fk_part_serienbriefselektion_selektion_i_id_part_selektion_i_id FOREIGN KEY (selektion_i_id) REFERENCES public.part_selektion(i_id);
ALTER TABLE ONLY public.part_telefonnummer
    ADD CONSTRAINT "fk_part_telefonnummer_ansprechpartner_i_id_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.part_telefonnummer
    ADD CONSTRAINT fk_part_telefonnummer_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.part_weblieferantfarnell
    ADD CONSTRAINT fk_part_weblieferantfarnell_webpartner_i_id_as_webpartner_i_id FOREIGN KEY (webpartner_i_id) REFERENCES public.as_webpartner(i_id);
ALTER TABLE ONLY public.part_serienbriefselektion
    ADD CONSTRAINT "fk_pass$_serienbrief_i_id_part_serienbrief_i_id" FOREIGN KEY (serienbrief_i_id) REFERENCES public.part_serienbrief(i_id);
ALTER TABLE ONLY public.part_serienbriefselektionnegativ
    ADD CONSTRAINT "fk_pasv$_selektion_i_id_part_selektion_i_id" FOREIGN KEY (selektion_i_id) REFERENCES public.part_selektion(i_id);
ALTER TABLE ONLY public.part_serienbriefselektionnegativ
    ADD CONSTRAINT "fk_pasv$_serienbrief_i_id_part_serienbrief_i_id" FOREIGN KEY (serienbrief_i_id) REFERENCES public.part_serienbrief(i_id);
ALTER TABLE ONLY public.ww_paternoster
    ADD CONSTRAINT fk_paternoster_lager_i_id_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.pers_artgrurolle
    ADD CONSTRAINT "fk_pear$_systemrolle_i_id_pers_systemrolle_i_id" FOREIGN KEY (systemrolle_i_id) REFERENCES public.pers_systemrolle(i_id);
ALTER TABLE ONLY public.pers_angehoerigenartspr
    ADD CONSTRAINT "fk_peas$_angehoerigenart_c_nr_pers_angehoerigenart_c_nr" FOREIGN KEY (angehoerigenart_c_nr) REFERENCES public.pers_angehoerigenart(c_nr);
ALTER TABLE ONLY public.pers_bereitschaft
    ADD CONSTRAINT "fk_peba$_bereitschaftart_i_id_pers_bereitschaftart_i_id" FOREIGN KEY (bereitschaftart_i_id) REFERENCES public.pers_bereitschaftart(i_id);
ALTER TABLE ONLY public.pers_benutzermandantsystemrolle
    ADD CONSTRAINT "fk_pebs$_alternativesystemrolle_i_id_pers_systemrolle_i_id" FOREIGN KEY (systemrolle_i_id_restapi) REFERENCES public.pers_systemrolle(i_id);
ALTER TABLE ONLY public.pers_benutzermandantsystemrolle
    ADD CONSTRAINT "fk_pebs$_benutzer_i_id_pers_benutzer_i_id" FOREIGN KEY (benutzer_i_id) REFERENCES public.pers_benutzer(i_id);
ALTER TABLE ONLY public.pers_benutzermandantsystemrolle
    ADD CONSTRAINT "fk_pebs$_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_benutzermandantsystemrolle
    ADD CONSTRAINT "fk_pebs$_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_benutzermandantsystemrolle
    ADD CONSTRAINT "fk_pebs$_personal_i_id_zugeordnet_pepe$_i_id" FOREIGN KEY (personal_i_id_zugeordnet) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_benutzermandantsystemrolle
    ADD CONSTRAINT "fk_pebs$_systemrolle_i_id_hvma_pers_systemrolle_i_id" FOREIGN KEY (systemrolle_i_id_hvma) REFERENCES public.pers_systemrolle(i_id);
ALTER TABLE ONLY public.pers_benutzermandantsystemrolle
    ADD CONSTRAINT "fk_pebs$_systemrolle_i_id_pers_systemrolle_i_id" FOREIGN KEY (systemrolle_i_id) REFERENCES public.pers_systemrolle(i_id);
ALTER TABLE ONLY public.pers_bereitschafttag
    ADD CONSTRAINT "fk_pebt$_bereitschaftart_i_id_pers_bereitschaftart_i_id" FOREIGN KEY (bereitschaftart_i_id) REFERENCES public.pers_bereitschaftart(i_id);
ALTER TABLE ONLY public.pers_fertigungsgrupperolle
    ADD CONSTRAINT "fk_pefr$_systemrolle_i_id_pers_systemrolle_i_id" FOREIGN KEY (systemrolle_i_id) REFERENCES public.pers_systemrolle(i_id);
ALTER TABLE ONLY public.pers_familienstandspr
    ADD CONSTRAINT "fk_pefs$_familienstand_c_nr_pers_familienstand_c_nr" FOREIGN KEY (familienstand_c_nr) REFERENCES public.pers_familienstand(c_nr);
ALTER TABLE ONLY public.pers_lohnartstundenfaktor
    ADD CONSTRAINT "fk_pell$_lohnstundenart_c_nr_pers_lohnstundenart_c_nr" FOREIGN KEY (lohnstundenart_c_nr) REFERENCES public.pers_lohnstundenart(c_nr);
ALTER TABLE ONLY public.pers_nachrichtenempfaenger
    ADD CONSTRAINT "fk_pene$_nachrichten_i_id_pers_nachrichten_i_id" FOREIGN KEY (nachrichten_i_id) REFERENCES public.pers_nachrichten(i_id);
ALTER TABLE ONLY public.pers_personalangehoerige
    ADD CONSTRAINT "fk_pepa$_angehoerigenart_c_nr_pers_angehoerigenart_c_nr" FOREIGN KEY (angehoerigenart_c_nr) REFERENCES public.pers_angehoerigenart(c_nr);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT "fk_pepe$_kostenstelle_i_id_abteilung_lp_kostenstelle_i_id" FOREIGN KEY (kostenstelle_i_id_abteilung) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT "fk_pepe$_partner_i_id_sozialversicherer_papa$_i_id" FOREIGN KEY (partner_i_id_sozialversicherer) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT "fk_pepe$_pendlerpauschale_i_id_pers_pendlerpauschale_i_id" FOREIGN KEY (pendlerpauschale_i_id) REFERENCES public.pers_pendlerpauschale(i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT "fk_pepe$_personalfunktion_c_nr_pepe$funktion_c_nr" FOREIGN KEY (personalfunktion_c_nr) REFERENCES public.pers_personalfunktion(c_nr);
ALTER TABLE ONLY public.pers_personalfunktionspr
    ADD CONSTRAINT "fk_pepe$funktionspr_personalfunktion_c_nr_pepe$funktion_c_nr" FOREIGN KEY (personalfunktion_c_nr) REFERENCES public.pers_personalfunktion(c_nr);
ALTER TABLE ONLY public.pers_personalzutrittsklasse
    ADD CONSTRAINT "fk_pepz$_zutrittsklasse_i_id_pezl$_i_id" FOREIGN KEY (zutrittsklasse_i_id) REFERENCES public.pers_zutrittsklasse(i_id);
ALTER TABLE ONLY public.pers_abwesenheitsartspr
    ADD CONSTRAINT fk_pers_abwesenheitsartspr_abwesenheitsart_i_id_pers_abwesenhei FOREIGN KEY (abwesenheitsart_i_id) REFERENCES public.pers_abwesenheitsart(i_id);
ALTER TABLE ONLY public.pers_artgrurolle
    ADD CONSTRAINT "fk_pers_agro$_artgru_i_id_ww_artgru_i_id" FOREIGN KEY (artgru_i_id) REFERENCES public.ww_artgru(i_id);
ALTER TABLE ONLY public.pers_angehoerigenartspr
    ADD CONSTRAINT fk_pers_angehoerigenartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.pers_anwesenheitsbestaetigung
    ADD CONSTRAINT fk_pers_anwesenheitsbestaetigung_auftrag_i_id_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.pers_anwesenheitsbestaetigung
    ADD CONSTRAINT fk_pers_anwesenheitsbestaetigung_datenformat_c_nr_lp_datenforma FOREIGN KEY (datenformat_c_nr) REFERENCES public.lp_datenformat(c_nr);
ALTER TABLE ONLY public.pers_anwesenheitsbestaetigung
    ADD CONSTRAINT fk_pers_anwesenheitsbestaetigung_personal_i_id_pers_personal_i_ FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_anwesenheitsbestaetigung
    ADD CONSTRAINT fk_pers_anwesenheitsbestaetigung_projekt_i_id_proj_projekt_i_id FOREIGN KEY (projekt_i_id) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.pers_artikelzulage
    ADD CONSTRAINT fk_pers_artikelzulage_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.pers_artikelzulage
    ADD CONSTRAINT fk_pers_artikelzulage_zulage_i_id_pers_zulage_i_id FOREIGN KEY (zulage_i_id) REFERENCES public.pers_zulage(i_id);
ALTER TABLE ONLY public.pers_artikelzuschlag
    ADD CONSTRAINT fk_pers_artikelzuschlag_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.pers_auszahlung_bva
    ADD CONSTRAINT fk_pers_auszahlung_bva_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_auszahlung_bva
    ADD CONSTRAINT fk_pers_auszahlung_bva_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_benutzer
    ADD CONSTRAINT fk_pers_benutzer_mandant_c_nr_default_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr_default) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_benutzer
    ADD CONSTRAINT fk_pers_benutzer_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_benutzer
    ADD CONSTRAINT fk_pers_benutzer_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_benutzermandantsystemrolle
    ADD CONSTRAINT fk_pers_benutzermandantsystemrolle_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_bereitschaft
    ADD CONSTRAINT fk_pers_bereitschaft_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_bereitschaftart
    ADD CONSTRAINT fk_pers_bereitschaftart_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_bereitschafttag
    ADD CONSTRAINT fk_pers_bereitschafttag_tagesart_i_id_pers_tagesart_i_id FOREIGN KEY (tagesart_i_id) REFERENCES public.pers_tagesart(i_id);
ALTER TABLE ONLY public.pers_betriebskalender
    ADD CONSTRAINT fk_pers_betriebskalender_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_betriebskalender
    ADD CONSTRAINT "fk_pers_betriebskalender_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_betriebskalender
    ADD CONSTRAINT fk_pers_betriebskalender_religion_i_id_pers_religion_i_id FOREIGN KEY (religion_i_id) REFERENCES public.pers_religion(i_id);
ALTER TABLE ONLY public.pers_betriebskalender
    ADD CONSTRAINT fk_pers_betriebskalender_tagesart_i_id_pers_tagesart_i_id FOREIGN KEY (tagesart_i_id) REFERENCES public.pers_tagesart(i_id);
ALTER TABLE ONLY public.pers_diaeten
    ADD CONSTRAINT fk_pers_diaeten_land_i_id_lp_land_i_id FOREIGN KEY (land_i_id) REFERENCES public.lp_land(i_id);
ALTER TABLE ONLY public.pers_diaetentagessatz
    ADD CONSTRAINT fk_pers_diaetentagessatz_diaeten_i_id_pers_diaeten_i_id FOREIGN KEY (diaeten_i_id) REFERENCES public.pers_diaeten(i_id);
ALTER TABLE ONLY public.pers_eintrittaustritt
    ADD CONSTRAINT fk_pers_eintrittaustritt_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_fahrzeug
    ADD CONSTRAINT fk_pers_fahrzeug_fahrzeugverwendungsart_c_nr_pers_fahrzeugverwe FOREIGN KEY (fahrzeugverwendungsart_c_nr) REFERENCES public.pers_fahrzeugverwendungsart(c_nr);
ALTER TABLE ONLY public.pers_fahrzeug
    ADD CONSTRAINT fk_pers_fahrzeug_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_familienstandspr
    ADD CONSTRAINT fk_pers_familienstandspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.pers_feiertag
    ADD CONSTRAINT fk_pers_feiertag_lp_tagesart FOREIGN KEY (tagesart_i_id) REFERENCES public.pers_tagesart(i_id);
ALTER TABLE ONLY public.pers_feiertag
    ADD CONSTRAINT fk_pers_feiertag_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_feiertag
    ADD CONSTRAINT fk_pers_feiertag_pers_religion FOREIGN KEY (religion_i_id) REFERENCES public.pers_religion(i_id);
ALTER TABLE ONLY public.pers_fertigungsgrupperolle
    ADD CONSTRAINT "fk_pers_fgro$_fertigungsgruppe_i_id_stk_fertigungsgruppe_i_id" FOREIGN KEY (fertigungsgruppe_i_id) REFERENCES public.stk_fertigungsgruppe(i_id);
ALTER TABLE ONLY public.pers_gleitzeitsaldo
    ADD CONSTRAINT fk_pers_gleitzeitsaldo_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_gleitzeitsaldo
    ADD CONSTRAINT fk_pers_gleitzeitsaldo_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_hvmabenutzer
    ADD CONSTRAINT fk_pers_hvmabenutzer_benutzer_i_id_hvma_pers_benutzer_i_id FOREIGN KEY (benutzer_i_id) REFERENCES public.pers_benutzer(i_id);
ALTER TABLE ONLY public.pers_hvmabenutzer
    ADD CONSTRAINT fk_pers_hvmabenutzer_hvmalizenz_i_id_hvmalizenz_i_id FOREIGN KEY (hvmalizenz_i_id) REFERENCES public.pers_hvmalizenz(i_id);
ALTER TABLE ONLY public.pers_hvmabenutzerparameter
    ADD CONSTRAINT fk_pers_hvmabenutzerparameter_c_nr_pers_hvmaparameter_c_nr FOREIGN KEY (c_nr, c_kategorie) REFERENCES public.pers_hvmaparameter(c_nr, c_kategorie);
ALTER TABLE ONLY public.pers_hvmabenutzerparameter
    ADD CONSTRAINT fk_pers_hvmabenutzerparameter_hvmabenutzer_i_id_pers_hvmabenutz FOREIGN KEY (hvmabenutzer_i_id) REFERENCES public.pers_hvmabenutzer(i_id);
ALTER TABLE ONLY public.pers_hvmabenutzerparameter
    ADD CONSTRAINT fk_pers_hvmabenutzerparameter_personal_i_id_aendern_pers_person FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_hvmaparameter
    ADD CONSTRAINT fk_pers_hvmaparameter_hvmalizenz_i_id_pers_hvmalizenz_i_id FOREIGN KEY (hvmalizenz_i_id) REFERENCES public.pers_hvmalizenz(i_id);
ALTER TABLE ONLY public.pers_hvmarecht
    ADD CONSTRAINT fk_pers_hvmarecht_lizenz_i_id_pers_hvmalizenz_i_id FOREIGN KEY (hvmalizenz_i_id) REFERENCES public.pers_hvmalizenz(i_id);
ALTER TABLE ONLY public.pers_hvmarolle
    ADD CONSTRAINT fk_pers_hvmarolle_hmvarecht_i_id_hvmarecht_i_id FOREIGN KEY (hvmarecht_i_id) REFERENCES public.pers_hvmarecht(i_id);
ALTER TABLE ONLY public.pers_hvmarolle
    ADD CONSTRAINT fk_pers_hvmarolle_systemrolle_i_id_hvma_pers_systemrolle_i_id FOREIGN KEY (systemrolle_i_id) REFERENCES public.pers_systemrolle(i_id);
ALTER TABLE ONLY public.pers_hvmasync
    ADD CONSTRAINT fk_pers_hvmasync_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_hvmasync
    ADD CONSTRAINT fk_pers_hvmasync_systemrolle_i_id_synczeitpunkt_pers_systemroll FOREIGN KEY (systemrolle_i_id_synczeitpunkt) REFERENCES public.pers_systemrolle(i_id);
ALTER TABLE ONLY public.pers_kollektivuestd50
    ADD CONSTRAINT fk_pers_kollektivuestd50_kollektiv_i_id_pers_kollektiv_i_id FOREIGN KEY (kollektiv_i_id) REFERENCES public.pers_kollektiv(i_id);
ALTER TABLE ONLY public.pers_kollektivuestd50
    ADD CONSTRAINT fk_pers_kollektivuestd50_tagesart_i_id_pers_tagesart_i_id FOREIGN KEY (tagesart_i_id) REFERENCES public.pers_tagesart(i_id);
ALTER TABLE ONLY public.pers_kollektivuestd_bva
    ADD CONSTRAINT fk_pers_kollektivuestd_bva_kollektiv_i_id_pers_kollektiv_i_id FOREIGN KEY (kollektiv_i_id) REFERENCES public.pers_kollektiv(i_id);
ALTER TABLE ONLY public.pers_kollektivuestd_bva
    ADD CONSTRAINT fk_pers_kollektivuestd_bva_tagesart_i_id_pers_tagesart_i_id FOREIGN KEY (tagesart_i_id) REFERENCES public.pers_tagesart(i_id);
ALTER TABLE ONLY public.pers_kollektivuestd
    ADD CONSTRAINT fk_pers_kollektivuestd_kollektiv_i_id_pers_kollektiv_i_id FOREIGN KEY (kollektiv_i_id) REFERENCES public.pers_kollektiv(i_id);
ALTER TABLE ONLY public.pers_kollektivuestd
    ADD CONSTRAINT fk_pers_kollektivuestd_tagesart_i_id_pers_tagesart_i_id FOREIGN KEY (tagesart_i_id) REFERENCES public.pers_tagesart(i_id);
ALTER TABLE ONLY public.pers_lagerrolle
    ADD CONSTRAINT fk_pers_lagerrolle_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.pers_lagerrolle
    ADD CONSTRAINT fk_pers_lagerrolle_systemrolle_i_id_pers_systemrolle_i_id FOREIGN KEY (systemrolle_i_id) REFERENCES public.pers_systemrolle(i_id);
ALTER TABLE ONLY public.pers_lohnart
    ADD CONSTRAINT fk_pers_lohnart_personalart_c_nr_pers_personalart_c_nr FOREIGN KEY (personalart_c_nr) REFERENCES public.pers_personalart(c_nr);
ALTER TABLE ONLY public.pers_lohnart
    ADD CONSTRAINT fk_pers_lohnart_taetigkeit_i_id_nl_pers_taetigkeit_i_id FOREIGN KEY (taetigkeit_i_id_nl) REFERENCES public.pers_taetigkeit(i_id);
ALTER TABLE ONLY public.pers_lohnartstundenfaktor
    ADD CONSTRAINT fk_pers_lohnartstundenfaktor_lohnart_i_id_pers_lohnart_i_id FOREIGN KEY (lohnart_i_id) REFERENCES public.pers_lohnart(i_id);
ALTER TABLE ONLY public.pers_lohnartstundenfaktor
    ADD CONSTRAINT fk_pers_lohnartstundenfaktor_schichtzeit_i_id_pers_schichtzeit_ FOREIGN KEY (schichtzeit_i_id) REFERENCES public.pers_schichtzeit(i_id);
ALTER TABLE ONLY public.pers_lohnartstundenfaktor
    ADD CONSTRAINT fk_pers_lohnartstundenfaktor_taetigkeit_i_id_pers_taetigkeit_i_ FOREIGN KEY (taetigkeit_i_id) REFERENCES public.pers_taetigkeit(i_id);
ALTER TABLE ONLY public.pers_lohnartstundenfaktor
    ADD CONSTRAINT fk_pers_lohnartstundenfaktor_tagesart_i_id_pers_tagesart_i_id FOREIGN KEY (tagesart_i_id) REFERENCES public.pers_tagesart(i_id);
ALTER TABLE ONLY public.pers_lohnartstundenfaktor
    ADD CONSTRAINT fk_pers_lohnartstundenfaktor_zeitmodell_i_id_pers_zeitmodell_i_ FOREIGN KEY (zeitmodell_i_id) REFERENCES public.pers_zeitmodell(i_id);
ALTER TABLE ONLY public.pers_maschine
    ADD CONSTRAINT fk_pers_maschine_artikel_i_id_verrechnen_ww_artikel_i_id FOREIGN KEY (artikel_i_id_verrechnen) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.pers_maschine
    ADD CONSTRAINT fk_pers_maschine_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_maschine
    ADD CONSTRAINT fk_pers_maschine_maschinengruppe_i_id_pers_maschinengruppe_i_id FOREIGN KEY (maschinengruppe_i_id) REFERENCES public.pers_maschinengruppe(i_id);
ALTER TABLE ONLY public.pers_maschineleistungsfaktor
    ADD CONSTRAINT fk_pers_maschineleistungsfaktor_maschine_i_id_pers_maschine_i_i FOREIGN KEY (maschine_i_id) REFERENCES public.pers_maschine(i_id);
ALTER TABLE ONLY public.pers_maschineleistungsfaktor
    ADD CONSTRAINT fk_pers_maschineleistungsfaktor_material_i_id_ww_material_i_id FOREIGN KEY (material_i_id) REFERENCES public.ww_material(i_id);
ALTER TABLE ONLY public.pers_maschinemaschinenzm
    ADD CONSTRAINT fk_pers_maschinemaschinenzm_maschine_i_id_pers_personal_i_id FOREIGN KEY (maschine_i_id) REFERENCES public.pers_maschine(i_id);
ALTER TABLE ONLY public.pers_maschinengruppe
    ADD CONSTRAINT fk_pers_maschinengruppe_fertigungsgruppe_i_id_stk_fertigungsgru FOREIGN KEY (fertigungsgruppe_i_id) REFERENCES public.stk_fertigungsgruppe(i_id);
ALTER TABLE ONLY public.pers_maschinengruppe
    ADD CONSTRAINT fk_pers_maschinengruppe_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_maschinenkosten
    ADD CONSTRAINT fk_pers_maschinenkosten_maschine_i_id_pers_maschine_i_id FOREIGN KEY (maschine_i_id) REFERENCES public.pers_maschine(i_id);
ALTER TABLE ONLY public.pers_maschinenzeitdaten
    ADD CONSTRAINT "fk_pers_maschinenzeitdaten_lossollarbeitsplan_i_id_fela$_i_id" FOREIGN KEY (lossollarbeitsplan_i_id) REFERENCES public.fert_lossollarbeitsplan(i_id);
ALTER TABLE ONLY public.pers_maschinenzeitdaten
    ADD CONSTRAINT fk_pers_maschinenzeitdaten_maschine_i_id_pers_maschine_i_id FOREIGN KEY (maschine_i_id) REFERENCES public.pers_maschine(i_id);
ALTER TABLE ONLY public.pers_maschinenzeitdaten
    ADD CONSTRAINT "fk_pers_maschinenzeitdaten_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_maschinenzeitdaten
    ADD CONSTRAINT "fk_pers_maschinenzeitdaten_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_maschinenzeitdaten
    ADD CONSTRAINT fk_pers_maschinenzeitdaten_personal_i_id_erledigt_pers_personal FOREIGN KEY (personal_i_id_erledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_maschinenzeitdaten
    ADD CONSTRAINT "fk_pers_maschinenzeitdaten_personal_i_id_gestartet_pepe$_i_id" FOREIGN KEY (personal_i_id_gestartet) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_maschinenzeitdatenverrechnet
    ADD CONSTRAINT fk_pers_maschinenzeitdatenverrechnet_maschinenzeitdaten_i_id_pe FOREIGN KEY (maschinenzeitdaten_i_id) REFERENCES public.pers_maschinenzeitdaten(i_id);
ALTER TABLE ONLY public.pers_maschinenzeitdatenverrechnet
    ADD CONSTRAINT fk_pers_maschinenzeitdatenverrechnet_rechnungposition_i_id_rech FOREIGN KEY (rechnungposition_i_id) REFERENCES public.rech_rechnungposition(i_id);
ALTER TABLE ONLY public.pers_maschinenzm
    ADD CONSTRAINT fk_pers_maschinenzm_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_maschinenzmtagesart
    ADD CONSTRAINT fk_pers_maschinenzmtagesart_tagesart_i_id_pers_tagesart_i_id FOREIGN KEY (tagesart_i_id) REFERENCES public.pers_tagesart(i_id);
ALTER TABLE ONLY public.pers_nachrichtarchiv
    ADD CONSTRAINT fk_pers_nachrichtarchiv_nchrichtart_i_id_pers_nachrichtart_i_id FOREIGN KEY (nachrichtart_i_id) REFERENCES public.pers_nachrichtart(i_id);
ALTER TABLE ONLY public.pers_nachrichtarchiv
    ADD CONSTRAINT "fk_pers_nachrichtarchiv_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_nachrichtarchiv
    ADD CONSTRAINT "fk_pers_nachrichtarchiv_personal_i_id_bearbeiter_pepe$_i_id" FOREIGN KEY (personal_i_id_bearbeiter) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_nachrichtarchiv
    ADD CONSTRAINT "fk_pers_nachrichtarchiv_personal_i_id_erledigt_pepe$_i_id" FOREIGN KEY (personal_i_id_erledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_nachrichtart
    ADD CONSTRAINT fk_pers_nachrichtart_thema_c_nr_pers_thema_c_nr FOREIGN KEY (thema_c_nr) REFERENCES public.pers_thema(c_nr);
ALTER TABLE ONLY public.pers_nachrichten
    ADD CONSTRAINT fk_pers_nachrichten_nachrichtenart_i_id_pers_nachrichtenart_i_i FOREIGN KEY (nachrichtenart_i_id) REFERENCES public.pers_nachrichtenart(i_id);
ALTER TABLE ONLY public.pers_nachrichten
    ADD CONSTRAINT fk_pers_nachrichten_personal_i_id_absender_pers_personal_i_id FOREIGN KEY (personal_i_id_absender) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_nachrichten
    ADD CONSTRAINT fk_pers_nachrichten_personal_i_id_erledigt_pers_personal_i_id FOREIGN KEY (personal_i_id_erledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_nachrichtenabo
    ADD CONSTRAINT fk_pers_nachrichtenabo_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_nachrichtenart
    ADD CONSTRAINT fk_pers_nachrichtenart_personal_i_id_eskalation_pers_personal_i FOREIGN KEY (personal_i_id_eskalation) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_nachrichtenartspr
    ADD CONSTRAINT fk_pers_nachrichtenartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.pers_nachrichtenartspr
    ADD CONSTRAINT fk_pers_nachrichtenartspr_nachrichtenart_i_id_pers_nachrichtena FOREIGN KEY (nachrichtenart_i_id) REFERENCES public.pers_nachrichtenart(i_id);
ALTER TABLE ONLY public.pers_nachrichtenempfaenger
    ADD CONSTRAINT fk_pers_nachrichtenempfaenger_personal_i_id_empfaenger_pers_per FOREIGN KEY (personal_i_id_empfaenger) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_passivereise
    ADD CONSTRAINT fk_pers_passivereise_kollektiv_i_id_pers_kollektiv_i_id FOREIGN KEY (kollektiv_i_id) REFERENCES public.pers_kollektiv(i_id);
ALTER TABLE ONLY public.pers_passivereise
    ADD CONSTRAINT fk_pers_passivereise_tagesart_i_id_pers_tagesart_i_id FOREIGN KEY (tagesart_i_id) REFERENCES public.pers_tagesart(i_id);
ALTER TABLE ONLY public.pers_maschinenzeitdatenverrechnet
    ADD CONSTRAINT fk_pers_pers_maschinenzeitdatenverrechnet_artikel_i_id_ww_artik FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.pers_zeitdatenverrechnet
    ADD CONSTRAINT fk_pers_pers_zeitdatenverrechnet_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT fk_pers_personal_beruf_i_id_pers_beruf_i_id FOREIGN KEY (beruf_i_id) REFERENCES public.pers_beruf(i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT fk_pers_personal_familienstand_c_nr_pers_familienstand_c_nr FOREIGN KEY (familienstand_c_nr) REFERENCES public.pers_familienstand(c_nr);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT fk_pers_personal_kollektiv_i_id_pers_kollektiv_i_id FOREIGN KEY (kollektiv_i_id) REFERENCES public.pers_kollektiv(i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT fk_pers_personal_kostenstelle_i_id_stamm_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id_stamm) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT fk_pers_personal_land_i_id_staatsangehoerigkeit_lp_land_i_id FOREIGN KEY (land_i_id_staatsangehoerigkeit) REFERENCES public.lp_land(i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT fk_pers_personal_landplzort_i_id_geburt_lp_landplzort_i_id FOREIGN KEY (landplzort_i_id_geburt) REFERENCES public.lp_landplzort(i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT fk_pers_personal_lohngruppe_i_id_pers_lohngruppe_i_id FOREIGN KEY (lohngruppe_i_id) REFERENCES public.pers_lohngruppe(i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT fk_pers_personal_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT fk_pers_personal_maschinengruppe_i_id_pers_maschinengruppe_i_id FOREIGN KEY (maschinengruppe_i_id) REFERENCES public.pers_maschinengruppe(i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT fk_pers_personal_partner_i_id_firma_part_partner_i_id FOREIGN KEY (partner_i_id_firma) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT fk_pers_personal_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT fk_pers_personal_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT fk_pers_personal_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT fk_pers_personal_personalart_c_nr_pers_personalart_c_nr FOREIGN KEY (personalart_c_nr) REFERENCES public.pers_personalart(c_nr);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT fk_pers_personal_personalgruppe_i_id_pers_personalgruppe_i_id FOREIGN KEY (personalgruppe_i_id) REFERENCES public.pers_personalgruppe(i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT fk_pers_personal_religion_i_id_pers_religion_i_id FOREIGN KEY (religion_i_id) REFERENCES public.pers_religion(i_id);
ALTER TABLE ONLY public.pers_personalangehoerige
    ADD CONSTRAINT fk_pers_personalangehoerige_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_personalartspr
    ADD CONSTRAINT fk_pers_personalartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.pers_personalartspr
    ADD CONSTRAINT fk_pers_personalartspr_personalart_c_nr_pers_personalart_c_nr FOREIGN KEY (personalart_c_nr) REFERENCES public.pers_personalart(c_nr);
ALTER TABLE ONLY public.pers_personalfahrzeug
    ADD CONSTRAINT fk_pers_personalfahrzeug_fahrzeug_i_id_pers_fahrzeug_i_id FOREIGN KEY (fahrzeug_i_id) REFERENCES public.pers_fahrzeug(i_id);
ALTER TABLE ONLY public.pers_personalfahrzeug
    ADD CONSTRAINT fk_pers_personalfahrzeug_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_personalfinger
    ADD CONSTRAINT fk_pers_personalfinger_fingerart_i_id_pers_fingerart_i_id FOREIGN KEY (fingerart_i_id) REFERENCES public.pers_fingerart(i_id);
ALTER TABLE ONLY public.pers_personalfinger
    ADD CONSTRAINT fk_pers_personalfinger_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_personalfinger
    ADD CONSTRAINT fk_pers_personalfinger_personal_i_id_pers_personal_i_id FOREIGN KEY (fingerart_i_id) REFERENCES public.pers_fingerart(i_id);
ALTER TABLE ONLY public.pers_personalfunktionspr
    ADD CONSTRAINT fk_pers_personalfunktionspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.pers_personalgehalt
    ADD CONSTRAINT fk_pers_personalgehalt_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_personalgehalt
    ADD CONSTRAINT fk_pers_personalgehalt_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_personalterminal
    ADD CONSTRAINT fk_pers_personalterminal_arbeitsplatz_i_id_lp_arbeitsplatz_i_id FOREIGN KEY (arbeitsplatz_i_id) REFERENCES public.lp_arbeitsplatz(i_id);
ALTER TABLE ONLY public.pers_personalterminal
    ADD CONSTRAINT "fk_pers_personalterminal_personal_i_id_pepe$_i_id" FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_personalverfuegbarkeit
    ADD CONSTRAINT fk_pers_personalverfuegbarkeit_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.pers_personalverfuegbarkeit
    ADD CONSTRAINT fk_pers_personalverfuegbarkeit_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_personalzeiten
    ADD CONSTRAINT fk_pers_personalzeiten_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_personalzeitmodell
    ADD CONSTRAINT fk_pers_personalzeitmodell_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_personalzeitmodell
    ADD CONSTRAINT fk_pers_personalzeitmodell_zeitmodell_i_id_pers_zeitmodell_i_id FOREIGN KEY (zeitmodell_i_id) REFERENCES public.pers_zeitmodell(i_id);
ALTER TABLE ONLY public.pers_personalzutrittsklasse
    ADD CONSTRAINT fk_pers_personalzutrittsklasse_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_reise
    ADD CONSTRAINT fk_pers_reise_ansprechpartner_i_id_part_ansprechpartner_i_id FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.pers_reise
    ADD CONSTRAINT fk_pers_reise_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.pers_reise
    ADD CONSTRAINT fk_pers_reise_diaeten_i_id_pers_diaeten_i_id FOREIGN KEY (diaeten_i_id) REFERENCES public.pers_diaeten(i_id);
ALTER TABLE ONLY public.pers_reise
    ADD CONSTRAINT fk_pers_reise_fahrzeug_i_id_pers_fahrzeug_i_id FOREIGN KEY (fahrzeug_i_id) REFERENCES public.pers_fahrzeug(i_id);
ALTER TABLE ONLY public.pers_reise
    ADD CONSTRAINT fk_pers_reise_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.pers_reise
    ADD CONSTRAINT fk_pers_reise_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_reise
    ADD CONSTRAINT fk_pers_reise_personal_i_id_erledigt_pers_personal_i_id FOREIGN KEY (personal_i_id_erledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_reise
    ADD CONSTRAINT fk_pers_reise_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_reiselog
    ADD CONSTRAINT fk_pers_reiselog_ansprechpartner_i_id_part_ansprechpartner_i_id FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.pers_reiselog
    ADD CONSTRAINT fk_pers_reiselog_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.pers_reiselog
    ADD CONSTRAINT fk_pers_reiselog_diaeten_i_id_pers_diaeten_i_id FOREIGN KEY (diaeten_i_id) REFERENCES public.pers_diaeten(i_id);
ALTER TABLE ONLY public.pers_reiselog
    ADD CONSTRAINT fk_pers_reiselog_fahrzeug_i_id_pers_fahrzeug_i_id FOREIGN KEY (fahrzeug_i_id) REFERENCES public.pers_fahrzeug(i_id);
ALTER TABLE ONLY public.pers_reiselog
    ADD CONSTRAINT fk_pers_reiselog_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.pers_reiselog
    ADD CONSTRAINT fk_pers_reiselog_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_reiselog
    ADD CONSTRAINT fk_pers_reiselog_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_reisespesen
    ADD CONSTRAINT fk_pers_reisespesen_eingangsrechnung_i_id_er_eingangsrechnung_i FOREIGN KEY (eingangsrechnung_i_id) REFERENCES public.er_eingangsrechnung(i_id);
ALTER TABLE ONLY public.pers_reisespesen
    ADD CONSTRAINT fk_pers_reisespesen_reise_i_id_pers_reise_i_id FOREIGN KEY (reise_i_id) REFERENCES public.pers_reise(i_id);
ALTER TABLE ONLY public.pers_reiseverrechnet
    ADD CONSTRAINT fk_pers_reiseverrechnet_rechnungposition_i_id_rech_rechnungposi FOREIGN KEY (rechnungposition_i_id) REFERENCES public.rech_rechnungposition(i_id);
ALTER TABLE ONLY public.pers_reiseverrechnet
    ADD CONSTRAINT fk_pers_reiseverrechnet_reise_i_id_pers_reise_i_id FOREIGN KEY (reise_i_id) REFERENCES public.pers_reise(i_id);
ALTER TABLE ONLY public.pers_religionspr
    ADD CONSTRAINT fk_pers_religionspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.pers_religionspr
    ADD CONSTRAINT fk_pers_religionspr_religion_i_id_pers_religion_i_id FOREIGN KEY (religion_i_id) REFERENCES public.pers_religion(i_id);
ALTER TABLE ONLY public.pers_rollerecht
    ADD CONSTRAINT fk_pers_rollerecht_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_rollerecht
    ADD CONSTRAINT fk_pers_rollerecht_recht_c_nr_pers_recht_c_nr FOREIGN KEY (recht_c_nr) REFERENCES public.pers_recht(c_nr);
ALTER TABLE ONLY public.pers_rollerecht
    ADD CONSTRAINT fk_pers_rollerecht_systemrolle_i_id_pers_systemrolle_i_id FOREIGN KEY (systemrolle_i_id) REFERENCES public.pers_systemrolle(i_id);
ALTER TABLE ONLY public.pers_schicht
    ADD CONSTRAINT fk_pers_schicht_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_schichtzeitmodell
    ADD CONSTRAINT fk_pers_schichtzeitmodell_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_schichtzeitmodell
    ADD CONSTRAINT fk_pers_schichtzeitmodell_zeitmodell_i_id_pers_zeitmodell_i_id FOREIGN KEY (zeitmodell_i_id) REFERENCES public.pers_zeitmodell(i_id);
ALTER TABLE ONLY public.pers_schichtzuschlag
    ADD CONSTRAINT fk_pers_schichtzuschlag_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_signatur
    ADD CONSTRAINT fk_pers_signatur_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.pers_signatur
    ADD CONSTRAINT fk_pers_signatur_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_sonderzeiten
    ADD CONSTRAINT fk_pers_sonderzeiten_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_sonderzeiten
    ADD CONSTRAINT fk_pers_sonderzeiten_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_sonderzeiten
    ADD CONSTRAINT fk_pers_sonderzeiten_taetigkeit_i_id_pers_taetigkeit_i_id FOREIGN KEY (taetigkeit_i_id) REFERENCES public.pers_taetigkeit(i_id);
ALTER TABLE ONLY public.pers_stundenabrechnung
    ADD CONSTRAINT "fk_pers_stundenabrechnung_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_stundenabrechnung
    ADD CONSTRAINT fk_pers_stundenabrechnung_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_taetigkeit
    ADD CONSTRAINT fk_pers_taetigkeit_abwesenheitsart_i_id_pers_abwesenheitsart_i_ FOREIGN KEY (abwesenheitsart_i_id) REFERENCES public.pers_abwesenheitsart(i_id);
ALTER TABLE ONLY public.pers_taetigkeit
    ADD CONSTRAINT fk_pers_taetigkeit_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_taetigkeit
    ADD CONSTRAINT fk_pers_taetigkeit_taetigkeitart_c_nr_pers_taetigkeitart_c_nr FOREIGN KEY (taetigkeitart_c_nr) REFERENCES public.pers_taetigkeitart(c_nr);
ALTER TABLE ONLY public.pers_taetigkeitartspr
    ADD CONSTRAINT fk_pers_taetigkeitartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.pers_taetigkeitspr
    ADD CONSTRAINT fk_pers_taetigkeitspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.pers_taetigkeitspr
    ADD CONSTRAINT fk_pers_taetigkeitspr_taetigkeit_i_id_pers_taetigkeit_i_id FOREIGN KEY (taetigkeit_i_id) REFERENCES public.pers_taetigkeit(i_id);
ALTER TABLE ONLY public.pers_tagesartspr
    ADD CONSTRAINT fk_pers_tagesartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.pers_tagesartspr
    ADD CONSTRAINT fk_pers_tagesartspr_tagesart_i_id_pers_tagesart_i_id FOREIGN KEY (tagesart_i_id) REFERENCES public.pers_tagesart(i_id);
ALTER TABLE ONLY public.pers_telefonzeiten
    ADD CONSTRAINT fk_pers_telefonzeiten_angebot_i_id_angb_angebot_i_id FOREIGN KEY (angebot_i_id) REFERENCES public.angb_angebot(i_id);
ALTER TABLE ONLY public.pers_telefonzeiten
    ADD CONSTRAINT "fk_pers_telefonzeiten_ansprechpartner_i_id_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.pers_telefonzeiten
    ADD CONSTRAINT fk_pers_telefonzeiten_auftrag_i_id_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.pers_telefonzeiten
    ADD CONSTRAINT fk_pers_telefonzeiten_kontaktart_i_id_part_kontaktart_i_id FOREIGN KEY (kontaktart_i_id) REFERENCES public.part_kontaktart(i_id);
ALTER TABLE ONLY public.pers_telefonzeiten
    ADD CONSTRAINT fk_pers_telefonzeiten_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.pers_telefonzeiten
    ADD CONSTRAINT fk_pers_telefonzeiten_personal_i_id_erledigt_pers_personal_i_id FOREIGN KEY (personal_i_id_erledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_telefonzeiten
    ADD CONSTRAINT fk_pers_telefonzeiten_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_telefonzeiten
    ADD CONSTRAINT fk_pers_telefonzeiten_personal_i_id_zugewiesener_pers_personal_ FOREIGN KEY (personal_i_id_zugewiesener) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_telefonzeiten
    ADD CONSTRAINT fk_pers_telefonzeiten_projekt_i_id_proj_projekt_i_id FOREIGN KEY (projekt_i_id) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.pers_telefonzeitenverrechnet
    ADD CONSTRAINT fk_pers_telefonzeitenverrechnet_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.pers_telefonzeitenverrechnet
    ADD CONSTRAINT fk_pers_telefonzeitenverrechnet_rechnungposition_i_id_rech_rech FOREIGN KEY (rechnungposition_i_id) REFERENCES public.rech_rechnungposition(i_id);
ALTER TABLE ONLY public.pers_telefonzeitenverrechnet
    ADD CONSTRAINT fk_pers_telefonzeitenverrechnet_telefonzeiten_i_id_pers_telefon FOREIGN KEY (telefonzeiten_i_id) REFERENCES public.pers_telefonzeiten(i_id);
ALTER TABLE ONLY public.pers_themarolle
    ADD CONSTRAINT fk_pers_themarolle_systemrolle_i_id_pers_systemrolle_i_id FOREIGN KEY (systemrolle_i_id) REFERENCES public.pers_systemrolle(i_id);
ALTER TABLE ONLY public.pers_themarolle
    ADD CONSTRAINT fk_pers_themarolle_thema_c_nr_pers_thema_c_nr FOREIGN KEY (thema_c_nr) REFERENCES public.pers_thema(c_nr);
ALTER TABLE ONLY public.pers_uebertrag_bva
    ADD CONSTRAINT fk_pers_uebertrag_bva_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_urlaubsanspruch
    ADD CONSTRAINT "fk_pers_urlaubsanspruch_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_urlaubsanspruch
    ADD CONSTRAINT fk_pers_urlaubsanspruch_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_zahltag
    ADD CONSTRAINT fk_pers_zahltag_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_zeitabschluss
    ADD CONSTRAINT fk_pers_zeitabschluss_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_zeitdaten
    ADD CONSTRAINT fk_pers_zeitdaten_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.pers_zeitdaten
    ADD CONSTRAINT fk_pers_zeitdaten_maschine_i_id_pers_maschine_i_id FOREIGN KEY (maschine_i_id) REFERENCES public.pers_maschine(i_id);
ALTER TABLE ONLY public.pers_zeitdaten
    ADD CONSTRAINT fk_pers_zeitdaten_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_zeitdaten
    ADD CONSTRAINT fk_pers_zeitdaten_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_zeitdaten
    ADD CONSTRAINT fk_pers_zeitdaten_personal_i_id_erledigt_pers_personal_i_id FOREIGN KEY (personal_i_id_erledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_zeitdaten
    ADD CONSTRAINT fk_pers_zeitdaten_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_zeitdaten
    ADD CONSTRAINT fk_pers_zeitdaten_taetigkeit_i_id_pers_taetigkeit_i_id FOREIGN KEY (taetigkeit_i_id) REFERENCES public.pers_taetigkeit(i_id);
ALTER TABLE ONLY public.pers_zeitdatenpruefen
    ADD CONSTRAINT fk_pers_zeitdatenpruefen_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.pers_zeitdatenpruefen
    ADD CONSTRAINT fk_pers_zeitdatenpruefen_personal_i_id_aendern_pers_personal_i_ FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_zeitdatenpruefen
    ADD CONSTRAINT fk_pers_zeitdatenpruefen_personal_i_id_anlegen_pers_personal_i_ FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_zeitdatenpruefen
    ADD CONSTRAINT fk_pers_zeitdatenpruefen_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_zeitdatenpruefen
    ADD CONSTRAINT fk_pers_zeitdatenpruefen_taetigkeit_i_id_pers_taetigkeit_i_id FOREIGN KEY (taetigkeit_i_id) REFERENCES public.pers_taetigkeit(i_id);
ALTER TABLE ONLY public.pers_zeitdatenverrechnet
    ADD CONSTRAINT fk_pers_zeitdatenverrechnet_rechnungposition_i_id_rech_rechnung FOREIGN KEY (rechnungposition_i_id) REFERENCES public.rech_rechnungposition(i_id);
ALTER TABLE ONLY public.pers_zeitdatenverrechnet
    ADD CONSTRAINT fk_pers_zeitdatenverrechnet_zeitdaten_i_id_pers_zeitdaten_i_id FOREIGN KEY (zeitdaten_i_id) REFERENCES public.pers_zeitdaten(i_id);
ALTER TABLE ONLY public.pers_zeitgutschrift
    ADD CONSTRAINT fk_pers_zeitgutschrift_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_zeitmodell
    ADD CONSTRAINT fk_pers_zeitmodell_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_zeitmodell
    ADD CONSTRAINT fk_pers_zeitmodell_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_zeitmodell
    ADD CONSTRAINT fk_pers_zeitmodell_schicht_i_id_pers_schicht_i_id FOREIGN KEY (schicht_i_id) REFERENCES public.pers_schicht(i_id);
ALTER TABLE ONLY public.pers_zeitmodellspr
    ADD CONSTRAINT fk_pers_zeitmodellspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.pers_zeitmodellspr
    ADD CONSTRAINT fk_pers_zeitmodellspr_zeitmodell_i_id_pers_zeitmodell_i_id FOREIGN KEY (zeitmodell_i_id) REFERENCES public.pers_zeitmodell(i_id);
ALTER TABLE ONLY public.pers_zeitmodelltag
    ADD CONSTRAINT fk_pers_zeitmodelltag_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_zeitmodelltag
    ADD CONSTRAINT fk_pers_zeitmodelltag_tagesart_i_id_pers_tagesart_i_id FOREIGN KEY (tagesart_i_id) REFERENCES public.pers_tagesart(i_id);
ALTER TABLE ONLY public.pers_zeitmodelltag
    ADD CONSTRAINT fk_pers_zeitmodelltag_zeitmodell_i_id_pers_zeitmodell_i_id FOREIGN KEY (zeitmodell_i_id) REFERENCES public.pers_zeitmodell(i_id);
ALTER TABLE ONLY public.pers_zeitstift
    ADD CONSTRAINT fk_pers_zeitstift_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_zeitstift
    ADD CONSTRAINT fk_pers_zeitstift_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_zeitverteilung
    ADD CONSTRAINT fk_pers_zeitverteilung_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.pers_zeitverteilung
    ADD CONSTRAINT fk_pers_zeitverteilung_los_i_id_fert_los_i_id FOREIGN KEY (los_i_id) REFERENCES public.fert_los(i_id);
ALTER TABLE ONLY public.pers_zeitverteilung
    ADD CONSTRAINT "fk_pers_zeitverteilung_lossollarbeitsplan_i_id_fela$_i_id" FOREIGN KEY (lossollarbeitsplan_i_id) REFERENCES public.fert_lossollarbeitsplan(i_id);
ALTER TABLE ONLY public.pers_zeitverteilung
    ADD CONSTRAINT fk_pers_zeitverteilung_maschine_i_id_pers_maschine_i_id FOREIGN KEY (maschine_i_id) REFERENCES public.pers_maschine(i_id);
ALTER TABLE ONLY public.pers_zeitverteilung
    ADD CONSTRAINT fk_pers_zeitverteilung_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_zutrittdaueroffen
    ADD CONSTRAINT fk_pers_zutrittdaueroffen_tagesart_i_id_pers_tagesart_i_id FOREIGN KEY (tagesart_i_id) REFERENCES public.pers_tagesart(i_id);
ALTER TABLE ONLY public.pers_zutrittonlinecheck
    ADD CONSTRAINT fk_pers_zutrittonlinecheck_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_zutrittsklasse
    ADD CONSTRAINT fk_pers_zutrittsklasse_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_zutrittslog
    ADD CONSTRAINT fk_pers_zutrittslog_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_zutrittslog
    ADD CONSTRAINT fk_pers_zutrittslog_mandant_c_nr_objekt_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr_objekt) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_zutrittsmodell
    ADD CONSTRAINT fk_pers_zutrittsmodell_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_zutrittsmodelltag
    ADD CONSTRAINT fk_pers_zutrittsmodelltag_tagesart_i_id_pers_tagesart_i_id FOREIGN KEY (tagesart_i_id) REFERENCES public.pers_tagesart(i_id);
ALTER TABLE ONLY public.pers_zutrittsobjekt
    ADD CONSTRAINT fk_pers_zutrittsobjekt_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_zutrittsobjektverwendung
    ADD CONSTRAINT fk_pers_zutrittsobjektverwendung_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.pers_personalgruppekosten
    ADD CONSTRAINT fk_personalgruppekosten_personalgruppe_i_id_personalgruppe_i_id FOREIGN KEY (personalgruppe_i_id) REFERENCES public.pers_personalgruppe(i_id);
ALTER TABLE ONLY public.pers_taetigkeitartspr
    ADD CONSTRAINT "fk_pets$_taetigkeitart_c_nr_pers_taetigkeitart_c_nr" FOREIGN KEY (taetigkeitart_c_nr) REFERENCES public.pers_taetigkeitart(c_nr);
ALTER TABLE ONLY public.pers_zutrittonlinecheck
    ADD CONSTRAINT "fk_pezc$_zutrittsklasse_i_id_pezl$_i_id" FOREIGN KEY (zutrittsklasse_i_id) REFERENCES public.pers_zutrittsklasse(i_id);
ALTER TABLE ONLY public.pers_zutrittsmodelltagdetail
    ADD CONSTRAINT "fk_pezd$_zutrittsmodelltag_i_id_pezm$_i_id" FOREIGN KEY (zutrittsmodelltag_i_id) REFERENCES public.pers_zutrittsmodelltag(i_id);
ALTER TABLE ONLY public.pers_zutrittsmodelltagdetail
    ADD CONSTRAINT "fk_pezd$_zutrittsoeffnungsart_c_nr_peza$_c_nr" FOREIGN KEY (zutrittsoeffnungsart_c_nr) REFERENCES public.pers_zutrittsoeffnungsart(c_nr);
ALTER TABLE ONLY public.pers_zutrittdaueroffen
    ADD CONSTRAINT "fk_pezf$_zutrittsobjekt_i_id_pezo$_i_id" FOREIGN KEY (zutrittsobjekt_i_id) REFERENCES public.pers_zutrittsobjekt(i_id);
ALTER TABLE ONLY public.pers_zutrittsklasseobjekt
    ADD CONSTRAINT "fk_pezk$_zutrittsklasse_i_id_pezl$_i_id" FOREIGN KEY (zutrittsklasse_i_id) REFERENCES public.pers_zutrittsklasse(i_id);
ALTER TABLE ONLY public.pers_zutrittsklasseobjekt
    ADD CONSTRAINT "fk_pezk$_zutrittsmodell_i_id_pers_zutrittsmodell_i_id" FOREIGN KEY (zutrittsmodell_i_id) REFERENCES public.pers_zutrittsmodell(i_id);
ALTER TABLE ONLY public.pers_zutrittsklasseobjekt
    ADD CONSTRAINT "fk_pezk$_zutrittsobjekt_i_id_pezo$_i_id" FOREIGN KEY (zutrittsobjekt_i_id) REFERENCES public.pers_zutrittsobjekt(i_id);
ALTER TABLE ONLY public.pers_zutrittsmodelltag
    ADD CONSTRAINT "fk_pezm$_zutrittsmodell_i_id_pers_zutrittsmodell_i_id" FOREIGN KEY (zutrittsmodell_i_id) REFERENCES public.pers_zutrittsmodell(i_id);
ALTER TABLE ONLY public.pers_zutrittsobjekt
    ADD CONSTRAINT "fk_pezo$_zutrittscontroller_i_id_pers_zutrittscontroller_i_id" FOREIGN KEY (zutrittscontroller_i_id) REFERENCES public.pers_zutrittscontroller(i_id);
ALTER TABLE ONLY public.pers_zutrittsobjekt
    ADD CONSTRAINT "fk_pezo$_zutrittsleser_c_nr_pers_zutrittsleser_c_nr" FOREIGN KEY (zutrittsleser_c_nr) REFERENCES public.pers_zutrittsleser(c_nr);
ALTER TABLE ONLY public.pers_zeitmodelltagpause
    ADD CONSTRAINT "fk_pezp$_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_zeitmodelltagpause
    ADD CONSTRAINT "fk_pezp$_zeitmodelltag_i_id_pers_zeitmodelltag_i_id" FOREIGN KEY (zeitmodelltag_i_id) REFERENCES public.pers_zeitmodelltag(i_id);
ALTER TABLE ONLY public.pers_zutrittsobjektverwendung
    ADD CONSTRAINT "fk_pezv$_zutrittsobjekt_i_id_pezo$_i_id" FOREIGN KEY (zutrittsobjekt_i_id) REFERENCES public.pers_zutrittsobjekt(i_id);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT "fk_pjeg$_projekterledigungsgrund_i_id_pjeg$_i_id" FOREIGN KEY (projekterledigungsgrund_i_id) REFERENCES public.proj_projekterledigungsgrund(i_id);
ALTER TABLE ONLY public.pers_nachrichtenabo
    ADD CONSTRAINT "fk_pnab$_nachrichtenart_i_id_pers_nachrichtenart_i_id" FOREIGN KEY (nachrichtenart_i_id) REFERENCES public.pers_nachrichtenart(i_id);
ALTER TABLE ONLY public.pers_nachrichtenabo
    ADD CONSTRAINT "fk_pnab$_nachrichtengruppe_i_id_pers_nachrichtengruppe_i_id" FOREIGN KEY (nachrichtengruppe_i_id) REFERENCES public.pers_nachrichtengruppe(i_id);
ALTER TABLE ONLY public.pers_nachrichtengruppeteilnehmer
    ADD CONSTRAINT "fk_pnge$_nachrichtengruppe_i_id_pers_nachrichtengruppe_i_id" FOREIGN KEY (nachrichtengruppe_i_id) REFERENCES public.pers_nachrichtengruppe(i_id);
ALTER TABLE ONLY public.pers_nachrichtengruppeteilnehmer
    ADD CONSTRAINT "fk_pnge$_personal_i_id_pers_personal_i_id" FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.pers_uebertrag_bva
    ADD CONSTRAINT fk_ppers_uebertrag_bva_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.proj_bereich
    ADD CONSTRAINT fk_proj_bereich_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.proj_history
    ADD CONSTRAINT fk_proj_history_historyart_i_id_proj_history_i_id FOREIGN KEY (historyart_i_id) REFERENCES public.proj_historyart(i_id);
ALTER TABLE ONLY public.proj_history
    ADD CONSTRAINT "fk_proj_history_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.proj_history
    ADD CONSTRAINT "fk_proj_history_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.proj_history
    ADD CONSTRAINT fk_proj_history_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.proj_history
    ADD CONSTRAINT "fk_proj_history_personal_i_id_wirddurchgefuehrt_pep$_i_id" FOREIGN KEY (personal_i_id_wirddurchgefuehrt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.proj_history
    ADD CONSTRAINT fk_proj_history_projekt_i_id_proj_projekt_i_id FOREIGN KEY (projekt_i_id) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.proj_kategorie
    ADD CONSTRAINT fk_proj_kategorie_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.proj_kategoriespr
    ADD CONSTRAINT fk_proj_kategoriespr_kategorie_c_nr_proj_kategorie_c_nr FOREIGN KEY (kategorie_c_nr, mandant_c_nr) REFERENCES public.proj_kategorie(c_nr, mandant_c_nr);
ALTER TABLE ONLY public.proj_kategoriespr
    ADD CONSTRAINT fk_proj_kategoriespr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.proj_kategoriespr
    ADD CONSTRAINT fk_proj_kategoriespr_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT "fk_proj_projekt_ansprechpartner_i_id_betreiber_paas$_i_id" FOREIGN KEY (ansprechpartner_i_id_betreiber) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_ansprechpartner_i_id_part_ansprechpartner_i_id FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_bereich_i_id_proj_bereich_i_id FOREIGN KEY (bereich_i_id) REFERENCES public.proj_bereich(i_id);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_c_attachmentstype_lp_datenformat_c_nr FOREIGN KEY (c_attachmentstype) REFERENCES public.lp_datenformat(c_nr);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_editorcontent_i_id_lp_editor_content_i_id FOREIGN KEY (editorcontent_i_id) REFERENCES public.lp_editor_content(i_id);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_kategorie_c_nr_proj_kategorie_c_nr FOREIGN KEY (kategorie_c_nr, mandant_c_nr) REFERENCES public.proj_kategorie(c_nr, mandant_c_nr);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_partner_i_id_betreiber_part_partner_i_id FOREIGN KEY (partner_i_id_betreiber) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_personal_i_id_erzeuger_pers_personal_i_id FOREIGN KEY (personal_i_id_erzeuger) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_personal_i_id_internerledigt_pers_personal_i_id FOREIGN KEY (personal_i_id_internerledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_personal_i_id_zugewiesener_pers_personal_i_id FOREIGN KEY (personal_i_id_zugewiesener) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_projekt_i_id_nachfolger_proj_projekt_i_id FOREIGN KEY (projekt_i_id_nachfolger) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_projektstatus_c_nr_proj_projektstatus_c_nr FOREIGN KEY (projektstatus_c_nr, mandant_c_nr) REFERENCES public.proj_projektstatus(c_nr, mandant_c_nr);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_projekttyp_c_nr_proj_projekttyp_c_nr FOREIGN KEY (projekttyp_c_nr, mandant_c_nr) REFERENCES public.proj_projekttyp(c_nr, mandant_c_nr);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT fk_proj_projekt_vkfortschritt_i_id_proj_vkfortschritt_i_id FOREIGN KEY (vkfortschritt_i_id) REFERENCES public.proj_vkfortschritt(i_id);
ALTER TABLE ONLY public.proj_projekterledigungsgrund
    ADD CONSTRAINT fk_proj_projekterledigungsgrund_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.proj_projektgruppe
    ADD CONSTRAINT fk_proj_projektgruppe_projekt_i_id_kind_proj_projekt_i_id FOREIGN KEY (projekt_i_id_kind) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.proj_projektgruppe
    ADD CONSTRAINT fk_proj_projektgruppe_projekt_i_id_vater_proj_projekt_i_id FOREIGN KEY (projekt_i_id_kind) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.proj_projektstatus
    ADD CONSTRAINT fk_proj_projektstatus_c_nr_lp_status_c_nr FOREIGN KEY (c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.proj_projektstatus
    ADD CONSTRAINT fk_proj_projektstatus_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.proj_projekttaetigkeit
    ADD CONSTRAINT fk_proj_projekttaetigkeit_artikel_i_id_pers_personal_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.proj_projekttaetigkeit
    ADD CONSTRAINT fk_proj_projekttaetigkeit_projekt_i_id_proj_projekt_i_id FOREIGN KEY (projekt_i_id) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.proj_projekttechniker
    ADD CONSTRAINT fk_proj_projekttechniker_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.proj_projekttechniker
    ADD CONSTRAINT fk_proj_projekttechniker_projekt_i_id_proj_projekt_i_id FOREIGN KEY (projekt_i_id) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.proj_projekttyp
    ADD CONSTRAINT fk_proj_projekttyp_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.proj_projekttypspr
    ADD CONSTRAINT fk_proj_projekttypspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.proj_projekttypspr
    ADD CONSTRAINT fk_proj_projekttypspr_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.proj_projekttypspr
    ADD CONSTRAINT fk_proj_projekttypspr_projekttyp_c_nr_proj_projekttyp_c_nr FOREIGN KEY (projekttyp_c_nr, mandant_c_nr) REFERENCES public.proj_projekttyp(c_nr, mandant_c_nr);
ALTER TABLE ONLY public.proj_vkfortschritt
    ADD CONSTRAINT fk_proj_vkfortschritt_leadstatus_i_id_proj_leadstatus_i_id FOREIGN KEY (leadstatus_i_id) REFERENCES public.proj_leadstatus(i_id);
ALTER TABLE ONLY public.proj_vkfortschritt
    ADD CONSTRAINT fk_proj_vkfortschritt_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.proj_vkfortschrittspr
    ADD CONSTRAINT fk_proj_vkfortschrittspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.proj_vkfortschrittspr
    ADD CONSTRAINT fk_proj_vkfortschrittspr_vkfortschritt_i_id_proj_vkfortschritt_ FOREIGN KEY (vkfortschritt_i_id) REFERENCES public.proj_vkfortschritt(i_id);
ALTER TABLE ONLY public.rech_abrechnungsvorschlag
    ADD CONSTRAINT fk_rech_abrechnungsvorschlag_auftrag_i_id_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.rech_abrechnungsvorschlag
    ADD CONSTRAINT fk_rech_abrechnungsvorschlag_auftragszuordnung_i_id_er_auftrags FOREIGN KEY (auftragszuordnung_i_id) REFERENCES public.er_auftragszuordnung(i_id);
ALTER TABLE ONLY public.rech_abrechnungsvorschlag
    ADD CONSTRAINT fk_rech_abrechnungsvorschlag_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.rech_abrechnungsvorschlag
    ADD CONSTRAINT fk_rech_abrechnungsvorschlag_los_i_id_fert_los_i_id FOREIGN KEY (los_i_id) REFERENCES public.fert_los(i_id);
ALTER TABLE ONLY public.rech_abrechnungsvorschlag
    ADD CONSTRAINT fk_rech_abrechnungsvorschlag_mandant_c_nr_pers_personal_i_id FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.rech_abrechnungsvorschlag
    ADD CONSTRAINT fk_rech_abrechnungsvorschlag_maschinenzeitdaten_i_id_pers_masch FOREIGN KEY (maschinenzeitdaten_i_id) REFERENCES public.pers_maschinenzeitdaten(i_id);
ALTER TABLE ONLY public.rech_abrechnungsvorschlag
    ADD CONSTRAINT fk_rech_abrechnungsvorschlag_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rech_abrechnungsvorschlag
    ADD CONSTRAINT fk_rech_abrechnungsvorschlag_projekt_i_id_proj_projekt_i_id FOREIGN KEY (projekt_i_id) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.rech_abrechnungsvorschlag
    ADD CONSTRAINT fk_rech_abrechnungsvorschlag_reise_i_id_pers_reise_i_id FOREIGN KEY (reise_i_id) REFERENCES public.pers_reise(i_id);
ALTER TABLE ONLY public.rech_abrechnungsvorschlag
    ADD CONSTRAINT fk_rech_abrechnungsvorschlag_telefonzeiten_i_id_pers_telefonzei FOREIGN KEY (telefonzeiten_i_id) REFERENCES public.pers_telefonzeiten(i_id);
ALTER TABLE ONLY public.rech_abrechnungsvorschlag
    ADD CONSTRAINT fk_rech_abrechnungsvorschlag_waehrung_c_nr_rechnung_lp_waehrung FOREIGN KEY (waehrung_c_nr_rechnung) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.rech_abrechnungsvorschlag
    ADD CONSTRAINT fk_rech_abrechnungsvorschlag_zeitdaten_i_id_pers_zeitdaten_i_id FOREIGN KEY (zeitdaten_i_id) REFERENCES public.pers_zeitdaten(i_id);
ALTER TABLE ONLY public.rech_gutschriftsgrundspr
    ADD CONSTRAINT "fk_rech_gutschriftsgrundspr_gutschriftsgrund_i_id_argg$_i_id" FOREIGN KEY (gutschriftsgrund_i_id) REFERENCES public.rech_gutschriftsgrund(i_id);
ALTER TABLE ONLY public.rech_gutschriftsgrundspr
    ADD CONSTRAINT fk_rech_gutschriftsgrundspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.rech_lastschriftvorschlag
    ADD CONSTRAINT fk_rech_lastschriftvorschlag_mahnlauf_i_id_fb_mahnlauf_i_id FOREIGN KEY (mahnlauf_i_id) REFERENCES public.fb_mahnlauf(i_id);
ALTER TABLE ONLY public.rech_lastschriftvorschlag
    ADD CONSTRAINT fk_rech_lastschriftvorschlag_personal_i_id_aendern FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rech_lastschriftvorschlag
    ADD CONSTRAINT fk_rech_lastschriftvorschlag_personal_i_id_gespeichert FOREIGN KEY (personal_i_id_gespeichert) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rech_lastschriftvorschlag
    ADD CONSTRAINT fk_rech_lastschriftvorschlag_rechnung_i_id_rech_rechnung_i_id FOREIGN KEY (rechnung_i_id) REFERENCES public.rech_rechnung(i_id);
ALTER TABLE ONLY public.rech_mmz
    ADD CONSTRAINT fk_rech_mmz_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.rech_mmz
    ADD CONSTRAINT fk_rech_mmz_land_i_id_lp_land_i_id FOREIGN KEY (land_i_id) REFERENCES public.lp_land(i_id);
ALTER TABLE ONLY public.rech_mmz
    ADD CONSTRAINT fk_rech_mmz_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_ansprechpartner_i_id_part_ansprechpartner_i_id FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_auftrag_i_id_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_konto_i_id_fb_konto_i_id FOREIGN KEY (konto_i_id) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_kunde_i_id_statistikadresse_part_kunde_i_id FOREIGN KEY (kunde_i_id_statistikadresse) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_lieferart_i_id_lp_lieferart_i_id FOREIGN KEY (lieferart_i_id) REFERENCES public.lp_lieferart(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_lieferschein_i_id_ls_lieferschein_i_id FOREIGN KEY (lieferschein_i_id) REFERENCES public.ls_lieferschein(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_lp_geschaeftsjahr FOREIGN KEY (i_geschaeftsjahr) REFERENCES public.lp_geschaeftsjahr(i_geschaeftsjahr);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_mwstsatz_i_id_lp_mwstsatz_i_id FOREIGN KEY (mwstsatz_i_id) REFERENCES public.lp_mwstsatz(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_personal_i_id_storniert_pers_personal_i_id FOREIGN KEY (personal_i_id_storniert) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_personal_i_id_vertreter_pers_personal_i_id FOREIGN KEY (personal_i_id_vertreter) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_personal_i_id_zollpapier_pers_personal_i_id FOREIGN KEY (personal_i_id_zollpapier) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_projekt_i_id_proj_projekt_i_id FOREIGN KEY (projekt_i_id) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_rech_gutschriftsgrund_i_id FOREIGN KEY (gutschriftsgrund_i_id) REFERENCES public.rech_gutschriftsgrund(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_rechnung_i_id_zurechnung_rech_rechnung_i_id FOREIGN KEY (rechnung_i_id_zurechnung) REFERENCES public.rech_rechnung(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_rechnungart_c_nr_rech_rechnungart_c_nr FOREIGN KEY (rechnungart_c_nr) REFERENCES public.rech_rechnungart(c_nr);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_reversechargeart_i_id_fb_reversechargeart_i_id FOREIGN KEY (reversechargeart_i_id) REFERENCES public.fb_reversechargeart(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_spediteur_i_id_lp_spediteur_i_id FOREIGN KEY (spediteur_i_id) REFERENCES public.lp_spediteur(i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.rech_rechnungstatus(status_c_nr);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_waehrung_c_nr_lp_waehrung_c_nr FOREIGN KEY (waehrung_c_nr) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_zahlungsart_c_nr_rech_zahlungsart_c_nr FOREIGN KEY (zahlungsart_c_nr) REFERENCES public.rech_zahlungsart(c_nr);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT fk_rech_rechnung_zahlungsziel_i_id_lp_zahlungsziel_i_id FOREIGN KEY (zahlungsziel_i_id) REFERENCES public.lp_zahlungsziel(i_id);
ALTER TABLE ONLY public.rech_rechnungart
    ADD CONSTRAINT fk_rech_rechnungart_rechnungtyp_c_nr_rech_rechnungtyp_c_nr FOREIGN KEY (rechnungtyp_c_nr) REFERENCES public.rech_rechnungtyp(c_nr);
ALTER TABLE ONLY public.rech_rechnungartspr
    ADD CONSTRAINT fk_rech_rechnungartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.rech_rechnungartspr
    ADD CONSTRAINT fk_rech_rechnungartspr_rechnungart_c_nr_rech_rechnungart_c_nr FOREIGN KEY (rechnungart_c_nr) REFERENCES public.rech_rechnungart(c_nr);
ALTER TABLE ONLY public.rech_rechnungkontierung
    ADD CONSTRAINT fk_rech_rechnungkontierung_rechnung_i_id_rech_rechnung_i_id FOREIGN KEY (rechnung_i_id) REFERENCES public.rech_rechnung(i_id);
ALTER TABLE ONLY public.rech_rechnungposition
    ADD CONSTRAINT fk_rech_rechnungposition_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.rech_rechnungposition
    ADD CONSTRAINT fk_rech_rechnungposition_einheit_c_nr_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.rech_rechnungposition
    ADD CONSTRAINT fk_rech_rechnungposition_lieferschein_i_id_ls_lieferschein_i_id FOREIGN KEY (lieferschein_i_id) REFERENCES public.ls_lieferschein(i_id);
ALTER TABLE ONLY public.rech_rechnungposition
    ADD CONSTRAINT fk_rech_rechnungposition_mwstsatz_i_id_lp_mwstsatz_i_id FOREIGN KEY (mwstsatz_i_id) REFERENCES public.lp_mwstsatz(i_id);
ALTER TABLE ONLY public.rech_rechnungposition
    ADD CONSTRAINT fk_rech_rechnungposition_positionsart_c_nr_lp_positionsart_c_nr FOREIGN KEY (positionsart_c_nr) REFERENCES public.lp_positionsart(c_nr);
ALTER TABLE ONLY public.rech_rechnungposition
    ADD CONSTRAINT fk_rech_rechnungposition_rechnung_i_id_rech_rechnung_i_id FOREIGN KEY (rechnung_i_id) REFERENCES public.rech_rechnung(i_id);
ALTER TABLE ONLY public.rech_rechnungposition
    ADD CONSTRAINT fk_rech_rechnungposition_verleih_i_id_ww_verleih_i_id FOREIGN KEY (verleih_i_id) REFERENCES public.ww_verleih(i_id);
ALTER TABLE ONLY public.rech_rechnungstatus
    ADD CONSTRAINT fk_rech_rechnungstatus_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.rech_rechnungtext
    ADD CONSTRAINT fk_rech_rechnungtext_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.rech_rechnungtext
    ADD CONSTRAINT fk_rech_rechnungtext_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.rech_rechnungzahlung
    ADD CONSTRAINT fk_rech_rechnungzahlung_kassenbuch_i_id_fb_kassenbuch_i_id FOREIGN KEY (kassenbuch_i_id) REFERENCES public.fb_kassenbuch(i_id);
ALTER TABLE ONLY public.rech_rechnungzahlung
    ADD CONSTRAINT fk_rech_rechnungzahlung_rechnung_i_id_rech_rechnung_i_id FOREIGN KEY (rechnung_i_id) REFERENCES public.rech_rechnung(i_id);
ALTER TABLE ONLY public.rech_rechnungzahlung
    ADD CONSTRAINT fk_rech_rechnungzahlung_zahlungsart_c_nr_rech_zahlungsart_c_nr FOREIGN KEY (zahlungsart_c_nr) REFERENCES public.rech_zahlungsart(c_nr);
ALTER TABLE ONLY public.rech_rechnungzahlung
    ADD CONSTRAINT fk_rech_rechungzahlung_buchungdetail_i_id_fb_buchungdetail_i_id FOREIGN KEY (buchungdetail_i_id) REFERENCES public.fb_buchungdetail(i_id);
ALTER TABLE ONLY public.rech_rechnungzahlung
    ADD CONSTRAINT fk_rech_rechzahlung_er_id_er_eingangsrechnung_i_id FOREIGN KEY (eingangsrechnung_i_id) REFERENCES public.er_eingangsrechnung(i_id);
ALTER TABLE ONLY public.rech_rechnungzahlung
    ADD CONSTRAINT fk_rech_rechzahlung_i_id_gutschrift_rech_rechzahlung_i_id FOREIGN KEY (rechnungzahlung_i_id_gutschrift) REFERENCES public.rech_rechnungzahlung(i_id);
ALTER TABLE ONLY public.rech_verrechnungsmodell
    ADD CONSTRAINT fk_rech_verrechnungsmodell_artikel_i_id_er_ww_artikel_i_id FOREIGN KEY (artikel_i_id_er) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.rech_verrechnungsmodelltag
    ADD CONSTRAINT fk_rech_verrechnungsmodell_artikel_i_id_gebucht_ww_artikel_i_id FOREIGN KEY (artikel_i_id_gebucht) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.rech_verrechnungsmodell
    ADD CONSTRAINT fk_rech_verrechnungsmodell_artikel_i_id_reisekilometer_ww_artik FOREIGN KEY (artikel_i_id_reisekilometer) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.rech_verrechnungsmodell
    ADD CONSTRAINT fk_rech_verrechnungsmodell_artikel_i_id_reisespesen_ww_artikel_ FOREIGN KEY (artikel_i_id_reisespesen) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.rech_verrechnungsmodell
    ADD CONSTRAINT fk_rech_verrechnungsmodell_artikel_i_id_telefon_ww_artikel_i_id FOREIGN KEY (artikel_i_id_telefon) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.rech_verrechnungsmodelltag
    ADD CONSTRAINT fk_rech_verrechnungsmodell_artikel_i_id_zuverrechnen_gebucht_ww FOREIGN KEY (artikel_i_id_zuverrechnen) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.rech_verrechnungsmodell
    ADD CONSTRAINT fk_rech_verrechnungsmodell_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.rech_verrechnungsmodelltag
    ADD CONSTRAINT fk_rech_verrechnungsmodelltag_tagesart_i_id_pers_tagesart_i_id FOREIGN KEY (tagesart_i_id) REFERENCES public.pers_tagesart(i_id);
ALTER TABLE ONLY public.rech_verrechnungsmodelltag
    ADD CONSTRAINT fk_rech_verrechnungsmodelltag_verrechnungsmodell_i_id_rech_verr FOREIGN KEY (verrechnungsmodell_i_id) REFERENCES public.rech_verrechnungsmodell(i_id);
ALTER TABLE ONLY public.rech_vorkasseposition
    ADD CONSTRAINT fk_rech_vorkasseposition_auftragsposition_i_id_auft_auftragposi FOREIGN KEY (auftragsposition_i_id) REFERENCES public.auft_auftragposition(i_id);
ALTER TABLE ONLY public.rech_vorkasseposition
    ADD CONSTRAINT fk_rech_vorkasseposition_rechnung_i_id_rech_rechnung_i_id FOREIGN KEY (rechnung_i_id) REFERENCES public.rech_rechnung(i_id);
ALTER TABLE ONLY public.rech_zahlungsartspr
    ADD CONSTRAINT fk_rech_zahlungsartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.rech_zahlungsartspr
    ADD CONSTRAINT fk_rech_zahlungsartspr_zahlungsart_c_nr_rech_zahlungsart_c_nr FOREIGN KEY (zahlungsart_c_nr) REFERENCES public.rech_zahlungsart(c_nr);
ALTER TABLE ONLY public.rekla_aufnahmeartspr
    ADD CONSTRAINT fk_rekla_aufnahmeartspr_aufnahmeart_i_id_rekla_aufnahmeart_i_id FOREIGN KEY (aufnahmeart_i_id) REFERENCES public.rekla_aufnahmeart(i_id);
ALTER TABLE ONLY public.rekla_aufnahmeartspr
    ADD CONSTRAINT fk_rekla_aufnahmeartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.rekla_behandlungspr
    ADD CONSTRAINT fk_rekla_behandlungspr_behandlung_i_id_rekla_behandlung_i_id FOREIGN KEY (behandlung_i_id) REFERENCES public.rekla_behandlung(i_id);
ALTER TABLE ONLY public.rekla_behandlungspr
    ADD CONSTRAINT fk_rekla_behandlungspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.rekla_fehlerangabespr
    ADD CONSTRAINT fk_rekla_fehlerangabespr_fehlerangabe_i_id_rekla_fehlerangabe_i FOREIGN KEY (fehlerangabe_i_id) REFERENCES public.rekla_fehlerangabe(i_id);
ALTER TABLE ONLY public.rekla_fehlerangabespr
    ADD CONSTRAINT fk_rekla_fehlerangabespr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.rekla_fehlerspr
    ADD CONSTRAINT fk_rekla_fehlerspr_fehler_i_id_rekla_fehler_i_id FOREIGN KEY (fehler_i_id) REFERENCES public.rekla_fehler(i_id);
ALTER TABLE ONLY public.rekla_fehlerspr
    ADD CONSTRAINT fk_rekla_fehlerspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.rekla_massnahmespr
    ADD CONSTRAINT fk_rekla_massnahmespr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.rekla_massnahmespr
    ADD CONSTRAINT fk_rekla_massnahmespr_massnahme_i_id_rekla_massnahme_i_id FOREIGN KEY (massnahme_i_id) REFERENCES public.rekla_massnahme(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT "fk_rekla_reklamation_ansprechpartner_i_id_lieferant_paap$_i_id" FOREIGN KEY (ansprechpartner_i_id_lieferant) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_ansprechpartner_i_id_part_ansprechpartner_ FOREIGN KEY (ansprechpartner_i_id) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_aufnahmeart_i_id_rekla_aufnahmeart_i_id FOREIGN KEY (aufnahmeart_i_id) REFERENCES public.rekla_aufnahmeart(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_behandlung_i_id_rekla_behandlung_i_id FOREIGN KEY (behandlung_i_id) REFERENCES public.rekla_behandlung(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_bestellung_i_id_bes_bestellung_i_id FOREIGN KEY (bestellung_i_id) REFERENCES public.bes_bestellung(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_fehler_i_id_rekla_fehler_i_id FOREIGN KEY (fehler_i_id) REFERENCES public.rekla_fehler(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_fehlerangabe_i_id_rekla_fehlerangabe_i_id FOREIGN KEY (fehlerangabe_i_id) REFERENCES public.rekla_fehlerangabe(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_lieferant_i_id_part_lieferant_i_id FOREIGN KEY (lieferant_i_id) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_lieferschein_i_id_ls_lieferschein_i_id FOREIGN KEY (lieferschein_i_id) REFERENCES public.ls_lieferschein(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_los_i_id_fert_los_i_id FOREIGN KEY (los_i_id) REFERENCES public.fert_los(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT "fk_rekla_reklamation_lossollarbeitsplan_i_id_fela$_i_id" FOREIGN KEY (lossollarbeitsplan_i_id) REFERENCES public.fert_lossollarbeitsplan(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_maschine_i_id_pers_maschine_i_id FOREIGN KEY (maschine_i_id) REFERENCES public.pers_maschine(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_massnahme_i_id_kurz_rekla_massnahme_i_id FOREIGN KEY (massnahme_i_id_kurz) REFERENCES public.rekla_massnahme(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_massnahme_i_id_lang_rekla_massnahme_i_id FOREIGN KEY (massnahme_i_id_lang) REFERENCES public.rekla_massnahme(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_massnahme_i_id_mittel_rekla_massnahme_i_id FOREIGN KEY (massnahme_i_id_mittel) REFERENCES public.rekla_massnahme(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT "fk_rekla_reklamation_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT "fk_rekla_reklamation_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT "fk_rekla_reklamation_personal_i_id_aufnehmer_pepe$_i_id" FOREIGN KEY (personal_i_id_aufnehmer) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT "fk_rekla_reklamation_personal_i_id_eingefuehrtkurz_pepe$_i_id" FOREIGN KEY (personal_i_id_eingefuehrtkurz) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT "fk_rekla_reklamation_personal_i_id_eingefuehrtlang_pepe$_i_id" FOREIGN KEY (personal_i_id_eingefuehrtlang) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT "fk_rekla_reklamation_personal_i_id_eingefuehrtmittel_pepe$_i_id" FOREIGN KEY (personal_i_id_eingefuehrtmittel) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT "fk_rekla_reklamation_personal_i_id_erledigt_pepe$_i_id" FOREIGN KEY (personal_i_id_erledigt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT "fk_rekla_reklamation_personal_i_id_ruecksprache_pepe$_i_id" FOREIGN KEY (personal_i_id_ruecksprache) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT "fk_rekla_reklamation_personal_i_id_verursacher_pepe$_i_id" FOREIGN KEY (personal_i_id_verursacher) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT "fk_rekla_reklamation_personal_i_id_wirksamkeit_pepe$_i_id" FOREIGN KEY (personal_i_id_wirksamkeit) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_projekt_i_id_proj_projekt_i_id FOREIGN KEY (projekt_i_id) REFERENCES public.proj_projekt(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_rechnung_i_id_rech_rechnung_i_id FOREIGN KEY (rechnung_i_id) REFERENCES public.rech_rechnung(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_reklamationart_c_nr_rekla_reklamationart_c FOREIGN KEY (reklamationart_c_nr) REFERENCES public.rekla_reklamationart(c_nr);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_schwere_i_id_erledigt_rekla_schwere_i_id FOREIGN KEY (schwere_i_id) REFERENCES public.rekla_schwere(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_status_c_nr_lp_status_c_nr FOREIGN KEY (status_c_nr) REFERENCES public.lp_status(c_nr);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_wareneingang_i_id_bes_wareneingang_i_id FOREIGN KEY (wareneingang_i_id) REFERENCES public.bes_wareneingang(i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT fk_rekla_reklamation_wirksamkeit_i_id_rekla_wirksamkeit_i_id FOREIGN KEY (wirksamkeit_i_id) REFERENCES public.rekla_wirksamkeit(i_id);
ALTER TABLE ONLY public.rekla_reklamationbild
    ADD CONSTRAINT fk_rekla_reklamationbild_datenformat_c_nr_lp_datenformat_c_nr FOREIGN KEY (datenformat_c_nr) REFERENCES public.lp_datenformat(c_nr);
ALTER TABLE ONLY public.rekla_reklamationbild
    ADD CONSTRAINT "fk_rekla_reklamationbild_reklamation_i_id_rekla$_i_id" FOREIGN KEY (reklamation_i_id) REFERENCES public.rekla_reklamation(i_id);
ALTER TABLE ONLY public.rekla_schwerespr
    ADD CONSTRAINT fk_rekla_schwerespr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.rekla_schwerespr
    ADD CONSTRAINT fk_rekla_schwerespr_schwere_i_id_rekla_schwere_i_id FOREIGN KEY (schwere_i_id) REFERENCES public.rekla_schwere(i_id);
ALTER TABLE ONLY public.rekla_wirksamkeitspr
    ADD CONSTRAINT fk_rekla_wirksamkeitspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.rekla_wirksamkeitspr
    ADD CONSTRAINT fk_rekla_wirksamkeitspr_wirksamkeit_i_id_rekla_wirksamkeit_i_id FOREIGN KEY (wirksamkeit_i_id) REFERENCES public.rekla_wirksamkeit(i_id);
ALTER TABLE ONLY public.rech_rechnungposition
    ADD CONSTRAINT "fk_repo$_kostentraeger_i_id_lp_kostentraeger_i_id" FOREIGN KEY (kostentraeger_i_id) REFERENCES public.lp_kostentraeger(i_id);
ALTER TABLE ONLY public.rech_rechnungposition
    ADD CONSTRAINT "fk_repo$_position_i_id_zugehoerig_repo$_i_id" FOREIGN KEY (position_i_id_zugehoerig) REFERENCES public.rech_rechnungposition(i_id);
ALTER TABLE ONLY public.pers_schichtzeit
    ADD CONSTRAINT fk_schichtzeit_schicht_i_id_pers_schicht_i_id FOREIGN KEY (schicht_i_id) REFERENCES public.pers_schicht(i_id);
ALTER TABLE ONLY public.pers_schichtzeit
    ADD CONSTRAINT fk_schichtzeit_schichtzuschlag_i_id_pers_schichtzuschlag_i_id FOREIGN KEY (schichtzuschlag_i_id) REFERENCES public.pers_schichtzuschlag(i_id);
ALTER TABLE ONLY public.stk_stuecklistearbeitsplan
    ADD CONSTRAINT "fk_stap$_apkommentar_i_id_stk_apkommentar_i_id" FOREIGN KEY (apkommentar_i_id) REFERENCES public.stk_apkommentar(i_id);
ALTER TABLE ONLY public.stk_stuecklistearbeitsplan
    ADD CONSTRAINT "fk_stap$_stueckliste_i_id_stk_stueckliste_i_id" FOREIGN KEY (stueckliste_i_id) REFERENCES public.stk_stueckliste(i_id);
ALTER TABLE ONLY public.stk_stuecklistearbeitsplan
    ADD CONSTRAINT "fk_stap$_stuecklisteposition_i_id_stk_stuecklisteposition_i_id" FOREIGN KEY (stuecklisteposition_i_id) REFERENCES public.stk_stuecklisteposition(i_id);
ALTER TABLE ONLY public.stk_alternativmaschine
    ADD CONSTRAINT "fk_stav$_stuecklistearbeitsplan_i_id_stap$_i_id" FOREIGN KEY (stuecklistearbeitsplan_i_id) REFERENCES public.stk_stuecklistearbeitsplan(i_id);
ALTER TABLE ONLY public.stk_alternativmaschine
    ADD CONSTRAINT fk_stk_alternativmaschine_maschine_i_id_pers_maschine_i_id FOREIGN KEY (maschine_i_id) REFERENCES public.pers_maschine(i_id);
ALTER TABLE ONLY public.stk_apkommentar
    ADD CONSTRAINT fk_stk_apkommentar_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.stk_apkommentarspr
    ADD CONSTRAINT fk_stk_apkommentarspr_apkommentar_i_id_stk_apkommentar_i_id FOREIGN KEY (apkommentar_i_id) REFERENCES public.stk_apkommentar(i_id);
ALTER TABLE ONLY public.stk_apkommentarspr
    ADD CONSTRAINT fk_stk_apkommentarspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.stk_fertigungsgruppe
    ADD CONSTRAINT fk_stk_fertigungsgruppe_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.stk_kommentarimport
    ADD CONSTRAINT "fk_stk_kommentarimport_artikelkommentarart_i_id_arka$_i_id" FOREIGN KEY (artikelkommentarart_i_id) REFERENCES public.ww_artikelkommentarart(i_id);
ALTER TABLE ONLY public.stk_kommentarimport
    ADD CONSTRAINT fk_stk_kommentarimport_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.stk_montageart
    ADD CONSTRAINT fk_stk_montageart_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.stk_montageart
    ADD CONSTRAINT fk_stk_montageart_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.stk_pruefartspr
    ADD CONSTRAINT fk_stk_pruefartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.stk_pruefartspr
    ADD CONSTRAINT fk_stk_pruefartspr_pruefart_i_id_stk_pruefart_i_id FOREIGN KEY (pruefart_i_id) REFERENCES public.stk_pruefart(i_id);
ALTER TABLE ONLY public.stk_pruefkombination
    ADD CONSTRAINT fk_stk_pruefkombination_artikel_i_id_kontakt_ww_artikel_i_id FOREIGN KEY (artikel_i_id_kontakt) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.stk_pruefkombination
    ADD CONSTRAINT fk_stk_pruefkombination_artikel_i_id_litze2_ww_artikel_i_id FOREIGN KEY (artikel_i_id_litze2) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.stk_pruefkombination
    ADD CONSTRAINT fk_stk_pruefkombination_artikel_i_id_litze_ww_artikel_i_id FOREIGN KEY (artikel_i_id_litze) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.stk_pruefkombinationspr
    ADD CONSTRAINT fk_stk_pruefkombination_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.stk_pruefkombination
    ADD CONSTRAINT "fk_stk_pruefkombination_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.stk_pruefkombination
    ADD CONSTRAINT "fk_stk_pruefkombination_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.stk_pruefkombination
    ADD CONSTRAINT fk_stk_pruefkombination_pruefart_i_id_stk_pruefart_i_id FOREIGN KEY (pruefart_i_id) REFERENCES public.stk_pruefart(i_id);
ALTER TABLE ONLY public.stk_pruefkombination
    ADD CONSTRAINT fk_stk_pruefkombination_verschleissteil_i_id_ww_verschleissteil FOREIGN KEY (verschleissteil_i_id) REFERENCES public.ww_verschleissteil(i_id);
ALTER TABLE ONLY public.stk_pruefkombinationspr
    ADD CONSTRAINT "fk_stk_pruefkombinationspr_pruefkombination_i_id_sspk$_i_id" FOREIGN KEY (pruefkombination_i_id) REFERENCES public.stk_pruefkombination(i_id);
ALTER TABLE ONLY public.stk_stklagerentnahme
    ADD CONSTRAINT fk_stk_stklagerentnahme_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.stk_stklagerentnahme
    ADD CONSTRAINT fk_stk_stklagerentnahme_stk_stueckliste_i_id FOREIGN KEY (stueckliste_i_id) REFERENCES public.stk_stueckliste(i_id);
ALTER TABLE ONLY public.stk_stklagerentnahme
    ADD CONSTRAINT fk_stk_stklagerentnahme_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.stk_stklparameter
    ADD CONSTRAINT fk_stk_stklparameter_stueckliste_id_stk_stueckliste_i_id FOREIGN KEY (stueckliste_i_id) REFERENCES public.stk_stueckliste(i_id);
ALTER TABLE ONLY public.stk_stklparameterspr
    ADD CONSTRAINT fk_stk_stklparameterspr_stklparameter_i_id_stk_stklparameter_i_ FOREIGN KEY (stklparameter_i_id) REFERENCES public.stk_stklparameter(i_id);
ALTER TABLE ONLY public.stk_stklpruefplan
    ADD CONSTRAINT "fk_stk_stklpruefplan_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.stk_stklpruefplan
    ADD CONSTRAINT "fk_stk_stklpruefplan_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.stk_stklpruefplan
    ADD CONSTRAINT fk_stk_stklpruefplan_pruefart_i_id_stk_pruefart_i_id FOREIGN KEY (pruefart_i_id) REFERENCES public.stk_pruefart(i_id);
ALTER TABLE ONLY public.stk_stklpruefplan
    ADD CONSTRAINT fk_stk_stklpruefplan_stueckliste_i_id_stk_stueckliste_i_id FOREIGN KEY (stueckliste_i_id) REFERENCES public.stk_stueckliste(i_id);
ALTER TABLE ONLY public.stk_stklpruefplan
    ADD CONSTRAINT "fk_stk_stklpruefplan_stuecklisteposition_i_id_kontakt_$stpo_i_i" FOREIGN KEY (stuecklisteposition_i_id_kontakt) REFERENCES public.stk_stuecklisteposition(i_id);
ALTER TABLE ONLY public.stk_stklpruefplan
    ADD CONSTRAINT "fk_stk_stklpruefplan_stuecklisteposition_i_id_litze2_$stpo_i_id" FOREIGN KEY (stuecklisteposition_i_id_litze2) REFERENCES public.stk_stuecklisteposition(i_id);
ALTER TABLE ONLY public.stk_stklpruefplan
    ADD CONSTRAINT "fk_stk_stklpruefplan_stuecklisteposition_i_id_litze_$stpo_i_id" FOREIGN KEY (stuecklisteposition_i_id_litze) REFERENCES public.stk_stuecklisteposition(i_id);
ALTER TABLE ONLY public.stk_stklpruefplan
    ADD CONSTRAINT "fk_stk_stklpruefplan_verschleissteil_i_id_$wwvt_i_id" FOREIGN KEY (verschleissteil_i_id) REFERENCES public.ww_verschleissteil(i_id);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT fk_stk_stueckliste_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT fk_stk_stueckliste_auftrag_i_id_leitauftrag_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id_leitauftrag) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT "fk_stk_stueckliste_fertigungsgruppe_i_id_stfg$_i_id" FOREIGN KEY (fertigungsgruppe_i_id) REFERENCES public.stk_fertigungsgruppe(i_id);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT fk_stk_stueckliste_lager_i_id_ziellager_ww_lager_i_id FOREIGN KEY (lager_i_id_ziellager) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT fk_stk_stueckliste_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT fk_stk_stueckliste_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT fk_stk_stueckliste_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT "fk_stk_stueckliste_personal_i_id_aendernarbeitsplan_pepe$_i_id" FOREIGN KEY (personal_i_id_aendernarbeitsplan) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT "fk_stk_stueckliste_personal_i_id_aendernposition_pepe$_i_id" FOREIGN KEY (personal_i_id_aendernposition) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT fk_stk_stueckliste_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT fk_stk_stueckliste_personal_i_id_freigabe_pers_personal_i_id FOREIGN KEY (personal_i_id_freigabe) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT fk_stk_stueckliste_stueckliste_i_id_ek_stk_stueckliste_i_id FOREIGN KEY (stueckliste_i_id_ek) REFERENCES public.stk_stueckliste(i_id);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT fk_stk_stueckliste_stuecklisteart_c_nr_stk_stuecklisteart_c_nr FOREIGN KEY (stuecklisteart_c_nr) REFERENCES public.stk_stuecklisteart(c_nr);
ALTER TABLE ONLY public.stk_stuecklistearbeitsplan
    ADD CONSTRAINT fk_stk_stuecklistearbeitsplan_agart_c_nr_stk_agart_c_nr FOREIGN KEY (agart_c_nr) REFERENCES public.stk_agart(c_nr);
ALTER TABLE ONLY public.stk_stuecklistearbeitsplan
    ADD CONSTRAINT fk_stk_stuecklistearbeitsplan_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.stk_stuecklistearbeitsplan
    ADD CONSTRAINT fk_stk_stuecklistearbeitsplan_maschine_i_id_pers_maschine_i_id FOREIGN KEY (maschine_i_id) REFERENCES public.pers_maschine(i_id);
ALTER TABLE ONLY public.stk_stuecklisteeigenschaft
    ADD CONSTRAINT "fk_stk_stuecklisteeigenschaft_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.stk_stuecklisteposition
    ADD CONSTRAINT "fk_stk_stuecklisteposition_ansppartner_i_id_aendern_paans$_i_id" FOREIGN KEY (ansprechpartner_i_id_aendern) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.stk_stuecklisteposition
    ADD CONSTRAINT "fk_stk_stuecklisteposition_ansppartner_i_id_anlegen_paans$_i_id" FOREIGN KEY (ansprechpartner_i_id_anlegen) REFERENCES public.part_ansprechpartner(i_id);
ALTER TABLE ONLY public.stk_stuecklisteposition
    ADD CONSTRAINT fk_stk_stuecklisteposition_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.stk_stuecklisteposition
    ADD CONSTRAINT fk_stk_stuecklisteposition_einheit_c_nr_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.stk_stuecklisteposition
    ADD CONSTRAINT fk_stk_stuecklisteposition_montageart_i_id_stk_montageart_i_id FOREIGN KEY (montageart_i_id) REFERENCES public.stk_montageart(i_id);
ALTER TABLE ONLY public.stk_stuecklisteposition
    ADD CONSTRAINT "fk_stk_stuecklisteposition_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.stk_stuecklisteposition
    ADD CONSTRAINT "fk_stk_stuecklisteposition_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT fk_stk_stuecklistescriptart_i_id_stk_stuecklistescriptart FOREIGN KEY (stuecklistescriptart_i_id) REFERENCES public.stk_stuecklistescriptart(i_id);
ALTER TABLE ONLY public.stk_stuecklistescriptart
    ADD CONSTRAINT fk_stk_stuecklistescriptart_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.stk_posersatz
    ADD CONSTRAINT "fk_stpo$_artikel_i_id_ersatz_ww_artikel_i_id" FOREIGN KEY (artikel_i_id_ersatz) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.stk_stuecklisteposition
    ADD CONSTRAINT "fk_stpo$_stueckliste_i_id_stk_stueckliste_i_id" FOREIGN KEY (stueckliste_i_id) REFERENCES public.stk_stueckliste(i_id);
ALTER TABLE ONLY public.stk_posersatz
    ADD CONSTRAINT "fk_stpo$_stuecklisteposition_i_id_stpo$_i_id" FOREIGN KEY (stuecklisteposition_i_id) REFERENCES public.stk_stuecklisteposition(i_id);
ALTER TABLE ONLY public.stk_stuecklisteeigenschaft
    ADD CONSTRAINT "fk_stse$_stueckliste_i_id_stk_stueckliste_i_id" FOREIGN KEY (stueckliste_i_id) REFERENCES public.stk_stueckliste(i_id);
ALTER TABLE ONLY public.stk_stuecklisteeigenschaft
    ADD CONSTRAINT "fk_stse$_stuecklisteeigenschaftart_i_id_stse$art_i_id" FOREIGN KEY (stuecklisteeigenschaftart_i_id) REFERENCES public.stk_stuecklisteeigenschaftart(i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT fk_waehrung_c_nr_zusaetzlich_waehrung_c_nr_zusaetzlich_lp_waehr FOREIGN KEY (waehrung_c_nr_zusaetzlich) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.fert_wiederholendelose
    ADD CONSTRAINT "fk_whlo$_auftragwiederholungsintervall_c_nr_abwi$_c_nr" FOREIGN KEY (auftragwiederholungsintervall_c_nr) REFERENCES public.auft_auftragwiederholungsintervall(c_nr);
ALTER TABLE ONLY public.fert_wiederholendelose
    ADD CONSTRAINT "fk_whlo$_fertigungsgruppe_i_id_stfg$_i_id" FOREIGN KEY (fertigungsgruppe_i_id) REFERENCES public.stk_fertigungsgruppe(i_id);
ALTER TABLE ONLY public.fert_wiederholendelose
    ADD CONSTRAINT "fk_whlo$_kostenstelle_i_id_lp_kostenstelle_i_id" FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.fert_wiederholendelose
    ADD CONSTRAINT "fk_whlo$_partner_i_id_fertigungsort_papa$_i_id" FOREIGN KEY (partner_i_id_fertigungsort) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.fert_wiederholendelose
    ADD CONSTRAINT "fk_whlo$_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_alergen
    ADD CONSTRAINT fk_ww_alergen_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_artgru
    ADD CONSTRAINT fk_ww_artgru_artgru_i_id_ww_artgru_i_id FOREIGN KEY (artgru_i_id) REFERENCES public.ww_artgru(i_id);
ALTER TABLE ONLY public.ww_artgru
    ADD CONSTRAINT fk_ww_artgru_artikel_i_id_kommentar_ww_artikel_i_id FOREIGN KEY (artikel_i_id_kommentar) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artgru
    ADD CONSTRAINT fk_ww_artgru_kostenstelle_i_id_lp_kostenstelle_i_id FOREIGN KEY (kostenstelle_i_id) REFERENCES public.lp_kostenstelle(i_id);
ALTER TABLE ONLY public.ww_artgru
    ADD CONSTRAINT fk_ww_artgru_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_artgru
    ADD CONSTRAINT "fk_ww_artgru_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_artgru
    ADD CONSTRAINT "fk_ww_artgru_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_artgrumandant
    ADD CONSTRAINT fk_ww_artgrumandant_artgru_i_id_ww_artgru_i_id FOREIGN KEY (artgru_i_id) REFERENCES public.ww_artgru(i_id);
ALTER TABLE ONLY public.ww_artgrumandant
    ADD CONSTRAINT fk_ww_artgrumandant_konto_i_id_fb_konto_i_id FOREIGN KEY (konto_i_id) REFERENCES public.fb_konto(i_id);
ALTER TABLE ONLY public.ww_artgrumandant
    ADD CONSTRAINT fk_ww_artgrumandant_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_artgruspr
    ADD CONSTRAINT fk_ww_artgruspr_artgru_i_id_ww_artgru_i_id FOREIGN KEY (artgru_i_id) REFERENCES public.ww_artgru(i_id);
ALTER TABLE ONLY public.ww_artgruspr
    ADD CONSTRAINT fk_ww_artgruspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_artgru_i_id_ww_artgru_i_id FOREIGN KEY (artgru_i_id) REFERENCES public.ww_artgru(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_artikel_i_id_ersatz_ww_artikel_i_id FOREIGN KEY (artikel_i_id_ersatz) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_artikel_i_id_zugehoerig_ww_artikel_i_id FOREIGN KEY (artikel_i_id_zugehoerig) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_artikelart_c_nr_ww_artikelart_c_nr FOREIGN KEY (artikelart_c_nr) REFERENCES public.ww_artikelart(c_nr);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_artkla_i_id_ww_artkla_i_id FOREIGN KEY (artkla_i_id) REFERENCES public.ww_artkla(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_automotive_i_id_ww_automotiva_i_id FOREIGN KEY (automotive_i_id) REFERENCES public.ww_automotive(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_einheit_c_nr_bestellung_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr_bestellung) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_einheit_c_nr_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_farbcode_i_id_ww_farbcode_i_id FOREIGN KEY (farbcode_i_id) REFERENCES public.ww_farbcode(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_hersteller_i_id_ww_hersteller_i_id FOREIGN KEY (hersteller_i_id) REFERENCES public.ww_hersteller(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_land_i_id_ursprungsland_lp_land_i_id FOREIGN KEY (land_i_id_ursprungsland) REFERENCES public.lp_land(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_laseroberflaeche_i_id_ww_laseroberflaeche_i_id FOREIGN KEY (laseroberflaeche_i_id) REFERENCES public.ww_laseroberflaeche(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_lfliefergruppe_i_id_part_lfliefergruppe_i_id FOREIGN KEY (lfliefergruppe_i_id) REFERENCES public.part_lfliefergruppe(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_material_i_id_ww_material_i_id FOREIGN KEY (material_i_id) REFERENCES public.ww_material(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_medical_i_id_ww_medical_i_id FOREIGN KEY (medical_i_id) REFERENCES public.ww_medical(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_mwstsatz_i_id_lp_mwstsatzbez_i_id FOREIGN KEY (mwstsatz_i_id) REFERENCES public.lp_mwstsatzbez(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_personal_i_id_anlegen_pers_personal_i_id FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_personal_i_id_freigabe_pers_personal_i_id FOREIGN KEY (personal_i_id_freigabe) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT "fk_ww_artikel_personal_i_id_letztewartung_pepe$_i_id" FOREIGN KEY (personal_i_id_letztewartung) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_reach_i_id_ww_reach_i_id FOREIGN KEY (reach_i_id) REFERENCES public.ww_reach(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_rohs_i_id_ww_rohs_i_id FOREIGN KEY (rohs_i_id) REFERENCES public.ww_rohs(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_shopgruppe_i_id_ww_shopgruppe_i_id FOREIGN KEY (shopgruppe_i_id) REFERENCES public.ww_shopgruppe(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_verpackungsmittel_i_id_ww_verpackungsmittel_i_id FOREIGN KEY (verpackungsmittel_i_id) REFERENCES public.ww_verpackungsmittel(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_vorzug_i_id_ww_ww_vorzug_i_id FOREIGN KEY (vorzug_i_id) REFERENCES public.ww_vorzug(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_waffenausfuehrung_i_id_ww_waffenausfuehrung_i_id FOREIGN KEY (waffenausfuehrung_i_id) REFERENCES public.ww_waffenausfuehrung(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_waffenkaliber_i_id_ww_waffenkaliber_i_id FOREIGN KEY (waffenkaliber_i_id) REFERENCES public.ww_waffenkaliber(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_waffenkategorie_i_id_ww_waffenkategorie_i_id FOREIGN KEY (waffenkategorie_i_id) REFERENCES public.ww_waffenkategorie(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_waffentyp_fein_i_id_ww_waffentyp_fein_i_id FOREIGN KEY (waffentyp_fein_i_id) REFERENCES public.ww_waffentyp_fein(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_waffentyp_i_id_ww_waffentyp_i_id FOREIGN KEY (waffentyp_i_id) REFERENCES public.ww_waffentyp(i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT fk_ww_artikel_waffenzusatz_i_id_ww_waffenzusatz_i_id FOREIGN KEY (waffenzusatz_i_id) REFERENCES public.ww_waffenzusatz(i_id);
ALTER TABLE ONLY public.ww_artikelalergen
    ADD CONSTRAINT fk_ww_artikelalergen_alergen_i_id_ww_alergen_i_id FOREIGN KEY (alergen_i_id) REFERENCES public.ww_alergen(i_id);
ALTER TABLE ONLY public.ww_artikelalergen
    ADD CONSTRAINT fk_ww_artikelalergen_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikelartspr
    ADD CONSTRAINT fk_ww_artikelartspr_artikelart_c_nr_ww_artikelart_c_nr FOREIGN KEY (artikelart_c_nr) REFERENCES public.ww_artikelart(c_nr);
ALTER TABLE ONLY public.ww_artikelartspr
    ADD CONSTRAINT fk_ww_artikelartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.ww_artikelbestellt
    ADD CONSTRAINT fk_ww_artikelbestellt_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikelbestellt
    ADD CONSTRAINT fk_ww_artikelbestellt_lp_belegart FOREIGN KEY (c_belegartnr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.ww_artikelfehlmenge
    ADD CONSTRAINT fk_ww_artikelfehlmenge_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikelkommentar
    ADD CONSTRAINT fk_ww_artikelkommentar_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikelkommentar
    ADD CONSTRAINT fk_ww_artikelkommentar_datenformat_c_nr_lp_datenformat_c_nr FOREIGN KEY (datenformat_c_nr) REFERENCES public.lp_datenformat(c_nr);
ALTER TABLE ONLY public.ww_artikelkommentarart
    ADD CONSTRAINT fk_ww_artikelkommentarart_branche_i_id_part_branche_i_id FOREIGN KEY (branche_i_id) REFERENCES public.part_branche(i_id);
ALTER TABLE ONLY public.ww_artikelkommentarartspr
    ADD CONSTRAINT fk_ww_artikelkommentarartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.ww_artikelkommentardruck
    ADD CONSTRAINT fk_ww_artikelkommentardruck_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikelkommentardruck
    ADD CONSTRAINT fk_ww_artikelkommentardruck_belegart_c_nr_lp_belegart_c_nr FOREIGN KEY (belegart_c_nr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.ww_artikelkommentarspr
    ADD CONSTRAINT fk_ww_artikelkommentarspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.ww_artikelkommentarspr
    ADD CONSTRAINT "fk_ww_artikelkommentarspr_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_artikellager
    ADD CONSTRAINT fk_ww_artikellager_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikellager
    ADD CONSTRAINT fk_ww_artikellager_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.ww_artikellager
    ADD CONSTRAINT fk_ww_artikellager_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_artikellagerplaetze
    ADD CONSTRAINT fk_ww_artikellagerplaetze_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikellagerplaetze
    ADD CONSTRAINT fk_ww_artikellagerplaetze_lagerplatz_i_id_ww_lagerplatz_i_id FOREIGN KEY (lagerplatz_i_id) REFERENCES public.ww_lagerplatz(i_id);
ALTER TABLE ONLY public.ww_artikellieferant
    ADD CONSTRAINT fk_ww_artikellieferant_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikellieferant
    ADD CONSTRAINT fk_ww_artikellieferant_einheit_c_nr_vpe_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr_vpe) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.ww_artikellieferant
    ADD CONSTRAINT fk_ww_artikellieferant_gebinde_i_id_ww_gebinde_i_id FOREIGN KEY (gebinde_i_id) REFERENCES public.ww_gebinde(i_id);
ALTER TABLE ONLY public.ww_artikellieferant
    ADD CONSTRAINT fk_ww_artikellieferant_lieferant_i_id_part_lieferant_i_id FOREIGN KEY (lieferant_i_id) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.ww_artikellieferant
    ADD CONSTRAINT fk_ww_artikellieferant_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_artikellieferantstaffel
    ADD CONSTRAINT fk_ww_artikellieferantstaffel_einheit_c_nr_vpe_lp_einheit_c_nr FOREIGN KEY (einheit_c_nr_vpe) REFERENCES public.lp_einheit(c_nr);
ALTER TABLE ONLY public.ww_artikellog
    ADD CONSTRAINT fk_ww_artikellog_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikellog
    ADD CONSTRAINT fk_ww_artikellog_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.ww_artikellog
    ADD CONSTRAINT fk_ww_artikellog_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_artikelreservierung
    ADD CONSTRAINT fk_ww_artikelreservierung_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikelreservierung
    ADD CONSTRAINT fk_ww_artikelreservierung_lp_belegart FOREIGN KEY (c_belegartnr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.ww_artikelshopgruppe
    ADD CONSTRAINT fk_ww_artikelshopgruppe_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikelshopgruppe
    ADD CONSTRAINT "fk_ww_artikelshopgruppe_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_artikelshopgruppe
    ADD CONSTRAINT "fk_ww_artikelshopgruppe_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_artikelshopgruppe
    ADD CONSTRAINT fk_ww_artikelshopgruppe_shopgruppe_i_id_ww_shopgruppe_i_id FOREIGN KEY (shopgruppe_i_id) REFERENCES public.ww_shopgruppe(i_id);
ALTER TABLE ONLY public.ww_artikelsnrchnr
    ADD CONSTRAINT fk_ww_artikelsnrchnr_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikelsperren
    ADD CONSTRAINT fk_ww_artikelsperren_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikelsperren
    ADD CONSTRAINT fk_ww_artikelsperren_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_artikelsperren
    ADD CONSTRAINT fk_ww_artikelsperren_sperren_i_id_ww_sperren_i_id FOREIGN KEY (sperren_i_id) REFERENCES public.ww_sperren(i_id);
ALTER TABLE ONLY public.ww_artikelspr
    ADD CONSTRAINT fk_ww_artikelspr_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikelspr
    ADD CONSTRAINT fk_ww_artikelspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.ww_artikelspr
    ADD CONSTRAINT fk_ww_artikelspr_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_artikeltrutops
    ADD CONSTRAINT fk_ww_artikeltrutops_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikeltrutopsmetadaten
    ADD CONSTRAINT fk_ww_artikeltrutopsmetadaten_artikeltrutops_i_id_ww_artikeltru FOREIGN KEY (artikeltrutops_i_id) REFERENCES public.ww_artikeltrutops(i_id);
ALTER TABLE ONLY public.ww_artkla
    ADD CONSTRAINT fk_ww_artkla_artkla_i_id_ww_artkla_i_id FOREIGN KEY (artkla_i_id) REFERENCES public.ww_artkla(i_id);
ALTER TABLE ONLY public.ww_artkla
    ADD CONSTRAINT fk_ww_artkla_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_artklaspr
    ADD CONSTRAINT fk_ww_artklaspr_artkla_i_id_ww_artkla_i_id FOREIGN KEY (artkla_i_id) REFERENCES public.ww_artkla(i_id);
ALTER TABLE ONLY public.ww_artklaspr
    ADD CONSTRAINT fk_ww_artklaspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.ww_automotive
    ADD CONSTRAINT fk_ww_automotive_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_dateiverweis
    ADD CONSTRAINT fk_ww_dateiverweis_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_einkaufsean
    ADD CONSTRAINT fk_ww_einkaufsean_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_ersatztypen
    ADD CONSTRAINT fk_ww_ersatztypen_artikel_i_id_ersatz_ww_artikel_i_id FOREIGN KEY (artikel_i_id_ersatz) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_ersatztypen
    ADD CONSTRAINT fk_ww_ersatztypen_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_fasession
    ADD CONSTRAINT fk_ww_fasession_personal_i_id_pers_personal_i_id FOREIGN KEY (personal_i_id) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_fasessioneintrag
    ADD CONSTRAINT fk_ww_fasessioneintrag_artikel_i_id_offenerag_ww_artikel_i_id FOREIGN KEY (artikel_i_id_offenerag) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_fasessioneintrag
    ADD CONSTRAINT fk_ww_fasessioneintrag_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_fasessioneintrag
    ADD CONSTRAINT fk_ww_fasessioneintrag_auftrag_i_id_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.ww_fasessioneintrag
    ADD CONSTRAINT fk_ww_fasessioneintrag_fasession_i_id_ww_fasession_i_id FOREIGN KEY (fasession_i_id) REFERENCES public.ww_fasession(i_id);
ALTER TABLE ONLY public.ww_fasessioneintrag
    ADD CONSTRAINT fk_ww_fasessioneintrag_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.ww_fasessioneintrag
    ADD CONSTRAINT fk_ww_fasessioneintrag_lieferschein_i_id_ls_lieferschein_i_id FOREIGN KEY (lieferschein_i_id) REFERENCES public.ls_lieferschein(i_id);
ALTER TABLE ONLY public.ww_fasessioneintrag
    ADD CONSTRAINT fk_ww_fasessioneintrag_los_i_id_fert_los_i_id FOREIGN KEY (los_i_id) REFERENCES public.fert_los(i_id);
ALTER TABLE ONLY public.ww_fasessioneintrag
    ADD CONSTRAINT "fk_ww_fasessioneintrag_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_gebinde
    ADD CONSTRAINT fk_ww_gebinde_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_geometrie
    ADD CONSTRAINT fk_ww_geometrie_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_geraetesnr
    ADD CONSTRAINT fk_ww_geraetesnr_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_handlagerbewegung
    ADD CONSTRAINT fk_ww_handlagerbewegung_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_handlagerbewegung
    ADD CONSTRAINT fk_ww_handlagerbewegung_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.ww_handlagerbewegung
    ADD CONSTRAINT "fk_ww_handlagerbewegung_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_hersteller
    ADD CONSTRAINT fk_ww_hersteller_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.ww_inventur
    ADD CONSTRAINT fk_ww_inventur_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.ww_inventur
    ADD CONSTRAINT fk_ww_inventur_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_inventur
    ADD CONSTRAINT "fk_ww_inventur_personal_i_id_abwertungdurchgefuehrt_pepe$_i_id" FOREIGN KEY (personal_i_id_abwertungdurchgefuehrt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_inventur
    ADD CONSTRAINT fk_ww_inventur_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_inventur
    ADD CONSTRAINT "fk_ww_inventur_personal_i_id_inventurdurchgefuehrt_pepe$_i_id" FOREIGN KEY (personal_i_id_inventurdurchgefuehrt) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_inventurliste
    ADD CONSTRAINT fk_ww_inventurliste_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_inventurliste
    ADD CONSTRAINT fk_ww_inventurliste_inventur_i_id_ww_inventur_i_id FOREIGN KEY (inventur_i_id) REFERENCES public.ww_inventur(i_id);
ALTER TABLE ONLY public.ww_inventurliste
    ADD CONSTRAINT fk_ww_inventurliste_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.ww_inventurliste
    ADD CONSTRAINT fk_ww_inventurliste_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_inventurprotokoll
    ADD CONSTRAINT fk_ww_inventurprotokoll_inventur_i_id_ww_inventur_i_id FOREIGN KEY (inventur_i_id) REFERENCES public.ww_inventur(i_id);
ALTER TABLE ONLY public.ww_inventurstand
    ADD CONSTRAINT fk_ww_inventurstand_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_inventurstand
    ADD CONSTRAINT fk_ww_inventurstand_inventur_i_id_ww_inventur_i_id FOREIGN KEY (inventur_i_id) REFERENCES public.ww_inventur(i_id);
ALTER TABLE ONLY public.ww_inventurstand
    ADD CONSTRAINT fk_ww_inventurstand_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.ww_katalog
    ADD CONSTRAINT fk_ww_katalog_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_lager
    ADD CONSTRAINT fk_ww_lager_lagerart_c_nr_ww_lagerart_c_nr FOREIGN KEY (lagerart_c_nr) REFERENCES public.ww_lagerart(c_nr);
ALTER TABLE ONLY public.ww_lager
    ADD CONSTRAINT fk_ww_lager_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_lager
    ADD CONSTRAINT fk_ww_lager_partner_i_id_part_partner_i_id FOREIGN KEY (partner_i_id) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.ww_lager
    ADD CONSTRAINT fk_ww_lager_partner_i_id_standort_part_partner_i_id FOREIGN KEY (partner_i_id_standort) REFERENCES public.part_partner(i_id);
ALTER TABLE ONLY public.ww_lagerartspr
    ADD CONSTRAINT fk_ww_lagerartspr_lagerart_c_nr_ww_lagerart_c_nr FOREIGN KEY (lagerart_c_nr) REFERENCES public.ww_lagerart(c_nr);
ALTER TABLE ONLY public.ww_lagerartspr
    ADD CONSTRAINT fk_ww_lagerartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.ww_lagerbewegung
    ADD CONSTRAINT fk_ww_lagerbewegung_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_lagerbewegung
    ADD CONSTRAINT fk_ww_lagerbewegung_gebinde_i_id_ww_gebinde_i_id FOREIGN KEY (gebinde_i_id) REFERENCES public.ww_gebinde(i_id);
ALTER TABLE ONLY public.ww_lagerbewegung
    ADD CONSTRAINT fk_ww_lagerbewegung_hersteller_i_id_ww_hersteller_i_id FOREIGN KEY (hersteller_i_id) REFERENCES public.ww_hersteller(i_id);
ALTER TABLE ONLY public.ww_lagerbewegung
    ADD CONSTRAINT fk_ww_lagerbewegung_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.ww_lagerbewegung
    ADD CONSTRAINT fk_ww_lagerbewegung_land_i_id_lp_land_i_id FOREIGN KEY (land_i_id) REFERENCES public.lp_land(i_id);
ALTER TABLE ONLY public.ww_lagerbewegung
    ADD CONSTRAINT fk_ww_lagerbewegung_lp_belegart FOREIGN KEY (c_belegartnr) REFERENCES public.lp_belegart(c_nr);
ALTER TABLE ONLY public.ww_lagerbewegung
    ADD CONSTRAINT fk_ww_lagerbewegung_ww_artikellager FOREIGN KEY (artikel_i_id, lager_i_id) REFERENCES public.ww_artikellager(artikel_i_id, lager_i_id);
ALTER TABLE ONLY public.ww_lagerplatz
    ADD CONSTRAINT fk_ww_lagerplatz_lager_i_id_ww_lager_i_id FOREIGN KEY (lager_i_id) REFERENCES public.ww_lager(i_id);
ALTER TABLE ONLY public.ww_lagerplatz
    ADD CONSTRAINT fk_ww_lagerplatz_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_laseroberflaeche
    ADD CONSTRAINT fk_ww_laseroberflaeche_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_materialpreis
    ADD CONSTRAINT fk_ww_materialpreis_material_i_id_ww_material_i_id FOREIGN KEY (material_i_id) REFERENCES public.ww_material(i_id);
ALTER TABLE ONLY public.ww_materialspr
    ADD CONSTRAINT fk_ww_materialspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.ww_materialspr
    ADD CONSTRAINT fk_ww_materialspr_material_i_id_ww_material_i_id FOREIGN KEY (material_i_id) REFERENCES public.ww_material(i_id);
ALTER TABLE ONLY public.ww_materialzuschlag
    ADD CONSTRAINT fk_ww_materialzuschlag_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_materialzuschlag
    ADD CONSTRAINT fk_ww_materialzuschlag_material_i_id_ww_material_i_id FOREIGN KEY (material_i_id) REFERENCES public.ww_material(i_id);
ALTER TABLE ONLY public.ww_medical
    ADD CONSTRAINT fk_ww_medical_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_montage
    ADD CONSTRAINT fk_ww_montage_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_paternoster
    ADD CONSTRAINT "fk_ww_paternoster_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_paternoster
    ADD CONSTRAINT "fk_ww_paternoster_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_paternostereigenschaft
    ADD CONSTRAINT fk_ww_paternostereigenschaft_paternoster_i_id_paternoster_i_id FOREIGN KEY (paternoster_i_id) REFERENCES public.ww_paternoster(i_id);
ALTER TABLE ONLY public.ww_rahmenbedarfe
    ADD CONSTRAINT fk_ww_rahmenbedarfe_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_rahmenbedarfe
    ADD CONSTRAINT fk_ww_rahmenbedarfe_auftrag_i_id_auft_auftrag_i_id FOREIGN KEY (auftrag_i_id) REFERENCES public.auft_auftrag(i_id);
ALTER TABLE ONLY public.ww_rahmenbedarfe
    ADD CONSTRAINT fk_ww_rahmenbedarfe_los_i_id_fert_los_i_id FOREIGN KEY (los_i_id) REFERENCES public.fert_los(i_id);
ALTER TABLE ONLY public.ww_reach
    ADD CONSTRAINT fk_ww_reach_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_rohs
    ADD CONSTRAINT fk_ww_rohs_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_shopgruppe
    ADD CONSTRAINT fk_ww_shopgruppe_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_shopgruppe
    ADD CONSTRAINT "fk_ww_shopgruppe_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_shopgruppe
    ADD CONSTRAINT "fk_ww_shopgruppe_personal_i_id_anlegen_pepe$_i_id" FOREIGN KEY (personal_i_id_anlegen) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_shopgruppe
    ADD CONSTRAINT fk_ww_shopgruppe_shopgruppe_i_id_ww_shopgruppe_i_id FOREIGN KEY (shopgruppe_i_id) REFERENCES public.ww_shopgruppe(i_id);
ALTER TABLE ONLY public.ww_shopgruppespr
    ADD CONSTRAINT fk_ww_shopgruppespr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.ww_shopgruppespr
    ADD CONSTRAINT fk_ww_shopgruppespr_shopgruppe_i_id_ww_shopgruppe_i_id FOREIGN KEY (shopgruppe_i_id) REFERENCES public.ww_shopgruppe(i_id);
ALTER TABLE ONLY public.ww_shopgruppewebshop
    ADD CONSTRAINT fk_ww_shopgruppewebshop_shopgruppe_i_id_ww_shopgruppe_i_id FOREIGN KEY (shopgruppe_i_id) REFERENCES public.ww_shopgruppe(i_id);
ALTER TABLE ONLY public.ww_shopgruppewebshop
    ADD CONSTRAINT fk_ww_shopgruppewebshop_webshop_i_id_ww_webshop_i_id FOREIGN KEY (webshop_i_id) REFERENCES public.ww_webshop(i_id);
ALTER TABLE ONLY public.ww_sollverkauf
    ADD CONSTRAINT fk_ww_sollverkauf_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_sperren
    ADD CONSTRAINT fk_ww_sperren_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_trumphtopslog
    ADD CONSTRAINT fk_ww_trumphtopslog_artikel_i_id_material_ww_artikel_i_id FOREIGN KEY (artikel_i_id_material) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_trumphtopslog
    ADD CONSTRAINT fk_ww_trumphtopslog_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_verpackung
    ADD CONSTRAINT fk_ww_verpackung_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_verpackungsmittel
    ADD CONSTRAINT fk_ww_verpackungsmittel_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_verpackungsmittelspr
    ADD CONSTRAINT fk_ww_verpackungsmittelspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.ww_verpackungsmittelspr
    ADD CONSTRAINT fk_ww_verpackungsmittelspr_verpackungsmittel_i_id_ww_verpackung FOREIGN KEY (verpackungsmittel_i_id) REFERENCES public.ww_verpackungsmittel(i_id);
ALTER TABLE ONLY public.ww_verschleissteilwerkzeug
    ADD CONSTRAINT fk_ww_verschleissteilwerkzeug_verschleissteil_i_id_ww_verschlei FOREIGN KEY (verschleissteil_i_id) REFERENCES public.ww_verschleissteil(i_id);
ALTER TABLE ONLY public.ww_verschleissteilwerkzeug
    ADD CONSTRAINT fk_ww_verschleissteilwerkzeug_werkzeug_i_id_ww_werkzeug_i_id FOREIGN KEY (werkzeug_i_id) REFERENCES public.ww_werkzeug(i_id);
ALTER TABLE ONLY public.ww_vkpfmengenstaffel
    ADD CONSTRAINT "fk_ww_vkms$_vkpfartikelpreisliste_i_id_ww_vkpl$_i_id" FOREIGN KEY (vkpfartikelpreisliste_i_id) REFERENCES public.ww_vkpfartikelpreisliste(i_id);
ALTER TABLE ONLY public.ww_vkpfartikelpreis
    ADD CONSTRAINT fk_ww_vkpfartikelpreis_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_vkpfartikelpreis
    ADD CONSTRAINT fk_ww_vkpfartikelpreis_personal_i_id_aendern_pers_personal_i_id FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_vkpfartikelpreis
    ADD CONSTRAINT "fk_ww_vkpfartikelpreis_vkpfartikelpreisliste_i_id_wwvp$_i_id" FOREIGN KEY (vkpfartikelpreisliste_i_id) REFERENCES public.ww_vkpfartikelpreisliste(i_id);
ALTER TABLE ONLY public.ww_vkpfartikelpreisliste
    ADD CONSTRAINT fk_ww_vkpfartikelpreisliste_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_vkpfartikelpreisliste
    ADD CONSTRAINT fk_ww_vkpfartikelpreisliste_webshop_i_id_ww_webshop_i_id FOREIGN KEY (webshop_i_id) REFERENCES public.ww_webshop(i_id);
ALTER TABLE ONLY public.ww_vkpfmengenstaffel
    ADD CONSTRAINT fk_ww_vkpfmengenstaffel_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_vorschlagstext
    ADD CONSTRAINT fk_ww_vorschlagstext_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.ww_vorzug
    ADD CONSTRAINT fk_ww_vorzug_mandant_c_nr_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_webshop
    ADD CONSTRAINT fk_ww_webshop_webshopart_c_nr_ww_webshopart_c_nr FOREIGN KEY (webshopart_c_nr) REFERENCES public.ww_webshopart(c_nr);
ALTER TABLE ONLY public.ww_webshopartikel
    ADD CONSTRAINT fk_ww_webshopartikel_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_webshopartikel
    ADD CONSTRAINT fk_ww_webshopartikel_webshop_id_ww_webshop_i_id FOREIGN KEY (webshop_i_id) REFERENCES public.ww_webshop(i_id);
ALTER TABLE ONLY public.ww_webshopartikelpreisliste
    ADD CONSTRAINT fk_ww_webshopartikelpreisliste_vkpfartikelpreisliste_i_id_ww_vk FOREIGN KEY (vkpfartikelpreisliste_i_id) REFERENCES public.ww_vkpfartikelpreisliste(i_id);
ALTER TABLE ONLY public.ww_webshopartikelpreisliste
    ADD CONSTRAINT fk_ww_webshopartikelpreisliste_webshop_id_ww_webshop_i_id FOREIGN KEY (webshop_i_id) REFERENCES public.ww_webshop(i_id);
ALTER TABLE ONLY public.ww_webshopartspr
    ADD CONSTRAINT fk_ww_webshopartspr_locale_c_nr_lp_locale_c_nr FOREIGN KEY (locale_c_nr) REFERENCES public.lp_locale(c_nr);
ALTER TABLE ONLY public.ww_webshopartspr
    ADD CONSTRAINT fk_ww_webshopartspr_webshopart_c_nr_ww_webshopart_c_nr FOREIGN KEY (webshopart_c_nr) REFERENCES public.ww_webshopart(c_nr);
ALTER TABLE ONLY public.ww_webshopkunde
    ADD CONSTRAINT fk_ww_webshopkunde_kunde_i_id_part_kunde_i_id FOREIGN KEY (kunde_i_id) REFERENCES public.part_kunde(i_id);
ALTER TABLE ONLY public.ww_webshopkunde
    ADD CONSTRAINT fk_ww_webshopkunde_webshop_id_ww_webshop_i_id FOREIGN KEY (webshop_i_id) REFERENCES public.ww_webshop(i_id);
ALTER TABLE ONLY public.ww_webshopmwstsatzbez
    ADD CONSTRAINT fk_ww_webshopmwstsatzbez_mwstsatzbez_i_id_lp_mwstsatzbez_i_id FOREIGN KEY (mwstsatzbez_i_id) REFERENCES public.lp_mwstsatzbez(i_id);
ALTER TABLE ONLY public.ww_webshopmwstsatzbez
    ADD CONSTRAINT fk_ww_webshopmwstsatzbez_webshop_id_ww_webshop_i_id FOREIGN KEY (webshop_i_id) REFERENCES public.ww_webshop(i_id);
ALTER TABLE ONLY public.ww_webshopshopgruppe
    ADD CONSTRAINT fk_ww_webshopshopgruppe_shopgruppe_i_id_ww_shopgruppe_i_id FOREIGN KEY (shopgruppe_i_id) REFERENCES public.ww_shopgruppe(i_id);
ALTER TABLE ONLY public.ww_webshopshopgruppe
    ADD CONSTRAINT fk_ww_webshopshopgruppe_webshop_id_ww_webshop_i_id FOREIGN KEY (webshop_i_id) REFERENCES public.ww_webshop(i_id);
ALTER TABLE ONLY public.ww_werkzeug
    ADD CONSTRAINT fk_ww_werkzeug_lagerplatz_i_id_ww_lagerplatz_i_id FOREIGN KEY (lagerplatz_i_id) REFERENCES public.ww_lagerplatz(i_id);
ALTER TABLE ONLY public.ww_werkzeug
    ADD CONSTRAINT fk_ww_werkzeug_lieferant_i_id_part_lieferant_i_id FOREIGN KEY (lieferant_i_id) REFERENCES public.part_lieferant(i_id);
ALTER TABLE ONLY public.ww_werkzeug
    ADD CONSTRAINT fk_ww_werkzeug_mandant_c_nr_standort_lp_mandant_c_nr FOREIGN KEY (mandant_c_nr_standort) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_artikellieferant
    ADD CONSTRAINT "fk_ww_wwal$_zertifikatart_i_id_anf_zertifikatart_i_id" FOREIGN KEY (zertifikatart_i_id) REFERENCES public.anf_zertifikatart(i_id);
ALTER TABLE ONLY public.ww_zugehoerige
    ADD CONSTRAINT fk_ww_zugehoerige_artikel_i_id_ww_artikel_i_id FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_zugehoerige
    ADD CONSTRAINT fk_ww_zugehoerige_artikel_i_id_zugehoerig_ww_artikel_i_id FOREIGN KEY (artikel_i_id_zugehoerig) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_artikelkommentar
    ADD CONSTRAINT "fk_wwak$_artikelkommentarart_i_id_wwaa$_i_id" FOREIGN KEY (artikelkommentarart_i_id) REFERENCES public.ww_artikelkommentarart(i_id);
ALTER TABLE ONLY public.ww_artikelkommentardruck
    ADD CONSTRAINT "fk_wwak$druck_artikelkommentar_i_id_wwak$_i_id" FOREIGN KEY (artikelkommentar_i_id) REFERENCES public.ww_artikelkommentar(i_id);
ALTER TABLE ONLY public.ww_artikelkommentarspr
    ADD CONSTRAINT "fk_wwak$spr_artikelkommentar_i_id_wwak$_i_id" FOREIGN KEY (artikelkommentar_i_id) REFERENCES public.ww_artikelkommentar(i_id);
ALTER TABLE ONLY public.ww_artikellieferant
    ADD CONSTRAINT "fk_wwal$_anfragepositionlieferdaten_i_id" FOREIGN KEY (anfragepositionlieferdaten_i_id) REFERENCES public.anf_anfragepositionlieferdaten(i_id);
ALTER TABLE ONLY public.ww_artikellieferantstaffel
    ADD CONSTRAINT "fk_wwas$_anfragepositionlieferdaten_i_id" FOREIGN KEY (anfragepositionlieferdaten_i_id) REFERENCES public.anf_anfragepositionlieferdaten(i_id);
ALTER TABLE ONLY public.ww_artikelkommentarartspr
    ADD CONSTRAINT "fk_wwas$_artikelkommentarart_i_id_wwaa$_i_id" FOREIGN KEY (artikelkommentarart_i_id) REFERENCES public.ww_artikelkommentarart(i_id);
ALTER TABLE ONLY public.ww_inventurprotokoll
    ADD CONSTRAINT "fk_wwip$_inventurliste_i_id_ww_inventurliste_i_id" FOREIGN KEY (inventurliste_i_id) REFERENCES public.ww_inventurliste(i_id);
ALTER TABLE ONLY public.ww_inventurprotokoll
    ADD CONSTRAINT "fk_wwip$_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_lagerbewegung
    ADD CONSTRAINT "fk_wwlb$_personal_i_id_einstandspreisgeaendert_pepe$_i_id" FOREIGN KEY (personal_i_id_einstandspreisgeaendert) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_lagerbewegung
    ADD CONSTRAINT "fk_wwlb$_personal_i_id_mengegeaendert_pepe$_i_id" FOREIGN KEY (personal_i_id_mengegeaendert) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_lagerbewegung
    ADD CONSTRAINT "fk_wwlb$_personal_i_id_verkaufspreisgeaendert_pepe$_i_id" FOREIGN KEY (personal_i_id_verkaufspreisgeaendert) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_artikellieferantstaffel
    ADD CONSTRAINT "fk_wwls$_artikellieferant_i_id_ww_artikellieferant_i_id" FOREIGN KEY (artikellieferant_i_id) REFERENCES public.ww_artikellieferant(i_id);
ALTER TABLE ONLY public.ww_artikellieferantstaffel
    ADD CONSTRAINT "fk_wwls$_personal_i_id_aendern_pers_personal_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_vkpfartikelverkaufspreisbasis
    ADD CONSTRAINT "fk_wwvb$_artikel_i_id_ww_artikel_i_id" FOREIGN KEY (artikel_i_id) REFERENCES public.ww_artikel(i_id);
ALTER TABLE ONLY public.ww_vkpfartikelverkaufspreisbasis
    ADD CONSTRAINT "fk_wwvb$_mandant_c_nr_lpmd$_c_nr" FOREIGN KEY (mandant_c_nr) REFERENCES public.lp_mandant(c_nr);
ALTER TABLE ONLY public.ww_vkpfartikelverkaufspreisbasis
    ADD CONSTRAINT "fk_wwvb$_personal_i_id_aendern_pers_personal_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_vkpfmengenstaffel
    ADD CONSTRAINT "fk_wwvm$_personal_i_id_aendern_pepe$_i_id" FOREIGN KEY (personal_i_id_aendern) REFERENCES public.pers_personal(i_id);
ALTER TABLE ONLY public.ww_vkpfartikelpreisliste
    ADD CONSTRAINT "fk_wwvp$_waehrung_c_nr_lp_waehrung_c_nr" FOREIGN KEY (waehrung_c_nr) REFERENCES public.lp_waehrung(c_nr);
ALTER TABLE ONLY public.pers_fahrzeugkosten
    ADD CONSTRAINT fkpers_fahrzeugkosten_fahrzeug_i_id_pers_fahrzeug_i_id FOREIGN KEY (fahrzeug_i_id) REFERENCES public.pers_fahrzeug(i_id);
