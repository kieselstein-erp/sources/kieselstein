SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;
CREATE SCHEMA dbo;
ALTER SCHEMA dbo OWNER TO postgres;
CREATE FUNCTION public.smallint_to_text(smallint) RETURNS text
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
 SELECT textin(int2out($1));
$_$;
ALTER FUNCTION public.smallint_to_text(smallint) OWNER TO postgres;
CREATE CAST (smallint AS text) WITH FUNCTION public.smallint_to_text(smallint) AS IMPLICIT;
CREATE FUNCTION public.int_to_text(integer) RETURNS text
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
SELECT textin(int4out($1));
$_$;
ALTER FUNCTION public.int_to_text(integer) OWNER TO postgres;
CREATE CAST (integer AS text) WITH FUNCTION public.int_to_text(integer) AS IMPLICIT;
CREATE FUNCTION dbo.add2date(c_datepart text, i_tage integer, t_datum timestamp without time zone) RETURNS timestamp without time zone
    LANGUAGE plpgsql
    AS $_$
BEGIN 
	if $1  = 'd' or $1  = 'dd' THEN
		RETURN  $3 + cast((cast($2 as varchar(6)) || ' day') as interval);
		else if $1 = 'y' or $1 = 'yy' THEN
			RETURN $3 + cast((cast($2 as varchar(6)) || ' year') as interval);
			else if $1 = 'm' or $1 = 'mm' THEN
				RETURN $3 + cast((cast($2 as varchar(6)) || ' month') as interval);
				else if $1 = 'wk' or $1 = 'ww' THEN
					RETURN $3 + cast((cast($2 as varchar(6)) || ' week') as interval);
					else if $1 = 'hh' THEN
						RETURN $3 + cast((cast($2 as varchar(6)) || ' hour') as interval);
						else if $1 = 'mi' or $1 = 'n' THEN
							RETURN $3 + cast((cast($2 as varchar(6)) || ' minute') as interval);
							else if $1 = 's' or $1 = 'ss' THEN
								RETURN $3 + cast((cast($2 as varchar(6)) || ' second') as interval);
							END IF;
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;
END;
$_$;
ALTER FUNCTION dbo.add2date(c_datepart text, i_tage integer, t_datum timestamp without time zone) OWNER TO postgres;
CREATE FUNCTION public.create_language_plpgsql() RETURNS boolean
    LANGUAGE sql
    AS $$
 CREATE LANGUAGE plpgsql;
SELECT TRUE;
$$;
ALTER FUNCTION public.create_language_plpgsql() OWNER TO postgres;
CREATE FUNCTION public.loguser() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN 
INSERT INTO lp_usercount VALUES (now(), getusercount(NEW.c_benutzername)); 
RETURN NEW; 
END 
$$;
ALTER FUNCTION public.loguser() OWNER TO postgres;
CREATE FUNCTION public.loguserinsert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN 
INSERT INTO lp_usercount VALUES (now(), getusercount(NEW.c_benutzername)+1);
RETURN NEW; 
END$$;
ALTER FUNCTION public.loguserinsert() OWNER TO postgres;
SET default_tablespace = '';
SET default_table_access_method = heap;
CREATE TABLE public.anf_anfrage (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    anfragestatus_c_nr character(15) NOT NULL,
    anfrageart_c_nr character(15) NOT NULL,
    belegart_c_nr character(15) NOT NULL,
    t_belegdatum timestamp without time zone NOT NULL,
    lieferant_i_id_anfrageadresse integer,
    ansprechpartner_i_id_lieferant integer,
    c_bez character varying(80),
    c_angebotnummer character varying(40),
    waehrung_c_nr_anfragewaehrung character(3) NOT NULL,
    f_wechselkursmandantwaehrungzuanfragewaehrung double precision NOT NULL,
    t_anliefertermin timestamp without time zone,
    kostenstelle_i_id integer NOT NULL,
    f_allgemeinerrabattsatz double precision NOT NULL,
    lieferart_i_id integer,
    zahlungsziel_i_id integer,
    spediteur_i_id integer,
    n_gesamtanfragewertinanfragewaehrung numeric(17,6),
    n_transportkosteninanfragewaehrung numeric(17,6) NOT NULL,
    anfragetext_i_id_kopftext integer,
    x_kopftextuebersteuert text,
    anfragetext_i_id_fusstext integer,
    x_fusstextuebersteuert text,
    t_gedruckt timestamp without time zone,
    personal_i_id_storniert integer,
    t_storniert timestamp without time zone,
    personal_i_id_manuellerledigt integer,
    t_manuellerledigt timestamp without time zone,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    anfrage_i_id_liefergruppenanfrage integer,
    lfliefergruppe_i_id integer,
    t_versandzeitpunkt timestamp without time zone,
    c_versandtype character varying(15),
    t_angebotdatum timestamp without time zone,
    t_angebotgueltigbis timestamp without time zone,
    c_lieferartort character varying(40),
    projekt_i_id integer,
    anfrageerledigungsgrund_i_id integer,
    partner_i_id_lieferadresse integer,
    ansprechpartner_i_id_lieferadresse integer,
    t_abgabetermin timestamp without time zone,
    n_zollkosteninanfragewaehrung numeric(17,6) NOT NULL,
    n_bankspeseninanfragewaehrung numeric(17,6) NOT NULL,
    n_sonstigespeseninanfragewaehrung numeric(17,6) NOT NULL,
    personal_i_id_anfrager integer,
    t_preisgueltigab timestamp without time zone
);
ALTER TABLE public.anf_anfrage OWNER TO postgres;
CREATE TABLE public.anf_anfrageart (
    c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.anf_anfrageart OWNER TO postgres;
CREATE TABLE public.anf_anfrageartspr (
    locale_c_nr character(10) NOT NULL,
    anfrageart_c_nr character(15) NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.anf_anfrageartspr OWNER TO postgres;
CREATE TABLE public.anf_anfrageerledigungsgrund (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.anf_anfrageerledigungsgrund OWNER TO postgres;
CREATE TABLE public.anf_anfrageposition (
    i_id integer NOT NULL,
    anfrage_i_id integer NOT NULL,
    i_sort integer NOT NULL,
    anfragepositionart_c_nr character(15) NOT NULL,
    artikel_i_id integer,
    c_bez character varying(80),
    b_artikelbezeichnunguebersteuert smallint NOT NULL,
    x_textinhalt text,
    mediastandard_i_id integer,
    n_menge numeric(17,6),
    einheit_c_nr character(15),
    n_richtpreis numeric(17,6),
    c_zbez character varying(80),
    anfrageposition_i_id_zugehoerig integer,
    lossollmaterial_i_id integer
);
ALTER TABLE public.anf_anfrageposition OWNER TO postgres;
CREATE TABLE public.anf_anfragepositionart (
    positionsart_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL,
    b_versteckt smallint NOT NULL
);
ALTER TABLE public.anf_anfragepositionart OWNER TO postgres;
CREATE TABLE public.anf_anfragepositionlieferdaten (
    i_id integer NOT NULL,
    anfrageposition_i_id integer NOT NULL,
    n_anliefermenge numeric(17,6) NOT NULL,
    n_nettogesamtpreis numeric(17,6) NOT NULL,
    n_nettogesamtpreisminusrabatt numeric(17,6),
    b_erfasst smallint NOT NULL,
    c_bezbeilieferant character varying(80),
    c_artikelnrlieferant character varying(40),
    n_verpackungseinheit numeric(17,6),
    n_standardmenge numeric(17,6),
    n_mindestbestellmenge numeric(17,6),
    zertifikatart_i_id integer,
    i_anlieferzeit integer,
    t_preisgueltigab timestamp without time zone
);
ALTER TABLE public.anf_anfragepositionlieferdaten OWNER TO postgres;
CREATE TABLE public.anf_anfragestatus (
    status_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.anf_anfragestatus OWNER TO postgres;
CREATE TABLE public.anf_anfragetext (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    mediaart_c_nr character varying(40) NOT NULL,
    x_textinhalt text NOT NULL
);
ALTER TABLE public.anf_anfragetext OWNER TO postgres;
CREATE TABLE public.lp_land (
    i_id integer NOT NULL,
    c_lkz character varying(50) NOT NULL,
    c_name character varying(40) NOT NULL,
    c_telvorwahl character varying(15),
    waehrung_c_nr character(3),
    t_eumitglied_von timestamp without time zone,
    n_uidnummerpruefenabbetrag numeric(17,6),
    i_laengeuidnummer integer NOT NULL,
    c_ustcode character varying(10),
    b_sepa smallint NOT NULL,
    f_gmtversatz double precision,
    n_muenzrundung numeric(15,2),
    land_i_id_gemeinsamespostland integer,
    b_plznachort smallint NOT NULL,
    b_postfachmitstrasse smallint NOT NULL,
    b_mwstmuenzrundung smallint NOT NULL,
    t_eumitglied_bis timestamp without time zone,
    b_praeferenzbeguenstigt smallint NOT NULL
);
ALTER TABLE public.lp_land OWNER TO postgres;
CREATE TABLE public.lp_landplzort (
    i_id integer NOT NULL,
    c_plz character varying(15) NOT NULL,
    land_i_id integer NOT NULL,
    ort_i_id integer NOT NULL
);
ALTER TABLE public.lp_landplzort OWNER TO postgres;
CREATE TABLE public.lp_ort (
    i_id integer NOT NULL,
    c_name character varying(50) NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.lp_ort OWNER TO postgres;
CREATE TABLE public.part_lieferant (
    i_id integer NOT NULL,
    partner_i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    waehrung_c_nr character(3) NOT NULL,
    b_moeglicherlieferant smallint NOT NULL,
    b_beurteilen smallint NOT NULL,
    spediteur_i_id integer NOT NULL,
    lieferart_i_id integer NOT NULL,
    zahlungsziel_i_id integer NOT NULL,
    mwstsatz_i_id integer,
    n_mindestbestellwert numeric(17,6),
    n_kredit numeric(17,6),
    n_jahrbonus numeric(17,6),
    n_abumsatz numeric(17,6),
    n_mindermengenzuschlag numeric(17,6),
    n_rabatt double precision,
    konto_i_id_kreditorenkonto integer,
    konto_i_id_warenkonto integer,
    c_kundennr character varying(40),
    c_hinweisintern character varying(80),
    c_hinweisextern character varying(80),
    x_kommentar text,
    i_beurteilung integer,
    partner_i_id_rechnungsadresse integer,
    kostenstelle_i_id integer,
    t_bestellsperream timestamp without time zone,
    n_transportkostenprolieferung numeric(17,6),
    n_kupferzahl numeric(15,6),
    c_freigabe character varying(40),
    t_freigabe timestamp without time zone,
    personal_i_id_freigabe integer,
    t_personal_freigabe timestamp without time zone,
    b_reversecharge smallint NOT NULL,
    lager_i_id_zubuchungslager integer NOT NULL,
    b_zollimportpapier smallint NOT NULL,
    b_igerwerb smallint NOT NULL,
    b_versteckterkunde smallint NOT NULL,
    reversechargeart_i_id integer NOT NULL,
    c_fremdsystemnr character varying(40),
    b_zuschlag_inklusive smallint NOT NULL,
    i_liefertag integer,
    partner_i_id_lieferadresse integer
);
ALTER TABLE public.part_lieferant OWNER TO postgres;
CREATE TABLE public.part_partner (
    i_id integer NOT NULL,
    locale_c_nr_kommunikation character(10) NOT NULL,
    partnerart_c_nr character(15) NOT NULL,
    c_kbez character varying(40) NOT NULL,
    b_versteckt smallint NOT NULL,
    c_name1nachnamefirmazeile1 character varying(40) NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer,
    c_name2vornamefirmazeile2 character varying(40),
    c_name3vorname2abteilung character varying(40),
    c_strasse character varying(80),
    anrede_c_nr character(15),
    landplzort_i_id integer,
    landplzort_i_id_postfach integer,
    c_postfach character varying(15),
    branche_i_id integer,
    partnerklasse_i_id integer,
    partner_i_id_vater integer,
    c_uid character varying(20),
    x_bemerkung text,
    t_geburtsdatumansprechpartner timestamp without time zone,
    rechtsform_i_id integer,
    partner_i_id_eigentuemer integer,
    c_firmenbuchnr character varying(50),
    c_titel character varying(80),
    c_gerichtsstand character varying(40),
    o_bild bytea,
    land_i_id_abweichendesustland integer,
    lager_i_id_ziellager integer,
    c_iln character varying(15),
    c_filialnummer character varying(15),
    c_adressart character(1),
    f_gmtversatz double precision,
    c_ntitel character varying(80),
    c_eori character varying(25),
    c_telefon character varying(80),
    c_fax character varying(80),
    c_direktfax character varying(80),
    c_handy character varying(80),
    c_email character varying(80),
    c_homepage character varying(80),
    versandweg_i_id integer,
    newslettergrund_i_id integer,
    c_exchangeid character varying(200),
    t_zuletzt_exportiert timestamp without time zone
);
ALTER TABLE public.part_partner OWNER TO postgres;
CREATE TABLE public.ww_artikel (
    i_id integer NOT NULL,
    c_nr character varying(25) NOT NULL,
    hersteller_i_id integer,
    c_artikelbezhersteller character varying(80),
    c_artikelnrhersteller character varying(300),
    artgru_i_id integer,
    artkla_i_id integer,
    artikelart_c_nr character(15) NOT NULL,
    einheit_c_nr character(15) NOT NULL,
    b_seriennrtragend smallint NOT NULL,
    b_chargennrtragend smallint NOT NULL,
    b_lagerbewirtschaftet smallint NOT NULL,
    b_lagerbewertet smallint NOT NULL,
    c_referenznr character varying(30),
    f_lagermindest double precision,
    f_lagersoll double precision,
    f_verpackungsmenge double precision,
    f_verschnittfaktor double precision,
    f_verschnittbasis double precision,
    f_jahresmenge double precision,
    mwstsatz_i_id integer,
    material_i_id integer,
    f_gewichtkg double precision,
    f_materialgewicht double precision,
    b_antistatic smallint NOT NULL,
    artikel_i_id_zugehoerig integer,
    f_vertreterprovisionmax double precision,
    f_minutenfaktor1 double precision,
    f_minutenfaktor2 double precision,
    f_mindestdeckungsbeitrag double precision NOT NULL,
    c_verkaufseannr character varying(15),
    c_warenverkehrsnummer character varying(10),
    b_rabattierbar smallint NOT NULL,
    i_garantiezeit integer,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    farbcode_i_id integer,
    einheit_c_nr_bestellung character(15),
    n_umrechnugsfaktor numeric(15,6),
    b_versteckt smallint NOT NULL,
    c_verpackungseannr character varying(15),
    artikel_i_id_ersatz integer,
    land_i_id_ursprungsland integer,
    f_fertigungssatzgroesse double precision,
    i_wartungsintervall integer,
    i_sofortverbrauch integer,
    b_dokumentenpflicht smallint NOT NULL,
    c_revision character varying(40),
    c_index character varying(15),
    f_stromverbrauchtyp double precision,
    f_stromverbrauchmax double precision,
    b_verleih smallint NOT NULL,
    f_detailprozentmindeststand double precision,
    lfliefergruppe_i_id integer,
    b_reinemannzeit smallint NOT NULL,
    b_nurzurinfo smallint NOT NULL,
    shopgruppe_i_id integer,
    b_bestellmengeneinheit_invers smallint NOT NULL,
    b_werbeabgabepflichtig smallint NOT NULL,
    personal_i_id_letztewartung integer,
    t_letztewartung timestamp without time zone,
    b_kalkulatorisch smallint NOT NULL,
    n_aufschlag_betrag numeric(17,6),
    f_aufschlag_prozent double precision,
    c_ul character varying(40),
    reach_i_id integer,
    rohs_i_id integer,
    automotive_i_id integer,
    medical_i_id integer,
    f_ueberproduktion double precision,
    vorzug_i_id integer,
    c_eccn character varying(15),
    f_fertigungs_vpe double precision,
    b_rahmenartikel smallint NOT NULL,
    n_verschnittmenge numeric(17,6),
    verpackungsmittel_i_id integer,
    n_verpackungsmittelmenge numeric(15,4),
    n_mindestverkaufsmenge numeric(15,4),
    b_azinabnachkalk smallint NOT NULL,
    f_multiplikator_zugehoerigerartikel double precision,
    b_kommissionieren smallint NOT NULL,
    b_keine_lagerzubuchung smallint NOT NULL,
    i_laengemin_snrchnr integer,
    i_laengemax_snrchnr integer,
    b_wep_info_an_anforderer smallint NOT NULL,
    b_vkpreispflichtig smallint NOT NULL,
    b_summe_in_bestellung smallint NOT NULL,
    i_externer_arbeitsgang integer NOT NULL,
    b_bevorzugt smallint NOT NULL,
    b_multiplikator_invers smallint NOT NULL,
    b_multiplikator_aufrunden smallint NOT NULL,
    i_passive_reisezeit integer NOT NULL,
    t_freigabe timestamp without time zone,
    personal_i_id_freigabe integer,
    c_freigabe_zurueckgenommen character varying(300),
    b_meldepflichtig smallint NOT NULL,
    b_bewilligungspflichtig smallint NOT NULL,
    waffenkaliber_i_id integer,
    waffentyp_i_id integer,
    waffentyp_fein_i_id integer,
    waffenkategorie_i_id integer,
    waffenzusatz_i_id integer,
    waffenausfuehrung_i_id integer,
    n_preis_zueghoerigerartikel numeric(17,6),
    f_maxfertigungssatzgroesse double precision,
    laseroberflaeche_i_id integer
);
ALTER TABLE public.ww_artikel OWNER TO postgres;
CREATE TABLE public.ww_artikelspr (
    artikel_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_kbez character varying(30),
    c_bez character varying(80),
    c_zbez character varying(80),
    c_zbez2 character varying(80),
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    c_siwert character(60)
);
ALTER TABLE public.ww_artikelspr OWNER TO postgres;
CREATE VIEW public.anf_textsuche AS
 SELECT DISTINCT 'A '::text AS c_typ,
    g.i_id,
    g.c_nr,
    (((COALESCE(a.c_bez, ''::character varying))::text || (COALESCE(a.c_zbez, ''::character varying))::text) || (COALESCE(a.c_zbez2, ''::character varying))::text) AS c_suche
   FROM ((public.anf_anfrage g
     JOIN public.anf_anfrageposition p ON ((g.i_id = p.anfrage_i_id)))
     JOIN public.ww_artikelspr a ON ((a.artikel_i_id = p.artikel_i_id)))
UNION
 SELECT DISTINCT 'P '::text AS c_typ,
    g.i_id,
    g.c_nr,
    ((COALESCE(p.c_bez, ''::character varying))::text || (COALESCE(p.c_zbez, ''::character varying))::text) AS c_suche
   FROM (public.anf_anfrage g
     JOIN public.anf_anfrageposition p ON ((g.i_id = p.anfrage_i_id)))
UNION
 SELECT DISTINCT 'PT '::text AS c_typ,
    g.i_id,
    g.c_nr,
    (p.x_textinhalt)::character varying(4000) AS c_suche
   FROM (public.anf_anfrage g
     JOIN public.anf_anfrageposition p ON ((g.i_id = p.anfrage_i_id)))
UNION
 SELECT DISTINCT 'ANF'::text AS c_typ,
    anf.i_id,
    anf.c_nr,
    anf.c_bez AS c_suche
   FROM public.anf_anfrage anf
UNION
 SELECT DISTINCT 'ID'::text AS c_typ,
    g.i_id,
    g.c_nr,
    ((a.c_nr)::text || (COALESCE(a.c_referenznr, ''::character varying))::text) AS c_suche
   FROM ((public.anf_anfrage g
     JOIN public.anf_anfrageposition p ON ((g.i_id = p.anfrage_i_id)))
     JOIN public.ww_artikel a ON ((a.i_id = p.artikel_i_id)))
UNION
 SELECT DISTINCT 'LF'::text AS c_typ,
    g.i_id,
    g.c_nr,
    ((COALESCE(p.c_name1nachnamefirmazeile1, ''::character varying))::text || (COALESCE(p.c_name2vornamefirmazeile2, ''::character varying))::text) AS c_suche
   FROM ((public.anf_anfrage g
     JOIN public.part_lieferant l ON ((l.i_id = g.lieferant_i_id_anfrageadresse)))
     JOIN public.part_partner p ON ((p.i_id = l.partner_i_id)))
UNION
 SELECT DISTINCT 'LPO'::text AS c_typ,
    g.i_id,
    g.c_nr,
    (((((((l.c_lkz)::text || '-'::text) || (lpo.c_plz)::text) || ' '::text) || (o.c_name)::text) || ' '::text) || (p.c_strasse)::text) AS c_suche
   FROM (((((public.lp_landplzort lpo
     JOIN public.part_partner p ON ((lpo.i_id = p.landplzort_i_id)))
     JOIN public.lp_land l ON ((lpo.land_i_id = l.i_id)))
     JOIN public.lp_ort o ON ((lpo.ort_i_id = o.i_id)))
     JOIN public.part_lieferant k ON ((p.i_id = k.partner_i_id)))
     JOIN public.anf_anfrage g ON ((k.i_id = g.lieferant_i_id_anfrageadresse)));
ALTER TABLE public.anf_textsuche OWNER TO postgres;
CREATE TABLE public.anf_zertifikatart (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.anf_zertifikatart OWNER TO postgres;
CREATE TABLE public.angb_akquisestatus (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(40) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.angb_akquisestatus OWNER TO postgres;
CREATE TABLE public.angb_angebot (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    angebotart_c_nr character(15) NOT NULL,
    angebotstatus_c_nr character(15) NOT NULL,
    belegart_c_nr character(15) NOT NULL,
    t_belegdatum timestamp without time zone NOT NULL,
    t_anfragedatum timestamp without time zone NOT NULL,
    t_angebotsgueltigkeitbis timestamp without time zone NOT NULL,
    kunde_i_id_angebotsadresse integer NOT NULL,
    ansprechpartner_i_id_kunde integer,
    personal_i_id_vertreter integer,
    c_bez character varying(80),
    waehrung_c_nr_angebotswaehrung character(3) NOT NULL,
    f_wechselkursmandantwaehrungzuangebotswaehrung double precision NOT NULL,
    i_lieferzeitinstunden integer,
    angeboteinheit_c_nr character(15) NOT NULL,
    kostenstelle_i_id integer NOT NULL,
    angeboterledigungsgrund_c_nr character varying(40),
    t_nachfasstermin timestamp without time zone NOT NULL,
    t_realisierungstermin timestamp without time zone,
    f_auftragswahrscheinlichkeit double precision NOT NULL,
    x_ablageort text,
    f_versteckteraufschlag double precision NOT NULL,
    f_allgemeinerrabattsatz double precision NOT NULL,
    f_projektierungsrabattsatz double precision NOT NULL,
    lieferart_i_id integer NOT NULL,
    zahlungsziel_i_id integer NOT NULL,
    spediteur_i_id integer NOT NULL,
    i_garantie integer NOT NULL,
    n_gesamtangebotswertinangebotswaehrung numeric(17,6),
    angebottext_i_id_kopftext integer,
    x_kopftextuebersteuert text,
    angebottext_i_id_fusstext integer,
    x_fusstextuebersteuert text,
    t_gedruckt timestamp without time zone,
    personal_i_id_storniert integer,
    t_storniert timestamp without time zone,
    personal_i_id_manuellerledigt integer,
    t_manuellerledigt timestamp without time zone,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    x_externerkommentar text,
    x_internerkommentar text,
    t_versandzeitpunkt timestamp without time zone,
    c_versandtype character varying(15),
    c_kundenanfrage character varying(40),
    b_mitzusammenfassung smallint NOT NULL,
    c_lieferartort character varying(40),
    projekt_i_id integer,
    n_korrekturbetrag numeric(17,6),
    t_aenderungsangebot timestamp without time zone,
    i_aenderungsangebot_version integer,
    kunde_i_id_rechnungsadresse integer NOT NULL,
    kunde_i_id_lieferadresse integer NOT NULL,
    ansprechpartner_i_id_rechnungsadresse integer,
    ansprechpartner_i_id_lieferadresse integer,
    personal_i_id_vertreter2 integer,
    akquisestatus_i_id integer,
    b_mindermengenzuschlag smallint NOT NULL,
    c_kommission character varying(40)
);
ALTER TABLE public.angb_angebot OWNER TO postgres;
CREATE TABLE public.angb_angebotart (
    c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.angb_angebotart OWNER TO postgres;
CREATE TABLE public.angb_angebotartspr (
    locale_c_nr character(10) NOT NULL,
    angebotart_c_nr character(15) NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.angb_angebotartspr OWNER TO postgres;
CREATE TABLE public.auft_auftrag (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    auftragart_c_nr character(15) NOT NULL,
    auftragstatus_c_nr character(15) NOT NULL,
    belegart_c_nr character(15) NOT NULL,
    t_belegdatum timestamp without time zone NOT NULL,
    kunde_i_id_auftragsadresse integer NOT NULL,
    ansprechpartner_i_id_kunde integer,
    personal_i_id_vertreter integer,
    kunde_i_id_lieferadresse integer NOT NULL,
    kunde_i_id_rechnungsadresse integer NOT NULL,
    c_bez character varying(80),
    c_bestellnummer character varying(40),
    t_bestelldatum timestamp without time zone,
    waehrung_c_nr_auftragswaehrung character(3) NOT NULL,
    f_wechselkursmandantwaehrungzuauftragswaehrung double precision NOT NULL,
    f_sonderrabattsatz double precision,
    t_liefertermin timestamp without time zone NOT NULL,
    b_lieferterminunverbindlich smallint NOT NULL,
    t_finaltermin timestamp without time zone NOT NULL,
    kostenstelle_i_id integer NOT NULL,
    b_teillieferungmoeglich smallint NOT NULL,
    b_poenale smallint NOT NULL,
    i_leihtage integer NOT NULL,
    f_versteckteraufschlag double precision NOT NULL,
    f_allgemeinerrabattsatz double precision NOT NULL,
    f_projektierungsrabattsatz double precision NOT NULL,
    lieferart_i_id integer NOT NULL,
    zahlungsziel_i_id integer NOT NULL,
    spediteur_i_id integer NOT NULL,
    i_garantie integer NOT NULL,
    n_gesamtauftragswertinauftragswaehrung numeric(17,6),
    n_materialwertinmandantenwaehrung numeric(17,6),
    n_rohdeckunginmandantenwaehrung numeric(17,6),
    auftragtext_i_id_kopftext integer,
    x_kopftextuebersteuert text,
    auftragtext_i_id_fusstext integer,
    x_fusstextuebersteuert text,
    t_gedruckt timestamp without time zone,
    personal_i_id_storniert integer,
    t_storniert timestamp without time zone,
    personal_i_id_manuellerledigt integer,
    t_manuellerledigt timestamp without time zone,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    auftrag_i_id_rahmenauftrag integer,
    angebot_i_id integer,
    x_externerkommentar text,
    x_internerkommentar text,
    t_erledigt timestamp without time zone,
    personal_i_id_erledigt integer,
    t_lauftermin timestamp without time zone,
    auftragwiederholungsintervall_c_nr character varying(40),
    f_erfuellungsgrad double precision,
    b_rohs smallint NOT NULL,
    n_rohdeckungaltinmandantenwaehrung numeric(17,6),
    t_versandzeitpunkt timestamp without time zone,
    c_versandtype character varying(15),
    auftragbegruendung_i_id integer,
    personal_i_id_begruendung integer,
    t_begruendung timestamp without time zone,
    b_versteckt smallint NOT NULL,
    ansprechpartner_i_id_lieferadresse integer,
    b_mitzusammenfassung smallint NOT NULL,
    lager_i_id_abbuchungslager integer NOT NULL,
    ansprechpartner_i_id_rechnungsadresse integer,
    c_lieferartort character varying(40),
    t_verrechenbar timestamp without time zone,
    personal_i_id_verrechenbar integer,
    projekt_i_id integer,
    n_korrekturbetrag numeric(17,6),
    t_response timestamp without time zone,
    personal_i_id_response integer,
    t_freigabe timestamp without time zone,
    personal_i_id_freigabe integer,
    n_praemie numeric(17,6),
    t_wunschtermin timestamp without time zone,
    t_aenderungsauftrag timestamp without time zone,
    t_lauftermin_bis timestamp without time zone,
    bestellung_i_id_anderermandant integer,
    t_auftragsfreigabe timestamp without time zone,
    personal_i_id_auftragsfreigabe integer,
    n_indexanpassung numeric(15,2),
    verrechenbar_i_id integer NOT NULL,
    i_aenderungsauftrag_version integer,
    laenderart_c_nr character varying(30),
    verrechnungsmodell_i_id integer,
    b_mindermengenzuschlag smallint NOT NULL,
    personal_i_id_vertreter2 integer,
    c_kommission character varying(40)
);
ALTER TABLE public.auft_auftrag OWNER TO postgres;
CREATE TABLE public.auft_auftragposition (
    i_id integer NOT NULL,
    auftrag_i_id integer NOT NULL,
    i_sort integer,
    auftragpositionart_c_nr character(15) NOT NULL,
    auftragpositionstatus_c_nr character(15),
    artikel_i_id integer,
    c_bez character varying(80),
    b_artikelbezeichnunguebersteuert smallint NOT NULL,
    x_textinhalt text,
    mediastandard_i_id integer,
    n_menge numeric(17,6),
    n_offenemenge numeric(17,6),
    einheit_c_nr character(15),
    f_rabattsatz double precision,
    b_rabattsatzuebersteuert smallint NOT NULL,
    f_zusatzrabattsatz double precision,
    mwstsatz_i_id integer,
    b_mwstsatzuebersteuert smallint NOT NULL,
    n_nettoeinzelpreis numeric(17,6),
    n_nettoeinzelpreisplusversteckteraufschlag numeric(17,6),
    n_rabattbetrag numeric(17,6),
    n_nettogesamtpreis numeric(17,6),
    n_nettogesamtpreisplusversteckteraufschlag numeric(17,6),
    n_nettogesamtpreisplusversteckteraufschlagminusrabatte numeric(17,6),
    n_mwstbetrag numeric(17,6),
    n_bruttogesamtpreis numeric(17,6),
    t_uebersteuerterliefertermin timestamp without time zone NOT NULL,
    b_drucken smallint NOT NULL,
    auftragposition_i_id_rahmenposition integer,
    c_zbez character varying(80),
    c_seriennrchargennr character varying(50),
    n_offenerahmenmenge numeric(17,6),
    position_i_id integer,
    typ_c_nr character varying(30),
    b_nettopreisuebersteuert smallint NOT NULL,
    n_einkaufpreis numeric(17,6),
    position_i_id_artikelset integer,
    verleih_i_id integer,
    kostentraeger_i_id integer,
    c_lvposition character varying(40),
    zwsvonposition_i_id integer,
    zwsbisposition_i_id integer,
    n_zwsnettosumme numeric(17,6),
    n_materialzuschlag numeric(17,6),
    lieferant_i_id integer,
    b_zwspositionspreiszeigen smallint NOT NULL,
    n_materialzuschlag_kurs numeric(15,6),
    t_materialzuschlag_datum timestamp without time zone,
    bestellposition_i_id integer,
    position_i_id_zugehoerig integer,
    b_gesehen smallint NOT NULL,
    b_hvmauebertragen smallint NOT NULL,
    b_pauschal smallint NOT NULL,
    n_dim_menge numeric(17,6),
    n_dim_hoehe numeric(17,6),
    n_dim_breite numeric(17,6),
    n_dim_tiefe numeric(17,6)
);
ALTER TABLE public.auft_auftragposition OWNER TO postgres;
CREATE VIEW public.angb_angebotauftrag AS
 SELECT row_number() OVER (ORDER BY a.angebot_i_id, a.auftrag_i_id) AS i_id,
    a.angebot_i_id,
    a.auftrag_i_id,
    a.c_nr,
    a.mandant_c_nr,
    a.kunde_i_id,
    a.c_bez,
    a.c_bestellnummer,
    a.t_belegdatum,
    a.t_angebot_nachfasstermin,
    a.t_auftrag_liefertermin,
    a.status_c_nr,
    a.n_wert,
    a.waehrung_c_nr,
    a.personal_i_id_vertreter,
    a.t_positionsliefertermin
   FROM ( SELECT ag.i_id AS angebot_i_id,
            NULL::integer AS auftrag_i_id,
            ag.c_nr,
            ag.mandant_c_nr,
            ag.kunde_i_id_angebotsadresse AS kunde_i_id,
            ag.c_bez,
            ag.c_kundenanfrage AS c_bestellnummer,
            ag.t_belegdatum,
            ag.t_nachfasstermin AS t_angebot_nachfasstermin,
            NULL::timestamp without time zone AS t_auftrag_liefertermin,
            ag.angebotstatus_c_nr AS status_c_nr,
            ag.n_gesamtangebotswertinangebotswaehrung AS n_wert,
            ag.waehrung_c_nr_angebotswaehrung AS waehrung_c_nr,
            ag.personal_i_id_vertreter,
            NULL::timestamp without time zone AS t_positionsliefertermin
           FROM public.angb_angebot ag
        UNION
         SELECT NULL::integer AS int4,
            ab.i_id,
            ab.c_nr,
            ab.mandant_c_nr,
            ab.kunde_i_id_auftragsadresse,
            ab.c_bez,
            ab.c_bestellnummer,
            ab.t_belegdatum,
            NULL::timestamp without time zone AS "timestamp",
            ab.t_liefertermin,
            ab.auftragstatus_c_nr,
            ab.n_gesamtauftragswertinauftragswaehrung,
            ab.waehrung_c_nr_auftragswaehrung,
            ab.personal_i_id_vertreter,
            ( SELECT min(ap.t_uebersteuerterliefertermin) AS min
                   FROM (public.auft_auftragposition ap
                     LEFT JOIN public.ww_artikel art ON ((art.i_id = ap.artikel_i_id)))
                  WHERE ((art.b_kalkulatorisch = 0) AND (ap.auftragpositionstatus_c_nr <> ALL (ARRAY['Erledigt       '::bpchar, 'Storniert      '::bpchar])) AND (ap.auftrag_i_id = ab.i_id))) AS min
           FROM public.auft_auftrag ab) a;
ALTER TABLE public.angb_angebotauftrag OWNER TO postgres;
CREATE TABLE public.angb_angeboteinheit (
    einheit_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.angb_angeboteinheit OWNER TO postgres;
CREATE TABLE public.angb_angeboterledigungsgrund (
    c_nr character varying(40) NOT NULL,
    i_sort integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.angb_angeboterledigungsgrund OWNER TO postgres;
CREATE TABLE public.angb_angeboterledigungsgrundspr (
    locale_c_nr character(10) NOT NULL,
    angeboterledigungsgrund_c_nr character varying(40) NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.angb_angeboterledigungsgrundspr OWNER TO postgres;
CREATE TABLE public.angb_angebotposition (
    i_id integer NOT NULL,
    angebot_i_id integer NOT NULL,
    i_sort integer NOT NULL,
    angebotpositionart_c_nr character(15) NOT NULL,
    artikel_i_id integer,
    c_bez character varying(80),
    n_gestehungspreis numeric(17,6),
    x_textinhalt text,
    mediastandard_i_id integer,
    n_menge numeric(17,6),
    einheit_c_nr character(15),
    f_rabattsatz double precision,
    b_rabattsatzuebersteuert smallint NOT NULL,
    mwstsatz_i_id integer,
    b_mwstsatzuebersteuert smallint NOT NULL,
    n_nettoeinzelpreis numeric(17,6),
    n_nettoeinzelpreisplusversteckteraufschlag numeric(17,6),
    n_rabattbetrag numeric(17,6),
    n_nettogesamtpreis numeric(17,6),
    n_nettogesamtpreisplusversteckteraufschlag numeric(17,6),
    n_nettogesamtpreisplusversteckteraufschlagminusrabatte numeric(17,6),
    n_mwstbetrag numeric(17,6),
    n_bruttogesamtpreis numeric(17,6),
    f_zusatzrabattsatz double precision,
    agstkl_i_id integer,
    n_gesamtwertagstklinangebotswaehrung numeric(17,6),
    c_zbez character varying(80),
    b_alternative smallint NOT NULL,
    position_i_id integer,
    typ_c_nr character varying(30),
    b_nettopreisuebersteuert smallint NOT NULL,
    position_i_id_artikelset integer,
    verleih_i_id integer,
    kostentraeger_i_id integer,
    c_lvposition character varying(40),
    zwsvonposition_i_id integer,
    zwsbisposition_i_id integer,
    n_zwsnettosumme numeric(17,6),
    n_materialzuschlag numeric(17,6),
    lieferant_i_id integer,
    n_einkaufpreis numeric(17,6),
    b_zwspositionspreiszeigen smallint NOT NULL,
    n_materialzuschlag_kurs numeric(15,6),
    t_materialzuschlag_datum timestamp without time zone,
    position_i_id_zugehoerig integer,
    i_lieferzeitinstunden integer,
    n_dim_menge numeric(17,6),
    n_dim_hoehe numeric(17,6),
    n_dim_breite numeric(17,6),
    n_dim_tiefe numeric(17,6)
);
ALTER TABLE public.angb_angebotposition OWNER TO postgres;
CREATE TABLE public.angb_angebotpositionart (
    positionsart_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL,
    b_versteckt smallint NOT NULL
);
ALTER TABLE public.angb_angebotpositionart OWNER TO postgres;
CREATE TABLE public.angb_angebotstatus (
    status_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.angb_angebotstatus OWNER TO postgres;
CREATE TABLE public.angb_angebottext (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    mediaart_c_nr character varying(40) NOT NULL,
    x_textinhalt text NOT NULL
);
ALTER TABLE public.angb_angebottext OWNER TO postgres;
CREATE TABLE public.part_kunde (
    i_id integer NOT NULL,
    partner_i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    waehrung_c_nr character(3) NOT NULL,
    mwstsatz_i_id integer NOT NULL,
    lieferart_i_id integer NOT NULL,
    spediteur_i_id integer NOT NULL,
    zahlungsziel_i_id integer NOT NULL,
    b_mindermengenzuschlag smallint NOT NULL,
    b_monatsrechnung smallint NOT NULL,
    b_sammelrechnung smallint NOT NULL,
    b_istreempfaenger smallint NOT NULL,
    b_preiseanlsandrucken smallint NOT NULL,
    b_rechnungsdruckmitrabatt smallint NOT NULL,
    b_distributor smallint NOT NULL,
    b_akzeptiertteillieferung smallint NOT NULL,
    b_lsgewichtangeben smallint NOT NULL,
    b_istinteressent smallint NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_bekommeprovision integer NOT NULL,
    kostenstelle_i_id integer NOT NULL,
    personal_i_id_aendern integer,
    f_rabattsatz double precision,
    i_garantieinmonaten integer,
    x_kommentar text,
    c_kurznr character varying(3),
    n_kreditlimit numeric(17,6),
    t_bonitaet timestamp without time zone,
    t_liefersperream timestamp without time zone,
    i_defaultrekopiendrucken integer,
    i_defaultlskopiendrucken integer,
    i_mitarbeiteranzahl integer,
    c_tour character varying(40),
    c_lieferantennr character varying(20),
    c_abc character(1),
    t_agbuebermittelung timestamp without time zone,
    c_fremdsystemnr character varying(40),
    konto_i_id_erloesekonto integer,
    konto_i_id_debitorenkonto integer,
    partner_i_id_rechnungsadresse integer,
    partnerbank_i_id integer,
    c_hinweisintern character varying(80),
    c_hinweisextern character varying(80),
    f_zessionsfaktor double precision,
    b_versteckterlieferant smallint NOT NULL,
    b_reversecharge smallint NOT NULL,
    vkpfartikelpreisliste_i_id_stdpreisliste integer NOT NULL,
    i_kundennummer integer,
    t_erwerbsberechtigung timestamp without time zone,
    c_erwerbsberechtigungsbegruendung character varying(80),
    lager_i_id_abbuchungslager integer NOT NULL,
    i_lieferdauer integer NOT NULL,
    c_id_extern character varying(30),
    b_zollpapier smallint NOT NULL,
    reversechargeart_i_id integer NOT NULL,
    n_kupferzahl numeric(15,6),
    b_zuschlag_inklusive smallint NOT NULL,
    n_mindestbestellwert numeric(17,6),
    c_email_rechnungsempfang character varying(300),
    laenderart_c_nr character varying(30),
    verrechnungsmodell_i_id integer,
    i_max_repos integer,
    b_rechnung_je_lieferadresse smallint NOT NULL,
    f_verpackungskosten_in_prozent double precision,
    b_vkpreis_anhand_ls_datum smallint NOT NULL
);
ALTER TABLE public.part_kunde OWNER TO postgres;
CREATE VIEW public.angb_textsuche AS
 SELECT DISTINCT 'A '::text AS c_typ,
    g.i_id,
    g.c_nr,
    (((COALESCE(a.c_bez, ''::character varying))::text || (COALESCE(a.c_zbez, ''::character varying))::text) || (COALESCE(a.c_zbez2, ''::character varying))::text) AS c_suche
   FROM ((public.angb_angebot g
     JOIN public.angb_angebotposition p ON ((g.i_id = p.angebot_i_id)))
     JOIN public.ww_artikelspr a ON ((a.artikel_i_id = p.artikel_i_id)))
UNION
 SELECT DISTINCT 'PT '::text AS c_typ,
    g.i_id,
    g.c_nr,
    (p.x_textinhalt)::character varying(4000) AS c_suche
   FROM (public.angb_angebot g
     JOIN public.angb_angebotposition p ON ((g.i_id = p.angebot_i_id)))
UNION
 SELECT DISTINCT 'ID '::text AS c_typ,
    g.i_id,
    g.c_nr,
    ((a.c_nr)::text || (COALESCE(a.c_referenznr, ''::character varying))::text) AS c_suche
   FROM ((public.angb_angebot g
     JOIN public.angb_angebotposition p ON ((g.i_id = p.angebot_i_id)))
     JOIN public.ww_artikel a ON ((a.i_id = p.artikel_i_id)))
UNION
 SELECT DISTINCT 'KD'::text AS c_typ,
    g.i_id,
    g.c_nr,
    ((COALESCE(p.c_name1nachnamefirmazeile1, ''::character varying))::text || (COALESCE(p.c_name2vornamefirmazeile2, ''::character varying))::text) AS c_suche
   FROM ((public.angb_angebot g
     JOIN public.part_kunde k ON ((k.i_id = g.kunde_i_id_angebotsadresse)))
     JOIN public.part_partner p ON ((p.i_id = k.partner_i_id)))
UNION
 SELECT DISTINCT 'ANG'::text AS c_typ,
    ang.i_id,
    ang.c_nr,
    ang.c_bez AS c_suche
   FROM public.angb_angebot ang
UNION
 SELECT DISTINCT 'LPO'::text AS c_typ,
    g.i_id,
    g.c_nr,
    (((((((l.c_lkz)::text || '-'::text) || (lpo.c_plz)::text) || ' '::text) || (o.c_name)::text) || ' '::text) || (p.c_strasse)::text) AS c_suche
   FROM (((((public.lp_landplzort lpo
     JOIN public.part_partner p ON ((lpo.i_id = p.landplzort_i_id)))
     JOIN public.lp_land l ON ((lpo.land_i_id = l.i_id)))
     JOIN public.lp_ort o ON ((lpo.ort_i_id = o.i_id)))
     JOIN public.part_kunde k ON ((p.i_id = k.partner_i_id)))
     JOIN public.angb_angebot g ON ((k.i_id = g.kunde_i_id_angebotsadresse)));
ALTER TABLE public.angb_textsuche OWNER TO postgres;
CREATE TABLE public.as_agstkl (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_nr character varying(15) NOT NULL,
    belegart_c_nr character(15) NOT NULL,
    kunde_i_id integer NOT NULL,
    ansprechpartner_i_id_kunde integer,
    c_bez character varying(40),
    t_belegdatum timestamp without time zone NOT NULL,
    waehrung_c_nr character(3) NOT NULL,
    f_wechselkursmandantwaehrungzuagstklwaehrung double precision NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    projekt_i_id integer,
    i_ekpreisbasis smallint NOT NULL,
    b_datengeaendert smallint NOT NULL,
    stueckliste_i_id integer,
    b_vorlage smallint NOT NULL,
    o_media bytea,
    datenformat_c_nr character varying(40),
    i_hoehe_dialog integer,
    c_dateiname character varying(260),
    c_zeichnungsnummer character varying(30),
    n_initialkosten numeric(17,6)
);
ALTER TABLE public.as_agstkl OWNER TO postgres;
CREATE TABLE public.as_agstklarbeitsplan (
    i_id integer NOT NULL,
    agstkl_i_id integer NOT NULL,
    i_arbeitsgang integer NOT NULL,
    i_unterarbeitsgang integer,
    artikel_i_id integer NOT NULL,
    l_stueckzeit bigint NOT NULL,
    l_ruestzeit bigint NOT NULL,
    c_kommentar character varying(80),
    x_langtext text,
    maschine_i_id integer,
    agart_c_nr character(15),
    i_aufspannung integer,
    b_nurmaschinenzeit smallint NOT NULL,
    n_stundensatzmann numeric(17,6),
    n_stundensatzmaschine numeric(17,6),
    b_initial smallint NOT NULL
);
ALTER TABLE public.as_agstklarbeitsplan OWNER TO postgres;
CREATE TABLE public.as_agstklaufschlag (
    i_id integer NOT NULL,
    agstkl_i_id integer NOT NULL,
    aufschlag_i_id integer NOT NULL,
    f_aufschlag double precision NOT NULL
);
ALTER TABLE public.as_agstklaufschlag OWNER TO postgres;
CREATE TABLE public.as_agstklmaterial (
    i_id integer NOT NULL,
    agstkl_i_id integer NOT NULL,
    material_i_id integer NOT NULL,
    c_materialtyp character(15),
    c_bez character varying(80),
    n_dimension1 numeric(15,4),
    n_dimension2 numeric(15,4),
    n_dimension3 numeric(15,4),
    n_gewichtpreis numeric(17,6),
    i_sort integer NOT NULL,
    n_gewicht numeric(15,4)
);
ALTER TABLE public.as_agstklmaterial OWNER TO postgres;
CREATE TABLE public.as_agstklmengenstaffel (
    i_id integer NOT NULL,
    agstkl_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    n_materialeinsatz_lief1 numeric(17,6),
    n_azeinsatz_lief1 numeric(17,6),
    n_vkpreis numeric(17,6),
    n_vkpreis_gewaehlt numeric(17,6),
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.as_agstklmengenstaffel OWNER TO postgres;
CREATE TABLE public.as_agstklmengenstaffel_schnellerfassung (
    i_id integer NOT NULL,
    agstkl_i_id integer NOT NULL,
    n_menge numeric(15,4) NOT NULL,
    n_aufschlag_az numeric(15,4) NOT NULL,
    n_aufschlag_material numeric(15,4) NOT NULL,
    n_wert_material numeric(17,6) NOT NULL,
    n_wert_az numeric(17,6) NOT NULL,
    n_preis_einheit numeric(17,6) NOT NULL
);
ALTER TABLE public.as_agstklmengenstaffel_schnellerfassung OWNER TO postgres;
CREATE TABLE public.as_agstklposition (
    i_id integer NOT NULL,
    agstkl_i_id integer NOT NULL,
    i_sort integer,
    agstklpositionsart_c_nr character(15) NOT NULL,
    artikel_i_id integer,
    c_bez character varying(80),
    c_zbez character varying(80),
    b_artikelbezeichnunguebersteuert smallint NOT NULL,
    n_menge numeric(17,6),
    einheit_c_nr character(15),
    f_rabattsatz double precision,
    b_rabattsatzuebersteuert smallint NOT NULL,
    f_zusatzrabattsatz double precision,
    n_nettoeinzelpreis numeric(17,6),
    n_nettogesamtpreis numeric(17,6),
    n_gestehungspreis numeric(17,6) NOT NULL,
    b_drucken smallint NOT NULL,
    n_materialzuschlag numeric(17,6),
    f_aufschlag double precision,
    n_aufschlag numeric(17,6),
    n_nettogesamtmitaufschlag numeric(17,6),
    b_aufschlaggesamt_fixiert smallint NOT NULL,
    c_position character varying(3000),
    b_mit_preisen smallint NOT NULL,
    b_ruestmenge smallint NOT NULL,
    b_initial smallint NOT NULL
);
ALTER TABLE public.as_agstklposition OWNER TO postgres;
CREATE TABLE public.as_agstklpositionsart (
    positionsart_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.as_agstklpositionsart OWNER TO postgres;
CREATE TABLE public.as_aufschlag (
    i_id integer NOT NULL,
    f_aufschlag double precision NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(80) NOT NULL,
    b_material smallint NOT NULL
);
ALTER TABLE public.as_aufschlag OWNER TO postgres;
CREATE TABLE public.as_einkaufsangebot (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_nr character varying(15) NOT NULL,
    t_belegdatum timestamp without time zone NOT NULL,
    c_projekt character varying(80),
    kunde_i_id integer NOT NULL,
    ansprechpartner_i_id integer,
    n_menge1 numeric(17,6) NOT NULL,
    n_menge2 numeric(17,6) NOT NULL,
    n_menge3 numeric(17,6) NOT NULL,
    n_menge4 numeric(17,6) NOT NULL,
    n_menge5 numeric(17,6) NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    t_liefertermin timestamp without time zone,
    b_rohs smallint NOT NULL,
    i_anzahlwebabfragen integer,
    b_kunde_exportieren smallint NOT NULL,
    i_optimieren_lieferzeit integer,
    b_optimieren_minmenge smallint NOT NULL,
    b_optimieren_verpackungseinheit smallint NOT NULL,
    t_fertigungstermin timestamp without time zone,
    c_kommentar character varying(3000),
    i_optimieren_menge integer,
    i_sortierung integer
);
ALTER TABLE public.as_einkaufsangebot OWNER TO postgres;
CREATE TABLE public.as_einkaufsangebotposition (
    i_id integer NOT NULL,
    einkaufsangebot_i_id integer NOT NULL,
    i_sort integer,
    agstklpositionsart_c_nr character(15) NOT NULL,
    artikel_i_id integer,
    c_bez character varying(80),
    c_zbez character varying(80),
    b_artikelbezeichnunguebersteuert smallint NOT NULL,
    i_wiederbeschaffungszeit integer,
    i_verpackungseinheit integer,
    f_mindestbestellmenge double precision,
    n_menge numeric(17,6),
    n_preis1 numeric(17,6),
    n_preis2 numeric(17,6),
    n_preis3 numeric(17,6),
    n_preis4 numeric(17,6),
    n_preis5 numeric(17,6),
    c_bemerkung character varying(300),
    einheit_c_nr character(15),
    c_position character varying(3000),
    c_internebemerkung character varying(300),
    b_mitdrucken smallint NOT NULL,
    c_kommentar1 character varying(3000),
    c_kommentar2 character varying(3000),
    hersteller_i_id integer,
    lieferant_i_id integer,
    t_letztewebabfrage timestamp without time zone,
    c_buyerurl character varying(300),
    c_artikelnrhersteller character varying(40),
    positionlieferant_i_id_uebersteuert_menge1 integer,
    positionlieferant_i_id_uebersteuert_menge2 integer,
    positionlieferant_i_id_uebersteuert_menge3 integer,
    positionlieferant_i_id_uebersteuert_menge4 integer,
    positionlieferant_i_id_uebersteuert_menge5 integer,
    c_artikelbezhersteller character varying(80),
    c_zbez2 character varying(80),
    i_lfdnummer integer
);
ALTER TABLE public.as_einkaufsangebotposition OWNER TO postgres;
CREATE TABLE public.as_ekaglieferant (
    i_id integer NOT NULL,
    einkaufsangebot_i_id integer NOT NULL,
    lieferant_i_id integer NOT NULL,
    t_import timestamp without time zone,
    c_angebotsnummer character varying(300),
    waehrung_c_nr character(3) NOT NULL,
    ansprechpartner_i_id integer,
    t_versand timestamp without time zone,
    n_aufschlag numeric(15,4) NOT NULL
);
ALTER TABLE public.as_ekaglieferant OWNER TO postgres;
CREATE TABLE public.as_ekgruppe (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.as_ekgruppe OWNER TO postgres;
CREATE TABLE public.as_ekgruppelieferant (
    i_id integer NOT NULL,
    ekgruppe_i_id integer NOT NULL,
    lieferant_i_id integer NOT NULL,
    ansprechpartner_i_id integer
);
ALTER TABLE public.as_ekgruppelieferant OWNER TO postgres;
CREATE TABLE public.as_ekweblieferant (
    i_id integer NOT NULL,
    einkaufsangebot_i_id integer NOT NULL,
    webpartner_i_id integer NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.as_ekweblieferant OWNER TO postgres;
CREATE TABLE public.as_positionlieferant (
    i_id integer NOT NULL,
    einkaufsangebotposition_i_id integer NOT NULL,
    ekaglieferant_i_id integer NOT NULL,
    c_artikelnrlieferant character varying(100),
    c_bemerkung character varying(300),
    i_lieferzeitinkw integer,
    n_lagerstand numeric(15,4),
    n_verpackungseinheit numeric(15,4),
    n_mindestbestellmenge numeric(15,4),
    n_transportkosten numeric(17,6),
    n_preis_menge1 numeric(17,6),
    n_preis_menge2 numeric(17,6),
    n_preis_menge3 numeric(17,6),
    n_preis_menge4 numeric(17,6),
    n_preis_menge5 numeric(17,6),
    t_aendern timestamp without time zone NOT NULL,
    b_menge1_bestellen smallint NOT NULL,
    b_menge2_bestellen smallint NOT NULL,
    b_menge3_bestellen smallint NOT NULL,
    b_menge4_bestellen smallint NOT NULL,
    b_menge5_bestellen smallint NOT NULL,
    c_bemerkung_intern character varying(300),
    c_bemerkung_verkauf character varying(300)
);
ALTER TABLE public.as_positionlieferant OWNER TO postgres;
CREATE TABLE public.as_webabfrage (
    i_id integer NOT NULL,
    i_typ integer NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.as_webabfrage OWNER TO postgres;
CREATE TABLE public.as_webfindchips (
    webpartner_i_id integer NOT NULL,
    c_distributor character varying(40) NOT NULL,
    c_name character varying(40)
);
ALTER TABLE public.as_webfindchips OWNER TO postgres;
CREATE TABLE public.as_weblieferant (
    i_id integer NOT NULL,
    webpartner_i_id integer NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.as_weblieferant OWNER TO postgres;
CREATE TABLE public.as_webpartner (
    i_id integer NOT NULL,
    webabfrage_i_id integer NOT NULL,
    lieferant_i_id integer
);
ALTER TABLE public.as_webpartner OWNER TO postgres;
CREATE TABLE public.auft_auftragart (
    c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.auft_auftragart OWNER TO postgres;
CREATE TABLE public.auft_auftragartspr (
    locale_c_nr character(10) NOT NULL,
    auftragart_c_nr character(15) NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.auft_auftragartspr OWNER TO postgres;
CREATE TABLE public.auft_auftragauftragdokument (
    i_id integer NOT NULL,
    auftrag_i_id integer NOT NULL,
    auftragdokument_i_id integer NOT NULL
);
ALTER TABLE public.auft_auftragauftragdokument OWNER TO postgres;
CREATE TABLE public.auft_auftragbegruendung (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.auft_auftragbegruendung OWNER TO postgres;
CREATE TABLE public.auft_auftragdokument (
    i_id integer NOT NULL,
    c_nr character(15) NOT NULL,
    c_bez character varying(80),
    b_versteckt smallint NOT NULL
);
ALTER TABLE public.auft_auftragdokument OWNER TO postgres;
CREATE TABLE public.auft_auftragkostenstelle (
    i_id integer NOT NULL,
    auftrag_i_id integer NOT NULL,
    kostenstelle_i_id integer NOT NULL
);
ALTER TABLE public.auft_auftragkostenstelle OWNER TO postgres;
CREATE TABLE public.auft_auftragpositionart (
    positionsart_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL,
    b_versteckt smallint NOT NULL
);
ALTER TABLE public.auft_auftragpositionart OWNER TO postgres;
CREATE TABLE public.auft_auftragpositionstatus (
    status_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.auft_auftragpositionstatus OWNER TO postgres;
CREATE TABLE public.auft_auftragseriennrn (
    i_id integer NOT NULL,
    auftragposition_i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    c_seriennr character varying(50) NOT NULL,
    i_sort integer NOT NULL,
    c_kommentar character varying(80),
    version_nr integer,
    personal_i_id_anlegen integer,
    t_anlegen timestamp without time zone
);
ALTER TABLE public.auft_auftragseriennrn OWNER TO postgres;
CREATE TABLE public.auft_auftragstatus (
    status_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.auft_auftragstatus OWNER TO postgres;
CREATE TABLE public.auft_auftragteilnehmer (
    i_id integer NOT NULL,
    i_sort integer NOT NULL,
    auftrag_i_id integer NOT NULL,
    partner_i_id_auftragteilnehmer integer NOT NULL,
    b_istexternerteilnehmer smallint NOT NULL,
    funktion_i_id integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    kostenstelle_i_id integer
);
ALTER TABLE public.auft_auftragteilnehmer OWNER TO postgres;
CREATE TABLE public.auft_auftragtext (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    mediaart_c_nr character varying(40) NOT NULL,
    x_textinhalt text NOT NULL
);
ALTER TABLE public.auft_auftragtext OWNER TO postgres;
CREATE TABLE public.auft_auftragwiederholungsintervall (
    c_nr character varying(40) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.auft_auftragwiederholungsintervall OWNER TO postgres;
CREATE TABLE public.auft_auftragwiederholungsintervallspr (
    locale_c_nr character(10) NOT NULL,
    auftragwiederholungsintervall_c_nr character varying(40) NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.auft_auftragwiederholungsintervallspr OWNER TO postgres;
CREATE TABLE public.auft_indexanpassung_log (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    auftrag_i_id integer NOT NULL,
    t_durchgefuehrt timestamp without time zone NOT NULL,
    n_um_index_angepasst numeric(15,2) NOT NULL
);
ALTER TABLE public.auft_indexanpassung_log OWNER TO postgres;
CREATE TABLE public.ls_lieferschein (
    i_id integer NOT NULL,
    rechnung_i_id integer,
    c_nr character varying(15) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    lieferscheinart_c_nr character(15) NOT NULL,
    lieferscheinstatus_c_nr character(15) NOT NULL,
    belegart_c_nr character(15) NOT NULL,
    t_belegdatum timestamp without time zone NOT NULL,
    b_verrechenbar smallint NOT NULL,
    kunde_i_id_lieferadresse integer NOT NULL,
    ansprechpartner_i_id_kunde integer,
    personal_i_id_vertreter integer,
    kunde_i_id_rechnungsadresse integer NOT NULL,
    c_bez character varying(80),
    c_bestellnummer character varying(40),
    waehrung_c_nr_lieferscheinwaehrung character(3) NOT NULL,
    f_wechselkursmandantwaehrungzulieferscheinwaehrung double precision NOT NULL,
    f_versteckteraufschlag double precision NOT NULL,
    f_allgemeinerrabatt double precision NOT NULL,
    n_gesamtwertinlieferscheinwaehrung numeric(17,6),
    n_gestehungswertinmandantenwaehrung numeric(17,6),
    kostenstelle_i_id integer NOT NULL,
    lager_i_id integer NOT NULL,
    t_liefertermin timestamp without time zone,
    t_rueckgabetermin timestamp without time zone,
    lieferart_i_id integer NOT NULL,
    zahlungsziel_i_id integer NOT NULL,
    spediteur_i_id integer NOT NULL,
    i_anzahlpakete integer NOT NULL,
    f_gewichtlieferung double precision NOT NULL,
    c_versandnummer character varying(80),
    lieferscheintext_i_id_kopftext integer,
    x_kopftextuebersteuert text,
    lieferscheintext_i_id_fusstext integer,
    x_fusstextuebersteuert text,
    t_gedruckt timestamp without time zone,
    personal_i_id_manuellerledigt integer,
    t_manuellerledigt timestamp without time zone,
    personal_i_id_storniert integer,
    t_storniert timestamp without time zone,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    auftrag_i_id integer,
    ziellager_i_id integer,
    c_kommission character varying(40),
    t_versandzeitpunkt timestamp without time zone,
    c_versandtype character varying(15),
    begruendung_i_id integer,
    ansprechpartner_i_id_rechnungsadresse integer,
    c_lieferartort character varying(40),
    projekt_i_id integer,
    eingangsrechnung_i_id_zollexport integer,
    c_zollexportpapier character varying(40),
    t_zollexportpapier timestamp without time zone,
    personal_i_id_zollexportpapier integer,
    t_lieferaviso timestamp without time zone,
    personal_i_id_lieferaviso integer,
    c_versandnummer2 character varying(80),
    i_kommissioniertyp integer,
    laenderart_c_nr character varying(30),
    x_internerkommentar text
);
ALTER TABLE public.ls_lieferschein OWNER TO postgres;
CREATE TABLE public.ls_lieferscheinposition (
    i_id integer NOT NULL,
    lieferschein_i_id integer NOT NULL,
    i_sort integer NOT NULL,
    lieferscheinpositionart_c_nr character(15) NOT NULL,
    auftragposition_i_id integer,
    artikel_i_id integer,
    c_bez character varying(80),
    b_artikelbezeichnunguebersteuert smallint NOT NULL,
    x_textinhalt text,
    mediastandard_i_id integer,
    n_menge numeric(17,6),
    einheit_c_nr character(15),
    f_rabattsatz double precision,
    b_rabattsatzuebersteuert smallint NOT NULL,
    f_zusatzrabattsatz double precision,
    f_kupferzuschlag double precision,
    mwstsatz_i_id integer,
    b_mwstsatzuebersteuert smallint NOT NULL,
    n_nettoeinzelpreis numeric(17,6),
    n_nettoeinzelpreisplusversteckteraufschlag numeric(17,6),
    n_rabattbetrag numeric(17,6),
    n_nettogesamtpreis numeric(17,6),
    n_nettogesamtpreisplusversteckteraufschlag numeric(17,6),
    n_nettogesamtpreisplusversteckteraufschlagminusrabatt numeric(17,6),
    n_mwstbetrag numeric(17,6),
    n_bruttogesamtpreis numeric(17,6),
    c_zbez character varying(80),
    position_i_id integer,
    typ_c_nr character varying(30),
    b_nettopreisuebersteuert smallint NOT NULL,
    position_i_id_artikelset integer,
    verleih_i_id integer,
    kostentraeger_i_id integer,
    b_keinlieferrest smallint NOT NULL,
    c_lvposition character varying(40),
    zwsvonposition_i_id integer,
    zwsbisposition_i_id integer,
    n_zwsnettosumme numeric(17,6),
    n_materialzuschlag numeric(17,6),
    c_snrchnr_mig character varying(3000),
    lager_i_id integer,
    b_zwspositionspreiszeigen smallint NOT NULL,
    n_materialzuschlag_kurs numeric(15,6),
    t_materialzuschlag_datum timestamp without time zone,
    forecastposition_i_id integer,
    wareneingangsposition_i_id_anderermandant integer,
    position_i_id_zugehoerig integer,
    n_dim_menge numeric(17,6),
    n_dim_hoehe numeric(17,6),
    n_dim_breite numeric(17,6),
    n_dim_tiefe numeric(17,6)
);
ALTER TABLE public.ls_lieferscheinposition OWNER TO postgres;
CREATE TABLE public.rech_rechnung (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    i_geschaeftsjahr integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    rechnung_i_id_zurechnung integer,
    kunde_i_id integer NOT NULL,
    partner_i_id_rechnungsadresse integer,
    ansprechpartner_i_id integer,
    auftrag_i_id integer,
    lieferschein_i_id integer,
    lager_i_id integer,
    t_belegdatum timestamp without time zone NOT NULL,
    status_c_nr character(15) NOT NULL,
    rechnungart_c_nr character varying(30),
    kostenstelle_i_id integer NOT NULL,
    waehrung_c_nr character(3) NOT NULL,
    n_kurs numeric(16,13),
    mwstsatz_i_id integer,
    b_mwstallepositionen smallint NOT NULL,
    n_wert numeric(15,2),
    n_wertfw numeric(15,2),
    n_wertust numeric(15,2),
    n_wertustfw numeric(15,2),
    f_versteckteraufschlag double precision,
    f_allgemeinerrabattsatz double precision,
    b_mindermengenzuschlag smallint,
    n_provision numeric(17,6),
    c_provisiontext character varying(40),
    zahlungsziel_i_id integer NOT NULL,
    lieferart_i_id integer NOT NULL,
    spediteur_i_id integer NOT NULL,
    t_gedruckt timestamp without time zone,
    t_fibuuebernahme timestamp without time zone,
    c_kopftextuebersteuert text,
    c_fusstextuebersteuert text,
    t_storniert timestamp without time zone,
    personal_i_id_storniert integer,
    t_bezahltdatum timestamp without time zone,
    t_mahnsperrebis timestamp without time zone,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_manuellerledigt timestamp without time zone,
    personal_i_id_manuellerledigt integer,
    konto_i_id integer,
    c_bestellnummer character varying(40),
    b_reversecharge smallint NOT NULL,
    kunde_i_id_statistikadresse integer NOT NULL,
    personal_i_id_vertreter integer,
    gutschriftsgrund_i_id integer,
    t_versandzeitpunkt timestamp without time zone,
    c_versandtype character varying(15),
    n_mtl_zahlbetrag numeric(17,6),
    i_zahltag_mtl_zahlbetrag integer,
    c_lieferartort character varying(40),
    c_bez character varying(80),
    t_zollpapier timestamp without time zone,
    personal_i_id_zollpapier integer,
    c_zollpapier character varying(40),
    projekt_i_id integer,
    reversechargeart_i_id integer,
    t_elektronisch timestamp without time zone,
    personal_i_id_elektronisch integer,
    c_mahnungsanmerkung character varying(80),
    zahlungsart_c_nr character varying(20)
);
ALTER TABLE public.rech_rechnung OWNER TO postgres;
CREATE TABLE public.rech_rechnungposition (
    i_id integer NOT NULL,
    rechnung_i_id integer NOT NULL,
    positionsart_c_nr character(15),
    i_sort integer NOT NULL,
    rechnung_i_id_gutschrift integer,
    lieferschein_i_id integer,
    rechnungposition_i_id integer,
    auftragposition_i_id integer,
    c_bez character varying(80),
    c_zbez character varying(80),
    x_textinhalt text,
    mediastandard_i_id integer,
    artikel_i_id integer,
    n_menge numeric(15,4),
    einheit_c_nr character(15),
    b_drucken smallint NOT NULL,
    f_kupferzuschlag double precision,
    f_rabattsatz double precision,
    b_rabattsatzuebersteuert smallint NOT NULL,
    mwstsatz_i_id integer,
    b_mwstsatzuebersteuert smallint NOT NULL,
    n_einzelpreis numeric(17,6),
    n_einzelpreisplusaufschlag numeric(17,6),
    n_nettoeinzelpreis numeric(17,6),
    n_nettoeinzelpreisplusaufschlag numeric(17,6),
    n_nettoeinzelpreisplusaufschlagminusrabatt numeric(17,6),
    n_bruttoeinzelpreis numeric(17,6),
    f_zusatzrabattsatz double precision,
    position_i_id integer,
    typ_c_nr character varying(30),
    b_nettopreisuebersteuert smallint NOT NULL,
    position_i_id_artikelset integer,
    verleih_i_id integer,
    kostentraeger_i_id integer,
    zwsvonposition_i_id integer,
    zwsbisposition_i_id integer,
    n_zwsnettosumme numeric(17,6),
    c_lvposition character varying(40),
    n_materialzuschlag numeric(17,6),
    c_snrchnr_mig character varying(3000),
    b_zwspositionspreiszeigen smallint NOT NULL,
    n_materialzuschlag_kurs numeric(15,6),
    t_materialzuschlag_datum timestamp without time zone,
    position_i_id_zugehoerig integer,
    n_dim_menge numeric(17,6),
    n_dim_hoehe numeric(17,6),
    n_dim_breite numeric(17,6),
    n_dim_tiefe numeric(17,6)
);
ALTER TABLE public.rech_rechnungposition OWNER TO postgres;
CREATE VIEW public.auft_lieferstatus AS
 SELECT row_number() OVER (ORDER BY a.c_belegartnr) AS i_id,
    a.status_c_nr,
    a.c_belegartnr,
    a.auftragposition_i_id,
    a.lieferscheinposition_i_id,
    a.rechnungposition_i_id,
    a.artikel_i_id
   FROM ( SELECT ls.lieferscheinstatus_c_nr AS status_c_nr,
            'Lieferschein    '::text AS c_belegartnr,
            lp.auftragposition_i_id,
            lp.i_id AS lieferscheinposition_i_id,
            NULL::integer AS rechnungposition_i_id,
            lp.artikel_i_id
           FROM (public.ls_lieferscheinposition lp
             LEFT JOIN public.ls_lieferschein ls ON ((ls.i_id = lp.lieferschein_i_id)))
          WHERE (lp.auftragposition_i_id IS NOT NULL)
        UNION
         SELECT re.status_c_nr,
            'Auftrag        '::text AS c_belegartnr,
            rp.auftragposition_i_id,
            NULL::integer AS int4,
            rp.i_id,
            rp.artikel_i_id
           FROM (public.rech_rechnungposition rp
             LEFT JOIN public.rech_rechnung re ON ((re.i_id = rp.rechnung_i_id)))
          WHERE (rp.auftragposition_i_id IS NOT NULL)) a;
ALTER TABLE public.auft_lieferstatus OWNER TO postgres;
CREATE TABLE public.auft_meilenstein (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_nr character varying(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.auft_meilenstein OWNER TO postgres;
CREATE TABLE public.auft_meilensteinspr (
    locale_c_nr character(10) NOT NULL,
    meilenstein_i_id integer NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.auft_meilensteinspr OWNER TO postgres;
CREATE TABLE public.rech_rechnungzahlung (
    i_id integer NOT NULL,
    rechnung_i_id integer NOT NULL,
    t_zahldatum timestamp without time zone NOT NULL,
    zahlungsart_c_nr character varying(20) NOT NULL,
    bankverbindung_i_id integer,
    kassenbuch_i_id integer,
    rechnung_i_id_gutschrift integer,
    n_kurs numeric(16,13),
    n_betrag numeric(15,4) NOT NULL,
    n_betragfw numeric(15,4) NOT NULL,
    n_betragust numeric(15,4) NOT NULL,
    n_betragustfw numeric(15,4) NOT NULL,
    f_skonto double precision,
    t_wechselfaelligam timestamp without time zone,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    rechnungzahlung_i_id_gutschrift integer,
    eingangsrechnung_i_id integer,
    i_auszug integer,
    buchungdetail_i_id integer,
    c_kommentar character varying(3000)
);
ALTER TABLE public.rech_rechnungzahlung OWNER TO postgres;
CREATE VIEW public.auft_sicht_ls_re AS
 SELECT ls_lieferschein.i_id,
    'LS'::text AS c_typ,
    ls_lieferschein.c_nr AS c_nr_lieferschein,
    ls_lieferschein.t_belegdatum AS t_belegdatum_lieferschein,
    ls_lieferschein.personal_i_id_vertreter AS personal_i_id_vertreter_lieferschein,
    ls_lieferschein.lieferscheinstatus_c_nr AS status_c_nr_lieferschein,
    ls_lieferschein.n_gesamtwertinlieferscheinwaehrung AS n_wert_lieferschein,
    ls_lieferschein.waehrung_c_nr_lieferscheinwaehrung AS waehrung_c_nr_lieferschein,
    ls_lieferschein.auftrag_i_id,
    rech_rechnung.i_id AS i_id_rechnung,
    rech_rechnung.c_nr AS c_nr_rechnung,
    rech_rechnung.t_belegdatum AS t_belegdatum_rechnung,
    rech_rechnung.personal_i_id_vertreter AS personal_i_id_vertreter_rechnung,
    rech_rechnung.status_c_nr AS status_c_nr_rechnung,
    rech_rechnung.n_wertfw AS n_wert_rechnung,
    rech_rechnung.waehrung_c_nr AS waehrung_c_nr_rechnung,
    (rech_rechnung.n_wertfw - ( SELECT COALESCE(sum(rech_rechnungzahlung.n_betragfw), 0.0) AS "coalesce"
           FROM public.rech_rechnungzahlung
          WHERE (rech_rechnungzahlung.rechnung_i_id = rech_rechnung.i_id))) AS n_offen
   FROM ((public.ls_lieferschein
     LEFT JOIN public.rech_rechnungposition ON (((ls_lieferschein.i_id = rech_rechnungposition.lieferschein_i_id) AND (rech_rechnungposition.positionsart_c_nr = 'Lieferschein'::bpchar))))
     LEFT JOIN public.rech_rechnung ON ((rech_rechnung.i_id = rech_rechnungposition.rechnung_i_id)))
  WHERE (ls_lieferschein.auftrag_i_id IS NOT NULL)
UNION
 SELECT (- rech_rechnung.i_id) AS i_id,
    'RE'::text AS c_typ,
    NULL::character varying AS c_nr_lieferschein,
    NULL::timestamp without time zone AS t_belegdatum_lieferschein,
    NULL::integer AS personal_i_id_vertreter_lieferschein,
    NULL::bpchar AS status_c_nr_lieferschein,
    NULL::numeric AS n_wert_lieferschein,
    NULL::bpchar AS waehrung_c_nr_lieferschein,
    rech_rechnung.auftrag_i_id,
    rech_rechnung.i_id AS i_id_rechnung,
    rech_rechnung.c_nr AS c_nr_rechnung,
    rech_rechnung.t_belegdatum AS t_belegdatum_rechnung,
    rech_rechnung.personal_i_id_vertreter AS personal_i_id_vertreter_rechnung,
    rech_rechnung.status_c_nr AS status_c_nr_rechnung,
    rech_rechnung.n_wertfw AS n_wert_rechnung,
    rech_rechnung.waehrung_c_nr AS waehrung_c_nr_rechnung,
    (rech_rechnung.n_wertfw - ( SELECT COALESCE(sum(rech_rechnungzahlung.n_betragfw), 0.0) AS "coalesce"
           FROM public.rech_rechnungzahlung
          WHERE (rech_rechnungzahlung.rechnung_i_id = rech_rechnung.i_id))) AS n_offen
   FROM public.rech_rechnung
  WHERE (rech_rechnung.auftrag_i_id IS NOT NULL);
ALTER TABLE public.auft_sicht_ls_re OWNER TO postgres;
CREATE VIEW public.auft_textsuche AS
 SELECT DISTINCT 'A '::text AS c_typ,
    g.i_id,
    g.c_nr,
    (((COALESCE(a.c_bez, ''::character varying))::text || (COALESCE(a.c_zbez, ''::character varying))::text) || (COALESCE(a.c_zbez2, ''::character varying))::text) AS c_suche
   FROM ((public.auft_auftrag g
     JOIN public.auft_auftragposition p ON ((g.i_id = p.auftrag_i_id)))
     JOIN public.ww_artikelspr a ON ((a.artikel_i_id = p.artikel_i_id)))
  WHERE (p.b_artikelbezeichnunguebersteuert = 0)
UNION
 SELECT DISTINCT 'P '::text AS c_typ,
    g.i_id,
    g.c_nr,
    ((COALESCE(p.c_bez, ''::character varying))::text || (COALESCE(p.c_zbez, ''::character varying))::text) AS c_suche
   FROM (public.auft_auftrag g
     JOIN public.auft_auftragposition p ON ((g.i_id = p.auftrag_i_id)))
  WHERE (p.b_artikelbezeichnunguebersteuert = 1)
UNION
 SELECT DISTINCT 'PT '::text AS c_typ,
    g.i_id,
    g.c_nr,
    (p.x_textinhalt)::character varying(4000) AS c_suche
   FROM (public.auft_auftrag g
     JOIN public.auft_auftragposition p ON ((g.i_id = p.auftrag_i_id)))
UNION
 SELECT DISTINCT 'AUFT'::text AS c_typ,
    auft.i_id,
    auft.c_nr,
    auft.c_bez AS c_suche
   FROM public.auft_auftrag auft
UNION
 SELECT DISTINCT 'AB'::text AS c_typ,
    auft.i_id,
    auft.c_nr,
    auft.c_bestellnummer AS c_suche
   FROM public.auft_auftrag auft
UNION
 SELECT DISTINCT 'ID'::text AS c_typ,
    g.i_id,
    g.c_nr,
    ((a.c_nr)::text || (COALESCE(a.c_referenznr, ''::character varying))::text) AS c_suche
   FROM ((public.auft_auftrag g
     JOIN public.auft_auftragposition p ON ((g.i_id = p.auftrag_i_id)))
     JOIN public.ww_artikel a ON ((a.i_id = p.artikel_i_id)));
ALTER TABLE public.auft_textsuche OWNER TO postgres;
CREATE TABLE public.auft_verrechenbar (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    b_verrechenbar smallint NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.auft_verrechenbar OWNER TO postgres;
CREATE TABLE public.auft_zahlungsplan (
    i_id integer NOT NULL,
    auftrag_i_id integer NOT NULL,
    n_betrag numeric(17,6) NOT NULL,
    n_betrag_ursprung numeric(17,6) NOT NULL,
    t_termin timestamp without time zone
);
ALTER TABLE public.auft_zahlungsplan OWNER TO postgres;
CREATE TABLE public.auft_zahlungsplanmeilenstein (
    i_id integer NOT NULL,
    zahlungsplan_i_id integer NOT NULL,
    meilenstein_i_id integer NOT NULL,
    c_kommentar character varying(300),
    x_text text,
    t_erledigt timestamp without time zone,
    personal_i_id_erledigt integer,
    i_sort integer NOT NULL
);
ALTER TABLE public.auft_zahlungsplanmeilenstein OWNER TO postgres;
CREATE TABLE public.auft_zeitplan (
    i_id integer NOT NULL,
    auftrag_i_id integer NOT NULL,
    c_kommentar character varying(300),
    x_text text,
    n_material numeric(17,6),
    n_material_ursprung numeric(17,6),
    n_dauer numeric(17,6),
    n_dauer_ursprung numeric(17,6),
    t_material_erledigt timestamp without time zone,
    personal_i_id_material_erledigt integer,
    t_dauer_erledigt timestamp without time zone,
    personal_i_id_dauer_erledigt integer,
    t_termin timestamp without time zone
);
ALTER TABLE public.auft_zeitplan OWNER TO postgres;
CREATE TABLE public.auft_zeitplantyp (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.auft_zeitplantyp OWNER TO postgres;
CREATE TABLE public.auft_zeitplantypdetail (
    i_id integer NOT NULL,
    zeitplantyp_i_id integer NOT NULL,
    c_kommentar character varying(300) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.auft_zeitplantypdetail OWNER TO postgres;
CREATE TABLE public.auto_4vending_export (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    b_artikel smallint NOT NULL,
    c_pfadpattern_artikel character varying(400),
    b_kunden smallint NOT NULL,
    c_pfadpattern_kunden character varying(400),
    b_lieferanten smallint NOT NULL,
    c_pfadpattern_lieferanten character varying(400),
    c_email_fehler character varying(80),
    c_email_erfolgreich character varying(80)
);
ALTER TABLE public.auto_4vending_export OWNER TO postgres;
CREATE TABLE public.auto_arbeitszeitstatus (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_pfadpattern character varying(400),
    c_emailempfaenger character varying(80),
    i_tagebisstichtag integer,
    i_archivierungstage integer
);
ALTER TABLE public.auto_arbeitszeitstatus OWNER TO postgres;
CREATE TABLE public.auto_artikellieferant_webabfrage (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_email_fehler character varying(80),
    c_email_erfolgreich character varying(80)
);
ALTER TABLE public.auto_artikellieferant_webabfrage OWNER TO postgres;
CREATE TABLE public.auto_auslieferliste (
    i_id integer NOT NULL,
    mandant_c_nr character varying(400),
    c_pfadpattern character varying(400),
    i_tagebisstichtag integer,
    i_archivierungstage integer,
    b_nur_lose_nach_endetermin smallint
);
ALTER TABLE public.auto_auslieferliste OWNER TO postgres;
CREATE TABLE public.auto_bedarfsuebernahmeoffenjournal (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_emailempfaenger character varying(400)
);
ALTER TABLE public.auto_bedarfsuebernahmeoffenjournal OWNER TO postgres;
CREATE TABLE public.auto_bestellvorschlag (
    i_id integer NOT NULL,
    mandant_c_nr character varying(400),
    c_drucker character varying(400)
);
ALTER TABLE public.auto_bestellvorschlag OWNER TO postgres;
CREATE TABLE public.auto_er_import (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_importpfad character varying(400),
    c_email_fehler character varying(80),
    c_email_erfolgreich character varying(80)
);
ALTER TABLE public.auto_er_import OWNER TO postgres;
CREATE TABLE public.auto_fehlmengendruck (
    i_id integer NOT NULL,
    mandant_c_nr character varying(400),
    c_drucker character varying(400)
);
ALTER TABLE public.auto_fehlmengendruck OWNER TO postgres;
CREATE TABLE public.auto_kpireport (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_pfadpattern character varying(400),
    i_tage integer,
    i_archivierungstage integer,
    c_emailempfaenger character varying(300)
);
ALTER TABLE public.auto_kpireport OWNER TO postgres;
CREATE TABLE public.auto_loseerledigen (
    i_id integer NOT NULL,
    mandant_c_nr character varying(400),
    i_arbeitstage integer
);
ALTER TABLE public.auto_loseerledigen OWNER TO postgres;
CREATE TABLE public.auto_lumiquote (
    i_id integer NOT NULL,
    mandant_c_nr character varying(400),
    c_pfad character varying(400),
    c_artikelfilter character varying(400)
);
ALTER TABLE public.auto_lumiquote OWNER TO postgres;
CREATE TABLE public.auto_mahnen (
    i_id integer NOT NULL,
    mandant_c_nr character varying(400) NOT NULL,
    b_abmahnen integer,
    b_liefermahnen integer
);
ALTER TABLE public.auto_mahnen OWNER TO postgres;
CREATE TABLE public.auto_mahnungsversand (
    i_id integer NOT NULL,
    mandant_c_nr character varying(400),
    mahnlauf_i_id integer,
    c_drucker character varying(400),
    c_versandart character varying(400),
    b_rahmenbestellungmahnen integer,
    c_email_zusaetzilch character varying(300)
);
ALTER TABLE public.auto_mahnungsversand OWNER TO postgres;
CREATE TABLE public.auto_monatsabrechnungversand (
    i_id integer NOT NULL,
    mandant_c_nr character varying(400),
    i_wochentag integer,
    b_monatlich integer
);
ALTER TABLE public.auto_monatsabrechnungversand OWNER TO postgres;
CREATE TABLE public.auto_monatsabrechnungversand_abteilungen (
    i_id integer NOT NULL,
    mandant_c_nr character varying(400),
    i_wochentag integer,
    b_monatlich integer
);
ALTER TABLE public.auto_monatsabrechnungversand_abteilungen OWNER TO postgres;
CREATE TABLE public.auto_offenerahmendetailsdruck (
    i_id integer NOT NULL,
    mandant_c_nr character varying(400) NOT NULL,
    c_drucker character varying(400),
    b_sortiertnachartikel integer NOT NULL
);
ALTER TABLE public.auto_offenerahmendetailsdruck OWNER TO postgres;
CREATE TABLE public.auto_shopsync_item (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    webshop_i_id integer NOT NULL,
    t_vollstaendig timestamp without time zone,
    t_geaendert timestamp without time zone,
    c_email_fehler character varying(80),
    c_email_erfolgreich character varying(80)
);
ALTER TABLE public.auto_shopsync_item OWNER TO postgres;
CREATE TABLE public.auto_shopsync_order (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    webshop_i_id integer NOT NULL
);
ALTER TABLE public.auto_shopsync_order OWNER TO postgres;
CREATE TABLE public.auto_wejournal (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_pfadpattern character varying(400),
    c_emailempfaenger character varying(80),
    i_tagebisstichtag integer,
    i_archivierungstage integer
);
ALTER TABLE public.auto_wejournal OWNER TO postgres;
CREATE TABLE public.bes_bestellposition (
    i_id integer NOT NULL,
    bestellung_i_id integer NOT NULL,
    i_sort integer,
    bestellpositionart_c_nr character(15) NOT NULL,
    artikel_i_id integer,
    c_bez character varying(80),
    c_zbez character varying(80),
    b_artikelbezuebersteuert smallint NOT NULL,
    c_textinhalt text,
    mediastandard_i_id integer,
    n_menge numeric(17,6),
    einheit_c_nr character(15),
    f_rabattsatz double precision,
    mwstsatz_i_id integer,
    b_mwstsatzuebersteuert smallint NOT NULL,
    n_nettoeinzelpreis numeric(17,6),
    n_rabattbetrag numeric(17,6),
    n_nettogesamtpreis numeric(17,6),
    n_nettogesamtpreisminusrabatte numeric(17,6),
    t_uebersteuerterliefertermin timestamp without time zone NOT NULL,
    b_drucken smallint NOT NULL,
    n_offenemenge numeric(17,6),
    bestellposition_i_id_rahmenposition integer,
    t_auftragsbestaetigungstermin timestamp without time zone,
    c_abkommentar character varying(3000),
    c_abnummer character varying(20),
    bestellpositionstatus_c_nr character(15) NOT NULL,
    n_fixkosten numeric(17,6),
    n_fixkostengeliefert numeric(17,6),
    t_manuellvollstaendiggeliefert timestamp without time zone,
    t_abtermin_aendern timestamp without time zone,
    personal_i_id_abtermin_aendern integer,
    t_abursprungstermin timestamp without time zone,
    c_angebotnummer character varying(40),
    n_materialzuschlag numeric(17,6),
    t_lieferterminbestaetigt timestamp without time zone,
    personal_i_id_lieferterminbestaetigt integer,
    lossollmaterial_i_id integer,
    position_i_id_artikelset integer,
    kunde_i_id integer,
    b_nettopreisuebersteuert smallint NOT NULL,
    gebinde_i_id integer,
    n_anzahlgebinde numeric(17,6),
    b_wep_info_an_anforderer smallint NOT NULL,
    t_ab_belegdatum timestamp without time zone
);
ALTER TABLE public.bes_bestellposition OWNER TO postgres;
CREATE TABLE public.bes_bestellpositionart (
    positionsart_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL,
    b_versteckt smallint NOT NULL
);
ALTER TABLE public.bes_bestellpositionart OWNER TO postgres;
CREATE TABLE public.bes_bestellpositionstatus (
    status_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.bes_bestellpositionstatus OWNER TO postgres;
CREATE TABLE public.bes_bestellung (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    bestellungart_c_nr character(20),
    bestellungstatus_c_nr character(15),
    belegart_c_nr character(15) NOT NULL,
    t_belegdatum timestamp without time zone NOT NULL,
    lieferant_i_id_bestelladresse integer,
    ansprechpartner_i_id integer,
    personal_i_id_anforderer integer,
    lieferant_i_id_rechnungsadresse integer,
    c_bezprojektbezeichnung character varying(80),
    waehrung_c_nr_bestellungswaehrung character(3),
    f_wechselkursmandantwaehrungbestellungswaehrung double precision,
    t_liefertermin timestamp without time zone NOT NULL,
    kostenstelle_i_id integer,
    b_teillieferungmoeglich smallint NOT NULL,
    i_leihtage integer,
    f_allgemeinerrabattsatz double precision,
    lieferart_i_id integer NOT NULL,
    zahlungsziel_i_id integer NOT NULL,
    spediteur_i_id integer NOT NULL,
    n_bestellwert numeric(17,6),
    bestellungtext_i_id_kopftext integer,
    c_kopftextuebersteuert text,
    bestellungtext_i_id_fusstext integer,
    c_fusstextuebersteuert text,
    anfrage_i_id integer,
    t_gedruckt timestamp without time zone,
    personal_i_id_storniert integer,
    t_storniert timestamp without time zone,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    partner_i_id_lieferadresse integer,
    personal_i_id_manuellgeliefert integer,
    t_manuellgeliefert timestamp without time zone,
    bestellung_i_id_rahmenbestellung integer,
    t_mahnsperrebis timestamp without time zone,
    x_externerkommentar text,
    x_internerkommentar text,
    auftrag_i_id integer,
    mahnstufe_i_id integer,
    t_aenderungsbestellung timestamp without time zone,
    t_versandzeitpunkt timestamp without time zone,
    ansprechpartner_i_id_lieferadresse integer,
    c_versandtype character varying(15),
    partner_i_id_abholadresse integer,
    ansprechpartner_i_id_abholadresse integer,
    b_poenale smallint NOT NULL,
    c_lieferantenangebot character varying(40),
    c_lieferartort character varying(40),
    projekt_i_id integer,
    n_korrekturbetrag numeric(17,6),
    t_komissionierung_geplant timestamp without time zone,
    t_komissionierung_durchgefueht timestamp without time zone,
    t_uebergabe_technik timestamp without time zone,
    t_vollstaendig_geliefert timestamp without time zone,
    i_aenderungsbestellung_version integer,
    n_transportkosten numeric(17,6),
    personal_i_id_interneranforderer integer
);
ALTER TABLE public.bes_bestellung OWNER TO postgres;
CREATE TABLE public.bes_bestellungart (
    c_nr character(20) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.bes_bestellungart OWNER TO postgres;
CREATE TABLE public.bes_bestellungartspr (
    locale_c_nr character(10) NOT NULL,
    bestellungart_c_nr character(20) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.bes_bestellungartspr OWNER TO postgres;
CREATE TABLE public.bes_bestellungstatus (
    status_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.bes_bestellungstatus OWNER TO postgres;
CREATE TABLE public.bes_bestellungtext (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_nr character varying(15) NOT NULL,
    x_textinhalt text NOT NULL
);
ALTER TABLE public.bes_bestellungtext OWNER TO postgres;
CREATE TABLE public.bes_bestellvorschlag (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    artikel_i_id integer NOT NULL,
    n_zubestellendemenge numeric(17,6) NOT NULL,
    t_bestelltermin timestamp without time zone NOT NULL,
    belegart_c_nr character(15),
    i_belegartid integer,
    lieferant_i_id integer,
    n_nettoeinzelpreis numeric(17,6),
    n_rabattbetrag numeric(17,6),
    n_nettogesamtpreisminusrabatte numeric(17,6),
    n_nettogesamtpreis numeric(17,6),
    f_rabattsatz double precision,
    i_belegartpositionid integer,
    b_nettopreisuebersteuert smallint NOT NULL,
    projekt_i_id integer,
    x_textinhalt text,
    b_vormerkung smallint NOT NULL,
    t_vormerkung timestamp without time zone,
    personal_i_id_vormerkung integer,
    partner_i_id_standort integer,
    gebinde_i_id integer,
    n_anzahlgebinde numeric(17,6),
    personal_i_id integer,
    i_wiederbeschaffungszeit integer,
    t_bearbeitet timestamp without time zone,
    personal_i_id_bearbeitet integer,
    f_lagermindest double precision
);
ALTER TABLE public.bes_bestellvorschlag OWNER TO postgres;
CREATE TABLE public.bes_bsmahnlauf (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.bes_bsmahnlauf OWNER TO postgres;
CREATE TABLE public.bes_bsmahnstufe (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    i_tage integer NOT NULL
);
ALTER TABLE public.bes_bsmahnstufe OWNER TO postgres;
CREATE TABLE public.bes_bsmahntext (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    bsmahnstufe_i_id integer NOT NULL,
    x_textinhalt text NOT NULL
);
ALTER TABLE public.bes_bsmahntext OWNER TO postgres;
CREATE TABLE public.bes_bsmahnung (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    bsmahnlauf_i_id integer NOT NULL,
    bsmahnstufe_i_id integer NOT NULL,
    bestellung_i_id integer NOT NULL,
    t_mahndatum timestamp without time zone NOT NULL,
    t_gedruckt timestamp without time zone,
    personal_i_id_gedruckt integer,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    bestellposition_i_id integer NOT NULL,
    n_offenemenge numeric(17,6)
);
ALTER TABLE public.bes_bsmahnung OWNER TO postgres;
CREATE TABLE public.bes_bszahlungsplan (
    i_id integer NOT NULL,
    bestellung_i_id integer NOT NULL,
    t_termin timestamp without time zone NOT NULL,
    n_betrag numeric(17,6) NOT NULL,
    n_betrag_ursprung numeric(17,6) NOT NULL,
    c_kommentar character varying(300),
    x_text text,
    t_erledigt timestamp without time zone,
    personal_i_id_erledigt integer
);
ALTER TABLE public.bes_bszahlungsplan OWNER TO postgres;
CREATE TABLE public.ww_artikellieferant (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    lieferant_i_id integer NOT NULL,
    c_bezbeilieferant character varying(80),
    c_artikelnrlieferant character varying(40),
    b_herstellerbez smallint NOT NULL,
    n_einzelpreis numeric(17,6),
    f_rabatt double precision,
    n_nettopreis numeric(17,6),
    f_standardmenge double precision,
    f_mindestbestellmenge double precision,
    n_fixkosten numeric(17,6),
    c_rabattgruppe character varying(5),
    t_preisgueltigab timestamp without time zone NOT NULL,
    t_preisgueltigbis timestamp without time zone,
    i_sort integer NOT NULL,
    i_wiederbeschaffungszeit integer,
    n_verpackungseinheit numeric(17,6),
    b_rabattbehalten smallint NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    b_webshop smallint NOT NULL,
    c_angebotnummer character varying(40),
    zertifikatart_i_id integer,
    c_weblink character varying(300),
    einheit_c_nr_vpe character(15),
    gebinde_i_id integer,
    n_gebindemenge numeric(17,6),
    x_kommentar_fixkosten character varying(3000),
    t_letztewebabfrage timestamp without time zone,
    n_webabfrage_bestand numeric(15,4),
    b_nicht_lieferbar smallint NOT NULL,
    x_kommentar_nicht_lieferbar character varying(3000),
    anfragepositionlieferdaten_i_id integer,
    n_initialkosten numeric(17,6)
);
ALTER TABLE public.ww_artikellieferant OWNER TO postgres;
CREATE VIEW public.bes_lieferantenoptimieren AS
 SELECT bv.i_id AS bestellvorschlag_i_id,
    al.i_id AS artikellieferant_i_id,
    bv.lieferant_i_id AS lieferant_i_id_bestellvorschlag,
    al.lieferant_i_id AS lieferant_i_id_artikellieferant,
    al.artikel_i_id
   FROM (public.ww_artikellieferant al
     JOIN public.bes_bestellvorschlag bv ON ((bv.artikel_i_id = al.artikel_i_id)))
  WHERE ((al.gebinde_i_id = bv.gebinde_i_id) OR ((al.gebinde_i_id IS NULL) AND (bv.gebinde_i_id IS NULL)));
ALTER TABLE public.bes_lieferantenoptimieren OWNER TO postgres;
CREATE TABLE public.bes_mahngruppe (
    artgru_i_id integer NOT NULL
);
ALTER TABLE public.bes_mahngruppe OWNER TO postgres;
CREATE VIEW public.bes_textsuche AS
 SELECT DISTINCT 'A '::text AS c_typ,
    g.i_id,
    g.c_nr,
    (((COALESCE(a.c_bez, ''::character varying))::text || (COALESCE(a.c_zbez, ''::character varying))::text) || (COALESCE(a.c_zbez2, ''::character varying))::text) AS c_suche
   FROM ((public.bes_bestellung g
     JOIN public.bes_bestellposition p ON ((g.i_id = p.bestellung_i_id)))
     JOIN public.ww_artikelspr a ON ((a.artikel_i_id = p.artikel_i_id)))
UNION
 SELECT DISTINCT 'PT '::text AS c_typ,
    g.i_id,
    g.c_nr,
    ((((p.c_textinhalt)::character varying(4000))::text || (COALESCE(p.c_bez, ''::character varying))::text) || (COALESCE(p.c_zbez, ''::character varying))::text) AS c_suche
   FROM (public.bes_bestellung g
     JOIN public.bes_bestellposition p ON ((g.i_id = p.bestellung_i_id)))
UNION
 SELECT DISTINCT 'ID '::text AS c_typ,
    g.i_id,
    g.c_nr,
    ((a.c_nr)::text || (COALESCE(a.c_referenznr, ''::character varying))::text) AS c_suche
   FROM ((public.bes_bestellung g
     JOIN public.bes_bestellposition p ON ((g.i_id = p.bestellung_i_id)))
     JOIN public.ww_artikel a ON ((a.i_id = p.artikel_i_id)))
UNION
 SELECT DISTINCT 'KD'::text AS c_typ,
    g.i_id,
    g.c_nr,
    ((COALESCE(p.c_name1nachnamefirmazeile1, ''::character varying))::text || (COALESCE(p.c_name2vornamefirmazeile2, ''::character varying))::text) AS c_suche
   FROM ((public.bes_bestellung g
     JOIN public.part_lieferant k ON ((k.i_id = g.lieferant_i_id_bestelladresse)))
     JOIN public.part_partner p ON ((p.i_id = k.partner_i_id)))
UNION
 SELECT DISTINCT 'BES'::text AS c_typ,
    bes.i_id,
    bes.c_nr,
    bes.c_bezprojektbezeichnung AS c_suche
   FROM public.bes_bestellung bes
UNION
 SELECT DISTINCT 'LPO'::text AS c_typ,
    g.i_id,
    g.c_nr,
    (((((((l.c_lkz)::text || '-'::text) || (lpo.c_plz)::text) || ' '::text) || (o.c_name)::text) || ' '::text) || (p.c_strasse)::text) AS c_suche
   FROM (((((public.lp_landplzort lpo
     JOIN public.part_partner p ON ((lpo.i_id = p.landplzort_i_id)))
     JOIN public.lp_land l ON ((lpo.land_i_id = l.i_id)))
     JOIN public.lp_ort o ON ((lpo.ort_i_id = o.i_id)))
     JOIN public.part_lieferant k ON ((p.i_id = k.partner_i_id)))
     JOIN public.bes_bestellung g ON ((k.i_id = g.lieferant_i_id_bestelladresse)));
ALTER TABLE public.bes_textsuche OWNER TO postgres;
CREATE TABLE public.bes_wareneingang (
    i_id integer NOT NULL,
    i_sort integer,
    c_lieferscheinnr character varying(50) NOT NULL,
    t_lieferscheindatum timestamp without time zone NOT NULL,
    n_transportkosten numeric(15,2),
    f_gemeinkostenfaktor double precision,
    f_rabattsatz double precision,
    t_wareneingangsdatum timestamp without time zone NOT NULL,
    bestellung_i_id integer NOT NULL,
    lager_i_id integer NOT NULL,
    n_wechselkurs numeric(16,13),
    eingangsrechnung_i_id integer,
    n_zollkosten numeric(15,2),
    n_bankspesen numeric(15,2),
    n_sonstigespesen numeric(15,2)
);
ALTER TABLE public.bes_wareneingang OWNER TO postgres;
CREATE TABLE public.bes_wareneingangsposition (
    i_id integer NOT NULL,
    wareneingang_i_id integer NOT NULL,
    n_geliefertemenge numeric(17,6),
    n_gelieferterpreis numeric(17,6),
    n_rabattwert numeric(17,6),
    n_anteiligetransportkosten numeric(17,6),
    n_einstandspreis numeric(17,6),
    bestellposition_i_id integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    x_externerkommentar text,
    x_internerkommentar text,
    eingangsrechnung_i_id_uebersteuert integer,
    t_manuellerledigt timestamp without time zone,
    n_anteiligefixkosten numeric(17,6),
    b_preiseerfasst smallint NOT NULL,
    b_verraeumt smallint NOT NULL,
    n_breite_in_mm numeric(15,4),
    n_tiefe_in_mm numeric(15,4),
    i_rn integer
);
ALTER TABLE public.bes_wareneingangsposition OWNER TO postgres;
CREATE VIEW public.bes_warezwischenmandanten AS
 SELECT (((((COALESCE((bp.i_id)::character varying, ''::character varying))::text || '|'::text) || (COALESCE((ap.i_id)::character varying, ''::character varying))::text) || '|'::text) || (COALESCE((lp.i_id)::character varying, ''::character varying))::text) AS c_nr,
    bp.i_id AS bestellposition_i_id,
    ap.i_id AS auftragposition_i_id,
    lp.i_id AS lieferscheinposition_i_id
   FROM (((public.auft_auftragposition ap
     LEFT JOIN public.bes_bestellposition bp ON ((ap.bestellposition_i_id = bp.i_id)))
     LEFT JOIN public.bes_bestellung b ON ((bp.bestellung_i_id = b.i_id)))
     LEFT JOIN public.ls_lieferscheinposition lp ON ((lp.auftragposition_i_id = ap.i_id)))
  WHERE ((ap.bestellposition_i_id IS NOT NULL) AND (((b.bestellungstatus_c_nr <> ALL (ARRAY['Erledigt'::bpchar, 'Storniert'::bpchar])) AND (bp.bestellpositionstatus_c_nr <> ALL (ARRAY['Erledigt'::bpchar, 'Geliefert'::bpchar]))) OR ((b.bestellungstatus_c_nr <> ALL (ARRAY['Erledigt'::bpchar, 'Storniert'::bpchar])) AND (lp.i_id IS NOT NULL) AND (lp.wareneingangsposition_i_id_anderermandant IS NULL))));
ALTER TABLE public.bes_warezwischenmandanten OWNER TO postgres;
CREATE TABLE public.er_auftragszuordnung (
    i_id integer NOT NULL,
    eingangsrechnung_i_id integer NOT NULL,
    auftrag_i_id integer NOT NULL,
    n_betrag numeric(15,2) NOT NULL,
    c_text character varying(40),
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    b_keine_auftragswertung smallint NOT NULL,
    f_verrechenbar double precision,
    t_erledigt timestamp without time zone,
    personal_i_id_erledigt integer
);
ALTER TABLE public.er_auftragszuordnung OWNER TO postgres;
CREATE TABLE public.er_auftragszuordnungverrechnet (
    i_id integer NOT NULL,
    auftragszuordnung_i_id integer NOT NULL,
    rechnungposition_i_id integer NOT NULL,
    n_betrag numeric(17,6) NOT NULL
);
ALTER TABLE public.er_auftragszuordnungverrechnet OWNER TO postgres;
CREATE TABLE public.er_eingangsrechnung (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    i_geschaeftsjahr integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    eingangsrechnungart_c_nr character(20) NOT NULL,
    t_belegdatum timestamp without time zone NOT NULL,
    t_freigabedatum timestamp without time zone NOT NULL,
    lieferant_i_id integer NOT NULL,
    c_text character varying(40),
    kostenstelle_i_id integer,
    zahlungsziel_i_id integer,
    bestellung_i_id integer,
    konto_i_id integer,
    n_betrag numeric(15,2) NOT NULL,
    n_betragfw numeric(15,2) NOT NULL,
    n_ustbetrag numeric(15,2) NOT NULL,
    n_ustbetragfw numeric(15,2) NOT NULL,
    mwstsatz_i_id integer,
    n_kurs numeric(16,13),
    waehrung_c_nr character(3) NOT NULL,
    status_c_nr character(15) NOT NULL,
    t_bezahltdatum timestamp without time zone,
    mahnstufe_i_id integer,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_manuellerledigt integer,
    t_manuellerledigt timestamp without time zone,
    c_lieferantenrechnungsnummer character varying(20),
    t_fibuuebernahme timestamp without time zone,
    c_kundendaten character varying(40),
    t_gedruckt timestamp without time zone,
    auftragwiederholungsintervall_c_nr character varying(40),
    eingangsrechnung_i_id_nachfolger integer,
    b_reversecharge smallint NOT NULL,
    b_igerwerb smallint NOT NULL,
    t_mahndatum timestamp without time zone,
    t_wiederholenderledigt timestamp without time zone,
    personal_i_id_wiederholenderledigt integer,
    t_zollimportpapier timestamp without time zone,
    personal_i_id_zollimportpapier integer,
    c_zollimportpapier character varying(40),
    c_weartikel character varying(80),
    eingangsrechnung_i_id_zollimport integer,
    b_mitpositionen integer NOT NULL,
    c_kopftextuebersteuert text,
    c_fusstextuebersteuert text,
    reversechargeart_i_id integer,
    personal_i_id_abw_bankverbindung integer,
    t_geprueft timestamp without time zone,
    personal_i_id_geprueft integer
);
ALTER TABLE public.er_eingangsrechnung OWNER TO postgres;
CREATE TABLE public.er_eingangsrechnungart (
    c_nr character(20) NOT NULL
);
ALTER TABLE public.er_eingangsrechnungart OWNER TO postgres;
CREATE TABLE public.er_eingangsrechnungartspr (
    eingangsrechnungart_c_nr character(20) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.er_eingangsrechnungartspr OWNER TO postgres;
CREATE TABLE public.er_eingangsrechnungstatus (
    status_c_nr character(15) NOT NULL
);
ALTER TABLE public.er_eingangsrechnungstatus OWNER TO postgres;
CREATE TABLE public.er_eingangsrechnungtext (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_nr character varying(15) NOT NULL,
    x_textinhalt text NOT NULL
);
ALTER TABLE public.er_eingangsrechnungtext OWNER TO postgres;
CREATE TABLE public.er_eingangsrechnungzahlung (
    i_id integer NOT NULL,
    eingangsrechnung_i_id integer NOT NULL,
    t_zahldatum timestamp without time zone NOT NULL,
    zahlungsart_c_nr character varying(20) NOT NULL,
    bankverbindung_i_id integer,
    kassenbuch_i_id integer,
    n_kurs numeric(16,13),
    n_betrag numeric(15,2) NOT NULL,
    n_betragfw numeric(15,2) NOT NULL,
    n_betragust numeric(15,2) NOT NULL,
    n_betragustfw numeric(15,2) NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    eingangsrechnungzahlung_i_id_gutschrift integer,
    eingangsrechnung_i_id_gutschrift integer,
    rechnungzahlung_i_id integer,
    i_auszug integer,
    b_kursuebersteuert smallint NOT NULL,
    buchungdetail_i_id integer,
    c_kommentar character varying(3000)
);
ALTER TABLE public.er_eingangsrechnungzahlung OWNER TO postgres;
CREATE TABLE public.er_kontierung (
    i_id integer NOT NULL,
    eingangsrechnung_i_id integer NOT NULL,
    n_betrag numeric(15,2) NOT NULL,
    n_betragust numeric(15,2) NOT NULL,
    mwstsatz_i_id integer NOT NULL,
    kostenstelle_i_id integer NOT NULL,
    konto_i_id integer NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    c_kommentar character varying(3000),
    artikel_i_id integer,
    reversechargeart_i_id integer
);
ALTER TABLE public.er_kontierung OWNER TO postgres;
CREATE VIEW public.er_textsuche AS
 SELECT DISTINCT 'ER '::text AS c_typ,
    e.i_id,
    e.c_nr,
    ((((COALESCE(e.c_lieferantenrechnungsnummer, ''::character varying))::text || (COALESCE(e.c_text, ''::character varying))::text) || (COALESCE(e.c_weartikel, ''::character varying))::text) || (COALESCE(e.c_kundendaten, ''::character varying))::text) AS c_suche
   FROM public.er_eingangsrechnung e
  WHERE ((NOT (e.c_lieferantenrechnungsnummer IS NULL)) OR (NOT (e.c_text IS NULL)) OR (NOT (e.c_kundendaten IS NULL)))
UNION
 SELECT DISTINCT 'LPO'::text AS c_typ,
    e.i_id,
    e.c_nr,
    (((((((l.c_lkz)::text || '-'::text) || (lpo.c_plz)::text) || ' '::text) || (o.c_name)::text) || ' '::text) || (COALESCE(p.c_strasse, ''::character varying))::text) AS c_suche
   FROM (((((public.lp_landplzort lpo
     JOIN public.part_partner p ON ((lpo.i_id = p.landplzort_i_id)))
     JOIN public.lp_land l ON ((lpo.land_i_id = l.i_id)))
     JOIN public.lp_ort o ON ((lpo.ort_i_id = o.i_id)))
     JOIN public.part_lieferant li ON ((p.i_id = li.partner_i_id)))
     JOIN public.er_eingangsrechnung e ON ((li.i_id = e.lieferant_i_id)))
  WHERE (NOT (p.landplzort_i_id IS NULL));
ALTER TABLE public.er_textsuche OWNER TO postgres;
CREATE TABLE public.er_zahlungsvorschlag (
    i_id integer NOT NULL,
    zahlungsvorschlaglauf_i_id integer NOT NULL,
    eingangsrechnung_i_id integer NOT NULL,
    b_bezahlen smallint NOT NULL,
    b_waere_vollstaendig_bezahlt smallint NOT NULL,
    t_faellig timestamp without time zone NOT NULL,
    n_angewandterskontosatz numeric(15,4) NOT NULL,
    n_er_brutto_betrag numeric(15,2) NOT NULL,
    n_bereits_bezahlt numeric(15,2) NOT NULL,
    n_zahlbetrag numeric(15,2) NOT NULL,
    c_auftraggeberreferenz character varying(35)
);
ALTER TABLE public.er_zahlungsvorschlag OWNER TO postgres;
CREATE TABLE public.er_zahlungsvorschlaglauf (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    t_zahlungsstichtag timestamp without time zone NOT NULL,
    t_naechsterzahlungslauf timestamp without time zone NOT NULL,
    b_mitskonto smallint NOT NULL,
    i_skontoueberziehungsfristintagen integer NOT NULL,
    bankverbindung_i_id integer NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_gespeichert timestamp without time zone,
    personal_i_id_gespeichert integer
);
ALTER TABLE public.er_zahlungsvorschlaglauf OWNER TO postgres;
CREATE TABLE public.fb_bankverbindung (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    bank_i_id integer NOT NULL,
    c_kontonummer character varying(25),
    konto_i_id integer NOT NULL,
    c_bez character varying(40),
    c_iban character varying(34),
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    b_in_liquiditaetsvorschau smallint,
    c_sepaverzeichnis character varying(100),
    b_fuer_sepalastschrift smallint NOT NULL,
    b_als_geldtransitkonto smallint,
    i_stellen_auszugsnummer integer
);
ALTER TABLE public.fb_bankverbindung OWNER TO postgres;
CREATE TABLE public.fb_belegart (
    c_nr character(15) NOT NULL,
    c_kbez character(2) NOT NULL
);
ALTER TABLE public.fb_belegart OWNER TO postgres;
CREATE TABLE public.fb_belegbuchung (
    i_id integer NOT NULL,
    belegart_c_nr character(15) NOT NULL,
    i_belegiid integer NOT NULL,
    buchung_i_id integer NOT NULL,
    buchung_i_id_zahlung integer
);
ALTER TABLE public.fb_belegbuchung OWNER TO postgres;
CREATE TABLE public.fb_buchung (
    i_id integer NOT NULL,
    buchungsart_c_nr character varying(30) NOT NULL,
    t_buchungsdatum timestamp without time zone NOT NULL,
    c_text character varying(80) NOT NULL,
    kostenstelle_i_id integer NOT NULL,
    c_belegnummer character(15) NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    geschaeftsjahr_i_geschaeftsjahr integer NOT NULL,
    t_storniert timestamp without time zone,
    personal_i_id_storniert integer,
    uvaverprobung_i_id integer,
    belegart_c_nr character(15),
    b_autombuchung smallint NOT NULL,
    b_autombuchung_eb smallint NOT NULL,
    b_autombuchung_gv smallint NOT NULL
);
ALTER TABLE public.fb_buchung OWNER TO postgres;
CREATE TABLE public.fb_buchungdetail (
    i_id integer NOT NULL,
    buchung_i_id integer NOT NULL,
    konto_i_id integer NOT NULL,
    konto_i_id_gegenkonto integer,
    n_ust numeric(15,2) NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    buchungdetailart_c_nr character(15) NOT NULL,
    n_betrag numeric(17,6) NOT NULL,
    i_auszug integer,
    i_ausziffern integer,
    c_kommentar character varying(3000)
);
ALTER TABLE public.fb_buchungdetail OWNER TO postgres;
CREATE TABLE public.fb_buchungdetailart (
    c_nr character(15) NOT NULL
);
ALTER TABLE public.fb_buchungdetailart OWNER TO postgres;
CREATE TABLE public.fb_buchungsart (
    c_nr character varying(30) NOT NULL,
    c_kbez character(2)
);
ALTER TABLE public.fb_buchungsart OWNER TO postgres;
CREATE TABLE public.fb_buchungsartspr (
    buchungsart_c_nr character varying(30) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.fb_buchungsartspr OWNER TO postgres;
CREATE TABLE public.fb_ergebnisgruppe (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(40) NOT NULL,
    ergebnisgruppe_i_id_summe integer,
    i_reihung integer NOT NULL,
    b_summenegativ smallint NOT NULL,
    b_invertiert smallint NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    b_prozentbasis smallint NOT NULL,
    i_typ integer NOT NULL,
    b_bilanzgruppe smallint NOT NULL,
    b_jahresgewinn smallint NOT NULL
);
ALTER TABLE public.fb_ergebnisgruppe OWNER TO postgres;
CREATE TABLE public.fb_exportdaten (
    i_id integer NOT NULL,
    exportlauf_i_id integer NOT NULL,
    belegart_c_nr character(15) NOT NULL,
    i_belegiid integer NOT NULL
);
ALTER TABLE public.fb_exportdaten OWNER TO postgres;
CREATE TABLE public.fb_exportlauf (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    t_stichtag timestamp without time zone
);
ALTER TABLE public.fb_exportlauf OWNER TO postgres;
CREATE TABLE public.fb_iso20022bankverbindung (
    i_id integer NOT NULL,
    bankverbindung_i_id integer NOT NULL,
    zahlungsauftragschema_i_id integer,
    lastschriftschema_i_id integer
);
ALTER TABLE public.fb_iso20022bankverbindung OWNER TO postgres;
CREATE TABLE public.fb_iso20022lastschriftschema (
    i_id integer NOT NULL,
    standard_i_id integer NOT NULL,
    schema_i_id integer NOT NULL
);
ALTER TABLE public.fb_iso20022lastschriftschema OWNER TO postgres;
CREATE TABLE public.fb_iso20022schema (
    i_id integer NOT NULL,
    c_nr character(15) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.fb_iso20022schema OWNER TO postgres;
CREATE TABLE public.fb_iso20022standard (
    i_id integer NOT NULL,
    c_nr character(15) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.fb_iso20022standard OWNER TO postgres;
CREATE TABLE public.fb_iso20022zahlungsauftragschema (
    i_id integer NOT NULL,
    standard_i_id integer NOT NULL,
    schema_i_id integer NOT NULL
);
ALTER TABLE public.fb_iso20022zahlungsauftragschema OWNER TO postgres;
CREATE TABLE public.fb_kassenbuch (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(40) NOT NULL,
    konto_i_id integer NOT NULL,
    b_negativerlaubt smallint NOT NULL,
    b_hauptkassenbuch smallint NOT NULL,
    t_gueltigvon timestamp without time zone NOT NULL,
    t_gueltigbis timestamp without time zone,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.fb_kassenbuch OWNER TO postgres;
CREATE TABLE public.fb_konto (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_nr character varying(15) NOT NULL,
    c_bez character varying(80) NOT NULL,
    konto_i_id_weiterfuehrendust integer,
    rechenregel_c_nr_weiterfuehrendust character(10),
    rechenregel_c_nr_weiterfuehrendbilanz character(10),
    konto_i_id_weiterfuehrendskonto integer,
    rechenregel_c_nr_weiterfuehrendskonto character(10),
    t_gueltigvon timestamp without time zone,
    t_gueltigbis timestamp without time zone,
    finanzamt_i_id integer,
    kostenstelle_i_id integer,
    b_automeroeffnungsbuchung smallint NOT NULL,
    b_allgemeinsichtbar smallint NOT NULL,
    b_manuellbebuchbar smallint NOT NULL,
    kontoart_c_nr character varying(30),
    kontotyp_c_nr character varying(30) NOT NULL,
    ergebnisgruppe_i_id integer,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    c_letztesortierung character varying(40),
    i_letzteselektiertebuchung integer,
    b_versteckt smallint NOT NULL,
    steuerkategorie_i_id integer,
    steuerkategorie_i_id_reverse integer,
    uvaart_i_id integer,
    c_sortiert character varying(40),
    x_bemerkung text,
    waehrung_c_nr_druck character(3),
    i_gj_eb smallint,
    t_eb_anlegen timestamp without time zone,
    ergebnisgruppe_i_id_negativ integer,
    b_ohneust smallint NOT NULL,
    c_steuerart character varying(4),
    steuerkategorie_c_nr character(15),
    mwstsatz_i_id integer
);
ALTER TABLE public.fb_konto OWNER TO postgres;
CREATE TABLE public.fb_kontoart (
    c_nr character varying(30) NOT NULL,
    i_sort integer
);
ALTER TABLE public.fb_kontoart OWNER TO postgres;
CREATE TABLE public.fb_kontoartspr (
    kontoart_c_nr character varying(30) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.fb_kontoartspr OWNER TO postgres;
CREATE TABLE public.fb_kontolaenderart (
    konto_i_id integer NOT NULL,
    laenderart_c_nr character varying(30) NOT NULL,
    konto_i_id_uebersetzt integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    finanzamt_i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    reversechargeart_i_id integer NOT NULL,
    i_id integer NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL
);
ALTER TABLE public.fb_kontolaenderart OWNER TO postgres;
CREATE TABLE public.fb_kontoland (
    konto_i_id integer NOT NULL,
    land_i_id integer NOT NULL,
    konto_i_id_uebersetzt integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    i_id integer NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL
);
ALTER TABLE public.fb_kontoland OWNER TO postgres;
CREATE TABLE public.fb_kontotyp (
    c_nr character varying(30) NOT NULL
);
ALTER TABLE public.fb_kontotyp OWNER TO postgres;
CREATE TABLE public.fb_kontotypspr (
    kontotyp_c_nr character varying(30) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.fb_kontotypspr OWNER TO postgres;
CREATE TABLE public.fb_laenderart (
    c_nr character varying(30) NOT NULL,
    i_sort integer
);
ALTER TABLE public.fb_laenderart OWNER TO postgres;
CREATE TABLE public.fb_laenderartspr (
    laenderart_c_nr character varying(30) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.fb_laenderartspr OWNER TO postgres;
CREATE TABLE public.fb_mahnlauf (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.fb_mahnlauf OWNER TO postgres;
CREATE TABLE public.fb_mahnspesen (
    i_id integer NOT NULL,
    mahnstufe_i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    waehrung_c_nr character(3) NOT NULL,
    n_mahnspesen numeric(17,6) NOT NULL
);
ALTER TABLE public.fb_mahnspesen OWNER TO postgres;
CREATE TABLE public.fb_mahnstufe (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    i_tage integer NOT NULL,
    n_mahnspesen numeric(18,0),
    f_zinssatz double precision
);
ALTER TABLE public.fb_mahnstufe OWNER TO postgres;
CREATE TABLE public.fb_mahntext (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    mahnstufe_i_id integer NOT NULL,
    x_textinhalt text NOT NULL
);
ALTER TABLE public.fb_mahntext OWNER TO postgres;
CREATE TABLE public.fb_mahnung (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    mahnlauf_i_id integer NOT NULL,
    mahnstufe_i_id integer NOT NULL,
    mahnstufe_i_id_letztemahnstufe integer,
    rechnung_i_id integer NOT NULL,
    t_mahndatum timestamp without time zone NOT NULL,
    t_letztesmahndatum timestamp without time zone,
    t_gedruckt timestamp without time zone,
    personal_i_id_gedruckt integer,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_mahnsperrebis timestamp without time zone
);
ALTER TABLE public.fb_mahnung OWNER TO postgres;
CREATE TABLE public.fb_rechenregel (
    c_nr character(10) NOT NULL
);
ALTER TABLE public.fb_rechenregel OWNER TO postgres;
CREATE TABLE public.fb_reversechargeart (
    i_id integer NOT NULL,
    c_nr character varying(30) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    i_sort integer,
    b_versteckt smallint NOT NULL
);
ALTER TABLE public.fb_reversechargeart OWNER TO postgres;
CREATE TABLE public.fb_reversechargeartspr (
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(80),
    reversechargeart_i_id integer NOT NULL
);
ALTER TABLE public.fb_reversechargeartspr OWNER TO postgres;
CREATE TABLE public.fb_sepakontoauszug (
    i_id integer NOT NULL,
    bankverbindung_i_id integer NOT NULL,
    i_auszug integer NOT NULL,
    t_auszug timestamp without time zone NOT NULL,
    n_anfangssaldo numeric(17,6),
    n_endsaldo numeric(17,6),
    i_version integer NOT NULL,
    c_camtformat character varying(15) NOT NULL,
    o_kontoauszug bytea NOT NULL,
    status_c_nr character(15) NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_verbuchen timestamp without time zone,
    personal_i_id_verbuchen integer
);
ALTER TABLE public.fb_sepakontoauszug OWNER TO postgres;
CREATE TABLE public.fb_steuerkategorie (
    i_id integer NOT NULL,
    c_nr character(15) NOT NULL,
    mandant_c_nr character(3) NOT NULL,
    c_bez character varying(80) NOT NULL,
    b_reversecharge smallint NOT NULL,
    i_sort integer,
    konto_i_id_forderungen integer,
    konto_i_id_verbindlichkeiten integer,
    finanzamt_i_id integer NOT NULL,
    konto_i_id_kursgewinn integer,
    konto_i_id_kursverlust integer,
    reversechargeart_i_id integer NOT NULL
);
ALTER TABLE public.fb_steuerkategorie OWNER TO postgres;
CREATE TABLE public.fb_steuerkategoriekonto (
    steuerkategorie_i_id integer NOT NULL,
    mwstsatzbez_i_id integer NOT NULL,
    konto_i_id_vk integer,
    konto_i_id_ek integer,
    konto_i_id_skontoek integer,
    konto_i_id_skontovk integer,
    konto_i_id_einfuhrust integer,
    i_id integer NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL
);
ALTER TABLE public.fb_steuerkategoriekonto OWNER TO postgres;
CREATE TABLE public.fb_ustuebersetzung (
    c_nr character(15) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    mwstsatzbez_i_id integer NOT NULL,
    b_igerwerb smallint NOT NULL,
    reversechargeart_i_id integer NOT NULL
);
ALTER TABLE public.fb_ustuebersetzung OWNER TO postgres;
CREATE TABLE public.fb_uvaart (
    c_nr character varying(30) NOT NULL,
    i_sort integer,
    i_id integer NOT NULL,
    c_kennzeichen character varying(15),
    mandant_c_nr character varying(3) NOT NULL,
    b_invertiert smallint NOT NULL,
    b_keine_auswahl_bei_er smallint NOT NULL
);
ALTER TABLE public.fb_uvaart OWNER TO postgres;
CREATE TABLE public.fb_uvaartspr (
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40),
    uvaart_i_id integer NOT NULL
);
ALTER TABLE public.fb_uvaartspr OWNER TO postgres;
CREATE TABLE public.fb_uvaformular (
    i_id integer NOT NULL,
    finanzamt_i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    uvaart_i_id integer NOT NULL,
    i_sort integer,
    i_gruppe integer,
    c_kennzeichen character varying(15),
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.fb_uvaformular OWNER TO postgres;
CREATE TABLE public.fb_uvaverprobung (
    i_id integer NOT NULL,
    geschaeftsjahr_i_geschaeftsjahr integer NOT NULL,
    i_monat integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    finanzamt_i_id integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL
);
ALTER TABLE public.fb_uvaverprobung OWNER TO postgres;
CREATE TABLE public.fb_warenverkehrsnummer (
    c_nr character varying(10) NOT NULL,
    c_bez character varying(1000) NOT NULL,
    c_bm character varying(40)
);
ALTER TABLE public.fb_warenverkehrsnummer OWNER TO postgres;
CREATE TABLE public.fc_fclieferadresse (
    i_id integer NOT NULL,
    forecast_i_id integer NOT NULL,
    kunde_i_id_lieferadresse integer NOT NULL,
    c_pfad_import character varying(300),
    importdef_c_nr_pfad character varying(15),
    b_zusammenziehen smallint NOT NULL,
    b_kommissionieren smallint NOT NULL,
    i_typ_rundung_position integer NOT NULL,
    i_typ_rundung_linienabruf integer NOT NULL,
    kommdrucker_i_id integer,
    b_liefermengenberuecksichtigen smallint NOT NULL
);
ALTER TABLE public.fc_fclieferadresse OWNER TO postgres;
CREATE TABLE public.fc_forecast (
    i_id integer NOT NULL,
    c_nr character varying(80) NOT NULL,
    c_projekt character varying(80),
    kunde_i_id integer NOT NULL,
    status_c_nr character(15) NOT NULL,
    c_pfad_forecastauftrag character varying(300),
    c_pfad_cow character varying(300),
    c_pfad_cod character varying(300),
    c_pfad_linienabruf character varying(300),
    importdef_c_nr_forecastauftrag character varying(15),
    importdef_c_nr_cow character varying(15),
    importdef_c_nr_cod character varying(15),
    importdef_c_nr_linienabruf character varying(15),
    i_tage_cod integer NOT NULL,
    i_wochen_cow integer NOT NULL,
    i_monate_fca integer NOT NULL,
    i_tage_gueltig integer
);
ALTER TABLE public.fc_forecast OWNER TO postgres;
CREATE TABLE public.fc_forecastart (
    c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.fc_forecastart OWNER TO postgres;
CREATE TABLE public.fc_forecastartspr (
    locale_c_nr character(10) NOT NULL,
    forecastart_c_nr character(15) NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.fc_forecastartspr OWNER TO postgres;
CREATE TABLE public.fc_forecastauftrag (
    i_id integer NOT NULL,
    c_bemerkung text,
    status_c_nr character(15) NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    t_freigabe timestamp without time zone,
    personal_i_id_freigabe integer,
    fclieferadresse_i_id integer NOT NULL,
    x_kommentar text
);
ALTER TABLE public.fc_forecastauftrag OWNER TO postgres;
CREATE TABLE public.fc_forecastposition (
    i_id integer NOT NULL,
    forecastauftrag_i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    c_bestellnummer character varying(40),
    t_termin timestamp without time zone NOT NULL,
    n_menge numeric(15,4) NOT NULL,
    status_c_nr character(15) NOT NULL,
    forecastart_c_nr character(15),
    x_kommentar text,
    personal_i_id_produktion integer,
    t_produktion timestamp without time zone,
    n_menge_erfasst numeric(15,4) NOT NULL
);
ALTER TABLE public.fc_forecastposition OWNER TO postgres;
CREATE TABLE public.fc_importdef (
    c_nr character varying(15) NOT NULL
);
ALTER TABLE public.fc_importdef OWNER TO postgres;
CREATE TABLE public.fc_importdefspr (
    locale_c_nr character(10) NOT NULL,
    importdef_c_nr character varying(15) NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.fc_importdefspr OWNER TO postgres;
CREATE TABLE public.fc_kommdrucker (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.fc_kommdrucker OWNER TO postgres;
CREATE TABLE public.fc_linienabruf (
    i_id integer NOT NULL,
    c_linie character varying(15) NOT NULL,
    t_produktionstermin timestamp without time zone NOT NULL,
    c_bereich_nr character varying(40) NOT NULL,
    c_bereich_bez character varying(40) NOT NULL,
    c_bestellnummer character varying(40) NOT NULL,
    n_menge numeric(15,4) NOT NULL,
    forecastposition_i_id integer NOT NULL,
    n_menge_erfasst numeric(15,4) NOT NULL
);
ALTER TABLE public.fc_linienabruf OWNER TO postgres;
CREATE TABLE public.fert_bedarfsuebernahme (
    i_id integer NOT NULL,
    c_losnummer character varying(15) NOT NULL,
    los_i_id integer,
    c_artikelnummer character varying(25),
    c_artikelbezeichnung character varying(80),
    artikel_i_id integer,
    c_kommentar character varying(80),
    status_c_nr character(15) NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    t_wunschtermin timestamp without time zone NOT NULL,
    personal_i_id_verbucht_gedruckt integer,
    t_verbucht_gedruckt timestamp without time zone,
    n_wunschmenge numeric(15,4) NOT NULL,
    b_abgang smallint NOT NULL,
    b_zusaetzlich smallint NOT NULL,
    o_image bytea,
    lossollmaterial_i_id integer
);
ALTER TABLE public.fert_bedarfsuebernahme OWNER TO postgres;
CREATE TABLE public.fert_internebestellung (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    internebestellung_i_id_elternlos integer,
    belegart_c_nr character(15),
    i_belegiid integer,
    stueckliste_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    t_liefertermin timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    i_belegpositioniid integer,
    t_produktionsbeginn timestamp without time zone NOT NULL,
    partner_i_id_standort integer,
    auftrag_i_id_kopfauftrag integer,
    x_ausloeser text,
    f_lagermindest double precision
);
ALTER TABLE public.fert_internebestellung OWNER TO postgres;
CREATE TABLE public.fert_los (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_nr character varying(15) NOT NULL,
    los_i_id_elternlos integer,
    kostenstelle_i_id integer NOT NULL,
    auftragposition_i_id integer,
    c_kommentar character varying(40),
    c_projekt character varying(80),
    n_losgroesse numeric(17,6) NOT NULL,
    partner_i_id_fertigungsort integer,
    personal_i_id_techniker integer,
    t_produktionsende timestamp without time zone NOT NULL,
    t_produktionsbeginn timestamp without time zone NOT NULL,
    t_ausgabe timestamp without time zone,
    personal_i_id_ausgabe integer,
    t_erledigt timestamp without time zone,
    personal_i_id_erledigt integer,
    t_produktionsstop timestamp without time zone,
    personal_i_id_produktionsstop integer,
    lager_i_id_ziel integer NOT NULL,
    status_c_nr character(15) NOT NULL,
    t_aktualisierungstueckliste timestamp without time zone,
    t_aktualisierungarbeitszeit timestamp without time zone,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_manuellerledigt integer,
    t_manuellerledigt timestamp without time zone,
    x_text text,
    c_zusatznummer character varying(4),
    t_leitstandstop timestamp without time zone,
    personal_i_id_leitstandstop integer,
    stueckliste_i_id integer,
    auftrag_i_id integer,
    fertigungsgruppe_i_id integer NOT NULL,
    wiederholendelose_i_id integer,
    f_bewertung double precision,
    x_produktionsinformation text,
    kunde_i_id integer,
    n_sollmaterial numeric(17,6),
    losbereich_i_id integer,
    t_material_vollstaendig timestamp without time zone,
    personal_i_id_material_vollstaendig integer,
    projekt_i_id integer,
    t_vp_etikettengedruckt timestamp without time zone,
    personal_i_id_vp_etikettengedruckt integer,
    forecastposition_i_id integer,
    c_schachtelplan character varying(40),
    t_nachtraeglich_geoeffnet timestamp without time zone,
    personal_i_id_nachtraeglich_geoeffnet integer,
    c_abposnr character varying(15),
    lagerplatz_i_id integer
);
ALTER TABLE public.fert_los OWNER TO postgres;
CREATE TABLE public.fert_losablieferung (
    i_id integer NOT NULL,
    los_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    n_gestehungspreis numeric(17,6) NOT NULL,
    n_materialwert numeric(17,6) NOT NULL,
    n_arbeitszeitwert numeric(17,6) NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    n_materialwertdetailliert numeric(17,6) NOT NULL,
    n_arbeitszeitwertdetailliert numeric(17,6) NOT NULL,
    b_gestehungspreisneuberechnen smallint NOT NULL,
    c_snrchnr_mig character varying(3000),
    lager_i_id integer
);
ALTER TABLE public.fert_losablieferung OWNER TO postgres;
CREATE TABLE public.fert_losbereich (
    i_id integer NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.fert_losbereich OWNER TO postgres;
CREATE TABLE public.fert_losgutschlecht (
    i_id integer NOT NULL,
    zeitdaten_i_id integer,
    n_gut numeric(17,6) NOT NULL,
    n_schlecht numeric(17,6) NOT NULL,
    n_inarbeit numeric(17,6) NOT NULL,
    lossollarbeitsplan_i_id integer NOT NULL,
    maschinenzeitdaten_i_id integer,
    fehler_i_id integer,
    c_kommentar character varying(300),
    t_zeitpunkt timestamp without time zone,
    personal_i_id_erfasst integer,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL
);
ALTER TABLE public.fert_losgutschlecht OWNER TO postgres;
CREATE TABLE public.fert_losistmaterial (
    i_id integer NOT NULL,
    lossollmaterial_i_id integer NOT NULL,
    lager_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    b_abgang smallint NOT NULL
);
ALTER TABLE public.fert_losistmaterial OWNER TO postgres;
CREATE TABLE public.fert_losklasse (
    i_id integer NOT NULL,
    c_nr character varying(20) NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.fert_losklasse OWNER TO postgres;
CREATE TABLE public.fert_losklassespr (
    losklasse_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40),
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.fert_losklassespr OWNER TO postgres;
CREATE TABLE public.fert_loslagerentnahme (
    i_id integer NOT NULL,
    los_i_id integer NOT NULL,
    lager_i_id integer NOT NULL,
    i_sort integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.fert_loslagerentnahme OWNER TO postgres;
CREATE TABLE public.fert_loslosklasse (
    i_id integer NOT NULL,
    los_i_id integer NOT NULL,
    losklasse_i_id integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.fert_loslosklasse OWNER TO postgres;
CREATE TABLE public.fert_lospruefplan (
    i_id integer NOT NULL,
    los_i_id integer NOT NULL,
    lossollmaterial_i_id_kontakt integer,
    lossollmaterial_i_id_litze integer,
    werkzeug_i_id integer,
    verschleissteil_i_id integer,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    pruefart_i_id integer NOT NULL,
    lossollmaterial_i_id_litze2 integer,
    b_doppelanschlag smallint NOT NULL,
    i_sort integer NOT NULL,
    pruefkombination_i_id integer
);
ALTER TABLE public.fert_lospruefplan OWNER TO postgres;
CREATE TABLE public.fert_lossollarbeitsplan (
    i_id integer NOT NULL,
    los_i_id integer NOT NULL,
    artikel_i_id_taetigkeit integer NOT NULL,
    l_ruestzeit bigint NOT NULL,
    l_stueckzeit bigint NOT NULL,
    n_gesamtzeit numeric(17,6) NOT NULL,
    i_arbeitsgangnummer integer NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    x_text text,
    c_komentar character varying(80),
    b_nachtraeglich smallint NOT NULL,
    maschine_i_id integer,
    b_fertig smallint NOT NULL,
    agart_c_nr character(15),
    i_aufspannung integer,
    b_autoendebeigeht smallint NOT NULL,
    i_unterarbeitsgang integer,
    b_nurmaschinenzeit smallint NOT NULL,
    i_maschinenversatztage integer,
    personal_i_id_zugeordneter integer,
    lossollmaterial_i_id integer,
    i_maschinenversatz_ms integer,
    f_fortschritt double precision,
    apkommentar_i_id integer,
    n_ppm numeric(15,4),
    t_fertig timestamp without time zone,
    personal_i_id_fertig integer,
    i_maschinenversatztage_aus_stueckliste integer,
    i_reihung integer NOT NULL,
    t_agbeginn_berechnet timestamp without time zone
);
ALTER TABLE public.fert_lossollarbeitsplan OWNER TO postgres;
CREATE TABLE public.fert_lossollmaterial (
    i_id integer NOT NULL,
    los_i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    einheit_c_nr character(15) NOT NULL,
    f_dimension1 double precision,
    f_dimension2 double precision,
    f_dimension3 double precision,
    c_position character varying(3000),
    c_kommentar character varying(80),
    montageart_i_id integer NOT NULL,
    i_lfdnummer integer,
    i_sort integer NOT NULL,
    b_nachtraeglich smallint NOT NULL,
    n_sollpreis numeric(17,6) NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    lossollmaterial_i_id_original integer,
    i_beginnterminoffset integer NOT NULL,
    b_ruestmenge smallint NOT NULL,
    n_menge_stklpos numeric(17,6),
    einheit_c_nr_stklpos character(15),
    b_dringend smallint NOT NULL,
    n_menge_pro_los numeric(23,12),
    t_export_beginn timestamp without time zone,
    t_export_ende timestamp without time zone,
    c_fehlercode character varying(1024),
    c_fehlertext character varying(3000)
);
ALTER TABLE public.fert_lossollmaterial OWNER TO postgres;
CREATE TABLE public.fert_losstatus (
    status_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.fert_losstatus OWNER TO postgres;
CREATE TABLE public.fert_lostechniker (
    i_id integer NOT NULL,
    los_i_id integer NOT NULL,
    personal_i_id integer NOT NULL
);
ALTER TABLE public.fert_lostechniker OWNER TO postgres;
CREATE TABLE public.fert_loszusatzstatus (
    i_id integer NOT NULL,
    los_i_id integer NOT NULL,
    zusatzstatus_i_id integer NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.fert_loszusatzstatus OWNER TO postgres;
CREATE TABLE public.pers_zeitdaten (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    t_zeit timestamp without time zone NOT NULL,
    taetigkeit_i_id integer,
    b_taetigkeitgeaendert smallint NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    c_belegartnr character(15),
    i_belegartid integer,
    i_belegartpositionid integer,
    c_bemerkungzubelegart character varying(80),
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    artikel_i_id integer,
    b_automatikbuchung smallint NOT NULL,
    x_kommentar text,
    c_wowurdegebucht character varying(80),
    f_verrechenbar double precision,
    t_erledigt timestamp without time zone,
    personal_i_id_erledigt integer,
    maschine_i_id integer,
    x_kommentar_intern text,
    f_dauer_uebersteuert double precision
);
ALTER TABLE public.pers_zeitdaten OWNER TO postgres;
CREATE TABLE public.stk_stueckliste (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    b_fremdfertigung smallint NOT NULL,
    auftrag_i_id_leitauftrag integer,
    n_losgroesse numeric(17,6) NOT NULL,
    partner_i_id integer,
    n_defaultdurchlaufzeit numeric(17,6),
    t_aendernposition timestamp without time zone NOT NULL,
    personal_i_id_aendernposition integer NOT NULL,
    t_aendernarbeitsplan timestamp without time zone NOT NULL,
    personal_i_id_aendernarbeitsplan integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    x_kommentar text,
    fertigungsgruppe_i_id integer NOT NULL,
    stuecklisteart_c_nr character(15) NOT NULL,
    b_materialbuchungbeiablieferung smallint NOT NULL,
    b_ausgabeunterstueckliste smallint NOT NULL,
    n_erfassungsfaktor numeric(17,6) NOT NULL,
    lager_i_id_ziellager integer NOT NULL,
    stueckliste_i_id_ek integer,
    b_ueberlieferbar smallint NOT NULL,
    t_freigabe timestamp without time zone,
    personal_i_id_freigabe integer,
    b_keine_automatische_materialbuchung smallint NOT NULL,
    stuecklistescriptart_i_id integer,
    c_fremdsystemnr character varying(30),
    b_mit_formeln smallint NOT NULL,
    stueckliste_i_id_formelstueckliste integer,
    i_reihenfolge integer,
    b_jahreslos smallint NOT NULL,
    b_druckeinlagerstandsdetail integer NOT NULL,
    b_hierarchische_chargennummern smallint NOT NULL
);
ALTER TABLE public.stk_stueckliste OWNER TO postgres;
CREATE VIEW public.fert_offeneags AS
 SELECT sa.i_id,
    COALESCE(los.kunde_i_id, ab.kunde_i_id_auftragsadresse) AS kunde_i_id,
    los.mandant_c_nr,
    los.i_id AS los_i_id,
    ( SELECT stk.artikel_i_id
           FROM public.stk_stueckliste stk
          WHERE (stk.i_id = los.stueckliste_i_id)) AS artikel_i_id_stueckliste,
    sa.i_arbeitsgangnummer,
    sa.artikel_i_id_taetigkeit,
    los.t_produktionsbeginn,
    COALESCE(sa.i_maschinenversatztage, 0) AS i_maschinenversatztage,
    COALESCE(sa.i_maschinenversatz_ms, 0) AS i_maschinenversatz_ms,
    sa.n_gesamtzeit,
    sa.maschine_i_id,
    sa.lossollmaterial_i_id,
    ( SELECT count(z.i_id) AS count
           FROM public.pers_zeitdaten z
          WHERE ((z.c_belegartnr = 'Los'::bpchar) AND (z.i_belegartpositionid = sa.i_id))) AS i_anzahlzeitdaten,
    sm.artikel_i_id AS artikel_i_id_sollmaterial
   FROM (((public.fert_lossollarbeitsplan sa
     LEFT JOIN public.fert_los los ON ((sa.los_i_id = los.i_id)))
     LEFT JOIN public.auft_auftrag ab ON ((los.auftrag_i_id = ab.i_id)))
     LEFT JOIN public.fert_lossollmaterial sm ON ((sa.lossollmaterial_i_id = sm.i_id)))
  WHERE ((sa.b_fertig = 0) AND (los.status_c_nr <> ALL (ARRAY['Storniert'::bpchar, 'Erledigt'::bpchar])));
ALTER TABLE public.fert_offeneags OWNER TO postgres;
CREATE VIEW public.fert_produzieren AS
 SELECT l.i_id AS los_i_id,
    ( SELECT sum(sa.n_gesamtzeit) AS sum
           FROM public.fert_lossollarbeitsplan sa
          WHERE ((sa.los_i_id = l.i_id) AND (sa.b_fertig = 0) AND (sa.b_nurmaschinenzeit = 0))) AS n_dauer_offen_personal,
    ( SELECT sum(sa.n_gesamtzeit) AS sum
           FROM public.fert_lossollarbeitsplan sa
          WHERE ((sa.los_i_id = l.i_id) AND (sa.b_fertig = 0) AND (sa.maschine_i_id IS NOT NULL))) AS n_dauer_offen_maschine,
    COALESCE(l.kunde_i_id, ab.kunde_i_id_auftragsadresse) AS kunde_i_id,
    ( SELECT sa.i_id
           FROM public.fert_lossollarbeitsplan sa
          WHERE ((sa.los_i_id = l.i_id) AND (sa.i_arbeitsgangnummer IN ( SELECT min(sa2.i_arbeitsgangnummer) AS min
                   FROM public.fert_lossollarbeitsplan sa2
                  WHERE ((sa2.los_i_id = l.i_id) AND (sa2.b_fertig = 0)))) AND ((sa.i_unterarbeitsgang IS NULL) OR (sa.i_unterarbeitsgang IN ( SELECT min(sa2.i_unterarbeitsgang) AS min
                   FROM public.fert_lossollarbeitsplan sa2
                  WHERE ((sa2.los_i_id = l.i_id) AND (sa2.b_fertig = 0))))) AND (sa.t_aendern IN ( SELECT min(sa2.t_aendern) AS min
                   FROM public.fert_lossollarbeitsplan sa2
                  WHERE ((sa2.los_i_id = l.i_id) AND (sa2.b_fertig = 0))
                  GROUP BY sa2.i_arbeitsgangnummer
                 HAVING (sa2.i_arbeitsgangnummer = min(sa2.i_arbeitsgangnummer)))))) AS lossollarbeitsplan_i_id_ersterarbeitsgang,
    COALESCE(( SELECT sa.i_maschinenversatztage
           FROM public.fert_lossollarbeitsplan sa
          WHERE ((sa.los_i_id = l.i_id) AND (sa.i_arbeitsgangnummer IN ( SELECT min(sa2.i_arbeitsgangnummer) AS min
                   FROM public.fert_lossollarbeitsplan sa2
                  WHERE ((sa2.los_i_id = l.i_id) AND (sa2.b_fertig = 0)))) AND ((sa.i_unterarbeitsgang IS NULL) OR (sa.i_unterarbeitsgang IN ( SELECT min(sa2.i_unterarbeitsgang) AS min
                   FROM public.fert_lossollarbeitsplan sa2
                  WHERE ((sa2.los_i_id = l.i_id) AND (sa2.b_fertig = 0))))) AND (sa.t_aendern IN ( SELECT min(sa2.t_aendern) AS min
                   FROM public.fert_lossollarbeitsplan sa2
                  WHERE ((sa2.los_i_id = l.i_id) AND (sa2.b_fertig = 0))
                  GROUP BY sa2.i_arbeitsgangnummer
                 HAVING (sa2.i_arbeitsgangnummer = min(sa2.i_arbeitsgangnummer)))))), 0) AS i_maschinenversatztage
   FROM (public.fert_los l
     LEFT JOIN public.auft_auftrag ab ON ((l.auftrag_i_id = ab.i_id)));
ALTER TABLE public.fert_produzieren OWNER TO postgres;
CREATE TABLE public.fert_pruefergebins (
    i_id integer NOT NULL,
    losablieferung_i_id integer NOT NULL,
    lospruefplan_i_id integer NOT NULL,
    n_crimphoehe_draht numeric(15,4),
    n_crimphoehe_isolation numeric(15,4),
    n_crimpbreite_draht numeric(15,4),
    n_crimpbreite_isolation numeric(15,4),
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    n_wert numeric(15,4),
    b_wert numeric(15,4),
    n_abzugskraft_litze numeric(15,4),
    n_abzugskraft_litze2 numeric(15,4),
    c_wert character varying(80)
);
ALTER TABLE public.fert_pruefergebins OWNER TO postgres;
CREATE TABLE public.ww_artgru (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    artgru_i_id integer,
    b_rueckgabe smallint NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    b_zertifizierung smallint NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    b_keinevkwarnmeldungenimls smallint NOT NULL,
    kostenstelle_i_id integer,
    n_ekpreisaufschlag numeric(15,4),
    b_seriennrtragend smallint NOT NULL,
    b_chargennrtragend smallint NOT NULL,
    b_kein_belegdruck_mit_rabatt smallint NOT NULL,
    b_aufschlag_einzelpreis smallint NOT NULL,
    artikel_i_id_kommentar integer,
    b_fremdfertigung smallint NOT NULL,
    b_bei_erster_zeitbuchung_abbuchen smallint NOT NULL
);
ALTER TABLE public.ww_artgru OWNER TO postgres;
CREATE TABLE public.ww_artikelbestellt (
    i_id integer NOT NULL,
    c_belegartnr character(15) NOT NULL,
    i_belegartpositionid integer NOT NULL,
    artikel_i_id integer NOT NULL,
    t_liefertermin timestamp without time zone NOT NULL,
    n_menge numeric(17,6) NOT NULL
);
ALTER TABLE public.ww_artikelbestellt OWNER TO postgres;
CREATE TABLE public.ww_artikelfehlmenge (
    i_id integer NOT NULL,
    c_belegartnr character(15) NOT NULL,
    i_belegartpositionid integer NOT NULL,
    artikel_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    t_liefertermin timestamp without time zone NOT NULL
);
ALTER TABLE public.ww_artikelfehlmenge OWNER TO postgres;
CREATE TABLE public.ww_artikellager (
    artikel_i_id integer NOT NULL,
    lager_i_id integer NOT NULL,
    n_gestehungspreis numeric(17,6) NOT NULL,
    n_lagerstand numeric(17,6) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    f_lagermindest double precision,
    f_lagersoll double precision
);
ALTER TABLE public.ww_artikellager OWNER TO postgres;
CREATE TABLE public.ww_artikelsperren (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    sperren_i_id integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    c_grund character varying(80) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.ww_artikelsperren OWNER TO postgres;
CREATE VIEW public.fert_verfuegbarkeit AS
 SELECT sollmat.los_i_id,
    sollmat.artikel_i_id,
    sum(sollmat.n_menge) AS n_sollmenge,
    ( SELECT COALESCE(sum(al.n_lagerstand), (0)::numeric) AS "coalesce"
           FROM public.ww_artikellager al
          WHERE ((al.artikel_i_id = sollmat.artikel_i_id) AND (al.lager_i_id IN ( SELECT la.lager_i_id
                   FROM public.fert_loslagerentnahme la
                  WHERE (la.los_i_id = sollmat.los_i_id))))) AS n_lagerstand,
    ((sum(sollmat.n_menge) - ( SELECT COALESCE(sum(al.n_lagerstand), (0)::numeric) AS "coalesce"
           FROM public.ww_artikellager al
          WHERE ((al.artikel_i_id = sollmat.artikel_i_id) AND (al.lager_i_id IN ( SELECT la.lager_i_id
                   FROM public.fert_loslagerentnahme la
                  WHERE (la.los_i_id = sollmat.los_i_id)))))) - ( SELECT COALESCE(sum(bs.n_menge), (0)::numeric) AS "coalesce"
           FROM (public.ww_artikelbestellt bs
             LEFT JOIN public.bes_bestellposition bp ON ((bs.i_belegartpositionid = bp.i_id)))
          WHERE ((bp.t_auftragsbestaetigungstermin IS NOT NULL) AND (bs.artikel_i_id = sollmat.artikel_i_id)))) AS n_soll_minus_lager_plus_bestellt,
    (( SELECT sum(fm.n_menge) AS sum
           FROM (public.ww_artikelfehlmenge fm
             LEFT JOIN public.fert_lossollmaterial sm ON ((sm.i_id = fm.i_belegartpositionid)))
          WHERE ((fm.c_belegartnr = 'Los'::bpchar) AND (sm.los_i_id = sollmat.los_i_id) AND (fm.artikel_i_id = sollmat.artikel_i_id))) - ( SELECT COALESCE(sum(al.n_lagerstand), (0)::numeric) AS "coalesce"
           FROM public.ww_artikellager al
          WHERE ((al.artikel_i_id = sollmat.artikel_i_id) AND (al.lager_i_id IN ( SELECT la.lager_i_id
                   FROM public.fert_loslagerentnahme la
                  WHERE (la.los_i_id = sollmat.los_i_id)))))) AS n_fehlmenge_minus_lager,
    ((( SELECT sum(fm.n_menge) AS sum
           FROM (public.ww_artikelfehlmenge fm
             LEFT JOIN public.fert_lossollmaterial sm ON ((sm.i_id = fm.i_belegartpositionid)))
          WHERE ((fm.c_belegartnr = 'Los'::bpchar) AND (sm.los_i_id = sollmat.los_i_id) AND (fm.artikel_i_id = sollmat.artikel_i_id))) - ( SELECT COALESCE(sum(al.n_lagerstand), (0)::numeric) AS "coalesce"
           FROM public.ww_artikellager al
          WHERE ((al.artikel_i_id = sollmat.artikel_i_id) AND (al.lager_i_id IN ( SELECT la.lager_i_id
                   FROM public.fert_loslagerentnahme la
                  WHERE (la.los_i_id = sollmat.los_i_id)))))) - ( SELECT COALESCE(sum(bs.n_menge), (0)::numeric) AS "coalesce"
           FROM (public.ww_artikelbestellt bs
             LEFT JOIN public.bes_bestellposition bp ON ((bs.i_belegartpositionid = bp.i_id)))
          WHERE ((bp.t_auftragsbestaetigungstermin IS NOT NULL) AND (bs.artikel_i_id = sollmat.artikel_i_id)))) AS n_fehlmenge_minus_lager_plus_bestellt,
    ( SELECT count(sperren.artikel_i_id) AS count
           FROM public.ww_artikelsperren sperren
          WHERE (sperren.artikel_i_id = sollmat.artikel_i_id)) AS i_anzahlsperren
   FROM (((public.fert_lossollmaterial sollmat
     LEFT JOIN public.fert_los lo ON ((lo.i_id = sollmat.los_i_id)))
     LEFT JOIN public.ww_artikel artikel ON ((artikel.i_id = sollmat.artikel_i_id)))
     LEFT JOIN public.ww_artgru ag ON ((ag.i_id = artikel.artgru_i_id)))
  WHERE ((artikel.b_lagerbewirtschaftet = 1) AND ((ag.b_fremdfertigung = 0) OR (ag.b_fremdfertigung IS NULL)))
  GROUP BY sollmat.artikel_i_id, sollmat.los_i_id, ag.b_fremdfertigung
 HAVING ((sum(sollmat.n_menge) - ( SELECT COALESCE(sum(al.n_lagerstand), (0)::numeric) AS "coalesce"
           FROM public.ww_artikellager al
          WHERE ((al.artikel_i_id = sollmat.artikel_i_id) AND (al.lager_i_id IN ( SELECT la.lager_i_id
                   FROM public.fert_loslagerentnahme la
                  WHERE (la.los_i_id = sollmat.los_i_id)))))) > (0)::numeric);
ALTER TABLE public.fert_verfuegbarkeit OWNER TO postgres;
CREATE TABLE public.fert_wiederholendelose (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    kostenstelle_i_id integer NOT NULL,
    c_projekt character varying(40),
    stueckliste_i_id integer,
    n_losgroesse numeric(17,6) NOT NULL,
    partner_i_id_fertigungsort integer NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    t_termin timestamp without time zone NOT NULL,
    lager_i_id_ziel integer NOT NULL,
    i_tagevoreilend integer NOT NULL,
    fertigungsgruppe_i_id integer NOT NULL,
    auftragwiederholungsintervall_c_nr character varying(40) NOT NULL,
    b_versteckt smallint NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.fert_wiederholendelose OWNER TO postgres;
CREATE TABLE public.fert_zusatzstatus (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(40) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.fert_zusatzstatus OWNER TO postgres;
CREATE TABLE public.is_anlage (
    i_id integer NOT NULL,
    halle_i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.is_anlage OWNER TO postgres;
CREATE TABLE public.is_geraet (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(80),
    standort_i_id integer NOT NULL,
    halle_i_id integer,
    anlage_i_id integer,
    ismaschine_i_id integer,
    geraetetyp_i_id integer,
    c_standort character varying(80),
    c_geraetesnr character varying(80),
    c_versorgungskreis character varying(80),
    b_aufwand smallint NOT NULL,
    b_messwertabsolut smallint NOT NULL,
    n_grenzwertmin numeric(17,6),
    n_grenzwertmax numeric(17,6),
    n_grenzwert numeric(17,6),
    x_bemerkung text,
    b_versteckt smallint NOT NULL,
    c_fabrikat character varying(80),
    i_anzahl integer,
    c_leistung character varying(80),
    gewerk_i_id integer,
    hersteller_i_id integer
);
ALTER TABLE public.is_geraet OWNER TO postgres;
CREATE TABLE public.is_geraetehistorie (
    i_id integer NOT NULL,
    geraet_i_id integer NOT NULL,
    personal_i_id_techniker integer NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    t_wartung timestamp without time zone NOT NULL,
    b_nichtmoeglich smallint NOT NULL
);
ALTER TABLE public.is_geraetehistorie OWNER TO postgres;
CREATE TABLE public.is_geraetetyp (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.is_geraetetyp OWNER TO postgres;
CREATE TABLE public.is_gewerk (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.is_gewerk OWNER TO postgres;
CREATE TABLE public.is_halle (
    i_id integer NOT NULL,
    standort_i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.is_halle OWNER TO postgres;
CREATE TABLE public.is_instandhaltung (
    i_id integer NOT NULL,
    kunde_i_id integer NOT NULL,
    b_versteckt smallint NOT NULL,
    kategorie_i_id integer NOT NULL
);
ALTER TABLE public.is_instandhaltung OWNER TO postgres;
CREATE TABLE public.is_ismaschine (
    i_id integer NOT NULL,
    anlage_i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.is_ismaschine OWNER TO postgres;
CREATE TABLE public.is_kategorie (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.is_kategorie OWNER TO postgres;
CREATE TABLE public.is_standort (
    i_id integer NOT NULL,
    instandhaltung_i_id integer NOT NULL,
    partner_i_id integer NOT NULL,
    b_versteckt smallint NOT NULL,
    ansprechpartner_i_id integer,
    x_bemerkung text,
    c_dokumentenlink character varying(300),
    auftrag_i_id integer NOT NULL
);
ALTER TABLE public.is_standort OWNER TO postgres;
CREATE TABLE public.is_standorttechniker (
    i_id integer NOT NULL,
    standort_i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    b_verantwortlicher smallint NOT NULL
);
ALTER TABLE public.is_standorttechniker OWNER TO postgres;
CREATE TABLE public.is_wartungsliste (
    i_id integer NOT NULL,
    geraet_i_id integer NOT NULL,
    artikel_i_id integer,
    c_bez character varying(80),
    n_menge numeric(17,6) NOT NULL,
    b_wartungsmaterial smallint NOT NULL,
    b_verrechenbar smallint NOT NULL,
    x_bemerkung text,
    c_veraltet character varying(40),
    t_veraltet timestamp without time zone,
    personal_i_id_veraltet integer,
    t_personal_veraltet timestamp without time zone,
    i_sort integer NOT NULL
);
ALTER TABLE public.is_wartungsliste OWNER TO postgres;
CREATE TABLE public.is_wartungsschritte (
    i_id integer NOT NULL,
    geraet_i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    l_dauer bigint NOT NULL,
    t_abdurchfuehren timestamp without time zone NOT NULL,
    auftragwiederholungsintervall_c_nr character varying(40) NOT NULL,
    tagesart_i_id integer,
    i_sort integer NOT NULL,
    personalgruppe_i_id integer NOT NULL,
    c_bemerkung character varying(80),
    lieferant_i_id integer,
    c_bemerkunglieferant character varying(80)
);
ALTER TABLE public.is_wartungsschritte OWNER TO postgres;
CREATE TABLE public.iv_inserat (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_nr character varying(15) NOT NULL,
    t_belegdatum timestamp without time zone NOT NULL,
    f_kd_rabatt double precision NOT NULL,
    f_kd_zusatzrabatt double precision NOT NULL,
    f_kd_nachlass double precision NOT NULL,
    c_bez character varying(300),
    c_rubrik character varying(40),
    c_rubrik2 character varying(40),
    c_stichwort character varying(40),
    c_stichwort2 character varying(40),
    c_medium character varying(80),
    x_anhang text,
    lieferant_i_id integer NOT NULL,
    ansprechpartner_i_id_lieferant integer,
    f_lf_rabatt double precision NOT NULL,
    f_lf_zusatzrabatt double precision NOT NULL,
    f_lf_nachlass double precision NOT NULL,
    x_anhang_lf text,
    t_termin timestamp without time zone NOT NULL,
    artikel_i_id_inseratart integer NOT NULL,
    status_c_nr character(15) NOT NULL,
    personal_i_id_vertreter integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    n_nettoeinzelpreis_ek numeric(17,6) NOT NULL,
    n_nettoeinzelpreis_vk numeric(17,6) NOT NULL,
    t_erschienen timestamp without time zone,
    personal_i_id_erschienen integer,
    t_verrechnen timestamp without time zone,
    personal_i_id_verrechnen integer,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    bestellposition_i_id integer,
    b_druck_bestellung_lf smallint NOT NULL,
    b_druck_bestellung_kd smallint NOT NULL,
    b_druck_rechnung_kd smallint NOT NULL,
    t_termin_bis timestamp without time zone,
    t_manuellerledigt timestamp without time zone,
    personal_i_id_manuellerledigt integer,
    t_gestoppt timestamp without time zone,
    personal_i_id_gestoppt integer,
    c_gestoppt character varying(80),
    b_wertaufteilen smallint NOT NULL,
    personal_i_id_manuellverrechnen integer,
    t_manuellverrechnen timestamp without time zone
);
ALTER TABLE public.iv_inserat OWNER TO postgres;
CREATE TABLE public.iv_inseratartikel (
    i_id integer NOT NULL,
    inserat_i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    n_nettoeinzelpreis_ek numeric(17,6) NOT NULL,
    n_nettoeinzelpreis_vk numeric(17,6) NOT NULL,
    bestellposition_i_id integer
);
ALTER TABLE public.iv_inseratartikel OWNER TO postgres;
CREATE TABLE public.iv_inserater (
    i_id integer NOT NULL,
    inserat_i_id integer NOT NULL,
    eingangsrechnung_i_id integer NOT NULL,
    n_betrag numeric(15,2) NOT NULL,
    c_text character varying(40),
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.iv_inserater OWNER TO postgres;
CREATE TABLE public.iv_inseratrechnung (
    i_id integer NOT NULL,
    inserat_i_id integer NOT NULL,
    kunde_i_id integer NOT NULL,
    ansprechpartner_i_id integer,
    rechnungposition_i_id integer,
    i_sort integer NOT NULL
);
ALTER TABLE public.iv_inseratrechnung OWNER TO postgres;
CREATE TABLE public.iv_inseratrechnungartikel (
    i_id integer NOT NULL,
    inseratrechnung_i_id integer NOT NULL,
    rechnungposition_i_id integer NOT NULL
);
ALTER TABLE public.iv_inseratrechnungartikel OWNER TO postgres;
CREATE TABLE public.kue_bedienerlager (
    i_id integer NOT NULL,
    c_bedienernummer character varying(80) NOT NULL,
    lager_i_id integer NOT NULL
);
ALTER TABLE public.kue_bedienerlager OWNER TO postgres;
CREATE TABLE public.kue_kassaartikel (
    i_id integer NOT NULL,
    c_artikelnummerkassa character varying(80) NOT NULL,
    c_bez character varying(80) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.kue_kassaartikel OWNER TO postgres;
CREATE TABLE public.kue_kassaimport (
    i_id integer NOT NULL,
    kunde_i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    n_preis numeric(17,6) NOT NULL,
    t_import timestamp without time zone NOT NULL,
    t_kassa timestamp without time zone NOT NULL,
    speiseplan_i_id integer
);
ALTER TABLE public.kue_kassaimport OWNER TO postgres;
CREATE TABLE public.kue_kdc100log (
    i_id integer NOT NULL,
    c_seriennummer character varying(40) NOT NULL,
    c_barcode character varying(80) NOT NULL,
    c_kommentar character varying(300) NOT NULL,
    t_buchungszeit timestamp without time zone NOT NULL,
    t_stiftzeit timestamp without time zone NOT NULL,
    c_art character varying(40) NOT NULL
);
ALTER TABLE public.kue_kdc100log OWNER TO postgres;
CREATE TABLE public.kue_kuecheumrechnung (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    c_artikelnummerkassa character varying(80) NOT NULL,
    n_faktor numeric(17,6) NOT NULL
);
ALTER TABLE public.kue_kuecheumrechnung OWNER TO postgres;
CREATE TABLE public.kue_speiseplan (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    t_datum timestamp without time zone NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    stueckliste_i_id integer NOT NULL,
    kassaartikel_i_id integer NOT NULL,
    fertigungsgruppe_i_id integer NOT NULL
);
ALTER TABLE public.kue_speiseplan OWNER TO postgres;
CREATE TABLE public.kue_speiseplanposition (
    i_id integer NOT NULL,
    speiseplan_i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL
);
ALTER TABLE public.kue_speiseplanposition OWNER TO postgres;
CREATE TABLE public.kue_tageslos (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    kostenstelle_i_id integer NOT NULL,
    lager_i_id_abbuchung integer NOT NULL,
    b_sofortverbrauch smallint NOT NULL
);
ALTER TABLE public.kue_tageslos OWNER TO postgres;
CREATE TABLE public.lp_anwender (
    i_id integer NOT NULL,
    mandant_c_nr_hauptmandant character varying(3),
    i_buildnummerdb integer NOT NULL,
    c_versiondb character varying(50) NOT NULL,
    i_buildnummerclientvon integer NOT NULL,
    i_buildnummerclientbis integer NOT NULL,
    i_buildnummerservervon integer NOT NULL,
    i_buildnummerserverbis integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer,
    t_ablauf timestamp without time zone,
    o_code bytea,
    o_hash bytea,
    o_hintergrund bytea,
    t_subscription timestamp without time zone,
    i_serverid integer
);
ALTER TABLE public.lp_anwender OWNER TO postgres;
CREATE TABLE public.lp_arbeitsplatz (
    i_id integer NOT NULL,
    c_standort character varying(80),
    c_bemerkung character varying(80),
    c_pcname character varying(255) NOT NULL,
    c_typ character varying(40),
    c_geraetecode character varying(80),
    o_hash bytea,
    o_code bytea
);
ALTER TABLE public.lp_arbeitsplatz OWNER TO postgres;
CREATE TABLE public.lp_arbeitsplatzkonfiguration (
    arbeitsplatz_i_id integer NOT NULL,
    c_system text,
    c_benutzerdefiniert text
);
ALTER TABLE public.lp_arbeitsplatzkonfiguration OWNER TO postgres;
CREATE TABLE public.lp_arbeitsplatzparameter (
    i_id integer NOT NULL,
    arbeitsplatz_i_id integer NOT NULL,
    parameter_c_nr character varying(80) NOT NULL,
    c_wert character varying(100)
);
ALTER TABLE public.lp_arbeitsplatzparameter OWNER TO postgres;
CREATE TABLE public.lp_arbeitsplatztyp (
    c_nr character varying(40) NOT NULL
);
ALTER TABLE public.lp_arbeitsplatztyp OWNER TO postgres;
CREATE TABLE public.lp_automatikjobs (
    i_id integer NOT NULL,
    mandant_c_nr character varying(100),
    c_name character varying(100),
    c_beschreibung character varying(500),
    b_active integer,
    d_lastperformed timestamp without time zone,
    d_nextperform timestamp without time zone,
    i_intervall integer,
    b_monthjob integer,
    b_performonnonworkingdays integer,
    i_sort integer,
    i_automatikjobtype_iid integer,
    i_scheduler integer NOT NULL
);
ALTER TABLE public.lp_automatikjobs OWNER TO postgres;
CREATE TABLE public.lp_automatikjobtype (
    i_id integer NOT NULL,
    c_jobtype character varying(100),
    c_beschreibung character varying(400)
);
ALTER TABLE public.lp_automatikjobtype OWNER TO postgres;
CREATE TABLE public.lp_automatiktimer (
    i_id integer NOT NULL,
    b_enabled integer,
    t_timetoperform time without time zone
);
ALTER TABLE public.lp_automatiktimer OWNER TO postgres;
CREATE TABLE public.lp_belegart (
    c_nr character(15) NOT NULL,
    i_sort integer NOT NULL,
    c_kbez character(2) NOT NULL,
    i_standarderledigungszeitintagen integer NOT NULL
);
ALTER TABLE public.lp_belegart OWNER TO postgres;
CREATE TABLE public.lp_belegartdokument (
    i_id integer NOT NULL,
    belegart_c_nr character(15) NOT NULL,
    i_belegartid integer NOT NULL,
    dokument_i_id integer NOT NULL,
    i_sort integer NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL
);
ALTER TABLE public.lp_belegartdokument OWNER TO postgres;
CREATE TABLE public.lp_belegartmedia (
    i_id integer NOT NULL,
    usecase_id integer NOT NULL,
    i_key integer NOT NULL,
    i_sort integer NOT NULL,
    c_bez character varying(300),
    datenformat_c_nr character varying(40) NOT NULL,
    o_media bytea,
    x_text text,
    i_ausrichtung integer NOT NULL,
    c_dateiname character varying(300)
);
ALTER TABLE public.lp_belegartmedia OWNER TO postgres;
CREATE TABLE public.lp_belegartspr (
    locale_c_nr character(10) NOT NULL,
    belegart_c_nr character(15) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.lp_belegartspr OWNER TO postgres;
CREATE TABLE public.lp_datenformat (
    c_nr character varying(40) NOT NULL,
    i_sort integer NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.lp_datenformat OWNER TO postgres;
CREATE TABLE public.lp_direkthilfe (
    i_id integer NOT NULL,
    c_token character varying(200) NOT NULL,
    c_text text NOT NULL,
    locale_c_nr character(2) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    b_anwender smallint NOT NULL
);
ALTER TABLE public.lp_direkthilfe OWNER TO postgres;
CREATE TABLE public.lp_dokument (
    i_id integer NOT NULL,
    o_inhalt bytea NOT NULL,
    datenformat_c_nr character varying(40),
    c_dateiname character varying(120),
    c_bez character varying(80),
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL
);
ALTER TABLE public.lp_dokument OWNER TO postgres;
CREATE TABLE public.lp_dokumentbelegart (
    mandant_c_nr character varying(3) NOT NULL,
    c_nr character varying(80) NOT NULL
);
ALTER TABLE public.lp_dokumentbelegart OWNER TO postgres;
CREATE TABLE public.lp_dokumentenlink (
    i_id integer NOT NULL,
    belegart_c_nr character(15) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_basispfad character varying(300) NOT NULL,
    c_ordner character varying(80),
    c_menuetext character varying(40) NOT NULL,
    b_pfadabsolut smallint NOT NULL,
    b_url smallint NOT NULL,
    b_pfad_aus_arbeitsplatzparameter smallint NOT NULL,
    b_titel smallint NOT NULL,
    recht_c_nr character(100)
);
ALTER TABLE public.lp_dokumentenlink OWNER TO postgres;
CREATE TABLE public.lp_dokumentenlinkbeleg (
    i_id integer NOT NULL,
    i_id_belegart integer NOT NULL,
    dokumentenlink_i_id integer NOT NULL,
    c_pfad character varying(300)
);
ALTER TABLE public.lp_dokumentenlinkbeleg OWNER TO postgres;
CREATE TABLE public.lp_dokumentgruppierung (
    mandant_c_nr character varying(3) NOT NULL,
    c_nr character varying(80) NOT NULL
);
ALTER TABLE public.lp_dokumentgruppierung OWNER TO postgres;
CREATE TABLE public.lp_dokumentnichtarchiviert (
    mandant_c_nr character varying(3) NOT NULL,
    c_reportname character varying(80) NOT NULL
);
ALTER TABLE public.lp_dokumentnichtarchiviert OWNER TO postgres;
CREATE TABLE public.lp_dokumentschlagwort (
    i_id integer NOT NULL,
    dokument_i_id integer NOT NULL,
    c_schlagwort character varying(80) NOT NULL
);
ALTER TABLE public.lp_dokumentschlagwort OWNER TO postgres;
CREATE TABLE public.lp_editor_base_block (
    i_id integer NOT NULL,
    "row" integer NOT NULL,
    col integer NOT NULL,
    editor_content_id integer NOT NULL
);
ALTER TABLE public.lp_editor_base_block OWNER TO postgres;
CREATE TABLE public.lp_editor_content (
    i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL
);
ALTER TABLE public.lp_editor_content OWNER TO postgres;
CREATE TABLE public.lp_editor_image_block (
    i_id integer NOT NULL,
    lp_image_i_id integer NOT NULL,
    width double precision NOT NULL,
    height double precision NOT NULL,
    alignment character varying(255) NOT NULL
);
ALTER TABLE public.lp_editor_image_block OWNER TO postgres;
CREATE TABLE public.lp_editor_text_block (
    i_id integer NOT NULL,
    text character varying(3000)
);
ALTER TABLE public.lp_editor_text_block OWNER TO postgres;
CREATE TABLE public.lp_einheit (
    c_nr character(15) NOT NULL,
    i_dimension integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.lp_einheit OWNER TO postgres;
CREATE TABLE public.lp_einheitkonvertierung (
    i_id integer NOT NULL,
    einheit_c_nr_von character(15) NOT NULL,
    einheit_c_nr_zu character(15) NOT NULL,
    n_faktor numeric(17,6) NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.lp_einheitkonvertierung OWNER TO postgres;
CREATE TABLE public.lp_einheitspr (
    einheit_c_nr character(15) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.lp_einheitspr OWNER TO postgres;
CREATE TABLE public.lp_entitylog (
    i_id integer NOT NULL,
    c_entity_key character varying(40) NOT NULL,
    c_operation character(10) NOT NULL,
    entity_i_id character varying(300) NOT NULL,
    c_key character varying(80) NOT NULL,
    c_von character varying(3000),
    c_nach character varying(3000),
    locale_c_nr character(10) NOT NULL,
    personal_i_id integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    c_filter_key character varying(40) NOT NULL,
    filter_i_id character varying(300) NOT NULL
);
ALTER TABLE public.lp_entitylog OWNER TO postgres;
CREATE TABLE public.lp_extraliste (
    i_id integer NOT NULL,
    c_bez character varying(30) NOT NULL,
    belegart_c_nr character(15) NOT NULL,
    x_query text,
    i_dialogbreite integer
);
ALTER TABLE public.lp_extraliste OWNER TO postgres;
CREATE TABLE public.lp_finanzamt (
    partner_i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_steuernummer character varying(15),
    c_referat character varying(40),
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    i_formularnummer integer,
    b_umsatzrunden smallint NOT NULL,
    konto_i_id_ebsachkonten integer,
    konto_i_id_ebdebitoren integer,
    konto_i_id_ebkreditoren integer,
    konto_i_id_anzahlung_erhalt_verr integer,
    konto_i_id_anzahlung_erhalt integer,
    konto_i_id_anzahlung_geleist_verr integer,
    konto_i_id_anzahlung_geleist integer,
    konto_i_id_rc_anzahlung_gegeben_verr integer,
    konto_i_id_rc_anzahlung_gegeben_beza integer,
    konto_i_id_rc_anzahlung_erhalt_verr integer,
    konto_i_id_rc_anzahlung_erhalt_beza integer,
    konto_i_id_gewinnvortrag integer,
    konto_i_id_jahresgewinn integer
);
ALTER TABLE public.lp_finanzamt OWNER TO postgres;
CREATE TABLE public.lp_funktion (
    i_id integer NOT NULL,
    c_nr character varying(40) NOT NULL
);
ALTER TABLE public.lp_funktion OWNER TO postgres;
CREATE TABLE public.lp_funktionspr (
    funktion_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bezeichnung character varying(40) NOT NULL
);
ALTER TABLE public.lp_funktionspr OWNER TO postgres;
CREATE TABLE public.lp_geschaeftsjahr (
    i_geschaeftsjahr integer NOT NULL,
    t_beginndatum timestamp without time zone NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_sperre timestamp without time zone,
    personal_i_id_sperre integer
);
ALTER TABLE public.lp_geschaeftsjahr OWNER TO postgres;
CREATE TABLE public.lp_geschaeftsjahrmandant (
    i_id integer NOT NULL,
    i_geschaeftsjahr integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    t_beginndatum timestamp without time zone NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_sperre timestamp without time zone,
    personal_i_id_sperre integer
);
ALTER TABLE public.lp_geschaeftsjahrmandant OWNER TO postgres;
CREATE TABLE public.lp_hardwareart (
    c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.lp_hardwareart OWNER TO postgres;
CREATE TABLE public.lp_hardwareartspr (
    hardwareart_c_nr character(15) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.lp_hardwareartspr OWNER TO postgres;
CREATE TABLE public.lp_hvmausercount (
    i_id integer NOT NULL,
    systemrolle_i_id integer,
    hvmalizenz_i_id integer NOT NULL,
    hvmaresource character varying(40) NOT NULL,
    t_zeitpunkt timestamp without time zone NOT NULL,
    i_anzahl integer NOT NULL
);
ALTER TABLE public.lp_hvmausercount OWNER TO postgres;
CREATE TABLE public.lp_image (
    i_id integer NOT NULL,
    datenformat_c_nr character varying(40) NOT NULL,
    b_data bytea
);
ALTER TABLE public.lp_image OWNER TO postgres;
CREATE TABLE public.lp_installer (
    i_id integer NOT NULL,
    o_clientpc bytea NOT NULL,
    i_buildnummerclientpc integer NOT NULL
);
ALTER TABLE public.lp_installer OWNER TO postgres;
CREATE TABLE public.lp_internekopie (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    c_pc character varying(256) NOT NULL,
    theclient_c_nr character(120) NOT NULL,
    belegart_c_nr character(15) NOT NULL,
    i_belegiid integer NOT NULL,
    i_belegpositioniid integer NOT NULL,
    positionsart_c_nr character(15) NOT NULL,
    artikel_i_id integer,
    n_menge numeric(17,6),
    c_bez character varying(40),
    c_zusatzbezeichnung character varying(40),
    b_artikelbezeichnunguebersteuert smallint NOT NULL,
    x_text text,
    mediastandard_i_id integer,
    einheit_c_nr character(15),
    n_einzelpreis numeric(17,6),
    n_rabattsatz numeric(17,6),
    n_zusatzrabattsatz numeric(17,6),
    n_nettoeinzelpreis numeric(17,6),
    mwstsatz_i_id integer,
    n_bruttoeinzelpreis numeric(17,6),
    t_positionstermin timestamp without time zone,
    b_mitdrucken smallint NOT NULL,
    f_dimension1 double precision,
    f_dimension2 double precision,
    f_dimension3 double precision,
    c_position character varying(3000),
    c_kommentar character varying(80),
    montageart_i_id integer,
    i_lfdnummer integer,
    t_anlegen timestamp without time zone NOT NULL
);
ALTER TABLE public.lp_internekopie OWNER TO postgres;
CREATE TABLE public.lp_kennung (
    i_id integer NOT NULL,
    c_nr character varying(40) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.lp_kennung OWNER TO postgres;
CREATE TABLE public.lp_kennungspr (
    kennung_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.lp_kennungspr OWNER TO postgres;
CREATE TABLE public.lp_keyvalue (
    c_gruppe character varying(50) NOT NULL,
    c_key character varying(100) NOT NULL,
    c_value character varying(3000) NOT NULL,
    c_datentyp character varying(50) NOT NULL
);
ALTER TABLE public.lp_keyvalue OWNER TO postgres;
CREATE TABLE public.lp_kostenstelle (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_nr character varying(40) NOT NULL,
    b_profitcenter smallint NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    c_bez character varying(80),
    konto_i_id integer,
    x_bemerkung text,
    personal_i_id_anlegen integer,
    personal_i_id_aendern integer,
    c_subdirectory character varying(40),
    b_versteckt smallint NOT NULL,
    lager_i_id_ohneabbuchung integer
);
ALTER TABLE public.lp_kostenstelle OWNER TO postgres;
CREATE TABLE public.lp_kostentraeger (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.lp_kostentraeger OWNER TO postgres;
CREATE TABLE public.lp_landkfzkennzeichen (
    c_lkz character varying(3) NOT NULL,
    c_kfzkennzeichen character varying(3) NOT NULL
);
ALTER TABLE public.lp_landkfzkennzeichen OWNER TO postgres;
CREATE TABLE public.lp_landspr (
    locale_c_nr character(10) NOT NULL,
    land_i_id integer NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.lp_landspr OWNER TO postgres;
CREATE TABLE public.lp_lieferart (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    b_frachtkostenalserledigtverbuchen smallint NOT NULL,
    c_versandort character varying(40),
    mandant_c_nr character varying(3) NOT NULL,
    b_versteckt smallint NOT NULL,
    i_lieferort integer NOT NULL,
    c_extern character varying(40),
    artikel_i_id_versand integer
);
ALTER TABLE public.lp_lieferart OWNER TO postgres;
CREATE TABLE public.lp_lieferartspr (
    lieferart_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bezeichnung character varying(120)
);
ALTER TABLE public.lp_lieferartspr OWNER TO postgres;
CREATE TABLE public.lp_locale (
    c_nr character(10) NOT NULL,
    b_aktiv smallint NOT NULL
);
ALTER TABLE public.lp_locale OWNER TO postgres;
CREATE TABLE public.lp_mailproperty (
    c_nr character varying(80) NOT NULL,
    c_wert character varying(160),
    c_defaultwert character varying(160),
    mandant_c_nr character varying(3) NOT NULL,
    personal_i_id_aendern integer,
    t_aendern timestamp without time zone
);
ALTER TABLE public.lp_mailproperty OWNER TO postgres;
CREATE TABLE public.lp_mandant (
    c_nr character varying(3) NOT NULL,
    c_kbez character varying(25) NOT NULL,
    waehrung_c_nr character(3),
    partner_i_id integer,
    lieferart_i_id_kunde integer,
    spediteur_i_id_kunde integer,
    zahlungsziel_i_id_kunde integer,
    kostenstelle_i_id integer,
    lieferart_i_id_lieferant integer,
    spediteur_i_id_lieferant integer,
    zahlungsziel_i_id_lieferant integer,
    vkpfartikelpreisliste_i_id integer,
    t_anlegen timestamp without time zone,
    personal_i_id_anlegen integer,
    t_aendern timestamp without time zone,
    personal_i_id_aendern integer,
    mwstsatz_i_id_standardinlandmwstsatz integer,
    bankverbindung_i_id_mandant integer,
    mwstsatz_i_id_standardauslandmwstsatz integer,
    partner_i_id_lieferadresse integer NOT NULL,
    i_benutzermax integer,
    b_demo smallint NOT NULL,
    o_hash bytea,
    o_code bytea,
    mwstsatz_i_id_standarddrittlandmwstsatz integer,
    kunde_i_id_stueckliste integer,
    partner_i_id_finanzamt integer,
    i_jahre_rueckdatierbar integer NOT NULL,
    kostenstelle_i_id_fibu integer,
    lager_i_id_ziellager integer,
    c_glaeubiger character varying(40),
    waehrung_c_nr_zusaetzlich character(3),
    verrechnungsmodell_i_id integer,
    zahlungsziel_i_id_anzahlung integer,
    i_maxpersonen integer NOT NULL,
    b_agb_anhang smallint NOT NULL,
    b_agb_angebot smallint NOT NULL,
    b_agb_auftrag smallint NOT NULL,
    b_agb_lieferschein smallint NOT NULL,
    b_agb_rechnung smallint NOT NULL,
    b_agb_anfrage smallint NOT NULL,
    b_agb_bestellung smallint NOT NULL,
    b_preisliste_fuer_neukunde smallint NOT NULL,
    c_intrastatregion character(2)
);
ALTER TABLE public.lp_mandant OWNER TO postgres;
CREATE TABLE public.lp_mandantagbspr (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    o_pdf bytea NOT NULL
);
ALTER TABLE public.lp_mandantagbspr OWNER TO postgres;
CREATE TABLE public.lp_mediaart (
    c_nr character varying(40) NOT NULL
);
ALTER TABLE public.lp_mediaart OWNER TO postgres;
CREATE TABLE public.lp_mediaartspr (
    mediaart_c_nr character varying(40) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.lp_mediaartspr OWNER TO postgres;
CREATE TABLE public.lp_mediastandard (
    i_id integer NOT NULL,
    c_nr character varying(40) NOT NULL,
    o_media bytea NOT NULL,
    datenformat_c_nr character varying(40),
    c_dateiname character varying(260),
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    b_versteckt smallint NOT NULL
);
ALTER TABLE public.lp_mediastandard OWNER TO postgres;
CREATE VIEW public.lp_mediastandard_textsuche AS
 SELECT ms.i_id,
    ms.c_nr,
    ms.o_media,
    ms.datenformat_c_nr,
    ms.c_dateiname,
    ms.personal_i_id_anlegen,
    ms.t_anlegen,
    ms.personal_i_id_aendern,
    ms.t_aendern,
    ms.mandant_c_nr,
    ms.locale_c_nr,
    ms.b_versteckt,
    ( SELECT encode(ms2.o_media, 'escape'::text) AS encode
           FROM public.lp_mediastandard ms2
          WHERE (((ms2.datenformat_c_nr)::text = 'text/html'::text) AND (ms2.i_id = ms.i_id))) AS c_inhalt_o_media
   FROM public.lp_mediastandard ms;
ALTER TABLE public.lp_mediastandard_textsuche OWNER TO postgres;
CREATE TABLE public.lp_modulberechtigung (
    belegart_c_nr character(15) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    o_hash bytea,
    o_code bytea
);
ALTER TABLE public.lp_modulberechtigung OWNER TO postgres;
CREATE TABLE public.lp_mwstsatz (
    i_id integer NOT NULL,
    f_mwstsatz double precision NOT NULL,
    d_gueltigab timestamp without time zone,
    mwstsatzbez_i_id integer NOT NULL
);
ALTER TABLE public.lp_mwstsatz OWNER TO postgres;
CREATE TABLE public.lp_mwstsatzbez (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bezeichnung character varying(40) NOT NULL,
    b_handeingabe smallint,
    c_druckname character varying(30),
    finanzamt_i_id integer
);
ALTER TABLE public.lp_mwstsatzbez OWNER TO postgres;
CREATE TABLE public.lp_mwstsatzcode (
    i_id integer NOT NULL,
    mwstsatz_i_id integer NOT NULL,
    reversechargeart_i_id integer NOT NULL,
    c_steuercode_ar character varying(40),
    c_steuercode_er character varying(40)
);
ALTER TABLE public.lp_mwstsatzcode OWNER TO postgres;
CREATE TABLE public.lp_panel (
    c_nr character(25) NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.lp_panel OWNER TO postgres;
CREATE TABLE public.lp_panelbeschreibung (
    i_id integer NOT NULL,
    panel_c_nr character(25) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_name character varying(120) NOT NULL,
    c_typ character(20) NOT NULL,
    c_tokeninresourcebundle character varying(3000),
    i_gridx integer NOT NULL,
    i_gridy integer NOT NULL,
    i_gridwidth integer NOT NULL,
    i_gridheigth integer NOT NULL,
    c_fill character varying(40) NOT NULL,
    c_anchor character varying(40) NOT NULL,
    i_insetsleft integer NOT NULL,
    i_insetsright integer NOT NULL,
    i_insetstop integer NOT NULL,
    i_insetsbottom integer NOT NULL,
    i_ipadx integer NOT NULL,
    i_ipady integer NOT NULL,
    b_mandatory smallint NOT NULL,
    f_weightx double precision NOT NULL,
    f_weighty double precision NOT NULL,
    c_druckname character varying(50),
    artgru_i_id integer,
    partnerklasse_i_id integer,
    c_default character varying(40),
    kostenstelle_i_id integer,
    bereich_i_id integer,
    projekttyp_c_nr character varying(30),
    b_ueberschrift smallint NOT NULL
);
ALTER TABLE public.lp_panelbeschreibung OWNER TO postgres;
CREATE TABLE public.lp_paneldaten (
    i_id integer NOT NULL,
    panel_c_nr character(25) NOT NULL,
    panelbeschreibung_i_id integer NOT NULL,
    c_key character varying(80) NOT NULL,
    c_datentypkey character varying(50) NOT NULL,
    x_inhalt character varying(3000),
    o_inhalt bytea
);
ALTER TABLE public.lp_paneldaten OWNER TO postgres;
CREATE TABLE public.lp_panelsperren (
    i_id integer NOT NULL,
    belegart_c_nr character(15) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_ressource_unten character varying(300) NOT NULL,
    c_ressource_oben character varying(300) NOT NULL
);
ALTER TABLE public.lp_panelsperren OWNER TO postgres;
CREATE TABLE public.lp_parameter (
    c_nr character varying(80) NOT NULL,
    c_bemerkung character varying(3000),
    c_datentyp character varying(50) NOT NULL
);
ALTER TABLE public.lp_parameter OWNER TO postgres;
CREATE TABLE public.lp_parameteranwender (
    c_nr character(50) NOT NULL,
    c_kategorie character varying(40) NOT NULL,
    c_wert character varying(3000) NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    c_bemerkungsmall character varying(70),
    c_bemerkunglarge character varying(3000),
    c_datentyp character varying(50)
);
ALTER TABLE public.lp_parameteranwender OWNER TO postgres;
CREATE TABLE public.lp_parametermandant (
    c_nr character varying(50) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_kategorie character varying(40) NOT NULL,
    c_wert character varying(3000) NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    c_bemerkungsmall character varying(300),
    c_bemerkunglarge character varying(3000),
    c_datentyp character varying(50)
);
ALTER TABLE public.lp_parametermandant OWNER TO postgres;
CREATE TABLE public.lp_parametermandantgueltigab (
    c_nr character varying(50) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_kategorie character varying(40) NOT NULL,
    c_wert character varying(100) NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL
);
ALTER TABLE public.lp_parametermandantgueltigab OWNER TO postgres;
CREATE TABLE public.lp_positionsart (
    c_nr character(15) NOT NULL
);
ALTER TABLE public.lp_positionsart OWNER TO postgres;
CREATE TABLE public.lp_positionsartspr (
    positionsart_c_nr character(15) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.lp_positionsartspr OWNER TO postgres;
CREATE TABLE public.lp_primarykey (
    i_index integer NOT NULL,
    c_name character varying(40) NOT NULL
);
ALTER TABLE public.lp_primarykey OWNER TO postgres;
CREATE TABLE public.lp_primarykey_belegnr (
    i_geschaeftsjahr integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_nametabelle character varying(40) NOT NULL,
    c_namebeleg character varying(40) NOT NULL,
    i_index integer NOT NULL
);
ALTER TABLE public.lp_primarykey_belegnr OWNER TO postgres;
CREATE TABLE public.lp_protokoll (
    i_id integer NOT NULL,
    c_art character varying(15) NOT NULL,
    c_typ character varying(40) NOT NULL,
    c_text character varying(3000) NOT NULL,
    c_langtext character varying(3000),
    t_quelle timestamp without time zone,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL
);
ALTER TABLE public.lp_protokoll OWNER TO postgres;
CREATE TABLE public.lp_rechtsform (
    i_id integer NOT NULL,
    c_bez character varying(40) NOT NULL,
    b_juristisch smallint NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.lp_rechtsform OWNER TO postgres;
CREATE TABLE public.lp_reportkonf (
    i_id integer NOT NULL,
    standarddrucker_i_id integer NOT NULL,
    c_komponentenname character varying(80) NOT NULL,
    c_komponententyp character varying(80) NOT NULL,
    c_key character varying(200) NOT NULL,
    c_typ character varying(200)
);
ALTER TABLE public.lp_reportkonf OWNER TO postgres;
CREATE TABLE public.lp_reportvariante (
    i_id integer NOT NULL,
    c_reportname character varying(80) NOT NULL,
    c_reportnamevariante character varying(80) NOT NULL,
    c_ressource character varying(80) NOT NULL
);
ALTER TABLE public.lp_reportvariante OWNER TO postgres;
CREATE TABLE public.lp_spediteur (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_namedesspediteurs character varying(40) NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    b_versteckt smallint NOT NULL,
    c_email character varying(80),
    partner_i_id integer,
    ansprechpartner_i_id integer,
    c_verkehrszweig character(3)
);
ALTER TABLE public.lp_spediteur OWNER TO postgres;
CREATE TABLE public.lp_standarddrucker (
    i_id integer NOT NULL,
    c_pc character varying(100) NOT NULL,
    c_reportname character varying(180) NOT NULL,
    c_drucker character varying(150) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    reportvariante_i_id integer,
    b_standard smallint NOT NULL,
    c_drucker_kopien character varying(150)
);
ALTER TABLE public.lp_standarddrucker OWNER TO postgres;
CREATE TABLE public.lp_status (
    c_nr character(15) NOT NULL,
    o_bild bytea
);
ALTER TABLE public.lp_status OWNER TO postgres;
CREATE TABLE public.lp_statusspr (
    status_c_nr character(15) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.lp_statusspr OWNER TO postgres;
CREATE TABLE public.lp_text (
    c_token character varying(100) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_text character varying(200) NOT NULL
);
ALTER TABLE public.lp_text OWNER TO postgres;
CREATE TABLE public.lp_theclient (
    c_nr character(120) NOT NULL,
    c_benutzername character(120) NOT NULL,
    c_kennwort character varying(40),
    c_mandant character(3) NOT NULL,
    c_mandantwaehrung character(3) NOT NULL,
    i_personal integer NOT NULL,
    c_uilocale character(10) NOT NULL,
    c_mandantenlocale character(10) NOT NULL,
    c_konzernlocale character(10) NOT NULL,
    i_status integer,
    t_loggedin timestamp without time zone NOT NULL,
    t_loggedout timestamp without time zone,
    systemrolle_i_id integer,
    i_rmiport integer,
    hvmalizenz_i_id integer,
    hvmaresource character varying(40)
);
ALTER TABLE public.lp_theclient OWNER TO postgres;
CREATE TABLE public.lp_thejudge (
    c_wer character varying(50) NOT NULL,
    c_was character varying(200) NOT NULL,
    c_usernr character(120) NOT NULL,
    t_wann timestamp without time zone NOT NULL,
    personal_i_id_locker integer NOT NULL
);
ALTER TABLE public.lp_thejudge OWNER TO postgres;
CREATE TABLE public.lp_usercount (
    t_zeitpunkt timestamp without time zone NOT NULL,
    i_anzahl integer NOT NULL,
    systemrolle_i_id integer,
    i_id integer NOT NULL
);
ALTER TABLE public.lp_usercount OWNER TO postgres;
CREATE TABLE public.lp_versandanhang (
    i_id integer NOT NULL,
    versandauftrag_i_id integer,
    c_dateiname character varying(260),
    o_inhalt bytea
);
ALTER TABLE public.lp_versandanhang OWNER TO postgres;
CREATE TABLE public.lp_versandauftrag (
    i_id integer NOT NULL,
    c_empfaenger character varying(300) NOT NULL,
    c_ccempfaenger character varying(300),
    c_betreff character varying(100),
    c_text text,
    c_absenderadresse character varying(100),
    c_dateiname character varying(260),
    t_sendezeitpunktwunsch timestamp without time zone NOT NULL,
    t_sendezeitpunkt timestamp without time zone,
    personal_i_id integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    partner_i_id_empfaenger integer,
    partner_i_id_sender integer,
    belegart_c_nr character(15),
    i_belegiid integer,
    status_c_nr character(15),
    c_statustext character varying(1000),
    o_inhalt bytea,
    c_jobid character varying(40),
    b_empfangsbestaetigung smallint NOT NULL,
    c_bccempfaenger character varying(300),
    o_versandinfo bytea,
    o_message bytea
);
ALTER TABLE public.lp_versandauftrag OWNER TO postgres;
CREATE TABLE public.lp_versandstatus (
    status_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.lp_versandstatus OWNER TO postgres;
CREATE TABLE public.lp_versandweg (
    i_id integer NOT NULL,
    c_nr character(30) NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.lp_versandweg OWNER TO postgres;
CREATE TABLE public.lp_versandwegberechtigung (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    o_code bytea,
    o_hash bytea
);
ALTER TABLE public.lp_versandwegberechtigung OWNER TO postgres;
CREATE TABLE public.lp_versandwegcc (
    i_id integer NOT NULL,
    c_endpunkt character varying(300) NOT NULL
);
ALTER TABLE public.lp_versandwegcc OWNER TO postgres;
CREATE TABLE public.lp_versandwegccpartner (
    i_id integer NOT NULL,
    partner_i_id integer NOT NULL,
    versandweg_i_id integer NOT NULL,
    c_kundennummer character varying(300) NOT NULL,
    c_kennwort character varying(300) NOT NULL,
    c_keystore character varying(300) NOT NULL,
    c_keystorekennwort character varying(40) NOT NULL,
    i_soko_adresstyp integer NOT NULL
);
ALTER TABLE public.lp_versandwegccpartner OWNER TO postgres;
CREATE TABLE public.lp_versandwegpartner (
    i_id integer NOT NULL,
    partner_i_id integer NOT NULL,
    versandweg_i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.lp_versandwegpartner OWNER TO postgres;
CREATE TABLE public.lp_versandwegpartnercc (
    i_id integer NOT NULL,
    c_kundennummer character varying(300) NOT NULL,
    c_kennwort character varying(300) NOT NULL,
    c_keystore character varying(300) NOT NULL,
    c_keystorekennwort character varying(40) NOT NULL,
    i_soko_adresstyp integer NOT NULL
);
ALTER TABLE public.lp_versandwegpartnercc OWNER TO postgres;
CREATE TABLE public.lp_versandwegpartnerdesadv (
    i_id integer NOT NULL,
    c_endpunkt character varying(300) NOT NULL,
    c_benutzer character varying(80) NOT NULL,
    c_kennwort character varying(80) NOT NULL,
    c_unbempfaenger character varying(35) NOT NULL
);
ALTER TABLE public.lp_versandwegpartnerdesadv OWNER TO postgres;
CREATE TABLE public.lp_versandwegpartneredi4all (
    i_id integer NOT NULL,
    c_exportpfad character varying(300) NOT NULL,
    c_00e character varying(80),
    c_094 character varying(80)
);
ALTER TABLE public.lp_versandwegpartneredi4all OWNER TO postgres;
CREATE TABLE public.lp_versandwegpartnerlinienabruf (
    i_id integer NOT NULL,
    c_exportpfad character varying(300) NOT NULL,
    c_kopfzeile character varying(300),
    c_datenzeile character varying(300) NOT NULL,
    c_bestellnummer character varying(80)
);
ALTER TABLE public.lp_versandwegpartnerlinienabruf OWNER TO postgres;
CREATE TABLE public.lp_versandwegpartnerordrsp (
    i_id integer NOT NULL,
    c_endpunkt character varying(300) NOT NULL,
    c_benutzer character varying(80) NOT NULL,
    c_kennwort character varying(80) NOT NULL,
    c_unbempfaenger character varying(35) NOT NULL
);
ALTER TABLE public.lp_versandwegpartnerordrsp OWNER TO postgres;
CREATE TABLE public.lp_waehrung (
    c_nr character(3) NOT NULL,
    c_kommentar character varying(80),
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.lp_waehrung OWNER TO postgres;
CREATE TABLE public.lp_wechselkurs (
    waehrung_c_nr_von character(3) NOT NULL,
    waehrung_c_nr_zu character(3) NOT NULL,
    t_datum timestamp without time zone NOT NULL,
    n_kurs numeric(16,13),
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.lp_wechselkurs OWNER TO postgres;
CREATE TABLE public.lp_woerterbuch (
    id integer NOT NULL,
    locale character varying(10) NOT NULL,
    wort character varying NOT NULL
);
ALTER TABLE public.lp_woerterbuch OWNER TO postgres;
CREATE TABLE public.lp_zahlungsziel (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(40) NOT NULL,
    i_anzahlzieltagefuernetto integer NOT NULL,
    n_skontoprozentsatz1 numeric(15,4),
    i_skontoanzahltage1 integer,
    n_skontoprozentsatz2 numeric(15,4),
    i_skontoanzahltage2 integer,
    b_versteckt smallint NOT NULL,
    b_inzahlungsvorschlagberuecksichtigen smallint NOT NULL,
    b_stichtag smallint NOT NULL,
    i_stichtag integer,
    i_folgemonat integer,
    b_stichtag_monatsletzter smallint NOT NULL,
    b_lastschrift smallint NOT NULL,
    i_folgemonat_skontotage1 integer,
    i_folgemonat_skontotage2 integer,
    n_anzahlung_prozent numeric(15,4)
);
ALTER TABLE public.lp_zahlungsziel OWNER TO postgres;
CREATE TABLE public.lp_zahlungszielspr (
    zahlungsziel_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bezeichnung character varying(120)
);
ALTER TABLE public.lp_zahlungszielspr OWNER TO postgres;
CREATE TABLE public.lp_zusatzfunktion (
    c_nr character(30) NOT NULL
);
ALTER TABLE public.lp_zusatzfunktion OWNER TO postgres;
CREATE TABLE public.lp_zusatzfunktionberechtigung (
    zusatzfunktion_c_nr character(30) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    o_hash bytea,
    o_code bytea
);
ALTER TABLE public.lp_zusatzfunktionberechtigung OWNER TO postgres;
CREATE TABLE public.ls_ausliefervorschlag (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    belegart_c_nr character(15) NOT NULL,
    i_belegartid integer NOT NULL,
    i_belegartpositionid integer NOT NULL,
    artikel_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    t_ausliefertermin timestamp without time zone NOT NULL,
    kunde_i_id integer NOT NULL,
    kunde_i_id_lieferadresse integer NOT NULL,
    n_verfuegbar numeric(17,6) NOT NULL
);
ALTER TABLE public.ls_ausliefervorschlag OWNER TO postgres;
CREATE TABLE public.ls_begruendung (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.ls_begruendung OWNER TO postgres;
CREATE TABLE public.ls_lieferscheinart (
    c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.ls_lieferscheinart OWNER TO postgres;
CREATE TABLE public.ls_lieferscheinartspr (
    locale_c_nr character(10) NOT NULL,
    lieferscheinart_c_nr character(15) NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.ls_lieferscheinartspr OWNER TO postgres;
CREATE TABLE public.ls_lieferscheinpositionart (
    positionsart_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL,
    b_versteckt smallint NOT NULL
);
ALTER TABLE public.ls_lieferscheinpositionart OWNER TO postgres;
CREATE TABLE public.ls_lieferscheinstatus (
    status_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.ls_lieferscheinstatus OWNER TO postgres;
CREATE TABLE public.ls_lieferscheintext (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    mediaart_c_nr character varying(40) NOT NULL,
    x_textinhalt text NOT NULL
);
ALTER TABLE public.ls_lieferscheintext OWNER TO postgres;
CREATE TABLE public.ls_packstueck (
    i_id integer NOT NULL,
    l_nummer bigint NOT NULL,
    los_i_id integer,
    losablieferung_i_id integer,
    lieferschein_i_id integer,
    lieferscheinposition_i_id integer,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    forecastposition_i_id integer
);
ALTER TABLE public.ls_packstueck OWNER TO postgres;
CREATE VIEW public.ls_textsuche AS
 SELECT DISTINCT 'A '::text AS c_typ,
    g.i_id,
    g.c_nr,
    (((COALESCE(a.c_bez, ''::character varying))::text || (COALESCE(a.c_zbez, ''::character varying))::text) || (COALESCE(a.c_zbez2, ''::character varying))::text) AS c_suche
   FROM ((public.ls_lieferschein g
     JOIN public.ls_lieferscheinposition p ON ((g.i_id = p.lieferschein_i_id)))
     JOIN public.ww_artikelspr a ON ((a.artikel_i_id = p.artikel_i_id)))
UNION
 SELECT DISTINCT 'P '::text AS c_typ,
    g.i_id,
    g.c_nr,
    ((COALESCE(p.c_bez, ''::character varying))::text || (COALESCE(p.c_zbez, ''::character varying))::text) AS c_suche
   FROM (public.ls_lieferschein g
     JOIN public.ls_lieferscheinposition p ON ((g.i_id = p.lieferschein_i_id)))
UNION
 SELECT DISTINCT 'PT '::text AS c_typ,
    g.i_id,
    g.c_nr,
    (p.x_textinhalt)::character varying(4000) AS c_suche
   FROM (public.ls_lieferschein g
     JOIN public.ls_lieferscheinposition p ON ((g.i_id = p.lieferschein_i_id)))
UNION
 SELECT DISTINCT 'LS'::text AS c_typ,
    ls.i_id,
    ls.c_nr,
    (((COALESCE(ls.c_bez, ''::character varying))::text || ' '::text) || (COALESCE(ls.c_kommission, ''::character varying))::text) AS c_suche
   FROM public.ls_lieferschein ls
UNION
 SELECT DISTINCT 'ID'::text AS c_typ,
    g.i_id,
    g.c_nr,
    ((a.c_nr)::text || (COALESCE(a.c_referenznr, ''::character varying))::text) AS c_suche
   FROM ((public.ls_lieferschein g
     JOIN public.ls_lieferscheinposition p ON ((g.i_id = p.lieferschein_i_id)))
     JOIN public.ww_artikel a ON ((a.i_id = p.artikel_i_id)))
UNION
 SELECT DISTINCT 'KD'::text AS c_typ,
    g.i_id,
    g.c_nr,
    ((COALESCE(p.c_name1nachnamefirmazeile1, ''::character varying))::text || (COALESCE(p.c_name2vornamefirmazeile2, ''::character varying))::text) AS c_suche
   FROM ((public.ls_lieferschein g
     JOIN public.part_kunde k ON ((k.i_id = g.kunde_i_id_rechnungsadresse)))
     JOIN public.part_partner p ON ((p.i_id = k.partner_i_id)))
UNION
 SELECT DISTINCT 'LPO'::text AS c_typ,
    g.i_id,
    g.c_nr,
    (((((((l.c_lkz)::text || '-'::text) || (lpo.c_plz)::text) || ' '::text) || (o.c_name)::text) || ' '::text) || (p.c_strasse)::text) AS c_suche
   FROM (((((public.lp_landplzort lpo
     JOIN public.part_partner p ON ((lpo.i_id = p.landplzort_i_id)))
     JOIN public.lp_land l ON ((lpo.land_i_id = l.i_id)))
     JOIN public.lp_ort o ON ((lpo.ort_i_id = o.i_id)))
     JOIN public.part_lieferant k ON ((p.i_id = k.partner_i_id)))
     JOIN public.ls_lieferschein g ON ((k.i_id = g.kunde_i_id_rechnungsadresse)));
ALTER TABLE public.ls_textsuche OWNER TO postgres;
CREATE TABLE public.ls_verkettet (
    i_id integer NOT NULL,
    lieferschein_i_id integer NOT NULL,
    lieferschein_i_id_verkettet integer NOT NULL
);
ALTER TABLE public.ls_verkettet OWNER TO postgres;
CREATE TABLE public.media_emailattachment (
    i_id integer NOT NULL,
    media_i_id integer NOT NULL,
    c_uuid character varying(40) NOT NULL,
    c_name character varying(1024) NOT NULL,
    c_description character varying(1024),
    c_mimetype character varying(1024),
    i_size integer NOT NULL
);
ALTER TABLE public.media_emailattachment OWNER TO postgres;
CREATE TABLE public.media_emailmeta (
    i_id integer NOT NULL,
    c_from character varying(1024) NOT NULL,
    c_to character varying(1024) NOT NULL,
    c_cc character varying(1024),
    c_bcc character varying(1024),
    c_subject character varying(1024),
    reply_media_i_id integer,
    t_emaildate timestamp without time zone NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    b_priority smallint NOT NULL,
    b_html smallint NOT NULL,
    x_content character varying(2048)
);
ALTER TABLE public.media_emailmeta OWNER TO postgres;
CREATE TABLE public.media_inbox (
    i_id integer NOT NULL,
    media_i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    status_c_nr character(15) NOT NULL,
    b_versteckt smallint NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_gelesen timestamp without time zone
);
ALTER TABLE public.media_inbox OWNER TO postgres;
CREATE TABLE public.media_store (
    i_id integer NOT NULL,
    c_uuid character varying(40) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    b_typ smallint NOT NULL,
    b_online smallint NOT NULL
);
ALTER TABLE public.media_store OWNER TO postgres;
CREATE TABLE public.media_storebeleg (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    media_i_id integer NOT NULL,
    c_belegartnr character(15) NOT NULL,
    beleg_i_id integer NOT NULL,
    belegposition_i_id integer,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_gelesen timestamp without time zone
);
ALTER TABLE public.media_storebeleg OWNER TO postgres;
CREATE TABLE public.part_anrede (
    c_nr character(15) NOT NULL
);
ALTER TABLE public.part_anrede OWNER TO postgres;
CREATE TABLE public.part_anredespr (
    anrede_c_nr character(15) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.part_anredespr OWNER TO postgres;
CREATE TABLE public.part_ansprechpartner (
    i_id integer NOT NULL,
    partner_i_id integer NOT NULL,
    partner_i_id_ansprechpartner integer NOT NULL,
    ansprechpartnerfunktion_i_id integer NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL,
    i_sort integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    x_bemerkung text,
    b_versteckt smallint NOT NULL,
    c_fremdsystemnr character varying(30),
    c_telefon character varying(80),
    c_fax character varying(80),
    c_direktfax character varying(80),
    c_handy character varying(80),
    c_email character varying(300),
    c_abteilung character varying(40),
    c_kennwort character varying(40),
    newslettergrund_i_id integer,
    b_durchwahl smallint NOT NULL,
    c_exchangeid character varying(200),
    t_zuletzt_exportiert timestamp without time zone
);
ALTER TABLE public.part_ansprechpartner OWNER TO postgres;
CREATE TABLE public.part_ansprechpartneradressbuch (
    i_id integer NOT NULL,
    ansprechpartner_i_id integer NOT NULL,
    c_exchangeid character varying(200),
    t_zuletzt_exportiert timestamp without time zone NOT NULL,
    c_emailadresse character varying(80) NOT NULL
);
ALTER TABLE public.part_ansprechpartneradressbuch OWNER TO postgres;
CREATE TABLE public.part_ansprechpartnerfunktion (
    i_id integer NOT NULL,
    c_nr character(15) NOT NULL,
    c_reportname character varying(80)
);
ALTER TABLE public.part_ansprechpartnerfunktion OWNER TO postgres;
CREATE TABLE public.part_ansprechpartnerfunktionspr (
    ansprechpartnerfunktion_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.part_ansprechpartnerfunktionspr OWNER TO postgres;
CREATE TABLE public.part_bank (
    partner_i_id integer NOT NULL,
    c_blz character(11),
    c_bic character varying(11)
);
ALTER TABLE public.part_bank OWNER TO postgres;
CREATE TABLE public.part_beauskunftung (
    i_id integer NOT NULL,
    partner_i_id integer NOT NULL,
    identifikation_i_id integer,
    b_kostenpflichtig smallint NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL
);
ALTER TABLE public.part_beauskunftung OWNER TO postgres;
CREATE TABLE public.part_branche (
    c_nr character varying(120),
    i_id integer NOT NULL
);
ALTER TABLE public.part_branche OWNER TO postgres;
CREATE TABLE public.part_branchespr (
    branche_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(120)
);
ALTER TABLE public.part_branchespr OWNER TO postgres;
CREATE TABLE public.part_dsgvokategorie (
    i_id integer NOT NULL,
    c_nr character varying(40) NOT NULL
);
ALTER TABLE public.part_dsgvokategorie OWNER TO postgres;
CREATE TABLE public.part_dsgvokategoriespr (
    dsgvokategorie_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.part_dsgvokategoriespr OWNER TO postgres;
CREATE TABLE public.part_dsgvotext (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    dsgvokategorie_i_id integer NOT NULL,
    b_kopftext smallint NOT NULL,
    i_sort integer NOT NULL,
    x_inhalt character varying(3000) NOT NULL
);
ALTER TABLE public.part_dsgvotext OWNER TO postgres;
CREATE TABLE public.part_geodaten (
    i_id integer NOT NULL,
    partner_i_id integer NOT NULL,
    n_laengengrad numeric(15,6) NOT NULL,
    n_breitengrad numeric(15,6) NOT NULL
);
ALTER TABLE public.part_geodaten OWNER TO postgres;
CREATE TABLE public.part_identifikation (
    i_id integer NOT NULL,
    c_nr character varying(40) NOT NULL
);
ALTER TABLE public.part_identifikation OWNER TO postgres;
CREATE TABLE public.part_identifikationspr (
    identifikation_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.part_identifikationspr OWNER TO postgres;
CREATE TABLE public.part_kommunikationsart (
    c_nr character(15) NOT NULL
);
ALTER TABLE public.part_kommunikationsart OWNER TO postgres;
CREATE TABLE public.part_kommunikationsartspr (
    kommunikationsart_c_nr character(15) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.part_kommunikationsartspr OWNER TO postgres;
CREATE TABLE public.part_kontakt (
    i_id integer NOT NULL,
    c_titel character varying(80) NOT NULL,
    partner_i_id integer NOT NULL,
    ansprechpartner_i_id integer,
    personal_i_id_zugewiesener integer NOT NULL,
    kontaktart_i_id integer NOT NULL,
    t_kontakt timestamp without time zone NOT NULL,
    t_kontaktbis timestamp without time zone,
    t_wiedervorlage timestamp without time zone,
    x_kommentar text,
    t_erledigt timestamp without time zone,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL
);
ALTER TABLE public.part_kontakt OWNER TO postgres;
CREATE TABLE public.part_kontaktart (
    i_id integer NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.part_kontaktart OWNER TO postgres;
CREATE VIEW public.part_kunde_struktur AS
 SELECT
        CASE
            WHEN (k.partner_i_id_rechnungsadresse IS NULL) THEN 'R'::text
            WHEN (k.partner_i_id_rechnungsadresse IS NOT NULL) THEN 'L'::text
            ELSE NULL::text
        END AS typ,
        CASE
            WHEN (k.partner_i_id = k.partner_i_id_rechnungsadresse) THEN 'X'::text
            ELSE ''::text
        END AS loop,
        CASE
            WHEN (k.partner_i_id_rechnungsadresse IS NOT NULL) THEN k.partner_i_id_rechnungsadresse
            ELSE p.i_id
        END AS partner_i_id_gruppe,
        CASE
            WHEN (k.partner_i_id_rechnungsadresse IS NULL) THEN p.c_name1nachnamefirmazeile1
            WHEN (k.partner_i_id_rechnungsadresse IS NOT NULL) THEN r.c_name1nachnamefirmazeile1
            ELSE NULL::character varying
        END AS rechnungsadresse,
        CASE
            WHEN (k.partner_i_id_rechnungsadresse IS NOT NULL) THEN p.c_name1nachnamefirmazeile1
            ELSE NULL::character varying
        END AS lieferadresse,
    k.i_id AS kunde_i_id,
    p.i_id AS partner_i_id,
    k.partner_i_id_rechnungsadresse AS partner_i_id_rechnungadresse,
    p.i_id,
    p.locale_c_nr_kommunikation,
    p.partnerart_c_nr,
    p.c_kbez,
    p.b_versteckt,
    p.c_name1nachnamefirmazeile1,
    p.t_anlegen,
    p.personal_i_id_anlegen,
    p.t_aendern,
    p.personal_i_id_aendern,
    p.c_name2vornamefirmazeile2,
    p.c_name3vorname2abteilung,
    p.c_strasse,
    p.anrede_c_nr,
    p.landplzort_i_id,
    p.landplzort_i_id_postfach,
    p.c_postfach,
    p.branche_i_id,
    p.partnerklasse_i_id,
    p.partner_i_id_vater,
    p.c_uid,
    p.x_bemerkung,
    p.t_geburtsdatumansprechpartner,
    p.rechtsform_i_id,
    p.partner_i_id_eigentuemer,
    p.c_firmenbuchnr,
    p.c_titel,
    p.c_gerichtsstand,
    p.o_bild,
    p.land_i_id_abweichendesustland,
    p.lager_i_id_ziellager,
    p.c_iln,
    p.c_filialnummer,
    p.c_adressart,
    p.f_gmtversatz,
    p.c_ntitel,
    p.c_eori,
    p.c_telefon,
    p.c_fax,
    p.c_direktfax,
    p.c_handy,
    p.c_email,
    p.c_homepage,
    p.versandweg_i_id,
    p.newslettergrund_i_id
   FROM ((public.part_kunde k
     LEFT JOIN public.part_partner r ON ((r.i_id = k.partner_i_id_rechnungsadresse)))
     JOIN public.part_partner p ON ((p.i_id = k.partner_i_id)));
ALTER TABLE public.part_kunde_struktur OWNER TO postgres;
CREATE TABLE public.part_kundekennung (
    i_id integer NOT NULL,
    kunde_i_id integer NOT NULL,
    kennung_i_id integer NOT NULL,
    c_wert character varying(80) NOT NULL
);
ALTER TABLE public.part_kundekennung OWNER TO postgres;
CREATE TABLE public.part_kundematerial (
    i_id integer NOT NULL,
    kunde_i_id integer NOT NULL,
    material_i_id integer NOT NULL,
    material_i_id_notierung integer NOT NULL,
    n_materialbasis numeric(15,6) NOT NULL,
    b_material_inklusive smallint NOT NULL,
    n_aufschlag_betrag numeric(17,6),
    f_aufschlag_prozent double precision
);
ALTER TABLE public.part_kundematerial OWNER TO postgres;
CREATE TABLE public.part_kundesachbearbeiter (
    i_id integer NOT NULL,
    kunde_i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    funktion_i_id integer,
    t_gueltigab timestamp without time zone NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.part_kundesachbearbeiter OWNER TO postgres;
CREATE TABLE public.part_kundesoko (
    i_id integer NOT NULL,
    kunde_i_id integer NOT NULL,
    artikel_i_id integer,
    artgru_i_id integer,
    t_preisgueltigab timestamp without time zone NOT NULL,
    t_preisgueltigbis timestamp without time zone,
    c_bemerkung character varying(40),
    b_bemerkungdrucken smallint NOT NULL,
    b_rabattsichtbar smallint NOT NULL,
    b_drucken smallint NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    c_kundeartikelnummer character varying(25),
    c_kundeartikelbez character varying(80),
    c_kundeartikelzbez character varying(80),
    b_wirkt_nicht_fuer_preisfindung smallint NOT NULL,
    b_keine_mengenstaffel smallint NOT NULL,
    n_startwert_liefermenge numeric(15,4)
);
ALTER TABLE public.part_kundesoko OWNER TO postgres;
CREATE TABLE public.part_kundesokomengenstaffel (
    i_id integer NOT NULL,
    kundesoko_i_id integer NOT NULL,
    n_menge numeric(15,4) NOT NULL,
    f_artikelstandardrabattsatz double precision NOT NULL,
    n_artikelfixpreis numeric(17,6)
);
ALTER TABLE public.part_kundesokomengenstaffel OWNER TO postgres;
CREATE TABLE public.part_kundespediteur (
    i_id integer NOT NULL,
    kunde_i_id integer NOT NULL,
    spediteur_i_id integer NOT NULL,
    n_gewichtinkg numeric(15,4) NOT NULL,
    c_accounting character varying(80) NOT NULL
);
ALTER TABLE public.part_kundespediteur OWNER TO postgres;
CREATE TABLE public.part_kurzbrief (
    i_id integer NOT NULL,
    partner_i_id integer NOT NULL,
    ansprechpartner_i_id integer,
    c_betreff character varying(80),
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    x_text text,
    belegart_c_nr character(15) NOT NULL,
    b_html smallint NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.part_kurzbrief OWNER TO postgres;
CREATE TABLE public.part_lflfliefergruppe (
    lieferant_i_id integer NOT NULL,
    lfliefergruppe_i_id integer NOT NULL
);
ALTER TABLE public.part_lflfliefergruppe OWNER TO postgres;
CREATE TABLE public.part_lfliefergruppe (
    i_id integer NOT NULL,
    c_nr character varying(30) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.part_lfliefergruppe OWNER TO postgres;
CREATE TABLE public.part_lfliefergruppespr (
    lfliefergruppe_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(30),
    x_text text
);
ALTER TABLE public.part_lfliefergruppespr OWNER TO postgres;
CREATE TABLE public.part_lieferantbeurteilung (
    i_id integer NOT NULL,
    lieferant_i_id integer NOT NULL,
    t_datum timestamp without time zone NOT NULL,
    i_punkte integer NOT NULL,
    b_gesperrt smallint NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    b_manuellgeaendert smallint NOT NULL,
    c_kommentar character varying(80),
    c_klasse character varying(10)
);
ALTER TABLE public.part_lieferantbeurteilung OWNER TO postgres;
CREATE TABLE public.part_liefermengen (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    kunde_i_id_lieferadresse integer NOT NULL,
    t_datum timestamp without time zone NOT NULL,
    n_menge numeric(15,4) NOT NULL,
    c_lstext character varying(80)
);
ALTER TABLE public.part_liefermengen OWNER TO postgres;
CREATE TABLE public.part_newslettergrund (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    b_angemeldet smallint NOT NULL
);
ALTER TABLE public.part_newslettergrund OWNER TO postgres;
CREATE TABLE public.part_partneradressbuch (
    i_id integer NOT NULL,
    partner_i_id integer NOT NULL,
    c_exchangeid character varying(200),
    t_zuletzt_exportiert timestamp without time zone NOT NULL,
    c_emailadresse character varying(80) NOT NULL
);
ALTER TABLE public.part_partneradressbuch OWNER TO postgres;
CREATE TABLE public.part_partnerart (
    c_nr character(15) NOT NULL
);
ALTER TABLE public.part_partnerart OWNER TO postgres;
CREATE TABLE public.part_partnerartspr (
    partnerart_c_nr character(15) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.part_partnerartspr OWNER TO postgres;
CREATE TABLE public.part_partnerbank (
    i_id integer NOT NULL,
    partner_i_id integer NOT NULL,
    partnerbank_i_id integer NOT NULL,
    c_ktonr character varying(50),
    c_iban character varying(40),
    i_sort integer NOT NULL,
    c_sepamandatsnummer character varying(40),
    t_sepaerteilt timestamp without time zone,
    c_esr character varying(9),
    waehrung_c_nr character(3)
);
ALTER TABLE public.part_partnerbank OWNER TO postgres;
CREATE TABLE public.part_partnerbild (
    partner_i_id integer NOT NULL,
    o_bild bytea NOT NULL
);
ALTER TABLE public.part_partnerbild OWNER TO postgres;
CREATE TABLE public.part_partnerklasse (
    i_id integer NOT NULL,
    c_nr character(15) NOT NULL,
    c_importart character varying(15)
);
ALTER TABLE public.part_partnerklasse OWNER TO postgres;
CREATE TABLE public.part_partnerklassespr (
    partnerklasse_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.part_partnerklassespr OWNER TO postgres;
CREATE TABLE public.part_partnerkommentar (
    i_id integer NOT NULL,
    partner_i_id integer NOT NULL,
    b_kunde smallint NOT NULL,
    partnerkommentarart_i_id integer NOT NULL,
    datenformat_c_nr character varying(40),
    x_kommentar text,
    o_media bytea,
    i_art integer NOT NULL,
    i_sort integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    c_dateiname character varying(300),
    t_filedatum timestamp without time zone
);
ALTER TABLE public.part_partnerkommentar OWNER TO postgres;
CREATE TABLE public.part_partnerkommentarart (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.part_partnerkommentarart OWNER TO postgres;
CREATE TABLE public.part_partnerkommentardruck (
    i_id integer NOT NULL,
    partnerkommentar_i_id integer NOT NULL,
    belegart_c_nr character(15) NOT NULL
);
ALTER TABLE public.part_partnerkommentardruck OWNER TO postgres;
CREATE TABLE public.part_partnerkommunikation (
    i_id integer NOT NULL,
    partner_i_id integer NOT NULL,
    kommunikationsart_c_nr character(15) NOT NULL,
    c_bez character varying(80) NOT NULL,
    c_inhalt character varying(80) NOT NULL,
    mandant_c_nr character varying(3)
);
ALTER TABLE public.part_partnerkommunikation OWNER TO postgres;
CREATE TABLE public.part_paselektion (
    partner_i_id integer NOT NULL,
    selektion_i_id integer NOT NULL,
    c_bemerkung character varying(80),
    i_id integer NOT NULL
);
ALTER TABLE public.part_paselektion OWNER TO postgres;
CREATE VIEW public.part_persoenlichesadressbuch AS
 SELECT row_number() OVER (ORDER BY a.partner_i_id, a.ansprechpartner_i_id) AS i_id,
    a.partner_i_id,
    a.ansprechpartner_i_id,
    a.c_emailadresse_partneradressbuch,
    a.c_exchangeid_partneradressbuch,
    a.t_zuletzt_exportiert_partneradressbuch,
    a.c_emailadresse_ansprechpartneradressbuch,
    a.c_exchangeid_ansprechpartneradressbuch,
    a.t_zuletzt_exportiert_ansprechpartneradressbuch
   FROM ( SELECT p.i_id AS partner_i_id,
            ansp.i_id AS ansprechpartner_i_id,
            padr.c_emailadresse AS c_emailadresse_partneradressbuch,
            padr.c_exchangeid AS c_exchangeid_partneradressbuch,
            padr.t_zuletzt_exportiert AS t_zuletzt_exportiert_partneradressbuch,
            ansadr.c_emailadresse AS c_emailadresse_ansprechpartneradressbuch,
            ansadr.c_exchangeid AS c_exchangeid_ansprechpartneradressbuch,
            ansadr.t_zuletzt_exportiert AS t_zuletzt_exportiert_ansprechpartneradressbuch
           FROM (((public.part_partner p
             LEFT JOIN public.part_ansprechpartner ansp ON ((ansp.partner_i_id = p.i_id)))
             LEFT JOIN public.part_partneradressbuch padr ON ((padr.partner_i_id = p.i_id)))
             LEFT JOIN public.part_ansprechpartneradressbuch ansadr ON ((ansadr.ansprechpartner_i_id = ansp.i_id)))
          WHERE ((p.b_versteckt = 0) AND ((ansp.b_versteckt = 0) OR (ansp.b_versteckt IS NULL)))) a;
ALTER TABLE public.part_persoenlichesadressbuch OWNER TO postgres;
CREATE TABLE public.part_selektion (
    i_id integer NOT NULL,
    c_nr character varying(20) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    b_webshop smallint NOT NULL
);
ALTER TABLE public.part_selektion OWNER TO postgres;
CREATE TABLE public.part_selektionspr (
    selektion_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.part_selektionspr OWNER TO postgres;
CREATE TABLE public.part_serienbrief (
    i_id integer NOT NULL,
    c_bez character varying(50) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    b_gehtankunden smallint NOT NULL,
    b_gehtaninteressenten smallint NOT NULL,
    b_verstecktedabei smallint NOT NULL,
    b_ansprechpartnerfunktionauchohne smallint NOT NULL,
    c_plz character varying(8),
    ansprechpartnerfunktion_i_id integer,
    c_betreff character varying(80),
    x_text text,
    land_i_id integer,
    b_gehtanlieferanten smallint NOT NULL,
    b_gehtanmoeglichelieferanten smallint NOT NULL,
    b_gehtanpartner smallint NOT NULL,
    n_abumsatz numeric(17,6),
    n_bisumsatz numeric(17,6),
    t_umsatzab timestamp without time zone,
    t_umsatzbis timestamp without time zone,
    b_mitzugeordnetenfirmen smallint NOT NULL,
    partnerklasse_i_id integer,
    branche_i_id integer,
    x_mailtext text,
    b_newsletter smallint,
    b_selektionen_logisches_oder smallint NOT NULL,
    b_wennkeinanspmitfkt_dannersteransp smallint NOT NULL,
    b_html smallint NOT NULL,
    locale_c_nr character(10) NOT NULL
);
ALTER TABLE public.part_serienbrief OWNER TO postgres;
CREATE TABLE public.part_serienbriefselektion (
    serienbrief_i_id integer NOT NULL,
    selektion_i_id integer NOT NULL,
    c_bemerkung character varying(50)
);
ALTER TABLE public.part_serienbriefselektion OWNER TO postgres;
CREATE TABLE public.part_serienbriefselektionnegativ (
    serienbrief_i_id integer NOT NULL,
    selektion_i_id integer NOT NULL,
    c_bemerkung character varying(50)
);
ALTER TABLE public.part_serienbriefselektionnegativ OWNER TO postgres;
CREATE TABLE public.part_telefonnummer (
    i_id integer NOT NULL,
    partner_i_id integer NOT NULL,
    ansprechpartner_i_id integer,
    c_nummer character varying(80) NOT NULL,
    b_versteckt smallint NOT NULL
);
ALTER TABLE public.part_telefonnummer OWNER TO postgres;
CREATE TABLE public.part_weblieferantfarnell (
    webpartner_i_id integer NOT NULL,
    c_url character varying(100),
    c_apikey character varying(100),
    c_customerid character varying(100),
    c_customerkey character varying(100),
    c_store character varying(100)
);
ALTER TABLE public.part_weblieferantfarnell OWNER TO postgres;
CREATE TABLE public.pers_abwesenheitsart (
    i_id integer NOT NULL,
    c_nr character varying(40) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.pers_abwesenheitsart OWNER TO postgres;
CREATE TABLE public.pers_abwesenheitsartspr (
    abwesenheitsart_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.pers_abwesenheitsartspr OWNER TO postgres;
CREATE TABLE public.pers_angehoerigenart (
    c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.pers_angehoerigenart OWNER TO postgres;
CREATE TABLE public.pers_angehoerigenartspr (
    locale_c_nr character(10) NOT NULL,
    angehoerigenart_c_nr character(15) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.pers_angehoerigenartspr OWNER TO postgres;
CREATE TABLE public.pers_anwesenheitsbestaetigung (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    auftrag_i_id integer,
    projekt_i_id integer,
    t_unterschrift timestamp without time zone NOT NULL,
    t_versandt timestamp without time zone,
    o_unterschrift bytea,
    datenformat_c_nr character varying(40),
    o_pdf bytea,
    c_bemerkung character varying(80),
    i_lfdnr integer NOT NULL,
    c_name character varying(80)
);
ALTER TABLE public.pers_anwesenheitsbestaetigung OWNER TO postgres;
CREATE TABLE public.pers_artgrurolle (
    i_id integer NOT NULL,
    artgru_i_id integer NOT NULL,
    systemrolle_i_id integer NOT NULL
);
ALTER TABLE public.pers_artgrurolle OWNER TO postgres;
CREATE TABLE public.pers_artikelzulage (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    zulage_i_id integer NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL
);
ALTER TABLE public.pers_artikelzulage OWNER TO postgres;
CREATE TABLE public.pers_artikelzuschlag (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    n_zuschlag numeric(17,6) NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL
);
ALTER TABLE public.pers_artikelzuschlag OWNER TO postgres;
CREATE TABLE public.pers_auszahlung_bva (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    t_datum timestamp without time zone,
    n_gleitzeit numeric(17,6) NOT NULL,
    n_uest50_gz numeric(17,6) NOT NULL,
    n_uest50_gz_zuschlag numeric(17,6) NOT NULL,
    n_uest50 numeric(17,6) NOT NULL,
    n_uest50_zuschlag numeric(17,6) NOT NULL,
    n_uest100 numeric(17,6) NOT NULL,
    n_uest100_zuschlag numeric(17,6) NOT NULL,
    c_kommentar character varying(80),
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.pers_auszahlung_bva OWNER TO postgres;
CREATE TABLE public.pers_benutzer (
    i_id integer NOT NULL,
    c_benutzerkennung character varying(40) NOT NULL,
    c_kennwort character varying(40) NOT NULL,
    b_aendern smallint NOT NULL,
    b_gesperrt smallint NOT NULL,
    t_gueltigbis timestamp without time zone,
    i_fehlversuchegemacht integer,
    mandant_c_nr_default character varying(3),
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.pers_benutzer OWNER TO postgres;
CREATE TABLE public.pers_benutzermandantsystemrolle (
    i_id integer NOT NULL,
    benutzer_i_id integer NOT NULL,
    systemrolle_i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    personal_i_id_zugeordnet integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    systemrolle_i_id_restapi integer,
    systemrolle_i_id_hvma integer
);
ALTER TABLE public.pers_benutzermandantsystemrolle OWNER TO postgres;
CREATE TABLE public.pers_bereitschaft (
    i_id integer NOT NULL,
    bereitschaftart_i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    t_beginn timestamp without time zone NOT NULL,
    t_ende timestamp without time zone NOT NULL,
    c_bemerkung character varying(80)
);
ALTER TABLE public.pers_bereitschaft OWNER TO postgres;
CREATE TABLE public.pers_bereitschaftart (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.pers_bereitschaftart OWNER TO postgres;
CREATE TABLE public.pers_bereitschafttag (
    i_id integer NOT NULL,
    bereitschaftart_i_id integer NOT NULL,
    tagesart_i_id integer NOT NULL,
    u_beginn time without time zone NOT NULL,
    u_ende time without time zone,
    b_endedestages smallint NOT NULL
);
ALTER TABLE public.pers_bereitschafttag OWNER TO postgres;
CREATE TABLE public.pers_beruf (
    i_id integer NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.pers_beruf OWNER TO postgres;
CREATE TABLE public.pers_betriebskalender (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(80),
    t_datum timestamp without time zone NOT NULL,
    religion_i_id integer,
    tagesart_i_id integer NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.pers_betriebskalender OWNER TO postgres;
CREATE TABLE public.pers_diaeten (
    i_id integer NOT NULL,
    c_bez character varying(50) NOT NULL,
    land_i_id integer NOT NULL
);
ALTER TABLE public.pers_diaeten OWNER TO postgres;
CREATE TABLE public.pers_diaetentagessatz (
    i_id integer NOT NULL,
    diaeten_i_id integer NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL,
    i_abstunden integer NOT NULL,
    b_stundenweise smallint NOT NULL,
    n_stundensatz numeric(17,6) NOT NULL,
    n_tagessatz numeric(17,6) NOT NULL,
    n_mindestsatz numeric(17,6) NOT NULL,
    c_filename_script character varying(80)
);
ALTER TABLE public.pers_diaetentagessatz OWNER TO postgres;
CREATE TABLE public.pers_eintrittaustritt (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    t_eintritt timestamp without time zone NOT NULL,
    t_austritt timestamp without time zone,
    c_austrittsgrund character varying(80),
    b_wiedereintritt smallint NOT NULL
);
ALTER TABLE public.pers_eintrittaustritt OWNER TO postgres;
CREATE TABLE public.pers_fahrzeug (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(40) NOT NULL,
    c_kennzeichen character varying(40) NOT NULL,
    fahrzeugverwendungsart_c_nr character(15) NOT NULL,
    b_versteckt smallint NOT NULL
);
ALTER TABLE public.pers_fahrzeug OWNER TO postgres;
CREATE TABLE public.pers_fahrzeugkosten (
    i_id integer NOT NULL,
    fahrzeug_i_id integer NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL,
    n_kmkosten numeric(17,6) NOT NULL
);
ALTER TABLE public.pers_fahrzeugkosten OWNER TO postgres;
CREATE TABLE public.pers_fahrzeugverwendungsart (
    c_nr character(15) NOT NULL
);
ALTER TABLE public.pers_fahrzeugverwendungsart OWNER TO postgres;
CREATE TABLE public.pers_familienstand (
    c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.pers_familienstand OWNER TO postgres;
CREATE TABLE public.pers_familienstandspr (
    locale_c_nr character(10) NOT NULL,
    familienstand_c_nr character(15) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.pers_familienstandspr OWNER TO postgres;
CREATE TABLE public.pers_feiertag (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(80),
    i_tag integer,
    i_monat integer,
    religion_i_id integer,
    i_offset_ostersonntag integer,
    tagesart_i_id integer NOT NULL
);
ALTER TABLE public.pers_feiertag OWNER TO postgres;
CREATE TABLE public.pers_fertigungsgrupperolle (
    i_id integer NOT NULL,
    fertigungsgruppe_i_id integer NOT NULL,
    systemrolle_i_id integer NOT NULL
);
ALTER TABLE public.pers_fertigungsgrupperolle OWNER TO postgres;
CREATE TABLE public.pers_fingerart (
    i_id integer NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.pers_fingerart OWNER TO postgres;
CREATE TABLE public.pers_gleitzeitsaldo (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    i_jahr integer NOT NULL,
    i_monat integer NOT NULL,
    t_abrechnungsstichtag timestamp without time zone,
    n_saldomehrstunden numeric(17,6) NOT NULL,
    n_saldouestfrei50 numeric(17,6) NOT NULL,
    n_saldouestpflichtig50 numeric(17,6) NOT NULL,
    n_saldouestfrei100 numeric(17,6) NOT NULL,
    n_saldouestpflichtig100 numeric(17,6) NOT NULL,
    b_gesperrt smallint NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    n_saldo numeric(17,6) NOT NULL,
    n_saldouest200 numeric(17,6) NOT NULL,
    n_gz_saldo_mit_uestd_in_normalstunden numeric(17,6) NOT NULL
);
ALTER TABLE public.pers_gleitzeitsaldo OWNER TO postgres;
CREATE TABLE public.pers_hvmabenutzer (
    i_id integer NOT NULL,
    hvmalizenz_i_id integer NOT NULL,
    benutzer_i_id integer NOT NULL,
    c_token character varying(40),
    t_anlegen timestamp without time zone
);
ALTER TABLE public.pers_hvmabenutzer OWNER TO postgres;
CREATE TABLE public.pers_hvmabenutzerparameter (
    i_id integer NOT NULL,
    hvmabenutzer_i_id integer NOT NULL,
    c_nr character varying(80) NOT NULL,
    c_kategorie character varying(40) NOT NULL,
    c_wert character varying(3000) NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.pers_hvmabenutzerparameter OWNER TO postgres;
CREATE TABLE public.pers_hvmalizenz (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    i_max_user integer NOT NULL
);
ALTER TABLE public.pers_hvmalizenz OWNER TO postgres;
CREATE TABLE public.pers_hvmaparameter (
    c_nr character varying(80) NOT NULL,
    c_kategorie character varying(40) NOT NULL,
    hvmalizenz_i_id integer NOT NULL,
    c_datentyp character varying(50) NOT NULL,
    c_bemerkung character varying(3000) NOT NULL,
    c_default character varying(3000) NOT NULL,
    b_uebertragen smallint NOT NULL
);
ALTER TABLE public.pers_hvmaparameter OWNER TO postgres;
CREATE TABLE public.pers_hvmarecht (
    i_id integer NOT NULL,
    c_nr character varying(40) NOT NULL,
    hvmalizenz_i_id integer NOT NULL,
    b_aktiv smallint NOT NULL
);
ALTER TABLE public.pers_hvmarecht OWNER TO postgres;
CREATE TABLE public.pers_hvmarolle (
    i_id integer NOT NULL,
    systemrolle_i_id integer NOT NULL,
    hvmarecht_i_id integer NOT NULL
);
ALTER TABLE public.pers_hvmarolle OWNER TO postgres;
CREATE TABLE public.pers_hvmasync (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    t_sync timestamp without time zone,
    systemrolle_i_id_synczeitpunkt integer,
    c_wowurdegebucht character varying(80) NOT NULL
);
ALTER TABLE public.pers_hvmasync OWNER TO postgres;
CREATE TABLE public.pers_kollektiv (
    i_id integer NOT NULL,
    c_bez character varying(40) NOT NULL,
    n_normalstunden numeric(17,6),
    n_faktoruestd50 numeric(17,6) NOT NULL,
    n_faktoruestd100 numeric(17,6) NOT NULL,
    u_blockzeitab time without time zone,
    u_blockzeitbis time without time zone,
    b_verbraucheuestd smallint NOT NULL,
    n_faktormehrstd numeric(17,6) NOT NULL,
    b_uestdabsollstderbracht smallint NOT NULL,
    b_uestdverteilen smallint NOT NULL,
    n_faktoruestd200 numeric(17,6) NOT NULL,
    n_200prozentigeab numeric(17,6) NOT NULL,
    i_berechnungsbasis integer NOT NULL,
    b_wochengesamtsicht smallint NOT NULL,
    c_abrechnungsart character varying(80) NOT NULL,
    b_feiertagsueberstunden_ab_soll smallint NOT NULL,
    i_faktor_passive_reisezeit integer NOT NULL
);
ALTER TABLE public.pers_kollektiv OWNER TO postgres;
CREATE TABLE public.pers_kollektivuestd (
    i_id integer NOT NULL,
    kollektiv_i_id integer NOT NULL,
    b_restdestages smallint NOT NULL,
    tagesart_i_id integer NOT NULL,
    u_ab time without time zone NOT NULL,
    u_bis time without time zone,
    b_unterignorieren smallint NOT NULL
);
ALTER TABLE public.pers_kollektivuestd OWNER TO postgres;
CREATE TABLE public.pers_kollektivuestd50 (
    i_id integer NOT NULL,
    kollektiv_i_id integer NOT NULL,
    b_restdestages smallint NOT NULL,
    tagesart_i_id integer NOT NULL,
    u_von time without time zone NOT NULL,
    u_bis time without time zone,
    b_unterignorieren smallint NOT NULL
);
ALTER TABLE public.pers_kollektivuestd50 OWNER TO postgres;
CREATE TABLE public.pers_kollektivuestd_bva (
    i_id integer NOT NULL,
    kollektiv_i_id integer NOT NULL,
    u_100_ende time without time zone NOT NULL,
    u_50_beginn time without time zone,
    u_50_ende time without time zone NOT NULL,
    u_100_beginn time without time zone NOT NULL,
    u_gleitzeit_bis time without time zone NOT NULL,
    tagesart_i_id integer NOT NULL
);
ALTER TABLE public.pers_kollektivuestd_bva OWNER TO postgres;
CREATE TABLE public.pers_lagerrolle (
    i_id integer NOT NULL,
    lager_i_id integer NOT NULL,
    systemrolle_i_id integer NOT NULL
);
ALTER TABLE public.pers_lagerrolle OWNER TO postgres;
CREATE TABLE public.pers_lohnart (
    i_id integer NOT NULL,
    i_lohnart integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    c_kommentar character varying(80),
    taetigkeit_i_id_nl integer,
    c_formel character varying(300),
    c_typ character(15) NOT NULL,
    personalart_c_nr character(15),
    i_ausfall_wochen integer,
    f_mindestuestd double precision
);
ALTER TABLE public.pers_lohnart OWNER TO postgres;
CREATE TABLE public.pers_lohnartstundenfaktor (
    i_id integer NOT NULL,
    lohnart_i_id integer NOT NULL,
    lohnstundenart_c_nr character(15) NOT NULL,
    f_faktor double precision NOT NULL,
    tagesart_i_id integer,
    zeitmodell_i_id integer,
    schichtzeit_i_id integer,
    taetigkeit_i_id integer
);
ALTER TABLE public.pers_lohnartstundenfaktor OWNER TO postgres;
CREATE TABLE public.pers_lohngruppe (
    i_id integer NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.pers_lohngruppe OWNER TO postgres;
CREATE TABLE public.pers_lohnstundenart (
    c_nr character(15) NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.pers_lohnstundenart OWNER TO postgres;
CREATE TABLE public.pers_maschine (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_inventarnummer character varying(40) NOT NULL,
    c_bez character varying(80),
    b_autoendebeigeht smallint NOT NULL,
    t_kaufdatum timestamp without time zone,
    c_identifikationsnr character varying(15),
    maschinengruppe_i_id integer NOT NULL,
    b_versteckt smallint NOT NULL,
    n_anschaffungskosten numeric(15,4),
    i_abschreibung_in_monaten integer,
    n_verzinsung numeric(15,4),
    n_energierkosten numeric(15,4),
    n_raumkosten numeric(15,4),
    n_sonstige_kosten numeric(15,4),
    i_planstunden integer,
    artikel_i_id_verrechnen integer,
    b_manuelle_bedienung smallint NOT NULL,
    c_seriennummer character varying(40)
);
ALTER TABLE public.pers_maschine OWNER TO postgres;
CREATE TABLE public.pers_maschineleistungsfaktor (
    i_id integer NOT NULL,
    maschine_i_id integer NOT NULL,
    material_i_id integer NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL,
    n_faktor_in_prozent numeric(15,4) NOT NULL
);
ALTER TABLE public.pers_maschineleistungsfaktor OWNER TO postgres;
CREATE TABLE public.pers_maschinemaschinenzm (
    i_id integer NOT NULL,
    maschine_i_id integer NOT NULL,
    maschinenzm_i_id integer NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL
);
ALTER TABLE public.pers_maschinemaschinenzm OWNER TO postgres;
CREATE TABLE public.pers_maschinengruppe (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    b_auslastungsanzeige smallint NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    fertigungsgruppe_i_id integer NOT NULL,
    i_sort integer NOT NULL,
    c_kbez character varying(2) NOT NULL
);
ALTER TABLE public.pers_maschinengruppe OWNER TO postgres;
CREATE TABLE public.pers_maschinenkosten (
    i_id integer NOT NULL,
    maschine_i_id integer NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL,
    n_stundensatz numeric(17,6) NOT NULL,
    n_vkstundensatz numeric(17,6) NOT NULL
);
ALTER TABLE public.pers_maschinenkosten OWNER TO postgres;
CREATE TABLE public.pers_maschinenzeitdaten (
    i_id integer NOT NULL,
    maschine_i_id integer NOT NULL,
    t_von timestamp without time zone NOT NULL,
    t_bis timestamp without time zone,
    lossollarbeitsplan_i_id integer NOT NULL,
    c_bemerkung character varying(80),
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    personal_i_id_gestartet integer NOT NULL,
    f_verrechenbar double precision,
    t_erledigt timestamp without time zone,
    personal_i_id_erledigt integer,
    b_parallel smallint NOT NULL
);
ALTER TABLE public.pers_maschinenzeitdaten OWNER TO postgres;
CREATE TABLE public.pers_maschinenzeitdatenverrechnet (
    i_id integer NOT NULL,
    maschinenzeitdaten_i_id integer NOT NULL,
    rechnungposition_i_id integer NOT NULL,
    n_stunden numeric(15,4) NOT NULL,
    artikel_i_id integer NOT NULL
);
ALTER TABLE public.pers_maschinenzeitdatenverrechnet OWNER TO postgres;
CREATE TABLE public.pers_maschinenzm (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.pers_maschinenzm OWNER TO postgres;
CREATE TABLE public.pers_maschinenzmtagesart (
    i_id integer NOT NULL,
    maschinenzm_i_id integer NOT NULL,
    tagesart_i_id integer NOT NULL,
    l_verfuegbarkeit bigint NOT NULL
);
ALTER TABLE public.pers_maschinenzmtagesart OWNER TO postgres;
CREATE TABLE public.pers_nachrichtarchiv (
    i_id integer NOT NULL,
    nachrichtart_i_id integer NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    personal_i_id_bearbeiter integer,
    t_zeit timestamp without time zone NOT NULL,
    t_erledigt timestamp without time zone,
    t_bearbeitung timestamp without time zone,
    c_nachricht character varying(300) NOT NULL,
    personal_i_id_erledigt integer,
    c_erledigungsgrund character varying(80)
);
ALTER TABLE public.pers_nachrichtarchiv OWNER TO postgres;
CREATE TABLE public.pers_nachrichtart (
    i_id integer NOT NULL,
    c_nr character varying(40) NOT NULL,
    c_bez character varying(80),
    thema_c_nr character(15) NOT NULL,
    b_archivieren smallint NOT NULL,
    b_popup smallint NOT NULL
);
ALTER TABLE public.pers_nachrichtart OWNER TO postgres;
CREATE TABLE public.pers_nachrichten (
    i_id integer NOT NULL,
    nachrichtenart_i_id integer NOT NULL,
    personal_i_id_absender integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_erledigt integer,
    t_erledigt timestamp without time zone,
    c_betreff character varying(300),
    x_text character varying(3000),
    c_belegartnr character(15),
    beleg_i_id integer,
    belegposition_i_id integer
);
ALTER TABLE public.pers_nachrichten OWNER TO postgres;
CREATE TABLE public.pers_nachrichtenabo (
    i_id integer NOT NULL,
    nachrichtenart_i_id integer NOT NULL,
    personal_i_id integer,
    nachrichtengruppe_i_id integer
);
ALTER TABLE public.pers_nachrichtenabo OWNER TO postgres;
CREATE TABLE public.pers_nachrichtenart (
    i_id integer NOT NULL,
    c_nr character varying(40) NOT NULL,
    b_muss_erledigt_werden smallint NOT NULL,
    u_eskalation time without time zone,
    personal_i_id_eskalation integer
);
ALTER TABLE public.pers_nachrichtenart OWNER TO postgres;
CREATE TABLE public.pers_nachrichtenartspr (
    nachrichtenart_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.pers_nachrichtenartspr OWNER TO postgres;
CREATE TABLE public.pers_nachrichtenempfaenger (
    i_id integer NOT NULL,
    nachrichten_i_id integer NOT NULL,
    personal_i_id_empfaenger integer NOT NULL,
    t_empfangen timestamp without time zone,
    t_gelesen timestamp without time zone
);
ALTER TABLE public.pers_nachrichtenempfaenger OWNER TO postgres;
CREATE TABLE public.pers_nachrichtengruppe (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.pers_nachrichtengruppe OWNER TO postgres;
CREATE TABLE public.pers_nachrichtengruppeteilnehmer (
    i_id integer NOT NULL,
    nachrichtengruppe_i_id integer NOT NULL,
    personal_i_id integer NOT NULL
);
ALTER TABLE public.pers_nachrichtengruppeteilnehmer OWNER TO postgres;
CREATE TABLE public.pers_passivereise (
    i_id integer NOT NULL,
    kollektiv_i_id integer NOT NULL,
    u_ab time without time zone NOT NULL,
    u_bis time without time zone,
    b_restdestages smallint NOT NULL,
    tagesart_i_id integer NOT NULL
);
ALTER TABLE public.pers_passivereise OWNER TO postgres;
CREATE TABLE public.pers_pendlerpauschale (
    i_id integer NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.pers_pendlerpauschale OWNER TO postgres;
CREATE TABLE public.pers_personal (
    i_id integer NOT NULL,
    partner_i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    personalart_c_nr character(15) NOT NULL,
    personalfunktion_c_nr character(20),
    c_ausweis character varying(80),
    b_maennlich smallint NOT NULL,
    familienstand_c_nr character(15),
    kollektiv_i_id integer,
    beruf_i_id integer,
    lohngruppe_i_id integer,
    religion_i_id integer,
    land_i_id_staatsangehoerigkeit integer,
    b_ueberstundenausbezahlt smallint NOT NULL,
    landplzort_i_id_geburt integer,
    t_geburtsdatum timestamp without time zone,
    c_sozialversnr character varying(50),
    partner_i_id_sozialversicherer integer,
    partner_i_id_firma integer,
    kostenstelle_i_id_abteilung integer,
    kostenstelle_i_id_stamm integer NOT NULL,
    b_anwesenheitsliste smallint NOT NULL,
    c_kurzzeichen character varying(3),
    pendlerpauschale_i_id integer,
    x_kommentar text,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_anlegen integer,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer,
    c_unterschriftsfunktion character varying(15),
    c_unterschriftstext character varying(40),
    b_versteckt smallint NOT NULL,
    personalgruppe_i_id integer,
    c_imapbenutzer character varying(80),
    c_imapkennwort character varying(80),
    b_anwesenheitterminal smallint NOT NULL,
    b_anwesenheitalleterminal smallint NOT NULL,
    c_telefon character varying(80),
    c_fax character varying(80),
    c_direktfax character varying(80),
    c_handy character varying(80),
    c_email character varying(80),
    c_personalnr character varying(40) NOT NULL,
    b_telefonzeitstarten smallint NOT NULL,
    c_imapinboxfolder character varying(80),
    b_start_mit_meinen_offenen_projekten smallint NOT NULL,
    b_kommt_am_terminal smallint NOT NULL,
    c_bccempfaenger character varying(300),
    b_keine_anzeige_am_terminal smallint NOT NULL,
    b_wep_info smallint NOT NULL,
    maschinengruppe_i_id integer,
    b_synch_alle_kontakte smallint NOT NULL,
    c_versandkennwort character varying(50)
);
ALTER TABLE public.pers_personal OWNER TO postgres;
CREATE TABLE public.pers_personalangehoerige (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    angehoerigenart_c_nr character(15) NOT NULL,
    c_vorname character varying(40) NOT NULL,
    c_name character varying(40),
    t_geburtsdatum timestamp without time zone,
    c_sozialversnr character varying(30)
);
ALTER TABLE public.pers_personalangehoerige OWNER TO postgres;
CREATE TABLE public.pers_personalart (
    c_nr character(15) NOT NULL
);
ALTER TABLE public.pers_personalart OWNER TO postgres;
CREATE TABLE public.pers_personalartspr (
    locale_c_nr character(10) NOT NULL,
    personalart_c_nr character(15) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.pers_personalartspr OWNER TO postgres;
CREATE TABLE public.pers_personalfahrzeug (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    fahrzeug_i_id integer NOT NULL
);
ALTER TABLE public.pers_personalfahrzeug OWNER TO postgres;
CREATE TABLE public.pers_personalfinger (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    fingerart_i_id integer NOT NULL,
    i_fingerid integer NOT NULL,
    o_template1 bytea NOT NULL,
    o_template2 bytea,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.pers_personalfinger OWNER TO postgres;
CREATE TABLE public.pers_personalfunktion (
    c_nr character(20) NOT NULL
);
ALTER TABLE public.pers_personalfunktion OWNER TO postgres;
CREATE TABLE public.pers_personalfunktionspr (
    locale_c_nr character(10) NOT NULL,
    personalfunktion_c_nr character(20) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.pers_personalfunktionspr OWNER TO postgres;
CREATE TABLE public.pers_personalgehalt (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    n_gehalt numeric(17,6) NOT NULL,
    f_uestpauschale double precision NOT NULL,
    n_stundensatz numeric(17,6) NOT NULL,
    f_verfuegbarkeit double precision NOT NULL,
    n_kmgeld1 numeric(17,6) NOT NULL,
    f_biskilometer double precision NOT NULL,
    n_kmgeld2 numeric(17,6) NOT NULL,
    b_kksgebbefreit smallint NOT NULL,
    c_grundkksgebbefreit character varying(80),
    b_alleinverdiener smallint NOT NULL,
    b_alleinerzieher smallint NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    n_uestdpuffer numeric(15,4) NOT NULL,
    f_leistungswert double precision,
    n_gehalt_netto numeric(17,6),
    n_gehalt_bruttobrutto numeric(17,6),
    n_praemie_bruttobrutto numeric(17,6),
    f_kalk_ist_jahrestunden double precision,
    n_lohnmittelstundensatz numeric(17,6),
    n_aufschlag_lohnmittelstundensatz numeric(17,6),
    f_faktor_lohnmittelstundensatz double precision,
    b_stundensatz_fixiert smallint NOT NULL,
    n_lohnsteuer numeric(17,6),
    n_drzpauschale numeric(15,4) NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL,
    b_negativstunden_in_urlaub_umwandeln_tageweise_betrachten smallint NOT NULL,
    i_uestdauszahlen integer NOT NULL,
    n_kmgeld_mitfahrer numeric(15,4) NOT NULL
);
ALTER TABLE public.pers_personalgehalt OWNER TO postgres;
CREATE TABLE public.pers_personalgruppe (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.pers_personalgruppe OWNER TO postgres;
CREATE TABLE public.pers_personalgruppekosten (
    i_id integer NOT NULL,
    personalgruppe_i_id integer NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL,
    n_stundensatz numeric(17,6)
);
ALTER TABLE public.pers_personalgruppekosten OWNER TO postgres;
CREATE TABLE public.pers_personalterminal (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    arbeitsplatz_i_id integer NOT NULL
);
ALTER TABLE public.pers_personalterminal OWNER TO postgres;
CREATE TABLE public.pers_personalverfuegbarkeit (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    f_anteilprozent double precision NOT NULL
);
ALTER TABLE public.pers_personalverfuegbarkeit OWNER TO postgres;
CREATE TABLE public.pers_personalzeiten (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    t_von timestamp without time zone NOT NULL,
    t_bis timestamp without time zone NOT NULL,
    c_bemerkung character varying(80)
);
ALTER TABLE public.pers_personalzeiten OWNER TO postgres;
CREATE TABLE public.pers_personalzeitmodell (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    zeitmodell_i_id integer NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL
);
ALTER TABLE public.pers_personalzeitmodell OWNER TO postgres;
CREATE TABLE public.pers_personalzutrittsklasse (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    zutrittsklasse_i_id integer NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL
);
ALTER TABLE public.pers_personalzutrittsklasse OWNER TO postgres;
CREATE TABLE public.pers_telefonzeiten (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    partner_i_id integer,
    ansprechpartner_i_id integer,
    t_von timestamp without time zone NOT NULL,
    t_bis timestamp without time zone,
    x_kommentarext text,
    x_kommentarint text,
    projekt_i_id integer,
    f_verrechenbar double precision,
    t_erledigt timestamp without time zone,
    personal_i_id_erledigt integer,
    c_telefonnr_gewaehlt character varying(100),
    t_wiedervorlage timestamp without time zone,
    t_wiedervorlage_erledigt timestamp without time zone,
    c_titel character varying(80),
    personal_i_id_zugewiesener integer NOT NULL,
    kontaktart_i_id integer,
    angebot_i_id integer,
    auftrag_i_id integer,
    f_dauer_uebersteuert double precision
);
ALTER TABLE public.pers_telefonzeiten OWNER TO postgres;
CREATE VIEW public.pers_projektzeiten AS
 SELECT row_number() OVER (ORDER BY a.t_zeit, a.personal_i_id, a.zeitdaten_i_id) AS i_id,
    a.zeitdaten_i_id,
    a.telefonzeiten_i_id,
    a.t_zeit,
    a.personal_i_id,
    a.projekt_i_id,
    a.f_verrechenbar,
    a.c_bemerkung
   FROM ( SELECT zd.i_id AS zeitdaten_i_id,
            NULL::integer AS telefonzeiten_i_id,
            zd.t_zeit,
            zd.personal_i_id,
            zd.i_belegartid AS projekt_i_id,
            zd.f_verrechenbar,
            zd.c_bemerkungzubelegart AS c_bemerkung
           FROM public.pers_zeitdaten zd
          WHERE ((zd.c_belegartnr = 'Projekt'::bpchar) AND (zd.i_belegartid IS NOT NULL))
        UNION
         SELECT NULL::integer AS int4,
            tz.i_id,
            tz.t_von,
            tz.personal_i_id,
            tz.projekt_i_id,
            tz.f_verrechenbar,
            tz.c_titel
           FROM public.pers_telefonzeiten tz
          WHERE (tz.projekt_i_id IS NOT NULL)) a;
ALTER TABLE public.pers_projektzeiten OWNER TO postgres;
CREATE TABLE public.pers_recht (
    c_nr character(100) NOT NULL,
    x_kommentar text
);
ALTER TABLE public.pers_recht OWNER TO postgres;
CREATE TABLE public.pers_reise (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    t_zeit timestamp without time zone NOT NULL,
    b_beginn smallint NOT NULL,
    partner_i_id integer,
    ansprechpartner_i_id integer,
    i_kmbeginn integer,
    i_kmende integer,
    n_spesen numeric(17,6),
    c_fahrzeug character varying(15),
    c_kommentar character varying(80),
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    diaeten_i_id integer,
    i_belegartid integer,
    belegart_c_nr character(15),
    fahrzeug_i_id integer,
    f_verrechenbar double precision,
    t_erledigt timestamp without time zone,
    personal_i_id_erledigt integer,
    i_mitfahrer integer
);
ALTER TABLE public.pers_reise OWNER TO postgres;
CREATE TABLE public.pers_reiselog (
    i_id integer NOT NULL,
    reise_i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    t_zeit timestamp without time zone NOT NULL,
    b_beginn smallint NOT NULL,
    partner_i_id integer,
    ansprechpartner_i_id integer,
    i_kmbeginn integer,
    i_kmende integer,
    n_spesen numeric(17,6),
    c_fahrzeug character varying(15),
    c_kommentar character varying(80),
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    c_art character varying(15) NOT NULL,
    diaeten_i_id integer,
    i_belegartid integer,
    belegart_c_nr character(15),
    fahrzeug_i_id integer
);
ALTER TABLE public.pers_reiselog OWNER TO postgres;
CREATE TABLE public.pers_reisespesen (
    i_id integer NOT NULL,
    reise_i_id integer NOT NULL,
    eingangsrechnung_i_id integer NOT NULL
);
ALTER TABLE public.pers_reisespesen OWNER TO postgres;
CREATE TABLE public.pers_reiseverrechnet (
    i_id integer NOT NULL,
    reise_i_id integer NOT NULL,
    rechnungposition_i_id integer NOT NULL,
    n_betrag numeric(17,6) NOT NULL,
    n_kilometer numeric(17,6) NOT NULL,
    n_spesen numeric(17,6) NOT NULL
);
ALTER TABLE public.pers_reiseverrechnet OWNER TO postgres;
CREATE TABLE public.pers_religion (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL
);
ALTER TABLE public.pers_religion OWNER TO postgres;
CREATE TABLE public.pers_religionspr (
    locale_c_nr character(10) NOT NULL,
    religion_i_id integer NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.pers_religionspr OWNER TO postgres;
CREATE TABLE public.pers_rollerecht (
    i_id integer NOT NULL,
    systemrolle_i_id integer NOT NULL,
    recht_c_nr character(100) NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.pers_rollerecht OWNER TO postgres;
CREATE TABLE public.pers_schicht (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(80) NOT NULL,
    b_pausenabziehen smallint NOT NULL,
    b_begrenzt_auf_tagessoll smallint NOT NULL
);
ALTER TABLE public.pers_schicht OWNER TO postgres;
CREATE TABLE public.pers_schichtzeit (
    i_id integer NOT NULL,
    schicht_i_id integer NOT NULL,
    schichtzuschlag_i_id integer NOT NULL,
    u_beginn time without time zone NOT NULL,
    u_ende time without time zone,
    b_endedestages smallint NOT NULL
);
ALTER TABLE public.pers_schichtzeit OWNER TO postgres;
CREATE TABLE public.pers_schichtzeitmodell (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    zeitmodell_i_id integer NOT NULL
);
ALTER TABLE public.pers_schichtzeitmodell OWNER TO postgres;
CREATE TABLE public.pers_schichtzuschlag (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.pers_schichtzuschlag OWNER TO postgres;
CREATE TABLE public.pers_signatur (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    x_signatur text NOT NULL
);
ALTER TABLE public.pers_signatur OWNER TO postgres;
CREATE TABLE public.pers_sonderzeiten (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    t_datum timestamp without time zone NOT NULL,
    taetigkeit_i_id integer NOT NULL,
    b_tag smallint NOT NULL,
    b_halbtag smallint NOT NULL,
    u_stunden time without time zone,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    b_automatik smallint NOT NULL
);
ALTER TABLE public.pers_sonderzeiten OWNER TO postgres;
CREATE TABLE public.pers_stundenabrechnung (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    t_datum timestamp without time zone NOT NULL,
    n_mehrstunden numeric(17,6) NOT NULL,
    n_uestfrei50 numeric(17,6) NOT NULL,
    n_uestpflichtig50 numeric(17,6) NOT NULL,
    n_uestfrei100 numeric(17,6) NOT NULL,
    n_uestpflichtig100 numeric(17,6) NOT NULL,
    n_gutstunden numeric(17,6) NOT NULL,
    n_qualifikationspraemie numeric(17,6) NOT NULL,
    c_kommentar character varying(80),
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    n_normalstunden numeric(17,6) NOT NULL,
    n_uest200 numeric(17,6) NOT NULL,
    n_qualifikationsfaktor numeric(17,6)
);
ALTER TABLE public.pers_stundenabrechnung OWNER TO postgres;
CREATE TABLE public.pers_systemrolle (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    i_max_users integer,
    alias_rolle_i_id integer
);
ALTER TABLE public.pers_systemrolle OWNER TO postgres;
CREATE TABLE public.pers_taetigkeit (
    i_id integer NOT NULL,
    c_nr character(15) NOT NULL,
    taetigkeitart_c_nr character(15) NOT NULL,
    b_bdebuchbar smallint NOT NULL,
    b_feiertag smallint NOT NULL,
    b_tagbuchbar smallint NOT NULL,
    i_sort integer NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    f_bezahlt double precision NOT NULL,
    i_warnmeldunginkalendertagen integer,
    c_importkennzeichen character varying(40),
    b_unterbrichtwarnmeldung smallint NOT NULL,
    b_versteckt smallint NOT NULL,
    b_darf_selber_buchen smallint NOT NULL,
    abwesenheitsart_i_id integer
);
ALTER TABLE public.pers_taetigkeit OWNER TO postgres;
CREATE TABLE public.pers_taetigkeitart (
    c_nr character(15) NOT NULL
);
ALTER TABLE public.pers_taetigkeitart OWNER TO postgres;
CREATE TABLE public.pers_taetigkeitartspr (
    locale_c_nr character(10) NOT NULL,
    taetigkeitart_c_nr character(15) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.pers_taetigkeitartspr OWNER TO postgres;
CREATE TABLE public.pers_taetigkeitspr (
    taetigkeit_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.pers_taetigkeitspr OWNER TO postgres;
CREATE TABLE public.pers_tagesart (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.pers_tagesart OWNER TO postgres;
CREATE TABLE public.pers_tagesartspr (
    locale_c_nr character(10) NOT NULL,
    tagesart_i_id integer NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.pers_tagesartspr OWNER TO postgres;
CREATE VIEW public.pers_telefontodo AS
 SELECT concat('T', tz.i_id) AS c_nr,
    p.mandant_c_nr,
    tz.t_von,
    tz.t_bis,
    tz.partner_i_id,
    tz.ansprechpartner_i_id,
    tz.projekt_i_id,
    tz.angebot_i_id,
    tz.auftrag_i_id,
    tz.i_id AS telefonzeiten_i_id,
    tz.c_titel,
    tz.personal_i_id_zugewiesener,
    tz.t_wiedervorlage,
    tz.t_wiedervorlage_erledigt
   FROM (public.pers_telefonzeiten tz
     LEFT JOIN public.pers_personal p ON ((p.i_id = tz.personal_i_id_zugewiesener)))
UNION
 SELECT concat('A', ag.i_id) AS c_nr,
    ag.mandant_c_nr,
    ag.t_nachfasstermin AS t_von,
    NULL::timestamp without time zone AS t_bis,
    kd.partner_i_id,
    ag.ansprechpartner_i_id_kunde AS ansprechpartner_i_id,
    ag.projekt_i_id,
    ag.i_id AS angebot_i_id,
    NULL::integer AS auftrag_i_id,
    NULL::integer AS telefonzeiten_i_id,
    NULL::character varying AS c_titel,
    ag.personal_i_id_vertreter AS personal_i_id_zugewiesener,
    ag.t_nachfasstermin AS t_wiedervorlage,
    NULL::timestamp without time zone AS t_wiedervorlage_erledigt
   FROM (public.angb_angebot ag
     LEFT JOIN public.part_kunde kd ON ((kd.i_id = ag.kunde_i_id_angebotsadresse)))
  WHERE (ag.angebotstatus_c_nr <> ALL (ARRAY['Erledigt'::bpchar, 'Storniert'::bpchar]));
ALTER TABLE public.pers_telefontodo OWNER TO postgres;
CREATE TABLE public.pers_telefonzeitenverrechnet (
    i_id integer NOT NULL,
    telefonzeiten_i_id integer NOT NULL,
    rechnungposition_i_id integer NOT NULL,
    n_stunden numeric(15,4) NOT NULL,
    artikel_i_id integer NOT NULL
);
ALTER TABLE public.pers_telefonzeitenverrechnet OWNER TO postgres;
CREATE TABLE public.pers_thema (
    c_nr character(15) NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.pers_thema OWNER TO postgres;
CREATE TABLE public.pers_themarolle (
    i_id integer NOT NULL,
    thema_c_nr character(15) NOT NULL,
    systemrolle_i_id integer NOT NULL
);
ALTER TABLE public.pers_themarolle OWNER TO postgres;
CREATE TABLE public.pers_uebertrag_bva (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    t_datum timestamp without time zone,
    n_gleitzeit numeric(17,6) NOT NULL,
    n_uest50_gz numeric(17,6) NOT NULL,
    n_uest50_gz_zuschlag numeric(17,6) NOT NULL,
    n_uest50 numeric(17,6) NOT NULL,
    n_uest50_zuschlag numeric(17,6) NOT NULL,
    n_uest100 numeric(17,6) NOT NULL,
    n_uest100_zuschlag numeric(17,6) NOT NULL,
    b_gesperrt smallint NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.pers_uebertrag_bva OWNER TO postgres;
CREATE TABLE public.pers_urlaubsanspruch (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    i_jahr integer NOT NULL,
    f_stunden double precision NOT NULL,
    f_stundenzusaetzlich double precision NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    f_tage double precision,
    f_tagezusaetzlich double precision,
    f_resturlaubjahresendestunden double precision,
    f_resturlaubjahresendetage double precision,
    b_gesperrt smallint NOT NULL,
    f_jahresurlaubinwochen double precision NOT NULL
);
ALTER TABLE public.pers_urlaubsanspruch OWNER TO postgres;
CREATE TABLE public.pers_zahltag (
    i_id integer NOT NULL,
    i_monat integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    i_stichtag_netto integer NOT NULL,
    b_monatsletzter_netto integer NOT NULL,
    i_stichtag_lohnsteuer_relativ_zum_letzten integer NOT NULL,
    i_stichtag_sozialabgaben_relativ_zum_letzten integer NOT NULL,
    f_faktor double precision NOT NULL
);
ALTER TABLE public.pers_zahltag OWNER TO postgres;
CREATE TABLE public.pers_zeitabschluss (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    t_abgeschlossen_bis timestamp without time zone NOT NULL
);
ALTER TABLE public.pers_zeitabschluss OWNER TO postgres;
CREATE TABLE public.pers_zeitdatenpruefen (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    t_zeit timestamp without time zone NOT NULL,
    taetigkeit_i_id integer,
    b_taetigkeitgeaendert smallint NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    c_belegartnr character(15),
    i_belegartid integer,
    i_belegartpositionid integer,
    c_bemerkungzubelegart character varying(80),
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    artikel_i_id integer,
    x_kommentar text,
    b_automatikbuchung smallint NOT NULL,
    c_wowurdegebucht character varying(80),
    i_fehlercode integer,
    x_fehlertext text
);
ALTER TABLE public.pers_zeitdatenpruefen OWNER TO postgres;
CREATE TABLE public.pers_zeitdatenverrechnet (
    i_id integer NOT NULL,
    zeitdaten_i_id integer NOT NULL,
    rechnungposition_i_id integer NOT NULL,
    n_stunden numeric(15,4) NOT NULL,
    artikel_i_id integer NOT NULL
);
ALTER TABLE public.pers_zeitdatenverrechnet OWNER TO postgres;
CREATE TABLE public.pers_zeitdatenverrechnetzeitraum (
    i_id integer NOT NULL,
    rechnungposition_i_id integer NOT NULL,
    zeitdaten_i_id integer NOT NULL,
    t_von timestamp without time zone NOT NULL,
    t_bis timestamp without time zone NOT NULL,
    n_stunden numeric(15,4)
);
ALTER TABLE public.pers_zeitdatenverrechnetzeitraum OWNER TO postgres;
CREATE VIEW public.pers_zeitenmanuellerledigt AS
 SELECT row_number() OVER (ORDER BY a.zeitdaten_i_id, a.telefonzeiten_i_id, a.maschinenzeitdaten_i_id, a.reise_i_id) AS i_id,
    a.zeitdaten_i_id,
    a.telefonzeiten_i_id,
    a.maschinenzeitdaten_i_id,
    a.reise_i_id
   FROM ( SELECT z.i_id AS zeitdaten_i_id,
            NULL::integer AS telefonzeiten_i_id,
            NULL::integer AS maschinenzeitdaten_i_id,
            NULL::integer AS reise_i_id
           FROM public.pers_zeitdaten z
          WHERE ((z.t_erledigt IS NOT NULL) AND ((( SELECT count(*) AS count
                   FROM public.pers_zeitdatenverrechnet zv
                  WHERE (zv.zeitdaten_i_id = z.i_id)) + ( SELECT count(*) AS count
                   FROM public.pers_zeitdatenverrechnetzeitraum zv
                  WHERE (zv.zeitdaten_i_id = z.i_id))) = 0))
        UNION
         SELECT NULL::integer AS int4,
            t.i_id,
            NULL::integer AS int4,
            NULL::integer AS int4
           FROM public.pers_telefonzeiten t
          WHERE ((t.t_erledigt IS NOT NULL) AND (( SELECT count(*) AS count
                   FROM public.pers_telefonzeitenverrechnet tv
                  WHERE (tv.telefonzeiten_i_id = t.i_id)) = 0))
        UNION
         SELECT NULL::integer AS int4,
            NULL::integer AS int4,
            m.i_id,
            NULL::integer AS int4
           FROM public.pers_maschinenzeitdaten m
          WHERE ((m.t_erledigt IS NOT NULL) AND (( SELECT count(*) AS count
                   FROM public.pers_maschinenzeitdatenverrechnet mv
                  WHERE (mv.maschinenzeitdaten_i_id = m.i_id)) = 0))
        UNION
         SELECT NULL::integer AS int4,
            NULL::integer AS int4,
            NULL::integer AS int4,
            r.i_id
           FROM public.pers_reise r
          WHERE ((r.t_erledigt IS NOT NULL) AND (( SELECT count(*) AS count
                   FROM public.pers_reiseverrechnet rv
                  WHERE (rv.reise_i_id = r.i_id)) = 0))) a;
ALTER TABLE public.pers_zeitenmanuellerledigt OWNER TO postgres;
CREATE TABLE public.pers_zeitgutschrift (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    t_datum timestamp without time zone NOT NULL,
    u_gutschrift_kommt time without time zone NOT NULL,
    u_gutschrift_geht time without time zone NOT NULL
);
ALTER TABLE public.pers_zeitgutschrift OWNER TO postgres;
CREATE TABLE public.pers_zeitmodell (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_nr character varying(15) NOT NULL,
    b_teilzeit smallint NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    b_versteckt smallint NOT NULL,
    n_sollstundenfix numeric(17,6),
    f_urlaubstageprowoche double precision NOT NULL,
    b_dynamisch smallint NOT NULL,
    i_minutenabzug integer NOT NULL,
    b_feiertagssoll_addieren smallint NOT NULL,
    n_maximales_wochenist numeric(17,6),
    b_fixepause_trotzkommtgeht smallint NOT NULL,
    schicht_i_id integer,
    b_firmenzeitmodell smallint NOT NULL,
    n_maximale_mehrzeit numeric(17,6),
    u_gutschrift_kommt time without time zone,
    u_gutschrift_geht time without time zone,
    b_unproduktiv_als_pause smallint NOT NULL,
    b_feiertag_am_naechsten_tag smallint NOT NULL
);
ALTER TABLE public.pers_zeitmodell OWNER TO postgres;
CREATE TABLE public.pers_zeitmodellspr (
    zeitmodell_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.pers_zeitmodellspr OWNER TO postgres;
CREATE TABLE public.pers_zeitmodelltag (
    i_id integer NOT NULL,
    zeitmodell_i_id integer NOT NULL,
    tagesart_i_id integer NOT NULL,
    i_mindestpausenanzahl integer,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    u_sollzeit time without time zone NOT NULL,
    u_mindestpause time without time zone,
    u_beginn time without time zone,
    u_ende time without time zone,
    u_ueberstd time without time zone,
    u_mehrstd time without time zone,
    u_erlaubteanwesenheitszeit time without time zone,
    u_autopauseab time without time zone,
    i_rundungbeginn integer NOT NULL,
    i_rundungende integer NOT NULL,
    b_rundesondertaetigkeiten smallint,
    u_autopauseab2 time without time zone,
    u_mindestpause2 time without time zone,
    u_autopauseab3 time without time zone,
    u_mindestpause3 time without time zone,
    u_ende_akzeptiert_ab time without time zone,
    u_beginn_akzeptiert_bis time without time zone,
    u_schichterkennung time without time zone,
    b_beginn_vortag smallint NOT NULL
);
ALTER TABLE public.pers_zeitmodelltag OWNER TO postgres;
CREATE TABLE public.pers_zeitmodelltagpause (
    i_id integer NOT NULL,
    zeitmodelltag_i_id integer NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    u_beginn time without time zone NOT NULL,
    u_ende time without time zone NOT NULL
);
ALTER TABLE public.pers_zeitmodelltagpause OWNER TO postgres;
CREATE TABLE public.pers_zeitstift (
    i_id integer NOT NULL,
    c_nr character(15) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    b_mehrfachstift smallint NOT NULL,
    personal_i_id integer,
    b_personenzuordnung smallint NOT NULL,
    c_typ character varying(15)
);
ALTER TABLE public.pers_zeitstift OWNER TO postgres;
CREATE TABLE public.pers_zeitverteilung (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    los_i_id integer NOT NULL,
    artikel_i_id integer,
    t_zeit timestamp without time zone NOT NULL,
    lossollarbeitsplan_i_id integer,
    maschine_i_id integer,
    b_verteilt smallint NOT NULL,
    i_id_block integer,
    zeitdaten_i_id_umgewandelt integer
);
ALTER TABLE public.pers_zeitverteilung OWNER TO postgres;
CREATE TABLE public.pers_zulage (
    i_id integer NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.pers_zulage OWNER TO postgres;
CREATE TABLE public.pers_zutrittdaueroffen (
    i_id integer NOT NULL,
    tagesart_i_id integer NOT NULL,
    zutrittsobjekt_i_id integer NOT NULL,
    u_offenvon time without time zone NOT NULL,
    u_offenbis time without time zone NOT NULL
);
ALTER TABLE public.pers_zutrittdaueroffen OWNER TO postgres;
CREATE TABLE public.pers_zutrittonlinecheck (
    i_id integer NOT NULL,
    zutrittsklasse_i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_pincode character varying(40),
    c_ausweis character varying(80),
    t_gueltigab timestamp without time zone NOT NULL,
    t_gueltigbis timestamp without time zone NOT NULL
);
ALTER TABLE public.pers_zutrittonlinecheck OWNER TO postgres;
CREATE TABLE public.pers_zutrittscontroller (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    c_bez character varying(80),
    c_adresse character varying(40) NOT NULL
);
ALTER TABLE public.pers_zutrittscontroller OWNER TO postgres;
CREATE TABLE public.pers_zutrittsklasse (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.pers_zutrittsklasse OWNER TO postgres;
CREATE TABLE public.pers_zutrittsklasseobjekt (
    i_id integer NOT NULL,
    zutrittsmodell_i_id integer NOT NULL,
    zutrittsklasse_i_id integer NOT NULL,
    zutrittsobjekt_i_id integer NOT NULL
);
ALTER TABLE public.pers_zutrittsklasseobjekt OWNER TO postgres;
CREATE TABLE public.pers_zutrittsleser (
    c_nr character(15) NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.pers_zutrittsleser OWNER TO postgres;
CREATE TABLE public.pers_zutrittslog (
    i_id integer NOT NULL,
    c_ausweis character varying(40),
    c_person character varying(80),
    c_zutrittscontroller character varying(55) NOT NULL,
    c_zutrittsobjekt character varying(55) NOT NULL,
    t_zeitpunkt timestamp without time zone NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    b_erlaubt smallint NOT NULL,
    mandant_c_nr_objekt character varying(3)
);
ALTER TABLE public.pers_zutrittslog OWNER TO postgres;
CREATE TABLE public.pers_zutrittsmodell (
    i_id integer NOT NULL,
    c_nr character varying(40) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    x_bem text
);
ALTER TABLE public.pers_zutrittsmodell OWNER TO postgres;
CREATE TABLE public.pers_zutrittsmodelltag (
    i_id integer NOT NULL,
    zutrittsmodell_i_id integer NOT NULL,
    tagesart_i_id integer NOT NULL,
    x_bem text
);
ALTER TABLE public.pers_zutrittsmodelltag OWNER TO postgres;
CREATE TABLE public.pers_zutrittsmodelltagdetail (
    i_id integer NOT NULL,
    zutrittsmodelltag_i_id integer NOT NULL,
    zutrittsoeffnungsart_c_nr character(15) NOT NULL,
    b_restdestages character(15) NOT NULL,
    u_offenvon time without time zone NOT NULL,
    u_offenbis time without time zone
);
ALTER TABLE public.pers_zutrittsmodelltagdetail OWNER TO postgres;
CREATE TABLE public.pers_zutrittsobjekt (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    c_adresse character varying(40) NOT NULL,
    mandant_c_nr character varying(3),
    c_bez character varying(80),
    zutrittscontroller_i_id integer NOT NULL,
    c_relais character varying(10) NOT NULL,
    f_oeffnungszeit double precision NOT NULL,
    zutrittsleser_c_nr character(15) NOT NULL
);
ALTER TABLE public.pers_zutrittsobjekt OWNER TO postgres;
CREATE TABLE public.pers_zutrittsobjektverwendung (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    i_anzahlverwendung integer NOT NULL,
    zutrittsobjekt_i_id integer NOT NULL
);
ALTER TABLE public.pers_zutrittsobjektverwendung OWNER TO postgres;
CREATE TABLE public.pers_zutrittsoeffnungsart (
    c_nr character(15) NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.pers_zutrittsoeffnungsart OWNER TO postgres;
CREATE TABLE public.proj_bereich (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    i_sort integer NOT NULL,
    b_projekt_mit_betreiber smallint NOT NULL,
    b_projekt_mit_artikel smallint NOT NULL,
    b_projekt_artikel_eindeutig smallint NOT NULL,
    b_projekt_artikel_pflichtfeld smallint NOT NULL,
    b_durchgefuehrt_von_in_offene smallint NOT NULL,
    b_detailtext_ist_pflichtfeld smallint NOT NULL
);
ALTER TABLE public.proj_bereich OWNER TO postgres;
CREATE TABLE public.proj_history (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    projekt_i_id integer NOT NULL,
    t_belegdatum timestamp without time zone NOT NULL,
    x_text text NOT NULL,
    o_attachments bytea,
    c_attachmentstype character(15),
    c_titel character varying(80),
    historyart_i_id integer,
    b_html smallint NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_wirddurchgefuehrt integer,
    f_erledigungsgrad double precision NOT NULL,
    n_dauer_geplant numeric(15,4)
);
ALTER TABLE public.proj_history OWNER TO postgres;
CREATE TABLE public.proj_historyart (
    i_id integer NOT NULL,
    c_bez character varying(40) NOT NULL,
    i_rot integer NOT NULL,
    i_gruen integer NOT NULL,
    i_blau integer NOT NULL,
    b_aktualisierezieltermin smallint NOT NULL,
    b_in_auswahlliste_anzeigen smallint NOT NULL
);
ALTER TABLE public.proj_historyart OWNER TO postgres;
CREATE TABLE public.proj_kategorie (
    c_nr character(15) NOT NULL,
    i_sort integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.proj_kategorie OWNER TO postgres;
CREATE TABLE public.proj_kategoriespr (
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40) NOT NULL,
    kategorie_c_nr character(15) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.proj_kategoriespr OWNER TO postgres;
CREATE TABLE public.proj_leadstatus (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.proj_leadstatus OWNER TO postgres;
CREATE TABLE public.proj_projekt (
    i_id integer NOT NULL,
    kategorie_c_nr character(15) NOT NULL,
    c_titel character varying(120) NOT NULL,
    personal_i_id_erzeuger integer NOT NULL,
    personal_i_id_zugewiesener integer NOT NULL,
    i_prio integer NOT NULL,
    o_attachments bytea,
    x_freetext text,
    t_zielwunschdatum timestamp without time zone NOT NULL,
    partner_i_id integer NOT NULL,
    ansprechpartner_i_id integer,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    projektstatus_c_nr character(15) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    t_zeit timestamp without time zone NOT NULL,
    c_dateiname character varying(260),
    c_attachmentstype character varying(40),
    projekttyp_c_nr character varying(30),
    t_erledigt timestamp without time zone,
    personal_i_id_erlediger integer,
    f_dauer double precision,
    c_nr character varying(15) NOT NULL,
    b_freigegeben smallint NOT NULL,
    i_sort integer,
    n_umsatzgeplant numeric(17,6),
    i_wahrscheinlichkeit integer,
    projekterledigungsgrund_i_id integer,
    bereich_i_id integer NOT NULL,
    projekt_i_id_nachfolger integer,
    t_internerledigt timestamp without time zone,
    personal_i_id_internerledigt integer,
    c_buildnumber character varying(30),
    c_deploynumber character varying(30),
    partner_i_id_betreiber integer,
    vkfortschritt_i_id integer,
    t_realisierung timestamp without time zone,
    ansprechpartner_i_id_betreiber integer,
    artikel_i_id integer,
    i_verrechenbar integer NOT NULL,
    editorcontent_i_id integer
);
ALTER TABLE public.proj_projekt OWNER TO postgres;
CREATE TABLE public.proj_projekterledigungsgrund (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.proj_projekterledigungsgrund OWNER TO postgres;
CREATE TABLE public.proj_projektgruppe (
    i_id integer NOT NULL,
    projekt_i_id_vater integer NOT NULL,
    projekt_i_id_kind integer NOT NULL
);
ALTER TABLE public.proj_projektgruppe OWNER TO postgres;
CREATE TABLE public.proj_projektstatus (
    i_sort integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_nr character(15) NOT NULL,
    b_aenderungprotokollieren smallint NOT NULL,
    b_erledigt smallint NOT NULL,
    b_gesperrt smallint NOT NULL
);
ALTER TABLE public.proj_projektstatus OWNER TO postgres;
CREATE TABLE public.proj_projekttaetigkeit (
    i_id integer NOT NULL,
    projekt_i_id integer NOT NULL,
    artikel_i_id integer NOT NULL
);
ALTER TABLE public.proj_projekttaetigkeit OWNER TO postgres;
CREATE TABLE public.proj_projekttechniker (
    i_id integer NOT NULL,
    projekt_i_id integer NOT NULL,
    personal_i_id integer NOT NULL
);
ALTER TABLE public.proj_projekttechniker OWNER TO postgres;
CREATE TABLE public.proj_projekttyp (
    c_nr character varying(30) NOT NULL,
    i_sort integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.proj_projekttyp OWNER TO postgres;
CREATE TABLE public.proj_projekttypspr (
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40) NOT NULL,
    projekttyp_c_nr character varying(30) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.proj_projekttypspr OWNER TO postgres;
CREATE VIEW public.proj_textsuche AS
 SELECT DISTINCT 'PR '::text AS c_typ,
    p.i_id,
    p.c_titel AS c_suche
   FROM public.proj_projekt p
  WHERE (NOT (p.c_titel IS NULL))
UNION
 SELECT DISTINCT 'PR '::text AS c_typ,
    p.i_id,
    (p.x_freetext)::character varying(4000) AS c_suche
   FROM public.proj_projekt p
  WHERE (((p.x_freetext)::character varying(4000))::text <> ''::text)
UNION
 SELECT DISTINCT 'HT '::text AS c_typ,
    p.i_id,
    h.c_titel AS c_suche
   FROM (public.proj_projekt p
     JOIN public.proj_history h ON ((h.projekt_i_id = p.i_id)))
  WHERE (NOT (h.c_titel IS NULL))
UNION
 SELECT DISTINCT ' H '::text AS c_typ,
    p.i_id,
    (h.x_text)::character varying(4000) AS c_suche
   FROM (public.proj_projekt p
     JOIN public.proj_history h ON ((h.projekt_i_id = p.i_id)))
  WHERE (((h.x_text)::character varying(4000))::text <> ''::text);
ALTER TABLE public.proj_textsuche OWNER TO postgres;
CREATE VIEW public.proj_titelaushistory AS
 SELECT p.i_id,
    ( SELECT
                CASE
                    WHEN (( SELECT count(*) AS count
                       FROM public.proj_history
                      WHERE ((proj_history.projekt_i_id = p.i_id) AND (proj_history.historyart_i_id = ( SELECT proj_historyart.i_id
                               FROM public.proj_historyart
                              WHERE (proj_historyart.b_in_auswahlliste_anzeigen = 1)
                             LIMIT 1)))) = 1) THEN ( SELECT proj_history.c_titel
                       FROM public.proj_history
                      WHERE ((proj_history.projekt_i_id = p.i_id) AND (proj_history.historyart_i_id = ( SELECT proj_historyart.i_id
                               FROM public.proj_historyart
                              WHERE (proj_historyart.b_in_auswahlliste_anzeigen = 1)
                             LIMIT 1))))
                    ELSE ( SELECT proj_history.c_titel
                       FROM public.proj_history
                      WHERE ((proj_history.projekt_i_id = p.i_id) AND (proj_history.historyart_i_id = ( SELECT proj_historyart.i_id
                               FROM public.proj_historyart
                              WHERE (proj_historyart.b_in_auswahlliste_anzeigen = 1))) AND (proj_history.t_belegdatum <= CURRENT_DATE))
                      ORDER BY proj_history.t_belegdatum DESC
                     LIMIT 1)
                END AS c_titel) AS c_titel
   FROM public.proj_projekt p;
ALTER TABLE public.proj_titelaushistory OWNER TO postgres;
CREATE TABLE public.proj_vkfortschritt (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_nr character varying(40) NOT NULL,
    i_sort integer NOT NULL,
    leadstatus_i_id integer
);
ALTER TABLE public.proj_vkfortschritt OWNER TO postgres;
CREATE TABLE public.proj_vkfortschrittspr (
    locale_c_nr character(10) NOT NULL,
    vkfortschritt_i_id integer NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.proj_vkfortschrittspr OWNER TO postgres;
CREATE TABLE public.rech_abrechnungsvorschlag (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    auftrag_i_id integer,
    los_i_id integer,
    projekt_i_id integer,
    zeitdaten_i_id integer,
    reise_i_id integer,
    kunde_i_id integer,
    auftragszuordnung_i_id integer,
    n_betrag_offen numeric(17,6),
    n_stunden_offen numeric(17,6),
    t_anlegen timestamp without time zone NOT NULL,
    telefonzeiten_i_id integer,
    t_von timestamp without time zone NOT NULL,
    t_bis timestamp without time zone,
    personal_i_id integer,
    maschinenzeitdaten_i_id integer,
    b_verrechnet smallint NOT NULL,
    n_betrag_gesamt numeric(17,6),
    n_stunden_gesamt numeric(17,6),
    f_verrechenbar double precision NOT NULL,
    n_betrag_verrechenbar numeric(17,6),
    n_stunden_verrechenbar numeric(17,6),
    n_kilometer_gesamt numeric(17,6),
    n_kilometer_verrechenbar numeric(17,6),
    n_kilometer_offen numeric(17,6),
    auftragposition_i_id integer,
    n_spesen_gesamt numeric(17,6),
    n_spesen_verrechenbar numeric(17,6),
    n_spesen_offen numeric(17,6),
    waehrung_c_nr_rechnung character(3) NOT NULL
);
ALTER TABLE public.rech_abrechnungsvorschlag OWNER TO postgres;
CREATE TABLE public.rech_gutschriftpositionsart (
    positionsart_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL,
    b_versteckt smallint NOT NULL
);
ALTER TABLE public.rech_gutschriftpositionsart OWNER TO postgres;
CREATE TABLE public.rech_gutschriftsgrund (
    i_id integer NOT NULL,
    c_nr character varying(50) NOT NULL
);
ALTER TABLE public.rech_gutschriftsgrund OWNER TO postgres;
CREATE TABLE public.rech_gutschriftsgrundspr (
    gutschriftsgrund_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.rech_gutschriftsgrundspr OWNER TO postgres;
CREATE TABLE public.rech_gutschrifttext (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_nr character varying(15) NOT NULL,
    x_textinhalt text NOT NULL
);
ALTER TABLE public.rech_gutschrifttext OWNER TO postgres;
CREATE TABLE public.rech_lastschriftvorschlag (
    i_id integer NOT NULL,
    mahnlauf_i_id integer NOT NULL,
    rechnung_i_id integer NOT NULL,
    n_rechnungsbetrag numeric(15,2) NOT NULL,
    n_bereits_bezahlt numeric(15,2) NOT NULL,
    n_zahlbetrag numeric(15,2) NOT NULL,
    t_faellig timestamp without time zone NOT NULL,
    c_auftraggeberreferenz character varying(35),
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_gespeichert integer,
    t_gespeichert timestamp without time zone,
    c_verwendungszweck character varying(140)
);
ALTER TABLE public.rech_lastschriftvorschlag OWNER TO postgres;
CREATE TABLE public.rech_mmz (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    artikel_i_id integer NOT NULL,
    n_bis_wert numeric(17,6) NOT NULL,
    n_zuschlag numeric(17,6) NOT NULL,
    land_i_id integer
);
ALTER TABLE public.rech_mmz OWNER TO postgres;
CREATE TABLE public.rech_proformarechnungpositionsart (
    positionsart_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.rech_proformarechnungpositionsart OWNER TO postgres;
CREATE TABLE public.rech_rechnungart (
    c_nr character varying(30) NOT NULL,
    rechnungtyp_c_nr character varying(10) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.rech_rechnungart OWNER TO postgres;
CREATE TABLE public.rech_rechnungartspr (
    rechnungart_c_nr character varying(30) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.rech_rechnungartspr OWNER TO postgres;
CREATE TABLE public.rech_rechnungkontierung (
    i_id integer NOT NULL,
    rechnung_i_id integer NOT NULL,
    kostenstelle_i_id integer NOT NULL,
    n_prozentsatz numeric(15,2) NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.rech_rechnungkontierung OWNER TO postgres;
CREATE TABLE public.rech_rechnungpositionsart (
    positionsart_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL,
    b_versteckt smallint NOT NULL
);
ALTER TABLE public.rech_rechnungpositionsart OWNER TO postgres;
CREATE TABLE public.rech_rechnungstatus (
    status_c_nr character(15) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.rech_rechnungstatus OWNER TO postgres;
CREATE TABLE public.rech_rechnungtext (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_nr character varying(15) NOT NULL,
    x_textinhalt text NOT NULL
);
ALTER TABLE public.rech_rechnungtext OWNER TO postgres;
CREATE TABLE public.rech_rechnungtyp (
    c_nr character varying(10) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.rech_rechnungtyp OWNER TO postgres;
CREATE VIEW public.rech_textsuche AS
 SELECT DISTINCT 'A '::text AS c_typ,
    g.i_id,
    g.c_nr,
    (((COALESCE(a.c_bez, ''::character varying))::text || (COALESCE(a.c_zbez, ''::character varying))::text) || (COALESCE(a.c_zbez2, ''::character varying))::text) AS c_suche
   FROM ((public.rech_rechnung g
     JOIN public.rech_rechnungposition p ON ((g.i_id = p.rechnung_i_id)))
     JOIN public.ww_artikelspr a ON ((a.artikel_i_id = p.artikel_i_id)))
UNION
 SELECT DISTINCT 'P '::text AS c_typ,
    g.i_id,
    g.c_nr,
    ((COALESCE(p.c_bez, ''::character varying))::text || (COALESCE(p.c_zbez, ''::character varying))::text) AS c_suche
   FROM (public.rech_rechnung g
     JOIN public.rech_rechnungposition p ON ((g.i_id = p.rechnung_i_id)))
UNION
 SELECT DISTINCT 'PT '::text AS c_typ,
    g.i_id,
    g.c_nr,
    (p.x_textinhalt)::character varying(4000) AS c_suche
   FROM (public.rech_rechnung g
     JOIN public.rech_rechnungposition p ON ((g.i_id = p.rechnung_i_id)))
UNION
 SELECT DISTINCT 'RECH'::text AS c_typ,
    rech.i_id,
    rech.c_nr,
    rech.c_bez AS c_suche
   FROM public.rech_rechnung rech
UNION
 SELECT DISTINCT 'ID'::text AS c_typ,
    g.i_id,
    g.c_nr,
    ((a.c_nr)::text || (COALESCE(a.c_referenznr, ''::character varying))::text) AS c_suche
   FROM ((public.rech_rechnung g
     JOIN public.rech_rechnungposition p ON ((g.i_id = p.rechnung_i_id)))
     JOIN public.ww_artikel a ON ((a.i_id = p.artikel_i_id)))
UNION
 SELECT DISTINCT 'KD'::text AS c_typ,
    g.i_id,
    g.c_nr,
    ((COALESCE(p.c_name1nachnamefirmazeile1, ''::character varying))::text || (COALESCE(p.c_name2vornamefirmazeile2, ''::character varying))::text) AS c_suche
   FROM ((public.rech_rechnung g
     JOIN public.part_kunde k ON ((k.i_id = g.kunde_i_id)))
     JOIN public.part_partner p ON ((p.i_id = k.partner_i_id)))
UNION
 SELECT DISTINCT 'LPO'::text AS c_typ,
    g.i_id,
    g.c_nr,
    (((((((l.c_lkz)::text || '-'::text) || (lpo.c_plz)::text) || ' '::text) || (o.c_name)::text) || ' '::text) || (p.c_strasse)::text) AS c_suche
   FROM (((((public.lp_landplzort lpo
     JOIN public.part_partner p ON ((lpo.i_id = p.landplzort_i_id)))
     JOIN public.lp_land l ON ((lpo.land_i_id = l.i_id)))
     JOIN public.lp_ort o ON ((lpo.ort_i_id = o.i_id)))
     JOIN public.part_lieferant k ON ((p.i_id = k.partner_i_id)))
     JOIN public.rech_rechnung g ON ((k.i_id = g.kunde_i_id)));
ALTER TABLE public.rech_textsuche OWNER TO postgres;
CREATE TABLE public.rech_verrechnungsmodell (
    i_id integer NOT NULL,
    c_bez character varying(40) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    b_versteckt smallint NOT NULL,
    artikel_i_id_reisespesen integer NOT NULL,
    artikel_i_id_reisekilometer integer NOT NULL,
    artikel_i_id_telefon integer NOT NULL,
    artikel_i_id_er integer NOT NULL,
    f_aufschlag_er double precision NOT NULL,
    b_preise_aus_auftrag smallint NOT NULL,
    b_nach_artikel_verdichten smallint NOT NULL
);
ALTER TABLE public.rech_verrechnungsmodell OWNER TO postgres;
CREATE TABLE public.rech_verrechnungsmodelltag (
    i_id integer NOT NULL,
    verrechnungsmodell_i_id integer NOT NULL,
    tagesart_i_id integer NOT NULL,
    artikel_i_id_gebucht integer NOT NULL,
    u_dauer_ab time without time zone,
    u_zeitraum_von time without time zone,
    u_zeitraum_bis time without time zone,
    artikel_i_id_zuverrechnen integer NOT NULL,
    b_endedestages smallint NOT NULL
);
ALTER TABLE public.rech_verrechnungsmodelltag OWNER TO postgres;
CREATE TABLE public.rech_vorkasseposition (
    i_id integer NOT NULL,
    rechnung_i_id integer NOT NULL,
    auftragsposition_i_id integer NOT NULL,
    n_betrag numeric(17,6) NOT NULL
);
ALTER TABLE public.rech_vorkasseposition OWNER TO postgres;
CREATE TABLE public.rech_zahlungsart (
    c_nr character varying(20) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.rech_zahlungsart OWNER TO postgres;
CREATE TABLE public.rech_zahlungsartspr (
    zahlungsart_c_nr character varying(20) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.rech_zahlungsartspr OWNER TO postgres;
CREATE TABLE public.rekla_aufnahmeart (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.rekla_aufnahmeart OWNER TO postgres;
CREATE TABLE public.rekla_aufnahmeartspr (
    locale_c_nr character(10) NOT NULL,
    aufnahmeart_i_id integer NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.rekla_aufnahmeartspr OWNER TO postgres;
CREATE TABLE public.rekla_behandlung (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    i_punkte integer NOT NULL
);
ALTER TABLE public.rekla_behandlung OWNER TO postgres;
CREATE TABLE public.rekla_behandlungspr (
    locale_c_nr character(10) NOT NULL,
    behandlung_i_id integer NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.rekla_behandlungspr OWNER TO postgres;
CREATE TABLE public.rekla_fehler (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.rekla_fehler OWNER TO postgres;
CREATE TABLE public.rekla_fehlerangabe (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.rekla_fehlerangabe OWNER TO postgres;
CREATE TABLE public.rekla_fehlerangabespr (
    locale_c_nr character(10) NOT NULL,
    fehlerangabe_i_id integer NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.rekla_fehlerangabespr OWNER TO postgres;
CREATE TABLE public.rekla_fehlerspr (
    locale_c_nr character(10) NOT NULL,
    fehler_i_id integer NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.rekla_fehlerspr OWNER TO postgres;
CREATE TABLE public.rekla_massnahme (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.rekla_massnahme OWNER TO postgres;
CREATE TABLE public.rekla_massnahmespr (
    locale_c_nr character(10) NOT NULL,
    massnahme_i_id integer NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.rekla_massnahmespr OWNER TO postgres;
CREATE TABLE public.rekla_reklamation (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_nr character varying(15) NOT NULL,
    reklamationart_c_nr character(15) NOT NULL,
    t_belegdatum timestamp without time zone NOT NULL,
    kostenstelle_i_id integer NOT NULL,
    fehlerangabe_i_id integer NOT NULL,
    aufnahmeart_i_id integer NOT NULL,
    personal_i_id_aufnehmer integer NOT NULL,
    b_artikel smallint NOT NULL,
    artikel_i_id integer,
    c_handartikel character varying(80),
    n_menge numeric(17,6) NOT NULL,
    b_fremdprodukt smallint NOT NULL,
    c_grund character varying(80),
    los_i_id integer,
    bestellung_i_id integer,
    lieferschein_i_id integer,
    rechnung_i_id integer,
    kunde_i_id integer,
    lieferant_i_id integer,
    ansprechpartner_i_id integer,
    c_projekt character varying(80),
    x_analyse text,
    x_kommentar text,
    fehler_i_id integer,
    b_berechtigt smallint NOT NULL,
    personal_i_id_ruecksprache integer,
    t_ruecksprache timestamp without time zone,
    c_ruecksprachemit character varying(80),
    n_kostenmaterial numeric(17,6),
    n_kostenarbeitszeit numeric(17,6),
    massnahme_i_id_kurz integer,
    t_massnahmebiskurz timestamp without time zone,
    t_eingefuehrtkurz timestamp without time zone,
    personal_i_id_eingefuehrtkurz integer,
    massnahme_i_id_mittel integer,
    t_massnahmebismittel timestamp without time zone,
    t_eingefuehrtmittel timestamp without time zone,
    personal_i_id_eingefuehrtmittel integer,
    massnahme_i_id_lang integer,
    t_massnahmebislang timestamp without time zone,
    t_eingefuehrtlang timestamp without time zone,
    personal_i_id_eingefuehrtlang integer,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    t_erledigt timestamp without time zone,
    personal_i_id_erledigt integer,
    schwere_i_id integer,
    behandlung_i_id integer,
    wareneingang_i_id integer,
    wirksamkeit_i_id integer,
    x_wirksamkeit text,
    x_massnahme_lang text,
    x_massnahme_kurz text,
    x_massnahme_mittel text,
    b_betrifftlagerstand smallint NOT NULL,
    b_betrifftgeliferte smallint NOT NULL,
    n_stuecklagerstand numeric(17,6),
    n_stueckgelieferte numeric(17,6),
    x_grund_lang text,
    t_wirksamkeitbis timestamp without time zone,
    t_wirksamkeiteingefuehrt timestamp without time zone,
    personal_i_id_wirksamkeit integer,
    status_c_nr character(15) NOT NULL,
    maschine_i_id integer,
    c_kdreklanr character varying(40),
    c_kdlsnr character varying(40),
    personal_i_id_verursacher integer,
    lossollarbeitsplan_i_id integer,
    c_seriennrchargennr character varying(80),
    i_kundeunterart integer,
    ansprechpartner_i_id_lieferant integer,
    projekt_i_id integer,
    c_lfreklanr character varying(40),
    c_lflsnr character varying(40),
    t_ware_erhalten timestamp without time zone,
    c_bestellnummer character varying(40),
    c_wareneingang character varying(40)
);
ALTER TABLE public.rekla_reklamation OWNER TO postgres;
CREATE TABLE public.rekla_reklamationart (
    c_nr character(15) NOT NULL
);
ALTER TABLE public.rekla_reklamationart OWNER TO postgres;
CREATE TABLE public.rekla_reklamationbild (
    i_id integer NOT NULL,
    reklamation_i_id integer NOT NULL,
    i_sort integer NOT NULL,
    o_bild bytea NOT NULL,
    c_bez character varying(80),
    c_dateiname character varying(260),
    datenformat_c_nr character varying(40)
);
ALTER TABLE public.rekla_reklamationbild OWNER TO postgres;
CREATE TABLE public.rekla_schwere (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    i_punkte integer NOT NULL
);
ALTER TABLE public.rekla_schwere OWNER TO postgres;
CREATE TABLE public.rekla_schwerespr (
    locale_c_nr character(10) NOT NULL,
    schwere_i_id integer NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.rekla_schwerespr OWNER TO postgres;
CREATE TABLE public.rekla_termintreue (
    i_id integer NOT NULL,
    i_tage integer NOT NULL,
    i_punkte integer NOT NULL
);
ALTER TABLE public.rekla_termintreue OWNER TO postgres;
CREATE TABLE public.rekla_wirksamkeit (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.rekla_wirksamkeit OWNER TO postgres;
CREATE TABLE public.rekla_wirksamkeitspr (
    locale_c_nr character(10) NOT NULL,
    wirksamkeit_i_id integer NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.rekla_wirksamkeitspr OWNER TO postgres;
CREATE TABLE public.stk_agart (
    c_nr character(15) NOT NULL
);
ALTER TABLE public.stk_agart OWNER TO postgres;
CREATE TABLE public.stk_alternativmaschine (
    i_id integer NOT NULL,
    stuecklistearbeitsplan_i_id integer NOT NULL,
    maschine_i_id integer NOT NULL,
    i_sort integer NOT NULL,
    n_korrekturfaktor numeric(15,4) NOT NULL
);
ALTER TABLE public.stk_alternativmaschine OWNER TO postgres;
CREATE TABLE public.stk_apkommentar (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_nr character varying(15) NOT NULL
);
ALTER TABLE public.stk_apkommentar OWNER TO postgres;
CREATE TABLE public.stk_apkommentarspr (
    locale_c_nr character(10) NOT NULL,
    apkommentar_i_id integer NOT NULL,
    c_bez character varying(3000)
);
ALTER TABLE public.stk_apkommentarspr OWNER TO postgres;
CREATE TABLE public.stk_fertigungsgruppe (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(40) NOT NULL,
    i_sort integer NOT NULL,
    i_formularnummer integer
);
ALTER TABLE public.stk_fertigungsgruppe OWNER TO postgres;
CREATE TABLE public.stk_kommentarimport (
    i_id integer NOT NULL,
    belegart_c_nr character(15) NOT NULL,
    artikelkommentarart_i_id integer NOT NULL
);
ALTER TABLE public.stk_kommentarimport OWNER TO postgres;
CREATE TABLE public.stk_montageart (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(40) NOT NULL,
    i_sort integer NOT NULL,
    artikel_i_id integer
);
ALTER TABLE public.stk_montageart OWNER TO postgres;
CREATE TABLE public.stk_posersatz (
    i_id integer NOT NULL,
    stuecklisteposition_i_id integer NOT NULL,
    artikel_i_id_ersatz integer NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.stk_posersatz OWNER TO postgres;
CREATE TABLE public.stk_profirstignore (
    c_kbez character varying(40) NOT NULL
);
ALTER TABLE public.stk_profirstignore OWNER TO postgres;
CREATE TABLE public.stk_pruefart (
    i_id integer NOT NULL,
    c_nr character varying(40) NOT NULL
);
ALTER TABLE public.stk_pruefart OWNER TO postgres;
CREATE TABLE public.stk_pruefartspr (
    locale_c_nr character(10) NOT NULL,
    pruefart_i_id integer NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.stk_pruefartspr OWNER TO postgres;
CREATE TABLE public.stk_pruefkombination (
    i_id integer NOT NULL,
    artikel_i_id_kontakt integer,
    artikel_i_id_litze integer,
    b_standard smallint NOT NULL,
    n_crimphoehe_draht numeric(15,4),
    n_crimphoehe_isolation numeric(15,4),
    n_crimpbreite_draht numeric(15,4),
    n_crimpbreite_isolation numeric(15,4),
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    pruefart_i_id integer NOT NULL,
    n_toleranz_crimphoehe_draht numeric(15,4),
    n_toleranz_crimphoehe_isolation numeric(15,4),
    n_toleranz_crimpbreite_draht numeric(15,4),
    n_toleranz_crimpbreite_isolation numeric(15,4),
    n_wert numeric(15,4),
    n_toleranz_wert numeric(15,4),
    verschleissteil_i_id integer,
    b_doppelanschlag integer NOT NULL,
    artikel_i_id_litze2 integer,
    n_abzugskraft_litze numeric(15,4),
    n_abzugskraft_litze2 numeric(15,4)
);
ALTER TABLE public.stk_pruefkombination OWNER TO postgres;
CREATE TABLE public.stk_pruefkombinationspr (
    locale_c_nr character(10) NOT NULL,
    pruefkombination_i_id integer NOT NULL,
    c_bez character varying(3000)
);
ALTER TABLE public.stk_pruefkombinationspr OWNER TO postgres;
CREATE TABLE public.stk_stklagerentnahme (
    i_id integer NOT NULL,
    stueckliste_i_id integer NOT NULL,
    lager_i_id integer NOT NULL,
    i_sort integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.stk_stklagerentnahme OWNER TO postgres;
CREATE TABLE public.stk_stklparameter (
    i_id integer NOT NULL,
    stueckliste_i_id integer NOT NULL,
    c_nr character varying(120) NOT NULL,
    c_typ character(80) NOT NULL,
    n_min numeric(15,4),
    n_max numeric(15,4),
    i_sort integer NOT NULL,
    b_combobox smallint NOT NULL,
    c_bereich character varying(3000),
    b_mandatory smallint NOT NULL,
    c_wert character varying(600)
);
ALTER TABLE public.stk_stklparameter OWNER TO postgres;
CREATE TABLE public.stk_stklparameterspr (
    stklparameter_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.stk_stklparameterspr OWNER TO postgres;
CREATE TABLE public.stk_stklpruefplan (
    i_id integer NOT NULL,
    stueckliste_i_id integer NOT NULL,
    stuecklisteposition_i_id_kontakt integer,
    stuecklisteposition_i_id_litze integer,
    verschleissteil_i_id integer,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    pruefart_i_id integer NOT NULL,
    stuecklisteposition_i_id_litze2 integer,
    b_doppelanschlag smallint NOT NULL,
    i_sort integer NOT NULL,
    pruefkombination_i_id integer
);
ALTER TABLE public.stk_stklpruefplan OWNER TO postgres;
CREATE TABLE public.stk_stuecklistearbeitsplan (
    i_id integer NOT NULL,
    stueckliste_i_id integer NOT NULL,
    i_arbeitsgang integer NOT NULL,
    artikel_i_id integer NOT NULL,
    l_stueckzeit bigint NOT NULL,
    l_ruestzeit bigint NOT NULL,
    c_kommentar character varying(80),
    x_langtext text,
    maschine_i_id integer,
    agart_c_nr character(15),
    i_aufspannung integer,
    i_unterarbeitsgang integer,
    b_nurmaschinenzeit smallint NOT NULL,
    i_maschinenversatztage integer,
    stuecklisteposition_i_id integer,
    apkommentar_i_id integer,
    n_ppm numeric(15,4),
    x_formel text,
    i_mitarbeitergleichzeitig integer,
    b_initial smallint NOT NULL
);
ALTER TABLE public.stk_stuecklistearbeitsplan OWNER TO postgres;
CREATE TABLE public.stk_stuecklisteart (
    c_nr character(15) NOT NULL,
    c_bez character varying(40) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.stk_stuecklisteart OWNER TO postgres;
CREATE TABLE public.stk_stuecklisteeigenschaft (
    i_id integer NOT NULL,
    stueckliste_i_id integer NOT NULL,
    stuecklisteeigenschaftart_i_id integer NOT NULL,
    c_bez character varying(40) NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.stk_stuecklisteeigenschaft OWNER TO postgres;
CREATE TABLE public.stk_stuecklisteeigenschaftart (
    i_id integer NOT NULL,
    c_bez character varying(40) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.stk_stuecklisteeigenschaftart OWNER TO postgres;
CREATE TABLE public.stk_stuecklisteposition (
    i_id integer NOT NULL,
    stueckliste_i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    einheit_c_nr character(15) NOT NULL,
    f_dimension1 double precision,
    f_dimension2 double precision,
    f_dimension3 double precision,
    c_position character varying(3000),
    c_kommentar character varying(80),
    montageart_i_id integer NOT NULL,
    i_lfdnummer integer,
    i_sort integer NOT NULL,
    b_mitdrucken smallint NOT NULL,
    n_kalkpreis numeric(17,6),
    personal_i_id_anlegen integer NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    i_beginnterminoffset integer NOT NULL,
    ansprechpartner_i_id_anlegen integer,
    t_anlegen_ansprechpartner timestamp without time zone,
    ansprechpartner_i_id_aendern integer,
    t_aendern_ansprechpartner timestamp without time zone,
    b_ruestmenge smallint NOT NULL,
    x_formel text,
    b_initial smallint NOT NULL
);
ALTER TABLE public.stk_stuecklisteposition OWNER TO postgres;
CREATE TABLE public.stk_stuecklistescriptart (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(40) NOT NULL,
    c_script character varying(40) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.stk_stuecklistescriptart OWNER TO postgres;
CREATE VIEW public.stk_textsuche AS
 SELECT DISTINCT 'SP'::text AS c_typ,
    s.i_id,
    ((((((((COALESCE(artikel.c_nr, ''::character varying))::text || (COALESCE(artikel.c_referenznr, ''::character varying))::text) || (COALESCE(a.c_bez, ''::character varying))::text) || (COALESCE(a.c_kbez, ''::character varying))::text) || (COALESCE(a.c_zbez, ''::character varying))::text) || (COALESCE(a.c_zbez2, ''::character varying))::text) || (COALESCE(p.c_position, ''::character varying))::text) || (COALESCE(p.c_kommentar, ''::character varying))::text) AS c_suche
   FROM (((public.stk_stueckliste s
     JOIN public.stk_stuecklisteposition p ON ((s.i_id = p.stueckliste_i_id)))
     JOIN public.ww_artikelspr a ON ((a.artikel_i_id = p.artikel_i_id)))
     LEFT JOIN public.ww_artikel artikel ON ((artikel.i_id = p.artikel_i_id)))
UNION
 SELECT DISTINCT 'ST'::text AS c_typ,
    s.i_id,
    ((((((COALESCE(artikel.c_nr, ''::character varying))::text || (COALESCE(artikel.c_referenznr, ''::character varying))::text) || (COALESCE(a.c_bez, ''::character varying))::text) || (COALESCE(a.c_zbez, ''::character varying))::text) || (COALESCE(a.c_kbez, ''::character varying))::text) || (COALESCE(a.c_zbez2, ''::character varying))::text) AS c_suche
   FROM ((public.stk_stueckliste s
     JOIN public.ww_artikelspr a ON ((a.artikel_i_id = s.artikel_i_id)))
     LEFT JOIN public.ww_artikel artikel ON ((artikel.i_id = s.artikel_i_id)))
UNION
 SELECT DISTINCT 'AP'::text AS c_typ,
    s.i_id,
    ((((((((COALESCE(artikel.c_nr, ''::character varying))::text || (COALESCE(artikel.c_referenznr, ''::character varying))::text) || (COALESCE(a.c_bez, ''::character varying))::text) || (COALESCE(a.c_zbez, ''::character varying))::text) || (COALESCE(a.c_kbez, ''::character varying))::text) || (COALESCE(a.c_zbez2, ''::character varying))::text) || (COALESCE((p.x_langtext)::character varying(4000), ''::character varying))::text) || (COALESCE(p.c_kommentar, ''::character varying))::text) AS c_suche
   FROM (((public.stk_stueckliste s
     JOIN public.stk_stuecklistearbeitsplan p ON ((s.i_id = p.stueckliste_i_id)))
     JOIN public.ww_artikelspr a ON ((a.artikel_i_id = p.artikel_i_id)))
     LEFT JOIN public.ww_artikel artikel ON ((artikel.i_id = p.artikel_i_id)));
ALTER TABLE public.stk_textsuche OWNER TO postgres;
CREATE TABLE public.timers (
    timerid character varying(50) NOT NULL,
    targetid character varying(50) NOT NULL,
    initialdate timestamp without time zone NOT NULL,
    "interval" bigint,
    instancepk bytea,
    info bytea
);
ALTER TABLE public.timers OWNER TO postgres;
CREATE TABLE public.ww_alergen (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(40) NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.ww_alergen OWNER TO postgres;
CREATE TABLE public.ww_artgrumandant (
    i_id integer NOT NULL,
    artgru_i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    konto_i_id integer
);
ALTER TABLE public.ww_artgrumandant OWNER TO postgres;
CREATE TABLE public.ww_artgruspr (
    locale_c_nr character(10) NOT NULL,
    artgru_i_id integer NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.ww_artgruspr OWNER TO postgres;
CREATE TABLE public.ww_artikelalergen (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    alergen_i_id integer NOT NULL
);
ALTER TABLE public.ww_artikelalergen OWNER TO postgres;
CREATE TABLE public.ww_artikelart (
    c_nr character(15) NOT NULL
);
ALTER TABLE public.ww_artikelart OWNER TO postgres;
CREATE TABLE public.ww_artikelartspr (
    locale_c_nr character(10) NOT NULL,
    artikelart_c_nr character(15) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.ww_artikelartspr OWNER TO postgres;
CREATE TABLE public.ww_artikelkommentar (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    artikelkommentarart_i_id integer NOT NULL,
    datenformat_c_nr character varying(40),
    b_defaultbild smallint NOT NULL,
    i_art integer NOT NULL,
    i_sort integer NOT NULL,
    b_dateiverweis smallint NOT NULL
);
ALTER TABLE public.ww_artikelkommentar OWNER TO postgres;
CREATE TABLE public.ww_artikelkommentarart (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    branche_i_id integer,
    b_webshop smallint NOT NULL,
    b_tooltip smallint NOT NULL,
    b_detail smallint NOT NULL
);
ALTER TABLE public.ww_artikelkommentarart OWNER TO postgres;
CREATE TABLE public.ww_artikelkommentarartspr (
    artikelkommentarart_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.ww_artikelkommentarartspr OWNER TO postgres;
CREATE TABLE public.ww_artikelkommentardruck (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    artikelkommentar_i_id integer NOT NULL,
    belegart_c_nr character(15) NOT NULL
);
ALTER TABLE public.ww_artikelkommentardruck OWNER TO postgres;
CREATE TABLE public.ww_artikelkommentarspr (
    artikelkommentar_i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    x_kommentar text,
    o_media bytea,
    c_dateiname character varying(300),
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_filedatum timestamp without time zone
);
ALTER TABLE public.ww_artikelkommentarspr OWNER TO postgres;
CREATE TABLE public.ww_artikellagerplaetze (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    lagerplatz_i_id integer NOT NULL,
    n_lagerstandpaternoster numeric(17,6),
    t_aendern timestamp without time zone,
    i_sort integer NOT NULL
);
ALTER TABLE public.ww_artikellagerplaetze OWNER TO postgres;
CREATE TABLE public.ww_artikellieferantstaffel (
    i_id integer NOT NULL,
    artikellieferant_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    f_rabatt double precision,
    n_nettopreis numeric(17,6),
    t_preisgueltigab timestamp without time zone NOT NULL,
    t_preisgueltigbis timestamp without time zone,
    b_rabattbehalten smallint NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    i_wiederbeschaffungszeit integer,
    c_angebotnummer character varying(40),
    c_bezbeilieferant character varying(80),
    c_artikelnrlieferant character varying(80),
    einheit_c_nr_vpe character(15),
    anfragepositionlieferdaten_i_id integer
);
ALTER TABLE public.ww_artikellieferantstaffel OWNER TO postgres;
CREATE TABLE public.ww_artikellog (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    c_key character varying(40) NOT NULL,
    c_von character varying(80),
    c_nach character varying(80),
    locale_c_nr character(10) NOT NULL,
    personal_i_id integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.ww_artikellog OWNER TO postgres;
CREATE TABLE public.ww_artikelreservierung (
    i_id integer NOT NULL,
    c_belegartnr character(15) NOT NULL,
    i_belegartpositionid integer NOT NULL,
    artikel_i_id integer NOT NULL,
    t_liefertermin timestamp without time zone NOT NULL,
    n_menge numeric(17,6) NOT NULL
);
ALTER TABLE public.ww_artikelreservierung OWNER TO postgres;
CREATE VIEW public.ww_artikelreservierung_prueffunktion AS
 SELECT row_number() OVER (ORDER BY v.c_belegartnr, v.i_belegartpositionid) AS i_id,
    v.c_belegartnr,
    v.i_belegartpositionid,
    v.artikel_i_id,
    v.t_liefertermin,
    v.n_menge,
    v.auftragposition_i_id,
    v.lossollmaterial_i_id,
    v.forecastposition_i_id
   FROM ( SELECT 'Auftrag        '::text AS c_belegartnr,
            pos.i_id AS i_belegartpositionid,
            pos.artikel_i_id,
                CASE
                    WHEN (pos.n_offenemenge < (0)::numeric) THEN ab.t_finaltermin
                    ELSE pos.t_uebersteuerterliefertermin
                END AS t_liefertermin,
            pos.n_offenemenge AS n_menge,
            pos.i_id AS auftragposition_i_id,
            NULL::integer AS lossollmaterial_i_id,
            NULL::integer AS forecastposition_i_id
           FROM ((public.auft_auftragposition pos
             LEFT JOIN public.ww_artikel art ON ((art.i_id = pos.artikel_i_id)))
             LEFT JOIN public.auft_auftrag ab ON ((ab.i_id = pos.auftrag_i_id)))
          WHERE ((ab.auftragstatus_c_nr = ANY (ARRAY['Angelegt       '::bpchar, 'Offen          '::bpchar, 'Teilerledigt   '::bpchar])) AND (art.artikelart_c_nr <> 'Handartikel    '::bpchar) AND (ab.auftragart_c_nr <> 'Rahmenauftrag  '::bpchar) AND (pos.artikel_i_id IS NOT NULL) AND (pos.n_menge <> (0)::numeric) AND (pos.auftragpositionstatus_c_nr <> 'Erledigt       '::bpchar))
        UNION ALL
         SELECT 'Los            '::text AS c_belegartnr,
            mat.i_id AS i_belegartpositionid,
            mat.artikel_i_id,
                CASE
                    WHEN (( SELECT dbo.add2date('dd'::text, COALESCE(min(ap.i_maschinenversatztage), 0), los.t_produktionsbeginn) AS add2date
                       FROM public.fert_lossollarbeitsplan ap
                      WHERE (ap.lossollmaterial_i_id = mat.i_id)) > los.t_produktionsbeginn) THEN ( SELECT dbo.add2date('dd'::text, COALESCE(min(ap.i_maschinenversatztage), 0), los.t_produktionsbeginn) AS add2date
                       FROM public.fert_lossollarbeitsplan ap
                      WHERE (ap.lossollmaterial_i_id = mat.i_id))
                    ELSE los.t_produktionsbeginn
                END AS date,
            mat.n_menge,
            NULL::integer AS auftragposition_i_id,
            mat.i_id AS lossollmaterial_i_id,
            NULL::integer AS forecastposition_i_id
           FROM ((public.fert_lossollmaterial mat
             LEFT JOIN public.fert_los los ON ((los.i_id = mat.los_i_id)))
             LEFT JOIN public.ww_artikel art ON ((art.i_id = mat.artikel_i_id)))
          WHERE ((los.status_c_nr = 'Angelegt       '::bpchar) AND (art.artikelart_c_nr <> 'Handartikel    '::bpchar))
        UNION ALL
         SELECT 'Forecast       '::text AS c_belegartnr,
            fp.i_id AS i_belegartpostionid,
            fp.artikel_i_id,
            fp.t_termin AS t_liefertermin,
            (fp.n_menge - COALESCE(( SELECT sum(lp.n_menge) AS sum
                   FROM (public.ls_lieferscheinposition lp
                     LEFT JOIN public.ls_lieferschein ls ON ((ls.i_id = lp.lieferschein_i_id)))
                  WHERE ((lp.forecastposition_i_id = fp.i_id) AND (ls.lieferscheinstatus_c_nr <> 'Storniert      '::bpchar))), (0)::numeric)) AS n_menge,
            NULL::integer AS auftragposition_i_id,
            NULL::integer AS lossollmaterial_i_id,
            fp.i_id AS forecastposition_i_id
           FROM (((public.fc_forecastposition fp
             LEFT JOIN public.fc_forecastauftrag fa ON ((fa.i_id = fp.forecastauftrag_i_id)))
             LEFT JOIN public.fc_fclieferadresse la ON ((la.i_id = fa.fclieferadresse_i_id)))
             LEFT JOIN public.fc_forecast fc ON ((fc.i_id = la.forecast_i_id)))
          WHERE ((fa.status_c_nr = 'Freigegeben    '::bpchar) AND (fp.n_menge > (0)::numeric) AND (fc.status_c_nr = 'Angelegt       '::bpchar) AND (fa.t_freigabe IS NOT NULL) AND (fp.status_c_nr = ANY (ARRAY['Angelegt       '::bpchar, 'In Produktion  '::bpchar, 'Teilerledigt   '::bpchar])) AND ((fp.n_menge - COALESCE(( SELECT sum(lp.n_menge) AS sum
                   FROM (public.ls_lieferscheinposition lp
                     LEFT JOIN public.ls_lieferschein ls ON ((ls.i_id = lp.lieferschein_i_id)))
                  WHERE ((lp.forecastposition_i_id = fp.i_id) AND (ls.lieferscheinstatus_c_nr <> 'Storniert      '::bpchar))), (0)::numeric)) > (0)::numeric))
        UNION ALL
         SELECT 'Kueche         '::text AS c_belegartnr,
            pos.i_id AS i_belegartpostionid,
            pos.artikel_i_id,
            sp.t_datum AS t_liefertermin,
            pos.n_menge,
            NULL::integer AS auftragposition_i_id,
            NULL::integer AS lossollmaterial_i_id,
            NULL::integer AS forecastposition_i_id
           FROM (public.kue_speiseplanposition pos
             LEFT JOIN public.kue_speiseplan sp ON ((sp.i_id = pos.speiseplan_i_id)))
          WHERE ((pos.n_menge > (0)::numeric) AND (sp.t_datum >= date(now())))) v;
ALTER TABLE public.ww_artikelreservierung_prueffunktion OWNER TO postgres;
CREATE TABLE public.ww_artikelshopgruppe (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    shopgruppe_i_id integer NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.ww_artikelshopgruppe OWNER TO postgres;
CREATE TABLE public.ww_artikelsnrchnr (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    c_seriennrchargennr character varying(50) NOT NULL
);
ALTER TABLE public.ww_artikelsnrchnr OWNER TO postgres;
CREATE TABLE public.ww_artikeltrutops (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    t_export_beginn timestamp without time zone,
    t_export_ende timestamp without time zone,
    c_pfad character varying(1024),
    c_export_pfad character varying(1024),
    c_fehlercode character varying(1024),
    c_fehlertext character varying(3000)
);
ALTER TABLE public.ww_artikeltrutops OWNER TO postgres;
CREATE TABLE public.ww_artikeltrutopsmetadaten (
    i_id integer NOT NULL,
    artikeltrutops_i_id integer NOT NULL,
    c_filename character varying(1024),
    i_size integer,
    t_creation timestamp without time zone,
    t_modification timestamp without time zone,
    c_hash character varying(1024)
);
ALTER TABLE public.ww_artikeltrutopsmetadaten OWNER TO postgres;
CREATE TABLE public.ww_artkla (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    artkla_i_id integer,
    b_tops smallint NOT NULL,
    mandant_c_nr character varying(3) NOT NULL
);
ALTER TABLE public.ww_artkla OWNER TO postgres;
CREATE TABLE public.ww_artklaspr (
    locale_c_nr character(10) NOT NULL,
    artkla_i_id integer NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.ww_artklaspr OWNER TO postgres;
CREATE TABLE public.ww_automotive (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.ww_automotive OWNER TO postgres;
CREATE TABLE public.ww_dateiverweis (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_laufwerk character varying(80) NOT NULL,
    c_unc character varying(80) NOT NULL
);
ALTER TABLE public.ww_dateiverweis OWNER TO postgres;
CREATE TABLE public.ww_einkaufsean (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    c_ean character varying(13) NOT NULL
);
ALTER TABLE public.ww_einkaufsean OWNER TO postgres;
CREATE TABLE public.ww_ersatztypen (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    artikel_i_id_ersatz integer NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.ww_ersatztypen OWNER TO postgres;
CREATE TABLE public.ww_farbcode (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.ww_farbcode OWNER TO postgres;
CREATE TABLE public.ww_fasession (
    i_id integer NOT NULL,
    personal_i_id integer NOT NULL,
    t_beginn timestamp without time zone NOT NULL,
    t_gedruckt timestamp without time zone
);
ALTER TABLE public.ww_fasession OWNER TO postgres;
CREATE TABLE public.ww_fasessioneintrag (
    i_id integer NOT NULL,
    fasession_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    artikel_i_id integer NOT NULL,
    lager_i_id integer NOT NULL,
    c_snrchnr text,
    lieferschein_i_id integer,
    auftrag_i_id integer,
    los_i_id integer,
    artikel_i_id_offenerag integer,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL
);
ALTER TABLE public.ww_fasessioneintrag OWNER TO postgres;
CREATE VIEW public.ww_fehlmengereservierung AS
 SELECT 'R'::text AS typ,
    a.i_id,
    a.artikel_i_id,
    a.n_menge,
    a.t_liefertermin,
        CASE
            WHEN (a.c_belegartnr = 'Auftrag'::bpchar) THEN a.i_belegartpositionid
            ELSE NULL::integer
        END AS auftragposition_i_id,
        CASE
            WHEN (a.c_belegartnr = 'Los'::bpchar) THEN a.i_belegartpositionid
            ELSE NULL::integer
        END AS lossollmaterial_i_id,
        CASE
            WHEN (a.c_belegartnr = 'Los'::bpchar) THEN ( SELECT l.t_produktionsende
               FROM (public.fert_lossollmaterial mat
                 LEFT JOIN public.fert_los l ON ((mat.los_i_id = l.i_id)))
              WHERE (mat.i_id = a.i_belegartpositionid))
            ELSE a.t_liefertermin
        END AS t_produktionsende
   FROM public.ww_artikelreservierung a
UNION
 SELECT 'F'::text AS typ,
    fm.i_id,
    fm.artikel_i_id,
    fm.n_menge,
    fm.t_liefertermin,
        CASE
            WHEN (fm.c_belegartnr = 'Auftrag'::bpchar) THEN fm.i_belegartpositionid
            ELSE NULL::integer
        END AS auftragposition_i_id,
        CASE
            WHEN (fm.c_belegartnr = 'Los'::bpchar) THEN fm.i_belegartpositionid
            ELSE NULL::integer
        END AS lossollmaterial_i_id,
        CASE
            WHEN (fm.c_belegartnr = 'Los'::bpchar) THEN ( SELECT l.t_produktionsende
               FROM (public.fert_lossollmaterial mat
                 LEFT JOIN public.fert_los l ON ((mat.los_i_id = l.i_id)))
              WHERE (mat.i_id = fm.i_belegartpositionid))
            ELSE fm.t_liefertermin
        END AS t_produktionsende
   FROM public.ww_artikelfehlmenge fm;
ALTER TABLE public.ww_fehlmengereservierung OWNER TO postgres;
CREATE TABLE public.ww_gebinde (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.ww_gebinde OWNER TO postgres;
CREATE TABLE public.ww_geometrie (
    artikel_i_id integer NOT NULL,
    f_breite double precision,
    c_breitetext character varying(2),
    f_hoehe double precision,
    f_tiefe double precision
);
ALTER TABLE public.ww_geometrie OWNER TO postgres;
CREATE TABLE public.ww_geraetesnr (
    i_id integer NOT NULL,
    i_id_buchung integer NOT NULL,
    artikel_i_id integer NOT NULL,
    c_snr character varying(50) NOT NULL
);
ALTER TABLE public.ww_geraetesnr OWNER TO postgres;
CREATE TABLE public.ww_handlagerbewegung (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    lager_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    c_kommentar character varying(50) NOT NULL,
    t_buchungszeit timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    n_verkaufspreis numeric(17,6),
    n_einstandspreis numeric(17,6),
    n_gestehungspreis numeric(17,6),
    b_abgang smallint NOT NULL,
    c_snrchnr_mig character varying(3000)
);
ALTER TABLE public.ww_handlagerbewegung OWNER TO postgres;
CREATE TABLE public.ww_hersteller (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    partner_i_id integer NOT NULL,
    c_leadin character varying(40)
);
ALTER TABLE public.ww_hersteller OWNER TO postgres;
CREATE TABLE public.ww_inventur (
    i_id integer NOT NULL,
    t_inventurdatum timestamp without time zone NOT NULL,
    b_inventurdurchgefuehrt smallint NOT NULL,
    personal_i_id_inventurdurchgefuehrt integer,
    t_inventurdurchgefuehrt timestamp without time zone,
    b_abwertungdurchgefuehrt smallint NOT NULL,
    personal_i_id_abwertungdurchgefuehrt integer,
    t_abwertungdurchgefuehrt timestamp without time zone,
    c_bez character varying(80) NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    lager_i_id integer,
    b_nichtinventierteartikelabbuchen smallint NOT NULL,
    b_nichtinventierte_snrchnr_artikelabbuchen smallint NOT NULL
);
ALTER TABLE public.ww_inventur OWNER TO postgres;
CREATE TABLE public.ww_inventurliste (
    i_id integer NOT NULL,
    inventur_i_id integer NOT NULL,
    lager_i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    c_seriennrchargennr character varying(50),
    n_inventurmenge numeric(15,4) NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.ww_inventurliste OWNER TO postgres;
CREATE TABLE public.ww_inventurprotokoll (
    i_id integer NOT NULL,
    inventur_i_id integer NOT NULL,
    inventurliste_i_id integer NOT NULL,
    t_zeitpunkt timestamp without time zone NOT NULL,
    n_korrekturmenge numeric(17,6) NOT NULL,
    n_inventurpreis numeric(17,6) NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL
);
ALTER TABLE public.ww_inventurprotokoll OWNER TO postgres;
CREATE TABLE public.ww_inventurstand (
    i_id integer NOT NULL,
    inventur_i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    lager_i_id integer NOT NULL,
    n_inventurmenge numeric(17,6) NOT NULL,
    n_inventurpreis numeric(17,6) NOT NULL,
    n_abgewerteterpreis numeric(17,6),
    f_abwertung double precision,
    n_basispreis numeric(17,6) NOT NULL,
    c_kommentar character varying(300)
);
ALTER TABLE public.ww_inventurstand OWNER TO postgres;
CREATE TABLE public.ww_katalog (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    c_katalog character varying(15) NOT NULL,
    c_seite character varying(5)
);
ALTER TABLE public.ww_katalog OWNER TO postgres;
CREATE TABLE public.ww_lager (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    lagerart_c_nr character(15) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    b_bestellvorschlag smallint NOT NULL,
    b_internebestellung smallint NOT NULL,
    i_loslagersort integer,
    b_versteckt smallint NOT NULL,
    b_konsignationslager smallint NOT NULL,
    partner_i_id_standort integer,
    partner_i_id integer,
    i_sort integer NOT NULL,
    b_lagerstand_bei_0_anzeigen smallint NOT NULL
);
ALTER TABLE public.ww_lager OWNER TO postgres;
CREATE TABLE public.ww_lagerabgangursprung (
    i_lagerbewegungid integer NOT NULL,
    i_lagerbewegungidursprung integer NOT NULL,
    n_verbrauchtemenge numeric(17,6) NOT NULL,
    n_gestehungspreis numeric(17,6) NOT NULL
);
ALTER TABLE public.ww_lagerabgangursprung OWNER TO postgres;
CREATE TABLE public.ww_lagerart (
    c_nr character(15) NOT NULL
);
ALTER TABLE public.ww_lagerart OWNER TO postgres;
CREATE TABLE public.ww_lagerartspr (
    lagerart_c_nr character(15) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.ww_lagerartspr OWNER TO postgres;
CREATE TABLE public.ww_lagerbewegung (
    i_id integer NOT NULL,
    c_belegartnr character(15) NOT NULL,
    i_belegartid integer NOT NULL,
    i_belegartpositionid integer NOT NULL,
    i_id_buchung integer NOT NULL,
    lager_i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    personal_i_id_mengegeaendert integer NOT NULL,
    t_buchungszeit timestamp without time zone NOT NULL,
    c_seriennrchargennr character varying(50),
    n_verkaufspreis numeric(17,6),
    personal_i_id_verkaufspreisgeaendert integer,
    n_einstandspreis numeric(17,6),
    personal_i_id_einstandspreisgeaendert integer,
    n_gestehungspreis numeric(17,6) NOT NULL,
    b_abgang smallint NOT NULL,
    b_vollstaendigverbraucht smallint NOT NULL,
    t_belegdatum timestamp without time zone NOT NULL,
    b_historie smallint NOT NULL,
    land_i_id integer,
    hersteller_i_id integer,
    c_version character varying(40),
    gebinde_i_id integer,
    n_gebindemenge numeric(17,6)
);
ALTER TABLE public.ww_lagerbewegung OWNER TO postgres;
CREATE TABLE public.ww_lagerplatz (
    i_id integer NOT NULL,
    c_lagerplatz character varying(40) NOT NULL,
    i_maxmenge integer NOT NULL,
    lager_i_id integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    paternoster_i_id integer
);
ALTER TABLE public.ww_lagerplatz OWNER TO postgres;
CREATE VIEW public.ww_lagercockpitumbuchung AS
 SELECT soll.i_id,
    soll.artikel_i_id,
    soll.los_i_id,
    soll.n_menge,
    (soll.n_menge - (COALESCE(( SELECT sum(ist.n_menge) AS sum
           FROM public.fert_losistmaterial ist
          WHERE ((ist.lossollmaterial_i_id = soll.i_id) AND (ist.b_abgang = 1))), (0)::numeric) - COALESCE(( SELECT sum(ist.n_menge) AS sum
           FROM public.fert_losistmaterial ist
          WHERE ((ist.lossollmaterial_i_id = soll.i_id) AND (ist.b_abgang = 0))), (0)::numeric))) AS diff,
    COALESCE(( SELECT sum(al.n_lagerstand) AS sum
           FROM (public.ww_artikellager al
             LEFT JOIN public.ww_lager lager ON ((al.lager_i_id = lager.i_id)))
          WHERE ((al.artikel_i_id = soll.artikel_i_id) AND (lager.lagerart_c_nr = ANY (ARRAY['Hauptlager'::bpchar, 'Normal'::bpchar, 'Halbfertig'::bpchar])))), (0)::numeric) AS lagerstand,
    ( SELECT lp.lager_i_id
           FROM (public.ww_artikellagerplaetze alp
             LEFT JOIN public.ww_lagerplatz lp ON ((alp.lagerplatz_i_id = lp.i_id)))
          WHERE ((alp.i_sort = 1) AND (alp.artikel_i_id = soll.artikel_i_id))) AS lager_i_id_lagerplatz
   FROM (public.fert_lossollmaterial soll
     JOIN public.fert_los los ON ((soll.los_i_id = los.i_id)))
  WHERE ((los.status_c_nr <> ALL (ARRAY['Erledigt'::bpchar, 'Storniert'::bpchar, 'Angelegt'::bpchar])) AND (soll.n_menge <> (COALESCE(( SELECT sum(ist.n_menge) AS sum
           FROM public.fert_losistmaterial ist
          WHERE ((ist.lossollmaterial_i_id = soll.i_id) AND (ist.b_abgang = 1))), (0)::numeric) - COALESCE(( SELECT sum(ist.n_menge) AS sum
           FROM public.fert_losistmaterial ist
          WHERE ((ist.lossollmaterial_i_id = soll.i_id) AND (ist.b_abgang = 0))), (0)::numeric))));
ALTER TABLE public.ww_lagercockpitumbuchung OWNER TO postgres;
CREATE TABLE public.ww_lagerumbuchung (
    i_lagerbewegungidzubuchung integer NOT NULL,
    i_lagerbewegungidabbuchung integer NOT NULL
);
ALTER TABLE public.ww_lagerumbuchung OWNER TO postgres;
CREATE TABLE public.ww_lagerzugangursprung (
    i_lagerbewegungid integer NOT NULL,
    i_lagerbewegungidursprung integer NOT NULL
);
ALTER TABLE public.ww_lagerzugangursprung OWNER TO postgres;
CREATE VIEW public.ww_lagerwert AS
 SELECT l.i_id,
    l.i_id_buchung,
    l.i_belegartid,
    l.i_belegartpositionid,
    l.n_gestehungspreis,
    l.n_verkaufspreis,
    l.n_einstandspreis,
    l.b_abgang,
    l.c_belegartnr,
    l.c_seriennrchargennr,
    l.n_menge,
    l.t_belegdatum,
    l.t_buchungszeit,
    l.artikel_i_id,
    l.lager_i_id,
    a.b_lagerbewirtschaftet,
        CASE
            WHEN (l.b_abgang = 0) THEN (l.n_menge * l.n_einstandspreis)
            ELSE NULL::numeric
        END AS n_einstandswert,
        CASE
            WHEN (a.b_lagerbewirtschaftet = 1) THEN ( SELECT sum((au.n_verbrauchtemenge * au.n_gestehungspreis)) AS sum
               FROM public.ww_lagerabgangursprung au
              WHERE (au.i_lagerbewegungid = l.i_id_buchung)
             HAVING (sum(au.n_verbrauchtemenge) > (0)::numeric))
            ELSE (l.n_menge * l.n_gestehungspreis)
        END AS n_gestwert,
    ( SELECT count(*) AS count
           FROM public.ww_lagerzugangursprung zu
          WHERE (zu.i_lagerbewegungid IN ( SELECT au2.i_lagerbewegungidursprung
                   FROM public.ww_lagerabgangursprung au2
                  WHERE (au2.i_lagerbewegungid = l.i_id_buchung)))) AS i_anzahl_urspruenge_des_zugangs
   FROM (public.ww_lagerbewegung l
     LEFT JOIN public.ww_artikel a ON ((l.artikel_i_id = a.i_id)))
  WHERE ((l.b_historie = 0) AND (l.n_menge <> (0)::numeric));
ALTER TABLE public.ww_lagerwert OWNER TO postgres;
CREATE TABLE public.ww_laseroberflaeche (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_nr character varying(15) NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.ww_laseroberflaeche OWNER TO postgres;
CREATE TABLE public.ww_material (
    i_id integer NOT NULL,
    c_nr character varying(50) NOT NULL,
    n_gewicht_in_kg numeric(15,4)
);
ALTER TABLE public.ww_material OWNER TO postgres;
CREATE TABLE public.ww_materialpreis (
    i_id integer NOT NULL,
    material_i_id integer NOT NULL,
    n_preis_pro_kg numeric(17,6) NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL
);
ALTER TABLE public.ww_materialpreis OWNER TO postgres;
CREATE TABLE public.ww_materialspr (
    locale_c_nr character(10) NOT NULL,
    material_i_id integer NOT NULL,
    c_bez character varying(80)
);
ALTER TABLE public.ww_materialspr OWNER TO postgres;
CREATE TABLE public.ww_materialzuschlag (
    i_id integer NOT NULL,
    material_i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    t_gueltigab timestamp without time zone NOT NULL,
    n_zuschlag numeric(17,6) NOT NULL
);
ALTER TABLE public.ww_materialzuschlag OWNER TO postgres;
CREATE TABLE public.ww_medical (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.ww_medical OWNER TO postgres;
CREATE TABLE public.ww_montage (
    artikel_i_id integer NOT NULL,
    f_rasterliegend double precision,
    f_rasterstehend double precision,
    b_hochstellen smallint,
    b_hochsetzen smallint,
    b_polarisiert smallint
);
ALTER TABLE public.ww_montage OWNER TO postgres;
CREATE TABLE public.ww_paternoster (
    i_id integer NOT NULL,
    c_nr character(15) NOT NULL,
    c_bez character varying(80) NOT NULL,
    c_paternostertyp character(15) NOT NULL,
    lager_i_id integer NOT NULL,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer,
    t_aendern timestamp without time zone
);
ALTER TABLE public.ww_paternoster OWNER TO postgres;
CREATE TABLE public.ww_paternostereigenschaft (
    i_id integer NOT NULL,
    paternoster_i_id integer NOT NULL,
    c_nr character varying(80) NOT NULL,
    c_wert character varying(100) NOT NULL
);
ALTER TABLE public.ww_paternostereigenschaft OWNER TO postgres;
CREATE TABLE public.ww_rahmenbedarfe (
    i_id integer NOT NULL,
    auftrag_i_id integer,
    artikel_i_id integer NOT NULL,
    n_gesamtmenge numeric(17,6) NOT NULL,
    los_i_id integer
);
ALTER TABLE public.ww_rahmenbedarfe OWNER TO postgres;
CREATE TABLE public.ww_reach (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.ww_reach OWNER TO postgres;
CREATE TABLE public.ww_rohs (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.ww_rohs OWNER TO postgres;
CREATE TABLE public.ww_shopgruppe (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_nr character varying(15) NOT NULL,
    shopgruppe_i_id integer,
    artikel_i_id integer,
    personal_i_id_anlegen integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    i_sort integer NOT NULL
);
ALTER TABLE public.ww_shopgruppe OWNER TO postgres;
CREATE TABLE public.ww_shopgruppespr (
    locale_c_nr character(10) NOT NULL,
    shopgruppe_i_id integer NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.ww_shopgruppespr OWNER TO postgres;
CREATE TABLE public.ww_shopgruppewebshop (
    i_id integer NOT NULL,
    shopgruppe_i_id integer NOT NULL,
    webshop_i_id integer NOT NULL
);
ALTER TABLE public.ww_shopgruppewebshop OWNER TO postgres;
CREATE TABLE public.ww_sollverkauf (
    artikel_i_id integer NOT NULL,
    f_aufschlag double precision,
    f_sollverkauf double precision
);
ALTER TABLE public.ww_sollverkauf OWNER TO postgres;
CREATE TABLE public.ww_sperren (
    i_id integer NOT NULL,
    c_bez character varying(80) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    b_gesperrt smallint NOT NULL,
    b_gesperrteinkauf smallint NOT NULL,
    b_gesperrtverkauf smallint NOT NULL,
    b_gesperrtlos smallint NOT NULL,
    b_gesperrtstueckliste smallint NOT NULL,
    b_durchfertigung smallint NOT NULL,
    o_bild bytea,
    b_default_bei_artikelneuanlage smallint NOT NULL
);
ALTER TABLE public.ww_sperren OWNER TO postgres;
CREATE TABLE public.ww_trumphtopslog (
    i_id integer NOT NULL,
    c_importfilename character varying(40) NOT NULL,
    c_error character varying(80) NOT NULL,
    artikel_i_id integer,
    artikel_i_id_material integer,
    n_gewicht numeric(15,4) NOT NULL,
    n_gestpreisneu numeric(15,4) NOT NULL,
    i_bearbeitungszeit integer NOT NULL,
    t_anlegen timestamp without time zone NOT NULL
);
ALTER TABLE public.ww_trumphtopslog OWNER TO postgres;
CREATE TABLE public.ww_verleih (
    i_id integer NOT NULL,
    i_tage integer NOT NULL,
    f_faktor double precision NOT NULL
);
ALTER TABLE public.ww_verleih OWNER TO postgres;
CREATE TABLE public.ww_verpackung (
    artikel_i_id integer NOT NULL,
    c_bauform character varying(20),
    c_verpackungsart character varying(20)
);
ALTER TABLE public.ww_verpackung OWNER TO postgres;
CREATE TABLE public.ww_verpackungsmittel (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    n_gewicht_in_kg numeric(15,4) NOT NULL
);
ALTER TABLE public.ww_verpackungsmittel OWNER TO postgres;
CREATE TABLE public.ww_verpackungsmittelspr (
    locale_c_nr character(10) NOT NULL,
    verpackungsmittel_i_id integer NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.ww_verpackungsmittelspr OWNER TO postgres;
CREATE TABLE public.ww_verschleissteil (
    i_id integer NOT NULL,
    c_nr character varying(40) NOT NULL,
    c_bez character varying(80),
    c_bez2 character varying(80)
);
ALTER TABLE public.ww_verschleissteil OWNER TO postgres;
CREATE TABLE public.ww_verschleissteilwerkzeug (
    i_id integer NOT NULL,
    verschleissteil_i_id integer NOT NULL,
    werkzeug_i_id integer NOT NULL
);
ALTER TABLE public.ww_verschleissteilwerkzeug OWNER TO postgres;
CREATE TABLE public.ww_vkpfartikelpreis (
    i_id integer NOT NULL,
    vkpfartikelpreisliste_i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    t_preisgueltigab timestamp without time zone NOT NULL,
    n_artikelstandardrabattsatz numeric(17,6) NOT NULL,
    n_artikelfixpreis numeric(17,6),
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    c_bemerkung character varying(80)
);
ALTER TABLE public.ww_vkpfartikelpreis OWNER TO postgres;
CREATE TABLE public.ww_vkpfartikelpreisliste (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    i_sort integer NOT NULL,
    c_nr character varying(40) NOT NULL,
    b_preislisteaktiv smallint NOT NULL,
    waehrung_c_nr character(3) NOT NULL,
    c_fremdsystemnr character varying(15),
    n_standardrabattsatz numeric(17,6),
    webshop_i_id integer
);
ALTER TABLE public.ww_vkpfartikelpreisliste OWNER TO postgres;
CREATE TABLE public.ww_vkpfartikelverkaufspreisbasis (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    artikel_i_id integer NOT NULL,
    n_verkaufspreisbasis numeric(17,6) NOT NULL,
    t_verkaufspreisbasisgueltigab timestamp without time zone NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    personal_i_id_aendern integer NOT NULL,
    c_bemerkung character varying(300)
);
ALTER TABLE public.ww_vkpfartikelverkaufspreisbasis OWNER TO postgres;
CREATE TABLE public.ww_vkpfmengenstaffel (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    n_menge numeric(17,6) NOT NULL,
    f_artikelstandardrabattsatz double precision NOT NULL,
    n_artikelfixpreis numeric(17,6),
    t_preisgueltigab timestamp without time zone NOT NULL,
    t_preisgueltigbis timestamp without time zone,
    personal_i_id_aendern integer NOT NULL,
    t_aendern timestamp without time zone NOT NULL,
    vkpfartikelpreisliste_i_id integer,
    b_allepreislisten smallint NOT NULL,
    c_bemerkung character varying(80)
);
ALTER TABLE public.ww_vkpfmengenstaffel OWNER TO postgres;
CREATE TABLE public.ww_vorschlagstext (
    i_id integer NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40) NOT NULL
);
ALTER TABLE public.ww_vorschlagstext OWNER TO postgres;
CREATE TABLE public.ww_vorzug (
    i_id integer NOT NULL,
    c_nr character varying(1) NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(20) NOT NULL
);
ALTER TABLE public.ww_vorzug OWNER TO postgres;
CREATE TABLE public.ww_waffenausfuehrung (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.ww_waffenausfuehrung OWNER TO postgres;
CREATE TABLE public.ww_waffenkaliber (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.ww_waffenkaliber OWNER TO postgres;
CREATE TABLE public.ww_waffenkategorie (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.ww_waffenkategorie OWNER TO postgres;
CREATE TABLE public.ww_waffentyp (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.ww_waffentyp OWNER TO postgres;
CREATE TABLE public.ww_waffentyp_fein (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.ww_waffentyp_fein OWNER TO postgres;
CREATE TABLE public.ww_waffenzusatz (
    i_id integer NOT NULL,
    c_nr character varying(15) NOT NULL,
    c_bez character varying(80) NOT NULL
);
ALTER TABLE public.ww_waffenzusatz OWNER TO postgres;
CREATE VIEW public.ww_wareneigangsreferenz AS
 SELECT l_zu.i_id AS lagerbewegung_i_id_zubuchung,
    l_ab.i_id AS lagerbewegung_i_id_abbuchung,
    au.n_verbrauchtemenge
   FROM ((public.ww_lagerabgangursprung au
     LEFT JOIN public.ww_lagerbewegung l_zu ON ((l_zu.i_id_buchung = au.i_lagerbewegungidursprung)))
     LEFT JOIN public.ww_lagerbewegung l_ab ON ((l_ab.i_id_buchung = au.i_lagerbewegungid)))
  WHERE ((l_zu.b_historie = 0) AND (l_ab.b_historie = 0));
ALTER TABLE public.ww_wareneigangsreferenz OWNER TO postgres;
CREATE TABLE public.ww_webshop (
    i_id integer NOT NULL,
    mandant_c_nr character varying(3) NOT NULL,
    c_bez character varying(20) NOT NULL,
    webshopart_c_nr character(15) NOT NULL,
    c_url character varying(100),
    c_user character varying(100),
    c_password character varying(100)
);
ALTER TABLE public.ww_webshop OWNER TO postgres;
CREATE TABLE public.ww_webshopart (
    c_nr character(15) NOT NULL
);
ALTER TABLE public.ww_webshopart OWNER TO postgres;
CREATE TABLE public.ww_webshopartikel (
    i_id integer NOT NULL,
    webshop_i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    c_externe_id character varying(300) NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.ww_webshopartikel OWNER TO postgres;
CREATE TABLE public.ww_webshopartikelpreisliste (
    i_id integer NOT NULL,
    webshop_i_id integer NOT NULL,
    vkpfartikelpreisliste_i_id integer NOT NULL,
    c_externe_id character varying(300) NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.ww_webshopartikelpreisliste OWNER TO postgres;
CREATE TABLE public.ww_webshopartspr (
    webshopart_c_nr character(15) NOT NULL,
    locale_c_nr character(10) NOT NULL,
    c_bez character varying(40)
);
ALTER TABLE public.ww_webshopartspr OWNER TO postgres;
CREATE TABLE public.ww_webshopkunde (
    i_id integer NOT NULL,
    webshop_i_id integer NOT NULL,
    kunde_i_id integer NOT NULL,
    c_externe_id character varying(300) NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.ww_webshopkunde OWNER TO postgres;
CREATE TABLE public.ww_webshopmwstsatzbez (
    i_id integer NOT NULL,
    webshop_i_id integer NOT NULL,
    mwstsatzbez_i_id integer NOT NULL,
    c_externe_id character varying(300) NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.ww_webshopmwstsatzbez OWNER TO postgres;
CREATE TABLE public.ww_webshopshopgruppe (
    i_id integer NOT NULL,
    webshop_i_id integer NOT NULL,
    shopgruppe_i_id integer NOT NULL,
    c_externe_id character varying(300) NOT NULL,
    c_pfad character varying(300) NOT NULL,
    t_anlegen timestamp without time zone NOT NULL,
    t_aendern timestamp without time zone NOT NULL
);
ALTER TABLE public.ww_webshopshopgruppe OWNER TO postgres;
CREATE TABLE public.ww_werkzeug (
    i_id integer NOT NULL,
    c_nr character varying(40) NOT NULL,
    c_bez character varying(80),
    x_kommentar text,
    mandant_c_nr_standort character varying(3) NOT NULL,
    lieferant_i_id integer,
    n_kaufpreis numeric(17,6),
    t_kaufdatum timestamp without time zone,
    lagerplatz_i_id integer
);
ALTER TABLE public.ww_werkzeug OWNER TO postgres;
CREATE TABLE public.ww_zugehoerige (
    i_id integer NOT NULL,
    artikel_i_id integer NOT NULL,
    artikel_i_id_zugehoerig integer NOT NULL
);
ALTER TABLE public.ww_zugehoerige OWNER TO postgres;
ALTER TABLE ONLY public.lp_finanzamt
    ADD CONSTRAINT allg_finanzamt_pkey PRIMARY KEY (partner_i_id, mandant_c_nr);
ALTER TABLE ONLY public.fb_bankverbindung
    ADD CONSTRAINT fb_bankkonto_pkey PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_buchungsart
    ADD CONSTRAINT fb_buchungsart_pkey PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.fb_buchungsartspr
    ADD CONSTRAINT fb_buchungsartspr_pkey PRIMARY KEY (buchungsart_c_nr, locale_c_nr);
ALTER TABLE ONLY public.fb_kassenbuch
    ADD CONSTRAINT fb_kassenbuch_pkey PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_laenderart
    ADD CONSTRAINT fb_laenderart_pkey PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.fb_laenderartspr
    ADD CONSTRAINT fb_laenderartspr_pkey PRIMARY KEY (laenderart_c_nr, locale_c_nr);
ALTER TABLE ONLY public.as_agstkl
    ADD CONSTRAINT ix_as_agstkl UNIQUE (mandant_c_nr, c_nr);
ALTER TABLE ONLY public.bes_bsmahnstufe
    ADD CONSTRAINT ix_bes_bsmahnstufe UNIQUE (i_id, mandant_c_nr);
ALTER TABLE ONLY public.lp_belegartmedia
    ADD CONSTRAINT ix_lp_belegartmedia UNIQUE (usecase_id, i_key, i_sort);
ALTER TABLE ONLY public.lp_waehrung
    ADD CONSTRAINT ix_lp_waehrung_c_nr_curreny UNIQUE (c_nr);
ALTER TABLE ONLY public.ls_verkettet
    ADD CONSTRAINT ix_ls_verkettet UNIQUE (lieferschein_i_id, lieferschein_i_id_verkettet);
ALTER TABLE ONLY public.ls_verkettet
    ADD CONSTRAINT ix_ls_verkettet_2 UNIQUE (lieferschein_i_id_verkettet);
ALTER TABLE ONLY public.part_partnerkommentar
    ADD CONSTRAINT ix_part_partnerkommentar UNIQUE (partner_i_id, partnerkommentarart_i_id, datenformat_c_nr, b_kunde);
ALTER TABLE ONLY public.part_partnerkommentardruck
    ADD CONSTRAINT ix_part_partnerkommentardruck UNIQUE (partnerkommentar_i_id, belegart_c_nr);
ALTER TABLE ONLY public.pers_auszahlung_bva
    ADD CONSTRAINT ix_pers_auszahlung_bva UNIQUE (personal_i_id, t_datum);
ALTER TABLE ONLY public.pers_maschinemaschinenzm
    ADD CONSTRAINT ix_pers_maschinemaschinenzm UNIQUE (maschine_i_id, maschinenzm_i_id, t_gueltigab);
ALTER TABLE ONLY public.pers_maschinenzm
    ADD CONSTRAINT ix_pers_maschinenzm UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.pers_maschinenzmtagesart
    ADD CONSTRAINT ix_pers_maschinenzmtagesart UNIQUE (maschinenzm_i_id, tagesart_i_id);
ALTER TABLE ONLY public.pers_personalfunktion
    ADD CONSTRAINT ix_pers_personalfunktion UNIQUE (c_nr);
ALTER TABLE ONLY public.pers_personalgruppekosten
    ADD CONSTRAINT ix_pers_personalgruppekosten UNIQUE (personalgruppe_i_id, t_gueltigab);
ALTER TABLE ONLY public.pers_uebertrag_bva
    ADD CONSTRAINT ix_pers_uebertrag_bva UNIQUE (personal_i_id, t_datum);
ALTER TABLE ONLY public.rech_verrechnungsmodell
    ADD CONSTRAINT ix_rech_verrechnungsmodell UNIQUE (mandant_c_nr, c_bez);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT ix_stk_stueckliste UNIQUE (artikel_i_id, mandant_c_nr);
ALTER TABLE ONLY public.stk_stuecklisteart
    ADD CONSTRAINT ix_stk_stuecklisteart UNIQUE (c_bez);
ALTER TABLE ONLY public.ww_lager
    ADD CONSTRAINT ix_ww_lager UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.ww_webshopartikel
    ADD CONSTRAINT ix_ww_webshopartikel UNIQUE (webshop_i_id, artikel_i_id);
ALTER TABLE ONLY public.ww_webshopartikelpreisliste
    ADD CONSTRAINT ix_ww_webshopartikelpreisliste UNIQUE (webshop_i_id, vkpfartikelpreisliste_i_id);
ALTER TABLE ONLY public.ww_webshopkunde
    ADD CONSTRAINT ix_ww_webshopkunde UNIQUE (webshop_i_id, kunde_i_id);
ALTER TABLE ONLY public.ww_webshopmwstsatzbez
    ADD CONSTRAINT ix_ww_webshopmwstsatzbez UNIQUE (webshop_i_id, mwstsatzbez_i_id);
ALTER TABLE ONLY public.ww_webshopshopgruppe
    ADD CONSTRAINT ix_ww_webshopshopgruppe UNIQUE (webshop_i_id, shopgruppe_i_id);
ALTER TABLE ONLY public.lp_usercount
    ADD CONSTRAINT lp_usercount_pkey PRIMARY KEY (i_id);
ALTER TABLE ONLY public.timers
    ADD CONSTRAINT pk PRIMARY KEY (timerid);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT pk_anf_anfrage PRIMARY KEY (i_id);
ALTER TABLE ONLY public.anf_anfrageart
    ADD CONSTRAINT pk_anf_anfrageart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.anf_anfrageartspr
    ADD CONSTRAINT pk_anf_anfrageartspr PRIMARY KEY (locale_c_nr, anfrageart_c_nr);
ALTER TABLE ONLY public.anf_anfrageerledigungsgrund
    ADD CONSTRAINT pk_anf_anfrageerledigungsgrund PRIMARY KEY (i_id);
ALTER TABLE ONLY public.anf_anfrageposition
    ADD CONSTRAINT pk_anf_anfrageposition PRIMARY KEY (i_id);
ALTER TABLE ONLY public.anf_anfragepositionart
    ADD CONSTRAINT pk_anf_anfragepositionart PRIMARY KEY (positionsart_c_nr);
ALTER TABLE ONLY public.anf_anfragepositionlieferdaten
    ADD CONSTRAINT pk_anf_anfragepositionlieferdaten PRIMARY KEY (i_id);
ALTER TABLE ONLY public.anf_anfragestatus
    ADD CONSTRAINT pk_anf_anfragestatus PRIMARY KEY (status_c_nr);
ALTER TABLE ONLY public.anf_anfragetext
    ADD CONSTRAINT pk_anf_anfragetext PRIMARY KEY (i_id);
ALTER TABLE ONLY public.anf_zertifikatart
    ADD CONSTRAINT pk_anf_zertifikatart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.angb_akquisestatus
    ADD CONSTRAINT pk_angb_akquisestatus PRIMARY KEY (i_id);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT pk_angb_angebot PRIMARY KEY (i_id);
ALTER TABLE ONLY public.angb_angebotart
    ADD CONSTRAINT pk_angb_angebotart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.angb_angeboteinheit
    ADD CONSTRAINT pk_angb_angeboteinheit PRIMARY KEY (einheit_c_nr);
ALTER TABLE ONLY public.angb_angeboterledigungsgrund
    ADD CONSTRAINT pk_angb_angeboterledigungsgrund PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.angb_angeboterledigungsgrundspr
    ADD CONSTRAINT pk_angb_angeboterledigungsgrundspr PRIMARY KEY (locale_c_nr, angeboterledigungsgrund_c_nr);
ALTER TABLE ONLY public.angb_angebotposition
    ADD CONSTRAINT pk_angb_angebotposition PRIMARY KEY (i_id);
ALTER TABLE ONLY public.angb_angebotpositionart
    ADD CONSTRAINT pk_angb_angebotpositionart PRIMARY KEY (positionsart_c_nr);
ALTER TABLE ONLY public.angb_angebotstatus
    ADD CONSTRAINT pk_angb_angebotstatus PRIMARY KEY (status_c_nr);
ALTER TABLE ONLY public.angb_angebottext
    ADD CONSTRAINT pk_angb_angebottext PRIMARY KEY (i_id);
ALTER TABLE ONLY public.angb_angebotartspr
    ADD CONSTRAINT pk_angebotspr PRIMARY KEY (locale_c_nr, angebotart_c_nr);
ALTER TABLE ONLY public.pers_angehoerigenartspr
    ADD CONSTRAINT pk_angehoerartspr PRIMARY KEY (locale_c_nr, angehoerigenart_c_nr);
ALTER TABLE ONLY public.pers_angehoerigenart
    ADD CONSTRAINT pk_angehoerigenart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.ww_artgru
    ADD CONSTRAINT pk_artgru PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artgruspr
    ADD CONSTRAINT pk_artgruspr PRIMARY KEY (locale_c_nr, artgru_i_id);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT pk_artikel PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikelartspr
    ADD CONSTRAINT pk_artikelartspr2 PRIMARY KEY (locale_c_nr, artikelart_c_nr);
ALTER TABLE ONLY public.ww_artkla
    ADD CONSTRAINT pk_artkla PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artklaspr
    ADD CONSTRAINT pk_artklaspr1 PRIMARY KEY (locale_c_nr, artkla_i_id);
ALTER TABLE ONLY public.as_agstkl
    ADD CONSTRAINT pk_as_agstkl PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_agstklarbeitsplan
    ADD CONSTRAINT pk_as_agstklarbeitsplan PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_agstklaufschlag
    ADD CONSTRAINT pk_as_agstklaufschlag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_agstklmaterial
    ADD CONSTRAINT pk_as_agstklmaterial PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_agstklmengenstaffel
    ADD CONSTRAINT pk_as_agstklmengenstaffel PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_agstklmengenstaffel_schnellerfassung
    ADD CONSTRAINT pk_as_agstklmengenstaffel_schnellerfassung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_agstklpositionsart
    ADD CONSTRAINT pk_as_agstklposition PRIMARY KEY (positionsart_c_nr);
ALTER TABLE ONLY public.as_agstklposition
    ADD CONSTRAINT pk_as_agstklposition_1 PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_aufschlag
    ADD CONSTRAINT pk_as_aufschlag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_einkaufsangebot
    ADD CONSTRAINT pk_as_einkaufsangebot PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_einkaufsangebotposition
    ADD CONSTRAINT pk_as_einkaufsangebotposition PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_ekaglieferant
    ADD CONSTRAINT pk_as_ekaglieferant PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_ekgruppe
    ADD CONSTRAINT pk_as_ekgruppe PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_ekgruppelieferant
    ADD CONSTRAINT pk_as_ekgruppelieferant PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_ekweblieferant
    ADD CONSTRAINT pk_as_ekweblieferant PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_positionlieferant
    ADD CONSTRAINT pk_as_positionlieferant PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_webabfrage
    ADD CONSTRAINT pk_as_webabfrage PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_webfindchips
    ADD CONSTRAINT pk_as_webfindchips PRIMARY KEY (webpartner_i_id);
ALTER TABLE ONLY public.as_weblieferant
    ADD CONSTRAINT pk_as_weblieferant PRIMARY KEY (i_id);
ALTER TABLE ONLY public.as_webpartner
    ADD CONSTRAINT pk_as_webpartner PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT pk_auft_auftrag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_auftragart
    ADD CONSTRAINT pk_auft_auftragart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.auft_auftragartspr
    ADD CONSTRAINT pk_auft_auftragartspr PRIMARY KEY (locale_c_nr, auftragart_c_nr);
ALTER TABLE ONLY public.auft_auftragauftragdokument
    ADD CONSTRAINT pk_auft_auftragauftragdokument PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_auftragbegruendung
    ADD CONSTRAINT pk_auft_auftragbegruendung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_auftragdokument
    ADD CONSTRAINT pk_auft_auftragdokument PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_auftragkostenstelle
    ADD CONSTRAINT pk_auft_auftragkostenstelle PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_auftragposition
    ADD CONSTRAINT pk_auft_auftragposition PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_auftragpositionart
    ADD CONSTRAINT pk_auft_auftragpositionart PRIMARY KEY (positionsart_c_nr);
ALTER TABLE ONLY public.auft_auftragpositionstatus
    ADD CONSTRAINT pk_auft_auftragpositionstatus PRIMARY KEY (status_c_nr);
ALTER TABLE ONLY public.auft_auftragseriennrn
    ADD CONSTRAINT pk_auft_auftragseriennrn PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_auftragstatus
    ADD CONSTRAINT pk_auft_auftragstatus PRIMARY KEY (status_c_nr);
ALTER TABLE ONLY public.auft_auftragteilnehmer
    ADD CONSTRAINT pk_auft_auftragteilnehmer PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_auftragtext
    ADD CONSTRAINT pk_auft_auftragtext PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_auftragwiederholungsintervall
    ADD CONSTRAINT pk_auft_auftragwiederholungsintervall PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.auft_auftragwiederholungsintervallspr
    ADD CONSTRAINT pk_auft_auftragwiederholungsintervallspr PRIMARY KEY (locale_c_nr, auftragwiederholungsintervall_c_nr);
ALTER TABLE ONLY public.auft_indexanpassung_log
    ADD CONSTRAINT pk_auft_indexanpassung_log PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_meilenstein
    ADD CONSTRAINT pk_auft_meilenstein PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_meilensteinspr
    ADD CONSTRAINT pk_auft_meilensteinspr PRIMARY KEY (meilenstein_i_id, locale_c_nr);
ALTER TABLE ONLY public.auft_verrechenbar
    ADD CONSTRAINT pk_auft_verrechenbar PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_zahlungsplan
    ADD CONSTRAINT pk_auft_zahlungsplan PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_zahlungsplanmeilenstein
    ADD CONSTRAINT pk_auft_zahlungsplanmeilenstein PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_zeitplan
    ADD CONSTRAINT pk_auft_zeitplan PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_zeitplantyp
    ADD CONSTRAINT pk_auft_zeitplantyp PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auft_zeitplantypdetail
    ADD CONSTRAINT pk_auft_zeitplantypdetail PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_4vending_export
    ADD CONSTRAINT pk_auto_4vending_export PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_arbeitszeitstatus
    ADD CONSTRAINT pk_auto_arbeitszeitstatus PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_artikellieferant_webabfrage
    ADD CONSTRAINT pk_auto_artikellieferant_webabfrage PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_auslieferliste
    ADD CONSTRAINT pk_auto_auslieferliste PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_bedarfsuebernahmeoffenjournal
    ADD CONSTRAINT pk_auto_bedarfsuebernahmeoffenjournal PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_bestellvorschlag
    ADD CONSTRAINT pk_auto_bestellvorschlag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_er_import
    ADD CONSTRAINT pk_auto_er_import PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_fehlmengendruck
    ADD CONSTRAINT pk_auto_fehlmengendruck PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_kpireport
    ADD CONSTRAINT pk_auto_kpireport PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_loseerledigen
    ADD CONSTRAINT pk_auto_loseerledigen PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_lumiquote
    ADD CONSTRAINT pk_auto_lumiquote PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_mahnungsversand
    ADD CONSTRAINT pk_auto_mahnungsversand PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_monatsabrechnungversand
    ADD CONSTRAINT pk_auto_monatsabrechnungversand PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_monatsabrechnungversand_abteilungen
    ADD CONSTRAINT pk_auto_monatsabrechnungversand_abteilungen PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_shopsync_item
    ADD CONSTRAINT pk_auto_shopsync_item PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_shopsync_order
    ADD CONSTRAINT pk_auto_shopsync_order PRIMARY KEY (i_id);
ALTER TABLE ONLY public.auto_wejournal
    ADD CONSTRAINT pk_auto_wejournal PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_belegart
    ADD CONSTRAINT pk_belegart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.lp_belegartspr
    ADD CONSTRAINT pk_belegartspr PRIMARY KEY (locale_c_nr, belegart_c_nr);
ALTER TABLE ONLY public.bes_bestellposition
    ADD CONSTRAINT pk_bes_bestellposition PRIMARY KEY (i_id);
ALTER TABLE ONLY public.bes_bestellpositionart
    ADD CONSTRAINT pk_bes_bestellpositionart PRIMARY KEY (positionsart_c_nr);
ALTER TABLE ONLY public.bes_bestellpositionstatus
    ADD CONSTRAINT pk_bes_bestellpositionstatus PRIMARY KEY (status_c_nr);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT pk_bes_bestellung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.bes_bestellungart
    ADD CONSTRAINT pk_bes_bestellungart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.bes_bestellungartspr
    ADD CONSTRAINT pk_bes_bestellungartspr PRIMARY KEY (locale_c_nr, bestellungart_c_nr);
ALTER TABLE ONLY public.bes_bestellungstatus
    ADD CONSTRAINT pk_bes_bestellungstatus PRIMARY KEY (status_c_nr);
ALTER TABLE ONLY public.bes_bestellungtext
    ADD CONSTRAINT pk_bes_bestellungtext PRIMARY KEY (i_id);
ALTER TABLE ONLY public.bes_bsmahnlauf
    ADD CONSTRAINT pk_bes_bsmahnlauf PRIMARY KEY (i_id);
ALTER TABLE ONLY public.bes_bsmahnstufe
    ADD CONSTRAINT pk_bes_bsmahnstufe PRIMARY KEY (i_id, mandant_c_nr);
ALTER TABLE ONLY public.bes_bsmahntext
    ADD CONSTRAINT pk_bes_bsmahntext PRIMARY KEY (i_id);
ALTER TABLE ONLY public.bes_bsmahnung
    ADD CONSTRAINT pk_bes_bsmahnung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.bes_bszahlungsplan
    ADD CONSTRAINT pk_bes_bszahlungsplan PRIMARY KEY (i_id);
ALTER TABLE ONLY public.bes_mahngruppe
    ADD CONSTRAINT pk_bes_mahngruppe PRIMARY KEY (artgru_i_id);
ALTER TABLE ONLY public.bes_wareneingang
    ADD CONSTRAINT pk_bes_wareneingang PRIMARY KEY (i_id);
ALTER TABLE ONLY public.bes_wareneingangsposition
    ADD CONSTRAINT pk_bes_wareneingangposition PRIMARY KEY (i_id);
ALTER TABLE ONLY public.bes_bestellvorschlag
    ADD CONSTRAINT pk_bestellvorschlag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_einheit
    ADD CONSTRAINT pk_eht PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.er_auftragszuordnung
    ADD CONSTRAINT pk_er_auftragszuordnung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.er_auftragszuordnungverrechnet
    ADD CONSTRAINT pk_er_auftragszuordnungverrechnet PRIMARY KEY (i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT pk_er_eingangsrechnung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.er_eingangsrechnungart
    ADD CONSTRAINT pk_er_eingangsrechnungart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.er_eingangsrechnungartspr
    ADD CONSTRAINT pk_er_eingangsrechnungartspr PRIMARY KEY (eingangsrechnungart_c_nr, locale_c_nr);
ALTER TABLE ONLY public.er_eingangsrechnungstatus
    ADD CONSTRAINT pk_er_eingangsrechnungstatus PRIMARY KEY (status_c_nr);
ALTER TABLE ONLY public.er_eingangsrechnungtext
    ADD CONSTRAINT pk_er_eingangsrechnungtext PRIMARY KEY (i_id);
ALTER TABLE ONLY public.er_eingangsrechnungzahlung
    ADD CONSTRAINT pk_er_eingangsrechnungzahlung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.er_kontierung
    ADD CONSTRAINT pk_er_kontierung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.er_zahlungsvorschlag
    ADD CONSTRAINT pk_er_zahlungsvorschlag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.er_zahlungsvorschlaglauf
    ADD CONSTRAINT pk_er_zahlungsvorschlaglauf PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_familienstand
    ADD CONSTRAINT pk_familienstand PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.pers_familienstandspr
    ADD CONSTRAINT pk_familienstspr PRIMARY KEY (locale_c_nr, familienstand_c_nr);
ALTER TABLE ONLY public.fb_belegart
    ADD CONSTRAINT pk_fb_belegart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.fb_belegbuchung
    ADD CONSTRAINT pk_fb_belegbuchung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_buchungdetail
    ADD CONSTRAINT pk_fb_buchungdetail PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_buchungdetailart
    ADD CONSTRAINT pk_fb_buchungdetailart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.fb_buchung
    ADD CONSTRAINT pk_fb_buchungsjournal PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_ergebnisgruppe
    ADD CONSTRAINT pk_fb_ergebnisgruppe PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_exportdaten
    ADD CONSTRAINT pk_fb_exportdaten PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_exportlauf
    ADD CONSTRAINT pk_fb_exportlauf PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_iso20022bankverbindung
    ADD CONSTRAINT pk_fb_iso20022bankverbindung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_iso20022lastschriftschema
    ADD CONSTRAINT pk_fb_iso20022lastschriftschema PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_iso20022schema
    ADD CONSTRAINT pk_fb_iso20022schema PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_iso20022standard
    ADD CONSTRAINT pk_fb_iso20022standard PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_iso20022zahlungsauftragschema
    ADD CONSTRAINT pk_fb_iso20022zahlungsauftragschema PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_kontolaenderart
    ADD CONSTRAINT pk_fb_kontolaenderart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_kontoland
    ADD CONSTRAINT pk_fb_kontoland PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_mahnlauf
    ADD CONSTRAINT pk_fb_mahnlauf PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_mahnspesen
    ADD CONSTRAINT pk_fb_mahnspesen PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_mahnstufe
    ADD CONSTRAINT pk_fb_mahnstufe PRIMARY KEY (i_id, mandant_c_nr);
ALTER TABLE ONLY public.fb_mahntext
    ADD CONSTRAINT pk_fb_mahntext PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_mahnung
    ADD CONSTRAINT pk_fb_mahnung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_rechenregel
    ADD CONSTRAINT pk_fb_rechenregel PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.fb_reversechargeart
    ADD CONSTRAINT pk_fb_reversechargeart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_sepakontoauszug
    ADD CONSTRAINT pk_fb_sepakontoauszug PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_steuerkategorie
    ADD CONSTRAINT pk_fb_steuerkategorie PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_steuerkategoriekonto
    ADD CONSTRAINT pk_fb_steuerkategoriekonto PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_ustuebersetzung
    ADD CONSTRAINT pk_fb_ustuebersetzung PRIMARY KEY (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.fb_uvaart
    ADD CONSTRAINT pk_fb_uvaart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_uvaformular
    ADD CONSTRAINT pk_fb_uvaformular PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_uvaverprobung
    ADD CONSTRAINT pk_fb_uvaverprobung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_warenverkehrsnummer
    ADD CONSTRAINT pk_fb_warenverkehrsnummer PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.fc_fclieferadresse
    ADD CONSTRAINT pk_fc_fclieferadresse PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fc_forecast
    ADD CONSTRAINT pk_fc_forecast PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fc_forecastart
    ADD CONSTRAINT pk_fc_forecastart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.fc_forecastartspr
    ADD CONSTRAINT pk_fc_forecastartspr PRIMARY KEY (locale_c_nr, forecastart_c_nr);
ALTER TABLE ONLY public.fc_forecastauftrag
    ADD CONSTRAINT pk_fc_forecastauftrag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fc_forecastposition
    ADD CONSTRAINT pk_fc_forecastposition PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fc_importdef
    ADD CONSTRAINT pk_fc_importdef PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.fc_importdefspr
    ADD CONSTRAINT pk_fc_importdefspr PRIMARY KEY (locale_c_nr, importdef_c_nr);
ALTER TABLE ONLY public.fc_kommdrucker
    ADD CONSTRAINT pk_fc_kommdrucker PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fc_linienabruf
    ADD CONSTRAINT pk_fc_linienabruf PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_bedarfsuebernahme
    ADD CONSTRAINT pk_fert_bedarfsuebernahme PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_internebestellung
    ADD CONSTRAINT pk_fert_internebestelung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT pk_fert_los PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_losablieferung
    ADD CONSTRAINT pk_fert_losablieferung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_losbereich
    ADD CONSTRAINT pk_fert_losbereich PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_losgutschlecht
    ADD CONSTRAINT pk_fert_losgutschlecht PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_losklasse
    ADD CONSTRAINT pk_fert_losklasse PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_loslosklasse
    ADD CONSTRAINT pk_fert_losklassemapping PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_losklassespr
    ADD CONSTRAINT pk_fert_losklassespr PRIMARY KEY (losklasse_i_id, locale_c_nr);
ALTER TABLE ONLY public.fert_loslagerentnahme
    ADD CONSTRAINT pk_fert_losmateriallager PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_lospruefplan
    ADD CONSTRAINT pk_fert_lospruefplan PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_lossollmaterial
    ADD CONSTRAINT pk_fert_lossollmaterial PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_losstatus
    ADD CONSTRAINT pk_fert_losstatus PRIMARY KEY (status_c_nr);
ALTER TABLE ONLY public.fert_lostechniker
    ADD CONSTRAINT pk_fert_lostechniker PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_loszusatzstatus
    ADD CONSTRAINT pk_fert_loszusatzstatus PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_pruefergebins
    ADD CONSTRAINT pk_fert_pruefergebins PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_lossollarbeitsplan
    ADD CONSTRAINT pk_fert_sollarbeitsplan PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_wiederholendelose
    ADD CONSTRAINT pk_fert_wiederholendelose PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_zusatzstatus
    ADD CONSTRAINT pk_fert_zusatzstatus PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_hardwareart
    ADD CONSTRAINT pk_hardwareart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.lp_hardwareartspr
    ADD CONSTRAINT pk_hardwareartspr PRIMARY KEY (locale_c_nr, hardwareart_c_nr);
ALTER TABLE ONLY public.is_anlage
    ADD CONSTRAINT pk_is_anlage PRIMARY KEY (i_id);
ALTER TABLE ONLY public.is_geraet
    ADD CONSTRAINT pk_is_geraet PRIMARY KEY (i_id);
ALTER TABLE ONLY public.is_geraetehistorie
    ADD CONSTRAINT pk_is_geraetehistorie PRIMARY KEY (i_id);
ALTER TABLE ONLY public.is_geraetetyp
    ADD CONSTRAINT pk_is_geraetetyp PRIMARY KEY (i_id);
ALTER TABLE ONLY public.is_gewerk
    ADD CONSTRAINT pk_is_gewerk PRIMARY KEY (i_id);
ALTER TABLE ONLY public.is_halle
    ADD CONSTRAINT pk_is_halle PRIMARY KEY (i_id);
ALTER TABLE ONLY public.is_instandhaltung
    ADD CONSTRAINT pk_is_instandhaltung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.is_ismaschine
    ADD CONSTRAINT pk_is_ismaschine PRIMARY KEY (i_id);
ALTER TABLE ONLY public.is_kategorie
    ADD CONSTRAINT pk_is_kategorie PRIMARY KEY (i_id);
ALTER TABLE ONLY public.is_standort
    ADD CONSTRAINT pk_is_standort PRIMARY KEY (i_id);
ALTER TABLE ONLY public.is_standorttechniker
    ADD CONSTRAINT pk_is_standorttechniker PRIMARY KEY (i_id);
ALTER TABLE ONLY public.is_wartungsliste
    ADD CONSTRAINT pk_is_wartungsliste PRIMARY KEY (i_id);
ALTER TABLE ONLY public.is_wartungsschritte
    ADD CONSTRAINT pk_is_wartungsschritte PRIMARY KEY (i_id);
ALTER TABLE ONLY public.iv_inserat
    ADD CONSTRAINT pk_iv_inserat PRIMARY KEY (i_id);
ALTER TABLE ONLY public.iv_inseratartikel
    ADD CONSTRAINT pk_iv_inseratartikel PRIMARY KEY (i_id);
ALTER TABLE ONLY public.iv_inserater
    ADD CONSTRAINT pk_iv_inserater PRIMARY KEY (i_id);
ALTER TABLE ONLY public.iv_inseratrechnung
    ADD CONSTRAINT pk_iv_inseratrechnung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.iv_inseratrechnungartikel
    ADD CONSTRAINT pk_iv_inseratrechnungartikel PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_kollektiv
    ADD CONSTRAINT pk_kollektiv PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT pk_konto PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_kostenstelle
    ADD CONSTRAINT pk_kost PRIMARY KEY (i_id);
ALTER TABLE ONLY public.kue_bedienerlager
    ADD CONSTRAINT pk_kue_bedienerlager PRIMARY KEY (i_id);
ALTER TABLE ONLY public.kue_kassaartikel
    ADD CONSTRAINT pk_kue_kassaartikel PRIMARY KEY (i_id);
ALTER TABLE ONLY public.kue_kassaimport
    ADD CONSTRAINT pk_kue_kassaimport PRIMARY KEY (i_id);
ALTER TABLE ONLY public.kue_kdc100log
    ADD CONSTRAINT pk_kue_kdc100log PRIMARY KEY (i_id);
ALTER TABLE ONLY public.kue_kuecheumrechnung
    ADD CONSTRAINT pk_kue_kuecheumrechnung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.kue_speiseplan
    ADD CONSTRAINT pk_kue_speiseplan PRIMARY KEY (i_id);
ALTER TABLE ONLY public.kue_speiseplanposition
    ADD CONSTRAINT pk_kue_speiseplanposition PRIMARY KEY (i_id);
ALTER TABLE ONLY public.kue_tageslos
    ADD CONSTRAINT pk_kue_tageslos PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_lagerbewegung
    ADD CONSTRAINT pk_lagerzugang PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_lagerabgangursprung
    ADD CONSTRAINT pk_lagerzuganglagerabgang PRIMARY KEY (i_lagerbewegungid, i_lagerbewegungidursprung);
ALTER TABLE ONLY public.ww_lagerzugangursprung
    ADD CONSTRAINT pk_lagerzugangursprung PRIMARY KEY (i_lagerbewegungid, i_lagerbewegungidursprung);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT pk_lieferung1 PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikelfehlmenge
    ADD CONSTRAINT pk_los_fehlmenge PRIMARY KEY (i_id);
ALTER TABLE ONLY public.fert_losistmaterial
    ADD CONSTRAINT pk_los_istmaterial PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_anwender
    ADD CONSTRAINT pk_lp_anwender PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_arbeitsplatz
    ADD CONSTRAINT pk_lp_arbeitsplatz PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_arbeitsplatzkonfiguration
    ADD CONSTRAINT pk_lp_arbeitsplatzkonfiguration PRIMARY KEY (arbeitsplatz_i_id);
ALTER TABLE ONLY public.lp_arbeitsplatzparameter
    ADD CONSTRAINT pk_lp_arbeitsplatzparameter PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_arbeitsplatztyp
    ADD CONSTRAINT pk_lp_arbeitsplatztyp PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.lp_automatikjobs
    ADD CONSTRAINT pk_lp_automatikjobs PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_automatikjobtype
    ADD CONSTRAINT pk_lp_automatikjobtype PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_automatiktimer
    ADD CONSTRAINT pk_lp_automatiktimer PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_belegartdokument
    ADD CONSTRAINT pk_lp_belegartdokument PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_belegartmedia
    ADD CONSTRAINT pk_lp_belegartmedia PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_datenformat
    ADD CONSTRAINT pk_lp_datenformat PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.lp_direkthilfe
    ADD CONSTRAINT pk_lp_direkthilfe UNIQUE (c_token, locale_c_nr, mandant_c_nr, b_anwender);
ALTER TABLE ONLY public.lp_dokument
    ADD CONSTRAINT pk_lp_dokument PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_dokumentbelegart
    ADD CONSTRAINT pk_lp_dokumentbelegart PRIMARY KEY (mandant_c_nr, c_nr);
ALTER TABLE ONLY public.lp_dokumentenlink
    ADD CONSTRAINT pk_lp_dokumentenlink PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_dokumentenlinkbeleg
    ADD CONSTRAINT pk_lp_dokumentenlinkbeleg PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_dokumentgruppierung
    ADD CONSTRAINT pk_lp_dokumentgruppierung PRIMARY KEY (mandant_c_nr, c_nr);
ALTER TABLE ONLY public.lp_dokumentnichtarchiviert
    ADD CONSTRAINT pk_lp_dokumentnichtarchiviert PRIMARY KEY (mandant_c_nr, c_reportname);
ALTER TABLE ONLY public.lp_dokumentschlagwort
    ADD CONSTRAINT pk_lp_dokumentschlagwort PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_editor_base_block
    ADD CONSTRAINT pk_lp_editor_base_block PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_editor_content
    ADD CONSTRAINT pk_lp_editor_content PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_editor_image_block
    ADD CONSTRAINT pk_lp_editor_image_block PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_editor_text_block
    ADD CONSTRAINT pk_lp_editor_text_block PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_einheitkonvertierung
    ADD CONSTRAINT pk_lp_einheitkonvertierung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_einheitspr
    ADD CONSTRAINT pk_lp_einheitspr PRIMARY KEY (einheit_c_nr, locale_c_nr);
ALTER TABLE ONLY public.lp_entitylog
    ADD CONSTRAINT pk_lp_entitylog PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_extraliste
    ADD CONSTRAINT pk_lp_extraliste PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_funktion
    ADD CONSTRAINT pk_lp_funktion PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_funktionspr
    ADD CONSTRAINT pk_lp_funktionspr PRIMARY KEY (funktion_i_id, locale_c_nr);
ALTER TABLE ONLY public.lp_geschaeftsjahr
    ADD CONSTRAINT pk_lp_geschaeftsjahr PRIMARY KEY (i_geschaeftsjahr);
ALTER TABLE ONLY public.lp_geschaeftsjahrmandant
    ADD CONSTRAINT pk_lp_geschaeftsjahrmandant PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_hvmausercount
    ADD CONSTRAINT pk_lp_hvmausercount PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_image
    ADD CONSTRAINT pk_lp_image PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_installer
    ADD CONSTRAINT pk_lp_installer PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_internekopie
    ADD CONSTRAINT pk_lp_internekopie PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_kennung
    ADD CONSTRAINT pk_lp_kennung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_kennungspr
    ADD CONSTRAINT pk_lp_kennungspr PRIMARY KEY (kennung_i_id, locale_c_nr);
ALTER TABLE ONLY public.lp_keyvalue
    ADD CONSTRAINT pk_lp_keyvalue PRIMARY KEY (c_gruppe, c_key);
ALTER TABLE ONLY public.fb_kontoart
    ADD CONSTRAINT pk_lp_kontoart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.fb_kontoartspr
    ADD CONSTRAINT pk_lp_kontoartspr PRIMARY KEY (kontoart_c_nr, locale_c_nr);
ALTER TABLE ONLY public.fb_kontotyp
    ADD CONSTRAINT pk_lp_kontotyp PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.fb_kontotypspr
    ADD CONSTRAINT pk_lp_kontotypspr PRIMARY KEY (kontotyp_c_nr, locale_c_nr);
ALTER TABLE ONLY public.lp_kostentraeger
    ADD CONSTRAINT pk_lp_kostentraeger PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_land
    ADD CONSTRAINT pk_lp_land PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_landkfzkennzeichen
    ADD CONSTRAINT pk_lp_landkfzkennzeichen PRIMARY KEY (c_lkz);
ALTER TABLE ONLY public.lp_landplzort
    ADD CONSTRAINT pk_lp_landplzort PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_landspr
    ADD CONSTRAINT pk_lp_landspr PRIMARY KEY (locale_c_nr, land_i_id);
ALTER TABLE ONLY public.lp_lieferart
    ADD CONSTRAINT pk_lp_lieferart2 PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_lieferartspr
    ADD CONSTRAINT pk_lp_lieferartspr2 PRIMARY KEY (lieferart_i_id, locale_c_nr);
ALTER TABLE ONLY public.lp_spediteur
    ADD CONSTRAINT pk_lp_lieferbedingungen_des_spediteurs PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_mailproperty
    ADD CONSTRAINT pk_lp_mailproperty PRIMARY KEY (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.lp_mandantagbspr
    ADD CONSTRAINT pk_lp_mandantagbspr PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_mediaart
    ADD CONSTRAINT pk_lp_mediaart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.lp_mediaartspr
    ADD CONSTRAINT pk_lp_mediaartspr PRIMARY KEY (mediaart_c_nr, locale_c_nr);
ALTER TABLE ONLY public.lp_mediastandard
    ADD CONSTRAINT pk_lp_mediastandard PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_modulberechtigung
    ADD CONSTRAINT pk_lp_modulberechtigung PRIMARY KEY (belegart_c_nr, mandant_c_nr);
ALTER TABLE ONLY public.lp_mwstsatz
    ADD CONSTRAINT pk_lp_mwstsatz PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_mwstsatzbez
    ADD CONSTRAINT pk_lp_mwstsatzbez PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_mwstsatzcode
    ADD CONSTRAINT pk_lp_mwstsatzcode PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_ort
    ADD CONSTRAINT pk_lp_ort PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_panel
    ADD CONSTRAINT pk_lp_panel PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.lp_panelbeschreibung
    ADD CONSTRAINT pk_lp_panelbeschreibung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_paneldaten
    ADD CONSTRAINT pk_lp_paneldaten PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_panelsperren
    ADD CONSTRAINT pk_lp_panelsperren PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_parameter
    ADD CONSTRAINT pk_lp_parameter PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.lp_parameteranwender
    ADD CONSTRAINT pk_lp_parameteranwender PRIMARY KEY (c_nr, c_kategorie);
ALTER TABLE ONLY public.lp_parametermandant
    ADD CONSTRAINT pk_lp_parametermandant PRIMARY KEY (c_nr, mandant_c_nr, c_kategorie);
ALTER TABLE ONLY public.lp_parametermandantgueltigab
    ADD CONSTRAINT pk_lp_parametermandantgueltigab PRIMARY KEY (c_nr, mandant_c_nr, c_kategorie, t_gueltigab);
ALTER TABLE ONLY public.lp_positionsart
    ADD CONSTRAINT pk_lp_positionsart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.lp_positionsartspr
    ADD CONSTRAINT pk_lp_positionsartspr PRIMARY KEY (positionsart_c_nr, locale_c_nr);
ALTER TABLE ONLY public.lp_primarykey
    ADD CONSTRAINT pk_lp_primarykey PRIMARY KEY (c_name);
ALTER TABLE ONLY public.lp_primarykey_belegnr
    ADD CONSTRAINT pk_lp_primarykey_belegnr PRIMARY KEY (i_geschaeftsjahr, mandant_c_nr, c_nametabelle, c_namebeleg);
ALTER TABLE ONLY public.lp_protokoll
    ADD CONSTRAINT pk_lp_protokoll PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rech_rechnungart
    ADD CONSTRAINT pk_lp_rechnungart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.rech_rechnungtyp
    ADD CONSTRAINT pk_lp_rechnungtyp PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.rech_rechnungstatus
    ADD CONSTRAINT pk_lp_rechstatus PRIMARY KEY (status_c_nr);
ALTER TABLE ONLY public.pers_recht
    ADD CONSTRAINT pk_lp_recht PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.lp_reportkonf
    ADD CONSTRAINT pk_lp_reportkonf PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_reportvariante
    ADD CONSTRAINT pk_lp_reportvariante PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_rollerecht
    ADD CONSTRAINT pk_lp_rollerecht PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_standarddrucker
    ADD CONSTRAINT pk_lp_standarddrucker PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_tagesart
    ADD CONSTRAINT pk_lp_tagesart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_text
    ADD CONSTRAINT pk_lp_text PRIMARY KEY (c_token, mandant_c_nr, locale_c_nr);
ALTER TABLE ONLY public.lp_thejudge
    ADD CONSTRAINT pk_lp_thejudge PRIMARY KEY (c_wer, c_was, c_usernr);
ALTER TABLE ONLY public.lp_versandanhang
    ADD CONSTRAINT pk_lp_versandanhang PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_versandauftrag
    ADD CONSTRAINT pk_lp_versandauftrag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_versandstatus
    ADD CONSTRAINT pk_lp_versandstatus PRIMARY KEY (status_c_nr);
ALTER TABLE ONLY public.lp_versandweg
    ADD CONSTRAINT pk_lp_versandweg PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_versandwegberechtigung
    ADD CONSTRAINT pk_lp_versandwegberechtigung PRIMARY KEY (i_id, mandant_c_nr);
ALTER TABLE ONLY public.lp_versandwegcc
    ADD CONSTRAINT pk_lp_versandwegcc PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_versandwegccpartner
    ADD CONSTRAINT pk_lp_versandwegccpartner PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_versandwegpartner
    ADD CONSTRAINT pk_lp_versandwegpartner PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_versandwegpartnercc
    ADD CONSTRAINT pk_lp_versandwegpartnercc PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_versandwegpartnerdesadv
    ADD CONSTRAINT pk_lp_versandwegpartnerdesadv PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_versandwegpartneredi4all
    ADD CONSTRAINT pk_lp_versandwegpartneredi4all PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_versandwegpartnerlinienabruf
    ADD CONSTRAINT pk_lp_versandwegpartnerlinienabruf PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_versandwegpartnerordrsp
    ADD CONSTRAINT pk_lp_versandwegpartnerordrsp PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_waehrung
    ADD CONSTRAINT pk_lp_waehrung PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.lp_wechselkurs
    ADD CONSTRAINT pk_lp_wechselkurs PRIMARY KEY (waehrung_c_nr_von, waehrung_c_nr_zu, t_datum);
ALTER TABLE ONLY public.lp_woerterbuch
    ADD CONSTRAINT pk_lp_woerterbuch PRIMARY KEY (id);
ALTER TABLE ONLY public.rech_zahlungsart
    ADD CONSTRAINT pk_lp_zahlungsart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.lp_zahlungsziel
    ADD CONSTRAINT pk_lp_zahlungsziel PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_zahlungszielspr
    ADD CONSTRAINT pk_lp_zahlungszielspr PRIMARY KEY (zahlungsziel_i_id, locale_c_nr);
ALTER TABLE ONLY public.lp_zusatzfunktion
    ADD CONSTRAINT pk_lp_zusatzfunktion PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.lp_zusatzfunktionberechtigung
    ADD CONSTRAINT pk_lp_zusatzfunktionberechtigung PRIMARY KEY (zusatzfunktion_c_nr, mandant_c_nr);
ALTER TABLE ONLY public.ls_ausliefervorschlag
    ADD CONSTRAINT pk_ls_ausliefervorschlag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ls_begruendung
    ADD CONSTRAINT pk_ls_begruendung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT pk_ls_lieferschein PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ls_lieferscheinart
    ADD CONSTRAINT pk_ls_lieferscheinart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.ls_lieferscheinartspr
    ADD CONSTRAINT pk_ls_lieferscheinartspr PRIMARY KEY (locale_c_nr, lieferscheinart_c_nr);
ALTER TABLE ONLY public.ls_lieferscheinposition
    ADD CONSTRAINT pk_ls_lieferscheinposition PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ls_lieferscheinpositionart
    ADD CONSTRAINT pk_ls_lieferscheinpositionart PRIMARY KEY (positionsart_c_nr);
ALTER TABLE ONLY public.ls_lieferscheinstatus
    ADD CONSTRAINT pk_ls_lieferscheinstatus PRIMARY KEY (status_c_nr);
ALTER TABLE ONLY public.ls_lieferscheintext
    ADD CONSTRAINT pk_ls_lieferscheintext PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ls_packstueck
    ADD CONSTRAINT pk_ls_packstueck PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ls_verkettet
    ADD CONSTRAINT pk_ls_verkettet PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_mandant
    ADD CONSTRAINT pk_mandant PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.media_emailattachment
    ADD CONSTRAINT pk_media_emailattachment PRIMARY KEY (i_id);
ALTER TABLE ONLY public.media_emailmeta
    ADD CONSTRAINT pk_media_emailmeta PRIMARY KEY (i_id);
ALTER TABLE ONLY public.media_inbox
    ADD CONSTRAINT pk_media_inbox PRIMARY KEY (i_id);
ALTER TABLE ONLY public.media_store
    ADD CONSTRAINT pk_media_store PRIMARY KEY (i_id);
ALTER TABLE ONLY public.media_storebeleg
    ADD CONSTRAINT pk_media_storebeleg PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rech_mmz
    ADD CONSTRAINT pk_mmz PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_anrede
    ADD CONSTRAINT pk_part_anrede PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.part_anredespr
    ADD CONSTRAINT pk_part_anredespr PRIMARY KEY (anrede_c_nr, locale_c_nr);
ALTER TABLE ONLY public.part_ansprechpartner
    ADD CONSTRAINT pk_part_ansprechpartner PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_ansprechpartnerfunktion
    ADD CONSTRAINT pk_part_ansprechpartnerfunktion PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_ansprechpartnerfunktionspr
    ADD CONSTRAINT pk_part_ansprechpartnerfunktionspr PRIMARY KEY (ansprechpartnerfunktion_i_id, locale_c_nr);
ALTER TABLE ONLY public.part_bank
    ADD CONSTRAINT pk_part_bank PRIMARY KEY (partner_i_id);
ALTER TABLE ONLY public.part_beauskunftung
    ADD CONSTRAINT pk_part_beauskunftung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_branche
    ADD CONSTRAINT pk_part_branche PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_branchespr
    ADD CONSTRAINT pk_part_branchespr PRIMARY KEY (branche_i_id, locale_c_nr);
ALTER TABLE ONLY public.part_dsgvokategorie
    ADD CONSTRAINT pk_part_dsgvokategorie PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_dsgvokategoriespr
    ADD CONSTRAINT pk_part_dsgvokategoriespr PRIMARY KEY (dsgvokategorie_i_id, locale_c_nr);
ALTER TABLE ONLY public.part_dsgvotext
    ADD CONSTRAINT pk_part_dsgvotext PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_geodaten
    ADD CONSTRAINT pk_part_geodaten PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_identifikation
    ADD CONSTRAINT pk_part_identifikation PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_identifikationspr
    ADD CONSTRAINT pk_part_identifikationspr PRIMARY KEY (identifikation_i_id, locale_c_nr);
ALTER TABLE ONLY public.part_kommunikationsart
    ADD CONSTRAINT pk_part_kommunikationsart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.part_kommunikationsartspr
    ADD CONSTRAINT pk_part_kommunikationsartspr PRIMARY KEY (kommunikationsart_c_nr, locale_c_nr);
ALTER TABLE ONLY public.part_kontakt
    ADD CONSTRAINT pk_part_kontakt PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_kontaktart
    ADD CONSTRAINT pk_part_kontaktart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT pk_part_kunde PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_kundekennung
    ADD CONSTRAINT pk_part_kundekennung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_kundematerial
    ADD CONSTRAINT pk_part_kundematerial PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_kundesachbearbeiter
    ADD CONSTRAINT pk_part_kundesachbearbeiter PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_kundesoko
    ADD CONSTRAINT pk_part_kundesoko PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_kundesokomengenstaffel
    ADD CONSTRAINT pk_part_kundesokomengenstaffel PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_kundespediteur
    ADD CONSTRAINT pk_part_kundespediteur PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_kurzbrief
    ADD CONSTRAINT pk_part_kurzbrief PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT pk_part_lieferant PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_lieferantbeurteilung
    ADD CONSTRAINT pk_part_lieferantbeurteilung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_lflfliefergruppe
    ADD CONSTRAINT pk_part_lieferantlieferantliefergruppe PRIMARY KEY (lieferant_i_id, lfliefergruppe_i_id);
ALTER TABLE ONLY public.part_lfliefergruppe
    ADD CONSTRAINT pk_part_lieferantliefergruppe PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_lfliefergruppespr
    ADD CONSTRAINT pk_part_lieferantliefergruppespr PRIMARY KEY (lfliefergruppe_i_id, locale_c_nr);
ALTER TABLE ONLY public.part_liefermengen
    ADD CONSTRAINT pk_part_liefermengen PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_newslettergrund
    ADD CONSTRAINT pk_part_newslettergrund PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_partner
    ADD CONSTRAINT pk_part_partner PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_partnerart
    ADD CONSTRAINT pk_part_partnerart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.part_partnerartspr
    ADD CONSTRAINT pk_part_partnerartspr PRIMARY KEY (partnerart_c_nr, locale_c_nr);
ALTER TABLE ONLY public.part_partnerbild
    ADD CONSTRAINT pk_part_partnerbild PRIMARY KEY (partner_i_id);
ALTER TABLE ONLY public.part_partnerklasse
    ADD CONSTRAINT pk_part_partnerklasse PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_partnerklassespr
    ADD CONSTRAINT pk_part_partnerklassespr PRIMARY KEY (partnerklasse_i_id, locale_c_nr);
ALTER TABLE ONLY public.part_partnerkommentar
    ADD CONSTRAINT pk_part_partnerkommentar PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_partnerkommentarart
    ADD CONSTRAINT pk_part_partnerkommentarart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_partnerkommentardruck
    ADD CONSTRAINT pk_part_partnerkommentardruck PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_partnerkommunikation
    ADD CONSTRAINT pk_part_partnerkommunikation PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_paselektion
    ADD CONSTRAINT pk_part_paselektion PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_selektion
    ADD CONSTRAINT pk_part_selektion PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_selektionspr
    ADD CONSTRAINT pk_part_selektionspr PRIMARY KEY (selektion_i_id, locale_c_nr);
ALTER TABLE ONLY public.part_serienbrief
    ADD CONSTRAINT pk_part_serienbrief PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_serienbriefselektion
    ADD CONSTRAINT pk_part_serienbriefselektion PRIMARY KEY (serienbrief_i_id, selektion_i_id);
ALTER TABLE ONLY public.part_serienbriefselektionnegativ
    ADD CONSTRAINT pk_part_serienbriefselektionnegativ PRIMARY KEY (serienbrief_i_id, selektion_i_id);
ALTER TABLE ONLY public.part_telefonnummer
    ADD CONSTRAINT pk_part_telefonnummer PRIMARY KEY (i_id);
ALTER TABLE ONLY public.lp_theclient
    ADD CONSTRAINT pk_part_theclient PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.part_weblieferantfarnell
    ADD CONSTRAINT pk_part_weblieferantfarnell PRIMARY KEY (webpartner_i_id);
ALTER TABLE ONLY public.pers_abwesenheitsart
    ADD CONSTRAINT pk_pers_abwesenheitsart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_abwesenheitsartspr
    ADD CONSTRAINT pk_pers_abwesenheitsartspr PRIMARY KEY (abwesenheitsart_i_id, locale_c_nr);
ALTER TABLE ONLY public.pers_anwesenheitsbestaetigung
    ADD CONSTRAINT pk_pers_anwesenheitsbestaetigung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_artgrurolle
    ADD CONSTRAINT pk_pers_artgrurolle PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_artikelzulage
    ADD CONSTRAINT pk_pers_artikelzulage PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_artikelzuschlag
    ADD CONSTRAINT pk_pers_artikelzuschlag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_benutzer
    ADD CONSTRAINT pk_pers_benutzer PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_benutzermandantsystemrolle
    ADD CONSTRAINT pk_pers_benutzermandantrolle PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_bereitschaft
    ADD CONSTRAINT pk_pers_bereitschaft PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_bereitschaftart
    ADD CONSTRAINT pk_pers_bereitschaftart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_bereitschafttag
    ADD CONSTRAINT pk_pers_bereitschafttag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_beruf
    ADD CONSTRAINT pk_pers_beruf PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_betriebskalender
    ADD CONSTRAINT pk_pers_betriebskalender PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_diaeten
    ADD CONSTRAINT pk_pers_diaeten PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_diaetentagessatz
    ADD CONSTRAINT pk_pers_diaetentagessatz PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_eintrittaustritt
    ADD CONSTRAINT pk_pers_eintrittaustritt PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_fahrzeug
    ADD CONSTRAINT pk_pers_fahrzeug PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_fahrzeugkosten
    ADD CONSTRAINT pk_pers_fahrzeugkosten PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_fahrzeugverwendungsart
    ADD CONSTRAINT pk_pers_fahrzeugverwendungsart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.pers_feiertag
    ADD CONSTRAINT pk_pers_feiertag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_fertigungsgrupperolle
    ADD CONSTRAINT pk_pers_fertigungsgrupperolle PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_fingerart
    ADD CONSTRAINT pk_pers_fingerart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_gleitzeitsaldo
    ADD CONSTRAINT pk_pers_gleitzeitsaldo PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_hvmabenutzer
    ADD CONSTRAINT pk_pers_hvmabenutzer PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_hvmabenutzerparameter
    ADD CONSTRAINT pk_pers_hvmabenutzerparameter PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_hvmalizenz
    ADD CONSTRAINT pk_pers_hvmalizenz PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_hvmaparameter
    ADD CONSTRAINT pk_pers_hvmaparameter PRIMARY KEY (c_nr, c_kategorie);
ALTER TABLE ONLY public.pers_hvmarecht
    ADD CONSTRAINT pk_pers_hvmarecht PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_hvmarolle
    ADD CONSTRAINT pk_pers_hvmarolle PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_hvmasync
    ADD CONSTRAINT pk_pers_hvmasync PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_kollektivuestd
    ADD CONSTRAINT pk_pers_kollektivuestd PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_kollektivuestd50
    ADD CONSTRAINT pk_pers_kollektivuestd50 PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_kollektivuestd_bva
    ADD CONSTRAINT pk_pers_kollektivuestd_bva PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_lagerrolle
    ADD CONSTRAINT pk_pers_lagerrolle PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_lohnart
    ADD CONSTRAINT pk_pers_lohnart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_lohnartstundenfaktor
    ADD CONSTRAINT pk_pers_lohnartstundenfaktor PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_lohngruppe
    ADD CONSTRAINT pk_pers_lohngruppe PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_lohnstundenart
    ADD CONSTRAINT pk_pers_lohnstundenart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.pers_maschine
    ADD CONSTRAINT pk_pers_maschine PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_maschineleistungsfaktor
    ADD CONSTRAINT pk_pers_maschineleistungsfaktor PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_maschinemaschinenzm
    ADD CONSTRAINT pk_pers_maschinemaschinenzm PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_maschinengruppe
    ADD CONSTRAINT pk_pers_maschinengruppe PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_maschinenkosten
    ADD CONSTRAINT pk_pers_maschinenkosten PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_maschinenzeitdaten
    ADD CONSTRAINT pk_pers_maschinenzeitdaten PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_maschinenzeitdatenverrechnet
    ADD CONSTRAINT pk_pers_maschinenzeitdatenverrechnet PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_maschinenzm
    ADD CONSTRAINT pk_pers_maschinenzm PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_maschinenzmtagesart
    ADD CONSTRAINT pk_pers_maschinenzmtagesart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_nachrichtarchiv
    ADD CONSTRAINT pk_pers_nachrichtarchiv PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_nachrichtart
    ADD CONSTRAINT pk_pers_nachrichtart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_nachrichten
    ADD CONSTRAINT pk_pers_nachrichten PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_nachrichtenabo
    ADD CONSTRAINT pk_pers_nachrichtenabo PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_nachrichtenart
    ADD CONSTRAINT pk_pers_nachrichtenart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_nachrichtenartspr
    ADD CONSTRAINT pk_pers_nachrichtenartspr PRIMARY KEY (nachrichtenart_i_id, locale_c_nr);
ALTER TABLE ONLY public.pers_nachrichtenempfaenger
    ADD CONSTRAINT pk_pers_nachrichtenempfaenger PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_nachrichtengruppe
    ADD CONSTRAINT pk_pers_nachrichtengruppe PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_nachrichtengruppeteilnehmer
    ADD CONSTRAINT pk_pers_nachrichtengruppeteilnehmer PRIMARY KEY (i_id);
ALTER TABLE ONLY public.part_partnerbank
    ADD CONSTRAINT pk_pers_partnerbank PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_passivereise
    ADD CONSTRAINT pk_pers_pasivereise PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_pendlerpauschale
    ADD CONSTRAINT pk_pers_pendlerpauschale PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT pk_pers_personal_neu PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_personalfahrzeug
    ADD CONSTRAINT pk_pers_personalfahrzeug PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_personalfinger
    ADD CONSTRAINT pk_pers_personalfinger PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_personalfunktion
    ADD CONSTRAINT pk_pers_personalfunktion PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.pers_personalfunktionspr
    ADD CONSTRAINT pk_pers_personalfunktionspr PRIMARY KEY (locale_c_nr, personalfunktion_c_nr);
ALTER TABLE ONLY public.pers_personalgehalt
    ADD CONSTRAINT pk_pers_personalgehalt PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_personalgruppe
    ADD CONSTRAINT pk_pers_personalgruppe PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_personalgruppekosten
    ADD CONSTRAINT pk_pers_personalgruppekosten PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_personalterminal
    ADD CONSTRAINT pk_pers_personalterminal PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_personalverfuegbarkeit
    ADD CONSTRAINT pk_pers_personalverfuegbarkeit PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_personalzeiten
    ADD CONSTRAINT pk_pers_personalzeiten PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_personalzeitmodell
    ADD CONSTRAINT pk_pers_personalzeitmodell PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_personalzutrittsklasse
    ADD CONSTRAINT pk_pers_personalzutrittsklasse PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_reise
    ADD CONSTRAINT pk_pers_reise PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_reiselog
    ADD CONSTRAINT pk_pers_reiselog PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_reisespesen
    ADD CONSTRAINT pk_pers_reisespesen PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_reiseverrechnet
    ADD CONSTRAINT pk_pers_reiseverrechnet PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_schicht
    ADD CONSTRAINT pk_pers_schicht PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_schichtzeit
    ADD CONSTRAINT pk_pers_schichtzeit PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_schichtzeitmodell
    ADD CONSTRAINT pk_pers_schichtzeitmodell PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_schichtzuschlag
    ADD CONSTRAINT pk_pers_schichtzuschlag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_signatur
    ADD CONSTRAINT pk_pers_signatur PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_sonderzeiten
    ADD CONSTRAINT pk_pers_sonderzeiten PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_stundenabrechnung
    ADD CONSTRAINT pk_pers_stundenabrechnung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_systemrolle
    ADD CONSTRAINT pk_pers_systemrolle PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_taetigkeit
    ADD CONSTRAINT pk_pers_taetigkeit PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_taetigkeitart
    ADD CONSTRAINT pk_pers_taetigkeitart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.pers_taetigkeitartspr
    ADD CONSTRAINT pk_pers_taetigkeitartspr PRIMARY KEY (locale_c_nr, taetigkeitart_c_nr);
ALTER TABLE ONLY public.pers_taetigkeitspr
    ADD CONSTRAINT pk_pers_taetigkeitspr PRIMARY KEY (taetigkeit_i_id, locale_c_nr);
ALTER TABLE ONLY public.pers_telefonzeiten
    ADD CONSTRAINT pk_pers_telefonzeiten PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_telefonzeitenverrechnet
    ADD CONSTRAINT pk_pers_telefonzeitenverrechnet PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_thema
    ADD CONSTRAINT pk_pers_thema PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.pers_themarolle
    ADD CONSTRAINT pk_pers_themarolle PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_urlaubsanspruch
    ADD CONSTRAINT pk_pers_urlaubsanspruch PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zahltag
    ADD CONSTRAINT pk_pers_zahltag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zeitabschluss
    ADD CONSTRAINT pk_pers_zeitabschluss PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zeitdaten
    ADD CONSTRAINT pk_pers_zeitdaten PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zeitdatenpruefen
    ADD CONSTRAINT pk_pers_zeitdatenpruefen PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zeitdatenverrechnet
    ADD CONSTRAINT pk_pers_zeitdatenverrechnet PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zeitdatenverrechnetzeitraum
    ADD CONSTRAINT pk_pers_zeitdatenverrechnetzeitraum PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zeitgutschrift
    ADD CONSTRAINT pk_pers_zeitgutschrift PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zeitmodell
    ADD CONSTRAINT pk_pers_zeitmodell PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zeitmodellspr
    ADD CONSTRAINT pk_pers_zeitmodellspr PRIMARY KEY (zeitmodell_i_id, locale_c_nr);
ALTER TABLE ONLY public.pers_zeitmodelltag
    ADD CONSTRAINT pk_pers_zeitmodelltag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zeitmodelltagpause
    ADD CONSTRAINT pk_pers_zeitmodelltagpause PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zeitstift
    ADD CONSTRAINT pk_pers_zeitstift PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zeitverteilung
    ADD CONSTRAINT pk_pers_zeitverteilung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zulage
    ADD CONSTRAINT pk_pers_zulage PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zutrittdaueroffen
    ADD CONSTRAINT pk_pers_zutrittdaueroffen PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zutrittonlinecheck
    ADD CONSTRAINT pk_pers_zutrittonlinecheck PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zutrittscontroller
    ADD CONSTRAINT pk_pers_zutrittscontroller PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zutrittsklasse
    ADD CONSTRAINT pk_pers_zutrittsklasse PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zutrittsklasseobjekt
    ADD CONSTRAINT pk_pers_zutrittsklasseobjekt PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zutrittsleser
    ADD CONSTRAINT pk_pers_zutrittsleser PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.pers_zutrittslog
    ADD CONSTRAINT pk_pers_zutrittslog PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zutrittsmodell
    ADD CONSTRAINT pk_pers_zutrittsmodell PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zutrittsmodelltag
    ADD CONSTRAINT pk_pers_zutrittsmodelltag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zutrittsmodelltagdetail
    ADD CONSTRAINT pk_pers_zutrittsmodelltagdetail PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zutrittsobjekt
    ADD CONSTRAINT pk_pers_zutrittsobjekt PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zutrittsobjektverwendung
    ADD CONSTRAINT pk_pers_zutrittsobjektverwendung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_zutrittsoeffnungsart
    ADD CONSTRAINT pk_pers_zutrittsoeffnungsart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.pers_personalangehoerige
    ADD CONSTRAINT pk_personalangeh PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_personalart
    ADD CONSTRAINT pk_personalart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.pers_personalartspr
    ADD CONSTRAINT pk_personalartspr PRIMARY KEY (locale_c_nr, personalart_c_nr);
ALTER TABLE ONLY public.proj_bereich
    ADD CONSTRAINT pk_proj_bereich PRIMARY KEY (i_id);
ALTER TABLE ONLY public.proj_history
    ADD CONSTRAINT pk_proj_history PRIMARY KEY (i_id);
ALTER TABLE ONLY public.proj_historyart
    ADD CONSTRAINT pk_proj_historyart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.proj_kategorie
    ADD CONSTRAINT pk_proj_kategorie PRIMARY KEY (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.proj_kategoriespr
    ADD CONSTRAINT pk_proj_kategoriespr PRIMARY KEY (locale_c_nr, kategorie_c_nr, mandant_c_nr);
ALTER TABLE ONLY public.proj_leadstatus
    ADD CONSTRAINT pk_proj_leadstatus PRIMARY KEY (i_id);
ALTER TABLE ONLY public.proj_projekt
    ADD CONSTRAINT pk_proj_projekt PRIMARY KEY (i_id);
ALTER TABLE ONLY public.proj_projekterledigungsgrund
    ADD CONSTRAINT pk_proj_projekterledigungsgrund PRIMARY KEY (i_id);
ALTER TABLE ONLY public.proj_projektgruppe
    ADD CONSTRAINT pk_proj_projektgruppe PRIMARY KEY (i_id);
ALTER TABLE ONLY public.proj_projektstatus
    ADD CONSTRAINT pk_proj_projektstatus PRIMARY KEY (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.proj_projekttaetigkeit
    ADD CONSTRAINT pk_proj_projekttaetigkeit PRIMARY KEY (i_id);
ALTER TABLE ONLY public.proj_projekttechniker
    ADD CONSTRAINT pk_proj_projekttechniker PRIMARY KEY (i_id);
ALTER TABLE ONLY public.proj_projekttyp
    ADD CONSTRAINT pk_proj_projekttyp PRIMARY KEY (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.proj_projekttypspr
    ADD CONSTRAINT pk_proj_projekttypspr PRIMARY KEY (locale_c_nr, projekttyp_c_nr, mandant_c_nr);
ALTER TABLE ONLY public.proj_vkfortschritt
    ADD CONSTRAINT pk_proj_vkfortschritt PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rech_abrechnungsvorschlag
    ADD CONSTRAINT pk_rech_abrechnungsvorschlag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rech_gutschriftpositionsart
    ADD CONSTRAINT pk_rech_gutschriftpositionsart PRIMARY KEY (positionsart_c_nr);
ALTER TABLE ONLY public.rech_gutschriftsgrund
    ADD CONSTRAINT pk_rech_gutschriftsgrund PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rech_gutschriftsgrundspr
    ADD CONSTRAINT pk_rech_gutschriftsgrundspr PRIMARY KEY (gutschriftsgrund_i_id, locale_c_nr);
ALTER TABLE ONLY public.rech_gutschrifttext
    ADD CONSTRAINT pk_rech_gutschrifttext PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rech_lastschriftvorschlag
    ADD CONSTRAINT pk_rech_lastschriftvorschlag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rech_proformarechnungpositionsart
    ADD CONSTRAINT pk_rech_proformarechnungpositionsart PRIMARY KEY (positionsart_c_nr);
ALTER TABLE ONLY public.rech_rechnungkontierung
    ADD CONSTRAINT pk_rech_rechnungkontierung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rech_rechnungposition
    ADD CONSTRAINT pk_rech_rechnungposition PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rech_rechnungpositionsart
    ADD CONSTRAINT pk_rech_rechnungpositionsart PRIMARY KEY (positionsart_c_nr);
ALTER TABLE ONLY public.rech_rechnungtext
    ADD CONSTRAINT pk_rech_rechnungtext PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rech_rechnungartspr
    ADD CONSTRAINT pk_rech_rechungartspr PRIMARY KEY (rechnungart_c_nr, locale_c_nr);
ALTER TABLE ONLY public.rech_verrechnungsmodell
    ADD CONSTRAINT pk_rech_verrechnungsmodell PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rech_verrechnungsmodelltag
    ADD CONSTRAINT pk_rech_verrechnungsmodelltag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rech_vorkasseposition
    ADD CONSTRAINT pk_rech_vorkasseposition PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rech_rechnungzahlung
    ADD CONSTRAINT pk_rech_zahlung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rech_zahlungsartspr
    ADD CONSTRAINT pk_rech_zahlungsartspr PRIMARY KEY (zahlungsart_c_nr, locale_c_nr);
ALTER TABLE ONLY public.lp_rechtsform
    ADD CONSTRAINT pk_rechtsform PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rekla_aufnahmeart
    ADD CONSTRAINT pk_rekla_aufnahmeart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rekla_aufnahmeartspr
    ADD CONSTRAINT pk_rekla_aufnahmeartspr PRIMARY KEY (locale_c_nr, aufnahmeart_i_id);
ALTER TABLE ONLY public.rekla_behandlungspr
    ADD CONSTRAINT pk_rekla_behandlungspr PRIMARY KEY (locale_c_nr, behandlung_i_id);
ALTER TABLE ONLY public.rekla_behandlung
    ADD CONSTRAINT pk_rekla_beurteilung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rekla_fehler
    ADD CONSTRAINT pk_rekla_fehler PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rekla_fehlerangabe
    ADD CONSTRAINT pk_rekla_fehlerangabe PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rekla_fehlerangabespr
    ADD CONSTRAINT pk_rekla_fehlerangabespr PRIMARY KEY (locale_c_nr, fehlerangabe_i_id);
ALTER TABLE ONLY public.rekla_fehlerspr
    ADD CONSTRAINT pk_rekla_fehlerspr PRIMARY KEY (locale_c_nr, fehler_i_id);
ALTER TABLE ONLY public.rekla_massnahme
    ADD CONSTRAINT pk_rekla_massnahme PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rekla_massnahmespr
    ADD CONSTRAINT pk_rekla_massnahmespr PRIMARY KEY (locale_c_nr, massnahme_i_id);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT pk_rekla_reklamation PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rekla_reklamationart
    ADD CONSTRAINT pk_rekla_reklamationart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.rekla_reklamationbild
    ADD CONSTRAINT pk_rekla_reklamationbild PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rekla_schwere
    ADD CONSTRAINT pk_rekla_schwere PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rekla_schwerespr
    ADD CONSTRAINT pk_rekla_schwerespr PRIMARY KEY (locale_c_nr, schwere_i_id);
ALTER TABLE ONLY public.rekla_termintreue
    ADD CONSTRAINT pk_rekla_termintreue PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rekla_wirksamkeit
    ADD CONSTRAINT pk_rekla_wirksamkeit PRIMARY KEY (i_id);
ALTER TABLE ONLY public.rekla_wirksamkeitspr
    ADD CONSTRAINT pk_rekla_wirksamkeitspr PRIMARY KEY (locale_c_nr, wirksamkeit_i_id);
ALTER TABLE ONLY public.pers_religion
    ADD CONSTRAINT pk_religion PRIMARY KEY (i_id);
ALTER TABLE ONLY public.pers_religionspr
    ADD CONSTRAINT pk_religionspr PRIMARY KEY (locale_c_nr, religion_i_id);
ALTER TABLE ONLY public.fb_reversechargeartspr
    ADD CONSTRAINT pk_reversechargeartspr PRIMARY KEY (reversechargeart_i_id, locale_c_nr);
ALTER TABLE ONLY public.lp_locale
    ADD CONSTRAINT pk_sprache PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.lp_status
    ADD CONSTRAINT pk_status PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.lp_statusspr
    ADD CONSTRAINT pk_statusspr PRIMARY KEY (status_c_nr, locale_c_nr);
ALTER TABLE ONLY public.stk_alternativmaschine
    ADD CONSTRAINT pk_stk_alternativmaschine PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_apkommentar
    ADD CONSTRAINT pk_stk_apkommentar PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_apkommentarspr
    ADD CONSTRAINT pk_stk_apkommentarspr PRIMARY KEY (apkommentar_i_id, locale_c_nr);
ALTER TABLE ONLY public.stk_fertigungsgruppe
    ADD CONSTRAINT pk_stk_fertigungsgruppe PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_kommentarimport
    ADD CONSTRAINT pk_stk_kommentarimport PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_montageart
    ADD CONSTRAINT pk_stk_montageart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_posersatz
    ADD CONSTRAINT pk_stk_posersatz PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_profirstignore
    ADD CONSTRAINT pk_stk_profirstignore PRIMARY KEY (c_kbez);
ALTER TABLE ONLY public.stk_pruefart
    ADD CONSTRAINT pk_stk_pruefart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_pruefkombination
    ADD CONSTRAINT pk_stk_pruefkombination PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_pruefkombinationspr
    ADD CONSTRAINT pk_stk_pruefkombinationspr PRIMARY KEY (pruefkombination_i_id, locale_c_nr);
ALTER TABLE ONLY public.stk_stklagerentnahme
    ADD CONSTRAINT pk_stk_stklagerentnahme PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_stklparameter
    ADD CONSTRAINT pk_stk_stklparameter PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_stklparameterspr
    ADD CONSTRAINT pk_stk_stklparameterspr PRIMARY KEY (stklparameter_i_id, locale_c_nr);
ALTER TABLE ONLY public.stk_stklpruefplan
    ADD CONSTRAINT pk_stk_stklpruefplan PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_stueckliste
    ADD CONSTRAINT pk_stk_stueckliste PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_stuecklistearbeitsplan
    ADD CONSTRAINT pk_stk_stuecklistearbeitsplan PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_stuecklisteart
    ADD CONSTRAINT pk_stk_stuecklisteart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.stk_stuecklisteeigenschaftart
    ADD CONSTRAINT pk_stk_stuecklisteneigenschaft PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_stuecklisteeigenschaft
    ADD CONSTRAINT pk_stk_stuecklisteneigenschaft_1 PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_stuecklisteposition
    ADD CONSTRAINT pk_stk_stuecklisteposition PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_stuecklistescriptart
    ADD CONSTRAINT pk_stk_stuecklistescriptart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.stk_agart
    ADD CONSTRAINT pk_stkl_agart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.pers_tagesartspr
    ADD CONSTRAINT pk_tag1 PRIMARY KEY (locale_c_nr, tagesart_i_id);
ALTER TABLE ONLY public.fb_uvaartspr
    ADD CONSTRAINT pk_uvaartspr PRIMARY KEY (uvaart_i_id, locale_c_nr);
ALTER TABLE ONLY public.ww_vkpfartikelverkaufspreisbasis
    ADD CONSTRAINT pk_vkpf_artikeleinzelverkaufspreis PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_vkpfartikelpreis
    ADD CONSTRAINT pk_vkpf_preisliste PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_vkpfartikelpreisliste
    ADD CONSTRAINT pk_vkpf_preislistenname PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_alergen
    ADD CONSTRAINT pk_ww_alergen PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artgrumandant
    ADD CONSTRAINT pk_ww_artgrumandant PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikelalergen
    ADD CONSTRAINT pk_ww_artikelalergen PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikelart
    ADD CONSTRAINT pk_ww_artikelart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.ww_artikelbestellt
    ADD CONSTRAINT pk_ww_artikelbestellt PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikelkommentar
    ADD CONSTRAINT pk_ww_artikelkommentar PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikelkommentarart
    ADD CONSTRAINT pk_ww_artikelkommentarart PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikelkommentarartspr
    ADD CONSTRAINT pk_ww_artikelkommentarartspr PRIMARY KEY (artikelkommentarart_i_id, locale_c_nr);
ALTER TABLE ONLY public.ww_artikelkommentardruck
    ADD CONSTRAINT pk_ww_artikelkommentardruck PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikelkommentarspr
    ADD CONSTRAINT pk_ww_artikelkommentarspr PRIMARY KEY (artikelkommentar_i_id, locale_c_nr);
ALTER TABLE ONLY public.ww_artikellager
    ADD CONSTRAINT pk_ww_artikellager PRIMARY KEY (artikel_i_id, lager_i_id);
ALTER TABLE ONLY public.ww_artikellagerplaetze
    ADD CONSTRAINT pk_ww_artikellagerplaetze PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikellieferant
    ADD CONSTRAINT pk_ww_artikellieferant PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikellieferantstaffel
    ADD CONSTRAINT pk_ww_artikellieferantstaffel PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikellog
    ADD CONSTRAINT pk_ww_artikellog PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikelshopgruppe
    ADD CONSTRAINT pk_ww_artikelshopgruppe PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikelsnrchnr
    ADD CONSTRAINT pk_ww_artikelsnrchnr PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikelsperren
    ADD CONSTRAINT pk_ww_artikelsperren PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikelspr
    ADD CONSTRAINT pk_ww_artikelspr PRIMARY KEY (artikel_i_id, locale_c_nr);
ALTER TABLE ONLY public.ww_artikeltrutops
    ADD CONSTRAINT pk_ww_artikeltrutops PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikeltrutopsmetadaten
    ADD CONSTRAINT pk_ww_artikeltrutopsmetadaten PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_artikelreservierung
    ADD CONSTRAINT pk_ww_auftragreservierung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_automotive
    ADD CONSTRAINT pk_ww_automotive PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_dateiverweis
    ADD CONSTRAINT pk_ww_dateiverweis PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_einkaufsean
    ADD CONSTRAINT pk_ww_einkaufsean PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_ersatztypen
    ADD CONSTRAINT pk_ww_ersatztypen PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_farbcode
    ADD CONSTRAINT pk_ww_farbcode PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_fasession
    ADD CONSTRAINT pk_ww_fasession PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_fasessioneintrag
    ADD CONSTRAINT pk_ww_fasessioneintrag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_gebinde
    ADD CONSTRAINT pk_ww_gebinde PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_geometrie
    ADD CONSTRAINT pk_ww_geometrie PRIMARY KEY (artikel_i_id);
ALTER TABLE ONLY public.ww_geraetesnr
    ADD CONSTRAINT pk_ww_geraetesnr PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_handlagerbewegung
    ADD CONSTRAINT pk_ww_handlagerzugang PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_hersteller
    ADD CONSTRAINT pk_ww_hersteller PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_inventur
    ADD CONSTRAINT pk_ww_inventur PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_inventurliste
    ADD CONSTRAINT pk_ww_inventurliste PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_inventurprotokoll
    ADD CONSTRAINT pk_ww_inventurprotokoll PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_inventurstand
    ADD CONSTRAINT pk_ww_inventurstand PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_katalog
    ADD CONSTRAINT pk_ww_katalog PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_lager
    ADD CONSTRAINT pk_ww_lager PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_lagerart
    ADD CONSTRAINT pk_ww_lagerart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.ww_lagerartspr
    ADD CONSTRAINT pk_ww_lagerartspr PRIMARY KEY (lagerart_c_nr, locale_c_nr);
ALTER TABLE ONLY public.ww_lagerplatz
    ADD CONSTRAINT pk_ww_lagerplatz PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_lagerumbuchung
    ADD CONSTRAINT pk_ww_lagerumbuchung PRIMARY KEY (i_lagerbewegungidzubuchung, i_lagerbewegungidabbuchung);
ALTER TABLE ONLY public.ww_laseroberflaeche
    ADD CONSTRAINT pk_ww_laseroberflaeche PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_material
    ADD CONSTRAINT pk_ww_material PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_materialpreis
    ADD CONSTRAINT pk_ww_materialpreis PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_materialspr
    ADD CONSTRAINT pk_ww_materialspr PRIMARY KEY (locale_c_nr, material_i_id);
ALTER TABLE ONLY public.ww_materialzuschlag
    ADD CONSTRAINT pk_ww_materialzuschlag PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_medical
    ADD CONSTRAINT pk_ww_medical PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_montage
    ADD CONSTRAINT pk_ww_montage PRIMARY KEY (artikel_i_id);
ALTER TABLE ONLY public.ww_paternoster
    ADD CONSTRAINT pk_ww_paternoster PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_paternostereigenschaft
    ADD CONSTRAINT pk_ww_paternostereigenschaft PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_rahmenbedarfe
    ADD CONSTRAINT pk_ww_rahmenbedarfe PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_reach
    ADD CONSTRAINT pk_ww_reach PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_rohs
    ADD CONSTRAINT pk_ww_rohs PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_shopgruppe
    ADD CONSTRAINT pk_ww_shopgruppe PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_shopgruppewebshop
    ADD CONSTRAINT pk_ww_shopgruppewebshop PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_sollverkauf
    ADD CONSTRAINT pk_ww_sollverkauf PRIMARY KEY (artikel_i_id);
ALTER TABLE ONLY public.ww_sperren
    ADD CONSTRAINT pk_ww_sperren PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_trumphtopslog
    ADD CONSTRAINT pk_ww_trumphtopslog PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_verleih
    ADD CONSTRAINT pk_ww_verleih PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_verpackung
    ADD CONSTRAINT pk_ww_verpackung PRIMARY KEY (artikel_i_id);
ALTER TABLE ONLY public.ww_verpackungsmittel
    ADD CONSTRAINT pk_ww_verpackungsmittel PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_verpackungsmittelspr
    ADD CONSTRAINT pk_ww_verpackungsmittelspr PRIMARY KEY (locale_c_nr, verpackungsmittel_i_id);
ALTER TABLE ONLY public.ww_verschleissteil
    ADD CONSTRAINT pk_ww_verschleissteil PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_verschleissteilwerkzeug
    ADD CONSTRAINT pk_ww_verschleissteilwerkzeug PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_vkpfmengenstaffel
    ADD CONSTRAINT pk_ww_vkpfmengenstaffel PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_vorschlagstext
    ADD CONSTRAINT pk_ww_vorschlagstext PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_vorzug
    ADD CONSTRAINT pk_ww_vorzug PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_waffenausfuehrung
    ADD CONSTRAINT pk_ww_waffenausfuehrung PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_waffenkaliber
    ADD CONSTRAINT pk_ww_waffenkaliber PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_waffenkategorie
    ADD CONSTRAINT pk_ww_waffenkategorie PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_waffentyp
    ADD CONSTRAINT pk_ww_waffentyp PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_waffentyp_fein
    ADD CONSTRAINT pk_ww_waffentyp_fein PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_waffenzusatz
    ADD CONSTRAINT pk_ww_waffenzusatz PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_webshop
    ADD CONSTRAINT pk_ww_webshop PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_webshopart
    ADD CONSTRAINT pk_ww_webshopart PRIMARY KEY (c_nr);
ALTER TABLE ONLY public.ww_webshopartikel
    ADD CONSTRAINT pk_ww_webshopartikel PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_webshopartikelpreisliste
    ADD CONSTRAINT pk_ww_webshopartikelpreisliste PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_webshopartspr
    ADD CONSTRAINT pk_ww_webshopartspr PRIMARY KEY (webshopart_c_nr, locale_c_nr);
ALTER TABLE ONLY public.ww_webshopkunde
    ADD CONSTRAINT pk_ww_webshopkunde PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_webshopmwstsatzbez
    ADD CONSTRAINT pk_ww_webshopmwstsatzbez PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_webshopshopgruppe
    ADD CONSTRAINT pk_ww_webshopshopgruppe PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_werkzeug
    ADD CONSTRAINT pk_ww_werkzeug PRIMARY KEY (i_id);
ALTER TABLE ONLY public.ww_zugehoerige
    ADD CONSTRAINT pk_ww_zugehoerige PRIMARY KEY (i_id);
ALTER TABLE ONLY public.anf_anfrage
    ADD CONSTRAINT uk_anf_anfrage UNIQUE (mandant_c_nr, c_nr);
ALTER TABLE ONLY public.anf_anfragestatus
    ADD CONSTRAINT uk_anf_anfragestatus_i_sort UNIQUE (i_sort);
ALTER TABLE ONLY public.anf_anfragetext
    ADD CONSTRAINT uk_anf_anfragetext UNIQUE (mandant_c_nr, locale_c_nr, mediaart_c_nr);
ALTER TABLE ONLY public.anf_zertifikatart
    ADD CONSTRAINT uk_anf_zertifikatart UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.angb_akquisestatus
    ADD CONSTRAINT uk_angb_akquisestatus UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.angb_angebot
    ADD CONSTRAINT uk_angb_angebot_c_nr_mandant_c_nr UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.angb_angeboteinheit
    ADD CONSTRAINT uk_angb_angeboteinheit_i_sort UNIQUE (i_sort);
ALTER TABLE ONLY public.angb_angebotstatus
    ADD CONSTRAINT uk_angb_angebotstatus_i_sort UNIQUE (status_c_nr);
ALTER TABLE ONLY public.angb_angebottext
    ADD CONSTRAINT uk_angb_angebottext UNIQUE (mandant_c_nr, locale_c_nr, mediaart_c_nr);
ALTER TABLE ONLY public.as_agstklaufschlag
    ADD CONSTRAINT uk_as_agstklaufschlag UNIQUE (agstkl_i_id, aufschlag_i_id);
ALTER TABLE ONLY public.as_aufschlag
    ADD CONSTRAINT uk_as_aufschlag UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.as_einkaufsangebot
    ADD CONSTRAINT uk_as_einkaufsangebot UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.as_ekaglieferant
    ADD CONSTRAINT uk_as_ekaglieferant UNIQUE (einkaufsangebot_i_id, lieferant_i_id);
ALTER TABLE ONLY public.as_ekgruppe
    ADD CONSTRAINT uk_as_ekgruppe UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.as_ekgruppelieferant
    ADD CONSTRAINT uk_as_ekgruppelieferant UNIQUE (ekgruppe_i_id, lieferant_i_id);
ALTER TABLE ONLY public.as_positionlieferant
    ADD CONSTRAINT uk_as_positionlieferant UNIQUE (einkaufsangebotposition_i_id, ekaglieferant_i_id);
ALTER TABLE ONLY public.as_webabfrage
    ADD CONSTRAINT uk_as_webabfrage UNIQUE (i_typ);
ALTER TABLE ONLY public.as_webfindchips
    ADD CONSTRAINT uk_as_webfindchips UNIQUE (c_distributor);
ALTER TABLE ONLY public.as_weblieferant
    ADD CONSTRAINT uk_as_weblieferant UNIQUE (webpartner_i_id);
ALTER TABLE ONLY public.auft_auftrag
    ADD CONSTRAINT uk_auft_auftrag UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.auft_auftragauftragdokument
    ADD CONSTRAINT uk_auft_auftragauftragdokument UNIQUE (auftrag_i_id, auftragdokument_i_id);
ALTER TABLE ONLY public.auft_auftragbegruendung
    ADD CONSTRAINT uk_auft_auftragbegruendung UNIQUE (c_nr);
ALTER TABLE ONLY public.auft_auftragdokument
    ADD CONSTRAINT uk_auft_auftragdokument UNIQUE (c_nr);
ALTER TABLE ONLY public.auft_auftragkostenstelle
    ADD CONSTRAINT uk_auft_auftragkostenstelle UNIQUE (auftrag_i_id, kostenstelle_i_id);
ALTER TABLE ONLY public.auft_auftragpositionstatus
    ADD CONSTRAINT uk_auft_auftragpositionstatus_i_sort UNIQUE (i_sort);
ALTER TABLE ONLY public.auft_auftragseriennrn
    ADD CONSTRAINT uk_auft_auftragseriennrn UNIQUE (artikel_i_id, c_seriennr, version_nr);
ALTER TABLE ONLY public.auft_auftragstatus
    ADD CONSTRAINT uk_auft_auftragstatus_i_sort UNIQUE (i_sort);
ALTER TABLE ONLY public.auft_auftragtext
    ADD CONSTRAINT uk_auft_auftragtext UNIQUE (mandant_c_nr, locale_c_nr, mediaart_c_nr);
ALTER TABLE ONLY public.auft_meilenstein
    ADD CONSTRAINT uk_auft_meilenstein UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.auft_verrechenbar
    ADD CONSTRAINT uk_auft_verrechenbar UNIQUE (c_bez);
ALTER TABLE ONLY public.auft_zahlungsplanmeilenstein
    ADD CONSTRAINT uk_auft_zahlungsplanmeilenstein UNIQUE (zahlungsplan_i_id, meilenstein_i_id);
ALTER TABLE ONLY public.auft_zeitplantyp
    ADD CONSTRAINT uk_auft_zeitplantyp UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.bes_bestellung
    ADD CONSTRAINT uk_bes_bestellung UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.bes_bestellungtext
    ADD CONSTRAINT uk_bes_bestellungtext UNIQUE (mandant_c_nr, locale_c_nr, c_nr);
ALTER TABLE ONLY public.bes_bsmahntext
    ADD CONSTRAINT uk_bes_bsmahntext UNIQUE (mandant_c_nr, locale_c_nr, bsmahnstufe_i_id);
ALTER TABLE ONLY public.bes_wareneingangsposition
    ADD CONSTRAINT uk_bes_wareneingangsposition UNIQUE (wareneingang_i_id, bestellposition_i_id);
ALTER TABLE ONLY public.er_eingangsrechnung
    ADD CONSTRAINT uk_er_eingangsrechnung UNIQUE (c_nr, mandant_c_nr, eingangsrechnungart_c_nr);
ALTER TABLE ONLY public.er_eingangsrechnungtext
    ADD CONSTRAINT uk_er_eingangsrechnungtext UNIQUE (mandant_c_nr, locale_c_nr, c_nr);
ALTER TABLE ONLY public.fb_bankverbindung
    ADD CONSTRAINT uk_fb_bankverbindung_2 UNIQUE (bank_i_id, c_kontonummer, mandant_c_nr);
ALTER TABLE ONLY public.fb_bankverbindung
    ADD CONSTRAINT uk_fb_bankverbindung_konto UNIQUE (konto_i_id);
ALTER TABLE ONLY public.fb_belegbuchung
    ADD CONSTRAINT uk_fb_belegbuchung UNIQUE (belegart_c_nr, i_belegiid, buchung_i_id);
ALTER TABLE ONLY public.fb_belegbuchung
    ADD CONSTRAINT uk_fb_belegbuchung_1 UNIQUE (buchung_i_id);
ALTER TABLE ONLY public.fb_konto
    ADD CONSTRAINT uk_fb_konto_c_nr_kontotyp_c_nr UNIQUE (c_nr, kontotyp_c_nr, mandant_c_nr);
ALTER TABLE ONLY public.fb_kontolaenderart
    ADD CONSTRAINT uk_fb_kontolaenderart UNIQUE (konto_i_id, laenderart_c_nr, finanzamt_i_id, reversechargeart_i_id, mandant_c_nr, t_gueltigab);
ALTER TABLE ONLY public.fb_kontoland
    ADD CONSTRAINT uk_fb_kontoland UNIQUE (konto_i_id, land_i_id, t_gueltigab);
ALTER TABLE ONLY public.fb_mahnspesen
    ADD CONSTRAINT uk_fb_mahnspesen UNIQUE (mahnstufe_i_id, waehrung_c_nr, mandant_c_nr);
ALTER TABLE ONLY public.fb_mahntext
    ADD CONSTRAINT uk_fb_mahntext UNIQUE (mandant_c_nr, locale_c_nr, mahnstufe_i_id);
ALTER TABLE ONLY public.fb_mahnung
    ADD CONSTRAINT uk_fb_mahnung UNIQUE (rechnung_i_id, mahnstufe_i_id);
ALTER TABLE ONLY public.fb_reversechargeart
    ADD CONSTRAINT uk_fb_reversechargeart UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.fb_steuerkategorie
    ADD CONSTRAINT uk_fb_steuerkategorie UNIQUE (c_nr, mandant_c_nr, finanzamt_i_id, reversechargeart_i_id);
ALTER TABLE ONLY public.fb_steuerkategoriekonto
    ADD CONSTRAINT uk_fb_steuerkategoriekonto UNIQUE (steuerkategorie_i_id, mwstsatzbez_i_id, t_gueltigab);
ALTER TABLE ONLY public.fb_uvaformular
    ADD CONSTRAINT uk_fb_uvaformular UNIQUE (mandant_c_nr, finanzamt_i_id, uvaart_i_id);
ALTER TABLE ONLY public.fb_uvaverprobung
    ADD CONSTRAINT uk_fb_uvaverprobung UNIQUE (geschaeftsjahr_i_geschaeftsjahr, i_monat, mandant_c_nr, finanzamt_i_id);
ALTER TABLE ONLY public.fb_uvaart
    ADD CONSTRAINT uk_fb_uvvart1 UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.fc_fclieferadresse
    ADD CONSTRAINT uk_fc_fclieferadresse UNIQUE (forecast_i_id, kunde_i_id_lieferadresse);
ALTER TABLE ONLY public.fc_kommdrucker
    ADD CONSTRAINT uk_fc_kommdrucker UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.fert_los
    ADD CONSTRAINT uk_fert_los UNIQUE (mandant_c_nr, c_nr);
ALTER TABLE ONLY public.fert_loslagerentnahme
    ADD CONSTRAINT uk_fert_loslagerentnahme UNIQUE (los_i_id, lager_i_id);
ALTER TABLE ONLY public.fert_loslagerentnahme
    ADD CONSTRAINT uk_fert_loslagerentnahme_1 UNIQUE (los_i_id, i_sort);
ALTER TABLE ONLY public.fert_loslosklasse
    ADD CONSTRAINT uk_fert_loslosklasse UNIQUE (los_i_id, losklasse_i_id);
ALTER TABLE ONLY public.fert_lostechniker
    ADD CONSTRAINT uk_fert_lostechniker UNIQUE (los_i_id, personal_i_id);
ALTER TABLE ONLY public.fert_loszusatzstatus
    ADD CONSTRAINT uk_fert_loszusatzstatus UNIQUE (los_i_id, zusatzstatus_i_id);
ALTER TABLE ONLY public.fert_pruefergebins
    ADD CONSTRAINT uk_fert_pruefergebins UNIQUE (losablieferung_i_id, lospruefplan_i_id);
ALTER TABLE ONLY public.fert_zusatzstatus
    ADD CONSTRAINT uk_fert_zusatzstatus UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.is_anlage
    ADD CONSTRAINT uk_is_anlage UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.is_geraetetyp
    ADD CONSTRAINT uk_is_geraetetyp UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.is_gewerk
    ADD CONSTRAINT uk_is_gewerk UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.is_halle
    ADD CONSTRAINT uk_is_halle UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.is_instandhaltung
    ADD CONSTRAINT uk_is_instandhaltung UNIQUE (kunde_i_id);
ALTER TABLE ONLY public.is_ismaschine
    ADD CONSTRAINT uk_is_ismaschine UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.is_kategorie
    ADD CONSTRAINT uk_is_kategorie UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.is_standort
    ADD CONSTRAINT uk_is_standort UNIQUE (instandhaltung_i_id, partner_i_id);
ALTER TABLE ONLY public.is_standorttechniker
    ADD CONSTRAINT uk_is_standorttechniker UNIQUE (standort_i_id, personal_i_id);
ALTER TABLE ONLY public.iv_inserat
    ADD CONSTRAINT uk_iv_inserat UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.iv_inserater
    ADD CONSTRAINT uk_iv_inserater UNIQUE (inserat_i_id, eingangsrechnung_i_id);
ALTER TABLE ONLY public.iv_inseratrechnung
    ADD CONSTRAINT uk_iv_inseratrechnung UNIQUE (inserat_i_id, kunde_i_id);
ALTER TABLE ONLY public.lp_kostenstelle
    ADD CONSTRAINT uk_kostenstelle_c_nr UNIQUE (mandant_c_nr, c_nr);
ALTER TABLE ONLY public.lp_kostenstelle
    ADD CONSTRAINT uk_kostenstelle_i_id UNIQUE (i_id, mandant_c_nr);
ALTER TABLE ONLY public.kue_bedienerlager
    ADD CONSTRAINT uk_kue_bedienerlager UNIQUE (c_bedienernummer);
ALTER TABLE ONLY public.kue_kassaartikel
    ADD CONSTRAINT uk_kue_kassaartikel UNIQUE (c_artikelnummerkassa);
ALTER TABLE ONLY public.kue_kuecheumrechnung
    ADD CONSTRAINT uk_kue_kuecheumrechnung UNIQUE (c_artikelnummerkassa);
ALTER TABLE ONLY public.kue_speiseplan
    ADD CONSTRAINT uk_kue_speiseplan UNIQUE (mandant_c_nr, kassaartikel_i_id, t_datum);
ALTER TABLE ONLY public.kue_tageslos
    ADD CONSTRAINT uk_kue_tageslos UNIQUE (lager_i_id_abbuchung, b_sofortverbrauch);
ALTER TABLE ONLY public.lp_lieferart
    ADD CONSTRAINT uk_lieferart_i_id_2 UNIQUE (i_id, mandant_c_nr);
ALTER TABLE ONLY public.lp_arbeitsplatz
    ADD CONSTRAINT uk_lp_arbeitsplatz UNIQUE (c_pcname);
ALTER TABLE ONLY public.lp_arbeitsplatzparameter
    ADD CONSTRAINT uk_lp_arbeitsplatzparameter UNIQUE (arbeitsplatz_i_id, parameter_c_nr);
ALTER TABLE ONLY public.lp_dokumentenlinkbeleg
    ADD CONSTRAINT uk_lp_dokumentenlinkbeleg UNIQUE (i_id_belegart, dokumentenlink_i_id);
ALTER TABLE ONLY public.lp_einheitkonvertierung
    ADD CONSTRAINT uk_lp_einheitkonvertierung UNIQUE (einheit_c_nr_von, einheit_c_nr_zu);
ALTER TABLE ONLY public.lp_extraliste
    ADD CONSTRAINT uk_lp_extraliste UNIQUE (belegart_c_nr, c_bez);
ALTER TABLE ONLY public.lp_geschaeftsjahrmandant
    ADD CONSTRAINT uk_lp_geschaeftsjahrmandant UNIQUE (i_geschaeftsjahr, mandant_c_nr);
ALTER TABLE ONLY public.lp_hardwareartspr
    ADD CONSTRAINT uk_lp_hardwareartspr_c_bez8 UNIQUE (locale_c_nr, c_bez);
ALTER TABLE ONLY public.lp_kennung
    ADD CONSTRAINT uk_lp_kennung UNIQUE (c_nr);
ALTER TABLE ONLY public.lp_kostentraeger
    ADD CONSTRAINT uk_lp_kostentraeger UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.lp_land
    ADD CONSTRAINT uk_lp_land UNIQUE (c_lkz);
ALTER TABLE ONLY public.lp_landplzort
    ADD CONSTRAINT uk_lp_landplzort UNIQUE (land_i_id, c_plz, ort_i_id);
ALTER TABLE ONLY public.lp_lieferart
    ADD CONSTRAINT uk_lp_lieferart2 UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.lp_mandantagbspr
    ADD CONSTRAINT uk_lp_mandantagbspr UNIQUE (mandant_c_nr, locale_c_nr);
ALTER TABLE ONLY public.lp_mediastandard
    ADD CONSTRAINT uk_lp_mediastandard_c_nr_mandant_c_nr_locale_c_nr UNIQUE (c_nr, mandant_c_nr, locale_c_nr);
ALTER TABLE ONLY public.lp_modulberechtigung
    ADD CONSTRAINT uk_lp_modulberechtigung UNIQUE (belegart_c_nr, mandant_c_nr);
ALTER TABLE ONLY public.lp_mwstsatzbez
    ADD CONSTRAINT uk_lp_mwstsatzbez_2 UNIQUE (mandant_c_nr, c_bezeichnung);
ALTER TABLE ONLY public.lp_mwstsatzcode
    ADD CONSTRAINT uk_lp_mwstsatzcode UNIQUE (mwstsatz_i_id, reversechargeart_i_id);
ALTER TABLE ONLY public.lp_ort
    ADD CONSTRAINT uk_lp_ort_c_name UNIQUE (c_name);
ALTER TABLE ONLY public.lp_panelbeschreibung
    ADD CONSTRAINT uk_lp_panelbeschreibung UNIQUE (c_name, panel_c_nr, mandant_c_nr);
ALTER TABLE ONLY public.lp_paneldaten
    ADD CONSTRAINT uk_lp_paneldaten UNIQUE (panel_c_nr, panelbeschreibung_i_id, c_key);
ALTER TABLE ONLY public.lp_reportvariante
    ADD CONSTRAINT uk_lp_reportvariante UNIQUE (c_reportname, c_reportnamevariante);
ALTER TABLE ONLY public.lp_spediteur
    ADD CONSTRAINT uk_lp_spediteur UNIQUE (mandant_c_nr, c_namedesspediteurs);
ALTER TABLE ONLY public.lp_standarddrucker
    ADD CONSTRAINT uk_lp_standarddrucker UNIQUE (c_pc, c_reportname, mandant_c_nr, reportvariante_i_id);
ALTER TABLE ONLY public.lp_thejudge
    ADD CONSTRAINT uk_lp_thejudge UNIQUE (c_wer, c_was);
ALTER TABLE ONLY public.lp_versandweg
    ADD CONSTRAINT uk_lp_versandweg UNIQUE (c_nr);
ALTER TABLE ONLY public.lp_versandwegccpartner
    ADD CONSTRAINT uk_lp_versandwegccpartner UNIQUE (partner_i_id, versandweg_i_id);
ALTER TABLE ONLY public.lp_versandwegpartner
    ADD CONSTRAINT uk_lp_versandwegpartner UNIQUE (partner_i_id, versandweg_i_id, mandant_c_nr);
ALTER TABLE ONLY public.lp_zahlungsziel
    ADD CONSTRAINT uk_lp_zahlungsziel UNIQUE (mandant_c_nr, c_bez);
ALTER TABLE ONLY public.ls_begruendung
    ADD CONSTRAINT uk_ls_begruendung UNIQUE (c_nr);
ALTER TABLE ONLY public.ls_lieferschein
    ADD CONSTRAINT uk_ls_lieferschein UNIQUE (mandant_c_nr, c_nr);
ALTER TABLE ONLY public.ls_lieferscheinstatus
    ADD CONSTRAINT uk_ls_lieferscheinstatus UNIQUE (i_sort);
ALTER TABLE ONLY public.ls_lieferscheintext
    ADD CONSTRAINT uk_ls_lieferscheintext UNIQUE (mandant_c_nr, locale_c_nr, mediaart_c_nr);
ALTER TABLE ONLY public.ls_packstueck
    ADD CONSTRAINT uk_ls_packstueck UNIQUE (l_nummer);
ALTER TABLE ONLY public.media_emailattachment
    ADD CONSTRAINT uk_media_emailattachment UNIQUE (c_uuid);
ALTER TABLE ONLY public.media_inbox
    ADD CONSTRAINT uk_media_inbox UNIQUE (media_i_id, personal_i_id);
ALTER TABLE ONLY public.media_store
    ADD CONSTRAINT uk_media_store UNIQUE (c_uuid);
ALTER TABLE ONLY public.media_storebeleg
    ADD CONSTRAINT uk_media_storebeleg UNIQUE (mandant_c_nr, media_i_id, c_belegartnr, beleg_i_id);
ALTER TABLE ONLY public.lp_mwstsatzbez
    ADD CONSTRAINT uk_mwstsatzbez UNIQUE (i_id, mandant_c_nr);
ALTER TABLE ONLY public.part_ansprechpartner
    ADD CONSTRAINT uk_part_ansprechpartner UNIQUE (partner_i_id, partner_i_id_ansprechpartner, ansprechpartnerfunktion_i_id, t_gueltigab);
ALTER TABLE ONLY public.part_ansprechpartneradressbuch
    ADD CONSTRAINT uk_part_ansprechpartneradressbuch UNIQUE (c_emailadresse, ansprechpartner_i_id);
ALTER TABLE ONLY public.part_ansprechpartnerfunktion
    ADD CONSTRAINT uk_part_ansprechpartnerfunktion UNIQUE (c_nr);
ALTER TABLE ONLY public.part_branche
    ADD CONSTRAINT uk_part_branche UNIQUE (c_nr);
ALTER TABLE ONLY public.part_dsgvokategorie
    ADD CONSTRAINT uk_part_dsgvokategorie UNIQUE (c_nr);
ALTER TABLE ONLY public.part_geodaten
    ADD CONSTRAINT uk_part_geodaten UNIQUE (partner_i_id);
ALTER TABLE ONLY public.part_identifikation
    ADD CONSTRAINT uk_part_identifikation UNIQUE (c_nr);
ALTER TABLE ONLY public.part_kontaktart
    ADD CONSTRAINT uk_part_kontaktart UNIQUE (c_bez);
ALTER TABLE ONLY public.part_kunde
    ADD CONSTRAINT uk_part_kunde_partner_mandant UNIQUE (partner_i_id, mandant_c_nr);
ALTER TABLE ONLY public.part_kundematerial
    ADD CONSTRAINT uk_part_kundematerial UNIQUE (kunde_i_id, material_i_id);
ALTER TABLE ONLY public.part_kundesachbearbeiter
    ADD CONSTRAINT uk_part_kundesachbearbeiter UNIQUE (personal_i_id, t_gueltigab, kunde_i_id);
ALTER TABLE ONLY public.part_kundesokomengenstaffel
    ADD CONSTRAINT uk_part_kundesokomengenstaffel UNIQUE (kundesoko_i_id, n_menge);
ALTER TABLE ONLY public.part_kundespediteur
    ADD CONSTRAINT uk_part_kundespediteur UNIQUE (kunde_i_id, spediteur_i_id);
ALTER TABLE ONLY public.part_lfliefergruppe
    ADD CONSTRAINT uk_part_lfliefergruppe UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.part_lieferant
    ADD CONSTRAINT uk_part_lieferant UNIQUE (partner_i_id, mandant_c_nr);
ALTER TABLE ONLY public.part_lieferantbeurteilung
    ADD CONSTRAINT uk_part_lieferantbeurteilung UNIQUE (lieferant_i_id, t_datum);
ALTER TABLE ONLY public.part_liefermengen
    ADD CONSTRAINT uk_part_liefermengen UNIQUE (artikel_i_id, kunde_i_id_lieferadresse, t_datum);
ALTER TABLE ONLY public.part_newslettergrund
    ADD CONSTRAINT uk_part_newslettergrund UNIQUE (c_bez);
ALTER TABLE ONLY public.part_partneradressbuch
    ADD CONSTRAINT uk_part_partneradressbuch UNIQUE (c_emailadresse, partner_i_id);
ALTER TABLE ONLY public.part_partnerklasse
    ADD CONSTRAINT uk_part_partnerklasse UNIQUE (c_nr);
ALTER TABLE ONLY public.part_partnerkommentarart
    ADD CONSTRAINT uk_part_partnerkommentarart UNIQUE (c_bez);
ALTER TABLE ONLY public.part_partnerkommunikation
    ADD CONSTRAINT uk_part_partnerkommunikation UNIQUE (partner_i_id, kommunikationsart_c_nr, c_bez, c_inhalt);
ALTER TABLE ONLY public.part_paselektion
    ADD CONSTRAINT uk_part_paselektion UNIQUE (partner_i_id, selektion_i_id);
ALTER TABLE ONLY public.part_selektion
    ADD CONSTRAINT uk_part_selektion UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.pers_abwesenheitsart
    ADD CONSTRAINT uk_pers_abwesenheitsart UNIQUE (c_nr);
ALTER TABLE ONLY public.pers_angehoerigenartspr
    ADD CONSTRAINT uk_pers_angehoerigenartspr_c_bez27 UNIQUE (locale_c_nr, c_bez);
ALTER TABLE ONLY public.pers_artgrurolle
    ADD CONSTRAINT uk_pers_artgrurolle UNIQUE (artgru_i_id, systemrolle_i_id);
ALTER TABLE ONLY public.pers_artikelzulage
    ADD CONSTRAINT uk_pers_artikelzulage UNIQUE (artikel_i_id, zulage_i_id, t_gueltigab);
ALTER TABLE ONLY public.pers_artikelzuschlag
    ADD CONSTRAINT uk_pers_artikelzuschlag UNIQUE (artikel_i_id, t_gueltigab);
ALTER TABLE ONLY public.pers_benutzer
    ADD CONSTRAINT uk_pers_benutzer UNIQUE (c_benutzerkennung);
ALTER TABLE ONLY public.pers_benutzermandantsystemrolle
    ADD CONSTRAINT uk_pers_benutzermandantsystemrolle UNIQUE (benutzer_i_id, mandant_c_nr);
ALTER TABLE ONLY public.pers_bereitschaftart
    ADD CONSTRAINT uk_pers_bereitschaftart UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.pers_bereitschafttag
    ADD CONSTRAINT uk_pers_bereitschafttag UNIQUE (bereitschaftart_i_id, tagesart_i_id, u_beginn);
ALTER TABLE ONLY public.pers_betriebskalender
    ADD CONSTRAINT uk_pers_betriebskalender UNIQUE (t_datum, mandant_c_nr, religion_i_id);
ALTER TABLE ONLY public.pers_diaeten
    ADD CONSTRAINT uk_pers_diaeten UNIQUE (c_bez);
ALTER TABLE ONLY public.pers_diaetentagessatz
    ADD CONSTRAINT uk_pers_diaetentagessatz UNIQUE (diaeten_i_id, t_gueltigab);
ALTER TABLE ONLY public.pers_eintrittaustritt
    ADD CONSTRAINT uk_pers_eintrittaustritt UNIQUE (personal_i_id, t_eintritt);
ALTER TABLE ONLY public.pers_fahrzeug
    ADD CONSTRAINT uk_pers_fahrzeug UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.pers_fahrzeugkosten
    ADD CONSTRAINT uk_pers_fahrzeugkosten UNIQUE (fahrzeug_i_id, t_gueltigab);
ALTER TABLE ONLY public.pers_familienstandspr
    ADD CONSTRAINT uk_pers_familienstandspr_c_bez26 UNIQUE (locale_c_nr, c_bez);
ALTER TABLE ONLY public.pers_fertigungsgrupperolle
    ADD CONSTRAINT uk_pers_fertigungsgrupperolle UNIQUE (fertigungsgruppe_i_id, systemrolle_i_id);
ALTER TABLE ONLY public.pers_fingerart
    ADD CONSTRAINT uk_pers_fingerart UNIQUE (c_bez);
ALTER TABLE ONLY public.pers_gleitzeitsaldo
    ADD CONSTRAINT uk_pers_gleitzeitsaldo UNIQUE (personal_i_id, i_jahr, i_monat);
ALTER TABLE ONLY public.pers_hvmabenutzer
    ADD CONSTRAINT uk_pers_hvmabenutzer UNIQUE (hvmalizenz_i_id, benutzer_i_id);
ALTER TABLE ONLY public.pers_hvmabenutzerparameter
    ADD CONSTRAINT uk_pers_hvmabenutzerparameter UNIQUE (hvmabenutzer_i_id, c_nr, c_kategorie);
ALTER TABLE ONLY public.pers_hvmalizenz
    ADD CONSTRAINT uk_pers_hvmalizenz UNIQUE (c_nr);
ALTER TABLE ONLY public.pers_hvmarecht
    ADD CONSTRAINT uk_pers_hvmarecht UNIQUE (c_nr, hvmalizenz_i_id);
ALTER TABLE ONLY public.pers_hvmarolle
    ADD CONSTRAINT uk_pers_hvmarolle UNIQUE (systemrolle_i_id, hvmarecht_i_id);
ALTER TABLE ONLY public.pers_kollektivuestd
    ADD CONSTRAINT uk_pers_kollektivuestd UNIQUE (kollektiv_i_id, tagesart_i_id);
ALTER TABLE ONLY public.pers_kollektivuestd50
    ADD CONSTRAINT uk_pers_kollektivuestd50 UNIQUE (kollektiv_i_id, tagesart_i_id);
ALTER TABLE ONLY public.pers_lagerrolle
    ADD CONSTRAINT uk_pers_lagerrolle UNIQUE (lager_i_id, systemrolle_i_id);
ALTER TABLE ONLY public.pers_lohnart
    ADD CONSTRAINT uk_pers_lohnart UNIQUE (i_lohnart);
ALTER TABLE ONLY public.pers_lohnartstundenfaktor
    ADD CONSTRAINT uk_pers_lohnartstundenfaktor UNIQUE (lohnart_i_id, lohnstundenart_c_nr, tagesart_i_id);
ALTER TABLE ONLY public.pers_maschine
    ADD CONSTRAINT uk_pers_maschine UNIQUE (mandant_c_nr, c_inventarnummer);
ALTER TABLE ONLY public.pers_maschineleistungsfaktor
    ADD CONSTRAINT uk_pers_maschineleistungsfaktor UNIQUE (maschine_i_id, material_i_id, t_gueltigab);
ALTER TABLE ONLY public.pers_maschinengruppe
    ADD CONSTRAINT uk_pers_maschinengruppe UNIQUE (c_kbez, mandant_c_nr);
ALTER TABLE ONLY public.pers_maschinenkosten
    ADD CONSTRAINT uk_pers_maschinenkosten UNIQUE (maschine_i_id, t_gueltigab);
ALTER TABLE ONLY public.pers_nachrichtart
    ADD CONSTRAINT uk_pers_nachrichtart UNIQUE (c_nr);
ALTER TABLE ONLY public.pers_nachrichtenart
    ADD CONSTRAINT uk_pers_nachrichtenart UNIQUE (c_nr);
ALTER TABLE ONLY public.pers_nachrichtengruppe
    ADD CONSTRAINT uk_pers_nachrichtengruppe UNIQUE (c_bez);
ALTER TABLE ONLY public.pers_passivereise
    ADD CONSTRAINT uk_pers_passivereise UNIQUE (kollektiv_i_id, tagesart_i_id);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT uk_pers_personal UNIQUE (c_personalnr, mandant_c_nr);
ALTER TABLE ONLY public.pers_personal
    ADD CONSTRAINT uk_pers_personal_neu UNIQUE (partner_i_id, mandant_c_nr);
ALTER TABLE ONLY public.pers_personalfahrzeug
    ADD CONSTRAINT uk_pers_personalfahrzeug UNIQUE (personal_i_id, fahrzeug_i_id);
ALTER TABLE ONLY public.pers_personalfinger
    ADD CONSTRAINT uk_pers_personalfinger UNIQUE (i_fingerid);
ALTER TABLE ONLY public.pers_personalfinger
    ADD CONSTRAINT uk_pers_personalfinger_2 UNIQUE (personal_i_id, fingerart_i_id);
ALTER TABLE ONLY public.pers_personalgruppe
    ADD CONSTRAINT uk_pers_personalgruppe UNIQUE (c_bez);
ALTER TABLE ONLY public.pers_personalterminal
    ADD CONSTRAINT uk_pers_personalterminal UNIQUE (personal_i_id, arbeitsplatz_i_id);
ALTER TABLE ONLY public.pers_personalverfuegbarkeit
    ADD CONSTRAINT uk_pers_personalverfuegbarkeit UNIQUE (personal_i_id, artikel_i_id);
ALTER TABLE ONLY public.pers_personalzeitmodell
    ADD CONSTRAINT uk_pers_personalzeitmodell UNIQUE (personal_i_id, t_gueltigab);
ALTER TABLE ONLY public.pers_personalzutrittsklasse
    ADD CONSTRAINT uk_pers_personalzutrittsklasse UNIQUE (personal_i_id, zutrittsklasse_i_id, t_gueltigab);
ALTER TABLE ONLY public.pers_reise
    ADD CONSTRAINT uk_pers_reise UNIQUE (personal_i_id, t_zeit);
ALTER TABLE ONLY public.pers_reisespesen
    ADD CONSTRAINT uk_pers_reisespesen UNIQUE (reise_i_id, eingangsrechnung_i_id);
ALTER TABLE ONLY public.pers_religion
    ADD CONSTRAINT uk_pers_religion UNIQUE (c_nr);
ALTER TABLE ONLY public.pers_rollerecht
    ADD CONSTRAINT uk_pers_rollerecht UNIQUE (systemrolle_i_id, recht_c_nr);
ALTER TABLE ONLY public.pers_schicht
    ADD CONSTRAINT uk_pers_schicht UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.pers_schichtzeitmodell
    ADD CONSTRAINT uk_pers_schichtzeitmodell UNIQUE (personal_i_id, zeitmodell_i_id);
ALTER TABLE ONLY public.pers_schichtzuschlag
    ADD CONSTRAINT uk_pers_schichtzuschlag UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.pers_signatur
    ADD CONSTRAINT uk_pers_signatur UNIQUE (personal_i_id, locale_c_nr);
ALTER TABLE ONLY public.pers_sonderzeiten
    ADD CONSTRAINT uk_pers_sonderzeiten UNIQUE (t_datum, taetigkeit_i_id, personal_i_id);
ALTER TABLE ONLY public.pers_stundenabrechnung
    ADD CONSTRAINT uk_pers_stundenabrechnung UNIQUE (personal_i_id, t_datum);
ALTER TABLE ONLY public.pers_systemrolle
    ADD CONSTRAINT uk_pers_systemrolle UNIQUE (c_bez);
ALTER TABLE ONLY public.pers_taetigkeit
    ADD CONSTRAINT uk_pers_taetigkeit UNIQUE (c_nr);
ALTER TABLE ONLY public.pers_tagesart
    ADD CONSTRAINT uk_pers_tagesart UNIQUE (c_nr);
ALTER TABLE ONLY public.pers_telefonzeiten
    ADD CONSTRAINT uk_pers_telefonzeiten UNIQUE (personal_i_id, t_von);
ALTER TABLE ONLY public.pers_themarolle
    ADD CONSTRAINT uk_pers_themarolle UNIQUE (thema_c_nr, systemrolle_i_id);
ALTER TABLE ONLY public.pers_urlaubsanspruch
    ADD CONSTRAINT uk_pers_urlaubsanspruch UNIQUE (personal_i_id, i_jahr);
ALTER TABLE ONLY public.pers_zahltag
    ADD CONSTRAINT uk_pers_zahltag UNIQUE (i_monat, mandant_c_nr);
ALTER TABLE ONLY public.pers_zeitabschluss
    ADD CONSTRAINT uk_pers_zeitabschluss UNIQUE (personal_i_id, t_abgeschlossen_bis);
ALTER TABLE ONLY public.pers_zeitdaten
    ADD CONSTRAINT uk_pers_zeitdaten UNIQUE (personal_i_id, t_zeit);
ALTER TABLE ONLY public.pers_zeitgutschrift
    ADD CONSTRAINT uk_pers_zeitgutschrift UNIQUE (personal_i_id, t_datum);
ALTER TABLE ONLY public.pers_zeitmodell
    ADD CONSTRAINT uk_pers_zeitmodell UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.pers_zeitmodelltag
    ADD CONSTRAINT uk_pers_zeitmodelltag UNIQUE (zeitmodell_i_id, tagesart_i_id);
ALTER TABLE ONLY public.pers_zeitstift
    ADD CONSTRAINT uk_pers_zeitstift UNIQUE (c_nr);
ALTER TABLE ONLY public.pers_zulage
    ADD CONSTRAINT uk_pers_zulage UNIQUE (c_bez);
ALTER TABLE ONLY public.pers_zutrittscontroller
    ADD CONSTRAINT uk_pers_zutrittscontroller UNIQUE (c_nr);
ALTER TABLE ONLY public.pers_zutrittscontroller
    ADD CONSTRAINT uk_pers_zutrittscontroller2 UNIQUE (c_adresse);
ALTER TABLE ONLY public.pers_zutrittsklasse
    ADD CONSTRAINT uk_pers_zutrittsklasse UNIQUE (c_nr);
ALTER TABLE ONLY public.pers_zutrittsklasseobjekt
    ADD CONSTRAINT uk_pers_zutrittsklasseobjekt UNIQUE (zutrittsmodell_i_id, zutrittsklasse_i_id, zutrittsobjekt_i_id);
ALTER TABLE ONLY public.pers_zutrittsmodell
    ADD CONSTRAINT uk_pers_zutrittsmodell UNIQUE (c_nr);
ALTER TABLE ONLY public.pers_zutrittsmodelltag
    ADD CONSTRAINT uk_pers_zutrittsmodelltag UNIQUE (zutrittsmodell_i_id, tagesart_i_id);
ALTER TABLE ONLY public.pers_zutrittsobjekt
    ADD CONSTRAINT uk_pers_zutrittsobjekt UNIQUE (c_nr);
ALTER TABLE ONLY public.pers_zutrittsobjektverwendung
    ADD CONSTRAINT uk_pers_zutrittsobjektverwendung UNIQUE (mandant_c_nr, zutrittsobjekt_i_id);
ALTER TABLE ONLY public.proj_bereich
    ADD CONSTRAINT uk_proj_bereich UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.proj_historyart
    ADD CONSTRAINT uk_proj_historyart UNIQUE (c_bez);
ALTER TABLE ONLY public.proj_leadstatus
    ADD CONSTRAINT uk_proj_leadstatus UNIQUE (c_bez);
ALTER TABLE ONLY public.proj_projektgruppe
    ADD CONSTRAINT uk_proj_projektgruppe UNIQUE (projekt_i_id_vater, projekt_i_id_kind);
ALTER TABLE ONLY public.proj_projekttaetigkeit
    ADD CONSTRAINT uk_proj_projekttaetigkeit UNIQUE (projekt_i_id, artikel_i_id);
ALTER TABLE ONLY public.proj_projekttechniker
    ADD CONSTRAINT uk_proj_projekttechniker UNIQUE (projekt_i_id, personal_i_id);
ALTER TABLE ONLY public.proj_vkfortschritt
    ADD CONSTRAINT uk_proj_vkfortschritt UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.proj_vkfortschrittspr
    ADD CONSTRAINT uk_proj_vkfortschrittspr PRIMARY KEY (locale_c_nr, vkfortschritt_i_id);
ALTER TABLE ONLY public.rech_gutschrifttext
    ADD CONSTRAINT uk_rech_gutschrifttext UNIQUE (mandant_c_nr, locale_c_nr, c_nr);
ALTER TABLE ONLY public.rech_rechnungtext
    ADD CONSTRAINT uk_rech_rechnungtext UNIQUE (mandant_c_nr, locale_c_nr, c_nr);
ALTER TABLE ONLY public.rech_vorkasseposition
    ADD CONSTRAINT uk_rech_vorkasseposition UNIQUE (rechnung_i_id, auftragsposition_i_id);
ALTER TABLE ONLY public.rekla_aufnahmeart
    ADD CONSTRAINT uk_rekla_aufnahmeart UNIQUE (c_bez);
ALTER TABLE ONLY public.rekla_behandlung
    ADD CONSTRAINT uk_rekla_beurteilung UNIQUE (c_nr);
ALTER TABLE ONLY public.rekla_fehler
    ADD CONSTRAINT uk_rekla_fehler UNIQUE (c_bez);
ALTER TABLE ONLY public.rekla_fehlerangabe
    ADD CONSTRAINT uk_rekla_fehlerangabe UNIQUE (c_bez);
ALTER TABLE ONLY public.rekla_massnahme
    ADD CONSTRAINT uk_rekla_massnahme UNIQUE (c_bez);
ALTER TABLE ONLY public.rekla_reklamation
    ADD CONSTRAINT uk_rekla_reklamation UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.rekla_schwere
    ADD CONSTRAINT uk_rekla_schwere UNIQUE (c_nr);
ALTER TABLE ONLY public.rekla_termintreue
    ADD CONSTRAINT uk_rekla_termintreue UNIQUE (i_tage);
ALTER TABLE ONLY public.rekla_wirksamkeit
    ADD CONSTRAINT uk_rekla_wirksamkeit UNIQUE (c_bez);
ALTER TABLE ONLY public.ww_shopgruppespr
    ADD CONSTRAINT uk_shopgruppespr PRIMARY KEY (locale_c_nr, shopgruppe_i_id);
ALTER TABLE ONLY public.lp_spediteur
    ADD CONSTRAINT uk_spediteur_i_id_2 UNIQUE (i_id, mandant_c_nr);
ALTER TABLE ONLY public.stk_alternativmaschine
    ADD CONSTRAINT uk_stk_alternativmaschine UNIQUE (stuecklistearbeitsplan_i_id, maschine_i_id);
ALTER TABLE ONLY public.stk_apkommentar
    ADD CONSTRAINT uk_stk_apkommentar UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.stk_fertigungsgruppe
    ADD CONSTRAINT uk_stk_fertigungsgruppe UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.stk_kommentarimport
    ADD CONSTRAINT uk_stk_kommentarimport UNIQUE (belegart_c_nr);
ALTER TABLE ONLY public.stk_montageart
    ADD CONSTRAINT uk_stk_montageart UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.stk_posersatz
    ADD CONSTRAINT uk_stk_posersatz UNIQUE (stuecklisteposition_i_id, artikel_i_id_ersatz);
ALTER TABLE ONLY public.stk_pruefart
    ADD CONSTRAINT uk_stk_pruefart UNIQUE (c_nr);
ALTER TABLE ONLY public.stk_pruefartspr
    ADD CONSTRAINT uk_stk_pruefartspr PRIMARY KEY (locale_c_nr, pruefart_i_id);
ALTER TABLE ONLY public.stk_stklagerentnahme
    ADD CONSTRAINT uk_stk_stklagerentnahme UNIQUE (stueckliste_i_id, lager_i_id);
ALTER TABLE ONLY public.stk_stklparameter
    ADD CONSTRAINT uk_stk_stklparameter UNIQUE (stueckliste_i_id, c_nr);
ALTER TABLE ONLY public.stk_stuecklisteeigenschaftart
    ADD CONSTRAINT uk_stk_stuecklisteeigenschaftart UNIQUE (c_bez);
ALTER TABLE ONLY public.stk_stuecklisteeigenschaft
    ADD CONSTRAINT uk_stk_stuecklisteneigenschaft_1 UNIQUE (stuecklisteeigenschaftart_i_id, stueckliste_i_id);
ALTER TABLE ONLY public.stk_stuecklistescriptart
    ADD CONSTRAINT uk_stk_stuecklistescriptart UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.stk_agart
    ADD CONSTRAINT uk_stkl_agart UNIQUE (c_nr);
ALTER TABLE ONLY public.ww_vkpfartikelpreisliste
    ADD CONSTRAINT uk_vkpfartikelpreisliste_i_id_2 UNIQUE (i_id, mandant_c_nr);
ALTER TABLE ONLY public.ww_alergen
    ADD CONSTRAINT uk_ww_alergen UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.ww_artgru
    ADD CONSTRAINT uk_ww_artgru UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.ww_artgrumandant
    ADD CONSTRAINT uk_ww_artgrumandant UNIQUE (artgru_i_id, mandant_c_nr);
ALTER TABLE ONLY public.ww_artikel
    ADD CONSTRAINT uk_ww_artikel UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.ww_artikelalergen
    ADD CONSTRAINT uk_ww_artikelalergen UNIQUE (artikel_i_id, alergen_i_id);
ALTER TABLE ONLY public.ww_artikelbestellt
    ADD CONSTRAINT uk_ww_artikelbestellt UNIQUE (c_belegartnr, i_belegartpositionid);
ALTER TABLE ONLY public.ww_artikelkommentar
    ADD CONSTRAINT uk_ww_artikelkommentar UNIQUE (artikel_i_id, artikelkommentarart_i_id, datenformat_c_nr);
ALTER TABLE ONLY public.ww_artikelkommentarart
    ADD CONSTRAINT uk_ww_artikelkommentarart UNIQUE (c_nr);
ALTER TABLE ONLY public.ww_artikelkommentardruck
    ADD CONSTRAINT uk_ww_artikelkommentardruck UNIQUE (artikel_i_id, artikelkommentar_i_id, belegart_c_nr);
ALTER TABLE ONLY public.ww_artikellagerplaetze
    ADD CONSTRAINT uk_ww_artikellagerplaetze UNIQUE (artikel_i_id, lagerplatz_i_id);
ALTER TABLE ONLY public.ww_artikellieferant
    ADD CONSTRAINT uk_ww_artikellieferant UNIQUE (artikel_i_id, lieferant_i_id, t_preisgueltigab, gebinde_i_id);
ALTER TABLE ONLY public.ww_artikellieferantstaffel
    ADD CONSTRAINT uk_ww_artikellieferantstaffel UNIQUE (artikellieferant_i_id, n_menge, t_preisgueltigab);
ALTER TABLE ONLY public.ww_artikelreservierung
    ADD CONSTRAINT uk_ww_artikelreservierung UNIQUE (c_belegartnr, i_belegartpositionid);
ALTER TABLE ONLY public.ww_artikelshopgruppe
    ADD CONSTRAINT uk_ww_artikelshopgruppe UNIQUE (artikel_i_id, shopgruppe_i_id);
ALTER TABLE ONLY public.ww_artikelsnrchnr
    ADD CONSTRAINT uk_ww_artikelsnrchnr UNIQUE (artikel_i_id, c_seriennrchargennr);
ALTER TABLE ONLY public.ww_artikelsperren
    ADD CONSTRAINT uk_ww_artikelsperren UNIQUE (artikel_i_id, sperren_i_id);
ALTER TABLE ONLY public.ww_artikeltrutops
    ADD CONSTRAINT uk_ww_artikeltrutops UNIQUE (artikel_i_id);
ALTER TABLE ONLY public.ww_artkla
    ADD CONSTRAINT uk_ww_artkla UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.ww_automotive
    ADD CONSTRAINT uk_ww_automotive UNIQUE (mandant_c_nr, c_bez);
ALTER TABLE ONLY public.ww_dateiverweis
    ADD CONSTRAINT uk_ww_dateiverweis UNIQUE (mandant_c_nr, c_laufwerk);
ALTER TABLE ONLY public.ww_einkaufsean
    ADD CONSTRAINT uk_ww_einkaufsean_2 UNIQUE (c_ean);
ALTER TABLE ONLY public.ww_ersatztypen
    ADD CONSTRAINT uk_ww_ersatztypen UNIQUE (artikel_i_id, artikel_i_id_ersatz);
ALTER TABLE ONLY public.ww_farbcode
    ADD CONSTRAINT uk_ww_farbcode UNIQUE (c_nr);
ALTER TABLE ONLY public.ww_gebinde
    ADD CONSTRAINT uk_ww_gebinde UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.ww_hersteller
    ADD CONSTRAINT uk_ww_hersteller UNIQUE (c_nr);
ALTER TABLE ONLY public.ww_inventurprotokoll
    ADD CONSTRAINT uk_ww_inventurprotokoll UNIQUE (inventurliste_i_id, t_zeitpunkt);
ALTER TABLE ONLY public.ww_inventurstand
    ADD CONSTRAINT uk_ww_inventurstand UNIQUE (inventur_i_id, artikel_i_id, lager_i_id);
ALTER TABLE ONLY public.ww_katalog
    ADD CONSTRAINT uk_ww_katalog UNIQUE (artikel_i_id, c_katalog);
ALTER TABLE ONLY public.ww_laseroberflaeche
    ADD CONSTRAINT uk_ww_laseroberflaeche UNIQUE (mandant_c_nr, c_nr);
ALTER TABLE ONLY public.ww_material
    ADD CONSTRAINT uk_ww_material UNIQUE (c_nr);
ALTER TABLE ONLY public.ww_materialpreis
    ADD CONSTRAINT uk_ww_materialpreis UNIQUE (material_i_id, t_gueltigab);
ALTER TABLE ONLY public.ww_materialzuschlag
    ADD CONSTRAINT uk_ww_materialzuschlag UNIQUE (material_i_id, mandant_c_nr, t_gueltigab);
ALTER TABLE ONLY public.ww_medical
    ADD CONSTRAINT uk_ww_medical UNIQUE (mandant_c_nr, c_bez);
ALTER TABLE ONLY public.ww_reach
    ADD CONSTRAINT uk_ww_reach UNIQUE (mandant_c_nr, c_bez);
ALTER TABLE ONLY public.ww_rohs
    ADD CONSTRAINT uk_ww_rohs UNIQUE (mandant_c_nr, c_bez);
ALTER TABLE ONLY public.ww_shopgruppe
    ADD CONSTRAINT uk_ww_shopgruppe UNIQUE (mandant_c_nr, c_nr);
ALTER TABLE ONLY public.ww_shopgruppewebshop
    ADD CONSTRAINT uk_ww_shopgruppewebshop UNIQUE (shopgruppe_i_id, webshop_i_id);
ALTER TABLE ONLY public.ww_sperren
    ADD CONSTRAINT uk_ww_sperren UNIQUE (c_bez, mandant_c_nr);
ALTER TABLE ONLY public.ww_verleih
    ADD CONSTRAINT uk_ww_verleih UNIQUE (i_tage);
ALTER TABLE ONLY public.ww_verpackungsmittel
    ADD CONSTRAINT uk_ww_verpackungsmittel UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.ww_verschleissteil
    ADD CONSTRAINT uk_ww_verschleissteil UNIQUE (c_nr);
ALTER TABLE ONLY public.ww_verschleissteilwerkzeug
    ADD CONSTRAINT uk_ww_verschleissteilwerkzeug UNIQUE (verschleissteil_i_id, werkzeug_i_id);
ALTER TABLE ONLY public.ww_vkpfartikelpreisliste
    ADD CONSTRAINT uk_ww_vkpfartikelpreisliste UNIQUE (mandant_c_nr, c_nr);
ALTER TABLE ONLY public.ww_vkpfmengenstaffel
    ADD CONSTRAINT uk_ww_vkpfmengenstaffel UNIQUE (artikel_i_id, n_menge, t_preisgueltigab, vkpfartikelpreisliste_i_id, b_allepreislisten);
ALTER TABLE ONLY public.ww_vorschlagstext
    ADD CONSTRAINT uk_ww_vorschlagstext UNIQUE (locale_c_nr, c_bez);
ALTER TABLE ONLY public.ww_vorzug
    ADD CONSTRAINT uk_ww_vorzug UNIQUE (c_nr, mandant_c_nr);
ALTER TABLE ONLY public.ww_waffenausfuehrung
    ADD CONSTRAINT uk_ww_waffenausfuehrung UNIQUE (c_nr);
ALTER TABLE ONLY public.ww_waffenkaliber
    ADD CONSTRAINT uk_ww_waffenkaliber UNIQUE (c_nr);
ALTER TABLE ONLY public.ww_waffenkategorie
    ADD CONSTRAINT uk_ww_waffenkategorie UNIQUE (c_nr);
ALTER TABLE ONLY public.ww_waffentyp
    ADD CONSTRAINT uk_ww_waffentyp UNIQUE (c_nr);
ALTER TABLE ONLY public.ww_waffentyp_fein
    ADD CONSTRAINT uk_ww_waffentyp_fein UNIQUE (c_nr);
ALTER TABLE ONLY public.ww_waffenzusatz
    ADD CONSTRAINT uk_ww_waffenzusatz UNIQUE (c_nr);
ALTER TABLE ONLY public.ww_webshop
    ADD CONSTRAINT uk_ww_webshop UNIQUE (mandant_c_nr, c_bez);
ALTER TABLE ONLY public.ww_werkzeug
    ADD CONSTRAINT uk_ww_werkzeug UNIQUE (c_nr);
ALTER TABLE ONLY public.ww_zugehoerige
    ADD CONSTRAINT uk_ww_zugehoerige UNIQUE (artikel_i_id, artikel_i_id_zugehoerig);
ALTER TABLE ONLY public.ww_vkpfartikelverkaufspreisbasis
    ADD CONSTRAINT "uk_wwvb$_mandant_c_nr_artikel_i_id_t_verkaufspreisbasisgueltiga" UNIQUE (mandant_c_nr, artikel_i_id, t_verkaufspreisbasisgueltigab);
ALTER TABLE ONLY public.lp_zahlungsziel
    ADD CONSTRAINT uk_zahlungsziel_i_id_2 UNIQUE (i_id, mandant_c_nr);
ALTER TABLE ONLY public.pers_kollektivuestd_bva
    ADD CONSTRAINT ukpers_kollektivuestd_bva UNIQUE (kollektiv_i_id, tagesart_i_id);
ALTER TABLE ONLY public.rech_rechnung
    ADD CONSTRAINT ux_rech_rechunung_mandant_c_nr_c_nr_rechnungart_c_nr UNIQUE (mandant_c_nr, rechnungart_c_nr, c_nr);
CREATE INDEX ix_angb_angebotposition_position_i_id_artikelset ON public.angb_angebotposition USING btree (position_i_id_artikelset);
CREATE INDEX ix_artikelreferenznuummer ON public.ww_artikel USING btree (mandant_c_nr, c_referenznr);
CREATE INDEX ix_auft_auftrag_bs ON public.auft_auftrag USING btree (bestellung_i_id_anderermandant);
CREATE INDEX ix_auft_auftragposition_position_i_id_artikelset ON public.auft_auftragposition USING btree (position_i_id_artikelset);
CREATE INDEX ix_bes_bestellposition_bestellung_i_id ON public.bes_bestellposition USING btree (bestellung_i_id);
CREATE INDEX ix_bes_bestellposition_i_sort ON public.bes_bestellposition USING btree (i_sort);
CREATE INDEX ix_bes_bestellposition_position_i_id_artikelset ON public.bes_bestellposition USING btree (position_i_id_artikelset);
CREATE INDEX ix_bes_bestellung1 ON public.bes_bestellung USING btree (personal_i_id_anforderer, mandant_c_nr, bestellungstatus_c_nr, lieferant_i_id_bestelladresse, i_id);
CREATE INDEX ix_bes_bestelung2 ON public.bes_bestellung USING btree (mandant_c_nr, bestellungstatus_c_nr);
CREATE INDEX ix_bes_wareneingangposition_bestellposition_i_id ON public.bes_wareneingangsposition USING btree (bestellposition_i_id);
CREATE INDEX ix_betriebskalender ON public.pers_betriebskalender USING btree (mandant_c_nr, t_datum);
CREATE INDEX ix_er_auftragszuordnung_auftrag_i_id ON public.er_auftragszuordnung USING btree (auftrag_i_id);
CREATE INDEX ix_fb_buchung1 ON public.fb_buchung USING btree (t_storniert, buchungsart_c_nr, t_buchungsdatum, b_autombuchung_eb);
CREATE INDEX ix_fb_buchung_geschaeftsjahr_i_geschaeftsjahr ON public.fb_buchung USING btree (geschaeftsjahr_i_geschaeftsjahr);
CREATE INDEX ix_fb_buchungdetail1 ON public.fb_buchungdetail USING btree (buchung_i_id, konto_i_id);
CREATE INDEX ix_fb_buchungdetail2 ON public.fb_buchungdetail USING btree (konto_i_id, buchungdetailart_c_nr, buchung_i_id);
CREATE INDEX ix_fb_buchungdetail_i_ausziffern ON public.fb_buchungdetail USING btree (i_ausziffern);
CREATE INDEX ix_fclieferadr1 ON public.fc_fclieferadresse USING btree (forecast_i_id);
CREATE INDEX ix_fert_lossollmaterial_lossollmaterial_i_id_original ON public.fert_lossollmaterial USING btree (lossollmaterial_i_id_original);
CREATE INDEX ix_forecastpos1 ON public.fc_forecastposition USING btree (forecastauftrag_i_id);
CREATE INDEX ix_lagerabgangursprung_i_lagerbewegungidursprung ON public.ww_lagerabgangursprung USING btree (i_lagerbewegungidursprung);
CREATE INDEX ix_lastschriftvorschlag_c_auftraggeberreferenz ON public.rech_lastschriftvorschlag USING btree (c_auftraggeberreferenz);
CREATE INDEX ix_lieferschein1 ON public.ls_lieferschein USING btree (mandant_c_nr, kunde_i_id_lieferadresse, rechnung_i_id, i_id);
CREATE INDEX ix_lieferschein2 ON public.ls_lieferschein USING btree (t_belegdatum, lieferscheinstatus_c_nr, i_id);
CREATE INDEX ix_lieferschein_mandant_c_nr ON public.ls_lieferschein USING btree (mandant_c_nr);
CREATE INDEX ix_losablieferung_los_i_id ON public.fert_losablieferung USING btree (los_i_id);
CREATE INDEX ix_losap1 ON public.fert_lossollarbeitsplan USING btree (lossollmaterial_i_id);
CREATE INDEX ix_losgutschlecht_lossollarbeitsplan_i_id ON public.fert_losgutschlecht USING btree (lossollarbeitsplan_i_id);
CREATE INDEX ix_losistmaterial_lossollmaterial_i_id ON public.fert_losistmaterial USING btree (lossollmaterial_i_id);
CREATE INDEX ix_loslagerentnahme1 ON public.fert_loslagerentnahme USING btree (los_i_id);
CREATE INDEX ix_lossollarbeitsplan_los_i_id_b_fertig ON public.fert_lossollarbeitsplan USING btree (los_i_id, b_fertig);
CREATE INDEX ix_lossollmaterial_los_i_id ON public.fert_lossollmaterial USING btree (los_i_id);
CREATE INDEX ix_ls_lieferscheinposition_artikel_i_id ON public.ls_lieferscheinposition USING btree (artikel_i_id);
CREATE INDEX ix_ls_lieferscheinposition_auftragposition_i_id ON public.ls_lieferscheinposition USING btree (auftragposition_i_id);
CREATE INDEX ix_ls_lieferscheinposition_forecastposition_i_id ON public.ls_lieferscheinposition USING btree (forecastposition_i_id);
CREATE INDEX ix_ls_lieferscheinposition_i_sort ON public.ls_lieferscheinposition USING btree (i_sort);
CREATE INDEX ix_ls_lieferscheinposition_lieferschein_i_id ON public.ls_lieferscheinposition USING btree (lieferschein_i_id);
CREATE INDEX ix_ls_lieferscheinposition_position_i_id_artikelset ON public.ls_lieferscheinposition USING btree (position_i_id_artikelset);
CREATE INDEX ix_paneldaten_panelbeschreibung_i_id_x_inhalt ON public.lp_paneldaten USING btree (panelbeschreibung_i_id, x_inhalt);
CREATE INDEX ix_part_kunde1 ON public.part_kunde USING btree (i_id, partner_i_id);
CREATE INDEX ix_part_partner1 ON public.part_partner USING btree (landplzort_i_id, i_id);
CREATE INDEX ix_pers_maschinenzeitdaten_maschine_i_id ON public.pers_maschinenzeitdaten USING btree (maschine_i_id);
CREATE INDEX ix_pers_zeitdaten_c_belegartnr_i_belegartid ON public.pers_zeitdaten USING btree (c_belegartnr, i_belegartid);
CREATE INDEX ix_pers_zeitdaten_c_belegartnr_i_belegartpositionid ON public.pers_zeitdaten USING btree (c_belegartnr, i_belegartpositionid);
CREATE INDEX ix_personalgehalt_personal_i_id ON public.pers_personalgehalt USING btree (personal_i_id);
CREATE INDEX ix_rech_rechnungposition_auftragposition_i_id ON public.rech_rechnungposition USING btree (auftragposition_i_id);
CREATE INDEX ix_rech_rechnungposition_position_i_id_artikelset ON public.rech_rechnungposition USING btree (position_i_id_artikelset);
CREATE INDEX ix_speiseplanposition ON public.kue_speiseplanposition USING btree (speiseplan_i_id);
CREATE INDEX ix_stk_stueckliste2 ON public.stk_stueckliste USING btree (c_fremdsystemnr, mandant_c_nr);
CREATE INDEX ix_stuecklistearbeitsplan_stueckliste_i_id ON public.stk_stuecklistearbeitsplan USING btree (stueckliste_i_id);
CREATE INDEX ix_stuecklisteposition_artikel_i_id ON public.stk_stuecklisteposition USING btree (artikel_i_id);
CREATE INDEX ix_stuecklisteposition_stueckliste_i_id ON public.stk_stuecklisteposition USING btree (stueckliste_i_id);
CREATE INDEX ix_vkpfartikelpreis_t_aendern ON public.ww_vkpfartikelpreis USING btree (t_aendern);
CREATE INDEX ix_ww_artikel_artkla_i_id ON public.ww_artikel USING btree (artkla_i_id);
CREATE INDEX ix_ww_artikelkommentarspr_t_aendern ON public.ww_artikelkommentarspr USING btree (t_aendern);
CREATE INDEX ix_ww_lagerabgangursprung_i_lagerbewegungid ON public.ww_lagerabgangursprung USING btree (i_lagerbewegungid);
CREATE INDEX ix_ww_lagerbewegung1 ON public.ww_lagerbewegung USING btree (artikel_i_id, t_belegdatum);
CREATE INDEX ix_ww_lagerbewegung2 ON public.ww_lagerbewegung USING btree (c_seriennrchargennr, b_historie, n_menge);
CREATE INDEX ix_ww_lagerbewegung3 ON public.ww_lagerbewegung USING btree (artikel_i_id, lager_i_id, b_abgang, b_historie, c_seriennrchargennr, i_id);
CREATE INDEX ix_ww_lagerbewegung_c_belegartnr_i_belegartpositionid ON public.ww_lagerbewegung USING btree (c_belegartnr, i_belegartpositionid);
CREATE INDEX ix_ww_lagerbewegung_i_id_buchung ON public.ww_lagerbewegung USING btree (i_id_buchung);
CREATE INDEX ix_zahlungsvorschlag_c_auftraggeberreferenz ON public.er_zahlungsvorschlag USING btree (c_auftraggeberreferenz);
CREATE INDEX ww_vkpfartikelverkaufspreisbasis_t_aendern ON public.ww_vkpfartikelverkaufspreisbasis USING btree (t_aendern);
