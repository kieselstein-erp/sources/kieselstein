# Installing Kieselstein Windows Service

To run kieselstein as a Windows service, we recommend to use [Apache Commons Procrun](https://commons.apache.org/proper/commons-daemon/procrun.html)

## Logging

Per default, logfiles are written to:

```
c:\Windows\System32\LogFiles\Apache\
```

## Attention
To run the install-kieselstein-services.bat do **not** run with administrativ rights. During installation it has to ask for apache installation.

## after installation
- check ?:\kieselstein\dist\wildfly-12.0.0.Final\standalone\deployments\kieselstein-?.?.?.ear.deploeyed
- check Tomcat bei http://localhost:8280/kieselstein-rest-docs/
- visit http://docs.kieselstein-erp.org

## Todo

Describe:
* Security
* Run on startup