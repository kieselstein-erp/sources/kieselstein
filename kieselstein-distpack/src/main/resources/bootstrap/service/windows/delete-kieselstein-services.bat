@echo off

rem # set this either from outside, or here
rem set KIESELSTEIN_DIST=
rem set KIESELSTEIN_DATA=

if "x%KIESELSTEIN_DIST%" == "x" (
    echo KIESELSTEIN_DIST not set. Set it to the folder, where wildfly and tomcat servers are in. Exiting.
    pause
    goto exit1
)

set service_exe=%KIESELSTEIN_DIST%/bootstrap/service/windows/bin/prunsrv.exe

%service_exe% delete Kieselstein-Main-Server
