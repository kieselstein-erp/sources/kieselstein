@echo off

rem # set this either from outside, or here
rem set KIESELSTEIN_DIST=
rem set KIESELSTEIN_DATA=
rem set JAVA_HOME=

if "x%KIESELSTEIN_DIST%" == "x" (
    echo KIESELSTEIN_DIST not set. Set it to the folder, where wildfly and tomcat servers are in. Exiting.
    pause
    goto exit1
)

if "x%KIESELSTEIN_DATA%" == "x" (
    echo KIESELSTEIN_DATA not set. Set it to the folder, data and configuration shall be placed. Exiting.
    pause
    goto exit1
)

if "x%JAVA_HOME%" == "x" (
    echo JAVA_HOME not set. Set it to the folder compatible with kieselstein.
    pause
    goto exit1
)

set service_exe=%KIESELSTEIN_DIST%/bootstrap/service/windows/bin/prunsrv.exe

%service_exe% update Kieselstein-Main-Server --Description "Kieselstein Main Server" --DisplayName "Kieselstein Main Server" ^
    --StopImage "cmd.exe" --StopPath "%KIESELSTEIN_DIST%\bin" --StopParams "/c SET JAVA_HOME=%JAVA_HOME%&&SET KIESELSTEIN_DIST=%KIESELSTEIN_DIST%&&SET KIESELSTEIN_DATA=%KIESELSTEIN_DATA%&&%KIESELSTEIN_DIST%\bin\shutdown-kieselstein-main-server.bat" --StopMode "exe" --StopTimeout 0 ^
    --StartImage "cmd.exe" --StartPath "%KIESELSTEIN_DIST%\bin" --StartParams "/c SET JAVA_HOME=%JAVA_HOME%&&SET KIESELSTEIN_DIST=%KIESELSTEIN_DIST%&&SET KIESELSTEIN_DATA=%KIESELSTEIN_DATA%&&%KIESELSTEIN_DIST%\bin\launch-kieselstein-main-server.bat" --StartMode "exe" ^
    --StdError "auto" --StdOutput "auto" --LogJniMessages 0 --Rotate 0 ^
    --ServiceUser "LocalSystem"

:exit1