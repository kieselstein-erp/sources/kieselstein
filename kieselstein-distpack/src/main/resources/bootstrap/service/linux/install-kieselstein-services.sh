#!/usr/bin/env bash

# set this either from outside, or here
# KIESELSTEIN_DIST=
# KIESELSTEIN_DATA=

if [ -z ${KIESELSTEIN_DIST+x} ]; then
  echo KIESELSTEIN_DIST not set. Set it to the folder, where wildfly and tomcat servers are in. Exiting.
  read -p "Press any key to resume ..."
  exit 1
fi

if [ -z ${KIESELSTEIN_DATA+x} ]; then
  echo KIESELSTEIN_DATA not set. Set it to the folder, data and configuration shall be placed. Exiting.
  read -p "Press any key to resume ..."
  exit 1
fi

# Create the kieselstein user and group if not exists
isgroup=$(/bin/egrep -i kieselstein /etc/group)
if [ "$isgroup" == "" ]
then
  groupadd -r kieselstein
fi

isuser=$(/bin/egrep -i kieselstein /etc/passwd)
if [ "$isuser" == "" ]
then
  useradd -r -g kieselstein -d $KIESELSTEIN_DIST -s /sbin/nologin kieselstein
fi

# Adjust user and group
chown -R kieselstein:kieselstein $KIESELSTEIN_DIST
chown -R kieselstein:kieselstein $KIESELSTEIN_DATA

cp kieselstein-main-server.service /etc/systemd/system/

# Start and enable
systemctl start kieselstein-main-server.service
systemctl enable kieselstein-main-server.service

