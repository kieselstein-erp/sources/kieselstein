## How to configure WildFly as a systemd service

### Install services
Set Environment variables KIESELSTEIN_DIST and KIESELSTEIN_DATA.
Install the services with root privilegs from command line:
    # ./install-kieselstein-services.sh

### Notes:

The install script performs following steps:
#### Create a wildfly user

    # groupadd -r kieselstein
    # useradd -r -g kieselstein -d $KIESELSTEIN_DIST -s /sbin/nologin kieselstein

#### Adjust user and group

    # chown -R kieselstein:kieselstein $KIESELSTEIN_DIST
    # chown -R kieselstein:kieselstein $KIESELSTEIN_DATA

#### Configure systemd

    # cp kieselstein-main-server.service /etc/systemd/system/

#### Start and enable services

    # systemctl start kieselstein-main-server.service
    # systemctl enable kieselstein-main-server.service
