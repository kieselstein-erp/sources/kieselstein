@echo off

rem # set this either from outside, or here
rem set KIESELSTEIN_DIST=
rem set KIESELSTEIN_DATA=

if "x%KIESELSTEIN_DIST%" == "x" (
    echo KIESELSTEIN_DIST not set. Set it to the folder, where wildfly and tomcat servers are in. Exiting.
    pause
    goto exit1
)

if "x%KIESELSTEIN_DATA%" == "x" (
    echo KIESELSTEIN_DATA not set. Set it to the folder, data and configuration shall be placed. Exiting.
    pause
    goto exit1
)

rem # database settings for the main kieselstein database
if "x%MAIN_DB_HOST%"=="x" set MAIN_DB_HOST=@mainDbHost@
if "x%MAIN_DB_PORT%"=="x" set MAIN_DB_PORT=@mainDbPort@
if "x%MAIN_DB_NAME%"=="x" set MAIN_DB_NAME=@mainDbName@
if "x%MAIN_DB_USER%"=="x" set MAIN_DB_USER=@mainDbUser@
if "x%MAIN_DB_PASS%"=="x" set MAIN_DB_PASS=@mainDbPass@

set PGPASSWORD=%MAIN_DB_PASS%
for /f %%i in ('psql -qtAX -U %MAIN_DB_USER% -d %MAIN_DB_NAME% -c "SELECT min(database_version) FROM lp_anwender"^&call echo %%^^errorlevel%%^>return.txt') do set DB_VERSION=%%i
set PGPASSWORD=

set /p RET=<return.txt
del return.txt

set /p VERSION=<%KIESELSTEIN_DIST%\VERSION.txt

if not %DB_VERSION% == "" (
    rem compare all charactes of the db-version fit's with the characters of the server-version,
    rem because in the server-version it can be, that the build number is added.
    setlocal enabledelayedexpansion
    set /a I=0
    set /a is_valid=1
    :loop_start
    if "!DB_VERSION:~%I%,1!"=="" goto loop_end
    if not "!VERSION:~%I%,1!"=="!DB_VERSION:~%I%,1!" set /a is_valid=0
    set /a I+=1
    goto loop_start
    :loop_end
    if "%is_valid%" == "0" (
        echo Server version %VERSION% and database version %DB_VERSION% do not match! Exiting.
        echo %date% %time%: Server version %VERSION% and database version %DB_VERSION% do not match! Exiting. >> %KIESELSTEIN_DIST%\launch-kieselstein-main.error.log
        goto exit1
    )
    rem important or variables (passwords) with !! will get expanded and will be invalid
    setlocal disabledelayedexpansion
)

rem  # database settings for the kieselstein doc database
if "x%DOC_DB_HOST%"=="x" set DOC_DB_HOST=@docDbHost@
if "x%DOC_DB_PORT%"=="x" set DOC_DB_PORT=@docDbPort@
if "x%DOC_DB_NAME%"=="x" set DOC_DB_NAME=@docDbName@
if "x%DOC_DB_USER%"=="x" set DOC_DB_USER=@docDbUser@
if "x%DOC_DB_PASS%"=="x" set DOC_DB_PASS=@docDbPass@

rem # Settings for the document-management system (='jackrabbit')
if "x%DOC_REPO%"=="x" set DOC_REPO=%KIESELSTEIN_DATA%\jackrabbit
rem # probably change this to a different location if you have your own special configuration
if "x%DOC_CONFIG%"=="x" set DOC_CONFIG=%KIESELSTEIN_DIST%\conf\@jackrabbitConfigFileName@
rem # the directory, which shall be served as "/clients", so that the clients are downloadable
if "x%CLIENTS_DIR%"=="x" set CLIENTS_DIR=%KIESELSTEIN_DIST%\clients
rem # Option to set the Xmx Option for the Java Process.
if "x%KIESELSTEIN_JAVA_OPT_XMX%"=="x" set KIESELSTEIN_JAVA_OPT_XMX=1512m
rem # Option to set the Xms Option for the Java Process.
if "x%KIESELSTEIN_JAVA_OPT_XMS%"=="x" set KIESELSTEIN_JAVA_OPT_XMS=64m

rem # Settings for clevercure
if "x%CLEVERCURE_USER%"=="x" set CLEVERCURE_USER=clevercure
if "x%CLEVERCURE_PASS%"=="x" set CLEVERCURE_PASS=clevercure
if "x%KIESELSTEIN_WEBSHOP%"=="x" set KIESELSTEIN_WEBSHOP=clevercure
if "x%CLEVERCURE_STORE_BEFORE%"=="x" set CLEVERCURE_STORE_BEFORE=TRUE

rem # this combines all the java opts for the main server
set MAIN_SERVER_OPTS=@javaOpts@
set MAIN_SERVER_OPTS=%MAIN_SERVER_OPTS% -Xmx%KIESELSTEIN_JAVA_OPT_XMX%
set MAIN_SERVER_OPTS=%MAIN_SERVER_OPTS% -Xms%KIESELSTEIN_JAVA_OPT_XMS%

rem # Use the following line if you get a timeout error. (Can happen when JackRabbit is updating it's indices)
rem # set MAIN_SERVER_OPTS=%MAIN_SERVER_OPTS% -Djboss.as.management.blocking.timeout=3600

rem #
rem # For some special wildfly ports, we do not have any envvars for the moment. Try to avoid changing them!
rem # in case needed, they could be changed as follows:
rem #
rem set MAIN_SERVER_OPTS=%MAIN_SERVER_OPTS% -Djboss.http.port=8080
rem set MAIN_SERVER_OPTS=%MAIN_SERVER_OPTS% -Djboss.https.port=8643
rem set MAIN_SERVER_OPTS=%MAIN_SERVER_OPTS% -Djboss.socket.binding.port-offset=0
rem set MAIN_SERVER_OPTS=%MAIN_SERVER_OPTS% -Djboss.management.http.port=9990
rem set MAIN_SERVER_OPTS=%MAIN_SERVER_OPTS% -Djboss.management.https.port=9993
rem set MAIN_SERVER_OPTS=%MAIN_SERVER_OPTS% -Djboss.ajp.port=8009

set JAVA_OPTS=%MAIN_SERVER_OPTS%
echo JAVA_OPTS: %JAVA_OPTS%

set wildfly_bin_dir=%KIESELSTEIN_DIST%\@wildflyDirName@\bin
set standalone_dir=%KIESELSTEIN_DIST%\@wildflyDirName@\standalone

set LAUNCH_COMMAND=%wildfly_bin_dir%\standalone.bat  -b 0.0.0.0 -bmanagement 0.0.0.0 -c @wildflyConfigFile@ -Djava.class.path="%standalone_dir%/lib/*"
echo LAUNCH_COMMAND: %LAUNCH_COMMAND%

cd %wildfly_bin_dir%

set NOPAUSE=TRUE
%LAUNCH_COMMAND%

:exit1
