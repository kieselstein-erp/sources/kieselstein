@echo off

rem # set this either from outside, or here
rem set KIESELSTEIN_DIST=

if "x%KIESELSTEIN_DIST%" == "x" (
    echo KIESELSTEIN_DIST not set. Set it to the folder, where wildfly and tomcat servers are in. Exiting.
    exit 1
)


set NOPAUSE=TRUE

set wildfly_bin_dir=%KIESELSTEIN_DIST%\@wildflyDirName@\bin

set SHUTDOWN_COMMAND=%wildfly_bin_dir%\jboss-cli.bat -c shutdown
echo SHUTDOWN_COMMAND: %SHUTDOWN_COMMAND%

%SHUTDOWN_COMMAND%

