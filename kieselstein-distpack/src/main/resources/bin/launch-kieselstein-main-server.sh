#!/usr/bin/env bash

# set this either from outside, or here
# KIESELSTEIN_DIST=
# KIESELSTEIN_DATA=

if [ -z ${KIESELSTEIN_DIST+x} ]; then
  echo KIESELSTEIN_DIST not set. Set it to the folder, where wildfly and tomcat servers are in. Exiting.
  read -p "Press any key to resume ..."
  exit 1
fi

if [ -z ${KIESELSTEIN_DATA+x} ]; then
  echo KIESELSTEIN_DATA not set. Set it to the folder, data and configuration shall be placed. Exiting.
  read -p "Press any key to resume ..."
  exit 1
fi

# database settings for the main kieselstein database
MAIN_DB_HOST=${MAIN_DB_HOST:-"@mainDbHost@"}
MAIN_DB_PORT=${MAIN_DB_PORT:-"@mainDbPort@"}
MAIN_DB_NAME=${MAIN_DB_NAME:-"@mainDbName@"}
MAIN_DB_USER=${MAIN_DB_USER:-"@mainDbUser@"}
MAIN_DB_PASS=${MAIN_DB_PASS:-"@mainDbPass@"}

# database settings for the kieselstein doc database
DOC_DB_HOST=${DOC_DB_HOST:-"@docDbHost@"}
DOC_DB_PORT=${DOC_DB_PORT:-"@docDbPort@"}
DOC_DB_NAME=${DOC_DB_NAME:-"@docDbName@"}
DOC_DB_USER=${DOC_DB_USER:-"@docDbUser@"}
DOC_DB_PASS=${DOC_DB_PASS:-"@docDbPass@"}

# Default Settings for clevercure
export CLEVERCURE_USER=${CLEVERCURE_USER:-"clevercure"}
export CLEVERCURE_PASS=${CLEVERCURE_PASS:-"clevercure"}
export KIESELSTEIN_WEBSHOP=${KIESELSTEIN_WEBSHOP:-"clevercure"}
export CLEVERCURE_STORE_BEFORE=${CLEVERCURE_STORE_BEFORE:-"TRUE"}

export DOC_REPO=${DOC_REPO:-"${KIESELSTEIN_DATA}/jackrabbit"}
export DOC_CONFIG=${DOC_CONFIG:-"${KIESELSTEIN_DIST}/conf/@jackrabbitConfigFileName@"}

# the directory, which shall be served as "/clients", so that the clients are downloadable
export CLIENTS_DIR=${CLIENTS_DIR:-"${KIESELSTEIN_DIST}/clients"}

DB_VERSION=$(psql -qtAX "postgres://$MAIN_DB_USER:$MAIN_DB_PASS@$MAIN_DB_HOST:$MAIN_DB_PORT/$MAIN_DB_NAME" -c "SELECT min(database_version) FROM lp_anwender")

if [[ $DB_VERSION == "" ]]; then
    echo "Failed to retrieve database version! Make sure database exists and is up-to-date!"
    exit
fi

SRV_VERSION=$(cat "$KIESELSTEIN_DIST/VERSION.txt")
if [[ $SRV_VERSION != *"$DB_VERSION"*  &&  "$DB_VERSION" != "" ]]; then
  echo "`date +"%Y-%m-%d %T"`: Server version ($SRV_VERSION) and database version ($DB_VERSION) do not match! Exiting." | tee -a $KIESELSTEIN_DIST/launch-kieselstein-main.error.log
  exit
fi

# Option to set the Xmx Option for the Java Process.
KIESELSTEIN_JAVA_OPT_XMX=${KIESELSTEIN_JAVA_OPT_XMX:-"2048m"}
# Option to set the Xms Option for the Java Process.
KIESELSTEIN_JAVA_OPT_XMS=${KIESELSTEIN_JAVA_OPT_XMS:-"64m"}

# this combines all the java opts for the main server
MAIN_SERVER_OPTS="@javaOpts@"
MAIN_SERVER_OPTS="${MAIN_SERVER_OPTS} -Xmx${KIESELSTEIN_JAVA_OPT_XMX}"
MAIN_SERVER_OPTS="${MAIN_SERVER_OPTS} -Xms${KIESELSTEIN_JAVA_OPT_XMS}"

# Use the following line if you get a timeout error. (Can happen when JackRabbit is updating it's indices)
# MAIN_SERVER_OPTS="${MAIN_SERVER_OPTS} -Djboss.as.management.blocking.timeout=3600"

################################################################################################

#
# For some special wildfly ports, we do not have any envvars for the moment. Try to avoid changing them!
# in case needed, they could be changed as follows:
#
# MAIN_SERVER_OPTS="${MAIN_SERVER_OPTS} -Djboss.http.port=8080"
# MAIN_SERVER_OPTS="${MAIN_SERVER_OPTS} -Djboss.https.port=8643"
# MAIN_SERVER_OPTS="${MAIN_SERVER_OPTS} -Djboss.socket.binding.port-offset=0"
# MAIN_SERVER_OPTS="${MAIN_SERVER_OPTS} -Djboss.management.http.port=9990"
# MAIN_SERVER_OPTS="${MAIN_SERVER_OPTS} -Djboss.management.https.port=9993"
# MAIN_SERVER_OPTS="${MAIN_SERVER_OPTS} -Djboss.ajp.port=8009"

export JAVA_OPTS="${MAIN_SERVER_OPTS}"
echo JAVA_OPTS: "${JAVA_OPTS}"

wildfly_bin_dir=${KIESELSTEIN_DIST}/@wildflyDirName@/bin
standalone_dir=${KIESELSTEIN_DIST}/@wildflyDirName@/standalone

LAUNCH_COMMAND="${wildfly_bin_dir}/standalone.sh  -b 0.0.0.0 -bmanagement 0.0.0.0 -c @wildflyConfigFile@ -Djava.class.path=${standalone_dir}/lib/*"
echo LAUNCH_COMMAND: "${LAUNCH_COMMAND}"

cd "${wildfly_bin_dir}" || exit

export NOPAUSE=TRUE
${LAUNCH_COMMAND}
