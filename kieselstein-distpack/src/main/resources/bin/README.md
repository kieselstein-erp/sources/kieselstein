# In diesem Verzeichnis befinden sich die Skripte zum Starten und Stoppen der Kieselstein-Server-Dienste.
## launch-kieselstein-main-server.bat & launch-kieselstein-main-server.sh
Diese Dateien starten den Wildfly-Dienst über den jegliche Kieselstein-Logik verarbeitet wird.
Für den Start dieser Skripte können folgende Umgebungsvariablen gesetzt werden.

### Pflicht Variablen:
- KIESELSTEIN_DIST:
  - Das Installationsverzeichnis wo sich die Distributions des Kieselstein befindet.
  - **Beispiel Windows**: C:\kieselstein\dist
  - **Beispiel Linux**: /opt/kieselstein/dist
- KIESELSTEIN_DATA:
  - Das Verzeichnis wo Dateien, und Konfiguration der Installation abgelegt werden sollen (bsp. Dokumentendatenbank).
  - **Beispiel Windows**: C:\kieselstein\data
  - **Beispiel Linux**: /opt/kieselstein/data

### Optionale Variablen:
- DOC_CONFIG:
  - Pfad zur Konfigurationsdatei für die Dokumentenablage.
  - **Initialwert (Linux)**: ${KIESELSTEIN_DIST}/conf/jackrabbit-datastore-db.xml
  - **Initialwert (Windows)**: %KIESELSTEIN_DIST%\conf\jackrabbit-datastore-db.xml
  - **Beispiele für Datei basierende Dokumentendatenbank**: ${KIESELSTEIN_DIST}/conf/jackrabbit-datastore-fs.xml bzw. %KIESELSTEIN_DIST%\conf\jackrabbit-datastore-fs.xml
- KIESELSTEIN_JAVA_OPT_XMX:
  - Steuert die maximale Größe des Java-Heap-Speichers. Dieser Wert sollte bei größeren Installationen angepasst werden.
  - **Initialwert**: 1512m
  - **Beispielwert**: 5G
- KIESELSTEIN_JAVA_OPT_XMS:
  - Steuert die Anfangsgröße des Java-Heap-Speichers. Dieser Wert sollte bei größeren Installationen angepasst werden.
  - **Initialwert**: 64m
  - **Beispielwert**: 512m
- LOG_LEVEL:
  - steuert, welche Logs in der Konsole ausgegeben werden
  - **default**: INFO
  - **andere moegliche Werte**: FATAL, TRACE, ERROR, WARN, INFO, DEBUG
- KIESELSTEIN_DEV:
  - verhindert, momentan nur das senden von E-Mails
  - **default**: false
  - **werte**: false|true

## Musterdateien zum Update des Servers unter Windows
- Beschreibung siehe https://docs.kieselstein-erp.org/docs/installation/01_server/update/windows/#kurzfassung-des-updatevorgangs
- loeschen.bat -> entfernen der nicht mehr benötigten Serverdateien (erst nach dem Kopieren des bestehenden Servers)
- initial.bat -> Heben der Datenbank (nach dem einkopieren des aktuellen Dist)