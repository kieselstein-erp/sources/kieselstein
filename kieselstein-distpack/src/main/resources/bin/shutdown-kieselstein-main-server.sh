#!/usr/bin/env bash

# set this either from outside, or here
# KIESELSTEIN_DIST=

if [ -z ${KIESELSTEIN_DIST+x} ];
then
  echo KIESELSTEIN_DIST not set. Set it to the folder, where wildfly and tomcat servers are in. Exiting.
  read -p "Press any key to resume ..."
  exit 1
fi

NOPAUSE=TRUE

wildfly_bin_dir=${KIESELSTEIN_DIST}/@wildflyDirName@/bin

SHUTDOWN_COMMAND="${wildfly_bin_dir}/jboss-cli.sh -c shutdown"
echo SHUTDOWN_COMMAND: "${SHUTDOWN_COMMAND}"

${SHUTDOWN_COMMAND}

