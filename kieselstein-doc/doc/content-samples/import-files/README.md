Hier werden Beispieldateien für den Import von Daten ins Kieselstein ERP (wie z.B. Artikel->Menüpunkt "XLS-Import") zur Verfügung gestellt.

## Hinweise:
Der Import funktioniert ausschließlich im angegebenen Musterformat.

Daher beachte bitte, wenn XLS angegeben ist, so muss im XLS Format importiert werden. XLSX wird derzeit generell nicht unterstützt.

Solltest du, z.B. von deinem Kunden, XLSX Dokumente bekommen haben, so wähle z.B. in deinem Libre-Office, Datei, speichern unter und dann Excel 97-2003

Alle Spalten müssen den korrekten Spaltentyp haben, das gilt insbesondere für Zahlen.

Bitte beachte, dass die Spaltentypen und Spaltenlängen zu den Daten in deinem Kieselstein ERP passen müssen

Für genauere Angaben siehe in der Dokumentation oder direkt in der Tabelle nach.

Beim Format CSV ist es leider vom jeweiligen Import abhängig, ob als Spaltentrenner das Komma (was die original Definition ist) oder das in Europa übliche Semicolon(;) verwendet wird.

Werden boolsche Werte angegeben (in der Regel sind das Checkboxen in der Oberfläche), so sind diese Pflichtfelder und müssen mit 0 (falsch) und 1 (Wahr) angegeben werden.

Kommentare die in Texteingaben abgespeichert werden, haben in der Regel eine Länge von 3000 Zeichen, inkl. jeglicher Formatierungssteuerzeichen.

In den XLS Importen sind die Spaltenüberschriften ausschlaggebend. Die Reihenfolge sollte in der Regel egal sein, wobei sich bewährt hat, den wichtigsten Schlüssel z.B. die Artikelnummer, immer in der ersten Spalte anzuführen.

In den anderen Importen müssen die angegebenen Reihenfolgen der Spalten eingehalten werden.

Beachte bitte auch, dass in den CSV / Text Importen unterschiedliche Verhalten für die erste Zeile gegeben ist.<br>
Leider sind derzeit beide Varianten umgesetzt, sodass manche mit Spaltenüberschriften zurecht kommen und andere diese wiederum als echte Daten interpretieren.

Die angeführten Musterdateien wurden aus der Beschreibung zusammengestellt, bzw. durch Analyse des Quellcodes.

## Musterdateien
| Zweck| Filename | Modul | unterer Reiter | oberer Reiter | Menüpunkt|
| --- | --- | --- | --- | --- | --- |
| **Artikel** |
| Import Artikelstammdaten | Artkelimport.xls | Artikel | Artikel | egal | Artikel, XLS-Import |
| Import der Artikel Mindest-/Solllagerstands Werte | Artikel_Lagermindest_Sollstand_Import.xls | Artikel | Artikel | egal | Artikel, XLS-Import Lagermindest/-sollstand |
| Import Artikelinventur | Artikelinventur_Import.csv | Artikel | Inventur | Inventurliste | Button CSV-Import |
| Import der Verkaufsstaffelmengen | Artikel_Verkaufsstaffelmengen_Import.xls | Artikel | Artikel | Pflege | VK-Staffelmengen XLS-Import |
| Import von Artikeleigenschaften | Artikel_Eigenschaften_Import.xls | Artikel | egal | Pflege, Eigenschaftenimport XLS. Es müssen dafür Eigenschaften für den Artikel definiert sein|
| Setzen des Gestehungspreises | Artikel_Gestehungspreis_Import.csv | System | Pflege | Pflege | Button Gestehungspreise importieren (nur für Admin) |
| **Stücklisten** |
| Import Stücklistenpositionen, hierarchisch | Stuecklistenpositionenimport.xls | Stücklisten | Stückliste | egal | Stückliste, Import, Positionen |
| Import Stücklistenarbeitsplan, hierarchisch | Stuecklistenarbeitsplanimport.xls | Stücklisten | Stückliste | egal | Stückliste, Import, Arbeitsplan |
| Import PPM Menge in den Arbeitsplan (Beispiel um nur die PPM zu importieren) | Stuecklistearbeitsplan_ppmmenge_import.xls | Stücklisten | Stückliste | egal | Stückliste, Import, Arbeitsplan |
| Import CREO CAD Daten | Stueckliste_CREO_Import.xls | Stücklisten | Stückliste | egal | Stückliste, Import, Creo |
| Stücklisten hierarchisch aus Solid Works übernehmen | Solidworks_Stuecklisten_Import.xls | Stücklisten | Stückliste| egal | Stückliste, Import, Solid-Works-Import. Beachte: der Parameter STRUKTURIERTER_STKLIMPORT muss auf 1 stehen |
| **Partner** |
| Import Partner, Kunden, Lieferanten | Partnerimport.xls | Partner | Partner | egal | Partner, XLS-Import |
| **Lose / Fertigung** |
| Import neuer Lose | Los_Import.xls | Los | Los | egal | Los, Import, XLS-Import |
| Import neuer Termine für bestehende Lose mit Aktualisierung der Auftragsliefertermine | Los_Termin_Import.xls | Los | Los | egal | Los, Import, XLS Los Termin Import |
| Los Istmaterial Import | Los_Istmaterial_Import.csv | Los | Los | Material | Button: Ist-Material Import |
| Los Sollmaterial Import | Los_Sollmaterial_Import.csv | Los | Los | Material | Button: Soll-Material Import |
| Los Geräteseriennummern Import | Los_Geraeteseriennummern_Ablieferungen_Import.csv | Los | Los | Ablieferung, Button: Geräteablieferung per CSV |
| **Finanzbuchhaltung** |
| Muster für Sachkontenimport | Sachkontenimport_Deutschland.csv | Fibu | Sachkonten | egal | Sachkonten Sachkonten importieren |
| **Auftrag** |
| Auftragspositionen mit Positionsterminen | Auftragspositionen_Import.csv | Auftrag | Auftrag | Positionen | Button im oberen Querypanel CSV-Import |
| WooCom
| **Bestellung** |
| Bestellvorschlagspositionen | Bestellvorschlag_Import.xls | Bestellung | Bestellvorschlag | Bestellvorschlag Import |
| ** Eingangsrechnung** |
| Eingangsrechnungen anlegen | Eingangsrechnungen_Import.xls | Eingangsrechnung | Eingangsrechnung | egal | Eingangsrechnung, Import, XLS-Import |
| **Lieferanten** |
| Lieferantenartikeldaten importieren | Lieferantenartikeldaten_Import_ausfuehrlich.csv<br>Lieferantenartikeldaten_Import_kurz.csv | Lieferant | Lieferant | egal | Lieferant, Artikellieferanten importieren |
| **Kunden** |
| KundenSonderkonditionen importieren | KundenSonderkonditionen_Import | Kunde | Kunde | egal Kunde auswählen | Pflege | XLS-Import der Sonderkonditionen |
| **Projekte** |
| Projekteigenschaften importieren | Projekteigenschaften_Import.xls | Projekt | Projekt | egal | Pflege, Projekteigenschaften Import. Beachte: Für das Projekt müssen Eigenschaften definiert sein. |
| Projekt Historyeigenschaften importieren | Projekt_Historyeigenschaften_Import.xls | Projekt | Projekt | egal | Pflege, Projektdetail + Eigenschaftenimport.xls, Beachte: Für die Projekthistory müssen Eigenschaften definiert sein. |
| **Forecast** |
| Forecast Format VAT | Forecast_Import_VAT.xls | Forecast | Forecast | Positionen | Button: Forecastpositionen XLS-Import -> Format VAT |
| Forecast Format Zeiss | Forecast_Import_Zeiss.csv | Forecast | Forecast | Positionen | Button: Forecastpositionen XLS-Import -> Format Zeiss |
| Forecast Import allgemein | Forecast_Import_Positionen.xls | Forecast | Forecast | Positionen | Button: Forecastpositionen XLS-Import -> Format Standard |
| Rollierende Planung | Forecast_Rollierende_Planung.xls |  Forecast | Forecast | Positionen | Button: Forecastpositionen XLS-Import -> Format Rollierende Planung |
| Forecast Format Epsilon | Forecast_Epsilon_Import.xls |  Forecast | Forecast | Positionen | Button: Forecastpositionen XLS-Import -> Format Epsilon |


## Prüf-Dateien
XLS Dateien um verschiedene Ergebnisse prüfen bzw. auch weiter rechnen zu können.

| Zweck| Filename | Modul | unterer Reiter | oberer Reiter | Menüpunkt|
| --- | --- | --- | --- | --- | --- |
| Prüfung der Jahres-UVA vor der Abgabe, Österreich | Jahres_UVA_Oesterreich.ods |
| Prüfung der Jahres-UVA vor der Abgabe, Deutschland | Jahres_UVA_Deutschland.ods |

## Formeln für Stücklisten
Unter *Stuecklisten_Formeln.java* findest du eine textliche Sammlung / Musterbeispiele für die Verwendung von Formeln in deinen Formelstücklisten.