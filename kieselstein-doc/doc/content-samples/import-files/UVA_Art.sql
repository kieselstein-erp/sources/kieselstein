-- Query um für einen neuen Mandanten die UVA Art nachzutragen
-- dabei auf die I_ID achten, die nicht vorhanden sein dürfen
-- und die Mandantennummer (hier '100') entsprechend anpassen.
select max(i_id) from fb_uvaart;
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Vorsteuerkonto',			  20,	113,'060','100',0,0);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('IG Erwerb red.Steuer',		  22,	114,'071','100',0,0);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('IG Erwerb Normalsteuer',	  23,	115,'072','100',0,0);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Vorsteuer betr KFZ',		  30,	125,'027','100',0,0);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Vorsteuer betr Gebaeude',	  31,	126,'028','100',0,0);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Nicht zutreffend',			   1,	119,'---','100',0,0);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Reverse Charge Leistung',	  11,	120,'57->66','100',0,0);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Reverse Charge Bauleistung',100,	128,'48->82','100',0,0);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Reverse Charge Schrott',	 101,	129,'32->89','100',0,0);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('EU Ausland mit UiD',		   6,	111,'017','100',1,1);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Export Drittland',			   7,	112,'011','100',1,1);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Umsatz Inland red.Steuer',	   2,	117,'029','100',1,1);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Umsatz Inland Normalsteuer',  3,	118,'022','100',1,1);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Anzahlung red.Steuer',		  15,	123,'029a','100',1,1);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Anzahlung Normalsteuer',	  16,	124,'022a','100',1,1);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Werbeabgabe',				  32,	127,'WA',  '100',1,1);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Anzahlung EU Ausland mit UiD',102,	130,'017a','100',1,1);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Anzahlung Drittland',		 103,	131,'011a','100',1,1);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Umsatz Reverse Charge',	  10,	121,'021','100',1,1);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Umsatz Inland steuerfrei',	 104,	132,'---','100',1,1);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Import Drittland',			  21,	116,'061','100',0,0);
INSERT INTO public.fb_uvaart (c_nr, i_sort, i_id, c_kennzeichen, mandant_c_nr, b_invertiert, b_keine_auswahl_bei_er) Values('Import Drittland Zahlung an FA',105,133,'083','100',0,0);

-- Und am Schluss die lp_primarykey aktualisieren
update lp_primarykey set i_index = (select max(i_id) from fb_uvaart) where c_name like 'uvaart';
