Allgemeine Definition zur Verwendung des Formelwesens in den Formelstücklisten
deine Kieselstein ERP

Hinweis: Reservierte Worte wie do, while, for usw. dürfen natürlich NICHT für Variablen verwendet werden.
Die Fehlermeldung die dann kommt sagt .... error not a statement

a) Übersicht
Zusatzfunktionsberechtigung muss aktiv sein.
Häkchen in der Stückliste Kopfdaten "mit Formel" muss gesetzt sein. Wenn gesetzt, werden auch Formeln ausgewertet (sofern gesetzt). Ist das Häkchen nicht gesetzt, wird auch nicht ausgewertet. 
Formel kann in der jeweiligen Position (im Reiter Position) eingegeben werden. 
Der Editor ist noch eher unhübsch - weil die Formatierung vernichtet wird. Zum Eingeben der Formeln tut es momentan aber. 
Beim Speichern/Erzeugen der Stücklistenposition wird die jeweils aktuelle Formel übersetzt, also geprüft. Gegebenenfalls kommt eine entsprechende Fehlermeldung.
D.h. gibt es einen Compile-Fehler beim Speichern, hat der Editor immer noch den neuen - vom Anwender geänderten Source-Text. 
Wenn man jetzt dann ein zweites Mal speichert, wird scheinbar fehlerfrei gespeichert, aber eigentlich ist die Formel nicht aktualisiert.
D.h. wen ein COmpilefehler kommt und man andere Dingfe ändern möchte, den Inhalt des Editors über die Zwischenablage in ein Textfile speichern
und mit verwerfen wieder in den Bearbeitenmodus der Stückliste zurückkehren.
Bitte beachte: Wenn Positionen einer Stückliste verändert werden, so können, während der Änderung auch die Parameter nicht geändert werden, auch nicht über einen zweiten Client.


Im Reiter Parameter können die in dieser Stückliste vom Benutzer abzufragenden Parameter hinterlegt werden.
Im Menü Bearbeiten gibt es einen zusätzlichen Menüpunkt "Konfigurieren", mit dem der 
Anwender die definierten Parameter erfassen kann. Mittels klick auf OK wird dann eine Gesamtkalkulation der Stückliste durchgeführt.
Dafür die Reportvariante mit Formeln auswählen, gegebenenfalls einrichten.

b) Details zu den Formeln / Arbeitsweise in der Stücklistenposition
Im einfachsten Falle ist das soetwas wie: "return new BigDecimal(getMenge().multiply(new BigDecimal("3")));" um die in der Position vorhandene Menge mit 3 zu multiplizieren.
Die Methode (für die Positionen) muss immer(!) einen BigDecimal zurückgeben. Dieser wird als Positionsmenge verwendet.

java.lang und java.math sind implizit vorhanden (importiert). Alle anderen Pakete müssen mit dem kompletten Klassennamen aufgerufen werden.
Z.B. BigDecimal pi = com.lp.util.Helper.rundeKaufmaennisch(new BigDecimal("3.141592"));

Der Zugriff auf Parameter ist mittels "$P{Parametername}" möglich. Zum Zeitpunkt des
compiles wird dann zuerst gesucht, ob diese/aktuelle(!) Stückliste den Parameter kennt.
Übersetzt wird das ganze in eine Art "(Datentyp) getParam(String parametername)". 
D.h. wenn man einen Parameter "Laenge" mit java.math.BigDecimal. definiert hat, und 
dann ".... $P{Laenge}.multiply(BigDecimal.TEN) " macht (also die Länge mit 10 multiplizieren), 
wird intern " ... ((java.math.BigDecimal)getParam("Laenge")).multiply(BigDecimal.TEN) " daraus. 
Parameter können nicht verändert werden. Das bedeutet auch, dass wenn in der 
Kopfstückliste ein Parameter definiert ist, muss dieser auch(!) in der Unterstückliste 
definiert (Reiter Parameter) werden, sofern in der Unterstückliste darauf zugegriffen 
wird (was der compile ja feststellt). Zum Zeitpunkt des Compilierens der Unterstückliste 
kennt die ja nicht die aufrufende Stückliste.

c.) Methoden um auf Felder / Werte der zu berechnenden Stücklistenposition zugreifen zu können:
    a) Dimension1 auslesen/setzen
    - Double getDimension1()
    - void setDimension1(Double value) 

    b) Dimension2 auslesen/setzen
    - Double getDimension2()
    - void setDimension2(Double value)

    c) Dimension3 auslesen/setzen
    - Double getDimension3()
    - void setDimension3(Double value)

    d) Mengeneinheit der Position
    - String getEinheitCnr()
    - void setEinheitCnr(String einheit)

    e) Menge der Position
    - BigDecimal getMenge()
    - void setMenge(BigDecimal menge)
	
	f) Setze Artikel
	- setArtikel($P{Artikel});
	  $P{Artikel} ist jener Parameter, der zu Beginn des Konfigurieren beim Anwender
	  abgefragt wird. Ist derzeit nur in der Stücklistenposition implementiert.

d.) Zusätzlich Funktionen, damit man ins Logfile des Servers schreiben kann:
    a) debug(String message) um eine Nachricht mit Priorität DEBUG ins log zu schreiben
    b) info(String message) um eine INFO Message ins log zu bekommen
    c) warn(String message) für eine WARN Message
    d) error(String message) für eine ERROR Message

	Meldungen die an den Report der Gesamtkalkulation übergeben werden können.
	- report.debug("Nachricht...");
	- report.info("Nachricht...");
	- report.warn("Nachricht...");
	- report.error("Nachricht...");
	Ein mittels "report.debug("wichtige DebugInformation")" erstellter Text landet im
	Report im Feld "ReportDebug". Analog landet "report.info(...)" im Feld "ReportInfo", "report.warn()" im Feld "ReportWarn" und "report.error(...)" im Feld "ReportError".
	Die Felder "Report(Debug|Info|Warn|Error)" sind vom Datentyp java.lang.String. 
	Die Methoden report.debug(...) nehmen ebenfalls einen String entgegen. 
	Die gleichen report.xxx Methoden können innerhalb einer Formel mehrfach aufgerufen
	werden. Jede einzelne Nachricht wird gespeichert. Bei der Übergabe an den Report
	werden dann - für den Fall, dass mehrere gleichartige Nachrichten in der Formel
	ausgegeben worden sind, die Einzelnachrichten mittels "\r\n" miteinander verkettet.
	Das Report-Feld enthält null, wenn keine Nachricht dieser Priorität in der Formel ausgegeben wurde.

	
    Wenn die "Gesamtkalkulation" aufgerufen wird und es sich um eine Formelstückliste 
	handelt, wird am Beginn der jeweiligen Stückliste diese "kompiliert". Hat sich die 
	Klasse ansich nicht geändert, wird die alte/bekannte instanziert. Wenn sie sich 
	geändert hat, wird die neue Klasse geladen und instanziert.


e.) Formeln im Arbeitsplan:
Formelrückgabewert ist java.lang.Long(!) (im Gegensatz zur Stücklistenposition, wo es java.math.BigDecimal ist).
Es wird die Stückzeit in ms (Millisekunden) zurückgegeben.
Es können folgende Werte gesetzt werden:
e1.) Stückzeit lesen/schreiben
- Long getStueckzeit() 
- void setStueckzeit(Long stueckzeit)
e2.) Rüstzeit lesen/schreiben
- Long getRuestzeit()
- void setRuestzeit(Long ruestzeit)

f.) Fehler die trotz Prüfung zur Laufzeit auftreten können:
Je nach Content der Formel kann es sein, dass die Prüfung beim Speichern keinen Fehler ergibt, aber beim Aufruf während der Berechnung ein entsprechender Fehler auftritt.
Das kann z.B. dann auftreten wenn die Rückgabewerte nicht richtig deklariert sind (Long anstatt BigDecimal usw.)
Derzeit bekommen wir bei der Interpretation der Formel zur Laufzeit zu wenig Infos um eine gute sprechende Fehlermeldung ausgeben zu können.
D.h. die Fehlersuche kann mühsam werden. Ändern Sie Ihre Stückliste daher Schritt für Schritt (so wie beim Anpassen von komplexen Formularen auch)
	
g.) Punkte auf die man gerne reinfällt und nicht so schnell findet:
g1.) Eine Variable wird deklariert aber nicht initialisiert (gesetzt) und dann damit gerechnet
	Bitte in jedem Falle initialisieren, sonst kommt ein Compiler Fehler.
g2.) Wenn das Speichern = Compilieren geht, aber par too die Gesamtkalkulation doch immer wieder Fehler meldet
	Dann doch mal den Kieselstein ERP Server neu starten (der Client kann da ruhig offen bleiben)

h.) es gehen auch die "iReport" If-Konstrukte. D.h. z.B.
	double lh = $P{LH}.doubleValue();
	lh == null ? return das eine : return das andere
	usw.

i.)	Es gibt jetzt zusätzlich 3 Methoden um mit "globalen Variablen" arbeiten zu können:
i1.) setVar(String variablenname, Object wert)
	Zum Setzen einer Variable mit einem bestimmten Wert.
	Beispiel: setVar("Gewicht", new BigDecimal("25.89"));
i2.) getVar(String variablenname)
	Zum Ermitteln des Werts der Variable
	Beispiel: BigDecimal gewicht = (BigDecimal) getVar("Gewicht");
i3.) existsVar(String variablenname)
	Zum Überprüfen, ob die Variable existiert
	Beispiel: if(existsVar("Gewicht")) {
	info("Es wurde das Gewicht '" + 
		((BigDecimal)getVar("Gewicht)).toPlainString() + "' erfasst.");
  In welcher (Stücklisten) Ebene die Variable gesetzt wird, ist dabei egal.
  Auch in welcher Ebene die Variable gelesen wird. Ein sinnvoller Wert (also != null)
  kann natürlich nur gelesen werden, wenn die Variable zuerst gesetzt wird.

j.) Übersteuern der erzeugten Kopf-Stücklisten-Artikelnummer und der Artikelnummer
Es geht darum, dass aus der Formel strukturierte Stücklisten und dazugehörende "Einkaufs-"Artikel angelegt werden können.
Dafür gibt es:
j1.) setUebergeordneteArtikelCnr(String artikelnummer);
Bedeutet wenn auf Produktionsstückliste erzeugen geklickt wird, wird die soeben erzeugte Produktionsstückliste auf diese CNr gesetzt
Es darf diese Funktion nur einmal je Stückliste(nebene) verwendet werden
j2.) setArtikelCnr(String artikelnummer);
Bedeutet, dass der Artikel der bearbeiteten Stücklistenposition (also der Artikel der hinter der Zeile mit der Formel liegt)
auf die (neue) Artikelnummer kopiert wird.
j3.) generiereUebergeordneteArtikelCnr(artnr+"0000"); 	// und GENERIERE_ARTIKELNUMMER_ZIFFERNBLOCK = 1 !!

 
k.) Es können, da der gesamte Java Syntax zur Verfügung steht auch Schleifen usw. eingebaut werden
Damit sind auch unendliche Schleifen möglich, welche zum Stillstand des Kieselstein ERP Server führen können.
Daher mit Hirn verwenden.

// === Eine while-Schleife
 BigDecimal laenge = $P{LL};

  /* Vielfache von Zehn ermitteln */
 int l = laenge.intValue();
 int zehner = 0;

 while(l > 0) {
     zehner++;
     l -= 10; /* alternativ l = l - 10; */
 }

 setMenge(new BigDecimal(zehner));
 return BigDecimal.ZERO;


// === Eine while-Schleife mit vorzeitigem Ende
 BigDecimal laenge = $P{LL};

  /* Vielfache von Zehn ermitteln */
 int l = laenge.intValue();
 int zehner = 0;

 while(l > 0) {
     zehner++;
     l -= 10; 

     if(l < 500) {
        break;
    }
 }

 setMenge(new BigDecimal(zehner));

 return BigDecimal.ZERO;

// === Eine For-Schleife mit 10er Inkrement
 BigDecimal laenge = $P{LL};

  /* Vielfache von Zehn ermitteln */
 int lMax = laenge.intValue();
 int zehner = 0;
 for(int l = 0; l < lMax; l += 10) {
     zehner++;
 }

 setMenge(new BigDecimal(zehner));

 return BigDecimal.ZERO;

// === Eine einfache For-Schleife zaehlt nach oben
 BigDecimal laenge = $P{LL};

  /* Simples nach oben durchzaehlen */
 int lMax = laenge.intValue();
 int zaehler = 0;
 for(int l = 0; l < lMax; l++) {
     zaehler++;
 }

 setMenge(new BigDecimal(zaehler));

 return BigDecimal.ZERO;

// === Eine einfache For-Schleife zaehlt nach unten
 BigDecimal laenge = $P{LL};

  /* Simples nach oben durchzaehlen */
 int lMax = laenge.intValue();
 int zaehler = 0;
 for(int l = lMax; l >= 0; l--) {
     zaehler++;
 }

 setMenge(new BigDecimal(zaehler));

 return BigDecimal.ZERO;





	
Formelbeispiele	
/* Prüfen ob Parameter Laenge gesetzt, falls größer 100 wird 3 zurückgegeben, ansonsten 2 */
    if($P{Laenge} == null) {
      return BigDecimal.ONE;
    }

    if($P{Laenge}.compareTo(new BigDecimal("100")) > 0) {
      return new BigDecimal("3");
    } else {
      return new BigDecimal("2");
    }

/* Die Anzahl der Klemmen mit 1,26500 multiplizieren: */
    if($P{Klemmen} == null) {
      return new BigDecimal("0.265000");
    }
    java.math.BigDecimal d = $P{Klemmen};
    return d.multiply(newBigDecimal("1.265000"));

/* Die Einheit auf mm setzen, die Menge auswerten, mit 1000 multiplizieren und 12460mm dazuzählen (x = 12460 + n * 1000) */
    setEinheitCnr("mm");
    return new BigDecimal("12460").add(getMenge().movePointRight(3));

/* weitere bisher genutzte Formeln */

/* Spannsatz 2 Stk. pro Seite nötig / pro Spannsatz 1 Stk.*/
Integer EinZwei=$P{EZ}; 
return new BigDecimal(EinZwei.intValue());

/* Winkelleiste für Anlenkung links, bei beidseitig 2Stk */
Integer EinZwei=$P{EZ};
if (EinZwei.intValue() == 2) {
	return new BigDecimal(2);  
}
return new BigDecimal(0);

/* Schwenkarm zusammengebaut, 2 Stk. pro Seite nötig */
Integer EinZwei=$P{EZ}; 
return new BigDecimal(EinZwei.intValue() * 2);

/* ein Scheuerblech pro Eck, d.h. 2 Stk. pro Seite */ 
Integer EinZwei=$P{EZ}; 
return new BigDecimal(EinZwei.intValue() * 2);

/* Löffelscharnier, ab 7000mm LL 1 Stk. nötig */
BigDecimal ll = $P{LL};
if(ll.compareTo(new BigDecimal(7000))>=0) {
	return (new BigDecimal(1));
}
return(new BigDecimal(0));

/* Scharnierbolzen unten, 1 Stk. á Meter LL */
BigDecimal ll = $P{LL};
int anzahl;
anzahl = ll.intValue() / 1000;
return new BigDecimal(anzahl);

/* Aufkleber 1 Stk. pro Wand / 1 Stk. = eins. 2Stk. = beidseitig */
Integer EinZwei=$P{EZ}; 
return new BigDecimal(EinZwei.intValue());

/* Schraublösung einseitig */
Integer EinZwei=$P{EZ};
if (EinZwei.intValue() == 1) {
	return new BigDecimal(1);  
}
return new BigDecimal(0);

/* Schraublösung beidseitig */
Integer EinZwei=$P{EZ};
if (EinZwei.intValue() == 2) {
	return new BigDecimal(1);  
}
return new BigDecimal(0);

/* Hydraulikaggregat beidseitig */
Integer EinZwei=$P{EZ};
if (EinZwei.intValue() == 2) {
	return new BigDecimal(1);  
}
return new BigDecimal(0);

/* Hydraulikaggregat einseitig */
Integer EinZwei=$P{EZ};
if (EinZwei.intValue() == 1) {
	return new BigDecimal(1);  
}
return new BigDecimal(0);

/* weitere */
BigDecimal lh = $P{LH}; 
return (lh.divide(new BigDecimal(100)));

BigDecimal ll = $P{LL};
return (ll.multiply(new BigDecimal(0.001)).multiply(new BigDecimal(4.0))).add(new BigDecimal(5));

BigDecimal ll = $P{LL};
if(ll.compareTo(new BigDecimal(1000))>0) {
	return (new BigDecimal(1));
}
return(new BigDecimal(0.5)); 

/* Abhängig eines Stringinhaltes was berechnen */
String af = $P{AF};
BigDecimal m;
if (af.toUpperCase().contains("ALU")) {
	m = new BigDecimal(1.00);
} else {
	m = new BigDecimal(0.00);
}
return m;

/* Abhängig von einer Länge eine Stückzahl über int herausrechnen */
BigDecimal ll = $P{LL};
int anzahl;
anzahl = ll.intValue() / 1000;
anzahl += 4;
return new BigDecimal(anzahl); 

/* bei einem Eindimensionalen Artikel die Breite (=Dimension1) setzen und die Menge gleich belassen */
BigDecimal ll = $P{LL};
BigDecimal mng = getMenge();
Double d1 = new Double ((ll.doubleValue() - 175) / 1000);
setDimension1( d1 );
return (mng);

/* oder auch */
BigDecimal ll = $P{LL};
double l;
l = (ll.doubleValue()-175)/1000;
setDimension1(new Double(l));
return getMenge();

Formeln im Arbeitsplan
/* im Arbeitsplan muss die Ruestzeit als Long gesetzt werden
   Die Stückzeit muss als Long zurückgegeben werden
   sowohl Stückzeit wie Rüstzeit sind in MilliSekunden */
BigDecimal ll = $P{LL};
long x;
x = ll.longValue() * 1000;
setRuestzeit(new Long(x) );
return getStueckzeit();

/* je 1000mm 10Minuten Rüstzeit 
   im Arbeitsplan muss die Ruestzeit als Long zurückgegeben werden
   Die Stückzeit muss immer und als Long zurücgegeben werden
  sowohl Stückzeit wie Rüstzeit sind in MilliSekunden
  D.h. 1000 = 1 Sekunde
          60.000 = 1 Minute
          3600.000 = 1 Std.
long = langer int(eger)
double = fließkomma ähnlich float
Es muss sowohl Stück wie Rüstzeit explizit gesetzt werden */

BigDecimal ll = $P{LL};
int anzahl;
long rz;
long sz;
anzahl = ll.intValue() / 1000; /* das sind die Meter */
rz = anzahl * 10 * 60000;
sz = 0;
/* setStueckzeit(new Long(0) );	muss ich diese extra nullsetzen */
setRuestzeit(new Long(rz) );
return (new Long(sz)); /* Das ist auch die Stückzeit, die gesetzt werden muss */

// ---------------------
// Fehler auf die man gerne reinfällt
// man verwendet eine Variable und die ist noch nicht initialisiert, dann kommt beim
//   Compilieren kein Fehler aber bei der Ausführung null / Methode nicht (initialisiert oder so ähnlich)
//   und es ist eigentlich der Fehler unlogisch
// Man macht einen SQLEXECUTE und der DB Zugriff geht nicht, weil Anwender Parameter auf die DB falsch
//   Es kommt auch der Ausführungsfehler (Methode nicht initialisiert)


// --------------- Prüfen auf Formelinhalte mittels SQL Query
select ww_artikel.c_nr, x_formel, * from stk_stuecklisteposition
inner join stk_stueckliste on stk_stuecklisteposition.stueckliste_i_id=stk_stueckliste.i_id
inner join ww_artikel on ww_artikel.i_id=stk_stueckliste.artikel_i_id
where x_formel like '%EIN%';

// --- per SQl Query auf DB Felder zugreifen
Sowohl in Formeln für die Stuecklistenpositionen als auch Arbeitsplan(positionen) stehen nun zusätzlich zur Verfügung:
Object sqlExecute(String sql);
Object[] sqlExecutes(String sql);	// Hinweis: Das sqlExecutes nur verwenden wenn wirklich mehrere Felder erwartet werden.
sqlExecute liefert - wie P_SQLEXEC.execute() aus den Reports - genau eine Spalte einer Zeile zurück.
sqlExecutes liefert - wie P_SQLEXEC.executes() aus den Reports - mehrere Spalten einer Zeile zurück.

Beispiel:
// === sqlExecute im Script
/* Auf Basis der Artikel-Id möchte ich die C_NR ermitteln */
Integer artikelId = getArtikel().getI_id();
String cnr = (String) sqlExecute("SELECT C_NR FROM WW_ARTIKEL WHERE I_ID = " + artikelId.toString() + ";");

// ===
Hinweis: sqlExecute liefert Daten vom Typ Object. Da wir wissen, dass in der C_NR in 
der Tabelle ein "String" (character varying(25)) ist, können/müssen wir dieses "Object" in einen "String" casten,
was mit "(String)" passiert. Vergisst man das, wird mit hoher Wahrscheinlichkeit zwar
der compile des Scripts noch korrekt sein, aber beim "Konfigurieren" (Stückliste::Bearbeiten::Konfigurieren) 
gibt es einen Fehler bezüglich "Datentypen sind nicht kompatibel", weil der Compiler
hier beim Erstellen der kompletten Klasse feststellt, dass sqlExecute nur Object
liefern kann, aber die Zuweisung an eine Variable vom Typ String erfolgt.

Natürlich können innerhalb des Scripts mehrere sqlExecute(s)() durchgeführt werden.
Sie werden genau in der Reihenfolge abgearbeitet, wie sie im Script stehen.

// === sqlExecutes im Script
/* Auf Basis der Artikel-Id möchte ich das Artikelgewicht und das Materialgewicht ermitteln */
Integer artikelId = getArtikel().getI_id();
Object[] o = sqlExecutes("SELECT C_NR, F_GEWICHTKG, F_MATERIALGEWICHT FROM WW_ARTIKEL " +
  "WHERE I_ID = " + artikelId.toString() + ";");
String cnr = (String) o[0];
Double artikelgewicht = (Double) o[1];
Double materialgewicht = (Double) o[2];

warn("Artikelgewicht in kg: " + (artikelgewicht == null ? "keines angegeben":  artikelgewicht.toString()));



/* Berechnen der AZ: Durchmesser oben + Durchmesser unten x 3,2 = x Meter zum Ritzen
   AZ 1 Meter ritzen = 30 sec
*/

BigDecimal d = $P{D};	// in mm!!
long sz;
BigDecimal l;	// Drahtlänge
BigDecimal z;	// Zeit für das ritzen in Sekunden
// BigDecimal zms;	// Zeit für das ritzen in ms

l = d.add(d);
l = l.multiply(new BigDecimal(3.2));
z = l.multiply(new BigDecimal(30.0));
// zms = z.multiply(new BigDecimal(1000)); da in mm erhalten nicht umrechnen

sz = z.longValue();

return (new Long(sz));


/* AG25  AZ Stoff auf Folie 2500 kaschieren
   Rüstzeit 5 min
   (AZ = 16 min / m²)
   z.B.: 
   0,384 m2 x 16 min = 6,144 min = 6 min 9 sec
   Aufkaschierten Stoff ausschneiden
   Umfang oben+unten + 2x Seitenlänge = m x 1 min
*/

long rz;
rz = 5 * 60 * 1000;
setRuestzeit(new Long(rz));


BigDecimal d = $P{D};
BigDecimal u;	// Umfang in m
u = d.multiply(new BigDecimal(3.2)).divide(new BigDecimal(1000));
BigDecimal h = $P{H}; // ist in mm!!
BigDecimal f;	// Fläche in m²
f = u.multiply(h).divide(new BigDecimal(1000));
BigDecimal z;	// Zeit in Sekunden
z = f.multiply(new BigDecimal(16)).multiply(new BigDecimal(60));

long sz;	// Stückzeit in ms
sz = z.multiply(new BigDecimal(1000)).longValue();

return (new Long(sz));

// Runden:
.setScale(4,BigDecimal.ROUND_HALF_EVEN)

// ACHTUNG: Berechnung der Teilung für die nachfolgenden Positionen

BigDecimal ll = $P{LL};
int felder;

if (ll.doubleValue() <= 5000)		felder = 2;
elseif (ll.doubleValue() <= 7300)		felder = 3;
elseif (ll.doubleValue() <= 9000)		felder = 4;
elseif (ll.doubleValue() <= 11500)	felder = 5;
else			felder = 6;

setVar("Felder", felder);

return BigDecimal.ZERO;


// alte Definition war (LH/2)-315 was nur bedingt stimmte
// Berechnung der Anzahl der oberen Einfassprofile
// Die Länge hängt von der Anzahl der Zwischenprofile ab und plus 2x30mm
// Es wird die Anzahl der hohe für unten und oben benötigt
// Für jedes Feld ein Satz plus dann dem Rand zusätzlich, also Felder +1

int felder  = ((Integer) getVar("Felder")).intValue();
int aoh = ((Integer) getVar("aoh")).intValue();
int aon = ((Integer) getVar("aon")).intValue();

// report.debug("Felder= "+felder);

double m;
m = (double) (felder +1) * 2;

// Länge aoh * 200 + aon * 150 + 60
Double d1;
//d1 = new Double (aoh * 200 + aon * 150 + 60);
setDimension1( d1 );

return new BigDecimal(m);

if (!(spmat.startsWith("Verbund 0,6mm")))	{	return BigDecimal.ONE;	}


// LH -100 als Höhe der Platte Breite immer 2460mm

BigDecimal lh = $P{LH};

setDimension1(new Double(2460) );	// Breite
// setDimension2(new Double(lh.doubleValue() - 100) );	// Höhe
BigDecimal menge = getMenge();

return menge;

-----------------------------
// LH -100 als Höhe der Platte Breite immer 2460mm
// nur wenn Stirnportal Material Verbund 1,2mm Aludeck|Verbund 0,8mm Aludeck|Verbund 0,6mm Stahldeck

BigDecimal lh = $P{LH};
String spmat=$P{SPMAT};	// SPMAT

if (!(spmat.startsWith("Verbund 0,6mm")))	{	return BigDecimal.ONE;	}

Double d1 = new Double (2460.0);
setDimension1( d1 );

Double d2 = new Double (lh.doubleValue() - 100);
setDimension2( d2 );

BigDecimal menge = getMenge();

return menge;