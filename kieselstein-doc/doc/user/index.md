# Using Kieselstein

## Local Installation

The easiest way to run kieselstein is to download the full distribution pack 'distpack' (`kieselstein-dispack-<version>.tar.gz) for the
[latest release](https://gitlab.com/kieselstein-erp/sources/kieselstein/-/releases/permalink/latest).

NOTE: Be aware that this package is relatively big (~650 MB). Therefore, you might want to use a fast internet connection.

After the download, unpack it to the folder of your choice. The package contains one single folder called `dist`. This
folder is intended to be fully replaced whenever a new release of kieselstein is installed. Therefore, any change which
is specific to your installation, should not be done in there.

Kieselstein also requires some space to store data and configuration files. For this another folder should be created.
This folder is in the following called the `data` folder.

Despite being planned, there is currently no fancy configuration mechanism in place. Therefor, for the moment we recommend
to copy the over the startup scripts from `dist/bin` into a new folder `data/bin` and edit them there according to your needs.

NOTE: Unfortunately, it is not guaranteed that the startup scripts stay the same in the future, so we highly recommend
to manually compare your local scripts with the new scripts from each release, to spot potential changes. In the future,
we aim to put in place a more comfortable mechanism for customizing your setup.

Most of the provided scripts need 2 configuration variables to be set. This can either be achieved by setting evironment
variables or by setting them in your customized scripts. The currently required ones are:
* `KIESELSTEIN_DIST` shall contain the absolute path to the unpacked `dist` folder,
* `KIESELSTEIN_DATA` shall contain the absolute path to the `data` folder created by you.

The folder structure below these 2 folders follow certain conventions. Thus do not change the names of these folders!

A setup could for example look like this:

```
/opt/kieselstein/
      |-- dist/
      |     |-- bin/
      |     |-- conf/
      |     |-- apache-tomcat-<tomcat-version>/
      |     |-- wildfly-<wildfly-version>/
      |     |-- clients/
      |     |-- bootstrap/
      |
      |-- data/
            |-- bin/
            |-- conf/
```

The environment variables would in this case point to:

Windows:
```bat
set KIESELSTEIN_DIST=/opt/kieselstein/dist
set KIESELSTEIN_DATA=/opt/kieselstein/data
```

### bootstrapping database

When installing Kieselstein the first time, a database has to be created.
This can be achieved using the scripts contained in the folder `dist/bootstrap/database`.

NOTE: The scripts have to be executed from within with their parent folder and the postgres commands (`psql`) have
to be in the path.

Windows:
```bat
cd <path to dist dir>/bootstrap/database
./createDb.bat
```

Linux:
```bash
cd <path to dist dir>/bootstrap/database
./createDb.sh
```

After this step, there are 2 options to continue:

#### Fill the database with basic data

Windows:
```bat
cd <path to dist dir>/bootstrap/database
./fillDb.bat
```

Linux:
```bash
cd <path to dist dir>/bootstrap/database
./fillDb.sh
```

#### restore a backed up database from a compatible system

(again we assume psql is in the path)

Linux:
```bash
cat <backup-file> | psql -h localhost -U postgres -d 'KIESELSTEIN'
```

## Kieselstein-ERP Komplettinstallation
Was ist alles zu tun, um von null weg ein Kieselstein-ERP System ans Laufen zu bekommen.

Nachfolgend eine sehr kompakte Beschreibung. Wir setzen gute Systemkenntnisse des / der Installierenden voraus.

Siehe dazu auch [Weiterführende Dokumentation](http://docs.kieselstein-erp.org/docs/installation/01_server/).

Windows
=======

Kurzbeschreibung der Installation eines Kieselstein-ERP-Servers von einer frischen Windows-Installation.<br>
**Hinweis**, insbesondere für Linux Sysadmins<br>
Die Einrichtung des Betriebssystems sollte in der Timezone erfolgen in der die Installation erfolgt.

### Werkzeuge die wir unter Windows gerne verwenden
1. Firefox (https://www.mozilla.org/de/firefox/new/) oder Google Chrome
2. TotalCommander von Ghilser siehe https://www.ghisler.com/ddownload.htm
3. Libre Office siehe https://de.libreoffice.org/download/download/
4. Acrobate Reader von Adobe siehe https://get.adobe.com/de/reader/
5. Notepad++ siehe https://notepad-plus-plus.org/downloads/
6. 7Zip siehe https://www.7-zip.org/download.html muss installiert werden

### Installation Java
Zulu Java Version 8 mit FX Extension von https://www.azul.com/downloads-new/?version=java-8-lts&os=windows&package=jdk-fx#zulu<br>
D.h. beim Package JDK FX auswählen.
- Auswahl 64Bit, MSI
- Java Home aktivieren

**Nur** für den Client kann auch Java 11 LTS mit FX ausgewählt werden. Natürlich für das jeweilige Betriebssystem.

### PostgresQL 14
- Download von https://www.postgresql.org/download/ bzw. https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
- nur PostgresQL und Command Line Tools installieren
- Einrichten mit User postgres, PW: postgres
- Einstellen pg_hba.conf "c:\Program Files\PostgreSQL\14\data\pg_hba.conf"
>		host    all             all             127.0.0.1/32        md5
>		host    all             all             192.168.xx.1/24		md5
- Einstellen postgresql.conf "c:\Program Files\PostgreSQL\14\data\postgresql.conf" 
>      listen_addresses = '*'		# what IP address(es) to listen on;	-->> ev. beschränken auf localhost oder ...
>      datestyle = 'iso, dmy'

**ACHTUNG:** Die jeweiligen postgresql.conf default-Einstellungen sind für jedes Betriebssystem anderes. Meine Empfehlung, jedes Mal prüfen.

### PgAdmin aktuelle Version
Download von https://www.pgadmin.org/download/pgadmin-4-windows/

### Installation Kieselstein
Ist schon fast eine Update-Beschreibung und kommt ohne Git-Bash aus. In anderen Worten, das kann sich JEDEr herunterladen und nutzen.
1. bestehende dist und data Verzeichnisse sichern.<br>
Es geht hier vor allem um die Reports. Auch sollten die Log-Dateien in ein Backup verschoben werden um so mit einer neuen Umgebung starten zu können.
2. Holen der aktuellen Version
https://gitlab.com/kieselstein-erp/sources/kieselstein<br>
Hier latest Release wählen
![](latest_release.png)  <br>
und das gesamte Paket herunterladen
![](download_dist_paket.png)  <br>
Dieses Paket entpacken (doppelt gepackt) und auf das Ziellaufwerk unter ?:\kieselstein\dist speichern.<br>
Zusätzlich auch das Verzeichnis ?:\kieselstein\data anlegen.
![](entpacken.png)  
3. Path und Environment ergänzen<br>
Start, System, erweiterte Systemeinstellungen, Umgebungsvariablen
- zu Path den Path zu den Bin des Postgres hinzufügen (sodass psql.exe gefunden wird)
  - c:\Program Files\PostgreSQL\14\bin\
- Systemvariablen hinzufügen
  - KIESELSTEIN_DIST=?:\Kieselstein\dist
  - KIESELSTEIN_DATA=?:\Kieselstein\data
4. Erzeugen der aktuellen leeren Datenbank<br>
**ACHTUNG:** Bei einem eventuellen Update ist eine erweiterte Vorgehensweise erforderlich. Diese wird ab Vorhandensein eines Updatemanagers entsprechend beschrieben.
- ?:\Kieselstein\dist\bootstrap\database\createDb.bat ausführen. Es erfolgt 4x die Abfrage nach dem postgres Passwort
- ?:\Kieselstein\dist\bootstrap\database\fillDb.bat ausführen. Es erfolgt 1x die Abfrage nach dem postgres Passwort
5. Testen ob der Wildfly startet<br>
?:\Kieselstein\dist\bin\launch-kieselstein-main-server.bat
Nach einigen Sekunden, 1-2 Minuten, je nach Leistungsfähigkeit des Servers müssen unter ?:\Kieselstein\dist\wildfly-12.0.0.Final\standalone\deployments\ für die drei Dateien auch die .deployed Einträge vorhanden sein. Sollte es .failed Einträge geben, so sieht man in diesen, oder im Server.log mögliche Ursachen.
6. Starten des Clients<br>
Es muss ein Client-Verzeichnis eingerichtet werden. D.h. dieses liegt üblicherweise unter ?:\Kieselstein\client. Hier die Dateien aus 
?:\Kieselstein\dist\clients\kieselstein-ui-swing-0.0.6.tar.gz\kieselstein-ui-swing-0.0.6.tar
also das bin und das lib Verzeichnis einkopieren.<br>
6.a. Direkt am Server<br>
Aus dem  ?:\Kieselstein\dist\client\bin das kieselstein-ui-swing.bat bzw. das kieselstein-ui-swing starten (für Linux die Ausführen Berechtigung setzen)<br>
6.b. für weitere Clients zur Verfügung stellen.<br>
In den kieselstein-ui-swing.bat bzw. das kieselstein-ui-swing (aus dem Verzeichnis ?:\Kieselstein\dist\client\bin) anstatt localhost die IP-Adresse des Servers eintragen.<br>
Beide Verzeichnisse z.B. als Client in ein Zip File und dieses unter ?:\Kieselstein\dist\clients zur Verfügung stellen. Ab der Version 0.0.11 stehen die Files in diesem Verzeichnis im Browser unter http://kieselstein_ERP_SERVER:8080 zur Verfügung. Bewährt hat sich auch hier die für die Clients benötigten Javaversionen zur Verfügung zu stellen (MAC, Linuxe, Windows)<br>
6.c. Anmelden<br>
Nach diesen Änderungen kann der Client durch Aufruf von ?:\Kieselstein\client\bin\kieselstein-ui-swing.bat gestartet werden.
Beim Anmelde-Dialog mit Admin, admin und Sprache Deutsch Österreich anmelden. Danach bekommst du die verfügbaren Module angezeigt.<br>
Funktioniert auch das, ist die Installation für den ersten Blick vollständig.

7. Einrichten als Service inkl. RestAPI
Ist obiges erfolgreich, so Client und Server, beenden und die beiden Module als Service einrichten.<br>
Dazu <u>nicht als Administrator</u> ?:\Kieselstein\dist\bootstrap\service\windows\install-kieselstein-services.bat ausführen.
Anschließende über die Dienste / Services die beiden Kieselstein-Dienste starten.
Nach erfolgtem Start, für den Wildfly kann gerne auf deployed geprüft werden, den Client starten. Zusätzlich sollte die [RestAPI](https://kieselstein-erp.gitlab.io/user-doc/docs/stammdaten/system/web_links/rest_api/) geprüft werden.

### Prüfen der Datenbank auf Unterschiede
Wenn eine bestehende Datenbank gegeben ist, muss diese, bei neuen Kieselstein-ERP Datenbankversionen aktualisiert werden.<br>
So kann mit dem PGAdmin, Version 6.20 die Tabellenstruktur vergleichen werden.
- Menü Tools, Schema Diff
	
### Update der Reports
Einspielen der aktuellen Reports aus dem Backup. Hier müssen die jeweiligen Anwenderverzeichnisse einkopiert werden.

### Tipps und Tricks
- Sollte die Dokumentenablage spinnen / nicht starten, den Wildfly stoppen und die Verzeichnisse: data, logp, tmp und jackrabbit komplett löschen.
- Anpassung des Client-Starts auf die gewünschte Sprache.<br>In der kieselstein-ui-swing.bat nach dem localhost/der IP-Adresse steht auch die Sprachdefinition, "-Dloc=de_AT". Hier die gewünschte Startsprache eintragen. Es stehen zusätzlich, de_DE, de_CH, en_US, en_GB (ist nicht übersetzt), it_IT (ist aktuell größtenteil in Englisch), pl_PL zur Verfügung.
- Log-Levels einstellen, Log-File-Größe reduzieren<br>
im ?:\Kieselstein\dist\wildfly-12.0.0.Final\standalone\configuration\kieselstein-standalone.xml<br>
logger category="org.jboss.as.config"><br>
\<level name="DEBUG"/><br>
Von DEBUG auf WARN umstellen, auch den INFO auf WARN
- Das Clientlog ist auf c:\Users\user_name\.kse\Default\log\lpclient_ zu finden.

Linux
=====
Die Linux Installation ist ähnlich der oben beschriebenen Vorgehensweise.<br>
Üblich ist, dass das kieselstein-Verzeichnis unter /opt/kieselstein angelegt wird. Alle weiteren Schritte sind sehr ähnlich, es sind anstelle der .bat Dateien auch immer die . Command-Script Dateien (ohne Extension) aufgeführt.<br>
Es muss unbedingt darauf geachtet werden, dass die Postgres und Java Versionen zum jeweiligen Linux Derivat passen und<br>
beachte, dass die Postgres-Konfiguration für jede Linux Distribution anders ist, also vermutlich wie oben beschrieben einstellen / anzupassen ist.

Client-Installation
===================
Für eine passende Client-Installation auf dem Client das dazugehörende Java installieren (wie oben beschrieben, gerne auch Java11) und aus dem Server-Clientverzeichnis das ?:\Kieselstein\client auf den lokalen Rechner kopieren. Üblicherweise unter c:\\. Den Bath / das Shell-Script starten, los gehts.

Backup
======
Richte ein entsprechendes tägliches, stündliches Backup ein. Für gespiegelte Datenbankserver kannst du dich gerne direkt an uns wenden.

Abschluss
=========

Damit ist die Installation deines **Kieselstein-ERP** Systems abgeschlossen. Alle weiteren Punkte findest du unter http://docs.kieselstein-erp.org.

Wir wünschen dir viel Freude mit deinem umfassenden ERP-System. Anregungen, Wünsche aber auch eventuelle Fehlermeldungen kannst du gerne an develop@kieselstein-erp.org senden.