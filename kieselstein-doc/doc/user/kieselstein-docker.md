# Running Kieselstein from docker

https://docs.docker.com/engine/reference/commandline/compose/#child-commands


https://gitlab.com//api/v4/projects/42809549/repository/archive.zip?path=kieselstein-docker/compose

## Creating tar for 'anwenderreports'

Execute this from the parent dir of your report dir:
```bash
find report -wholename "*/anwender/*" -type f -print0 | tar -cvzf user-reports.tar.gz --null -T -
```


ToDo:

max_wal_size in docker config einstellen (kommen bei Thomas meldungen beim import)