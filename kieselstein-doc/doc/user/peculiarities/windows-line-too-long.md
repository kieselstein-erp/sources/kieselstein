# "Line Too Long"

This problem might occur, e.g. when launching the kieselstein ui from a 
deeply nested folder-hierarchy on windows. The reason is that in windows, 
the maximum path length is restricted to 260 characters, as described [here](https://learn.microsoft.com/en-us/windows/win32/fileio/maximum-file-path-limitation)

There are 2 practical solutions for this:

* Copy the client to a different location, so that the path gets shorter (e.g. `c:\kieselstein\client` ).
* Remove the limitation on your windows installation. This is possible, starting from Windows 10, version 1607, as described [here](https://learn.microsoft.com/en-us/windows/win32/fileio/maximum-file-path-limitation?tabs=registry#enable-long-paths-in-windows-10-version-1607-and-later).
