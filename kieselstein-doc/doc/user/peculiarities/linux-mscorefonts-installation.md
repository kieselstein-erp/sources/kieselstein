# Installing MS core fonts on linux

Kieselstein requires the MS core fonts on the server for report generation. While on a windows machine,
these are present per default, they have to be manually installed on linux. Here are some hints how to accomplish this 
on various linux flavours:

## Debian

* Add `non-free` and `contrib` to each line in `/etc/apt/sources.list`, either manually using an interactive editor, or using sed, like:
```bash
sed -i -e's/ main/ main contrib non-free/g' /etc/apt/sources.list
```
* Refresh the index
```bash
apt-get -q update
```

And finally, install the package:
```bash
apt-get -qy ttf-mscorefonts-installer
```

## Redhat?

Something like (to be checked)
```bash
rpm -i https://downloads.sourceforge.net/project/mscorefonts2/rpms/msttcore-fonts-installer-2.6-1.noarch.rpm
```


