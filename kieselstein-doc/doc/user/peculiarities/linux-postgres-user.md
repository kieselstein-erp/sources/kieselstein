# Postgres on Linux

## Password for postgres user

When installing Kieselstein on a fresh linux system (with a fresh postgres server), 
then it is required to set also the password for the postgres (as used by kieselstein by default)

This is done by launching the psql shell as root, eg. by typing:
```bash
sudo su 
su postgres
psql
```

And then inside psql:
```
ALTER ROLE postgres WITH PASSWORD 'postgres';
exit
```
From now on the postgres user has the password 'postgres'.

## Installation

The installation of postgres itself (to be done before the above steps), depends on the operating system. 
For some, the required steps are summarized in the following sections 

### Ubuntu

```
apt-get install postgresql
sudo su
/etc/init.d/postgresql start
```
