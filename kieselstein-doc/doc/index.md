# Kieselstein Docs

Welcome to the core documentation for Kieselstein ERP!

This documentation is divided into 2 main parts:

|                                                                                          |
|:-----------------------------------------------------------------------------------------|
| [user documentation](user)<br/> How to run and use Kieselstein.                           |
| [development documentation](development) <br/> How to start contributing to Kieselstein. |

In addition, there are some prerequisites which apply to both scenarios. Those can be found in a separate
doc: [prerequisites](prerequisites.md)

## Common Problems / Peculiarities

| Peculiarity                                                             | Context                                          |
|:------------------------------------------------------------------------|:-------------------------------------------------|
| ["Line Too Long" Error](user/peculiarities/windows-line-too-long.md)    | Windows, when launching the client               |
| [postgres password on Linux](user/peculiarities/linux-postgres-user.md) | linux, when installing a fresh postgres          |
| [Installing ms-core fonts on Linux](user/peculiaritis/linux-mscorefonts-installation.md) | Linux, when errors appear when printing reports. |