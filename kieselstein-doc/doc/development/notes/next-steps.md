


# Next steps

## cf ports etc

* in docker, to access the host machine: host.docker.internal (instead of localhost)
* -> needs to be an ENV_VAR
* Add EXPOSE to docker file (otherwise --expose 8008 -p 8008:8008) -p HOST_PORT:CONTAINER_PORT
* Strategy: First counts, otherwise default values...

## database & Testing

* fert_los: view der irgendwie zrirkular ist ... noch zu checken
* shop-timer exceptions disablen.

* feldlaengen-probleme analysieren 
* Open question: Werden bei exceptions die FK-namen (und PK) verwendet fuer die Auswertung der Fehlerursache? -> 
  * Ist Voraussetzung, dass die bleiben koennen.
  * Wenn das nicht geht, dann automatisch anpassen. (Vorlage z.b.: https://dba.stackexchange.com/questions/243894/rename-foreign-keys-en-masse-on-sql-server-2017)


## To TEST!
* Find some way to change kieselstein parameters / potential web UI?

## build system ToDos
* coverage von rest tests mergen (coverage von rest-service auch?) 
Goal: Get blazingly fast
* separate docker base, to be used in any build and for final docker images (separate repo -> much smaller checkout)
* dependency management -> get rid of submodules (potentially eclipse works then also better)

## (Quasi-) Continuous deployment

Nach jedem Sprint (2-3 Wochen):
  * neuer docker verf�gbar
  * Aus der Sicht des kunden: "Achtung out-of date - Sicherheitsl�ck ROT!!!"
  * Kunde muss selbst einspielen k�nnen


To be split later into issues

* Zwischenablage testen
* lpserver applicationName and displayName (ear) -> must become kieselstein ...
* src sets should respect usual convention (`src/main/java` and `src/test/java`)

### database

* db tool (liquibase or flywaydb)
* renaming? -> only after migration tool in place
* Test to compare db generated from server with migrated db. How? (E.g. dump structure only and do text compare? Better ideas?)

Open Questions
* db password->should this come from the commandline?
* report de_CH hat falsche verzeichnisstruktur
* microsoft fonts should be downloaded directly

## einfaches entwickeln/testen (z.b. server mit inmemory db)
* z.b. docker up db ...

## build system ToDos
Goal: Get blazingly fast
* Derzeit zu viel in einem job -> splitten (e.g. build und tests)
* dependency management -> get rid of submodules (potentially eclipse works then also better)

## More ideas:
https://docs.jboss.org/author/display/WFLY8/Command%20line%20parameters.html