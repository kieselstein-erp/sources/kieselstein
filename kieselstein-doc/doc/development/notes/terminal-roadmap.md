# Terminal Konzept Proposal

## Preconditions

* Build-system cleanup
  * Siehe [next steps](next-steps.md)
* Quasi-Continuous deployment:
  * Docker
  * requirement: Report-Struktur review:
    * Anwender-reports ausserhalb docker/wildfly
    * Standard reports mit wildfly ausgeliefert
    * Wenn standard report ge�ndert -> dann anwender report zu �ndern.

## N�chste Schritte

* Rest API einbinden (merge repo hinein)
* Scripts als eigenst�ndigen tomcats
* vmtl neuere java version
* REST API docu ist im moment nicht dabei in der open source version
  (-> 4 MD, damit die bestehende Version einmal l�uft)

----

----

* Wenn das l�uft, dann Andi anschauen -> ob das leicht auf OpenAPI umgebaut werden kann.
* Als eigenes War in den tomcat deployen
* Tests schreiben, die auf beiden APIs laufen

----

STEP -> Calls auf derzeitige rest-api m�glich

----

* Logik f�r neue calls implementieren (in neuer technologie)


### F�r sp�ter
* Anschauen, ob man die REST API einfach am wildfly deployen kann (wenn auf neuerer version von wildfly)

### Ideen

* OpenApi: -> mit swagger rest api generieren
* UI-Technologie:
  * Xamarin (Maui) -> HW-Zugriff in C#
  * UNO - Platform -> C#
  * Mobile development (expo IO) - typescript - peripherie?
* Maschinenanbindung (z.b. Beckhoff)


### Input - Corinna
* Was muss die minimal-Version k�nnen?
* ... und dann weiter