# kieselstein technology migration roadmap

For the moment, this are just ideas how the road could be towards newer technologies

### Where we are ...
Currently, we are using wildfly 12.

Looking at the following table [here](https://www.mastertheboss.com/jbossas/jboss-eap/what-is-the-difference-between-jboss-eap-wildfly-and-jboss-as/),
we are somewhere between javaee 7 and 8. 

Furthermore, the following table from wikipedia might be of interest (comparing jboss vs wildfly versions):
https://de.wikipedia.org/wiki/WildFly








Things to consider:

### JDK & wildfly

The possible JDK version seems to be driven by wildfly:

[wildfly-jdk-versions](https://www.mastertheboss.com/jbossas/wildfly-8/choosing-the-jdk-for-wildfly-and-jboss-eap-7/)

In short: 
* up wildfly 26, jdk 8 is the minimum, 
* then from wildfly 27 onwards jkd 11 is the minimum
* The max jdk slowly increases

### javafx

* Either stay with the azul jdks, or make the switch to openjdk soon (and openjfx)



### Conclusion

One option seems to be:
* Walk through the wildfly versions slowly and stay quite some time with jdk 8
