# Notes on the build pipeline

This aims to be a concise description of how the current build of the kieselstein ERP system is set up, its caveats as
well its improvement potentials.

The current build pipeline for the system looks currently somehow like this:

![pipeline](images/pipeline.png)

The path goes from left to right and the grey lines represent dependencies between the jobs. The following might be
noted:

* We consider the names of the jobs self-explanatory. They produce corresponding artifacts (e.g. the `build-server`
  produces a fully configured zip file with the kieselstein-server etc.)
* Only the last stage produces test results

## Caching

To optimize the builds, gradle distributed caching (using gitlab caching) is used. This helps to reuse artifacts and
e.g. compiled classes across builds (and even across branches / merge requests). This way we *should* be able to get
relatively quick feedback if a branch / merge request works or not.

The aproach is very similar to the one
described [here](https://blog.jdriven.com/2021/11/reuse-gradle-build-cache-on-gitlab/). Also the configuration for merge
requests was changed like as follows:

* Fast Forward merge (no merge commits)
* Encourage squashing commits when merging

## Caching

For some parts, gradle caching is enabled:
* https://blog.jdriven.com/2021/11/reuse-gradle-build-cache-on-gitlab/
* https://docs.gitlab.com/ee/ci/caching/



## Caveats / Open questions / potential improvements

* NOTE: The improvement of the above-mentioned measures was not yet measured - so there is still a slight chance that
  downloading the
  cache eats up the improvement. - to be checked
* Some of the build - jobs (at least those which require javafx and database) - use a pre-compiled docker image, produce
  by a separate project: https://gitlab.com/kieselstein-erp/sources/kieselstein-docker. Unfortunately, this image is
  currently based on debian and thus relatively big. Switching to alpine should improve build times. (We just have to
  find out how to install zulu on alpine)
* Some of the jobs also still use oraclejdk -> this should be also changed to zulu (potentially all could use the same
  image if small.)
* The testing job still lets the server create its own db -> at some point this should be changed that e.g. the script
  from the previous stage is used (or a future db-migration tool - to be seen.)

## ToDos

* buildscripts: use task.register syntax in gradle files
* Release on tag
* Putting artifacts to gitlab repo? Would this improve build time?