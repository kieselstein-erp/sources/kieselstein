# Kieselstein Development

[Java 11](java11.md)


This document describes, how to start building kieselstein for development.

Also in this case, the [prerequisites apply](../prerequisites.md)



## Getting Started

For the moment, this project contains submodules with the legacy projects, as they
contain required libraries. To get all of them in one go, it is recommended to clone with the `--recurse-submodules`
option:

```
git clone --recurse-submodules <repo>
(this was not necessary when installing on 01/2025 and was
not mentioned when loading this project by intelliJ importer
so maybe deleted this part?)
```

Then, to check if the source code compiles, use the following commands from inside the cloned dir:

### on linux:

```
./gradlew compileJava
```

### on windows:

```
./gradlew.bat compileJava
```

### on macos (apple silicon m2)
Download Java 11 (LTS) for MacOS ARM 64-bit JDK FX (IMPORTANT THE FX VERSION!) 
with this Link https://www.azul.com/downloads/?version=java-11-lts&os=macos&architecture=arm-64-bit&package=jdk-fx#zulu
Then set this version in intelliJ for the project globally.

Open "kieselstein.client/build.gradle" and deactivate "id 'org.openjfx.javafxplugin' version '0.1.0'"
by comment it out + also disable javafx {..} settings.

Open the tab menu item of "Gradle" and right click on "kieselstein"
and press "Refresh Gradle Dependencies" otherwise you get strange
errors that he can't load parts of the JavaFX Stuff.

```
./gradlew.bat compileJava
```

There will be some warnings but there should be no errors.

In the following, `./gradlew` always refers to the `./gradlew.bat` for windows and `./gradlew` for linux.

## bootstrapping db and running kieselstein

#### 1. Create and fill the database with minimum data

To create a database and fill data for development, the following build tasks can be used:

``` bash
createdb "KIESELSTEIN"
createdb "KIESELSTEIN_DOCUMENTS"

# Setting up initial database
GO TO Gradle Tasklist in IntelliJ kieselstein-distpack -> Tasks -> liquibase -> updateDatabase

# When updating existing databased the following commands can be used.
./gradlew kieselstein-database:setDbVersion
./gradlew kieselstein-database:updateDb
```

#### 2. Configure a fresh wildfly server (with kieselstein in it)

```bash
./gradlew kieselstein-server:configureWildfly
./gradlew kieselstein-server:deployEar
```

This downloads a wildfly zip, unzips it and promotes all the jars and configuration files into the unpacked dir. The
result can be found below [`./kieselstein-server/build/wildfly`](./kieselstein-server/build/wildfly).

#### 3. Launch the server
Go inside IntelliJ to Run/Debug Configurations of "JBoss/WildFly 26.1.2.Final"
then next to "Application server" click on the button "Configure" 
and set the "JBoss/WildFly Home Path" to 
"[System path]../kieselstein/kieselstein-server/build/wildfly/wildfly-26.1.2.Final"
and save it.

```bash
./gradlew kieselstein-server:launchWildfly
```

After this step the server keeps running! If you want to use it, leave it like this.

#### 4. launch the client

```bash
./gradlew kieselstein-ui-swing:run
```

#### other useful gradle tasks

To stop the wildfly server, one can use:

```bash
./gradlew kieselstein-server:shutdownWildfly
```

To launch the server without database creation:

```bash
./gradlew kieselstein-server:launchWildfly
```

To drop the database (if nobody is accessing it)

```bash
./gradlew kieselstein-server:dropDb
```

And finally, if something is misbehaving and/or messed up, then use

```bash
./gradlew clean
```

This deletes the content of all build directories and one can start happily allover again.

## IntelliJ

In intellij, simply use new->project from existing source, select the folder where you cloned the repo and then select
to import from gradle model.

More instructions (e.g. for debugging etc.) can be found [here](doc/development/intellij/intellij-howto.md).

## Eclipse

In Eclipse, import this directory as gradle project.

Some additional configuration is required cf the defaults and for debugging the server within eclipse. This is described
in
a [separate file](doc/development/eclipse/eclipse-config.md).

## build pipeline

Notes concerning the kieselstein build pipeline are collected in a [separate document](doc/development/build-pipeline/README.md).

## Coding Guidelines

### Ticket workflow

When an Issue is created the label status:open is set. Then an issue will be set to status:to estimate where the implementation time of an issue will be determined by responsibles. 
After that an issue will be set to status:estimated and if instructed for implementation to status:instructed. If a developer decides to start working on an issue the developer needs to do the following:
change the Assignee to himself (the developer who implements the feature of fixes the bug) and change the label from status:instructed to status:in progress. An issue has to be developed in new branch that originated from the latest master commit.
When finished the branch has to be merged to develop for testing. Therefore, a merge request will be created. If granted the responsible developer has to change the status to Feedback. The change will be tested. 
If further changes need to be done the request will be denied and handed back to the developer with the status:in progress. If no further changed are necessary the status of the issue will be set to status:approved. 
After that the issue status will be set to status:documentation required. Therefore, the developer has to describe functionality in the open issue. The develop branch will be merged into the master branch periodically. 
After the branch was merged to the master the developer can close the issue. 

For prioritizing issues the label "priority" will be set. For sorting high priority issues the issues will also be weighted by a number between 1 and 100. Higher weighted issues have to be prioritized.
It is important that the status of the issue is set correctly to ensure that only one developer is working on an issue. 
Another important step is to set the milestone in the issue to determine in which version the feature will be available. 

### Versioning

Versioning in Kieselstein ERP will be handled with git tags. The version number has the following structure: Major.Minor.Patch, e.g:
0.0.13. If a developer starts implementing a feature the developer checks out from the develop branch with the current version number and creates a new branch while he is working on the feature.
After the implementation the developer merges his commits into the developer branch and bumps the version numbers patch number.
Periodically the develop branch will be merged into the master branch and the minor version will be bumped up e.g 0.x.0. 
In this step the master will then be merged into the develop branch and the develop version will bumped up to the next version e.g 0.x.1. Then a new milestone will be created with the next master version. 
When a new releases is created the Major number will be bumped up. 

### Process branches
![](./Ablauf-Versionsverwaltung-Branches.svg)
- When a new Feature or Bugfix should be implemented a new issue branch will be created from the develop branch.
- When the implementation is done the issue-branch will be merged in the develop-branch.
- Git Commits should contain the issue number and FIX, WIP OR NEW e.g. [#WIP: issue-number: description] for better understanding. 
- Then a testphase begins
- **Special-Cases**
  - When the implementation of an issue depends on changes which are not in the master at the time of starting, it's also possible that the new issue-branch will be checked out from the develop branch.
    - See Above Sample for Issue #c

### Therefore, after the merge into develop or master a tag has to be created:

creating a tag <br/>
<code>
git tag -a [version number] -m [tagging message]
</code>

pushing a tag <br/>
<code>
git push origin [version number]
</code>

### other usefull commands for git tags

list all tags <br/>
<code>
git tag -l 
</code>

list all tags from develop sorted by refname <br/>
<code>
git tag -l --sort=-v:refname --merged develop
</code>
This will only work if the develop branch is locally checked out.

list all tags from master sorted by refname <br/>
<code>
git tag -l --sort=-v:refname --merged master
</code>

for further information: https://git-scm.com/book/en/v2/Git-Basics-Tagging

Here is an example: a developer check out the develop branch of version 1.0.0. The features are implemented in the new branch and is later merged into the develop branch.
After the merge the developer does the following: git tag - a 1.0.1 -m "feature implementation v 1.0.1" and git push origin 1.0.1. 
### Database migrations

If database changes have been implemented the changes have to be added to the changelog in the /kieselstein-database/src/main/resources/changelogs.
To ensure that migrations are executed are in the correct order the folders have to be named correctly. The version number of the folder have to be identically to the next version in the develop branch.
E.g: if the current develop version is 0.0.13. The next changes will be put into folder 0.0.14
See also [Example Changelogs: XML Format](https://docs.liquibase.com/concepts/changelogs/xml-format.html) for more information.

### Packages
Type Definitions, Interfaces and Implementation should not be in the same package

### Docstrings
There should be docstrings for every class and method. 
Doctrings are written in english except german technical terms. 
If the Function name is clear (e.g. setters and getters) no docstrings are needed.

### Functions
Functions should be kept short. If there are new features functions should be kept in a reasonable length. 
If the function is too long helper functions should be created. If there are changes regarding bug fixes old code will be refactored 

this example shows a function that is too long

<code>

    private void setInhalt() throws Throwable {
        mandantCNr = theClientDto.getMandant();
        locUI = theClientDto.getLocUi();
        hmDaten = new ArrayList<AuftragUebersichtTabelleDto>();
        DateFormatSymbols symbols = new DateFormatSymbols(locUI);
        aMonatsnamen = symbols.getMonths();
    
        getFilterKriterien();
        FilterKriterium fkAuswertung = aFilterKriterium[AuftragFac.UMSATZUEBERSICHT_IDX_KRIT_AUSWERTUNG];
        FilterKriterium fkJahr = aFilterKriterium[AuftragFac.UMSATZUEBERSICHT_IDX_KRIT_JAHR];
        FilterKriterium fkPlusjahre = aFilterKriterium[AuftragFac.UMSATZUEBERSICHT_IDX_KRIT_PLUS_JAHRE];
    
        Integer iJahr = new Integer(fkJahr.value).intValue();
    
        // PJ21214
        Integer plusJahre = new Integer(fkPlusjahre.value).intValue();
    
        boolean bGerschaeftsjahr = false;
        if (fkJahr.kritName.equals(AuftragFac.KRIT_UEBERSICHT_GESCHAEFTSJAHR)) {
            bGerschaeftsjahr = true;
        }
    
        // Zeile 1 Vorjahr
        RechnungUmsatzTabelleDto zeileDto = new RechnungUmsatzTabelleDto();
        zeileDto.setSZeilenheader(getTextRespectUISpr("lp.vorjahr", mandantCNr, locUI) + " " + (iJahr - 1 + plusJahre));
    
        GregorianCalendar[] gcVonBis = getVorjahr(iJahr, plusJahre, bGerschaeftsjahr);
    
        hmDaten.add(befuelleDtoAnhandDatumsgrenzen(
                getTextRespectUISpr("lp.vorjahr", mandantCNr, locUI) + " " + (iJahr - 1 + plusJahre),
                fkAuswertung.kritName, gcVonBis[0], gcVonBis[1]));
    
        // Leerzeile
        hmDaten.add(new AuftragUebersichtTabelleDto());
    
        // Nun Monate des aktuellen GF durchgehen
    
        ArrayList<GregorianCalendar[]> alMonate = getMonateAktuellesJahr(iJahr, plusJahre, bGerschaeftsjahr);
    
        for (int i = 0; i < alMonate.size(); i++) {
    
            GregorianCalendar[] gcVonBisMonate = alMonate.get(i);
    
            hmDaten.add(befuelleDtoAnhandDatumsgrenzen(aMonatsnamen[gcVonBisMonate[0].get(GregorianCalendar.MONTH)],
                    fkAuswertung.kritName, gcVonBisMonate[0], gcVonBisMonate[1]));
        }
    
        // Leerzeile
        hmDaten.add(new AuftragUebersichtTabelleDto());
    
        // Summe aktuelles Jahr
        GregorianCalendar[] gcVonBisAktuell = getAktuellesJahr(iJahr, plusJahre, bGerschaeftsjahr);
    
        hmDaten.add(befuelleDtoAnhandDatumsgrenzen(
                getTextRespectUISpr("lp.summe", mandantCNr, locUI) + " " + (iJahr + plusJahre), fkAuswertung.kritName,
                gcVonBisAktuell[0], gcVonBisAktuell[1]));
    
        // Summe Gesamt
        GregorianCalendar[] gcVonBisGesamt = getGesamtJahr(iJahr, plusJahre, bGerschaeftsjahr);
    
        hmDaten.add(befuelleDtoAnhandDatumsgrenzen(getTextRespectUISpr("lp.gesamtsumme", mandantCNr, locUI),
                fkAuswertung.kritName, null, null));
    
        setAnzahlZeilen(hmDaten.size());

    }
</code>

### Indents and Lines

Indents and lines are to be kept in logical structure. 
In case of doubt use one line and helper variables even if line becomes too long

This example shows how a block of code can be structured to ensure readability by removing line breaks 

<code>

    public FilterKriterium[] createFKLagerlistefuerLagerListeMitMandant() throws Throwable {
        FilterKriterium[] krit = new FilterKriterium[4];

		krit[0] = new FilterKriterium("flrlager.c_nr", true, "('" + LagerFac.LAGER_KEINLAGER + "')",
				FilterKriterium.OPERATOR_NOT_IN, false);
		krit[1] = new FilterKriterium("flrlager.lagerart_c_nr", true, "('" + LagerFac.LAGERART_WERTGUTSCHRIFT + "')",
				FilterKriterium.OPERATOR_NOT_IN, false);

		krit[2] = new FilterKriterium("flrlager.b_versteckt", true, "(1)", FilterKriterium.OPERATOR_NOT_IN, false);

		krit[3] = new FilterKriterium("flrsystemrolle.i_id", true,
				DelegateFactory.getInstance().getTheJudgeDelegate().getSystemrolleIId() + "",
				FilterKriterium.OPERATOR_EQUAL, false);

		return krit;
	}

    public FilterKriterium[] createFKLagerlistefuerLagerListeMitMandant() throws Throwable {
        FilterKriterium[] krit = new FilterKriterium[4];

		krit[0] = new FilterKriterium("flrlager.c_nr", true, "('" + LagerFac.LAGER_KEINLAGER + "')", FilterKriterium.OPERATOR_NOT_IN, false);
		krit[1] = new FilterKriterium("flrlager.lagerart_c_nr", true, "('" + LagerFac.LAGERART_WERTGUTSCHRIFT + "')", FilterKriterium.OPERATOR_NOT_IN, false);
		krit[2] = new FilterKriterium("flrlager.b_versteckt", true, "(1)", FilterKriterium.OPERATOR_NOT_IN, false);
		krit[3] = new FilterKriterium("flrsystemrolle.i_id", true, DelegateFactory.getInstance().getTheJudgeDelegate().getSystemrolleIId() + "", FilterKriterium.OPERATOR_EQUAL, false);

		return krit;
	}

</code>

This example shows how a block of code can be structured to ensure readability by using helper variables

<code>

    public TableInfo getTableInfo() {
        if (tableInfo == null) {
            tableInfo = new TableInfo(
                new Class[] { String.class, String.class, BigDecimal.class, BigDecimal.class, BigDecimal.class,
                    String.class, BigDecimal.class, BigDecimal.class, BigDecimal.class },
                new String[] { " ", " ",
                    getTextRespectUISpr("auft.freieauftraege", theClientDto.getMandant(),
                    theClientDto.getLocUi()),
                    getTextRespectUISpr("auft.abrufautraege", theClientDto.getMandant(),
                    theClientDto.getLocUi()),
                    getTextRespectUISpr("auft.rahmenauftraege", theClientDto.getMandant(),
                    theClientDto.getLocUi()),
                    " ",
                    getTextRespectUISpr("auft.freieauftraege", theClientDto.getMandant(),
                    theClientDto.getLocUi()),
                    getTextRespectUISpr("auft.abrufautraege", theClientDto.getMandant(),
                    theClientDto.getLocUi()),
                    getTextRespectUISpr("auft.rahmenauftraege", theClientDto.getMandant(),
                    theClientDto.getLocUi()) },
                new String[] { "", "", "", "", "", "", "", "", "" });
        }

		return tableInfo;
	}


    public TableInfo getTableInfo() {
        if (tableInfo == null) {
            Class [] helper_one = 	new Class[] { String.class, String.class, BigDecimal.class, BigDecimal.class, BigDecimal.class, String.class, BigDecimal.class, BigDecimal.class, BigDecimal.class },
            String [] helper_two = new String[] { " ", " ",
                getTextRespectUISpr("auft.freieauftraege", theClientDto.getMandant(), theClientDto.getLocUi()),
                getTextRespectUISpr("auft.abrufautraege", theClientDto.getMandant(), theClientDto.getLocUi()),
                getTextRespectUISpr("auft.rahmenauftraege", theClientDto.getMandant(), theClientDto.getLocUi()), " ",
                getTextRespectUISpr("auft.freieauftraege", theClientDto.getMandant(), theClientDto.getLocUi()),
                getTextRespectUISpr("auft.abrufautraege", theClientDto.getMandant(), theClientDto.getLocUi()),
                getTextRespectUISpr("auft.rahmenauftraege", theClientDto.getMandant(), theClientDto.getLocUi())
            };
            String [] helper_three = new String[] { "", "", "", "", "", "", "", "", "" };
    
            tableInfo = new TableInfo(helper_one, helper_two, helper_three);
		}

        return tableInfo;
	}
</code>