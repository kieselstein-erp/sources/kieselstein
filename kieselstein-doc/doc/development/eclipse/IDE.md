Downloads:
Eclipse 2022
	Windows:	https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2022-12/R/eclipse-jee-2022-12-R-win32-x86_64.zip&mirror_id=17
	Linux:		https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2022-12/R/eclipse-jee-2022-12-R-linux-gtk-x86_64.tar.gz&mirror_id=17

Postgresql 14
	Windows:	https://get.enterprisedb.com/postgresql/postgresql-14.7-1-windows-x64.exe
	Linux:		je nach Distribution siehe https://www.postgresql.org/download/

VirtualBox:
	Windows:	https://download.virtualbox.org/virtualbox/7.0.6/VirtualBox-7.0.6-155176-Win.exe
	Linux:		je nach Distribution siehe https://www.virtualbox.org/wiki/Linux_Downloads

Installation
- Virtualbox installieren und starten
- VM mit Windows 10 erstellen
- Installationsdateien in die VM kopieren 
- Zulu-jdk installieren
- Git installieren
- eclipse entpacken nach c:\eclipse
- postgresql installieren (Passwort postgres, Port 5432, Locale Default)

optional:
	- Totalcommander installieren
	- Notepad++ installieren
	
Einrichten
- Ordner für c:\gitwork erstellen

- GitBash starten
```
	# cd /C/gitwork
	# git clone --recurse-submodules https://gitlab.com/kieselstein-erp/sources/kieselstein.git
```
- eclipse starten (default workspace)
	- File/Import/Gradle/Existing Gradle Projekt ausführen(aus Ordner c:/gitwork/kieselstein)
	
	JDK 8
	- Window Preferences öffnen
		- Java - "Installed JREs" auswählen und [Add...] klicken
		- im Dialog "Add JRE" "Standard VM" wählen und [Next >] klicken, mit Button [Directory...] den Ordner "Programme\Zulu\zulu-8" wählen und [Finish] klicken
		- [Apply and Close] klicken
	
## Wildfly installieren/konfigurieren

- Im subproject 'kieselstein-server' den task 'configureWildfly' ausführen. Dieser task lädt den wildfly server herunter, entpackt ihn und kopiert die benötigten jars und modules hinein. Der fertig konfigurierte wildfly liegt dann unter [`kieselstein-server/build/wildfly-12.0.0.Final`](../../kieselstein-server/build/).

- im Reiter Servers auf ".. Click this link ..." klicken
- Red Hat JBoss Middleware - Jboss AS.. auswählen, [Next >] klicken und Lizenz akzeptieren, nach Installation Neustart von eclipse bestätigen
- erneut im Reiter Servers auf ".. Click this link ..." klicken
- "Jboss Community - Wildfly 12" auswählen, [Next >] klicken, Einstellungen belassen und [Next >] klicken

- im Dialog "JBoss Runtime" auf "Browse" clicken und als home directory then obigen pfad (`kieselstein-server/build/wildfly-12.0.0.Final`) auswählen.

- im Dialog "JBoss Runtime" auf "Download und install runtime" klicken
	- im Dialog "Download Runtimes Wildfly 12" anwählen und [Next >], Lizenz akzeptieren, [Next >] klicken und [Finish] klicken
- nochmals auf [Finish] klicken
- den neu erstellten Servereintrag (Wildfly 12] mit rechter Maustaste anklicken und Add and Remove... wählen
	- im Dialog links kieselstein-ear anwählen und [Add >] klicken, [Finish] klicken
- die Datei "kieselstein-standalone.xml" aus dem Prokjekt Ordner "kieselstein-server/config" nach "c:\Users\OS\wildfly-12.0.0.Final\standalone\configuration" kopieren
- im Reiter Servers den Eintrag "Wildfly 12" doppelklicken und den Link "Runtime Environment" anklicken
- das "Configuration file" ändern auf "kieselstein-standalone.xml" und [Finish] klicken, mit Save oder Strg-S die Änderung speichern
- den Ordner "c:\gitwork\kieselstein\legacy\lpserver\wildfly\libs\modules" in den Ordner "c:\Users\OS\wildfly-12.0.0.Final\modules" kopieren (bestehende Dateien er setzen)
- den Ordner "modules" aus "c:\gitwork\kieselstein\legacy\lpserver\wildfly\wildfly-helium_skel.zip" in den Ordner "c:\Users\OS\wildfly-12.0.0.Final\modules" kopieren
	
	Client
	- kieselstein-ui-swing mit rechter Maus anklicken und Properties wählen
		- im Reiter Libraries die beiden Libs "jfxrt.jar" und "rt.jar" aus dem Zulu-jdk mit [Add External JARs...] hinzufügen
		- im Reiter Order and Export diese beiden ganz nach oben schieben
		- [Apply and Close] klicken
	- kieselstein-ui-swing mit rechter Maus anklicken und Properties wählen
		- "Run/Debug Settings" wählen und [New...] klicken
		- "Java Application" wählen und [OK] klicken
		- Name auf "Client" setzen
		- "Main class" auf "LPMain - com-lp-client.pc" setzen
		- im Reiter Arguments folgendes einfügen
			Program arguments
				--showiids
			VM arguments
				-Xms128m -Xmx512m -Djava.naming.factory.initial=org.wildfly.naming.client.WildFlyInitialContextFactory -Djava.naming.provider.url=remote+http://localhost:8080 -Dloc=de_AT -Dsun.java2d.uiScale=1
		- [Apply and Close] klicken
	
	