# Configuration for compiling and debugging in eclipse


## Importing the kieselstein project

After checking out, as described in the [main README](../../../README.md), import the folder as a gradle project by
navigating in the menu/dialogs to `File->Import->Gradle->Existing Gradle Project`.

Then execute the gradle target `kieselstein-server:configureWildfly` (from command line or
eclipse buildship plugin). This will prepare a wildfly server
under [`kieselstein-server/build/wildfly/wildfly-12.0.0.Final`](../../../kieselstein-server/build/wildfly/wildfly-12.0.0.Final),
which can later be used for debugging in eclipse.

WARNING: In this case do ***NOT*** execute the task `deployEar`! When debugging, eclipse will deploy the required
classes by its own mechanism.

TODO: describe further. Draft in [IDE.md](IDE.md).

## Warning/Errors configuration

Switch access rule violations to warning (are errors by default).

![access-rule-violation](images/forbidden-reference-access-rule.png)

Otherwise, eclipse complains that the kieselstein code accesses code in the ext dir of the jdk (javafx for zulu jdk).
