# Kieselstein development in IntelliJ

* Import the Project as gradle project
* Follow the instructions in the [main README](../../../README.md) to create a db and launch a wildfly server.

## debugging

### debugging the ui/client

* Set a breakpoint somewhere in the client code
* Right click on the `run` task of the 'kieselstein-ui-swing' subproject, select 'Debug ...' from the appearing menue,
  as shown in the following image:
  ![context menu](images/start-debugging-ui.png)

### debugging the server

* Run the gradle task `launchWildflyDebug` of the 'kieselstein-server' subproject.
* Wait until the task terminates (the server continuous running in the background)
* Set a breakpoint in the sourcecode
* Select from the Menu 'Run'->'Attach to Process ...' and select from the appearing menu the wildfly instance, as shown
  in the following image:
![attach to process](images/attach-to-process.png)


## recommended plugins

### drawio
To conveniently edit diagrams for the documentation (filetype `*.drawio.svg`), it is beneficial to have the following plugin installed:

![drwaio-plugin](images/drawio-plugin.png)