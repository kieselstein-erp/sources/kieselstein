# Steps to start developing inside the java 11 branch

1. [Install Java 11](https://docs.kieselstein-erp.org/docs/installation/01_server/anforderungen/#java-version)
2. Create/Update the database using liquibase (kieselstein-distpack:updateDatabase gradle task)
3. Run kieselstein-server:configureWildfly gradle task
4. If you are using IntelliJ you can use pre-defined runConfigurations to start the Server and the Client.
   * Runtime options can be configured using environment variables. (launch-kieselstein-main-server.sh)
   * Run Configurations -> Startup/Connection -> Run -> Environment variables
