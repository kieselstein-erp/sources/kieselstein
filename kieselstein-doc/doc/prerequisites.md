## Prerequisites

The following prerequisits have to be fulfilled in both cases, for running kieselstein and for development.

### java version
As Kieselstein for the moment uses a combination of swing and javafx components, and the build process is not (yet) set
up to use proper dependencies, it requires a java which also has the javafx components packaged with it. For the moment
the only java version which is (roughly) tested with kieselstein is the [Azul Zulu jdk](https://www.azul.com/downloads/#zulu). On the website, choose:

* Java Version: 'Java 8 (LTS)',
* Java Package: 'JDK FX'
* The rest (Operating System, Architecture), according to your needs.

E.g. here are some selected links:
* [windows, 64 bit](https://www.azul.com/downloads/?version=java-8-lts&os=windows&architecture=x86-64-bit&package=jdk-fx#zulu)
* [linux, 64 bit](https://www.azul.com/downloads/?version=java-8-lts&os=linux&architecture=x86-64-bit&package=jdk-fx#zulu)

### Java Home
The environment variable 'JAVA_HOME' must be set such that it points to the installed azure jdk installation.

### Database server
Kieselstein uniquely supports postgresql server. Despite other versions might work, we are currently using version 14
for development and testing. Therefore, we also recommend to use this version.

Postgresql can be downloaded from the [website](https://www.postgresql.org/download/).
During the installation, set user and password as follows:

* user: postgres
* password: postgres

The following settings are expected by kieselstein (which are usually the defaults during installation)
* port 5432
* Locale Default

NOTE: Additionally, to make the provided scripts (and also the build scripts for development) work, the  `bin` directory of
the postgres home folder must be added to the 'PATH' variable of the operating system.
